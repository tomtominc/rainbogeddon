﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Gamelogic.Grids2;
using UnityEngine;
using System.Linq;

[Serializable, XmlRoot("level")]
public class Level
{
    public const string BACKGROUND_LAYER = "Background";
    public const string ENTITY_LAYER = "Entities";
    public const string PILL_LAYER = "Pills";
    public const string POWERUP_LAYER = "PowerUps";
    public const string PARTICLE_LAYER = "Particles";
    public const string POWERUP_LAYER_FORMAT = "PowerUpLayer_{0}";

    public const string BACKGROUND_ID = "Background";
    public const string SOFT_WALL_ID = "SoftBlock";

    public const string EMPTY_CELL_ID = "-";

    [XmlAttribute("width")]
    public int width;
    [XmlAttribute("height")]
    public int height;
    [XmlAttribute("startX")]
    public int startX;
    [XmlAttribute("startY")]
    public int startY;
    [XmlAttribute("background")]
    public int background;
    [XmlElement("entity")]
    public string entities;
    [XmlElement("moreEntities")]
    public string pills;
    [XmlElement("powerup")]
    public string powerups;
    [XmlElement("paramText")]
    public List<NitromeXMLParam> paramValues;
    //[XmlElement("paramText")]
    //public List<string> paramText;

    public List<GridPoint2> powerUpPosList;

    public EntityPlayer1 player1
    {
        get { return m_player1; }
    }

    public EntityPlayer2 player2
    {
        get { return m_player2; }
    }

    public void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_levelController = m_controller.getController<LevelController>();
    }

    public string getId(string p_layerName, GridPoint2 p_point)
    {
        if (p_point.X < 0 || p_point.Y < 0 || p_point.X >= width || p_point.Y >= height)
            return string.Empty;

        if (m_tiles[p_layerName][p_point] == null)
            return string.Empty;

        return m_tiles[p_layerName][p_point].id;
    }

    public List<GridPoint2> getRadiusWrapped2(int p_x, int p_y, int p_range)
    {
        //IGrid<GridPoint2, GO> background = m_tiles[BACKGROUND_LAYER];

        GridPoint2 p_point = new GridPoint2(p_x, p_y);

        List<GridPoint2> pointsInRange = Algorithms.GetPointsInRange(
            getExtendingGrid(),
                                            p_point,
            getNeighborsNonWrap,
                                            x =>
                {
                    float realDist = Vector2.Distance(x.ToVector2(), p_point.ToVector2());
                    return realDist < p_range;
                },
                    (p, q) => 1, p_range).ToList();

        List<GridPoint2> wrappedPoints = new List<GridPoint2>();

        for (int i = 0; i < pointsInRange.Count; i++)
        {
            GridPoint2 point = getWrappedPoint(pointsInRange[i]);
            wrappedPoints.Add(point);
        }

        return wrappedPoints;
    }

    public List<GridPoint2> getRadiusWrapped(int p_x, int p_y, int p_range)
    {
        IGrid<GridPoint2, GO> l_backgroundLayer = m_tiles[BACKGROUND_LAYER];

        GridPoint2 p_point = new GridPoint2(p_x, p_y);

        return Algorithms.GetPointsInRange(
                                    l_backgroundLayer,
                                    p_point,
                                    getNeighbors,
                                    x => true,
            (p, q) => 1, p_range).ToList();
    }

    public List<GridPoint2> getRadiusWrapped(string p_layer, int p_x, int p_y, int p_range)
    {
        IGrid<GridPoint2, GO> layer = m_tiles[p_layer];

        GridPoint2 p_point = new GridPoint2(p_x, p_y);

        return Algorithms.GetPointsInRange(
                                    layer,
                                    p_point,
                                    getNeighbors,
                                    x => true,
            (p, q) => 1, p_range).ToList();
    }



    public List<GridPoint2> getGlow(int p_x, int p_y)
    {
        return getGlow(new GridPoint2(p_x, p_y));
    }

    public List<GridPoint2> getGlow(GridPoint2 p_point)
    {
        IGrid<GridPoint2, GO> l_backgroundLayer = m_tiles[BACKGROUND_LAYER];

        return Algorithms.GetPointsInRange(
                                    l_backgroundLayer,
                                    p_point,
                                    getNeighbors,
                                    x => l_backgroundLayer[x] != null,
                                    (p, q) => getSightCost(p_point, p, q), 3).ToList();
    }

    public Dictionary<GridPoint2, float> getCosts(GridPoint2 p_point)
    {
        IGrid<GridPoint2, GO> l_backgroundLayer = m_tiles[BACKGROUND_LAYER];

        return Algorithms.GetPointsInRangeCost(
                                    l_backgroundLayer,
                                    p_point,
                                    getNeighbors,
                                    x => l_backgroundLayer[x] != null,
                                    (p, q) => getSightCost(p_point, p, q), 3);
    }

    private float getSightCost(GridPoint2 p_origin, GridPoint2 p_from, GridPoint2 p_to)
    {
        if (p_origin.X != p_to.X && p_origin.Y != p_to.Y)
            return 2f;

        return 1f;
    }

    public GO getEntity(string p_layerName, GridPoint2 p_point)
    {
        if (p_point.X < 0 || p_point.Y < 0 || p_point.X >= width || p_point.Y >= height)
            return null;

        if (m_tiles[p_layerName][p_point] == null)
            return null;

        return m_tiles[p_layerName][p_point];
    }

    public GO getEntity(string p_layerName, int p_x, int p_y)
    {
        return getEntity(p_layerName, new GridPoint2(p_x, p_y));
    }

    public bool hasEntity(string p_layerName, GridPoint2 p_point)
    {
        if (p_point.X < 0 || p_point.Y < 0 || p_point.X >= width || p_point.Y >= height)
            return false;

        return m_tiles[p_layerName][p_point] != null;
    }

    public bool hasEntity(string p_layerName, string p_id, GridPoint2 p_point)
    {
        if (p_point.X < 0 || p_point.Y < 0 || p_point.X >= width || p_point.Y >= height)
            return false;

        return m_tiles[p_layerName][p_point] != null && m_tiles[p_layerName][p_point].id == p_id;
    }

    public IGrid<GridPoint2, GO> getLayer(string p_layerName)
    {
        return m_tiles[p_layerName];
    }

    public void spawn(string p_layerName, GO go)
    {
        if (!m_tiles.ContainsKey(p_layerName))
        {
            GameObject l_layerObj = new GameObject(p_layerName);
            l_layerObj.transform.SetParent(m_levelController.transform);

            IGrid<GridPoint2, GO> l_layer = Rect<GO>(width, height);
            m_tiles.Add(p_layerName, l_layer);
        }

        Transform child = m_levelController.transform.Find(p_layerName);
        go.transform.SetParent(child);

        GridPoint2 spawnPoint = new GridPoint2(go.tx, go.ty);

        if (m_tiles[p_layerName].Contains(spawnPoint))
        {
            m_tiles[p_layerName][spawnPoint] = go;
        }

    }

    public void move(string p_layerName, int p_ox, int p_oy, int p_tx, int p_ty)
    {
        move(p_layerName, new GridPoint2(p_ox, p_oy), new GridPoint2(p_tx, p_ty));
    }

    public void move(string p_layerName, GridPoint2 p_origin, GridPoint2 p_target)
    {
        GO l_entity = m_tiles[p_layerName][p_origin];
        m_tiles[p_layerName][p_origin] = null;
        m_tiles[p_layerName][p_target] = l_entity;
    }

    public void remove(string p_layerName, int p_x, int p_y)
    {
        remove(p_layerName, new GridPoint2(p_x, p_y));
    }

    public void remove(string p_layerName, GridPoint2 p_origin)
    {
        m_tiles[p_layerName][p_origin] = null;
    }

    public void fillGrid()
    {
        m_tiles = new Dictionary<string, IGrid<GridPoint2, GO>>();

        fillLayer(ENTITY_LAYER, decodeRLE(entities));
        fillLayer(PILL_LAYER, decodeRLE(pills));
        fillPowerUpLayer(decodeRLE(powerups));
        fillBackground();
        fillLayer(PARTICLE_LAYER, null);


        reformLayer(ENTITY_LAYER);

        setParams();

        m_player1 = getPlayer1();
        m_player2 = getPlayer2();
    }

    private void fillBackground()
    {
        GameObject l_layerObj = new GameObject(BACKGROUND_LAYER);
        l_layerObj.transform.SetParent(m_levelController.transform);

        IGrid<GridPoint2, GO> l_layer = Rect<GO>(width, height);
        m_tiles.Add(BACKGROUND_LAYER, l_layer);

        IGrid<GridPoint2, GO> l_entityLayer = getLayer(ENTITY_LAYER);


        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                GridPoint2 l_point = new GridPoint2(i, j);
                GO l_other = l_entityLayer[l_point];

                if (l_other == null || (l_other.id != SOFT_WALL_ID && l_other.id != "HardBlock"))
                {
                    string l_path = string.Format("{0}/{1}", BACKGROUND_LAYER, BACKGROUND_ID);
                    GameObject l_prefab = Resources.Load<GameObject>(l_path);

                    if (l_prefab != null)
                    {
                        GameObject l_obj = UnityEngine.Object.Instantiate(l_prefab);
                        GO l_entity = l_obj.GetComponent<GO>();
                        l_entity.id = BACKGROUND_ID;
                        l_entity.initialize(m_controller, l_point.X, l_point.Y);
                        l_entity.transform.SetParent(l_layerObj.transform);
                        l_layer.SetValue(l_point, l_entity);
                    }

                }

            }
        }
    }

    private void fillLayer(string p_layerName, List<string> p_entities)
    {
        GameObject layerObj = new GameObject(p_layerName);
        layerObj.transform.SetParent(m_levelController.transform);

        IGrid<GridPoint2, GO> layer = Rect<GO>(width, height);
        m_tiles.Add(p_layerName, layer);

        if (p_entities != null)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    GridPoint2 point = new GridPoint2(i, j);
                    string id = p_entities[i + (width * j)];
                    id = id.Split('_')[0];

                    if (id.Equals(EMPTY_CELL_ID))
                    {
                        continue;
                    }

                    string path = string.Format("{0}/{1}", p_layerName, id);
                    GameObject prefab = Resources.Load<GameObject>(path);

                    if (prefab != null)
                    {
                        GameObject obj = UnityEngine.Object.Instantiate(prefab);
                        GO entity = obj.GetComponentInChildren<GO>();
                        entity.id = id;
                        entity.transform.SetParent(layerObj.transform);
                        layer.SetValue(point, entity);
                    }
                    else
                    {
                        Debug.LogWarningFormat("No prefab of type {0} exists!", path);
                        //Debug.LogFormat("no prefab of {0} found.", l_path);
                    }
                }
            }
        }



        foreach (var pair in layer)
        {
            if (pair.cell != null)
            {
                pair.cell.initialize(m_controller, pair.point.X, pair.point.Y);
            }

        }
    }

    private void fillPowerUpLayer(List<string> p_powerUps)
    {
        GameObject l_layerObj = new GameObject(POWERUP_LAYER);
        l_layerObj.transform.SetParent(m_levelController.transform);

        IGrid<GridPoint2, GO> l_layer = Rect<GO>(width, height);
        m_tiles.Add(POWERUP_LAYER, l_layer);

        List<string> powerUpTypes = new List<string>();

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                GridPoint2 point = new GridPoint2(i, j);
                string id = p_powerUps[i + (width * j)];
                string[] param = id.Split('_');
                id = param[0];
                bool isFirstShown = param.Length > 1;
                string path = string.Format("{0}/{1}", POWERUP_LAYER, "PowerUpSpawner");
                GameObject prefab = Resources.Load<GameObject>(path);

                if (prefab != null && id != "-")
                {
                    GameObject obj = UnityEngine.Object.Instantiate(prefab);
                    PowerUpSpawner entity = obj.GetComponent<PowerUpSpawner>();
                    entity.transform.SetParent(l_layerObj.transform);
                    entity.isFirst = isFirstShown;
                    l_layer.SetValue(point, entity);
                    powerUpPosList.Add(point);
                    powerUpTypes.Add(id);

                    //Debug.LogFormat("added {0} as power up type", l_id);
                }
            }
        }

        foreach (var pair in l_layer)
        {
            if (pair.cell != null)
            {
                pair.cell.initialize(m_controller, pair.point.X, pair.point.Y);
                ((PowerUpSpawner)pair.cell).setPowerUpTypes(powerUpTypes);
            }
        }
    }

    public void reformLayer(string p_layer)
    {
        List<GO> blocks = m_tiles[p_layer].Cells.ToList();

        for (int i = 0; i < blocks.Count; i++)
        {
            if (blocks[i] == null)
                continue;

            if (blocks[i] is EntityBlock || blocks[i].GetType().IsSubclassOf(typeof(EntityBlock)))
            {
                ((EntityBlock)blocks[i]).reform();
            }
        }
    }

    public void setParams()
    {
        for (int i = 0; i < paramValues.Count; i++)
        {
            GridPoint2 point = new GridPoint2(paramValues[i].x, paramValues[i].y);

            Debug.LogFormat("setting param for point {0}", point);

            GO go = getEntity(ENTITY_LAYER, point);
            //Debug.Log(go.id);

            if (go != null)
            {
                go.setXMLProperty(paramValues[i]);
            }
        }
    }
    private EntityPlayer1 getPlayer1()
    {
        EntityPlayer1 l_player = null;
        List<GO> l_entities = m_tiles[ENTITY_LAYER].Cells.ToList();

        for (int i = 0; i < l_entities.Count; i++)
        {
            GO l_entity = l_entities[i];
            if (l_entity != null &&
                l_entity is EntityPlayer1)
            {
                l_player = l_entity as EntityPlayer1;
                break;
            }
        }

        return l_player;
    }

    private EntityPlayer2 getPlayer2()
    {
        EntityPlayer2 l_player = null;
        List<GO> l_entities = m_tiles[ENTITY_LAYER].Cells.ToList();

        for (int i = 0; i < l_entities.Count; i++)
        {
            GO l_entity = l_entities[i];
            if (l_entity != null &&
                l_entity is EntityPlayer2)
            {
                l_player = l_entity as EntityPlayer2;
                break;
            }
        }

        return l_player;
    }

    public Queue<GridPoint2> getPath(GridPoint2 p_origin, GridPoint2 p_target)
    {
        IGrid<GridPoint2, GO> l_backgroundLayer = m_tiles[BACKGROUND_LAYER];
        var l_path = Algorithms.AStar(l_backgroundLayer,
                                 p_origin,
                                 p_target,
                                 (p, q) => 1,
                                 x => l_backgroundLayer[x] != null,
                                 getNeighbors,
                                 (p, q) => 1);


        if (l_path == null)
            return new Queue<GridPoint2>();

        return new Queue<GridPoint2>(l_path);
    }

    public Queue<GridPoint2> getPath(int p_x, int p_y, int p_tx, int p_ty)
    {
        GridPoint2 p_origin = new GridPoint2(p_x, p_y);
        GridPoint2 p_target = new GridPoint2(p_tx, p_ty);

        return getPath(p_origin, p_target);
    }

    public int getDirectionFromPoint(GridPoint2 p_origin, GridPoint2 p_target)
    {
        if (p_origin.X > p_target.X)
        {
            if (Mathf.Abs(p_target.X - p_origin.X) > 2)
            {
                return Entity.DIR_LEFT;
            }
            else
            {
                return Entity.DIR_RIGHT;
            }

        }
        else if (p_origin.X < p_target.X)
        {
            if (Mathf.Abs(p_target.X - p_origin.X) > 2)
            {
                return Entity.DIR_RIGHT;
            }
            else
            {
                return Entity.DIR_LEFT;
            }
        }
        else if (p_origin.Y > p_target.Y)
        {
            if (Mathf.Abs(p_target.Y - p_origin.Y) > 2)
            {
                return Entity.DIR_UP;
            }
            else
            {
                return Entity.DIR_DOWN;
            }
        }
        else if (p_origin.Y < p_target.Y)
        {

            if (Mathf.Abs(p_target.Y - p_origin.Y) > 2)
            {
                return Entity.DIR_DOWN;
            }
            else
            {
                return Entity.DIR_UP;
            }
        }

        return Entity.DIR_NONE;
    }


    public Queue<int> getPathByDirections(int p_x, int p_y, int p_tx, int p_ty)
    {
        Queue<int> direction = new Queue<int>();
        Queue<GridPoint2> path = getPath(p_x, p_y, p_tx, p_ty);

        int currX = p_x;
        int currY = p_y;

        while (path.Count > 0)
        {
            GridPoint2 point = path.Dequeue();

            if (point.X == currX && point.Y == currY)
                continue;

            if (point.X > currX)
            {
                if (Mathf.Abs(currX - point.X) > 2)
                {
                    direction.Enqueue(Entity.DIR_LEFT);
                }
                else
                {
                    direction.Enqueue(Entity.DIR_RIGHT);
                }

            }
            else if (point.X < currX)
            {
                if (Mathf.Abs(currX - point.X) > 2)
                {
                    direction.Enqueue(Entity.DIR_RIGHT);
                }
                else
                {
                    direction.Enqueue(Entity.DIR_LEFT);
                }
            }
            else if (point.Y > currY)
            {
                if (Mathf.Abs(currY - point.Y) > 2)
                {
                    direction.Enqueue(Entity.DIR_DOWN);
                }
                else
                {
                    direction.Enqueue(Entity.DIR_UP);
                }
            }
            else if (point.Y < currY)
            {
                if (Mathf.Abs(currY - point.Y) > 2)
                {
                    direction.Enqueue(Entity.DIR_UP);
                }
                else
                {
                    direction.Enqueue(Entity.DIR_DOWN);
                }
            }

            currX = point.Y;
            currY = point.X;
        }

        return direction;
    }

    public override string ToString()
    {
        return string.Format("[Level] (w{0},h{1}), start=({2},{3}), " +
                             "background={4}, entity={5}, powerup={6}",
                             width, height, startX, startY, background, entities, powerups);
    }

    public static List<string> decodeRLE(string p_encoded)
    {
        List<string> l_layerKeys = new List<string>();

        string[] l_keys = p_encoded.Split(',');

        for (int i = l_keys.Length - 1; i > -1; i--)
        {
            string l_key = l_keys[i];

            if (l_key.Contains(":"))
            {
                string[] l_pair = l_key.Split(':');
                int l_count = int.Parse(l_pair[1]);

                for (int j = 0; j < l_count; j++)
                {
                    l_layerKeys.Add(l_pair[0]);
                }
            }
            else
            {
                l_layerKeys.Add(l_key);
            }
        }

        return l_layerKeys;
    }

    public static IGrid<GridPoint2, T> Rect<T>(int width, int height)
    {
        var dimensions = new GridPoint2(width, height);
        var grid = ImplicitShape
            .Parallelogram(dimensions)
            .ToExplicit(new GridRect(GridPoint2.Zero, dimensions))
            .ToGrid<T>();

        return grid;
    }

    public IGrid<GridPoint2, int> getExtendingGrid()
    {
        var dimensions = new GridPoint2(width * 4, height * 4);
        var grid = ImplicitShape
            .Parallelogram(dimensions)
            .ToExplicit(new GridRect(new GridPoint2(-width, -height), dimensions))
            .ToGrid<int>();

        return grid;
    }

    public List<GridPoint2> getNeighbors(GridPoint2 p_point)
    {
        return new List<GridPoint2>()
        {
            getNeighborWrapped(p_point,Direction.RIGHT),
            getNeighborWrapped(p_point,Direction.LEFT),
            getNeighborWrapped(p_point,Direction.UP),
            getNeighborWrapped(p_point,Direction.DOWN)
        };
    }

    public List<GridPoint2> getNeighborsNonWrap(GridPoint2 p_point)
    {
        return new List<GridPoint2>()
        {
            p_point+Direction.RIGHT,
            p_point+Direction.LEFT,
            p_point+Direction.UP,
            p_point+Direction.DOWN
        };
    }

    public GridPoint2 getWrappedPoint(GridPoint2 p_point)
    {
        if (p_point.X < 0)
        {
            p_point = new GridPoint2(p_point.X + width, p_point.Y);
        }
        if (p_point.X > width - 1)
        {
            p_point = new GridPoint2(p_point.X - width, p_point.Y);
        }
        if (p_point.Y < 0)
        {
            p_point = new GridPoint2(p_point.X, p_point.Y + height);
        }
        if (p_point.Y > height - 1)
        {
            p_point = new GridPoint2(p_point.X, p_point.Y - height);
        }

        return p_point;
    }


    public GridPoint2 getNeighborWrapped(GridPoint2 p_point, GridPoint2 p_direction)
    {
        GridPoint2 l_neighbor = p_point + p_direction;

        if (l_neighbor.X < 0)
            l_neighbor = new GridPoint2(width - 1, l_neighbor.Y);

        if (l_neighbor.Y < 0)
            l_neighbor = new GridPoint2(l_neighbor.X, height - 1);

        if (l_neighbor.X >= width)
            l_neighbor = new GridPoint2(0, l_neighbor.Y);

        if (l_neighbor.Y >= height)
            l_neighbor = new GridPoint2(l_neighbor.X, 0);

        return l_neighbor;
    }

    public GridPoint2 getNeighbor(GridPoint2 p_point, GridPoint2 p_direction)
    {
        return p_point + p_direction;
    }


    public bool canGo(GridPoint2 l_origin, GridPoint2 l_direction, bool p_canWrap, bool p_checkSafetyLayer = true)
    {
        GridPoint2 point = GridPoint2.Zero;

        if (p_canWrap)
        {
            point = getNeighborWrapped(l_origin, l_direction);
        }
        else
        {
            point = getNeighbor(l_origin, l_direction);
        }

        bool blocked = false;

        if (m_tiles[PARTICLE_LAYER][point] != null && p_checkSafetyLayer)
        {
            GO go = m_tiles[PARTICLE_LAYER][point];
            blocked = go.GetType() == typeof(EntityBubbleParticle);
        }

        return m_tiles[ENTITY_LAYER][point] == null
            || (!(m_tiles[ENTITY_LAYER][point] is EntityBlock)
                && !(m_tiles[ENTITY_LAYER][point] is EntityBomb))
            && !blocked;
    }

    public bool canGoToTile(int p_x, int p_y, bool p_canWrap = true, bool p_checkSafetyLayer = true)
    {
        int l_finalX = p_x;
        int l_finalY = p_y;

        if (l_finalX < 0)
        {
            if (p_canWrap)
            {
                l_finalX = width - 1;
            }
            else
            {
                return false;
            }
        }
        else if (l_finalX > width - 1)
        {
            if (p_canWrap)
            {
                l_finalX = 0;
            }
            else
            {
                return false;
            }
        }
        else if (l_finalY < 0)
        {
            if (p_canWrap)
            {
                l_finalY = height - 1;
            }
            else
            {
                return false;
            }
        }
        else if (l_finalY > height - 1)
        {
            if (p_canWrap)
            {
                l_finalY = 0;
            }
            else
            {
                return false;
            }
        }

        return canGo(new GridPoint2(l_finalX, l_finalY), GridPoint2.Zero, p_canWrap, p_checkSafetyLayer);
    }

    public bool canGoLeft(int p_fromX, int p_fromY, bool p_canWrap)
    {
        return canGo(new GridPoint2(p_fromX, p_fromY), Direction.LEFT, p_canWrap);
    }

    public bool canGoRight(int p_fromX, int p_fromY, bool p_canWrap)
    {
        return canGo(new GridPoint2(p_fromX, p_fromY), Direction.RIGHT, p_canWrap);
    }

    public bool canGoUp(int p_fromX, int p_fromY, bool p_canWrap)
    {
        return canGo(new GridPoint2(p_fromX, p_fromY), Direction.UP, p_canWrap);
    }

    public bool canGoDown(int p_fromX, int p_fromY, bool p_canWrap)
    {
        return canGo(new GridPoint2(p_fromX, p_fromY), Direction.DOWN, p_canWrap);
    }

    private ApplicationController m_controller;
    private LevelController m_levelController;
    private Dictionary<string, IGrid<GridPoint2, GO>> m_tiles;
    private EntityPlayer1 m_player1;
    private EntityPlayer2 m_player2;
}

public static class Direction
{
    public static GridPoint2 RIGHT = new GridPoint2(1, 0);
    public static GridPoint2 LEFT = new GridPoint2(-1, 0);
    public static GridPoint2 UP = new GridPoint2(0, 1);
    public static GridPoint2 DOWN = new GridPoint2(0, -1);
}
