﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayMode
{
    PLAYER_1_MODE,
    PLAYER_2_MODE
}
