﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ViewDefinition
{
    LOADING,
    MAIN_MENU,
    GAME_WIN,
    GAME_LOSE,
    GAME_HUD
}
