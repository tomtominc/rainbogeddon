﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateDefinition
{
    LOADING,
    MAIN_MENU,
    GAME_LOADING,
    GAME_UPDATE,
    GAME_LOSE,
    GAME_WIN
}
