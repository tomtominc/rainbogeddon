﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public event Action<int> pressedAction = delegate { };

    public void init(int levelNum)
    {
        m_levelNum = levelNum;

        Button button = GetComponent<Button>();
        Text buttonText = GetComponentInChildren<Text>();

        button.onClick.AddListener(OnPressed);
        buttonText.text = (m_levelNum + 1).ToString();
    }

    public void initDisabled()
    {
        Text buttonText = GetComponentInChildren<Text>();
        buttonText.text = "X";
    }

    private void OnPressed()
    {
        pressedAction(m_levelNum);
    }

    private int m_levelNum;
}
