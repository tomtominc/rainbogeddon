﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameWinController : View
{
    public Button noButton;
    public Button yesButton;

    public event Action noAction = delegate { };
    public event Action yesAction = delegate { };

    public override void open()
    {
        yesButton.onClick.AddListener(onYesButton);
        noButton.onClick.AddListener(onNoButton);
    }

    public override void close()
    {

        yesButton.onClick.RemoveListener(onYesButton);
        noButton.onClick.RemoveListener(onNoButton);
        base.close();
    }

    public void onYesButton()
    {
        yesAction();
    }

    private void onNoButton()
    {
        noAction();
    }


}
