﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    public const float CANVAS_HEIGHT = 284F;

    public virtual void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_audio = m_controller.getController<AudioController>();
        m_playerData = m_controller.playerData;

    }

    public virtual void open()
    {

    }

    public virtual void update()
    {

    }

    public virtual void close()
    {
        Destroy(gameObject);
    }

    protected virtual void popupOpenAnimation(RectTransform p_content, Image p_background, Action onOpen = null)
    {
        //p_background.color = Color.clear;
        //p_background.DOFade(1f, 1f);

        //Vector2 l_target = p_content.anchoredPosition;
        //Vector2 l_source = new Vector2(p_content.anchoredPosition.x,
        //                       (CANVAS_HEIGHT * 0.5f) + (p_content.sizeDelta.y * 0.5f));

        //p_content.anchoredPosition = l_source;
        //p_content.DOAnchorPos(l_target, 1f).SetEase(Ease.OutBack)
        //    .OnComplete(() =>
        //    {
        //        if (onOpen != null)
        //            onOpen();
        //    });
    }

    protected virtual void popupCloseAnimation(RectTransform p_content, Image p_background, Action onClose = null)
    {
        //p_background.DOFade(0f, 1f);
        //Vector2 l_target = new Vector2(p_content.anchoredPosition.x,
        //                       (CANVAS_HEIGHT * 0.5f) + (p_content.sizeDelta.y * 0.5f));

        //p_content.DOAnchorPos(l_target, 1f).SetEase(Ease.InBack)
        //    .OnComplete(() =>
        //    {
        //        if (onClose != null)
        //            onClose();
        //    });
    }

    protected T getController<T>() where T : Controller
    {
        return m_controller.getController<T>();
    }

    protected ApplicationController m_controller;
    protected PlayerData m_playerData;
    protected AudioController m_audio;
}
