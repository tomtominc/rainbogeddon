﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainMenuController : View
{
    public RectTransform m_buttonPrefab;
    public RectTransform m_buttonGridLayout;
    public int m_maxLevelComplete;
    public event Action<PlayMode> playAction;

    public override void open()
    {
        LevelController levelController = m_controller.getController<LevelController>();

        for (int i = 0; i < levelController.getLevelCount(); i++)
        {
            RectTransform buttonRect = Instantiate(m_buttonPrefab);
            buttonRect.SetParent(m_buttonGridLayout, false);

            LevelButton button = buttonRect.GetComponent<LevelButton>();

            if (i < m_maxLevelComplete)
            {
                button.init(i);
                button.pressedAction += Play;
            }
            else
            {
                button.initDisabled();
            }

        }
    }

    public override void close()
    {

        base.close();
    }

    public void Play(int level)
    {
        LevelController levelController = m_controller.getController<LevelController>();
        levelController.testLevel = level;

        playAction(PlayMode.PLAYER_1_MODE);
    }

    private void OnPlay1PlayerModeClicked()
    {
        playAction(PlayMode.PLAYER_1_MODE);
    }

    private void OnPlay2PlayerModeClicked()
    {
        playAction(PlayMode.PLAYER_2_MODE);
    }
}
