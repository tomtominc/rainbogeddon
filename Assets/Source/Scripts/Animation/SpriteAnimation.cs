﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class AnimationListItem
{
    public string animationName;
    public SpriteAnimationData animation;
    public SpriteAnimationAsset file;
}

public class AnimationParamaters
{
    public string animationState;
    public object[] arguments;

    public AnimationParamaters(string animationState, params object[] arguments)
    {
        this.animationState = animationState;
        this.arguments = arguments;
    }
}

public interface IAnimationListener
{
    void StartAnimation(SpriteAnimation animation, SpriteAnimationData data);

    void FinishAnimation(SpriteAnimation animation, SpriteAnimationData data);

    void KeyFrameUpdate(SpriteAnimation animation, SpriteAnimationData data, int frame, string id);
}

[DisallowMultipleComponent, RequireComponent(typeof(SpriteRenderer))]
public class SpriteAnimation : MonoBehaviour
{
    // Events
    public event Action<SpriteAnimation, SpriteAnimationData> OnStartAnimation;
    public event Action<SpriteAnimation, SpriteAnimationData> OnAnimationStartLoop;
    public event Action<SpriteAnimation, SpriteAnimationData> OnFinishAnimation;
    public event Action<SpriteAnimation, SpriteAnimationData> OnStopAnimation;
    public event Action<SpriteAnimation, SpriteAnimationData> OnFinishOrStopAnimation;
    public event Action<SpriteAnimation, SpriteAnimationData> OnFinishedTransitionQueue;
    public event Action<SpriteAnimation, SpriteAnimationData, int, string> OnKeyFrameEvent;

    protected Action<SpriteAnimation, SpriteAnimationData> cacheTransitionAction;
    protected Action<SpriteAnimation, SpriteAnimationData, int, string> cacheKeyFrameAction;

    public int KeyFrameInvokeCount
    {
        get
        {
            if (OnKeyFrameEvent == null)
                return 0;

            return OnKeyFrameEvent.GetInvocationList().Length;
        }
    }

    public int FinishedInvokeCount
    {
        get
        {
            if (OnFinishAnimation == null)
            {
                return 0;
            }

            return OnFinishAnimation.GetInvocationList().Length;
        }
    }

    public int CurrentFrame
    {
        get { return currentIdx; }
        set { currentIdx = value; }
    }

    public int CurrentFrameCount
    {
        get
        {
            SpriteAnimationData data;

            if (string.IsNullOrEmpty(currentAnimationName))
                return 0;

            if (animationsByName.TryGetValue(currentAnimationName, out data))
            {
                return data.frameDatas.Count;
            }

            return 0;
        }
    }

    public int animationsCount { get { return list.Count; } }

    [SerializeField]
    SpriteRenderer spriteRenderer;
    public List<AnimationListItem> list = new List<AnimationListItem>();

    public List<SpriteAnimationAsset> assets = new List<SpriteAnimationAsset>();

    private List<IAnimationListener> _listeners = new List<IAnimationListener>();

    public SpriteAnimationTimeMode mode;
    protected bool _playing = false;

    [SerializeField]
    int animIdx = -1;

    protected SpriteAnimationData currentAnim { get { return (animIdx >= 0 && animIdx < list.Count) ? list[animIdx].animation : null; } }

    public string currentAnimationName { get { return currentAnim != null ? currentAnim.name : string.Empty; } }

    public int currentAnimationIdx { get { return animIdx; } }

    protected int currentIdx = 0;

    [Range(0.001f, 10f)]
    public float speedRatio = 1f;

    public int minPlayFrom = 0;
    public int maxPlayFrom = 0;

    //[Range("minPlayFrom", "maxPlayFrom")]
    public int playFrom = 0;

    public Dictionary<string, SpriteAnimationData> animationsByName = new Dictionary<string, SpriteAnimationData>();
    public bool autoStart = false;

    void OnEnable()
    {
        UpdateAnimations();
    }

    void Start()
    {
        if (autoStart && currentIdx >= 0)
        {
            Play();
        }
    }

    public void Register(IAnimationListener listener)
    {
        if (_listeners.Contains(listener))
            return;

        //Debug.LogFormat("Adding Listener: {0} Time: {1}", listener.GetHashCode(), Time.time);
        _listeners.Add(listener);
    }

    public void Deregister(IAnimationListener listener)
    {
        _listeners.Remove(listener);
    }

    public void RemoveAllListeners()
    {
        _listeners.Clear();
        _listeners = new List<IAnimationListener>();
    }

    public void SetCurrentAnimation(int idx)
    {
        Stop();
        animIdx = idx;
        UpdateAnimations();
        minPlayFrom = 0;
        maxPlayFrom = currentAnim != null ? currentAnim.frameDatas.Count - 1 : 0;
        //playFrom = 0;
        currentIdx = 0;

        if (Application.isPlaying && autoStart)
        {
            Play();
        }
    }

    public void PlayWithOnKeyFrameEvent(string animation, Action<SpriteAnimation, SpriteAnimationData, int, string> action)
    {
        Play(animation);

        cacheKeyFrameAction = (anim, data, frame, id) =>
        {
            if (frame == data.frameDatas.Count - 1)
                OnKeyFrameEvent -= cacheKeyFrameAction;

            action.Invoke(anim, data, frame, id);
        };

        OnKeyFrameEvent += cacheKeyFrameAction;
    }

    public void PlayWithCallBack(object animation, Action<SpriteAnimation, SpriteAnimationData> action)
    {
        Play(animation);

        if (FinishedInvokeCount > 0)
            OnFinishAnimation -= cacheTransitionAction;

        cacheTransitionAction = (v, z) =>
        {
            OnFinishAnimation -= cacheTransitionAction;

            action.Invoke(v, z);
        };

        OnFinishAnimation += cacheTransitionAction;
    }

    public void PlayTransition(Queue<AnimationParamaters> animations)
    {
        if (animations.Count <= 0)
        {
            return;
        }

        if (FinishedInvokeCount > 0)
        {
            OnFinishAnimation -= cacheTransitionAction;
        }

        cacheTransitionAction = (x, y) =>
        {
            OnFinishAnimation -= cacheTransitionAction;

            var anim = animations.Dequeue();

            PlayFormat(anim.animationState, anim.arguments);

            if (animations.Count > 0)
            {
                PlayTransition(animations);
            }
            else
            {
                Action<SpriteAnimation, SpriteAnimationData> handler = null;

                handler = (v, z) =>
                {
                    OnFinishAnimation -= handler;

                    if (OnFinishedTransitionQueue != null)
                    {
                        OnFinishedTransitionQueue(x, y);
                    }
                };

                OnFinishAnimation += handler;
            }
        };


        OnFinishAnimation += cacheTransitionAction;
    }

    public float GetCurrentAnimationLength()
    {
        SpriteAnimationData data;

        if (animationsByName.TryGetValue(currentAnimationName, out data))
        {
            return currentAnim.frameDatas[0].time * data.frameDatas.Count;
        }

        return 0f;
    }

    public void SetCurrentAnimation(string aName)
    {
        Stop();
        UpdateAnimations();
        SpriteAnimationData adata = GetAnimationData(aName);
        if (adata != null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].animation == adata)
                {
                    animIdx = i;
                    break;
                }
            }
        }
        else
        {
            animIdx = -1;
        }
        minPlayFrom = 0;
        maxPlayFrom = currentAnim != null ? currentAnim.frameDatas.Count - 1 : 0;
        //playFrom = 0;
        currentIdx = 0;

        if (Application.isPlaying && autoStart)
        {
            Play();
        }
    }

    public void UpdateAnimations()
    {
        animationsByName.Clear();
        list.Clear();
        foreach (SpriteAnimationAsset asset in assets)
        {
            if (asset != null && asset.animations != null && asset.animations.Count > 0)
            {
                foreach (SpriteAnimationData data in asset.animations)
                {
                    animationsByName[data.name] = data;
                    AnimationListItem item = new AnimationListItem();
                    item.animation = data;
                    item.animationName = data.name;
                    item.file = asset;
                    list.Add(item);
                }
            }
        }
    }

    void Reset()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        UpdateAnimations();
    }

    public void PlayFormat(string animationState, params object[] args)
    {
        string l_animation = string.Format(animationState, args);

        Play(l_animation);
    }

    public bool Play(object anim)
    {
        return Play(anim.ToString());
    }

    public bool Play(string animName)
    {
        SetCurrentAnimation(animName);

        if (currentAnim != null && currentAnim.Valid())
        {
            PlayCurrentAnim();
            return true;
        }
        return false;
    }

    public bool Play()
    {
        if (currentAnim != null && currentAnim.Valid())
        {
            PlayCurrentAnim();
            return true;
        }
        return false;
    }

    public bool Play(int idx)
    {
        SetCurrentAnimation(idx);
        if (currentAnim != null && currentAnim.Valid())
        {
            PlayCurrentAnim();
            return true;
        }
        return false;
    }

    public void Stop()
    {
        if (!_playing)
            return;
        if (Application.isPlaying && OnStopAnimation != null)
            OnStopAnimation(this, currentAnim);
        _playing = false;
        StopCoroutine("playAnimation");
    }

    protected SpriteAnimationData GetAnimationData(string animName)
    {
        if (animationsByName.ContainsKey(animName) && animationsByName[animName].Valid())
        {
            return animationsByName[animName];
        }
        return null;
    }

    protected SpriteAnimationData GetAnimationData(int idx)
    {
        if (list.Count > idx && list[idx].animation.Valid())
        {
            return list[idx].animation;
        }
        return null;
    }

    protected void PlayCurrentAnim()
    {
        Stop();
        if (OnStartAnimation != null)
            OnStartAnimation(this, currentAnim);

        foreach (var listener in _listeners)
        {
            listener.StartAnimation(this, currentAnim);
        }

        //if (Application.isPlaying)
        StartCoroutine("playAnimation");
    }

    protected IEnumerator playAnimation()
    {
        _playing = true;
        currentIdx = playFrom - 1;
        float cfTime = 0f;
        bool loop = false;
        bool isFirstLoop = true;
        float cTime = GetTime();

        do
        {
            if (!isFirstLoop && OnAnimationStartLoop != null)
                OnAnimationStartLoop(this, currentAnim);
            do
            {
                cfTime = 0;
                do
                {
                    cTime += cfTime;
                    currentIdx++;
                    cfTime = GetCurrentFrameTime();
                }
                while (GetTime() > (cTime + cfTime) && !isEndFrame());

                SetCurrentFrame();

                foreach (var listener in _listeners)
                {
                    listener.KeyFrameUpdate(this, currentAnim, currentIdx, currentAnim.frameDatas[currentIdx].eventName);
                }

                if (!string.IsNullOrEmpty(currentAnim.frameDatas[currentIdx].eventName))
                {
                    SendMessage(currentAnim.frameDatas[currentIdx].eventName, SendMessageOptions.DontRequireReceiver);
                }

                if (OnKeyFrameEvent != null)
                {
                    OnKeyFrameEvent(this, currentAnim, currentIdx, currentAnim.frameDatas[currentIdx].eventName);
                }

                while (GetTime() < cTime + cfTime)
                {
                    cfTime = GetCurrentFrameTime();
                    yield return new WaitForEndOfFrame();
                }
                cTime += cfTime;
            }
            while (!isEndFrame());

            if (currentAnim.loop == SpriteAnimationLoopMode.LOOPTOSTART)
            {
                loop = true;
                currentIdx = -1;
            }
            else if (currentAnim.loop == SpriteAnimationLoopMode.LOOPTOFRAME)
            {
                loop = true;
                currentIdx = currentAnim.frameToLoop - 1;
            }
            if (OnFinishAnimation != null)
                OnFinishAnimation(this, currentAnim);
            if (OnFinishOrStopAnimation != null)
                OnFinishOrStopAnimation(this, currentAnim);

            foreach (var listener in _listeners)
            {
                listener.FinishAnimation(this, currentAnim);
            }

            isFirstLoop = false;
        }
        while (loop);
        yield return new WaitForEndOfFrame();
    }

    protected float GetTime()
    {
        if (mode == SpriteAnimationTimeMode.NORMAL)
            return Time.time;
        else if (mode == SpriteAnimationTimeMode.TIMESCALEINDEPENDENT)
            return Time.realtimeSinceStartup;
        return 0;
    }

    protected void SetCurrentFrame()
    {
        spriteRenderer.sprite = currentAnim.frameDatas[currentIdx].sprite;
    }

    protected float GetCurrentFrameTime()
    {
        if (currentIdx >= currentAnim.frameDatas.Count)
            return 0f;

        return (currentAnim.frameDatas[currentIdx].time) * (1 / speedRatio);
    }

    protected bool isEndFrame()
    {
        return currentIdx == currentAnim.frameDatas.Count - 1;
    }

    void Destroy()
    {
        OnStopAnimation = null;
        OnFinishOrStopAnimation = null;
        OnFinishAnimation = null;
    }
}
