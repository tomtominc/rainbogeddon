﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;

public static class Extensions
{
    public static bool isZero(this float f)
    {
        return System.Math.Abs(f) < float.Epsilon;
    }

    public static bool isEqual(this float l_f, float l_other)
    {
        return System.Math.Abs(l_f - l_other) < float.Epsilon;
    }

    public static void scaleBy(this Vector2 v, float scale)
    {
        v.Scale(scale * Vector2.one);
    }

    public static Color ToColor(this string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }

    public static uint ColorToUint(this Color32 color)
    {
        return (uint)((color.a << 24) | (color.r << 16) |
                   (color.g << 8) | (color.b << 0));
    }

    public static Color32 uintToColor(this uint color)
    {
        byte a = (byte)(color >> 24);
        byte r = (byte)(color >> 16);
        byte g = (byte)(color >> 8);
        byte b = (byte)(color >> 0);
        return new Color32(r, g, b, a);
    }

    public static int signum(this float p_f)
    {
        if (p_f < 0)
        {
            return -1;
        }
        if (p_f > 0)
        {
            return 1;
        }

        return 0;
    }
}
