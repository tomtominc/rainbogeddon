﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ApplicationController : MonoBehaviour
{
    public const float PIXEL_SIZE = 1F / 25F;
    public const string PLAYER_DATA_KEY = "PLAYER_DATA_V0.0.1";

    public PlayerData playerData
    {
        get { return m_playerData; }
    }

    private void Awake()
    {
        m_controllers = FindObjectsOfType<Controller>().ToList();

        for (int i = 0; i < m_controllers.Count; i++)
        {
            m_controllers[i].initialize(this);
        }
    }

    private void Start()
    {
        for (int i = 0; i < m_controllers.Count; i++)
        {
            m_controllers[i].onApplicationEnter();
        }
    }

    public T getController<T>() where T : Controller
    {
        return m_controllers.Find(x => x is T) as T;
    }

    public void onMatchEnter()
    {
        for (int i = 0; i < m_controllers.Count; i++)
        {
            m_controllers[i].onMatchEnter();
        }
    }

    public void onMatchExit()
    {
        for (int i = 0; i < m_controllers.Count; i++)
        {
            m_controllers[i].onMatchExit();
        }
    }

    private void load()
    {
        if (PlayerPrefs.HasKey(PLAYER_DATA_KEY))
        {
            string l_playerFile = PlayerPrefs.GetString(PLAYER_DATA_KEY);
            m_playerData = JsonUtility.FromJson<PlayerData>(l_playerFile);
        }
        else
        {
            m_playerData = new PlayerData();
        }

        save();
    }

    private void save()
    {
        string l_playerFile = JsonUtility.ToJson(m_playerData);
        PlayerPrefs.SetString(PLAYER_DATA_KEY, l_playerFile);
        PlayerPrefs.Save();
    }

    private List<Controller> m_controllers;
    private PlayerData m_playerData;
}
