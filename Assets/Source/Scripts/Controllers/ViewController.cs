﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewController : Controller
{
    private Dictionary<ViewDefinition, GameObject> m_viewPrefabs;
    private Dictionary<ViewDefinition, View> m_views;

    private CanvasScaler m_scaler;

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        // m_scaler = GetComponent<CanvasScaler>();
        //float l_pixelOffset = (float)Screen.width / 320f;
        //m_scaler.scaleFactor = Mathf.RoundToInt(l_pixelOffset);

        m_views = new Dictionary<ViewDefinition, View>();
        m_viewPrefabs = new Dictionary<ViewDefinition, GameObject>();

        m_viewPrefabs.Add(ViewDefinition.LOADING, Resources.Load<GameObject>("View/LoadingScreen"));
        m_viewPrefabs.Add(ViewDefinition.MAIN_MENU, Resources.Load<GameObject>("View/MainMenu"));
        m_viewPrefabs.Add(ViewDefinition.GAME_LOSE, Resources.Load<GameObject>("View/GameLoseScreen"));
        m_viewPrefabs.Add(ViewDefinition.GAME_WIN, Resources.Load<GameObject>("View/GameWinScreen"));
    }

    public View open(ViewDefinition p_definition)
    {
        View l_view = null;

        if (!m_views.ContainsKey(p_definition))
        {
            GameObject l_viewGo = Instantiate(m_viewPrefabs[p_definition]);
            l_viewGo.transform.SetParent(transform, false);
            l_view = l_viewGo.GetComponent<View>();
            l_view.initialize(m_controller);
            l_view.open();
            m_views.Add(p_definition, l_view);
        }
        else
        {
            l_view = m_views[p_definition];
        }

        return l_view;
    }

    public void close(ViewDefinition p_definition)
    {
        if (m_views.ContainsKey(p_definition))
        {
            View l_view = m_views[p_definition];
            l_view.close();
            m_views.Remove(p_definition);
        }
    }

    public View get(ViewDefinition p_definition)
    {
        View l_view = null;

        if (m_views.ContainsKey(p_definition))
        {
            l_view = m_views[p_definition];
        }

        return l_view;
    }
}
