﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public virtual void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_stateController = getController<StateController>();
        m_sound = getController<SoundController>();
        m_levelController = getController<LevelController>();
    }

    public virtual void onApplicationEnter()
    {

    }


    public virtual void onApplicationExit()
    {

    }

    public virtual void onMatchEnter()
    {

    }

    public virtual void onMatchExit()
    {

    }

    protected T getController<T>() where T : Controller
    {
        return m_controller.getController<T>();
    }

    protected ApplicationController m_controller;
    protected StateController m_stateController;
    protected SoundController m_sound;
    protected LevelController m_levelController;

}
