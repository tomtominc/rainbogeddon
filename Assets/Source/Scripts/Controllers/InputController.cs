﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDirection
{
    public const int ZERO = 1 << 0;
    public const int TAP = 1 << 1;
    public const int UP = 1 << 2;
    public const int DOWN = 1 << 3;
    public const int LEFT = 1 << 4;
    public const int RIGHT = 1 << 5;

    public static Vector2 getDirection(int p_swipeDirection)
    {
        if (p_swipeDirection.Equals(UP))
            return Vector2.up;
        if (p_swipeDirection.Equals(DOWN))
            return Vector2.down;
        if (p_swipeDirection.Equals(LEFT))
            return Vector2.left;
        if (p_swipeDirection.Equals(RIGHT))
            return Vector2.right;

        return Vector2.zero;
    }
}

public class InputController : MonoBehaviour
{
    private Vector2 fingerStartPos = Vector2.zero;

    private float minSwipeDist = 50.0f;

    private int m_lastDirection;
    private bool m_calculateSwipe;

    private bool isPlayer1;

    public void init(EntityPlayer player)
    {
        isPlayer1 = player is EntityPlayer1;
    }

    public int getSwipeDirection()
    {
        int l_dir = m_lastDirection;

        if (Input.GetMouseButtonDown(0))
        {
            fingerStartPos = Input.mousePosition;
        }

        if (hasValidStartPosition() && Input.GetMouseButton(0))
        {
            Vector2 l_fingerPos = Input.mousePosition;
            float l_distance = (l_fingerPos - fingerStartPos).magnitude;

            if (l_distance > minSwipeDist)
            {
                Vector2 l_direction = l_fingerPos - fingerStartPos;
                Vector2 l_swipeType = Vector2.zero;

                if (Mathf.Abs(l_direction.x) > Mathf.Abs(l_direction.y))
                {
                    l_swipeType = Vector2.right * Mathf.Sign(l_direction.x);
                }
                else
                {
                    l_swipeType = Vector2.up * Mathf.Sign(l_direction.y);
                }

                if (l_swipeType.x != 0.0f)
                {
                    l_dir = l_swipeType.x > 0f ? SwipeDirection.RIGHT : SwipeDirection.LEFT;
                }

                if (l_swipeType.y != 0.0f)
                {
                    l_dir = l_swipeType.y > 0f ? SwipeDirection.UP : SwipeDirection.DOWN;
                }
            }
        }

        if (hasValidStartPosition() && Input.GetMouseButtonUp(0))
        {
            Vector2 l_fingerPos = Input.mousePosition;
            float l_distance = (l_fingerPos - fingerStartPos).magnitude;

            if (l_distance < minSwipeDist)
            {
                l_dir = SwipeDirection.TAP;
            }

            m_lastDirection = SwipeDirection.ZERO;
        }

        return l_dir;
    }

    private bool hasValidStartPosition()
    {
        if (Game.playMode == PlayMode.PLAYER_2_MODE)
        {
            if (isPlayer1)
            {
                if (fingerStartPos.y < Screen.height * 0.5f)
                {
                    return true;
                }
            }
            else
            {
                if (fingerStartPos.y >= Screen.height * 0.5f)
                {
                    return true;
                }
            }

            return false;
        }

        return true;
    }
}
