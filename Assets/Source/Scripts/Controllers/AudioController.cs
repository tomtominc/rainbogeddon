﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : Controller
{
    public float MAX_SOUND_DISTANCE = 5F;

    [SerializeField]
    private List<AudioClip> m_audio;
    [SerializeField]
    private AudioSource m_music;
    [SerializeField]
    private AudioSource m_ambient;

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        m_audioTable = new Dictionary<string, AudioClip>();

        for (int i = 0; i < m_audio.Count; i++)
        {
            m_audioTable.Add(m_audio[i].name, m_audio[i]);
        }
    }

    public float playDistanceSound(string p_sound, Vector2 p_ear, Vector2 p_source)
    {
        float l_distance = Vector2.Distance(p_ear, p_source);

        if (l_distance <= MAX_SOUND_DISTANCE)
        {
            float l_volume = 1 - (l_distance / MAX_SOUND_DISTANCE);
            return playSound(p_sound, l_volume);
        }

        return 0;
    }

    public float playSound(string p_sound, float p_volume)
    {
        AudioClip l_clip = m_audioTable[p_sound];
        AudioSource.PlayClipAtPoint(l_clip, listenerPosition(), p_volume);
        return l_clip.length;
    }

    public void playMusic(string p_music)
    {
        AudioClip l_clip = m_audioTable[p_music];
        m_music.clip = l_clip;
        m_music.Play();
    }

    public void playAmbient(string p_ambient)
    {
        AudioClip l_clip = m_audioTable[p_ambient];
        m_ambient.clip = l_clip;
        m_ambient.Play();
    }

    private Vector2 listenerPosition()
    {
        return transform.position;
    }

    private Dictionary<string, AudioClip> m_audioTable;
}
