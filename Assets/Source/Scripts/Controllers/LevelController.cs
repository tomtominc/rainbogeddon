﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System;
using System.Linq;
using UnityEngine.SceneManagement;

public class LevelController : Controller
{
    public int testLevel;
    public Level currentLevel
    {
        get { return m_currentLevel; }
    }

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);
        m_levelAssets = Resources.LoadAll<TextAsset>("Levels/").ToList();
        m_levelAssets = m_levelAssets.OrderBy(x => int.Parse(x.name.Split('l')[2])).ToList();
    }

    public void spawnLevel()
    {
        m_currentLevel = convertFromXML(m_levelAssets[testLevel].text);
        m_currentLevel.initialize(m_controller);
        m_currentLevel.fillGrid();
    }

    public void destroyLevel()
    {

        for (int i = transform.childCount - 1; i > -1; i--)
        {
            GameObject child = transform.GetChild(i).gameObject;

            Destroy(child);
        }
    }

    private Level convertFromXML(string p_xml)
    {
        StringReader strReader = null;
        XmlSerializer serializer = null;
        XmlTextReader xmlReader = null;
        Level level = null;

        try
        {
            strReader = new StringReader(p_xml);
            serializer = new XmlSerializer(typeof(Level));
            xmlReader = new XmlTextReader(strReader);
            level = serializer.Deserialize(xmlReader) as Level;
        }
        catch (Exception exp)
        {
            Debug.LogErrorFormat("Exception thrown: {0} could not deserialize level {1}", exp, p_xml);
            //Handle Exception Code
        }
        finally
        {
            if (xmlReader != null)
            {
                xmlReader.Close();
            }
            if (strReader != null)
            {
                strReader.Close();
            }
        }

        return level;
    }

    public int getLevelCount()
    {
        return m_levelAssets.Count;
    }

    //public Sprite getBlockAtIndex(int p_index)
    //{
    //    return m_softBlocks[p_index];
    //}


    private List<TextAsset> m_levelAssets;
    //private List<Level> m_levels;
    private Level m_currentLevel;
    //private List<Sprite> m_softBlocks;

    private GameObject m_levelContainer;
}
