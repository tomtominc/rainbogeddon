﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneController : Controller
{
    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        m_scenes = new Dictionary<SceneDefinition, string>();
        m_scenes.Add(SceneDefinition.GAME, "Game");
    }

    public void loadScene(SceneDefinition p_definition)
    {
        StartCoroutine(loadSceneRoutine(p_definition));
    }

    private IEnumerator loadSceneRoutine(SceneDefinition p_definition)
    {
        if (m_scene.IsValid())
        {
            yield return SceneManager.UnloadSceneAsync(m_scene);
        }

        string l_scene = m_scenes[p_definition];

        yield return SceneManager.LoadSceneAsync(l_scene, LoadSceneMode.Additive);

        m_scene = SceneManager.GetSceneByName(l_scene);
        SceneManager.SetActiveScene(m_scene);
        sceneLoaded();

    }

    private Dictionary<SceneDefinition, string> m_scenes;
    private Scene m_scene;
    public event Action sceneLoaded = delegate { };
}
