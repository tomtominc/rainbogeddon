﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : Controller
{
    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);
        instance = this;
    }

    public void playSound(string p_sound)
    {
        //Debug.LogWarningFormat("trying to play sound {0} but the sound controller has not been setup!", p_sound);
    }

    public void playMusic(string p_music)
    {
        //Debug.LogWarningFormat("trying to play music {0} but the sound controller has not been setup!", p_music);
    }

    public void stopAllSounds()
    {
        //Debug.LogWarningFormat("Trying to stop all sounds but the sound controller has not been setup!");
    }

    public void stopMusic()
    {
        //Debug.LogWarningFormat("Trying to stop music but the sound controller has not been setup!");
    }

    public void garbageCollectChannels()
    {

    }

    public static SoundController instance;

}
