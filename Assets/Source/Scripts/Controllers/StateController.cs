﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : Controller
{
    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        m_states = new Dictionary<StateDefinition, State>();

        m_states.Add(StateDefinition.LOADING, new Loading(m_controller));
        m_states.Add(StateDefinition.MAIN_MENU, new MainMenu(m_controller));
        m_states.Add(StateDefinition.GAME_LOADING, new GameLoading(m_controller));
        m_states.Add(StateDefinition.GAME_UPDATE, new GameUpdate(m_controller));
        m_states.Add(StateDefinition.GAME_WIN, new GameWin(m_controller));
        m_states.Add(StateDefinition.GAME_LOSE, new GameLose(m_controller));
    }

    public override void onApplicationEnter()
    {
        m_current = StateDefinition.LOADING;
        m_states[m_current].enter();
    }

    public void Update()
    {
        m_states[m_current].update();
    }

    public void changeState(StateDefinition p_definition)
    {
        m_states[m_current].exit();
        m_current = p_definition;
        m_states[m_current].enter();
    }

    public bool isState(StateDefinition p_definition)
    {
        return m_current == p_definition;
    }

    public StateDefinition currentState
    {
        get { return m_current; }
    }

    private StateDefinition m_current;
    private Dictionary<StateDefinition, State> m_states;
}
