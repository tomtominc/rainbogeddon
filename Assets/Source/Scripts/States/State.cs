using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    public State(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_stateController = getController<StateController>();
        m_sceneController = getController<SceneController>();
        m_viewController = getController<ViewController>();
        m_levelController = getController<LevelController>();
        m_game = getController<Game>();
    }

    public virtual void enter()
    {

    }

    public virtual void update()
    {

    }

    public virtual void exit()
    {

    }

    protected T getController<T>() where T : Controller
    {
        return m_controller.getController<T>();
    }

    protected ApplicationController m_controller;
    protected StateController m_stateController;
    protected SceneController m_sceneController;
    protected ViewController m_viewController;
    protected LevelController m_levelController;
    protected Game m_game;
}
