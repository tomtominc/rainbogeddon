﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUpdate : State
{
    public GameUpdate(ApplicationController p_controller)
        : base(p_controller)
    {

    }

    public override void enter()
    {
        m_game.init();
    }


}
