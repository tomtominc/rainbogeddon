﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLose : State
{
    public GameLose(ApplicationController p_controller)
        : base(p_controller)
    {

    }

    public override void enter()
    {
        m_gameLoseMenu = m_viewController.open(ViewDefinition.GAME_LOSE) as GameLoseController;
        m_gameLoseMenu.yesAction += onPlayButton;
        m_gameLoseMenu.noAction += onQuitButton;
    }

    private void onPlayButton()
    {
        m_gameLoseMenu.yesAction -= onPlayButton;
        m_gameLoseMenu.noAction -= onQuitButton;

        m_viewController.close(ViewDefinition.GAME_LOSE);

        m_stateController.changeState(StateDefinition.GAME_LOADING);
    }

    private void onQuitButton()
    {
        m_gameLoseMenu.yesAction -= onPlayButton;
        m_gameLoseMenu.noAction -= onQuitButton;

        m_viewController.close(ViewDefinition.GAME_LOSE);

        m_stateController.changeState(StateDefinition.LOADING);
    }

    private GameLoseController m_gameLoseMenu;
}
