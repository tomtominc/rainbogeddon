﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading : State
{
    public const float FAKE_LOAD_TIME = 2F;

    public Loading(ApplicationController p_controller)
            : base(p_controller)
    {
    }

    public override void enter()
    {
        m_loadingScreen = m_viewController.open(ViewDefinition.LOADING) as LoadingController;
        m_currentTime = 0f;
    }

    public override void update()
    {
        m_currentTime += Time.deltaTime;

        if (m_currentTime >= Loading.FAKE_LOAD_TIME)
        {
            m_viewController.close(ViewDefinition.LOADING);
            m_stateController.changeState(StateDefinition.MAIN_MENU);
        }
    }

    private float m_currentTime;
    private LoadingController m_loadingScreen;
}

