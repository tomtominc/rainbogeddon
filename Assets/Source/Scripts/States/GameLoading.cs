﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoading : State
{
    public GameLoading(ApplicationController p_controller)
        : base(p_controller)
    {
    }

    public override void enter()
    {
        m_game.unloadRefs();
        m_levelController.destroyLevel();
        m_loadingScreen = m_viewController.open(ViewDefinition.LOADING) as LoadingController;
        m_currentTime = 0f;
    }

    public override void update()
    {
        m_currentTime += Time.deltaTime;

        if (m_currentTime >= Loading.FAKE_LOAD_TIME)
        {
            m_viewController.close(ViewDefinition.LOADING);
            m_stateController.changeState(StateDefinition.GAME_UPDATE);
        }
    }

    private float m_currentTime;
    private LoadingController m_loadingScreen;
}
