﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : State
{

    public MainMenu(ApplicationController p_controller)
        : base(p_controller)
    {
    }

    public override void enter()
    {
        m_mainMenu = m_viewController.open(ViewDefinition.MAIN_MENU) as MainMenuController;
        m_mainMenu.playAction += onPlayButton;
    }

    private void onPlayButton(PlayMode p_playMode)
    {
        Game.playMode = p_playMode;
        m_mainMenu.playAction -= onPlayButton;
        m_viewController.close(ViewDefinition.MAIN_MENU);
        m_stateController.changeState(StateDefinition.GAME_LOADING);
    }

    private MainMenuController m_mainMenu;
}
