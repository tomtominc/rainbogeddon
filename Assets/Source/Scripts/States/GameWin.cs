﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWin : State
{

    public GameWin(ApplicationController p_controller)
            : base(p_controller)
    {
    }


    public override void enter()
    {
        m_gameWinMenu = m_viewController.open(ViewDefinition.GAME_WIN) as GameWinController;
        m_gameWinMenu.yesAction += onPlayButton;
        m_gameWinMenu.noAction += onQuitButton;
    }

    private void onPlayButton()
    {
        m_gameWinMenu.yesAction -= onPlayButton;
        m_gameWinMenu.noAction -= onQuitButton;

        m_viewController.close(ViewDefinition.GAME_WIN);

        m_stateController.changeState(StateDefinition.GAME_LOADING);
    }

    private void onQuitButton()
    {
        m_gameWinMenu.yesAction -= onPlayButton;
        m_gameWinMenu.noAction -= onQuitButton;

        m_viewController.close(ViewDefinition.GAME_WIN);

        m_stateController.changeState(StateDefinition.LOADING);
    }

    private GameWinController m_gameWinMenu;
}
