﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafetyBubbleHolder : WeaponHolder
{
    public const float FIRE_DELAY = 2.5f;
    private float m_fireStartTime;
    private int m_maxBubbles;
    private List<EntitySafetyBubble> m_bubbleList;

    // private List<Entitysa> m_bubbleList:Vector.<EntitySafetyBubble>;
    public SafetyBubbleHolder(EntityCharacter p_parent)
        : base(p_parent)
    {
        m_fireStartTime = 0;
        m_bubbleList = new List<EntitySafetyBubble>();
        m_maxBubbles = 1;
    }

    public override bool tryFire()
    {
        m_parent.m_fire = false;

        refreshList();

        if (m_bubbleList.Count < m_maxBubbles)
        {
            if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
            {
                m_fireStartTime = Game.g.timer;
                SoundController.instance.playSound("CreateBubble");

                EntitySafetyBubble bubble = Game.g.createObject<EntitySafetyBubble>(Level.PARTICLE_LAYER, "EntitySafetyBubble", m_parent.tx, m_parent.ty);
                m_bubbleList.Add(bubble);

                return true;
            }// end if              

        }
        else if ((m_bubbleList.Count < m_maxBubbles + 1) && m_bubbleList[0].m_canDisable)
        {
            m_bubbleList[0].fade();

            if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
            {
                m_fireStartTime = Game.g.timer;
                SoundController.instance.playSound("CreateBubble");

                EntitySafetyBubble bubble = Game.g.createObject<EntitySafetyBubble>(Level.PARTICLE_LAYER, "EntitySafetyBubble", m_parent.tx, m_parent.ty);
                m_bubbleList.Add(bubble);
                return true;
            }
        }

        return false;
    }

    public void refreshList()
    {

        for (int i = m_bubbleList.Count - 1; i > -1; i--)
        {
            if (m_bubbleList[i] == null || m_bubbleList[i].m_terminated)
            {
                m_bubbleList.RemoveAt(i);
            }
        }
    }

    public override void terminate()
    {
        for (int i = 0; i < m_bubbleList.Count; i++)
        {
            m_bubbleList[i].fade();
        }
    }

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;
            m_maxBubbles++;
            return true;
        }

        return false;
    }
}
