﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GO : MonoBehaviour
{
    protected ApplicationController m_controller;
    protected SoundController m_sound;
    protected Game m_game;
    public string m_id;
    public Vector2 m_vel;
    public int m_tx; // target x
    public int m_ty; // target y
    public Rect m_bounds;
    public bool m_terminated;
    public bool m_drawWrapped;
    public bool m_draw;
    public int m_drawOrder;
    protected bool m_flicker;
    protected bool m_drawFast;
    protected SpriteRenderer m_renderer;
    protected SpriteRenderer[] m_renderers;
    protected Animator m_animator; // frameclip
    protected float m_time;
    protected Level m_tileMap;
    protected int m_drawOffset;

    public string id
    {
        get { return m_id; }
        set { m_id = value; }
    }

    public Vector2 position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public float x
    {
        get { return position.x; }
        set { position = new Vector2(value, position.y); }
    }

    public float y
    {
        get { return position.y; }
        set { position = new Vector2(position.x, value); }
    }

    public float scaleX
    {
        get { return m_renderer.transform.localScale.x; }
        set { m_renderer.transform.localScale = new Vector3(value, m_renderer.transform.localScale.y, 1); }
    }

    public float scaleY
    {
        get { return m_renderer.transform.localScale.y; }
        set { m_renderer.transform.localScale = new Vector3(m_renderer.transform.localScale.x, value, 1); }
    }

    public bool terminated
    {
        get { return m_terminated; }
        set { m_terminated = value; }
    }

    public int tx
    {
        get { return m_tx; }
        set { m_tx = value; }
    }

    public int ty
    {
        get { return m_ty; }
        set { m_ty = value; }
    }

    public float rotation
    {
        get { return transform.eulerAngles.z; }
        set { transform.eulerAngles = new Vector3(0f, 0f, value); }
    }

    public virtual void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        m_controller = p_controller;
        m_game = m_controller.getController<Game>();
        m_sound = m_controller.getController<SoundController>();
        m_tileMap = m_controller.getController<LevelController>().currentLevel;

        m_vel = Vector2.zero;
        x = Game.TILE_WIDTH * p_tx;
        y = Game.TILE_HEIGHT * p_ty;
        m_tx = p_tx;
        m_ty = p_ty;
        m_bounds = getBounds(this);
        m_drawWrapped = true;
        m_draw = true;
        m_drawOrder = 0;
        m_flicker = false;
        m_renderers = GetComponentsInChildren<SpriteRenderer>();

        if (m_renderers.Length > 0)
        {
            m_renderer = m_renderers[0];
            m_drawOffset = m_renderer.sortingOrder;
        }


        m_animator = GetComponentInChildren<Animator>();
        m_terminated = false;
    }

    public virtual void onGameEnd()
    {

    }

    public virtual void onGamePaused()
    {

    }

    public virtual void onGameUnpaused()
    {

    }

    public virtual void tick()
    {
        doInput();
        m_time = Time.realtimeSinceStartup;
        doPhysics();
        m_time = Time.realtimeSinceStartup - m_time;
    }

    public virtual void updatePosFromTile()
    {
        //TODO: update this to be better

        x = m_tx;// (Game.TILE_WIDTH * m_tx);
        y = m_ty;// (Game.TILE_HEIGHT * m_ty);
    }

    public virtual void updateTilePos()
    {

    }

    public virtual void doPhysics()
    {

    }

    public virtual void doInput()
    {

    }

    // game draws all the objects
    public virtual void draw()
    {
        if (terminated)
        {
            if (m_renderer != null)
                m_renderer.enabled = false;

            return;
        }

        if (m_renderer == null)
            return;

        if (!m_draw || (m_flicker && (Time.frameCount % 4 > 0)))
        {
            m_renderer.enabled = false;
        }
        else
        {
            m_renderer.sortingOrder = m_drawOffset + m_drawOrder;
            m_renderer.enabled = true;
        }
    }

    public virtual void gotoAndPlay(string p_animation)
    {
        gotoAndStop(p_animation);
    }

    public virtual void gotoAndStop(string p_animation)
    {
        if (m_animator != null && m_animator.runtimeAnimatorController != null)
        {
            m_animator.Play(p_animation);
        }
    }

    public virtual void play()
    {
        m_animator.speed = 1;
    }

    public virtual void stop()
    {
        //m_animator.speed = 0;
    }

    public virtual void setXMLProperty(NitromeXMLParam p_param)
    {

    }

    protected virtual Rect getBounds(GO p_go)
    {
        return new Rect(p_go.x - (Game.TILE_WIDTH * 0.5f), p_go.y - (Game.TILE_HEIGHT * 0.5f), Game.TILE_WIDTH, Game.TILE_HEIGHT);
    }
}
