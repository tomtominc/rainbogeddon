﻿using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids2;
using UnityEngine;

public class EntityEnemy : EntityCharacter
{

    public const int STATE_FOLLOWING = 0;
    public const int STATE_PATROLLING_CORRIDORS = 1;

    public const int STATE_SMART_FOLLOWING_NORMAL = 0;
    public const int STATE_SMART_FOLLOWING_COUNTDOWN = 1;

    public const float GOING_STRAIGHT_MIN_DURATION = 1.0f;// 10000;
    public const float GOING_STRAIGHT_MAX_DURATION = 3.0f;// 20000;
    public const float SMART_FOLLOW_DURATION = 1; // 15000;
    public const int MIN_TURN_TIMES = 2;
    public const int MAX_TURN_TIMES = 5;
    public const int CW = 0;
    public const int CCW = 1;
    public const int HIT_POINTS = 30;
    public const float SPEED = 2.25f; //1.5
    public const float MIN_CHECK_DIR_TIMEOUT = 0.025f;
    public const float VEL_CHANGE_TIMEOUT = 0.15f;
    public const int MAX_PATH_LOOK_AHEAD_TILES = 5;
    public const float SEPARATING_FROM_ENEMY_MIN_DURATION = 2.0f;
    public const float SEPARATING_FROM_ENEMY_MAX_DURATION = 4.0f;
    public const float REACTING_TO_HIT_DURATION = 0.25f;

    public const int ANIM_WALK_LEFT = 0;
    public const int ANIM_WALK_RIGHT = 1;
    public const int ANIM_WALK_UP = 2;
    public const int ANIM_WALK_DOWN = 3;

    public GameObject m_respawnEffectType;
    public GameObject deathExlosionPrefab;

    protected PathMap m_player1PathMap;
    protected PathMap m_player2PathMap;
    protected string[] m_dirTable;
    protected float m_velChangeStartTime;
    public int m_smartFollowingPlayerNumber;
    public bool m_smartFollowing;
    protected int m_smartFollowingState;
    public float m_smartFollowCountdownStartTime;
    protected Vector2 m_prevRandVel;
    protected int m_qCorridorDir;
    protected int m_numTurnTimes;
    protected int m_maxTurnTimes;
    protected bool m_goingStraightTryingToTurn;
    protected float m_goingStraightStartTime;
    protected float m_goingStraightDuration;
    protected bool m_waitingForCornerStop;

    protected Vector2 m_prevVel;
    protected Vector2 m_prevNonZeroVel;
    protected int m_damage;
    protected float m_checkDirStartTime;
    protected int m_currAnim;
    protected float m_checkDirTimeout;
    protected float m_checkDirMaxTimeout;
    protected float m_checkDirSmartFollowingStartTime;
    protected float m_checkDirSmartFollowTimeout;
    protected float m_checkDirSmartFollowMaxTimeout;
    protected int m_maxDistFactor;
    protected int m_backForthTimes;
    protected int m_targetTriggerRange;
    protected bool m_hasOnTargetRangeTrigger;
    protected bool m_targetInRange;
    protected bool m_hasLifetime;
    protected float m_lifetime;
    protected float m_lifeStartTime;
    protected float m_backForthDecPerc;
    protected bool m_evadesTarget;
    protected bool m_followsTarget;
    protected Alerted m_alerted;
    protected bool m_isAlerted;
    protected GameObject m_alertEffect;
    protected bool m_hasDecreasingTimeout;
    protected bool m_hasBackAndForthDecPerc;
    public bool m_canSmartFollow;
    protected bool m_checksLineOfSight;
    protected bool m_player1OnLineOfSight;
    protected bool m_player2OnLineOfSight;
    protected int m_lineOfSightPlayer1Dir1;
    protected int m_lineOfSightPlayer1Dir2;
    protected int m_lineOfSightPlayer2Dir1;
    protected int m_lineOfSightPlayer2Dir2;
    protected Entity m_chosenTarget;
    protected Entity m_currInRangeTarget;
    protected int m_currInRangeTargetDist;
    protected int m_fromTileX;
    protected int m_fromTileY;
    protected int m_distFromTarget;
    protected PathMap m_chosenPathMap;
    protected Queue<int> m_aStarPathMap;
    protected Queue<GridPoint2> m_aStarPointPath;
    protected bool m_prevFollowsTargetVal;
    protected bool m_prevHasDecreasingTimeoutVal;
    protected bool m_prevHasBackAndForthDecPercVal;
    protected bool m_respawns;
    protected bool m_showDeathExplosion;
    protected bool m_applyOverrideDir;
    protected int m_overrideDir;
    protected bool m_skipAnim;
    protected bool m_lifeTimeEnded;
    public static int m_numDead;
    protected int m_score;
    protected float m_separatingFromEnemyStartTime;
    protected float m_separatingFromEnemyDuration;
    protected bool m_isSeparatingFromEnemy;
    protected bool m_reactingToHit;
    protected float m_reactingToHitStartTime;
    protected bool m_canMove;
    protected bool m_soundOnDeath;

    protected RespawnExplosion m_explosion;

    protected GridPoint2 m_targetNode;

    public Entity chosenTarget
    {
        get { return m_chosenTarget; }
    }


    public bool canMove
    {
        get { return m_canMove; }
        set { m_canMove = value; }
    }

    protected static Dictionary<int, Dictionary<int, int>> m_corridorDirTable;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_score = 10;
        m_speed = SPEED;
        m_state = STATE_FOLLOWING;
        m_isSeparatingFromEnemy = false;
        m_collisionRect.x = -9;
        m_collisionRect.y = -9;
        m_collisionRect.width = 18;
        m_collisionRect.height = 18;
        m_hitPoints = HIT_POINTS;
        m_maxHitPoints = HIT_POINTS;
        m_goingStraightTryingToTurn = false;
        m_player1PathMap = m_game.m_player1PathMap;
        m_player2PathMap = m_game.m_player2PathMap;
        m_chosenPathMap = m_player1PathMap;
        m_chosenTarget = m_game.player1;
        m_drawOrder = 2;
        m_prevRandVel = Vector2.zero;
        m_glow.m_color = 0xffff0000;
        m_evadesTarget = false;
        m_followsTarget = true;
        m_checkDirStartTime = 0;
        m_checkDirMaxTimeout = 0.6f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_checkDirSmartFollowMaxTimeout = 0.01f;
        m_checkDirSmartFollowTimeout = m_checkDirSmartFollowMaxTimeout;
        m_hasOnTargetRangeTrigger = false;
        m_targetInRange = false;
        m_targetTriggerRange = 0;
        m_hasBackAndForthDecPerc = true;
        m_hasLifetime = false;
        m_lifetime = 0;
        m_lifeStartTime = 0;
        m_backForthDecPerc = 0.1f;
        m_backForthTimes = 0;
        m_maxDistFactor = 20;
        m_smartFollowing = false;
        m_canSmartFollow = true;
        m_prevVel = Vector2.zero;
        m_prevNonZeroVel = Vector2.zero;
        m_velChangeStartTime = m_game.timer;

        m_animator.Play("walk_down");
        m_currAnim = ANIM_WALK_DOWN;
        m_alerted = null;
        //m_frontChildGameObjectList.Add(m_alerted);
        m_hasDecreasingTimeout = true;
        m_checksLineOfSight = false;
        m_player1OnLineOfSight = false;
        m_player2OnLineOfSight = false;
        m_showDeathExplosion = true;
        m_applyOverrideDir = false;
        m_overrideDir = DIR_NONE;
        m_currInRangeTarget = null;
        m_currInRangeTargetDist = int.MaxValue;
        m_chosenTarget = null;
        m_dirTable = new string[5];
        m_lifeTimeEnded = false;
        m_isAlerted = false;
        m_alertEffect = null;
        m_skipAnim = false;
        m_respawns = true;
        m_soundOnDeath = true;

        if (m_game.state == Game.STATE_PLAYING_NORMAL)
        {
            m_canMove = true;
        }
        else
        {
            m_canMove = false;
        }// end else


        if (m_corridorDirTable == null)
        {
            m_corridorDirTable = new Dictionary<int, Dictionary<int, int>>();

            m_corridorDirTable.Add(DIR_LEFT, new Dictionary<int, int>());

            m_corridorDirTable[DIR_LEFT].Add(DIR_UP, CW);
            m_corridorDirTable[DIR_LEFT].Add(DIR_DOWN, CCW);

            m_corridorDirTable.Add(DIR_RIGHT, new Dictionary<int, int>());

            m_corridorDirTable[DIR_RIGHT].Add(DIR_UP, CCW);
            m_corridorDirTable[DIR_RIGHT].Add(DIR_DOWN, CW);

            m_corridorDirTable.Add(DIR_UP, new Dictionary<int, int>());

            m_corridorDirTable[DIR_UP].Add(DIR_LEFT, CCW);
            m_corridorDirTable[DIR_UP].Add(DIR_RIGHT, CW);

            m_corridorDirTable.Add(DIR_DOWN, new Dictionary<int, int>());

            m_corridorDirTable[DIR_DOWN].Add(DIR_LEFT, CW);
            m_corridorDirTable[DIR_DOWN].Add(DIR_RIGHT, CCW);

        }// end if

        m_dirTable[DIR_NONE] = "DIR_NONE";
        m_dirTable[DIR_LEFT] = "DIR_LEFT";
        m_dirTable[DIR_RIGHT] = "DIR_RIGHT";
        m_dirTable[DIR_UP] = "DIR_UP";
        m_dirTable[DIR_DOWN] = "DIR_DOWN";
    }

    public virtual void onRespawn()
    {

        if (m_numDead > 0)
        {

            m_numDead--;

            if (m_numDead <= 0)
            {
                m_numDead = 0;
                m_sound.playSound("EnemyRespawnEndOfMultiplier");

                //if (!Game.g.m_player1.m_isDead)
                {
                    //Game.g.m_gameObjectList.push(new FloatingTextMessage(Game.g.m_player1.x, Game.g.m_player1.y, "END OF MULTIPLIER"));
                }// end if

                //if (!Game.g.m_player2.m_isDead)
                {
                    //Game.g.m_gameObjectList.push(new FloatingTextMessage(Game.g.m_player2.x, Game.g.m_player2.y, "END OF MULTIPLIER"));
                }// end if                  

            }
            else if (m_numDead < EntityCoin.MAX_SIZE)
            {
                //SoundManager.playSound("EnemyRespawnMultiplier" + (m_numDead + 1));   
                m_sound.playSound("EnemyRespawnSound");

                //if (!Game.g.m_player1.m_isDead)
                {
                    //Game.g.m_gameObjectList.push(new FloatingTextMessage(Game.g.m_player1.x, Game.g.m_player1.y, "MULTIPLIER DOWN"));
                }// end if

                //if (!Game.g.m_player2.m_isDead)
                {
                    //Game.g.m_gameObjectList.push(new FloatingTextMessage(Game.g.m_player2.x, Game.g.m_player2.y, "MULTIPLIER DOWN"));
                }// end if                      

            }// end else if

        }// end if          

        //trace(this + " Respawn, numDead = " + m_numDead);

    }

    public virtual void startAlert()
    {
        //m_isAlerted = true;
        //m_alertEffect = new EnemyAlertEffect(this);
        //Game.g.m_gameObjectList.push(m_alertEffect);
        //gotoAndPlay(alertedLabel);
    }

    public virtual string alertedLabel()
    {
        return "alerted";
    }

    public virtual void applyOverrideDir(int dir_)
    {
        m_applyOverrideDir = true;
        m_overrideDir = dir_;
    }

    public virtual void onLifetimeEnded()
    {


    }

    public virtual void onTargetOutOfRange()
    {


    }

    public virtual void onTargetInRange(Entity p_target)
    {


    }

    public virtual void whileTargetInRange(Entity p_target)
    {


    }

    public virtual void alertDone()
    {
        stop();
        m_isAlerted = false;
        m_alertEffect = null;
        m_forceUpdateAnim = true;
    }

    public virtual bool isOtherEnemyInFollowRoute()
    {
        if (!(m_chosenTarget is EntityPlayer))
        {
            return false;
        }


        // inspect a specified number of tiles ahead in target path
        // and check if an enemy is already on one of those tiles and in following mode

        var lookAheadTileList = new List<List<int>>();

        int currTX = m_fromTileX;
        int currTY = m_fromTileY;

        lookAheadTileList.Add(new List<int>());
        lookAheadTileList[lookAheadTileList.Count - 1].Add(currTX);
        lookAheadTileList[lookAheadTileList.Count - 1].Add(currTY);

        for (var currLookAheadTile = 0; currLookAheadTile < MAX_PATH_LOOK_AHEAD_TILES; currLookAheadTile++)
        {

            var dir = m_chosenPathMap.getNextGoalDir(currTX, currTY);

            if (dir == DIR_NONE)
            {
                break;
            }// end if

            switch (dir)
            {

                case DIR_LEFT:
                    {

                        currTX = getWrappedTileX(currTX - 1);

                        break;
                    }// end case

                case DIR_RIGHT:
                    {

                        currTX = getWrappedTileX(currTX + 1);

                        break;
                    }// end case    

                case DIR_UP:
                    {

                        currTY = getWrappedTileY(currTY - 1);

                        break;
                    }// end case                        

                case DIR_DOWN:
                    {

                        currTY = getWrappedTileY(currTY + 1);

                        break;
                    }// end case                        

            }// end switch

            lookAheadTileList.Add(new List<int>());
            lookAheadTileList[lookAheadTileList.Count - 1].Add(currTX);
            lookAheadTileList[lookAheadTileList.Count - 1].Add(currTY);

        }// end for

        if (lookAheadTileList.Count <= 0)
        {
            return false;
        }// end if

        var gameObjectList = m_game.m_gameObjectList;

        for (var currObject = 0; currObject < gameObjectList.Count; currObject++)
        {
            if ((gameObjectList[currObject] is EntityEnemy) && (gameObjectList[currObject] != this))
            {

                var enemy = gameObjectList[currObject] as EntityEnemy;

                if ((enemy.m_state != STATE_FOLLOWING) || (!(enemy.m_chosenTarget is EntityPlayer)))
                {
                    continue;
                }// end if

                // check enemy position against look ahead tile list
                for (var currTile = 0; currTile < lookAheadTileList.Count; currTile++)
                {

                    if ((enemy.m_tx == lookAheadTileList[currTile][0]) && (enemy.m_ty == lookAheadTileList[currTile][1]))
                    {
                        return true;
                    }// end if

                }// end for

            }
        }
        return false;
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        if (m_respawns)
        {
            m_numDead++;

            if (m_numDead <= EntityCoin.MAX_SIZE && !m_game.completed)
            {
                if (m_soundOnDeath)
                {
                    m_game.setEnemyDeathSound("EnemyDieMultiplier" + (m_numDead + 1));
                }

                if (p_attacker is EntityPlayer1)
                {
                    m_game.setEnemyDeathMessagePlayer1("MULTIPLIER X" + (m_numDead + 1));
                }
                //else if (p_attacker is EntityPlayer2)
                //{
                //    m_game.setEnemyDeathMessagePlayer2("MULTIPLIER X" + (m_numDead + 1));
                //}
                else
                {
                    if (!m_game.player1.isDead)
                    {
                        m_game.setEnemyDeathMessagePlayer1("MULTIPLIER X" + (m_numDead + 1));
                    }

                    if (!m_game.player2.isDead)
                    {
                        m_game.setEnemyDeathMessagePlayer2("MULTIPLIER X" + (m_numDead + 1));
                    }
                }
            }
            else
            {
                if (m_soundOnDeath)
                {
                    m_game.setEnemyDeathSound("EnemyDie");
                }
            }
        }
        else
        {
            if (m_soundOnDeath)
            {
                m_game.setEnemyDeathSound("EnemyDie");
            }
        }

        if (m_showDeathExplosion)
        {
            GameObject deathExlosion = Instantiate(deathExlosionPrefab);
            deathExlosion.transform.position = position;
        }

        if (m_game.completed)
        {
            Debug.Log("show respawn explosion");
        }
        else if (m_respawns)
        {
            m_explosion = new RespawnExplosion(this, true);
        }
    }

    public virtual void onStopSmartFollowing()
    {

    }

    public virtual GameObject destroyedEffectType()
    {
        return null;
    }

    public virtual PathMap player1Map()
    {
        return m_game.m_player1PathMap;
    }

    public virtual PathMap player2Map()
    {
        return m_game.m_player2PathMap;
    }

    public virtual void updateMap()
    {
        // for safety map?

        m_fromTileX = (int)x;
        m_fromTileY = (int)y;

        m_fromTileX = getWrappedTileX(m_fromTileX);
        m_fromTileY = getWrappedTileY(m_fromTileY);

        //m_tileMap = m_game.m_enemyTileMap; //Game.g.m_tileMap;  
        m_player1PathMap = player1Map();
        m_player2PathMap = player2Map();
    }

    public virtual void updateTarget()
    {
        int distFromPlayer1 = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
        int distFromPlayer2 = int.MaxValue;// Vector2.Distance(m_game.player2.position, position);

        if (m_smartFollowing)
        {
            if (m_smartFollowingPlayerNumber == 1)
            {
                m_distFromTarget = distFromPlayer1;
                m_chosenPathMap = m_player1PathMap;
                m_chosenTarget = m_game.player1;
            }
            else
            {
                m_distFromTarget = distFromPlayer2;
                m_chosenPathMap = m_player2PathMap;
                m_chosenTarget = m_game.player2;
            }
        }
        else
        {
            if (distFromPlayer1 < distFromPlayer2)
            {
                m_distFromTarget = distFromPlayer1;
                m_chosenPathMap = m_player1PathMap;
                m_chosenTarget = m_game.player1;
            }
            else if (distFromPlayer1 > distFromPlayer2)
            {
                m_distFromTarget = distFromPlayer2;
                m_chosenPathMap = m_player2PathMap;
                m_chosenTarget = m_game.player2;
            }
        }
    }

    public virtual void updateTargetRange()
    {
        if (!m_hasOnTargetRangeTrigger)
        {
            return;
        }

        if (m_distFromTarget <= m_targetTriggerRange)
        {
            if (!m_targetInRange)
            {
                m_currInRangeTarget = m_chosenTarget;
                onTargetInRange(m_chosenTarget);
                whileTargetInRange(m_chosenTarget);
                m_targetInRange = true;
            }
            else
            {
                whileTargetInRange(m_chosenTarget);
            }
        }
        else if (m_targetInRange && (m_distFromTarget > m_targetTriggerRange))
        {
            m_currInRangeTarget = null;
            m_targetInRange = false;
            onTargetOutOfRange();
        }
    }

    public virtual bool isInLineOfSightPlayer(EntityPlayer p_player)
    {
        if (m_tx != p_player.tx && m_ty != p_player.ty)
        {
            return false;
        }

        int testX;
        int testY;
        bool dir1Blocked = false;
        bool dir2Blocked = false;

        if (p_player is EntityPlayer1)
        {
            m_lineOfSightPlayer1Dir1 = DIR_NONE;
            m_lineOfSightPlayer1Dir2 = DIR_NONE;
        }
        else
        {
            m_lineOfSightPlayer2Dir1 = DIR_NONE;
            m_lineOfSightPlayer2Dir2 = DIR_NONE;
        }

        if (m_ty == p_player.ty)
        {
            for (testX = m_tx; testX != p_player.tx;)
            {
                testX++;
                testX = getWrappedTileX(testX);

                if (!m_tileMap.canGoToTile(testX, m_ty, true))
                {
                    dir1Blocked = true;
                    break;
                }
            }

            if (testX == p_player.tx)
            {
                if (p_player is EntityPlayer1)
                {
                    m_lineOfSightPlayer1Dir1 = DIR_RIGHT;
                }
                else
                {
                    m_lineOfSightPlayer2Dir1 = DIR_RIGHT;
                }
            }

            for (testX = m_tx; testX != p_player.tx;)
            {
                testX--;
                testX = getWrappedTileX(testX);

                if (!m_tileMap.canGoToTile(testX, m_ty, true))
                {
                    dir2Blocked = true;
                    break;
                }
            }

            if (testX == p_player.tx)
            {
                if (p_player is EntityPlayer1)
                {
                    m_lineOfSightPlayer1Dir2 = DIR_LEFT;
                }
                else
                {
                    m_lineOfSightPlayer2Dir2 = DIR_LEFT;
                }
            }

            if (dir1Blocked && dir2Blocked)
            {
                return false;
            }
        }
        else
        {
            for (testY = m_ty; testY != p_player.ty;)
            {
                testY++;
                testY = getWrappedTileY(testY);

                if (!m_tileMap.canGoToTile(m_tx, testY, true))
                {
                    dir1Blocked = true;
                    break;
                }
            }

            if (testY == p_player.ty)
            {
                if (p_player is EntityPlayer1)
                {
                    m_lineOfSightPlayer1Dir1 = DIR_UP;
                }
                else
                {
                    m_lineOfSightPlayer2Dir1 = DIR_UP;
                }
            }

            for (testY = m_ty; testY != p_player.ty;)
            {
                testY--;
                testY = getWrappedTileY(testY);

                if (!m_tileMap.canGoToTile(m_tx, testY, true))
                {
                    dir2Blocked = true;
                    break;
                }
            }

            if (testY == p_player.ty)
            {
                if (p_player is EntityPlayer1)
                {
                    m_lineOfSightPlayer1Dir2 = DIR_DOWN;
                }
                else
                {
                    m_lineOfSightPlayer2Dir2 = DIR_DOWN;
                }
            }

            if (dir1Blocked && dir2Blocked)
            {
                return false;
            }
        }

        return true;
    }

    public virtual bool isInLineOfSightPlayer1()
    {
        return isInLineOfSightPlayer(m_game.player1);
    }

    public virtual bool isInLineOfSightPlayer2()
    {
        return isInLineOfSightPlayer(m_game.player2);
    }

    public virtual void outOfLineOfSightPlayer1()
    {

    }

    public virtual void outOfLineOfSightPlayer2()
    {

    }

    public virtual void onLineOfSightPlayer1()
    {

    }

    public virtual void onLineOfSightPlayer2()
    {

    }

    public virtual void whileOnLineOfSightPlayer1()
    {

    }

    public virtual void whileOnLineOfSightPlayer2()
    {

    }

    public virtual void stopSmartPlayerFollow()
    {
        if (m_smartFollowing)
        {
            m_smartFollowing = false;
            m_checkDirStartTime = m_game.timer;
            onStopSmartFollowing();
        }
    }

    public virtual void startSmartPlayerFollow(int p_playerNumber)
    {
        if (m_canSmartFollow)
        {
            int distFromTarget;

            if (p_playerNumber == 1)
            {
                distFromTarget = (int)Vector2.Distance(m_game.player1.position, position);
            }
            else
            {
                distFromTarget = (int)Vector2.Distance(m_game.player2.position, position);
            }

            if (distFromTarget == PathMap.TILE_BLOCKED)
            {
                return;
            }

            if (!m_smartFollowing)
            {
                startAlert();
            }

            m_smartFollowingPlayerNumber = p_playerNumber;
            m_smartFollowing = true;
            m_smartFollowingState = STATE_SMART_FOLLOWING_NORMAL;
            m_checkDirSmartFollowingStartTime = m_game.timer;
            m_isSeparatingFromEnemy = false;
        }
    }

    public virtual void startSmartPlayerFollowCountdown()
    {
        if (m_canSmartFollow)
        {
            m_smartFollowCountdownStartTime = m_game.timer;
            m_smartFollowingState = STATE_SMART_FOLLOWING_COUNTDOWN;
        }
    }

    public override void onDamage(Vector2 p_vel)
    {
        base.onDamage(p_vel);

        if (!m_reactingToHit)
        {
            Debug.Log("Spawn an 'EnemyHit' object here?");
            m_reactingToHit = true;
            m_reactingToHitStartTime = m_game.timer;
        }
    }

    public virtual int getNextCorridorDir(int p_dir1, int p_dir2)
    {
        if (m_corridorDirTable[p_dir1][p_dir2] == CW)
        {
            return getNextCorridorDirCW(p_dir2);
        }
        else
        {
            return getNextCorridorDirCCW(p_dir2);
        }
    }

    public virtual void playSideWalkAnim()
    {
        m_animator.Play("walk_side");
    }

    public virtual void playUpWalkAnim()
    {
        m_animator.Play("walk_up");
    }

    public virtual void playDownWalkAnim()
    {
        m_animator.Play("walk_down");
    }

    public virtual void updateAnim()
    {
        if (m_prevVel.x < 0)
        {
            if (m_currAnim != ANIM_WALK_LEFT || m_forceUpdateAnim)
            {
                m_forceUpdateAnim = false;
                playSideWalkAnim();
                m_currAnim = ANIM_WALK_LEFT;
                flipX(false);
            }
        }
        else if (m_prevVel.x > 0)
        {
            if (m_currAnim != ANIM_WALK_RIGHT || m_forceUpdateAnim)
            {
                m_forceUpdateAnim = false;
                playSideWalkAnim();
                m_currAnim = ANIM_WALK_RIGHT;
                flipX(true);
            }
        }
        else if (m_prevVel.y < 0)
        {
            if (m_currAnim != ANIM_WALK_DOWN || m_forceUpdateAnim)
            {
                m_forceUpdateAnim = false;
                playDownWalkAnim();
                m_currAnim = ANIM_WALK_DOWN;
            }
        }
        else if (m_prevVel.y > 0)
        {
            if (m_currAnim != ANIM_WALK_UP || m_forceUpdateAnim)
            {
                m_forceUpdateAnim = false;
                playUpWalkAnim();
                m_currAnim = ANIM_WALK_UP;
            }
        }
    }

    public virtual int getNextCorridorDirCCW(int p_dir)
    {
        if (p_dir == DIR_LEFT)
        {
            return DIR_DOWN;
        }
        else if (p_dir == DIR_DOWN)
        {
            return DIR_RIGHT;
        }
        else if (p_dir == DIR_RIGHT)
        {
            return DIR_UP;
        }
        else //if (p_dir == DIR_UP)
        {
            return DIR_LEFT;
        }
    }

    public virtual int getOppositeDir(int p_dir)
    {
        if (p_dir == DIR_LEFT)
        {
            return DIR_RIGHT;
        }
        else if (p_dir == DIR_DOWN)
        {
            return DIR_UP;
        }
        else if (p_dir == DIR_RIGHT)
        {
            return DIR_LEFT;
        }
        else //if (p_dir == DIR_UP)
        {
            return DIR_DOWN;
        }
    }

    public virtual int getNextCorridorDirCW(int p_dir)
    {
        if (p_dir == DIR_LEFT)
        {
            return DIR_UP;
        }
        else if (p_dir == DIR_UP)
        {
            return DIR_RIGHT;
        }
        else if (p_dir == DIR_RIGHT)
        {
            return DIR_DOWN;
        }
        else //if (p_dir == DIR_DOWN)
        {
            return DIR_LEFT;
        }
    }

    public virtual int chooseRandomCorridorDir(int p_fromTileX, int p_fromTileY)
    {
        List<List<int>> dirTable = new List<List<int>>(new List<int>[4]);
        List<List<int>> corridorDirTable = new List<List<int>>(new List<int>[4]);

        bool canGoDir1;
        bool canGoDir2;

        corridorDirTable[0] = new List<int>() { -1, 0 };
        corridorDirTable[1] = new List<int>() { 1, 0 };
        corridorDirTable[2] = new List<int>() { 0, -1 };
        corridorDirTable[3] = new List<int>() { 0, 1 };

        setDirTable(m_prevRandVel, dirTable);

        if (m_vel.x.isZero() && m_vel.y.isZero())
        {
            m_goingStraightTryingToTurn = false;
            m_goingStraightStartTime = m_game.timer;
            m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);

            m_waitingForCornerStop = false;

            canGoDir1 = m_tileMap.canGoToTile(p_fromTileX + dirTable[2][0], p_fromTileY + dirTable[2][1]);
            canGoDir2 = m_tileMap.canGoToTile(p_fromTileX + dirTable[3][0], p_fromTileY + dirTable[3][1]);

            if (!canGoDir1 && canGoDir2)
            {
                m_prevRandVel.x = dirTable[3][0];
                m_prevRandVel.y = dirTable[3][1];
                m_qCorridorDir = dirTable[0][2];
                return dirTable[3][2];
            }
            else if (!canGoDir2 && canGoDir1)
            {
                m_prevRandVel.x = dirTable[2][0];
                m_prevRandVel.y = dirTable[2][1];
                m_qCorridorDir = dirTable[0][2];
                return dirTable[2][2];
            }
            else if (!canGoDir1 && !canGoDir2)
            {

                m_prevRandVel.x = dirTable[1][0];
                m_prevRandVel.y = dirTable[1][1];

                if (Random.Range(0, 2) == 0)
                {
                    m_qCorridorDir = dirTable[2][2];
                }
                else
                {
                    m_qCorridorDir = dirTable[3][2];
                }

                //trace("TURNING BACK");
                return dirTable[1][2];

            }
            else
            {

                if (Random.Range(0, 2) == 0)
                {
                    m_prevRandVel.x = dirTable[2][0];
                    m_prevRandVel.y = dirTable[2][1];
                    m_qCorridorDir = dirTable[0][2];
                    return dirTable[2][2];
                }
                else
                {
                    m_prevRandVel.x = dirTable[3][0];
                    m_prevRandVel.y = dirTable[3][1];
                    m_qCorridorDir = dirTable[0][2];
                    return dirTable[3][2];
                }

            }

        }
        else if ((m_qCorridorDir != DIR_NONE) &&
                 m_tileMap.canGoToTile(p_fromTileX + corridorDirTable[m_qCorridorDir - 1][0],
                                       p_fromTileY + corridorDirTable[m_qCorridorDir - 1][1]) &&
                 !m_waitingForCornerStop)
        {
            m_goingStraightTryingToTurn = false;
            m_goingStraightStartTime = m_game.timer;
            m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
            m_numTurnTimes++;

            if (m_numTurnTimes > m_maxTurnTimes)
            {
                m_numTurnTimes = 0;
                m_maxTurnTimes = Random.Range(MIN_TURN_TIMES, MAX_TURN_TIMES);
                m_qCorridorDir = DIR_NONE;
                return dirTable[0][2];
            }// end if

            m_waitingForCornerStop = true;
            int newCorridorDir = m_qCorridorDir;

            int projTX = getWrappedTileX(p_fromTileX + corridorDirTable[m_qCorridorDir - 1][0]);
            int projTY = getWrappedTileY(p_fromTileY + corridorDirTable[m_qCorridorDir - 1][1]);

            m_qCorridorDir = getNextCorridorDir(dirTable[0][2], m_qCorridorDir);

            m_prevRandVel.x = corridorDirTable[newCorridorDir - 1][0];
            m_prevRandVel.y = corridorDirTable[newCorridorDir - 1][1];

            int oppositeDir = getOppositeDir(m_qCorridorDir);

            canGoDir2 = m_tileMap.canGoToTile(projTX + corridorDirTable[oppositeDir - 1][0],
                                              projTY + corridorDirTable[oppositeDir - 1][1]);

            if (!canGoDir2 && Random.Range(0, 2) == 0)
            {
                m_qCorridorDir = oppositeDir;
            }

            return newCorridorDir;
        }
        else
        {

            if (!m_goingStraightTryingToTurn)
            {

                if ((m_qCorridorDir != DIR_NONE) &&
                    !m_tileMap.canGoToTile(p_fromTileX +
                                           corridorDirTable[m_qCorridorDir - 1][0],
                                           p_fromTileY + corridorDirTable[m_qCorridorDir - 1][1]) &&
                    m_waitingForCornerStop)
                {
                    m_waitingForCornerStop = false;
                }// end if              

                if (m_game.timer - m_goingStraightStartTime > m_goingStraightDuration)
                {
                    m_goingStraightTryingToTurn = true;
                    m_qCorridorDir = DIR_NONE;
                }// end if

            }
            else
            {

                canGoDir1 = m_tileMap.canGoToTile(p_fromTileX + dirTable[2][0], p_fromTileY + dirTable[2][1]);
                canGoDir2 = m_tileMap.canGoToTile(p_fromTileX + dirTable[3][0], p_fromTileY + dirTable[3][1]);

                if (canGoDir1 && !canGoDir2)
                {
                    m_goingStraightTryingToTurn = false;
                    m_goingStraightStartTime = m_game.timer;
                    m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
                    m_prevRandVel.x = dirTable[2][0];
                    m_prevRandVel.y = dirTable[2][1];
                    return dirTable[2][2];
                }
                else if (!canGoDir1 && canGoDir2)
                {
                    m_goingStraightTryingToTurn = false;
                    m_goingStraightStartTime = m_game.timer;
                    m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
                    m_prevRandVel.x = dirTable[3][0];
                    m_prevRandVel.y = dirTable[3][1];
                    return dirTable[3][2];
                }
                else if (canGoDir1 && canGoDir2)
                {

                    if (Random.Range(0, 2) == 0)
                    {
                        m_goingStraightTryingToTurn = false;
                        m_goingStraightStartTime = m_game.timer;
                        m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
                        m_prevRandVel.x = dirTable[2][0];
                        m_prevRandVel.y = dirTable[2][1];
                        return dirTable[2][2];
                    }
                    else
                    {
                        m_goingStraightTryingToTurn = false;
                        m_goingStraightStartTime = m_game.timer;
                        m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
                        m_prevRandVel.x = dirTable[3][0];
                        m_prevRandVel.y = dirTable[3][1];
                        return dirTable[3][2];
                    }// end else

                }// end else if

            }// end else

            return dirTable[0][2];
        }
    }

    public void setDirTable(Vector2 p_chosenVel, List<List<int>> p_dirTable)
    {
        if (p_chosenVel.x < 0)
        {
            p_dirTable[0] = new List<int>() { -1, 0, DIR_LEFT };
            p_dirTable[1] = new List<int>() { 1, 0, DIR_RIGHT };
            p_dirTable[2] = new List<int>() { 0, -1, DIR_DOWN };
            p_dirTable[3] = new List<int>() { 0, 1, DIR_UP };
        }
        else if (p_chosenVel.x > 0)
        {
            p_dirTable[0] = new List<int>() { -1, 0, DIR_RIGHT };
            p_dirTable[1] = new List<int>() { 1, 0, DIR_LEFT };
            p_dirTable[2] = new List<int>() { 0, 1, DIR_UP };
            p_dirTable[3] = new List<int>() { 0, -1, DIR_DOWN };
        }
        else if (p_chosenVel.y < 0)
        {
            p_dirTable[0] = new List<int>() { 0, -1, DIR_DOWN };
            p_dirTable[1] = new List<int>() { 0, 1, DIR_UP };
            p_dirTable[2] = new List<int>() { 1, 0, DIR_LEFT };
            p_dirTable[3] = new List<int>() { -1, 0, DIR_RIGHT };
        }
        else if (p_chosenVel.y >= 0)
        {
            p_dirTable[0] = new List<int>() { 0, 1, DIR_UP };
            p_dirTable[1] = new List<int>() { 0, -1, DIR_DOWN };
            p_dirTable[2] = new List<int>() { -1, 0, DIR_RIGHT };
            p_dirTable[3] = new List<int>() { 1, 0, DIR_LEFT };
        }
    }

    public virtual GameObject respawnEffectType()
    {
        return m_respawnEffectType;
    }

    public virtual void updateDamage()
    {
        if (!m_game.player1.terminated && collidesWithObject(m_game.player1))
        {
            m_game.player1.doDamage(m_damage, m_vel);
        }

        //if (!m_game.player2.terminated && collidesWithObject(m_game.player2))
        {
            //m_game.player2.doDamage(m_damage, m_vel);
        }
    }

    public virtual void onTargetInaccessible()
    {

    }

    public virtual void updateLifeTime()
    {
        if (m_hasLifetime && !m_lifeTimeEnded && (m_game.timer - m_lifeStartTime) > m_lifetime)
        {
            m_lifeTimeEnded = true;
            onLifetimeEnded();
        }
    }

    public virtual void updateReactionToHit()
    {
        if (m_reactingToHit && (m_game.timer - m_reactingToHitStartTime) > REACTING_TO_HIT_DURATION)
        {
            m_reactingToHit = false;
        }
    }

    public void Update()
    {
        if (m_explosion != null)
        {
            m_explosion.doPhysics();

            if (m_explosion.m_done)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void doPhysics()
    {
        m_skipAnim = false;

        updateReactionToHit();

        if (m_isAlerted || !m_canMove)
        {
            updateDamage();
            return;
        }

        updateMap();

        updateTarget();

        int qDir;

        if (m_chosenPathMap == null)
        {
            m_chosenPathMap = player1Map();
        }

        if (m_state == STATE_FOLLOWING)
        {
            if (!m_followsTarget && !m_smartFollowing && !m_evadesTarget)
            {
                m_state = STATE_PATROLLING_CORRIDORS;
            }
            else
            {

                float distFactor;
                int lastQDir;

                if ((!m_smartFollowing) && (m_game.timer - m_checkDirStartTime > m_checkDirTimeout))
                {
                    m_checkDirStartTime = m_game.timer;

                    if (m_hasDecreasingTimeout)
                    {

                        if (m_distFromTarget > m_maxDistFactor)
                        {
                            m_distFromTarget = m_maxDistFactor;
                        }

                        distFactor = m_distFromTarget / m_maxDistFactor;
                        m_checkDirTimeout = m_checkDirMaxTimeout * distFactor;

                    }

                    lastQDir = m_qDir;

                    if (!m_applyOverrideDir)
                    {
                        if (m_followsTarget)
                        {
                            m_qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
                        }
                        else if (m_evadesTarget)
                        {
                            m_qDir = m_chosenPathMap.getNextEvasionDir(m_fromTileX, m_fromTileY);
                        }

                    }
                    else
                    {
                        m_applyOverrideDir = false;
                        m_qDir = m_overrideDir;
                    }

                    if (m_hasBackAndForthDecPerc)
                    {

                        m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);
                        if (m_checkDirTimeout < MIN_CHECK_DIR_TIMEOUT)
                        {
                            m_checkDirTimeout = MIN_CHECK_DIR_TIMEOUT;
                        }
                    }
                }
                else if ((m_smartFollowing) && (m_game.timer - m_checkDirSmartFollowingStartTime > m_checkDirSmartFollowTimeout))
                {

                    m_checkDirSmartFollowingStartTime = m_game.timer;

                    if (m_distFromTarget > m_maxDistFactor)
                    {
                        m_distFromTarget = m_maxDistFactor;
                    }

                    distFactor = m_distFromTarget / m_maxDistFactor;
                    m_checkDirTimeout = m_checkDirMaxTimeout * distFactor;

                    lastQDir = m_qDir;

                    if (!m_applyOverrideDir)
                    {
                        m_qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
                    }
                    else
                    {
                        m_applyOverrideDir = false;
                        m_qDir = m_overrideDir;
                    }

                    m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);
                    if (m_checkDirTimeout < 0)
                    {
                        m_checkDirTimeout = 0;
                    }

                    if ((m_smartFollowingState == STATE_SMART_FOLLOWING_COUNTDOWN) && (m_game.timer - m_smartFollowCountdownStartTime > SMART_FOLLOW_DURATION))
                    {
                        onStopSmartFollowing();
                        m_smartFollowing = false;
                        m_checkDirStartTime = m_game.timer;
                    }

                }

                bool cantReachTarget = (m_qDir == DIR_NONE); //&& (m_distFromTarget > 1);
                bool isOtherEnemyInFollowRouteRes = isOtherEnemyInFollowRoute();

                if (cantReachTarget || (!m_smartFollowing && isOtherEnemyInFollowRouteRes))
                {
                    if (cantReachTarget)
                    {
                        onTargetInaccessible();
                    }

                    if (isOtherEnemyInFollowRouteRes)
                    {
                        m_separatingFromEnemyStartTime = m_game.timer;
                        m_isSeparatingFromEnemy = true;
                        m_separatingFromEnemyDuration = Random.Range(SEPARATING_FROM_ENEMY_MIN_DURATION,
                                                                                                          SEPARATING_FROM_ENEMY_MAX_DURATION);
                    }

                    m_prevRandVel = m_vel;
                    m_prevRandVel = m_prevRandVel.normalized;
                    m_state = STATE_PATROLLING_CORRIDORS;

                    m_qCorridorDir = DIR_NONE;
                    m_goingStraightTryingToTurn = false;
                    m_goingStraightStartTime = m_game.timer;
                    m_goingStraightDuration = Random.Range(GOING_STRAIGHT_MIN_DURATION, GOING_STRAIGHT_MAX_DURATION);
                    m_numTurnTimes = 0;
                    m_maxTurnTimes = Random.Range(MIN_TURN_TIMES, MAX_TURN_TIMES);
                    m_checkDirStartTime = m_game.timer;
                    m_checkDirTimeout = m_checkDirMaxTimeout;

                }
            }
        }
        else if (m_state == STATE_PATROLLING_CORRIDORS)
        {
            Debug.Log("patrolling corridors");
            if (m_isSeparatingFromEnemy && (m_game.timer - m_separatingFromEnemyStartTime > m_separatingFromEnemyDuration))
            {
                m_isSeparatingFromEnemy = false;
            }


            if (m_game.timer - m_checkDirStartTime > m_checkDirTimeout)
            {
                m_checkDirStartTime = m_game.timer;

                if (m_evadesTarget)
                {
                    qDir = m_chosenPathMap.getNextEvasionDir(m_fromTileX, m_fromTileY);
                }
                else
                {


                    qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
                }

                if ((qDir != DIR_NONE) && (((m_followsTarget || m_evadesTarget) && !m_isSeparatingFromEnemy) || m_smartFollowing))
                {
                    m_state = STATE_FOLLOWING;
                    m_isSeparatingFromEnemy = false;
                    m_checkDirTimeout = m_checkDirMaxTimeout;

                    if (!m_applyOverrideDir)
                    {
                        m_qDir = qDir;
                    }
                    else
                    {
                        m_applyOverrideDir = false;
                        m_qDir = m_overrideDir;
                    }
                }
                else
                {
                    m_smartFollowing = false;

                    if (!m_applyOverrideDir)
                    {
                        m_qDir = chooseRandomCorridorDir(m_fromTileX, m_fromTileY);
                    }
                    else
                    {
                        m_applyOverrideDir = false;
                        m_qDir = m_overrideDir;
                    }
                }

                if (m_hasBackAndForthDecPerc)
                {
                    m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);

                    if (m_checkDirTimeout < MIN_CHECK_DIR_TIMEOUT)
                    {
                        m_checkDirTimeout = MIN_CHECK_DIR_TIMEOUT;
                    }

                    if (m_backForthTimes == 0)
                    {
                        m_checkDirTimeout = m_checkDirMaxTimeout;
                    }
                }
            }
        }

        if (m_qDir != DIR_NONE)
        {
            m_lastDir = m_qDir;
        }

        if (!m_skipAnim && (((!m_prevVel.x.isEqual(m_vel.x) || !m_prevVel.y.isEqual(m_vel.y)) &&
                             (m_game.timer - m_velChangeStartTime > VEL_CHANGE_TIMEOUT)) || m_forceUpdateAnim))
        {
            if (((m_vel.x > 0) && (m_prevNonZeroVel.x < 0)) ||
                ((m_vel.x < 0) && (m_prevNonZeroVel.x > 0)) ||
                ((m_vel.y < 0) && (m_prevNonZeroVel.y > 0)) ||
                ((m_vel.y > 0) && (m_prevNonZeroVel.y < 0)))
            {
                m_backForthTimes++;
            }
            else if (!m_vel.x.isZero() || !m_vel.y.isZero())
            {
                m_backForthTimes = 0;
            }

            m_velChangeStartTime = m_game.timer;
            m_prevVel.x = m_vel.x;
            m_prevVel.y = m_vel.y;

            if (!m_vel.x.isZero() || !m_vel.y.isZero())
            {
                m_prevNonZeroVel.x = m_vel.x;
                m_prevNonZeroVel.y = m_vel.y;
            }

            updateAnim();

        }


        updateTargetRange();
        updateLifeTime();
        updateDamage();

        base.doPhysics();

        if (m_checksLineOfSight)
        {

            if (m_game.player1 && !m_game.player1.terminated)
            {

                bool player1OnSightCheck = isInLineOfSightPlayer1();

                if (!m_player1OnLineOfSight && player1OnSightCheck)
                {
                    m_player1OnLineOfSight = true;

                    onLineOfSightPlayer1();
                }
                else if (m_player1OnLineOfSight)
                {
                    if (!player1OnSightCheck)
                    {
                        m_player1OnLineOfSight = false;

                        outOfLineOfSightPlayer1();
                    }
                    else
                    {

                        whileOnLineOfSightPlayer1();
                    }
                }
            }

            if (m_game.player2 && !m_game.player2.terminated)
            {

                bool player2OnSightCheck = isInLineOfSightPlayer2();

                if (!m_player2OnLineOfSight && player2OnSightCheck)
                {
                    m_player2OnLineOfSight = true;

                    onLineOfSightPlayer2();
                }
                else if (m_player2OnLineOfSight)
                {
                    if (!player2OnSightCheck)
                    {
                        m_player2OnLineOfSight = false;

                        outOfLineOfSightPlayer2();
                    }
                    else
                    {

                        whileOnLineOfSightPlayer2();
                    }
                }

            }
        }

        updateAbsCollisionRect();
    }
}
