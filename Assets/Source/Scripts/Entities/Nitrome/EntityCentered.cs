﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityCentered : Entity
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        x = (Game.TILE_WIDTH * m_tx) + (Game.TILE_WIDTH / 2f);
        y = (Game.TILE_HEIGHT * m_ty) + (Game.TILE_HEIGHT / 2f);
    }

    public override void updatePosFromTile()
    {
        x = (Game.TILE_WIDTH * m_tx) + (Game.TILE_WIDTH / 2f);
        y = (Game.TILE_HEIGHT * m_ty) + (Game.TILE_HEIGHT / 2f);
    }

    public override void updateTilePos()
    {
        m_tx = (int)((x - (Game.TILE_WIDTH / 2)) / Game.TILE_WIDTH);
        m_ty = (int)((y - (Game.TILE_HEIGHT / 2)) / Game.TILE_HEIGHT);
    }

    public override int getTileXForX(float x_)
    {
        return (int)Mathf.Floor((x_ - (Game.TILE_WIDTH / 2)) / Game.TILE_WIDTH);
    }

    public override int getTileYForY(float y_)
    {
        return (int)Mathf.Floor((y_ - (Game.TILE_HEIGHT / 2)) / Game.TILE_HEIGHT);
    }


    public virtual void flipX(bool p_setting)
    {
        scaleX = p_setting ? -1 : 1;
    }

    public virtual void flipY(bool p_setting)
    {
        scaleY = p_setting ? -1 : 1;
    }
}
