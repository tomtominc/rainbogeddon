﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointUint
{
    public int x;
    public int y;

    public PointUint(int p_x, int p_y)
    {
        x = p_x;
        y = p_y;
    }
}
