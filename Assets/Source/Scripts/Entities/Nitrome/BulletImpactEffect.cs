﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletImpactEffect : EffectCentered
{
    public virtual void setPowerLevel(int p_powerLevel)
    {
        int type = 0;

        if (p_powerLevel == 2)
        {
            type = 1 + Mathf.FloorToInt(Random.Range(0, 3));

            // play impact effect
        }
        else if (p_powerLevel == 3)
        {
            type = 1 + Mathf.FloorToInt(Random.Range(0, 5));

            //play impact effect
        }
    }

    public void terminate()
    {
        m_terminated = true;
    }
}
