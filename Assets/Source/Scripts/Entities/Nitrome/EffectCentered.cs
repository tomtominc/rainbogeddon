﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectCentered : GO
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        x = (Game.TILE_WIDTH * m_tx) + (Game.TILE_WIDTH / 2f);
        y = (Game.TILE_HEIGHT * m_ty) + (Game.TILE_HEIGHT / 2f);
    }
}
