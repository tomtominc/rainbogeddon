﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpTeleporter : PowerUp
{
    private TeleporterHolder m_teleportHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_teleportHolder = new TeleporterHolder(m_game.player1);
    }
    public override string soundID()
    {
        return "Teleport";
    }

    public override string displayName()
    {
        return "TELEPORTER";
    }

    public override WeaponHolder holder()
    {
        return m_teleportHolder;
    }
}
