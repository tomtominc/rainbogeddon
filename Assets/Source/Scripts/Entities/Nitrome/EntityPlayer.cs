﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class EntityPlayer : EntityCharacter
{
    public GameObject m_playerHitPrefab;
    public GameObject m_deathEffectPrefab;
    public PowerUpRingEffect m_powerUpRingEffect;
    public EntityDrill m_drill;

    public const float SPEED = 4.6F;
    public const int STATE_NORMAL = 0;
    public const int STATE_TELEPORTING_OUT = 1;
    public const int STATE_TELEPORTING_IN = 2;
    public const int STATE_STOPPING_FOR_VICTORY = 3;
    public const int STATE_VICTORY = 4;
    public const int STATE_STARTED_TELEPORTING = 5;
    public const int HIT_POINTS = 100;

    public const int ANIM_WALK_LEFT = 0;
    public const int ANIM_WALK_RIGHT = 1;
    public const int ANIM_WALK_UP = 2;
    public const int ANIM_WALK_DOWN = 3;
    public const int ANIM_TELEPORTING = 4;

    public const int RECOVERY_WAIT_DURATION = 2;
    public const int INVISIBILITY_DURATION = 2;
    public const int POS_PROJECTION_LENGTH = 7;
    public const int TEMPORARY_STOP_DURATION = 50;

    protected Entity m_invincibilitySource;
    protected bool m_canPressUp;
    protected bool m_canPressDown;
    protected bool m_canPressLeft;
    protected bool m_canPressRight;
    protected bool m_canPressFire;
    protected int m_currAnim;
    protected Vector2 m_prevVel;
    protected int m_teleportOldDir;
    protected EntityTeleporter m_teleporter;
    protected Vector2 m_oldVel;
    protected Entity m_currTileMarker;
    protected Entity m_oldTileMarker;
    protected Entity m_oldPrevTileMarker;
    public int m_projectedTX;
    public int m_projectedTY;
    protected int m_currTeleporterSpin;
    protected int m_safetyProjectedTX;
    protected int m_safetyProjectedTY;

    protected bool m_invisible;
    protected float m_invisibleStartTime;
    protected bool m_reactingToHit;
    protected int m_score;
    protected bool m_doingTemporaryStop;
    protected float m_temporaryStopStartTime;
    protected bool m_inputEnabled;
    protected EntityCollectible m_lastCollectedObject;

    protected InputController m_inputController;

    public event Action onWrappedAction = delegate { };

    public bool inputEnabled
    {
        get { return m_inputEnabled; }
        set { m_inputEnabled = value; }
    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_speed = SPEED;
        m_state = STATE_NORMAL;
        m_collisionRect.x = -11;
        m_collisionRect.y = -11;
        m_collisionRect.width = 22;
        m_collisionRect.height = 22;
        m_canPressFire = true;
        m_canPressUp = true;
        m_canPressDown = true;
        m_canPressLeft = true;
        m_canPressRight = true;
        m_hitPoints = HIT_POINTS;
        m_maxHitPoints = HIT_POINTS;
        m_invincibilitySource = null;
        m_oldVel = Vector2.zero;
        m_prevVel = Vector2.zero;
        m_currAnim = ANIM_WALK_UP;
        m_animator.Play(getAnimationFromIndex(m_currAnim));
        m_glow.m_color = 0xffffffff;
        m_projectedTX = m_tx;
        m_projectedTY = m_ty;
        m_safetyProjectedTX = m_tx;
        m_safetyProjectedTY = m_ty;
        m_drawOrder = 3;
        m_weaponHolder = null;
        m_powerUpRingEffect = GetComponentInChildren<PowerUpRingEffect>();
        m_backChildGameObjectList = new List<GO>();
        m_invisible = false;
        m_reactingToHit = false;
        m_score = 0;
        m_doingTemporaryStop = false;
        m_inputEnabled = false;
        m_lastCollectedObject = null;
        m_canDie = true;

        m_inputController = GetComponent<InputController>();
        m_inputController.init(this);

        if (m_drill)
        {
            m_drill.initialize(m_controller, (int)x, (int)y);
        }

    }

    public void doTemporaryStop()
    {
        m_doingTemporaryStop = true;
        m_temporaryStopStartTime = m_game.timer;
    }

    public override void doInput()
    {
        if (!m_inputEnabled)
            return;

        // input stuff

        int swipeDir = m_inputController.getSwipeDirection();

        if (Input.GetKeyDown(KeyCode.D) || (swipeDir & SwipeDirection.RIGHT) != 0)
        {
            m_qDir = DIR_RIGHT;
        }
        else if (Input.GetKeyDown(KeyCode.W) || (swipeDir & SwipeDirection.UP) != 0)
        {
            m_qDir = DIR_UP;
        }
        else if (Input.GetKeyDown(KeyCode.A) || (swipeDir & SwipeDirection.LEFT) != 0)
        {
            m_qDir = DIR_LEFT;
        }
        else if (Input.GetKeyDown(KeyCode.S) || (swipeDir & SwipeDirection.DOWN) != 0)
        {
            m_qDir = DIR_DOWN;
        }
        else if (Input.GetKeyDown(KeyCode.Space) || (swipeDir & SwipeDirection.TAP) != 0)
        {
            m_fire = true;
        }

    }

    public override void onWrapped()
    {
        onWrappedAction();
    }

    public void updateAnim()
    {
        if (!m_prevVel.x.isEqual(m_vel.x) || !m_prevVel.y.isEqual(m_vel.y) || m_forceUpdateAnim)
        {
            m_prevVel.x = m_vel.x;
            m_prevVel.y = m_vel.y;

            if (m_vel.x < 0)
            {
                m_forceUpdateAnim = false;
                m_currAnim = ANIM_WALK_LEFT;
                playAnimation(m_currAnim);
                flipX(false);
            }
            else if (m_vel.x > 0)
            {
                m_forceUpdateAnim = false;
                m_currAnim = ANIM_WALK_RIGHT;
                playAnimation(m_currAnim);
                flipX(true);
            }
            else if (m_vel.y < 0)
            {
                m_forceUpdateAnim = false;
                m_currAnim = ANIM_WALK_UP;
                playAnimation(m_currAnim);
            }
            else if (m_vel.y > 0)
            {
                m_forceUpdateAnim = false;
                m_currAnim = ANIM_WALK_DOWN;
                playAnimation(m_currAnim);
            }
        }

        if (m_vel.x.isZero() && m_vel.y.isZero())
        {
            bool l_isTeleporting = (m_state == STATE_TELEPORTING_OUT) || (m_state == STATE_TELEPORTING_IN);

            if (m_lastDir == DIR_LEFT)
            {
                if ((m_currAnim != ANIM_WALK_LEFT || m_forceUpdateAnim) &&
                    !(l_isTeleporting && !m_tileMap.canGoLeft((int)m_cTX, (int)m_cTY, true)))
                {
                    m_forceUpdateAnim = false;
                    m_currAnim = ANIM_WALK_LEFT;
                    playAnimation(m_currAnim);
                    flipX(false);
                }
            }
            else if (m_lastDir == DIR_RIGHT)
            {
                if ((m_currAnim != ANIM_WALK_RIGHT || m_forceUpdateAnim) &&
                    !(l_isTeleporting && !m_tileMap.canGoRight((int)m_cTX, (int)m_cTY, true)))
                {
                    m_forceUpdateAnim = false;
                    m_currAnim = ANIM_WALK_LEFT;
                    playAnimation(m_currAnim);
                    flipX(true);
                }
            }
            else if (m_lastDir == DIR_UP)
            {
                if ((m_currAnim != ANIM_WALK_UP || m_forceUpdateAnim) &&
                    !(l_isTeleporting && !m_tileMap.canGoUp((int)m_cTX, (int)m_cTY, true)))
                {
                    m_forceUpdateAnim = false;
                    m_currAnim = ANIM_WALK_DOWN;
                    playAnimation(m_currAnim);
                }
            }
            else if (m_lastDir == DIR_DOWN)
            {
                if ((m_currAnim != ANIM_WALK_DOWN || m_forceUpdateAnim) &&
                    !(l_isTeleporting && !m_tileMap.canGoDown((int)m_cTX, (int)m_cTY, true)))
                {
                    m_forceUpdateAnim = false;
                    m_currAnim = ANIM_WALK_UP;
                    playAnimation(m_currAnim);
                }
            }
            else if (m_lastDir == DIR_NONE)
            {
                if (m_forceUpdateAnim)
                {
                    m_forceUpdateAnim = false;
                    m_currAnim = ANIM_WALK_UP;
                    playAnimation(m_currAnim);
                }
            }
        }
    }

    public virtual void updateProjectedPos()
    {
        int incX = 0;
        int incY = 0;

        m_projectedTX = (int)m_cTX;
        m_projectedTY = (int)m_cTY;

        if (m_vel.x < 0)
        {
            incX = -1;
        }
        else if (m_vel.x > 0)
        {
            incX = 1;
        }
        else if (m_vel.y < 0)
        {
            incY = -1;
        }
        else if (m_vel.y > 0)
        {
            incY = 1;
        }

        if (incX == 0 && incY == 0)
        {
            return;
        }

        int testX = 0;
        int testY = 0;


        for (int incPos = 0; incPos < POS_PROJECTION_LENGTH; incPos++)
        {
            if (incX != 0)
            {
                testX = getWrappedTileX(m_projectedTX + incX);

                if (!m_tileMap.canGoToTile(testX, m_ty, true))
                {
                    break;
                }

                m_projectedTX = testX;
            }
            else
            {
                testY = getWrappedTileY(m_projectedTY + incY);

                if (!m_tileMap.canGoToTile(m_tx, testY, true))
                {
                    break;
                }

                m_projectedTY = testY;
            }
        }

        m_safetyProjectedTX = (int)m_cTX;
        m_safetyProjectedTY = (int)m_cTY;

        //TODO:
        // check if safety tilemap exists???
        // do checks (466)


    }

    public virtual void applyPowerUp(PowerUp p_powerUp)
    {
        if (m_weaponHolder != null)
        {
            if (m_weaponHolder.GetType() == p_powerUp.holder().GetType())
            {
                //Debug.Log("weapon holder same!");
                if (m_weaponHolder.upgrade())
                {
                    m_sound.playSound("LevelUp");
                    //Debug.Log("init a LEVEL UP text here!");
                }
                else
                {
                    m_sound.playSound(p_powerUp.soundID());
                    //Debug.LogFormat("init text displaying {0} here! ", p_powerUp.displayName());
                }
            }
            else
            {
                //Debug.Log("new weapon holder!");
                m_weaponHolder.terminate();
                m_sound.playSound(p_powerUp.soundID());
                //Debug.LogFormat("init text displaying {0} here! ", p_powerUp.displayName());
                m_weaponHolder = p_powerUp.getHolder(this);
            }

            m_powerUpRingEffect.setPowerLevel(m_weaponHolder.currUpgradeStage);
        }
        else
        {
            //Debug.Log("weapon holder was null");
            m_weaponHolder = p_powerUp.getHolder(this);
            m_powerUpRingEffect.setPowerLevel(m_weaponHolder.currUpgradeStage);
            m_sound.playSound(p_powerUp.soundID());
            //Debug.LogFormat("init text displaying {0} here! ", p_powerUp.displayName());
        }

        onApplyPowerUpUpdateHUD();
    }

    public virtual void onApplyPowerUpUpdateHUD()
    {

    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        m_sound.playSound(deathSoundID());
        spawnDeathExplosion();
    }

    public virtual string deathSoundID()
    {
        return string.Empty;
    }

    public virtual void destroyedEffectType()
    {

    }

    public virtual void spawnDeathExplosion()
    {
        if (m_deathEffectPrefab)
        {
            GameObject l_deathEffect = Instantiate(m_deathEffectPrefab);
            l_deathEffect.transform.position = new Vector3(x, y);
        }

    }

    public virtual void spawnPlayerHit()
    {
        if (m_playerHitPrefab)
        {
            GameObject l_playerHit = Instantiate(m_playerHitPrefab);
            l_playerHit.transform.position = new Vector3(x, y);
        }
    }

    public override void doDamage(int p_hitPoints, Vector2 p_vel, Entity p_attacker = null, Entity p_weapon = null)
    {
        if (!m_isDead && !m_triggerDeath && !m_invisible && !m_reactingToHit && m_canDie)
        {
            spawnPlayerHit();

            m_reactingToHit = true;

            if (m_state == STATE_TELEPORTING_OUT || m_state == STATE_TELEPORTING_IN)
            {
                abortTeleport();
                if (m_state == STATE_TELEPORTING_OUT)
                {
                    tryTerminateTeleporter();
                }
            }

            if (m_vel.x < 0)
            {
                m_animator.Play("hit_side");
            }
            else if (m_vel.x > 0)
            {
                m_animator.Play("hit_side");
                flipX(true);
            }
            else if (m_vel.y < 0)
            {
                m_animator.Play("hit_up");
            }
            else if (m_vel.y > 0)
            {
                m_animator.Play("hit_down");
            }


            if (m_vel.x.isZero() && m_vel.y.isZero())
            {
                if (m_lastDir == DIR_LEFT)
                {
                    m_animator.Play("hit_side");
                    flipX(false);
                }
                else if (m_lastDir == DIR_RIGHT)
                {
                    m_animator.Play("hit_side");
                    flipX(true);
                }
                else if (m_lastDir == DIR_UP)
                {
                    m_animator.Play("hit_up");
                }
                else
                {
                    m_animator.Play("hit_down");
                }
            }

            m_invisible = true;
            m_flicker = true;
            m_invisibleStartTime = m_game.timer;

            hitDone();

            if (m_weaponHolder != null)
            {
                m_sound.playSound("PlayerHitAndLosePowerUp");
                terminateWeapon();
            }
            else
            {
                onDeath(p_attacker, p_weapon);
                m_triggerDeath = true;
            }
        }
    }

    public virtual void terminateWeapon()
    {
        m_weaponHolder.terminate();
        m_weaponHolder = null;
        m_powerUpRingEffect.terminate();
        onHitUpdateHUD();
    }

    public virtual void onHitUpdateHUD()
    {

    }

    public virtual void startVictorySequence()
    {
        if (m_weaponHolder != null)
        {
            m_weaponHolder.tryStopFire();
        }

        m_state = STATE_STOPPING_FOR_VICTORY;
        m_inputEnabled = false;
        stopAtNextTile();
    }

    public virtual void hitDone()
    {
        stop();
        m_reactingToHit = false;
        m_forceUpdateAnim = true;
        m_invisible = true;
        m_invisibleStartTime = m_game.timer;
    }

    public override void kill(Entity p_attacker, Entity p_weapon)
    {
        if (!m_isDead && !m_triggerDeath && m_canDie)
        {
            m_triggerDeath = true;

            if (m_weaponHolder != null)
            {
                m_weaponHolder.terminate();
                m_weaponHolder = null;
                m_powerUpRingEffect.terminate();
            }

            onDeath(p_attacker, p_weapon);
        }
    }

    public override void doPhysics()
    {
        if (m_qDir != DIR_NONE)
        {
            m_game.startedMoving = true;
            m_prevQDir = m_lastDir;
            m_lastDir = m_qDir;
        }

        if ((m_state == STATE_TELEPORTING_OUT && (!m_vel.x.isZero() || !m_vel.y.isZero())) ||
            (m_teleporter != null && m_teleporter.terminated))
        {
            abortTeleport();

            if (m_teleporter && !m_teleporter.terminated)
            {
                tryTerminateTeleporter();
            }
        }
        else if (m_state == STATE_TELEPORTING_IN)
        {
            teleportingDone();
        }

        if (m_weaponHolder != null)
        {
            if (m_fire)
            {
                m_weaponHolder.tryFire();
            }

            if (m_stopFire)
            {
                m_weaponHolder.tryStopFire();
            }

            if (m_lastDir != m_prevQDir)
            {
                m_weaponHolder.onChangeDir();
            }
        }
        else
        {
            m_fire = false;
        }

        //collectible logic moved to 'onTriggerEnter2D' this makes more sense in Unity

        updateProjectedPos();

        if (!m_reactingToHit)
        {
            updateAnim();
        }

        if (m_invisible && (m_game.timer - m_invisibleStartTime > INVISIBILITY_DURATION))
        {
            m_invisible = false;
            m_flicker = false;
        }

        if (!m_reactingToHit && !m_doingTemporaryStop)
        {
            base.doPhysics();
        }

        if (m_doingTemporaryStop && m_game.timer - m_temporaryStopStartTime > TEMPORARY_STOP_DURATION)
        {
            m_doingTemporaryStop = false;
        }

        updateAbsCollisionRect();
    }

    public override void onTriggerEnter2D(Collider2D p_other)
    {
        base.onTriggerEnter2D(p_other);

        EntityCollectible l_collectible = p_other.transform.GetComponent<EntityCollectible>();

        if (l_collectible)
        {
            if (!l_collectible.collected)
            {
                m_score = m_score + l_collectible.score;
                l_collectible.collect();
                onCollectUpdateHUD();
                m_lastCollectedObject = l_collectible;
            }
        }
    }

    public virtual void tryTerminateTeleporter()
    {
        if (m_teleporter is EntityPlayerTeleporter && m_teleporter.getPowerLevel() > 0)
        {
            m_teleporter.terminate();
            m_teleporter = null;
        }
    }

    public virtual void abortTeleport()
    {
        teleportingDone();

        if (m_weaponHolder != null)
        {
            m_weaponHolder.onTeleportCancel();
        }
    }

    public virtual void onCollectUpdateHUD()
    {

    }

    public override void onStoppedAtTile()
    {
        if (m_state != STATE_STOPPING_FOR_VICTORY)
        {
            m_currAnim = ANIM_TELEPORTING;
            m_state = STATE_TELEPORTING_OUT;
            m_teleportOldDir = m_qDir;
            m_qDir = DIR_NONE;
            m_currTeleporterSpin = 0;

            if (m_teleporter && m_teleporter.spinTimes > 0)
            {
                m_animator.Play("spin_in_teleporter");
            }
            else
            {
                m_animator.Play("teleporting_out");
                m_sound.playSound("EnterTeleporter");

                if (m_weaponHolder != null)
                {
                    m_weaponHolder.onTeleport();
                }
            }
        }
        else
        {
            m_animator.Play("victory");
            m_state = STATE_VICTORY;
            m_qDir = DIR_NONE;
        }
    }

    public virtual void teleportFrom(EntityTeleporter p_teleporter, bool p_nextTile = false)
    {
        m_oldVel.x = m_vel.x;
        m_oldVel.y = m_vel.y;
        m_teleporter = p_teleporter;
        m_state = STATE_STARTED_TELEPORTING;

        if (!p_nextTile)
        {
            stopAtTile(m_teleporter.tx, m_teleporter.ty);
        }
        else
        {
            stopAtNextTile();
        }
    }

    public virtual bool isTeleporting()
    {
        return m_state == STATE_STARTED_TELEPORTING || m_state == STATE_TELEPORTING_OUT || m_state == STATE_TELEPORTING_IN;
    }

    public virtual void teleportIn()
    {
        m_animator.Play("teleporting_in");
        m_sound.playSound("ExitTeleporter");
        m_state = STATE_TELEPORTING_IN;

        if (m_vel.x.isZero() && m_vel.y.isZero())
        {
            m_tx = m_teleporter.twin.tx;
            m_ty = m_teleporter.twin.ty;
        }

        updatePosFromTile();

        if (m_weaponHolder != null)
        {
            m_weaponHolder.onTeleportDone();
        }

        if (m_teleporter is EntityPlayerTeleporter)
        {
            m_teleporter.terminate();
            m_teleporter = null;
        }
    }

    public virtual void spinDone()
    {
        if (!m_teleporter)
        {
            return;
        }

        m_currTeleporterSpin++;

        if (m_teleporter.spinTimes == EntityTeleporter.INFINITE_SPIN_TIMES || m_currTeleporterSpin < m_teleporter.spinTimes)
        {
            m_animator.Play("spin_in_teleporter" + m_animModifier);
        }
        else
        {
            m_animator.Play("teleporting_out" + m_animModifier);
            m_sound.playSound("EnterTeleporter");
        }

        if (m_teleporter.spinTimes != EntityTeleporter.INFINITE_SPIN_TIMES && m_weaponHolder != null)
        {
            m_weaponHolder.onTeleport();
        }
    }

    public virtual void teleportingDone()
    {
        playAnimation(ANIM_WALK_DOWN);
        m_state = STATE_NORMAL;

        if (m_vel.x.isZero() && m_vel.y.isZero())
        {
            m_vel.x = m_oldVel.x;
            m_vel.y = m_oldVel.y;
            m_qDir = m_teleportOldDir;
            m_lastDir = m_qDir;
        }
    }


    protected void playAnimation(int p_currAnim)
    {
        string l_currAnim = getAnimationFromIndex(p_currAnim);
        m_animator.Play(l_currAnim);
    }

    protected string getAnimationFromIndex(int p_currAnim)
    {
        string currAnim = string.Empty;

        switch (p_currAnim)
        {
            case ANIM_WALK_DOWN:
                currAnim = "walk_up";
                break;
            case ANIM_WALK_UP:
                currAnim = "walk_down";
                break;
            case ANIM_WALK_LEFT:
                currAnim = "walk_side";
                break;
            case ANIM_WALK_RIGHT:
                currAnim = "walk_side";
                break;
        }

        return currAnim + m_animModifier;
    }
}
