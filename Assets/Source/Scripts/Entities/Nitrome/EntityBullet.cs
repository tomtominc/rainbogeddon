﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EntityBullet : EntityCentered
{
    public const float SPEED = 14.5f;
    public const int DAMAGE = 10;

    public int m_damage;
    public bool m_wrapped;
    public int m_wrapTimes;
    public Entity m_parent;
    public int[] m_damageTable;
    private int m_powerLevel;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, 0, 0);
        m_speed = SPEED;
        m_collisionRect.x = -5;
        m_collisionRect.y = -5;
        m_collisionRect.width = 10;
        m_collisionRect.height = 10;

        m_damage = DAMAGE;
        m_wrapped = false;
        m_parent = null;
        m_wrapTimes = 0;
        m_canDie = false;


        //m_spatialHashMap.updateObject(this);
    }

    public virtual void setVelocity(float p_vx, float p_vy)
    {
        m_vel.x = p_vx;
        m_vel.y = p_vy;
        m_vel = m_vel.normalized;
        m_vel = m_vel * SPEED;
    }

    public virtual void setPowerLevel(int p_powerLevel)
    {
        m_powerLevel = p_powerLevel;
        m_damageTable = new int[4];
        m_damageTable[1] = 10;
        m_damageTable[2] = 20;
        m_damageTable[3] = 30;
        m_damage = m_damageTable[m_powerLevel];
        gotoAndPlay("level_" + m_powerLevel);

        m_sound.playSound(fireSoundID);
    }



    public override void doPhysics()
    {

        var ignoreList = new List<Type>();
        ignoreList.Add(typeof(EntityCollectible));
        ignoreList.Add(typeof(EntityCoin));
        ignoreList.Add(typeof(EntityBullet));

        Entity collidingObject = null;

        List<GO> collidingObjects = getCollidingObjects();

        for (int i = 0; i < collidingObjects.Count; i++)
        {
            if (collidingObjects[i].GetType() == typeof(EntityBlock))
            {
                Debug.Log("collided with block");
            }

            if (!ignoreList.Contains(collidingObjects[i].GetType()))
            {
                collidingObject = (Entity)collidingObjects[i];
                break;
            }
        }

        if (collidingObject)
        {
            Debug.LogFormat("Colliding Object Name = {0}", collidingObject.name);
            collidingObject.doDamage(m_damage, m_vel, m_parent, this);
            m_terminated = true;
            //m_tileMap.remove(Level.ENTITY_LAYER, m_tx, m_ty);

            if (collidingObject is EntityHardBlock)
            {
                m_sound.playSound("BulletHitHardWall");
            }
            else if (collidingObject is EntityEnemy)
            {
                m_sound.playSound("EnemyHitByBullet");
            }
            else
            {
                m_sound.playSound("BulletHitWall");

            }

            BulletImpactEffect bulletImpactEffect = Game.g.createObject<BulletImpactEffect>("BulletImpacts", effect, m_tx, m_ty);
            //bulletImpactEffect.x = x + m_vel.x;
            //bulletImpactEffect.y = y + m_vel.y;
            bulletImpactEffect.setPowerLevel(m_powerLevel);
            m_tileMap.reformLayer(Level.ENTITY_LAYER);

            return;

        }
        updatePos();

        if (m_vel.x > 0)
        {
            transform.eulerAngles = Vector3.forward * 180f;
        }
        else if (m_vel.x < 0)
        {
            transform.eulerAngles = Vector3.forward * 0f;
        }
        else if (m_vel.y < 0)
        {
            transform.eulerAngles = Vector3.forward * 90f;
        }
        else if (m_vel.y > 0)
        {
            transform.eulerAngles = Vector3.forward * 270f;
        }
    }

    public virtual string fireSoundID
    {
        get { return string.Empty; }
    }

    public virtual string effect
    {
        get { return string.Empty; }
    }

    public override void onWrapped()
    {
        m_wrapped = true;
        m_wrapTimes++;
        m_drawWrapped = false;

        if (m_wrapTimes >= 2)
        {
            m_terminated = true;
            base.onDeath(null, null);
        }

    }
}
