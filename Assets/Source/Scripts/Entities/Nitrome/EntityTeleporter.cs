﻿using UnityEngine;

public class EntityTeleporter : EntityCentered
{
    public const int INFINITE_SPIN_TIMES = int.MaxValue;

    [SerializeField]
    protected GameObject m_disappearEffectPrefab;

    public bool m_canDisable;
    protected EntityTeleporter m_twin;
    protected int m_powerLevel;
    protected int m_spinTimes;
    protected int[] m_powerToSpinTimesTable;
    protected bool m_firstCollisionP1CheckPerformed;
    protected bool m_firstCollisionP2CheckPerformed;
    protected bool m_waitingPlayer1UnCollide;
    protected bool m_waitingPlayer2UnCollide;

    public int spinTimes
    {
        get { return m_spinTimes; }
    }

    public EntityTeleporter twin
    {
        get { return m_twin; }
        set { m_twin = value; }
    }

    public bool firstCollisionP1CheckPerformed
    {
        get { return m_firstCollisionP1CheckPerformed; }
        set { m_firstCollisionP1CheckPerformed = value; }
    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_collisionRect.x = -12.5f;
        m_collisionRect.y = -12.5f;
        m_collisionRect.width = 25 * ApplicationController.PIXEL_SIZE;
        m_collisionRect.height = 25 * ApplicationController.PIXEL_SIZE;
        m_canDie = false;
        m_canDisable = true;
        m_powerToSpinTimesTable = new int[4];
        m_powerToSpinTimesTable[0] = INFINITE_SPIN_TIMES;
        m_powerToSpinTimesTable[1] = 2;
        m_powerToSpinTimesTable[2] = 1;
        m_powerToSpinTimesTable[3] = 0;
        m_firstCollisionP1CheckPerformed = false;
        m_firstCollisionP2CheckPerformed = false;
        m_waitingPlayer1UnCollide = false;
        m_waitingPlayer2UnCollide = false;
    }

    public virtual void setPowerLevel(int p_powerLevel)
    {
        m_powerLevel = p_powerLevel;
        m_spinTimes = m_powerToSpinTimesTable[p_powerLevel];
    }

    public virtual int getPowerLevel()
    {
        return m_powerLevel;
    }

    public virtual GameObject disappearEffect()
    {
        return m_disappearEffectPrefab;
    }

    public virtual GameObject spawnEffect(GameObject p_effectPrefab)
    {
        GameObject l_effect = Instantiate(p_effectPrefab);
        l_effect.transform.position = new Vector3(x, y);
        return l_effect;
    }

    public virtual void terminate()
    {
        m_powerLevel = 0;
        m_terminated = true;

        if (m_twin)
        {
            m_twin.m_twin = null;
        }

        spawnEffect(disappearEffect());
        m_tileMap.remove(Level.POWERUP_LAYER, m_tx, m_ty);
    }
}

