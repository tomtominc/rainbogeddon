﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpHeart : PowerUp
{
    private HeartHolder m_heartHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_heartHolder = new HeartHolder(m_game.player1);
    }

    public override string soundID()
    {
        return "Heart";
    }

    public override string displayName()
    {
        return "HEART";
    }

    public override WeaponHolder holder()
    {
        return m_heartHolder;
    }

    public override WeaponHolder getHolder(EntityCharacter p_parent)
    {
        return new HeartHolder(p_parent);
    }
}
