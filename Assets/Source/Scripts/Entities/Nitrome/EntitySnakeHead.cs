﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySnakeHead : EntityEnemy
{
    public EntitySnakeTail m_tail;
    public List<EntitySnakeBody> m_bodySegmentList;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_glow.m_color = 0xffff3503;
        m_followsTarget = false;
        m_checkDirMaxTimeout = MIN_CHECK_DIR_TIMEOUT;
        m_checkDirTimeout = m_checkDirMaxTimeout;

        for (int i = 0; i < m_bodySegmentList.Count; i++)
        {
            EntitySnakeBody body = m_bodySegmentList[i];

            body.initialize(m_controller, p_tx, p_ty);

            if (i == 0)
            {
                body.setLeader(this, this);
            }
            else
            {
                body.setLeader(m_bodySegmentList[i - 1], this);
            }

            Game.g.addGO(body);
        }

        m_tail.initialize(m_controller, p_tx, p_ty);
        m_tail.setLeader(m_bodySegmentList[m_bodySegmentList.Count - 1], this);
        Game.g.addGO(m_tail);
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        for (int i = 0; i < m_bodySegmentList.Count; i++)
        {
            if (!m_bodySegmentList[i].m_terminated)
            {
                m_bodySegmentList[i].onDeath(p_attacker, p_weapon);
            }
        }

        if (!m_tail.m_terminated)
        {
            m_tail.onDeath(p_attacker, p_weapon);
        }
    }


}
