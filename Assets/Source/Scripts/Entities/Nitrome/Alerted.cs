﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alerted : MonoBehaviour
{
    public const int STATE_OFF = 0;
    public const int STATE_FLASHING = 1;

    protected Entity m_parent;
    protected int m_state;
    protected bool m_canFlash;
    protected Animator m_animator;

    public void initialize(Entity p_parent)
    {
        m_state = STATE_OFF;
        m_parent = p_parent;
        m_canFlash = true;
        m_animator = GetComponent<Animator>();
    }

    public virtual void flash()
    {
        if (m_canFlash)
        {
            m_canFlash = false;
            m_state = STATE_FLASHING;
            m_animator.Play("flash");
        }
    }

    public virtual void stopFlashing()
    {
        m_state = STATE_OFF;
        m_animator.Play("off");
    }

}
