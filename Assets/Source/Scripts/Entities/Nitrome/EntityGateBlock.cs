﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Gamelogic.Grids2;

public class EntityGateBlock : EntityBlock
{
    private Text m_text;
    private int m_count;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_text = GetComponentInChildren<Text>();

        Debug.Log(position);
    }

    public override void reform()
    {
        //base.reform();
    }

    private void updateText()
    {
        m_text.text = m_count.ToString();
    }

    private void onCoinCollected(EntityCoin p_coin)
    {
        m_count--;

        updateText();

        if (m_count <= 0)
        {
            kill(null, null);

            m_tileMap.remove(Level.ENTITY_LAYER, m_tx, m_ty);
            m_tileMap.reformLayer(Level.ENTITY_LAYER);

            SoundController.instance.playSound("OpenDoor");
            EntityCoin.removeCollectedListener(onCoinCollected);
            Game.g.m_tileMap.onMapChanged();
        }
    }

    public override void setXMLProperty(NitromeXMLParam p_param)
    {
        int count;

        if (int.TryParse(p_param.property, out count))
        {
            m_count = count;
        }

        updateText();

        EntityCoin.addCollectedListener(onCoinCollected);
    }
}
