﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : EntityCentered
{
    public bool m_enabled;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_collisionRect.x = 0;
        m_collisionRect.y = 0;
        m_collisionRect.width = 25 * ApplicationController.PIXEL_SIZE;
        m_collisionRect.height = 25 * ApplicationController.PIXEL_SIZE;
        m_canDie = false;
        m_enabled = true;
    }

    public override void doPhysics()
    {
        if (!m_enabled)
        {
            return;
        }

        bool apply = false;
        EntityPlayer player = null;

        if (!m_tileMap.player1.terminated && collidesWithObject(m_tileMap.player1))
        {
            apply = true;
            player = m_tileMap.player1;
        }

        if (apply)
        {
            collect();
            player.applyPowerUp(this);
        }
    }

    public virtual void onAppeared()
    {
        m_animator.Play("idle");
    }

    public virtual void warnTermination()
    {
        m_flicker = true;
    }

    public virtual string soundID()
    {
        return string.Empty;
    }

    public virtual string displayName()
    {
        return string.Empty;
    }

    public virtual void collect()
    {
        m_flicker = false;
        m_enabled = false;

        if (m_animator != null)
        {
            m_animator.Play("collect");
        }
    }

    public virtual void terminate()
    {
        m_terminated = true;
        m_tileMap.remove(Level.POWERUP_LAYER, m_tx, m_ty);
        m_draw = false;
    }

    public virtual WeaponHolder holder()
    {
        return null;
    }

    public virtual WeaponHolder getHolder(EntityCharacter p_parent)
    {
        return new WeaponHolder(p_parent);
    }
}
