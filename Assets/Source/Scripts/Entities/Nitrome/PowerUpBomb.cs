﻿using UnityEngine;

public class PowerUpBomb : PowerUp
{
    private BombHolder m_bombHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_bombHolder = new BombHolder(m_game.player1);
    }
    public override string soundID()
    {
        return "Bomb";
    }

    public override string displayName()
    {
        return "BOMB";
    }

    public override WeaponHolder holder()
    {
        return m_bombHolder;
    }

    public override WeaponHolder getHolder(EntityCharacter p_parent)
    {
        return new BombHolder(p_parent);
    }
}
