﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBubbleParticle : Entity
{
    public static TileOutliner m_tileOutliner;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_drawFast = true;
        m_canDie = false;
        m_collisionRect.x = 0;
        m_collisionRect.y = 0;
        m_collisionRect.width = 25;
        m_collisionRect.height = 25;

        if (m_tileOutliner == null)
        {
            m_tileOutliner = new TileOutliner();
            m_tileOutliner.m_level = m_game.m_level;
            m_tileOutliner.m_tileMap = Level.PARTICLE_LAYER;
            m_tileOutliner.m_mapWidth = Game.g.mapWidth;
            m_tileOutliner.m_mapHeight = Game.g.mapHeight;
            m_tileOutliner.m_emptyTileValue = PathMap.TILE_SENTINEL;
        }

    }

    public static void free()
    {
        m_tileOutliner = null;
    }

    public void terminate()
    {
        m_terminated = true;
    }

    public override void doPhysics()
    {
        //int contourID = m_tileOutliner.getContourID(typeof(EntityBubbleParticle), (int)m_tx, (int)m_ty);
        //gotoAndStop("t_" + contourID);

        //if (contourID == 255)
        //{
        //    m_draw = false;
        //}
        //else
        //{
        //    m_draw = true;
        //}

        //Debug.Log(contourID);
    }
}
