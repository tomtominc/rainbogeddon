﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NitromeTileMap
{
    public int m_width;
    public int m_height;
    public List<int> m_tileMap;
    public bool m_active;
    public bool m_changed;
    public int m_changeNotifyTimes;

    public NitromeTileMap(int width_, int height_)
    {
        m_tileMap = new List<int>(new int[width_ * height_]);
        m_width = width_;
        m_height = height_;
        m_active = true;
        m_changed = false;
        m_changeNotifyTimes = 0;

    }// end public function PathMap(width_:uint, height_:uint)

    public void fillMap(List<GO> gameObjectList_)
    {

        m_tileMap = new List<int>(new int[m_width * m_height]);

        for (int currTile = 0; currTile < m_tileMap.Count; currTile++)
        {
            m_tileMap[currTile] = PathMap.TILE_SENTINEL;
        }// end for             

        for (int currGameObj = 0; currGameObj < gameObjectList_.Count; currGameObj++)
        {
            int index = gameObjectList_[currGameObj].ty * m_width + gameObjectList_[currGameObj].tx;

            if (index >= m_tileMap.Count)
            {
                Debug.LogFormat("Index {0} Count {1}", index, m_tileMap.Count);
                continue;
            }


            if (m_tileMap[index] == PathMap.TILE_SENTINEL)
            {
                m_tileMap[gameObjectList_[currGameObj].ty * m_width + gameObjectList_[currGameObj].tx] = gameObjectList_[currGameObj] is EntityBlock ? PathMap.TILE_BLOCKED : PathMap.TILE_SENTINEL;
            }// end if

        }// end for         

    }// end public function fillMap(gameObjectList_:Vector.<GameObject>)

    public void tick()
    {

        if (m_changed && m_changeNotifyTimes++ > 4)
        {
            m_changed = false;
            m_changeNotifyTimes = 0;
        }// end if

    }// end public function tick():void         

    public void onMapChanged()
    {

        m_changed = true;
        m_changeNotifyTimes = 0;

    }// end public function onMapChanged():void             

    public bool canGoToTile(int toX_, int toY_, bool canWrap_ = true)
    {
        int finalToX = toX_;
        int finalToY = toY_;

        if (finalToX < 0)
        {
            if (canWrap_)
            {
                finalToX = m_width - 1;
            }
            else
            {
                return false;
            }// end else
        }
        else if (finalToX > m_width - 1)
        {
            if (canWrap_)
            {
                finalToX = 0;
            }
            else
            {
                return false;
            }// end else
        }// end else

        if (finalToY < 0)
        {
            if (canWrap_)
            {
                finalToY = m_height - 1;
            }
            else
            {
                return false;
            }// end else
        }
        else if (finalToY > m_height - 1)
        {
            if (canWrap_)
            {
                finalToY = 0;
            }
            else
            {
                return false;
            }// end else
        }// end else            

        if (m_tileMap[(finalToY) * m_width + finalToX] == PathMap.TILE_SENTINEL)
        {

            return true;

        }// end if

        return false;

    }// end public function canGoToTile(toX_:int, toY_:int):Boolean     

    public bool canGoUp(int fromX_, int fromY_, bool canWrap_ = true)
    {
        var finalY = fromY_ - 1;
        var finalX = fromX_;

        if (fromY_ == 0)
        {
            if (canWrap_)
            {
                finalY = m_height - 1;
            }
            else
            {
                return false;
            }// end else
        }// end if

        if (finalX > m_width - 1)
        {
            finalX = 0;
        }// end if          

        if (finalX < 0)
        {
            finalX = m_width - 1;
        }// end if          

        if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
        {

            return true;

        }// end if

        return false;

    }// end public function canGoUp(fromX_:uint, fromY_:uint):Boolean

    public bool canGoDown(int fromX_, int fromY_, bool canWrap_ = true)
    {
        var finalY = fromY_ + 1;
        var finalX = fromX_;

        if (fromY_ == m_height - 1)
        {
            if (canWrap_)
            {
                finalY = 0;
            }
            else
            {
                return false;
            }// end else
        }// end if

        if (finalX > m_width - 1)
        {
            finalX = 0;
        }// end if          

        if (finalX < 0)
        {
            finalX = m_width - 1;
        }// end if              

        if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
        {

            return true;

        }// end if

        return false;

    }// end public function canGoUp(fromX_:uint, fromY_:uint):Boolean       

    public bool canGoLeft(int fromX_, int fromY_, bool canWrap_ = true)
    {
        var finalX = fromX_ - 1;
        var finalY = fromY_;

        if (fromX_ == 0)
        {
            if (canWrap_)
            {
                finalX = m_width - 1;
            }
            else
            {
                return false;
            }// end else
        }// end if

        if (finalY > m_height - 1)
        {
            finalY = 0;
        }// end if          

        if (finalY < 0)
        {
            finalY = m_height - 1;
        }// end if          

        if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
        {

            return true;

        }// end if

        return false;

    }// end public function canGoLeft(fromX_:uint, fromY_:uint):Boolean         

    public bool canGoRight(int fromX_, int fromY_, bool canWrap_ = true)
    {
        var finalX = fromX_ + 1;
        var finalY = fromY_;

        if (fromX_ == m_width - 1)
        {
            if (canWrap_)
            {
                finalX = 0;
            }
            else
            {
                return false;
            }// end else
        }// end if

        if (finalY > m_height - 1)
        {
            finalY = 0;
        }// end if          

        if (finalY < 0)
        {
            finalY = m_height - 1;
        }// end if          

        if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
        {

            return true;

        }// end if

        return false;

    }// end public function canGoRight(fromX_:uint, fromY_:uint):Boolean        
}
