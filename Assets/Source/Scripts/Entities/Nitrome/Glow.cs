﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class Glow : GO
{
    public const int SIZE = 3;

    public List<BackgroundTile> m_tileMapBitmap;
    public RadialPathMap m_radialPathMap;
    public uint m_color;
    public List<GridPoint2> m_affectedTilePosList;
    public Entity m_parent;
    public bool m_ranFirstTime;
    public bool m_hasColorShift;
    private float m_colorShiftSpeed;
    private uint m_colorFrom;
    private uint m_colorTo;
    private float m_colorShiftPos;
    private bool m_shiftingColorRight;
    private List<int> m_colorShiftList;
    private static int m_radius;
    private static Rect m_glowBitmapBounds;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_ranFirstTime = false;
        m_hasColorShift = false;
        m_colorShiftPos = 0;

    }

    public virtual void setValues(Entity p_parent, uint p_color)
    {
        m_parent = p_parent;
        m_color = p_color;
    }
    public virtual void terminate()
    {
        m_terminated = true;

        if (m_tileMapBitmap != null && m_tileMapBitmap.Count > 0)
        {
            for (int i = 0; i < m_tileMapBitmap.Count; i++)
            {
                m_tileMapBitmap[i].setGlow(Color.black);
            }

            m_tileMapBitmap.Clear();
        }

    }

    public void ShiftColor(uint colorTo_, float speed_)
    {
        m_hasColorShift = true;
        m_colorShiftSpeed = speed_;
        m_colorTo = colorTo_;
        m_colorFrom = m_color;
        m_colorShiftPos = 0;
        m_shiftingColorRight = true;
    }

    public void StopShiftColor()
    {
        m_hasColorShift = false;
        m_color = m_colorFrom;
    }

    public override void doPhysics()
    {
        if (m_terminated)
            return;

        if ((m_tx != m_parent.tx) || (m_ty != m_parent.ty) || !m_ranFirstTime || m_game.m_tileMap.m_changed)
        {
            m_ranFirstTime = true;
            m_tx = m_parent.tx;
            m_ty = m_parent.ty;

            generate();
        }// end if

        if (m_hasColorShift)
        {

            m_color = Color32.Lerp(m_colorFrom.uintToColor(), m_colorTo.uintToColor(), m_colorShiftPos).ColorToUint();

            if (m_shiftingColorRight)
            {
                m_colorShiftPos += m_colorShiftSpeed;

                if (m_colorShiftPos >= 1)
                {
                    m_colorShiftPos = 1.0f;
                    m_shiftingColorRight = false;
                }// end if

            }
            else
            {
                m_colorShiftPos -= m_colorShiftSpeed;

                if (m_colorShiftPos < 0)
                {
                    m_colorShiftPos = 0;
                    m_shiftingColorRight = true;
                }

            }
        }
    }

    public virtual void generate()
    {

        if (m_tileMapBitmap != null && m_tileMapBitmap.Count > 0)
        {
            for (int i = 0; i < m_tileMapBitmap.Count; i++)
            {
                m_tileMapBitmap[i].setGlow(Color.black);
            }

            m_tileMapBitmap.Clear();
        }

        m_affectedTilePosList = m_tileMap.getGlow(m_tx, m_ty);

        for (int i = 0; i < m_affectedTilePosList.Count; i++)
        {
            GO go = m_tileMap.getEntity(Level.BACKGROUND_LAYER, m_affectedTilePosList[i]);
            BackgroundTile backgroundTile = go as BackgroundTile;

            if (backgroundTile != null)
            {
                m_tileMapBitmap.Add(backgroundTile);
            }
        }
    }


    public override void draw()
    {
        if (!m_ranFirstTime)
        {
            return;
        }

        float currAlphaFactor = 1.0f;
        uint color;
        uint mapColor;
        uint mapColorAlpha;
        uint thisAlpha;

        uint totalAlpha;
        uint totalR;
        uint totalG;
        uint totalB;

        uint thisR;
        uint thisG;
        uint thisB;

        uint mapColorR;
        uint mapColorG;
        uint mapColorB;

        thisR = (m_color & 0x00ff0000) >> 16;
        thisG = (m_color & 0x0000ff00) >> 8;
        thisB = m_color & 0x000000ff;

        for (var currLayer = 0; currLayer < m_tileMapBitmap.Count; currLayer++)
        {

            thisAlpha = (uint)(0xff * currAlphaFactor);
            color = m_color & 0x00ffffff;


            mapColor = m_tileMapBitmap[currLayer].getGlow().ColorToUint();
            mapColorAlpha = ((mapColor & 0xff000000) >> 24) & 0x000000ff;

            totalAlpha = thisAlpha;// System.Math.Max(thisAlpha, mapColorAlpha);

            Color32 glowColor = ((thisAlpha << 24) + (thisR << 16) + (thisG << 8) + thisB).uintToColor();

            if (mapColor == 0)
            {
                m_tileMapBitmap[currLayer].setGlow(glowColor);
            }
            else
            {
                uint interpColor = Color32.Lerp(glowColor, mapColor.uintToColor(), 0.5f).ColorToUint();
                interpColor &= 0x00ffffff;
                interpColor += totalAlpha << 24;

                m_tileMapBitmap[currLayer].setGlow(interpColor.uintToColor());
            }

            currAlphaFactor *= 0.85f;

        }
    }
}
