﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpRingEffect : MonoBehaviour
{
    private void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    public void setPowerLevel(int p_powerLevel)
    {
        string l_animation = string.Format("level_{0}", p_powerLevel);
        m_animator.Play(l_animation);
    }

    public void terminate()
    {
        setPowerLevel(0);
    }

    private Animator m_animator;
}
