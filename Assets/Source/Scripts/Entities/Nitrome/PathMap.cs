﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMap
{
    public const int TILE_SENTINEL = int.MaxValue - 1;
    public const int TILE_BLOCKED = int.MaxValue;

    public List<int> m_map;
    public int m_width;
    public int m_height;
    public List<PathMapStackArg> m_stack;
    public List<PathMapStackArg> m_stackArgPool;
    public int m_stackPoolPointer;
    public List<int> m_tileMap;
    public int m_distanceCap;
    public bool m_wraps;

    public bool wraps
    {
        get { return m_wraps; }
        set { m_wraps = value; }
    }


    public PathMap(int p_width, int p_height)
    {
        m_width = p_width;
        m_height = p_height;

        m_map = new List<int>(new int[m_width * m_height]);
        m_tileMap = new List<int>();
        m_stack = new List<PathMapStackArg>();
        m_stackArgPool = new List<PathMapStackArg>();

        for (int currArg = 0; currArg < (m_map.Count * 2); currArg++)
        {
            m_stackArgPool.Add(new PathMapStackArg(0, 0, 0));
        }
        m_stackPoolPointer = 0;
        m_distanceCap = TILE_SENTINEL - 1;
        m_wraps = true;
    }

    public PathMapStackArg newPathMapStackArg(int p_x, int p_y, int p_dist)
    {
        PathMapStackArg arg = m_stackArgPool[m_stackPoolPointer];
        arg.x = p_x;
        arg.y = p_y;
        arg.dist = p_dist;
        m_stackPoolPointer++;
        return arg;
    }

    public List<PointUint> getMarkedPathPosList()
    {
        int posX = 0;
        int posY = 0;

        List<PointUint> posList = new List<PointUint>();

        for (int currTile = 0; currTile < m_map.Count; currTile++)
        {
            posY = currTile / m_width;
            posX = currTile - (posY * m_width);

            if (m_map[currTile] <= m_distanceCap)
            {
                posList.Add(new PointUint(posX, posY));
            }
        }

        return posList;
    }

    public int getDistFromTarget(int p_tileX, int p_tileY)
    {
        int currPosOffset = (m_width * p_tileY) + p_tileX;

        //Debug.LogFormat("Index {0} Count {1}", currPosOffset, m_map.Count);
        int cellValue = m_map[currPosOffset];

        if (cellValue == TILE_BLOCKED || cellValue == TILE_SENTINEL || p_tileX < 0
            || p_tileX > m_width - 1 || p_tileY < 0 || p_tileY > m_height - 1)
        {
            return TILE_BLOCKED;
        }

        return cellValue;
    }

    public int getNextGoalDir(int p_tileX, int p_tileY)
    {
        int currPosOffset = (m_width * p_tileY) + p_tileX;
        int cellValue = m_map[currPosOffset];

        //  Debug.LogFormat("pos off {0} value {1}", currPosOffset, cellValue);

        if (cellValue == TILE_BLOCKED || cellValue == TILE_SENTINEL || p_tileX < 0
           || p_tileX > m_width - 1 || p_tileY < 0 || p_tileY > m_height - 1)
        {
            return Entity.DIR_NONE;
        }

        int currDist = cellValue;

        int cellLeft = 0;
        int cellRight = 0;
        int cellTop = 0;
        int cellBottom = 0;

        int finalX = 0;
        int finalY = 0;

        //get left

        finalX = p_tileX - 1;
        finalY = p_tileY;

        if (finalX < 0)
        {
            if (m_wraps)
            {
                finalX = m_width - 1;
                cellLeft = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellLeft = TILE_SENTINEL;
            }// end else
        }
        else
        {
            cellLeft = m_map[(m_width * finalY) + finalX];
        }// end else

        // get right

        finalX = p_tileX + 1;
        finalY = p_tileY;

        if (finalX > m_width - 1)
        {
            if (m_wraps)
            {
                finalX = 0;
                cellRight = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellRight = TILE_SENTINEL;
            }// end else
        }
        else
        {
            cellRight = m_map[(m_width * finalY) + finalX];
        }// end else

        // get top

        finalX = p_tileX;
        finalY = p_tileY - 1;

        if (finalY < 0)
        {
            if (m_wraps)
            {
                finalY = m_height - 1;
                cellTop = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellTop = TILE_SENTINEL;
            }// end else
        }
        else
        {
            cellTop = m_map[(m_width * finalY) + finalX];
        }// end else

        // get bottom

        finalX = p_tileX;
        finalY = p_tileY + 1;

        if (finalY > m_height - 1)
        {
            if (m_wraps)
            {
                finalY = 0;
                cellBottom = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellBottom = TILE_SENTINEL;
            }// end else
        }
        else
        {
            cellBottom = m_map[(m_width * finalY) + finalX];
        }// end else


        int lowestDist = cellLeft;
        int lowestDir = Entity.DIR_LEFT;

        int choice = Random.Range(0, 2);

        if (cellRight < lowestDist)
        {
            lowestDist = cellRight;
            lowestDir = Entity.DIR_RIGHT;
        }
        else if ((cellRight == lowestDist) && choice > 0)
        {
            lowestDist = cellRight;
            lowestDir = Entity.DIR_RIGHT;
        }// end else


        if (cellTop < lowestDist)
        {
            lowestDist = cellTop;
            lowestDir = Entity.DIR_DOWN;
        }
        else if ((cellTop == lowestDist) && choice > 0)
        {
            lowestDist = cellTop;
            lowestDir = Entity.DIR_DOWN;
        }// end else    


        if (cellBottom < lowestDist)
        {
            lowestDist = cellBottom;
            lowestDir = Entity.DIR_UP;
        }
        else if ((cellBottom == lowestDist) && choice > 0)
        {
            lowestDist = cellBottom;
            lowestDir = Entity.DIR_UP;
        }// end else            


        //if (lowestDist <= currDist)
        //{
        return lowestDir;
        /*}
        else
        {
            return Entity.DIR_NONE;
        }// end else*/

    }

    public int getNextEvasionDir(int tileX_, int tileY_)
    {

        int currPosOffset = (m_width * tileY_) + tileX_;
        int cellValue = m_map[currPosOffset];

        if ((cellValue == TILE_BLOCKED) || (cellValue == TILE_SENTINEL) || (tileX_ < 0) || (tileX_ > m_width - 1) || (tileY_ < 0) || (tileY_ > m_height - 1))
        {

            return Entity.DIR_NONE;

        }// end if

        int currDist = cellValue;

        int cellLeft;
        int cellRight;
        int cellTop;
        int cellBottom;

        int finalX;
        int finalY;

        // get left

        finalX = tileX_ - 1;
        finalY = tileY_;

        if (finalX < 0)
        {
            if (m_wraps)
            {
                finalX = m_width - 1;
                cellLeft = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellLeft = 0;
            }// end else
        }
        else
        {
            cellLeft = m_map[(m_width * finalY) + finalX];
        }// end else




        // get right

        finalX = tileX_ + 1;
        finalY = tileY_;

        if (finalX > m_width - 1)
        {
            if (m_wraps)
            {
                finalX = 0;
                cellRight = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellRight = 0;
            }// end else
        }
        else
        {
            cellRight = m_map[(m_width * finalY) + finalX];
        }// end else



        // get top

        finalX = tileX_;
        finalY = tileY_ - 1;

        if (finalY < 0)
        {
            if (m_wraps)
            {
                finalY = m_height - 1;
                cellTop = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellTop = 0;
            }// end else
        }
        else
        {
            cellTop = m_map[(m_width * finalY) + finalX];
        }// end else



        // get bottom

        finalX = tileX_;
        finalY = tileY_ + 1;

        if (finalY > m_height - 1)
        {
            if (m_wraps)
            {
                finalY = 0;
                cellBottom = m_map[(m_width * finalY) + finalX];
            }
            else
            {
                cellBottom = 0;
            }// end else
        }
        else
        {
            cellBottom = m_map[(m_width * finalY) + finalX];
        }// end else




        int highestDist = (cellLeft == TILE_BLOCKED) ? 0 : cellLeft;
        int highestDir = Entity.DIR_LEFT;

        int choice = Random.Range(0, 2);

        if (cellRight != TILE_BLOCKED)
        {

            if (cellRight > highestDist)
            {
                highestDist = cellRight;
                highestDir = Entity.DIR_RIGHT;
            }
            else if ((cellRight == highestDist) && choice > 0)
            {
                highestDist = cellRight;
                highestDir = Entity.DIR_RIGHT;
            }// end else

        }// end if


        if (cellTop != TILE_BLOCKED)
        {

            if (cellTop > highestDist)
            {
                highestDist = cellTop;
                highestDir = Entity.DIR_UP;
            }
            else if ((cellTop == highestDist) && choice > 0)
            {
                highestDist = cellTop;
                highestDir = Entity.DIR_UP;
            }// end else    

        }// end if


        if (cellBottom != TILE_BLOCKED)
        {

            if (cellBottom > highestDist)
            {
                highestDist = cellBottom;
                highestDir = Entity.DIR_DOWN;
            }
            else if ((cellBottom == highestDist) && choice > 0)
            {
                highestDist = cellBottom;
                highestDir = Entity.DIR_DOWN;
            }// end else            

        }// end if

        /*if (highestDist >= currDist)
        {*/
        return highestDir;
        /*}
        else
        {
            return Entity.DIR_NONE;
        //}// end else*/

    }// end public function getNextEvasionDir(tileX_:int, tileY_:int):in

    public void floodFill(int goalX_, int goalY_)
    {
        if ((goalX_ < 0) || (goalY_ < 0) || (goalX_ >= m_width) || (goalY_ >= m_height))
        {
            return;
        }// end if

        int tileY = 0;
        int tileX = 0;

        m_map = new List<int>();
        m_map.AddRange(m_tileMap);

        m_stack.Clear();

        m_stackPoolPointer = 0;
        m_stack.Add(newPathMapStackArg(goalX_, goalY_, 0));

        int distance = 0;

        int offset = (m_width * goalY_) + goalX_;

        if (m_map[offset] == TILE_BLOCKED)
        {
            return;
        }// end if


        int currMarker = 0;

        m_map[offset] = m_stack[currMarker].dist;

        for (currMarker = 0; currMarker < m_stack.Count; currMarker++)
        {

            if (m_stack[currMarker].dist + 1 > m_distanceCap)
            {
                continue;
            }// end if

            offset = (m_width * m_stack[currMarker].y) + m_stack[currMarker].x;

            if ((m_stack[currMarker].x + 1 < m_width))
            {

                offset = (m_width * m_stack[currMarker].y) + m_stack[currMarker].x + 1;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x + 1, m_stack[currMarker].y, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if

            }
            else if (m_wraps)
            {

                offset = (m_width * m_stack[currMarker].y);

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(0, m_stack[currMarker].y, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if                  

            }// end else

            if ((m_stack[currMarker].x - 1 >= 0))
            {

                offset = (m_width * m_stack[currMarker].y) + m_stack[currMarker].x - 1;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x - 1, m_stack[currMarker].y, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if

            }
            else if (m_wraps)
            {

                offset = (m_width * m_stack[currMarker].y) + (m_width - 1);

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_width - 1, m_stack[currMarker].y, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if                  

            }// end else


            if ((m_stack[currMarker].y + 1 < m_height))
            {

                offset = (m_width * (m_stack[currMarker].y + 1)) + m_stack[currMarker].x;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x, m_stack[currMarker].y + 1, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if

            }
            else if (m_wraps)
            {

                offset = m_stack[currMarker].x;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x, 0, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if                  

            }// end else

            if ((m_stack[currMarker].y - 1 >= 0))
            {

                offset = (m_width * (m_stack[currMarker].y - 1)) + m_stack[currMarker].x;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x, m_stack[currMarker].y - 1, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if

            }
            else if (m_wraps)
            {

                offset = (m_width * (m_height - 1)) + m_stack[currMarker].x;

                if (m_map[offset] != TILE_BLOCKED)
                {

                    distance = m_map[offset];

                    if ((distance > (m_stack[currMarker].dist + 1)))
                    {
                        m_map[offset] = m_stack[currMarker].dist + 1;
                        m_stack.Add(newPathMapStackArg(m_stack[currMarker].x, m_height - 1, m_stack[currMarker].dist + 1));
                    }// end if

                }// end if                  

            }// end else


        }// end for


    }// end public function floodFill(goalX_:uint, goalY_:uint) 

}
