﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityTattleTale : EntityEnemy
{
    public const float ALERT_COUNTDOWN_DURATION = SMART_FOLLOW_DURATION;
    public const int ALERT_STATE_NONE = 0;
    public const int ALERT_STATE_ALERTING = 1;
    public const int ALERT_STATE_COUNTDOWN = 2;
    public const int ALERT_STATE_COUNTDOWN_DONE_WAITING = 3;

    public int m_alertState;
    private int m_currAlertTargetPlayerNumber;
    private Entity m_currAlertTarget;
    //private AlertRing m_alertRing;
    //private AlertBackground m_alertBackground;
    //private TargetMarker m_targetMarker;
    private bool m_waitToSignal;
    private bool m_signalSent;
    private bool m_countDownSent;
    public PathMap m_pathMap;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_glow.m_color = 0xffff3503;
        m_recoverWaitDuration = 4000;
        m_checkDirMaxTimeout = MIN_CHECK_DIR_TIMEOUT;
        m_followsTarget = false;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_targetTriggerRange = 3;
        m_hasOnTargetRangeTrigger = true;
        m_canSmartFollow = false;
        m_alertState = ALERT_STATE_NONE;
        //m_alertRing = null;
        //m_alertBackground = null;
        //m_targetMarker = null;
        m_currAlertTarget = null;
        m_waitToSignal = false;
        m_signalSent = false;
        m_countDownSent = false;
        m_hitPoints = 120;
        m_maxHitPoints = m_hitPoints;
        m_pathMap = new PathMap(Game.g.mapWidth, Game.g.mapHeight);
    }

    public override void doPhysics()
    {

        switch (m_alertState)
        {

            case ALERT_STATE_NONE:
                {

                    base.doPhysics();

                    break;
                }// end case                


            default:
                {
                    updateMap();
                    updateTarget();
                    updateTargetRange();
                    updateDamage();

                    int currAlertTargetDist = 0;

                    if (m_currAlertTarget is EntityPlayer1)
                    {
                        currAlertTargetDist = m_player1PathMap.getDistFromTarget((int)m_cTX, (int)m_cTY);
                    }
                    else if (m_currAlertTarget is EntityPlayer2)
                    {
                        currAlertTargetDist = m_player2PathMap.getDistFromTarget((int)m_cTX, (int)m_cTY);
                    }// end else if

                    EntityEnemy enemy;
                    int distFromTarget;
                    int distFromThis;

                    bool playedAlertSound = false;

                    if (m_alertState == ALERT_STATE_COUNTDOWN)
                    {

                        if (Game.g.timer - m_smartFollowCountdownStartTime > ALERT_COUNTDOWN_DURATION)
                        {
                            onAlertCountdownEnded();
                        }
                        else
                        {

                            if ((m_distFromTarget == PathMap.TILE_BLOCKED) || ((m_chosenTarget != m_currAlertTarget) && (currAlertTargetDist == PathMap.TILE_BLOCKED)))
                            {

                                // target is unreachable
                                onAlertCountdownEnded();

                                // stop alert state on enemies
                                for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
                                {
                                    if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                                    {
                                        (Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
                                    }// end if
                                }// end for                                     

                            }
                            else
                            {

                                // broadcast message to all other new enemies to start following
                                // the spotted player
                                for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
                                {
                                    if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                                    {

                                        enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;

                                        distFromThis = m_pathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);

                                        if (m_currAlertTargetPlayerNumber == 1)
                                        {
                                            distFromTarget = m_player1PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                                        }
                                        else
                                        {
                                            distFromTarget = m_player2PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                                        }// end else                                            

                                        if (enemy.m_canSmartFollow && (!enemy.m_smartFollowing || (m_currAlertTargetPlayerNumber != enemy.m_smartFollowingPlayerNumber)) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
                                        {
                                            if (!playedAlertSound)
                                            {
                                                playedAlertSound = true;
                                                //SoundManager.playSound("EnemyAlerted");
                                            }// end if
                                            enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
                                            enemy.startSmartPlayerFollowCountdown();
                                            enemy.m_smartFollowCountdownStartTime = m_smartFollowCountdownStartTime;
                                        }
                                        else if (enemy.m_canSmartFollow && enemy.m_smartFollowing && (distFromTarget == PathMap.TILE_BLOCKED) && (distFromThis == PathMap.TILE_BLOCKED))
                                        {
                                            enemy.stopSmartPlayerFollow();
                                        }// end else if

                                    }// end if
                                }// end for

                            }// end else

                        }// end else

                    }
                    else if ((m_alertState == ALERT_STATE_ALERTING) && m_signalSent)
                    {

                        if ((m_distFromTarget == PathMap.TILE_BLOCKED) || ((m_chosenTarget != m_currAlertTarget) && (currAlertTargetDist == PathMap.TILE_BLOCKED)))
                        {

                            // target is unreachable
                            onAlertCountdownEnded();

                            // stop alert state on enemies
                            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
                            {
                                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                                {
                                    (Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
                                }// end if
                            }// end for                                     

                        }
                        else
                        {

                            playedAlertSound = false;
                            // broadcast message to all other new enemies to start following
                            // the spotted player
                            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
                            {
                                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                                {

                                    enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;

                                    distFromThis = m_pathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);

                                    if (m_currAlertTargetPlayerNumber == 1)
                                    {
                                        distFromTarget = m_player1PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                                    }
                                    else
                                    {
                                        distFromTarget = m_player2PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                                    }// end else                                    

                                    if (enemy.m_canSmartFollow && (!enemy.m_smartFollowing || (m_currAlertTargetPlayerNumber != enemy.m_smartFollowingPlayerNumber)) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
                                    {
                                        if (!playedAlertSound)
                                        {
                                            playedAlertSound = true;
                                            //SoundManager.playSound("EnemyAlerted");
                                        }// end if
                                        enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
                                    }
                                    else if (enemy.m_canSmartFollow && enemy.m_smartFollowing && (distFromTarget == PathMap.TILE_BLOCKED) && (distFromThis == PathMap.TILE_BLOCKED))
                                    {
                                        enemy.stopSmartPlayerFollow();
                                    }// end else if                                 

                                }// end if
                            }// end for     

                        }// end else

                    }// end else if

                    break;
                }// end case

        }// end switch

        m_pathMap.m_tileMap = new List<int>();

        //if (Game.g.m_safetyTileMap.m_active)
        //{
        //    m_pathMap.m_tileMap = m_pathMap.m_tileMap.concat(Game.g.m_safetyTileMap.m_tileMap);
        //}
        //else
        //{
        m_pathMap.m_tileMap.AddRange(Game.g.m_enemyTileMap.m_tileMap);
        //}// end else


        m_pathMap.floodFill((int)m_cTX, (int)m_cTY);

    }// end public override function doPhysics():void

    public override void playSideWalkAnim()
    {
        if (m_alertState == ALERT_STATE_NONE)
        {
            gotoAndPlay("walk_side");
        }// end if
    }// end public override function playSideWalkAnim():void

    public override void playUpWalkAnim()
    {
        if (m_alertState == ALERT_STATE_NONE)
        {
            gotoAndPlay("walk_up");
        }// end if      
    }// end public override function playUpWalkAnim():void      

    public override void playDownWalkAnim()
    {
        if (m_alertState == ALERT_STATE_NONE)
        {
            gotoAndPlay("walk_down");
        }// end if      
    }// end public override function playDownWalkAnim():void            

    public void onAlertCountdownEnded()
    {
        m_alertState = ALERT_STATE_COUNTDOWN_DONE_WAITING;
        //trace("ended countdown");
    }// end public function onAlertCountdownEnded():void

    public void doneStartAlerting()
    {
        //if (!m_alertRing)
        //{
        //    m_alertRing = new AlertRing(this);
        //    Game.g.m_gameObjectList.push(m_alertRing);
        //    m_alertBackground = new AlertBackground();
        //    Game.g.m_gameObjectList.push(m_alertBackground);
        //}// end if          


        if (m_waitToSignal && !m_countDownSent)
        {

            bool playedAlertSound = false;
            m_signalSent = true;
            //trace("alerted enemies after wait");
            // broadcast message to all other enemies to start following
            // the spotted player
            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
            {
                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                {
                    if (!playedAlertSound)
                    {
                        playedAlertSound = true;
                        //SoundManager.playSound("EnemyAlerted");
                    }// end if
                    (Game.g.m_gameObjectList[currObject] as EntityEnemy).startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
                }// end if
            }// end for     

        }// end if          

    }// end public function doneStartAlerting():void        

    public override void startAlert()
    {
        //super.startAlert();

    }// end public override function startAlert():void      

    public override void onTargetInRange(Entity p_target)
    {

        //if (m_alertState != ALERT_STATE_ALERTING)
        //{

        var playerNumber = p_target is EntityPlayer1 ? 1 : 2;
        m_currAlertTargetPlayerNumber = playerNumber;

        //if (p_target != m_currAlertTarget)
        //{
        //    if (m_targetMarker)
        //    {
        //        m_targetMarker.m_terminated = true;
        //    }// end if
        //    m_targetMarker = new TargetMarker(p_target);
        //    Game.g.m_gameObjectList.push(m_targetMarker);
        //    m_currAlertTarget = p_target;
        //}// end if

        m_waitToSignal = false;
        m_signalSent = false;

        if ((m_alertState != ALERT_STATE_COUNTDOWN) && (m_alertState != ALERT_STATE_ALERTING))
        {
            gotoAndPlay("start_alert");
            m_waitToSignal = true;
            //m_alertEffect = new EnemyAlertEffect(this);
            //SoundManager.playSound("EnemyAlerted");
            //Game.g.m_gameObjectList.push(m_alertEffect);
        }// end if

        m_countDownSent = false;
        m_alertState = ALERT_STATE_ALERTING;
        EntityEnemy enemy;
        int distFromTarget;
        int distFromThis;
        //trace("started alert");               

        if (!m_waitToSignal)
        {

            var playedAlertSound = false;
            m_signalSent = true;
            //trace("alerted enemies immediately");
            // broadcast message to all other enemies to start following
            // the spotted player
            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
            {
                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                {

                    enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;

                    distFromThis = m_pathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);

                    if (m_currAlertTargetPlayerNumber == 1)
                    {
                        distFromTarget = m_player1PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                    }
                    else
                    {
                        distFromTarget = m_player2PathMap.getDistFromTarget((int)enemy.m_cTX, (int)enemy.m_cTY);
                    }// end else                                    

                    if ((enemy.m_canSmartFollow && !enemy.m_smartFollowing) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
                    {
                        if (!playedAlertSound)
                        {
                            playedAlertSound = true;
                            //SoundManager.playSound("EnemyAlerted");
                        }// end if
                        enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
                    }// end if

                }// end if
            }// end for     

        }// end if

        //}// end if

    }// end public override function onTargetInRange(target_:Entity):void           

    public void onAlertEnded()
    {
        m_alertState = ALERT_STATE_NONE;
        m_forceUpdateAnim = true;
        //m_alertRing.terminate();
        //m_alertRing = null;
        //m_alertBackground.terminate();
        //m_alertBackground = null;
        m_currAlertTarget = null;
        //m_targetMarker.m_terminated = true;
        //m_targetMarker = null;
        m_countDownSent = false;
        //trace("ended alert animation");
    }// end public function onAlertEnded():void     

    public void checkAlert()
    {
        if (m_alertState != ALERT_STATE_COUNTDOWN_DONE_WAITING)
        {
            gotoAndPlay("alerting");
        }
        else
        {
            gotoAndPlay("end_alert");
        }// end else
    }// end public function checkAlert():void

    public override void updateTargetRange()
    {

        if (!m_hasOnTargetRangeTrigger)
        {
            return;
        }// end if

        if (m_distFromTarget <= m_targetTriggerRange)
        {

            if (!m_targetInRange || ((m_chosenTarget != m_currInRangeTarget) && (m_distFromTarget < m_currInRangeTargetDist)))
            {
                m_currInRangeTarget = m_chosenTarget;
                m_currInRangeTargetDist = m_distFromTarget;
                m_targetInRange = true;


                onTargetInRange(m_chosenTarget);


                whileTargetInRange(m_chosenTarget);
            }
            else
            {


                whileTargetInRange(m_chosenTarget);
            }// end else

            if (m_currInRangeTarget == Game.g.player1)
            {
                m_currInRangeTargetDist = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
            }
            else if (m_currInRangeTarget == Game.g.player2)
            {
                m_currInRangeTargetDist = m_player2PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
            }// end else if             

        }
        else if (m_targetInRange && (m_distFromTarget > m_targetTriggerRange))
        {
            m_currInRangeTarget = null;
            m_targetInRange = false;


            onTargetOutOfRange();
        }// end else if

    }// end public override function updateTargetRange():void       

    public override void onTargetOutOfRange()
    {

        if (m_alertState == ALERT_STATE_ALERTING)
        {

            m_alertState = ALERT_STATE_COUNTDOWN;
            m_smartFollowCountdownStartTime = Game.g.timer;
            m_countDownSent = true;
            //trace("started countdown");
            // start alert countdown on enemies
            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
            {
                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                {
                    (Game.g.m_gameObjectList[currObject] as EntityEnemy).startSmartPlayerFollowCountdown();
                }// end if
            }// end for             

        }// end if

    }// end public override function onTargetOutOfRange():void      


    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        //if (m_alertRing)
        //{
        //    m_alertRing.terminate();
        //}// end if

        //if (m_alertBackground)
        //{
        //    m_alertBackground.terminate();
        //}// end if

        //if (m_targetMarker)
        //{
        //    m_targetMarker.m_terminated = true;
        //}// end if

        if (m_targetInRange)
        {

            // stop alert state on enemies
            for (var currObject = 0; currObject < Game.g.m_gameObjectList.Count; currObject++)
            {
                if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
                {
                    (Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
                }// end if
            }// end for         

        }// end if

    }// end public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void                   

    public override GameObject respawnEffectType()
    {
        // red
        return base.respawnEffectType();
    }

    public override GameObject destroyedEffectType()
    {
        //red
        return base.destroyedEffectType();
    }

}// end public class EntityTattleTale
