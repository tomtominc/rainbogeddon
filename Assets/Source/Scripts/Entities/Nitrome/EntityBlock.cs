﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class EntityBlock : Entity
{
    public string m_path;

    [Space]
    [Header("Layout")]
    public int north;
    public int south;
    public int east;
    public int west;
    public int northeast;
    public int northwest;
    public int southeast;
    public int southwest;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_drawFast = true;
        m_canDie = true;
        m_isDead = false;
        m_moves = false;
        m_hitPoints = 3;

        m_path = "Tiles/SoftBlocks/tile_{0}";
        id = "SoftBlock";

        reform();
    }

    public virtual void reform()
    {
        GridPoint2 point = new GridPoint2(tx, ty);
        north = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X, point.Y + 1)) ? 1 : 0;
        south = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X, point.Y - 1)) ? 1 : 0;
        east = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X + 1, point.Y)) ? 1 : 0;
        west = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X - 1, point.Y)) ? 1 : 0;
        northeast = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X + 1, point.Y + 1)) && north != 0 && east != 0 ? 1 : 0;
        northwest = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X - 1, point.Y + 1)) && north != 0 && west != 0 ? 1 : 0;
        southeast = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X + 1, point.Y - 1)) && south != 0 && east != 0 ? 1 : 0;
        southwest = m_tileMap.hasEntity(Level.ENTITY_LAYER, id, new GridPoint2(point.X - 1, point.Y - 1)) && south != 0 && west != 0 ? 1 : 0;

        int index3 = west + (2 * northwest) + (4 * north) + (8 * northeast) + (16 * east) + (32 * southeast) + (64 * south) + (128 * southwest);

        string path = string.Format(m_path, index3);
        Sprite sprite = Resources.Load<Sprite>(path);

        if (sprite == null)
        {
            Debug.LogErrorFormat("Couldn't find sprite at {0}", path);
            return;
        }
        m_renderer.sprite = sprite;
    }

    public override void doPhysics()
    {

    }

    public override void kill(Entity p_attacker, Entity p_weapon)
    {
        EntityCoin coin = m_game.createObject<EntityCoin>(Level.PILL_LAYER, "NormalCoin", m_tx, m_ty);
        BackgroundTile background = m_game.createObject<BackgroundTile>(Level.BACKGROUND_LAYER, Level.BACKGROUND_ID, m_tx, m_ty);

        base.kill(p_attacker, p_weapon);
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        EntityCoin coin = m_game.createObject<EntityCoin>(Level.PILL_LAYER, "NormalCoin", m_tx, m_ty);
        BackgroundTile background = m_game.createObject<BackgroundTile>(Level.BACKGROUND_LAYER, Level.BACKGROUND_ID, m_tx, m_ty);

        base.onDeath(p_attacker, p_weapon);
    }
}
