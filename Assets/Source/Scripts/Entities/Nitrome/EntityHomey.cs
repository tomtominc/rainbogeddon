﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityHomey : EntityEnemy
{
    private EntityHomeyShooter m_parent;

    public void setParent(EntityHomeyShooter p_parent)
    {
        m_parent = p_parent;
        x = m_parent.x;
        y = m_parent.y;

        m_drawOrder = 1;
        m_respawns = false;

        m_speed = 2.5f;
        m_hitPoints = 0;
        m_maxHitPoints = m_hitPoints;
        m_collisionRect.x = -5;
        m_collisionRect.y = -5;
        m_collisionRect.width = 10;
        m_collisionRect.height = 10;
        m_glow.m_color = 0xffff0000;
        m_recoverWaitDuration = 4f;
        m_checkDirMaxTimeout = 0.25f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_hasLifetime = true;
        m_lifeStartTime = Game.g.timer;
        m_lifetime = 5f;
    }
    public override void onLifetimeEnded()
    {
        onDeath(null, null);
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);
    }

    public override GameObject destroyedEffectType()
    {
        return base.destroyedEffectType();
    }

}// end public class EntityHomey
