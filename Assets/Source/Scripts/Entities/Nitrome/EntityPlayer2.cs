﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer2 : EntityPlayer
{

    public override string deathSoundID()
    {
        return "PlayerDieRip2";
    }

    public override void doInput()
    {
        if (!m_inputEnabled)
            return;

        // input stuff

        int swipeDir = m_inputController.getSwipeDirection();

        if ((swipeDir & SwipeDirection.RIGHT) != 0)
        {
            m_qDir = DIR_RIGHT;
        }
        else if ((swipeDir & SwipeDirection.UP) != 0)
        {
            m_qDir = DIR_UP;
        }
        else if ((swipeDir & SwipeDirection.LEFT) != 0)
        {
            m_qDir = DIR_LEFT;
        }
        else if ((swipeDir & SwipeDirection.DOWN) != 0)
        {
            m_qDir = DIR_DOWN;
        }
        else if ((swipeDir & SwipeDirection.TAP) != 0)
        {
            m_fire = true;
        }

    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);
    }

    public override void onKilledEntity(Entity p_entity)
    {
        if (p_entity is EntityEnemy)
        {
            // m_score = m_score + ((EntityEnemy)p_entity).score;

        }
    }

    public override void teleportIn()
    {
        if (m_teleporter)
        {
            m_teleporter.twin.firstCollisionP1CheckPerformed = false;
        }
        base.teleportIn();
    }
}
