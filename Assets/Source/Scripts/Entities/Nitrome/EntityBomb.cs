﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class EntityBomb : EntityCentered
{

    public const int STATE_WAITING_T0_EXPLODE = 0;
    public const int STATE_EXPLODING = 1;
    public const int STATE_EXPLOSION_FADING = 2;
    public const int STATE_EXPLOSION_OVER = 3;

    public const float EXPLOSION_DURATION = 0.7f;

    public int m_radius;
    public int m_size;
    public bool m_expandLayer;
    public bool m_stoppedClips;
    private int m_powerLevel;
    private bool m_startedAddingTiles;
    public static List<GridPoint2> m_explosionTilePosList;
    public List<ExplosionParticle> m_explosionParticleList;
    public int m_numFadedParticles;
    public int m_numFadedInParticles;
    public float m_explosionStartTime;
    public int m_currExplodingTile;
    public int m_currMainAnimFrameOffset;
    private bool m_currMainAnimFrameOffsetCreated;
    public Entity m_parent;
    public bool m_explodingNormal;
    public bool m_exploded;
    private EntityBombBlock m_block;
    private bool m_addedBlock;

    int explodeX, explodeY;
    public virtual void setValues(Entity p_parent, int p_powerLevel)
    {
        m_parent = p_parent;
        m_powerLevel = p_powerLevel;
        m_exploded = false;

        var powerLevelToSize = new int[4];
        powerLevelToSize[0] = 0;
        powerLevelToSize[1] = 3;
        powerLevelToSize[2] = 4;
        powerLevelToSize[3] = 5;

        m_animator.Play("level_" + m_powerLevel);
        m_size = powerLevelToSize[m_powerLevel];

        m_explosionTilePosList = m_tileMap.getRadiusWrapped(tx, ty, m_size);

        m_collisionRect.x = 1;
        m_collisionRect.y = 1;
        m_collisionRect.width = 23;
        m_collisionRect.height = 23;
        m_expandLayer = false;
        m_stoppedClips = false;
        m_startedAddingTiles = false;
        m_numFadedParticles = 0;
        m_numFadedInParticles = 0;
        m_currExplodingTile = 0;
        m_canDie = true;
        m_currMainAnimFrameOffsetCreated = false;
        m_addedBlock = false;
        m_explosionParticleList = new List<ExplosionParticle>();
        m_state = STATE_WAITING_T0_EXPLODE;
        m_block = null;

        explodeX = tx;
        explodeY = ty;

    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

    }

    public static void free()
    {
        m_explosionTilePosList = null;
    }

    public override void doPhysics()
    {
        if (m_terminated)
            return;

        switch (m_state)
        {
            case STATE_WAITING_T0_EXPLODE:
                {

                    if (!m_addedBlock)
                    {

                        var ignoreList = new List<System.Type>();
                        ignoreList.Add(typeof(EntityCollectible));
                        ignoreList.Add(typeof(EntityBullet));
                        ignoreList.Add(typeof(EntityBubbleParticle));
                        ignoreList.Add(typeof(EntitySafetyBubble));
                        ignoreList.Add(typeof(EntityBomb));
                        ignoreList.Add(typeof(PowerUp));


                        var collidingObject = getCollidingObjects();

                        if (collidingObject == null)
                        {
                            m_addedBlock = true;
                        }

                    }

                    break;
                }

            case STATE_EXPLODING:
                {
                    continueExploding();

                    break;
                }// end case    

            case STATE_EXPLOSION_FADING:
                {

                    m_currMainAnimFrameOffset++;

                    if (m_currMainAnimFrameOffset >= ExplosionParticle.NUM_MAIN_ANIM_LOOP_FRAMES)
                    {
                        m_currMainAnimFrameOffset = 0;
                    }

                    var explosionRect = new Rect();
                    var gameObjectList = new List<GO>();
                    explosionRect.width = Game.TILE_WIDTH;
                    explosionRect.height = Game.TILE_HEIGHT;

                    var lastExplodingTile = m_currExplodingTile + 20;
                    var reset = false;

                    if (lastExplodingTile >= m_explosionParticleList.Count)
                    {
                        lastExplodingTile = m_explosionParticleList.Count;
                        reset = true;
                    }

                    for (int i = 0; i < m_explosionParticleList.Count; i++)
                    {
                        GridPoint2 gridPos = new GridPoint2(m_explosionParticleList[i].m_tx, m_explosionParticleList[i].m_ty);
                        Entity entity = m_tileMap.getEntity(Level.ENTITY_LAYER, gridPos) as Entity;

                        if (entity != null)
                        {
                            entity.kill(m_parent, this);
                        }
                    }

                    m_tileMap.reformLayer(Level.ENTITY_LAYER);

                    if (reset)
                    {
                        m_currExplodingTile = 0;
                    }

                    if (Game.g.timer - m_explosionStartTime > EXPLOSION_DURATION)
                    {
                        m_state = STATE_EXPLOSION_OVER;

                        if (m_explosionParticleList.Count > 0)
                        {
                            for (var currParticle = 0; currParticle < m_explosionParticleList.Count; currParticle++)
                            {
                                m_explosionParticleList[currParticle].fade();
                            }
                        }
                        else
                        {
                            m_terminated = true;
                            base.onDeath(null, null);

                        }
                    }
                    break;
                }

        }
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);
        Destroy(gameObject);
    }
    public void onFaded()
    {

        if (!m_terminated)
        {
            m_numFadedParticles++;

            if (m_numFadedParticles >= m_explosionParticleList.Count)
            {
                m_terminated = true;
                base.onDeath(null, null);

            }

        }

    }

    public void explode()
    {
        if (m_exploded)
            return;

        m_explosionTilePosList = m_tileMap.getRadiusWrapped2(explodeX, explodeY, m_size);

        GridPoint2 explodePoint = new GridPoint2(explodeX, explodeY);
        Vector2 realPos = new Vector2(explodeX + 0.5f, explodeY + 0.5f);

        for (int i = 0; i < m_explosionTilePosList.Count; i++)
        {
            GridPoint2 tilePos = m_explosionTilePosList[i];
            Vector2 realTilePos = tilePos.ToVector2() + (Vector2.one * 0.5f);

            GO entity = m_tileMap.getEntity(Level.ENTITY_LAYER, tilePos);

            bool blocked = false;

            if (entity != null && entity.GetType() == typeof(EntityHardBlock))
            {
                blocked = true;
            }
            else
            {
                //if (realPos.x.isEqual(realTilePos.x) || realPos.y.isEqual(realTilePos.y))
                //{
                RaycastHit2D[] hits = Physics2D.LinecastAll(realPos, realTilePos);

                for (int j = 0; j < hits.Length; j++)
                {
                    RaycastHit2D hit = hits[j];

                    if (hit.transform != null)
                    {
                        GO go = hit.transform.GetComponent<GO>();

                        if (go.GetType() == typeof(EntityHardBlock))
                        {
                            blocked = true;
                        }
                    }
                }

                //}
            }


            if (!blocked)
            {
                ExplosionParticle explosion = m_game.createObject<ExplosionParticle>(Level.PARTICLE_LAYER,
                                                                                     "ExplosionParticle",
                                                                                     m_explosionTilePosList[i].X,
                                                                                     m_explosionTilePosList[i].Y);

                m_explosionParticleList.Add(explosion);
            }
        }

        for (int i = 0; i < m_explosionParticleList.Count; i++)
        {
            m_explosionParticleList[i].setValues(this);
        }

        m_explosionStartTime = Game.g.timer;
        m_state = STATE_EXPLOSION_FADING;
        Game.g.m_gameScreen.shake();
        m_exploded = true;
        m_sound.playSound("BombSound");
        m_draw = false;

        m_tileMap.remove(Level.ENTITY_LAYER, tx, ty);

    }

    public void onParticleFadeIn()
    {

        if (!m_currMainAnimFrameOffsetCreated)
        {
            m_currMainAnimFrameOffsetCreated = true;
            m_currMainAnimFrameOffset = 0;
        }

        m_numFadedInParticles++;

        if (m_numFadedInParticles >= m_explosionParticleList.Count)
        {

            m_currMainAnimFrameOffset = 0;
            m_explodingNormal = true;


            var gameObjectList = Game.g.m_gameObjectList;

            for (var currObject = 0; currObject < gameObjectList.Count; currObject++)
            {
                if ((gameObjectList[currObject] != this) && (gameObjectList[currObject] is EntityBomb))


                {
                    EntityBomb bomb = gameObjectList[currObject] as EntityBomb;

                    if (bomb.m_explodingNormal)
                    {
                        m_currMainAnimFrameOffset = bomb.m_currMainAnimFrameOffset;
                        break;
                    }// end if

                }// end if
            }// end for

            for (var currParticle = 0; currParticle < m_explosionParticleList.Count; currParticle++)
            {
                m_explosionParticleList[currParticle].startSyncedAnimLoop();
            }// end for             

        }// end if

    }// end public function onParticleFadeIn():void         


    public void continueExploding()
    {
        m_state = STATE_EXPLOSION_FADING;

        //var entityCollisionRect = new Rect();
        //var gameObjectList = Game.g.m_gameObjectList;

        //var done = false;
        //var addClips = false;

        //var affectedTilePosList = new List<GridPoint2>();

        //m_expandLayer = false;
        //done = m_radialPathMap.continueCastingPath();
        //affectedTilePosList = m_radialPathMap.getNewMarkedPathPosList();
        //m_accAffectedTilePosList.push(affectedTilePosList);
        //addClips = true;
        //fadeInClip = new ExplosionFadeInGuideParticle();

        //if (!m_startedAddingTiles)
        //{
        //    m_startedAddingTiles = true;
        //    m_explosionTilePosList.push(m_accAffectedTilePosList);
        //}// end if              


        //for (var currExplodingTile:uint = 0; currExplodingTile < affectedTilePosList.length; currExplodingTile++)
        //    {

        //    var absPosX:int = affectedTilePosList[currExplodingTile].m_x * Game.TILE_WIDTH;
        //    var absPosY:int = affectedTilePosList[currExplodingTile].m_y * Game.TILE_HEIGHT;

        //    if (addClips)
        //    {

        //        var explosion:ExplosionParticle = new ExplosionParticle(affectedTilePosList[currExplodingTile].m_x, affectedTilePosList[currExplodingTile].m_y, this, fadeInClip);//new (getDefinitionByName("ExplosionLevel" + m_powerLevel) as Class)();
        //        Game.g.m_gameObjectList.push(explosion);
        //        m_explosionParticleList.push(explosion);

        //    }// end if

        //}// end for     

        //if (done)
        //{



        //}// end if


    }// end public function continueExploding():void        

    public override void onGamePaused()
    {
        base.onGamePaused();


        stop();

        if (m_state != STATE_EXPLOSION_FADING)
        {
            //for (var currChild:uint = 0; currChild < numChildren; currChild++)
            //    {
            //    if (getChildAt(currChild) is MovieClip)
            //    {
            //        (getChildAt(currChild) as MovieClip).stop();
            //    }// end if
            //}// end for
        }// end if          
    }// end public override function onGamePaused():void        

    public override void onGameUnpaused()
    {
        base.onGameUnpaused();
        if (m_state == STATE_WAITING_T0_EXPLODE)
        {
            m_animator.playbackTime = 1;
        }// end if
        if (m_state != STATE_EXPLOSION_FADING)
        {
            //for (var currChild = 0; currChild < numChildren; currChild++)
            //{
            //    if (getChildAt(currChild) is MovieClip)
            //    {
            //        (getChildAt(currChild) as MovieClip).play();
            //    }// end if
            //}// end for
        }// end if          
    }
}
