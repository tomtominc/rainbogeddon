﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : GO
{
    public const int STATE_OFF = 0;
    public const int STATE_FLASHING = 1;

    protected Entity m_parent;
    protected bool m_canFlash;
    protected int m_state;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
    }

    public virtual void setParent(Entity p_parent)
    {
        m_state = STATE_OFF;
        m_parent = p_parent;
        transform.SetParent(p_parent.transform);
        transform.localPosition = Vector3.zero;
    }

    public virtual void flash()
    {
        if (m_canFlash)
        {
            m_canFlash = false;
            m_state = STATE_FLASHING;
            m_animator.Play("flash");
        }
    }

    public virtual void stopFlashing()
    {
        m_state = STATE_OFF;
        m_canFlash = true;
        m_animator.Play("off");
    }

    public static Flash create()
    {
        Transform l_flashPrefab = Resources.Load<Transform>("Prefabs/Effects/Flash");
        Transform l_flashObj = Instantiate(l_flashPrefab);
        return l_flashObj.GetComponent<Flash>();
    }
}
