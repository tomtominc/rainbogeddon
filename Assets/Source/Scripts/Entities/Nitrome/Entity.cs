﻿using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class Entity : GO
{
    public const int DIR_NONE = 0;
    public const int DIR_LEFT = 1;
    public const int DIR_RIGHT = 2;
    public const int DIR_UP = 3;
    public const int DIR_DOWN = 4;

    public const int SUB_STATE_WAITING_RECOVER = 0;
    public const int SUB_STATE_RECOVERED = 1;

    public const int RECOVER_WAIT_DURATION = 2000;
    public const int STOP_ON_HIT_DURATION = 500;

    public int m_qDir;
    protected int m_prevQDir;
    public int m_state;
    public float m_speed;
    public Rect m_collisionRect;
    public Rect m_absCollisionRect;
    protected int m_hitPoints;
    protected bool m_isDead;
    protected bool m_canDie;
    protected bool m_stopAtTile;
    protected bool m_stopAtNextTile;
    protected int m_stopTileX;
    protected int m_stopTileY;
    protected int m_prevPrevTileX;
    protected int m_prevPrevTileY;
    protected int m_oldPrevTileX;
    protected int m_oldPrevTileY;
    protected int m_prevTileX;
    protected int m_prevTileY;
    protected int m_oldTileX;
    protected int m_oldTileY;
    protected List<GO> m_backChildGameObjectList;
    protected List<GO> m_frontChildGameObjectList;
    public float m_cTX;
    public float m_cTY;
    public int m_recoverSubState;
    public float m_recoverWaitStartTime;
    public float m_recoverWaitDuration;
    public int m_maxHitPoints;
    public bool m_signalPosOnCenterNextTile;
    public Flash m_flash;
    public bool m_canWrap;
    public bool m_moves;
    public int m_iDUL;
    public int m_iDUR;
    public int m_iDLL;
    public int m_iDLR;
    public bool m_triggerDeath;
    public bool m_canCollide;
    public object m_spatialHashMap; //uhhhh?
    public bool m_stoppingOnHit;
    public float m_stopOnHitStartTime;
    public bool m_stopMoving;
    public bool m_stopSidewaysOnCorridor;
    public List<GO> m_collidingObjects;

    public int state
    {
        get { return m_state; }
    }

    public bool isDead
    {
        get { return m_isDead; }
        set { m_isDead = value; }
    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_canCollide = true;
        m_qDir = DIR_NONE;
        m_prevQDir = DIR_NONE;
        m_speed = 0;
        m_collisionRect = getBounds(this);
        m_collisionRect.width = m_collisionRect.width * ApplicationController.PIXEL_SIZE;
        m_collisionRect.height = m_collisionRect.height * ApplicationController.PIXEL_SIZE;
        m_absCollisionRect = getBounds(this);
        m_isDead = false;
        m_canDie = false;
        m_stopAtTile = false;
        m_stopAtNextTile = false;
        m_backChildGameObjectList = new List<GO>();
        //m_flash = Flash.create();
        //m_flash.initialize(m_controller, p_tx, p_ty);
        //m_flash.setParent(this);
        m_frontChildGameObjectList = new List<GO>();
        //m_frontChildGameObjectList.Add(m_flash);
        m_recoverSubState = SUB_STATE_RECOVERED;
        m_recoverWaitDuration = RECOVER_WAIT_DURATION;
        m_prevTileX = m_tx;
        m_prevTileY = m_ty;
        m_prevPrevTileX = m_tx;
        m_prevPrevTileY = m_ty;
        m_oldPrevTileX = m_tx;
        m_oldPrevTileY = m_ty;
        m_canWrap = true;
        m_cTX = x / Game.TILE_WIDTH;
        m_cTY = y / Game.TILE_HEIGHT;
        m_triggerDeath = false;
        m_signalPosOnCenterNextTile = false;
        m_moves = true;
        m_stoppingOnHit = false;
        m_stopMoving = false;
        m_stopSidewaysOnCorridor = false;
        m_collidingObjects = new List<GO>();
    }

    public virtual void stopMoving()
    {
        m_stopMoving = true;
    }

    public virtual void startMoving()
    {
        m_stopMoving = false;
    }

    public override void onGamePaused()
    {
        for (int i = 0; i < m_backChildGameObjectList.Count; i++)
        {
            m_backChildGameObjectList[i].onGamePaused();
        }

        for (int i = 0; i < m_frontChildGameObjectList.Count; i++)
        {
            m_frontChildGameObjectList[i].onGamePaused();
        }
    }

    public override void onGameUnpaused()
    {
        for (int i = 0; i < m_backChildGameObjectList.Count; i++)
        {
            m_backChildGameObjectList[i].onGameUnpaused();
        }

        for (int i = 0; i < m_frontChildGameObjectList.Count; i++)
        {
            m_frontChildGameObjectList[i].onGameUnpaused();
        }
    }

    public virtual void updateAbsCollisionRect()
    {
        m_absCollisionRect.x = x + m_collisionRect.x + m_vel.x;
        m_absCollisionRect.y = y + m_collisionRect.y + m_vel.y;
        m_absCollisionRect.width = m_collisionRect.width;
        m_absCollisionRect.height = m_collisionRect.height;
    }

    public virtual void signalPosOnCenterNextTile()
    {
        m_signalPosOnCenterNextTile = true;
    }

    public virtual void doDamage(int p_hitPoints, Vector2 p_vel, Entity p_attacker, Entity p_weapon)
    {
        if (!m_isDead && m_canDie && !m_triggerDeath)
        {
            m_hitPoints -= p_hitPoints;
            onDamage(p_vel);

            if (m_hitPoints <= 0)
            {
                onDeath(p_attacker, p_weapon);

                if (p_attacker != null)
                {
                    p_attacker.onKilledEntity(this);
                }

                m_triggerDeath = true;
            }
        }
    }

    public virtual void stopAtTile(int p_tx, int p_ty)
    {
        if ((m_tx == p_tx && m_ty == p_ty) && (m_vel.x.isZero() && m_vel.y.isZero()))
        {
            onStoppedAtTile();
        }
        else
        {
            m_stopAtTile = true;
            m_stopTileX = p_tx;
            m_stopTileY = p_ty;
        }
    }

    public virtual void stopAtNextTile()
    {
        if ((System.Math.Abs(m_vel.x) < float.Epsilon &&
             System.Math.Abs(m_vel.y) < float.Epsilon))
        {
            onStoppedAtTile();
        }
        else
        {
            m_stopAtNextTile = true;
        }
    }

    public virtual void onStoppedAtTile()
    {

    }

    public virtual void onCenterNextTile()
    {

    }

    public virtual void onDamage(Vector2 p_vel)
    {
        startRecovery();
        stopOnHit();
    }

    public virtual void onDeath(Entity p_attacker, Entity p_weapon)
    {
        m_tileMap.remove(Level.ENTITY_LAYER, new GridPoint2(m_tx, m_ty));
        m_draw = false;
    }

    public virtual void kill(Entity p_attacker, Entity p_weapon)
    {
        if (!m_isDead && m_canDie)
        {
            if (m_flash)
            {
                m_flash.flash();
            }

            m_hitPoints = 0;


            onDeath(p_attacker, p_weapon);

            if (p_attacker != null)
            {
                p_attacker.onKilledEntity(this);
            }
        }
    }

    public virtual void onKilledEntity(Entity p_entity)
    {

    }

    public override void tick()
    {
        doInput();

        if (!m_isDead)
        {
            doPhysics();
        }
    }

    public override void doPhysics()
    {
        if (m_isDead)
        {
            return;
        }

        if (m_tileMap == null)
        {
            return;
        }

        if (m_tx >= m_tileMap.width)
        {
            m_tx = 0;
        }

        if (m_ty >= m_tileMap.height)
        {
            m_ty = 0;
        }

        float l_tempX = x;
        float l_tempY = y;

        float toX = x;
        float toY = y;
        float fromX = x;
        float fromY = y;

        //float offset = 0.5f;

        if (m_vel.x < 0)
        {
            //toX -= 1;// m_vel.x;
            toX += m_vel.x * Time.deltaTime;
        }
        else if (m_vel.x > 0)
        {
            //toX += 1;// (Game.TILE_WIDTH + m_vel.x);
            //fromX +=  Game.TILE_WIDTH;
            toX += Game.TILE_WIDTH + (m_vel.x * Time.deltaTime);
            fromX += Game.TILE_WIDTH;
        }
        else if (m_vel.y < 0)
        {
            //toY -= 1; //m_vel.y;
            toY += m_vel.y * Time.deltaTime;
        }
        else if (m_vel.y > 0)
        {
            //toY += 1; //(Game.TILE_HEIGHT + m_vel.y);
            //fromY += Game.TILE_HEIGHT;
            toY += Game.TILE_HEIGHT + (m_vel.y * Time.deltaTime);
            fromY += Game.TILE_HEIGHT;
        }

        int l_toTileX = getTileXForX(toX);
        int l_toTileY = getTileYForY(toY);
        int l_fromTileX = getTileXForX(fromX);
        int l_fromTileY = getTileYForY(fromY);

        bool l_canGo = false;
        bool l_skipCollision = false;

        if (m_qDir == DIR_LEFT)
        {
            if (m_vel.x >= 0 && m_vel.y.isZero())
            {
                if (m_tileMap.canGoLeft(m_tx, m_ty, m_canWrap))
                {
                    m_vel.x = -m_speed;
                    m_vel.y = 0;
                    l_skipCollision = true;
                }
            }
            else if (l_fromTileY != l_toTileY)
            {
                l_canGo = m_tileMap.canGoLeft(m_tx, l_fromTileY, m_canWrap);

                if (l_canGo || (!l_canGo && m_stopSidewaysOnCorridor))
                {
                    if (l_canGo)
                    {
                        m_vel.x = -m_speed;
                        m_vel.y = 0;
                        l_skipCollision = true;
                    }
                    else
                    {
                        m_vel.x = 0;
                        m_vel.y = 0;
                    }
                }
                m_tx = (int)m_cTX;
                m_ty = l_fromTileY;

                if (m_ty < 0)
                {
                    m_ty = m_tileMap.height - 1;
                }
                else if (m_ty > m_tileMap.height - 1)
                {
                    m_ty = 0;
                }

                updatePosFromTile();
            }
        }
        else if (m_qDir == DIR_RIGHT)
        {
            if (m_vel.x <= 0 && m_vel.y.isZero())
            {
                if (m_tileMap.canGoRight(m_tx, m_ty, m_canWrap))
                {
                    m_vel.x = m_speed;
                    m_vel.y = 0;
                    l_skipCollision = true;
                }
            }
            else if (l_fromTileY != l_toTileY)
            {
                l_canGo = m_tileMap.canGoRight(m_tx, l_fromTileY, m_canWrap);

                if (l_canGo || (!l_canGo && m_stopSidewaysOnCorridor))
                {
                    if (l_canGo)
                    {
                        m_vel.x = m_speed;
                        m_vel.y = 0;
                        l_skipCollision = true;
                    }
                    else
                    {
                        m_vel.x = 0;
                        m_vel.y = 0;
                    }
                    m_tx = (int)m_cTX;
                    m_ty = l_fromTileY;
                    if (m_ty < 0)
                    {
                        m_ty = m_tileMap.height - 1;
                    }
                    else if (m_ty > m_tileMap.height - 1)
                    {
                        m_ty = 0;
                    }

                    updatePosFromTile();
                }
            }
        }
        // original code UP = DOWN
        else if (m_qDir == DIR_UP) // THIS CODE IS DOWN CODE!
        {
            if (m_vel.y <= 0 && m_vel.x.isZero())
            {
                if (m_tileMap.canGoUp(m_tx, m_ty, m_canWrap))
                {
                    m_vel.y = m_speed;
                    m_vel.x = 0;
                    l_skipCollision = true;
                }
            }
            else if (l_fromTileX != l_toTileX)
            {
                l_canGo = m_tileMap.canGoUp(l_fromTileX, m_ty, m_canWrap);

                if (l_canGo || (!l_canGo && m_stopSidewaysOnCorridor))
                {
                    if (l_canGo)
                    {
                        m_vel.y = m_speed;
                        m_vel.x = 0;
                        l_skipCollision = true;
                    }
                    else
                    {
                        m_vel.x = 0;
                        m_vel.y = 0;
                    }
                    m_ty = (int)m_cTY;
                    m_tx = l_fromTileX;

                    if (m_tx < 0)
                    {
                        m_tx = m_tileMap.width - 1;
                    }
                    else if (m_tx > m_tileMap.width - 1)
                    {
                        m_tx = 0;
                    }

                    updatePosFromTile();
                }
            }
        }
        // original code DOWN = UP
        else if (m_qDir == DIR_DOWN) // THIS CODE IS UP CODE!!!
        {
            if (m_vel.y >= 0 && m_vel.x.isZero())
            {
                if (m_tileMap.canGoDown(m_tx, m_ty, m_canWrap))
                {
                    m_vel.y = -m_speed;
                    m_vel.x = 0;
                    l_skipCollision = true;
                }
            }
            else if (l_fromTileX != l_toTileX)
            {
                l_canGo = m_tileMap.canGoDown(l_fromTileX, m_ty, m_canWrap);

                if (l_canGo || (!l_canGo && m_stopSidewaysOnCorridor))
                {
                    if (l_canGo)
                    {
                        m_vel.y = -m_speed;
                        m_vel.x = 0;
                        l_skipCollision = true;
                    }
                    else
                    {
                        m_vel.x = 0;
                        m_vel.y = 0;
                    }
                    m_ty = (int)m_cTY;
                    m_tx = l_fromTileX;

                    if (m_tx < 0)
                    {
                        m_tx = m_tileMap.width - 1;
                    }
                    else if (m_tx > m_tileMap.width - 1)
                    {
                        m_tx = 0;
                    }

                    updatePosFromTile();
                }
            }
        }

        if (m_stopAtTile)
        {
            int l_blockTileX = m_stopTileX;
            int l_blockTileY = m_stopTileY;

            if (m_vel.x < 0)
            {
                l_blockTileX--;
            }
            else if (m_vel.x > 0)
            {
                l_blockTileX++;
            }
            else if (m_vel.y < 0)
            {
                l_blockTileY--;
            }
            else if (m_vel.y > 0)
            {
                l_blockTileY++;
            }

            if ((l_blockTileX != m_stopTileX) || (l_blockTileY != m_stopTileY))
            {
                if ((l_blockTileX == l_toTileX) && (l_blockTileY == l_toTileY))
                {
                    m_tx = m_stopTileX;
                    m_ty = m_stopTileY;
                    m_vel.x = 0;
                    m_vel.y = 0;
                    updatePosFromTile();
                    m_stopAtTile = false;
                    l_skipCollision = true;
                    onStoppedAtTile();
                }
            }
        }

        if ((!l_skipCollision && !m_tileMap.canGoToTile(l_toTileX, l_toTileY, m_canWrap))
            || (m_stopAtNextTile && (l_fromTileX != l_toTileX || l_fromTileY != l_toTileY)))
        {
            if (m_vel.x < 0)
            {
                m_tx = l_toTileX + 1;
                m_ty = l_toTileY;
            }
            else if (m_vel.x > 0)
            {
                m_tx = l_toTileX - 1;
                m_ty = l_toTileY;
            }
            else if (m_vel.y < 0)
            {
                m_tx = l_toTileX;
                m_ty = l_toTileY + 1;
            }
            else if (m_vel.y > 0)
            {
                m_tx = l_toTileX;
                m_ty = l_toTileY - 1;
            }

            m_vel.x = 0;
            m_vel.y = 0;

            updatePosFromTile();

            if (m_stopAtNextTile)
            {
                m_stopAtNextTile = false;
                onStoppedAtTile();
            }
        }

        updatePos();

        m_oldTileX = (int)m_cTX;
        m_oldTileY = (int)m_cTY;

        m_cTX = x;// / Game.TILE_WIDTH;
        m_cTY = y;// / Game.TILE_HEIGHT;

        if (!(m_oldTileY == (int)m_cTY && m_oldTileX == (int)m_cTX))
        {
            if ((m_oldTileX != (int)m_cTX && m_oldTileY == (int)m_cTY) ||
                (m_oldTileY != (int)m_cTY && m_oldTileX == (int)m_cTX))
            {
                if (!(m_prevTileX == (int)m_cTX && m_prevTileY == (int)m_cTY))
                {
                    m_oldPrevTileX = m_prevTileX;
                    m_oldPrevTileY = m_prevTileY;
                    m_prevTileX = m_oldTileX;
                    m_prevTileY = m_oldTileY;

                    if ((m_oldPrevTileX != m_prevTileX && m_oldPrevTileY == m_prevTileY) ||
                        (m_oldPrevTileY != m_prevTileY && m_oldPrevTileX == m_prevTileX))
                    {
                        m_prevPrevTileX = m_oldPrevTileX;
                        m_prevPrevTileY = m_oldPrevTileY;
                    }
                }
            }
        }

        if (m_signalPosOnCenterNextTile)
        {
            float l_distance = Vector2.Distance(position, new Vector2(m_tx, m_ty));

            if (l_distance < 0.01f)
            {
                m_signalPosOnCenterNextTile = false;
                onCenterNextTile();
            }
        }

        updateRecovery();
        tickChildren();

        if (m_isDead || m_terminated)
        {
            return;
        }

        if (m_moves)
        {
            float l_distance = Vector2.Distance(position, new Vector2(m_tx, m_ty));

            if (l_distance < 0.5f)
            {
                m_tileMap.move(Level.ENTITY_LAYER, l_fromTileX, l_fromTileY, m_tx, m_ty);
            }
        }

        if (m_tx > m_tileMap.width)
        {
            m_tx = 0;
        }
        if (m_ty > m_tileMap.height)
        {
            m_ty = 0;
        }

        m_drawOrder = 200 + (int)-y;
    }

    public virtual void updateRecovery()
    {
        if (m_recoverSubState == SUB_STATE_WAITING_RECOVER)
        {
            if (m_game.timer - m_recoverWaitStartTime > m_recoverWaitDuration)
            {
                m_recoverWaitStartTime = m_game.timer;
                m_hitPoints += (int)(m_maxHitPoints * 0.3f);

                if (m_hitPoints >= m_maxHitPoints)
                {
                    m_hitPoints = m_maxHitPoints;
                    m_recoverSubState = SUB_STATE_RECOVERED;
                }
            }
        }
    }

    public virtual void tickChildren()
    {
        for (int i = m_backChildGameObjectList.Count - 1; i < -1; i--)
        {
            m_backChildGameObjectList[i].doPhysics();

            if (m_backChildGameObjectList[i].terminated)
            {
                m_backChildGameObjectList.RemoveAt(i);
            }
        }

        for (int i = m_frontChildGameObjectList.Count - 1; i < -1; i--)
        {
            m_frontChildGameObjectList[i].doPhysics();

            if (m_frontChildGameObjectList[i].terminated)
            {
                m_frontChildGameObjectList.RemoveAt(i);
            }
        }
    }

    public virtual void stopOnHit()
    {
        m_stoppingOnHit = true;
        m_stopOnHitStartTime = m_game.timer;
    }

    public virtual void startRecovery()
    {
        m_recoverWaitStartTime = m_game.timer;
        m_recoverSubState = SUB_STATE_WAITING_RECOVER;
    }

    public virtual void updatePos()
    {
        if (m_stoppingOnHit)
        {
            if (m_game.timer - m_stopOnHitStartTime > STOP_ON_HIT_DURATION)
            {
                m_stoppingOnHit = false;
            }
        }
        else if (!m_stopMoving)
        {
            x += m_vel.x * Time.deltaTime;
            y += m_vel.y * Time.deltaTime;
        }

        if (x >= m_tileMap.width * Game.TILE_WIDTH)
        {

            x -= m_tileMap.width * Game.TILE_WIDTH;
            onWrapped();
        }
        else if (x < 0)
        {
            x += m_tileMap.width * Game.TILE_WIDTH;
            onWrapped();
        }
        else if (y >= m_tileMap.height * Game.TILE_HEIGHT)
        {
            y -= m_tileMap.height * Game.TILE_HEIGHT;
            onWrapped();
        }
        else if (y < 0)
        {
            y += m_tileMap.height * Game.TILE_HEIGHT;
            onWrapped();
        }

        updateTilePos();
    }

    public virtual void onWrapped()
    {
        Debug.Log("wrapped!");
    }

    public virtual int getTileXForX(float p_x)
    {
        return Mathf.FloorToInt(p_x);// (int)p_x;
    }

    public virtual int getTileYForY(float p_y)
    {
        return Mathf.FloorToInt(p_y);//(int)p_y;
    }

    public virtual float getWrappedX(float p_x)
    {
        if (p_x < 0)
        {
            return m_tileMap.width - 1;
        }
        else if (p_x > m_tileMap.width)
        {
            return 0;
        }

        return p_x;
    }

    public virtual float getWrappedY(float p_y)
    {
        if (p_y < 0)
        {
            return m_tileMap.height - 1;
        }
        else if (p_y > m_tileMap.height)
        {
            return 0f;
        }

        return p_y;
    }

    public int getWrappedTileX(int p_x)
    {
        if (p_x < 0)
        {
            return m_tileMap.width - 1;
        }
        else if (p_x > m_tileMap.width - 1)
        {
            return 0;
        }

        return p_x;
    }

    public int getWrappedTileY(int p_y)
    {
        if (p_y < 0)
        {
            return m_tileMap.height - 1;
        }
        else if (p_y > m_tileMap.height - 1)
        {
            return 0;
        }

        return p_y;
    }

    public virtual bool collidesWithObject(GO l_go)
    {
        return m_collidingObjects.Contains(l_go);
    }

    public virtual List<GO> getCollidingObjects()
    {
        return m_collidingObjects;
    }

    private void OnTriggerEnter2D(Collider2D p_other)
    {
        Transform l_otherTransform = p_other.transform;
        GO l_go = p_other.transform.GetComponent<GO>();

        if (l_go != null && !m_collidingObjects.Contains(l_go))
        {
            m_collidingObjects.Add(l_go);
        }

        onTriggerEnter2D(p_other);
    }

    public virtual void onTriggerEnter2D(Collider2D p_other)
    {

    }

    private void OnTriggerExit2D(Collider2D p_other)
    {
        Transform l_otherTransform = p_other.transform;
        GO l_go = p_other.transform.GetComponent<GO>();

        if (l_go != null && m_collidingObjects.Contains(l_go))
        {
            m_collidingObjects.Remove(l_go);
        }

        onTriggerExit2D(p_other);
    }

    public virtual void onTriggerExit2D(Collider2D p_other)
    {

    }

}

public enum Tile
{
    NONE,
    PLAYER_1,
    PLAYER_2,
    SOFT_BLOCK,
    HARD_BLOCK,
    PILL,
    BACKGROUND
}