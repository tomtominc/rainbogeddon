﻿using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids2;
using UnityEngine;

public class ExplosionParticle : EntityCentered
{
    public const int STATE_FADING_IN = 0;
    public const int STATE_NORMAL = 1;
    public const int STATE_FADING_OUT = 2;

    public const int NUM_MAIN_ANIM_LOOP_FRAMES = 7;

    public TileOutliner m_tileOutliner;
    public EntityBomb m_parent;
    public bool m_timelineCode;
    public bool m_startedFadingIn;
    private int m_contourID;
    private List<uint> m_colorTable;

    public virtual void setValues(EntityBomb p_parent)
    {
        m_parent = p_parent;

        m_timelineCode = true;
        m_startedFadingIn = false;
        m_moves = false;
        m_canDie = false;
        //if (m_tileOutliner == null)
        //{
        m_tileOutliner = new TileOutliner();
        m_tileOutliner.m_level = m_game.m_level;
        m_tileOutliner.m_tileMap = Level.PARTICLE_LAYER;
        m_tileOutliner.m_mapWidth = Game.g.mapWidth;
        m_tileOutliner.m_mapHeight = Game.g.mapHeight;
        m_tileOutliner.m_emptyTileValue = PathMap.TILE_SENTINEL;

        m_colorTable = new List<uint>(new uint[7]);

        m_colorTable[0] = 0xffff0017;
        m_colorTable[1] = 0xffff6e00;
        m_colorTable[2] = 0xffffe700;
        m_colorTable[3] = 0xff04FF01;
        m_colorTable[4] = 0xff0091ff;
        m_colorTable[5] = 0xff8E00ff;
        m_colorTable[6] = 0xffeb00c0;

        //m_animator.Play("t_0");

        m_state = STATE_FADING_IN;

        reform();

    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_timelineCode = false;
        m_drawFast = true;
        m_collisionRect.x = 1;
        m_collisionRect.y = 1;
        m_collisionRect.width = 23 * ApplicationController.PIXEL_SIZE;
        m_collisionRect.height = 23 * ApplicationController.PIXEL_SIZE;
    }

    public void fadeOutDone()
    {
        //trace("fadeOutDone");
        if (m_timelineCode)
        {
            m_parent.onFaded();

            stop();
            m_terminated = true;
            m_tileMap.remove(Level.PARTICLE_LAYER, tx, ty);

        }
    }

    public void startSyncedAnimLoop()
    {
        if (m_timelineCode)
        {
            var contourID = m_tileOutliner.getContourID(typeof(ExplosionParticle), m_tx, m_ty);
            m_contourID = contourID;

            reform();

            //Debug.LogFormat("EXPLOSION PARTICLE ID = {0}", contourID);
            //gotoAndStop("t_" + contourID);
        }
    }// end public function startSyncedAnimLoop():void      

    public override void onGamePaused()
    {
        stop();
    }// end public override function onGamePaused():void

    public override void onGameUnpaused()
    {
        if ((m_state == STATE_FADING_IN) || (m_state == STATE_FADING_OUT))
        {
            play();
        }
    }

    public void fade()
    {
        //trace("fade");
        m_state = STATE_FADING_OUT;
        var contourID = m_tileOutliner.getContourID(typeof(ExplosionParticle), m_tx, m_ty);

        m_renderer.color = Color.clear;
        //gotoAndPlay("t_" + contourID + "_fade_out");
        m_contourID = 0;
        Destroy(gameObject);
    }// end public function fade():void 

    public void endFadeIn()
    {
        if (m_timelineCode)
        {
            m_state = STATE_NORMAL;
            var contourID = m_tileOutliner.getContourID(typeof(ExplosionParticle), m_tx, m_ty);


            //gotoAndStop("t_" + contourID);
            m_parent.onParticleFadeIn();
        }// end if
    }// end public function endFadeIn():void            

    public override void doPhysics()
    {

        if (m_terminated)
        {
            return;
        }// end if

        var contourID = 0;

        if (m_state == STATE_FADING_IN)
        {

            if (!m_startedFadingIn)
            {
                m_startedFadingIn = true;
                contourID = m_tileOutliner.getContourID(typeof(ExplosionParticle), m_tx, m_ty);


                //gotoAndPlay("t_" + contourID + "_fade_in");
                endFadeIn();
            }

        }
        else if (m_state == STATE_NORMAL)
        {
            contourID = m_tileOutliner.getContourID(typeof(ExplosionParticle), m_tx, m_ty);
            if (contourID != m_contourID)
            {


                //gotoAndStop("t_" + contourID);
                m_contourID = contourID;
            }

            List<GO> objs = getCollidingObjects();

            for (int i = 0; i < objs.Count; i++)
            {
                GO obj = objs[i];

                if (obj is Entity)
                {
                    ((Entity)obj).kill(m_parent.m_parent, m_parent);
                }
            }
        }

    }


    public virtual void reform()
    {
        GridPoint2 m_point = new GridPoint2(tx, ty);
        int l_north = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X, m_point.Y + 1)) ? 1 : 0;
        int l_south = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X, m_point.Y - 1)) ? 1 : 0;
        int l_east = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X + 1, m_point.Y)) ? 1 : 0;
        int l_west = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X - 1, m_point.Y)) ? 1 : 0;
        int l_northeast = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X + 1, m_point.Y + 1)) && l_north != 0 && l_east != 0 ? 1 : 0;
        int l_northwest = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X - 1, m_point.Y + 1)) && l_north != 0 && l_west != 0 ? 1 : 0;
        int l_southeast = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X + 1, m_point.Y - 1)) && l_south != 0 && l_east != 0 ? 1 : 0;
        int l_southwest = m_tileMap.hasEntity(Level.PARTICLE_LAYER, new GridPoint2(m_point.X - 1, m_point.Y - 1)) && l_south != 0 && l_west != 0 ? 1 : 0;

        int index = l_west + (2 * l_northwest) + (4 * l_north) + (8 * l_northeast) + (16 * l_east) + (32 * l_southeast) + (64 * l_south) + (128 * l_southwest);


        //Debug.LogErrorFormat("explosion: {0}", index);
        //m_animator.Play(index.ToString());
    }

    public override void draw()
    {

        if (m_state != STATE_NORMAL)
        {
            base.draw();
        }
        else
        {

            //m_bounds = m_boundsList[currentFrame + m_parent.m_currMainAnimFrameOffset];

            //if (m_contourID == 255)
            //{

            //    var color = m_colorTable[m_parent.m_currMainAnimFrameOffset]; // m_colorTable[currentFrame - m_firstFrame];

            //    m_drawBitmap.fillRect(new Rectangle(x, y, m_bounds.width, m_bounds.height), color);

            //    if (x + (m_bounds.x + m_bounds.width) > m_drawBitmap.width)
            //    {
            //        m_drawBitmap.fillRect(new Rectangle(x - m_drawBitmap.width, y, m_bounds.width, m_bounds.height), color);
            //    }
            //    else if ((x + m_bounds.x) < 0)
            //    {
            //        m_drawBitmap.fillRect(new Rectangle(x + m_drawBitmap.width, y, m_bounds.width, m_bounds.height), color);
            //    }
            //    else if (y + (m_bounds.y + m_bounds.height) > m_drawBitmap.height)
            //    {
            //        m_drawBitmap.fillRect(new Rectangle(x, y - m_drawBitmap.height, m_bounds.width, m_bounds.height), color);
            //    }
            //    else if ((y + m_bounds.y) < 0)
            //    {
            //        m_drawBitmap.fillRect(new Rectangle(x, y + m_drawBitmap.height, m_bounds.width, m_bounds.height), color);
            //    }// end else if         

            //}
            //else
            //{


            //    if (!m_bounds.width || !m_bounds.height)
            //    {
            //        return;
            //    }// end if

            //    var chosenBitmap:BitmapData = m_frameBitmapList[currentFrame + m_parent.m_currMainAnimFrameOffset - 1];

            //    var finalX:Number = x;
            //    var finalY:Number = y;

            //    m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x, finalY + m_bounds.y), null, null, true);


            //}// end else

        }// end else


    }// end public override function draw():void        

}
