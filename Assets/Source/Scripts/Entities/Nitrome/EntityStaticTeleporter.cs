﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityStaticTeleporter : EntityTeleporter
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        setPowerLevel(3);
    }

    public override void doPhysics()
    {
        if (m_terminated)
        {
            return;
        }

        if (!m_firstCollisionP1CheckPerformed)
        {

            m_firstCollisionP1CheckPerformed = true;

            if (collidesWithObject(Game.g.player1))
            {
                m_waitingPlayer1UnCollide = true;
            }// end if

        }
        else
        {

            if (m_waitingPlayer1UnCollide)
            {
                if (!collidesWithObject(Game.g.player1))
                {
                    m_waitingPlayer1UnCollide = false;
                }// end if
            }
            else
            {
                if (collidesWithObject(Game.g.player1))
                {
                    if (m_twin)
                    {
                        m_firstCollisionP1CheckPerformed = false;
                        Game.g.player1.teleportFrom(this);
                    }// end if
                }// end if                  
            }// end else

        }// end else

        if (!m_firstCollisionP2CheckPerformed)
        {

            m_firstCollisionP2CheckPerformed = true;

            if (collidesWithObject(Game.g.player2))
            {
                m_waitingPlayer2UnCollide = true;
            }// end if              

        }
        else
        {

            if (m_waitingPlayer2UnCollide)
            {
                if (!collidesWithObject(Game.g.player2))
                {
                    m_waitingPlayer2UnCollide = false;
                }// end if                  
            }
            else
            {
                if (collidesWithObject(Game.g.player2))
                {
                    if (m_twin)
                    {
                        m_firstCollisionP2CheckPerformed = false;
                        Game.g.player2.teleportFrom(this);
                    }// end if
                }// end if                      
            }// end else                

        }// end else                        


        if (!m_twin)
        {
            var gameObjectList = Game.g.m_gameObjectList;

            for (var currObject = 0; currObject < gameObjectList.Count; currObject++)
            {
                if ((gameObjectList[currObject] != this) && (gameObjectList[currObject].id == id))

                {
                    m_twin = gameObjectList[currObject] as EntityTeleporter;
                    m_twin.twin = this;
                    this.gotoAndPlay("new");
                    (gameObjectList[currObject] as EntityTeleporter).gotoAndPlay("old");
                    break;
                }// end if
            }// end for

        }// end if
    }
}
