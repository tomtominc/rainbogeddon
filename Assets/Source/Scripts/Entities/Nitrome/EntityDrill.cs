﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class EntityDrill : EntityCentered
{
    public const int DAMAGE = 1;
    public const int STATE_OFF = 0;
    public const int STATE_ON = 1;

    public int m_damage;
    public bool m_wrapped;
    public int m_wrappedTimes;
    public Entity m_parent;
    public bool m_isColliding;
    public bool m_isDrillingOnHardWall;
    public DrillEffect m_effect;
    public Vector2 m_direction;

    public float m_damageTimer;
    public float m_damageTime;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_damage = DAMAGE;
        m_wrapped = false;
        m_parent = transform.parent.GetComponent<Entity>();
        m_wrappedTimes = 0;
        m_isColliding = false;
        m_isDrillingOnHardWall = false;
        m_effect.gotoAndPlay("off");
    }

    public void terminate()
    {
        //m_terminated = true;
        m_parent.startMoving();
        SoundController.instance.playSound("Drill");
        SoundController.instance.playSound("DrillOnHardWall");
        gameObject.SetActive(false);
    }

    public void turnOn()
    {
        if (m_state != STATE_ON)
        {
            SoundController.instance.playSound("Drill");
            m_state = STATE_ON;
            m_damageTime = 0;
            m_damageTimer = 1f;
        }
    }

    public void turnOff()
    {
        if (m_state != STATE_OFF)
        {
            //SoundController.instance
            m_state = STATE_OFF;
            m_isColliding = false;
            m_isDrillingOnHardWall = false;
            m_parent.startMoving();
        }
    }

    public void goUp()
    {
        x = m_parent.x;
        y = m_parent.y + 0.5f;
        m_direction = Vector2.up;
        m_effect.rotation = 270;
    }

    public void goDown()
    {
        x = m_parent.x;
        y = m_parent.y - 0.5f;
        m_direction = Vector2.down;
        m_effect.rotation = 90;

    }

    public void goLeft()
    {
        x = m_parent.x - 0.5f;
        y = m_parent.y;
        m_direction = Vector2.left;
        m_effect.rotation = 0;
    }

    public void goRight()
    {
        x = m_parent.x + 0.5f;
        y = m_parent.y;
        m_direction = Vector2.right;
        m_effect.rotation = 180;
    }

    public override void doPhysics()
    {
        if (m_terminated)
        {
            return;
        }// end if

        if (!gameObject.activeSelf)
        {
            return;
        }

        var posX = 0f;
        var posY = 0f;

        var parentPlayer = m_parent as EntityPlayer;
        var parentVel = parentPlayer.m_vel;

        if (parentVel.x > 0)
        {
            goRight();
        }
        else if (parentVel.x < 0)
        {
            goLeft();
        }
        else if (parentVel.y < 0)
        {
            goUp();
        }
        else if (parentVel.y > 0)
        {
            goDown();
        }
        else if ((parentVel.x.isZero()) && (parentVel.y.isZero()))
        {

            switch ((m_parent as EntityPlayer).m_lastDir)
            {
                case DIR_UP:
                case DIR_NONE:
                    {
                        goUp();
                        break;
                    }// end case

                case DIR_DOWN:
                    {
                        goDown();
                        break;
                    }// end case                    

                case DIR_LEFT:
                    {
                        goLeft();
                        break;
                    }// end case        

                case DIR_RIGHT:
                    {
                        goRight();
                        break;
                    }// end case                        

            }// end switch                      


        }// end else if


        switch (m_state)
        {

            case STATE_OFF:
                {

                    break;
                }// end case

            case STATE_ON:
                {


                    GridPoint2 point = new GridPoint2((int)m_parent.position.x + (int)m_direction.x, (int)m_parent.position.y + (int)m_direction.y);
                    GridPoint2 wrappedPoint = m_tileMap.getWrappedPoint(point);

                    var ignoreList = new List<Type>();
                    ignoreList.Add(typeof(EntityCollectible));
                    ignoreList.Add(typeof(EntityBullet));
                    ignoreList.Add(typeof(EntityBubbleParticle));
                    ignoreList.Add(typeof(EntitySafetyBubble));
                    ignoreList.Add(typeof(PowerUp));

                    Entity collidingObject = m_tileMap.getEntity(Level.ENTITY_LAYER, wrappedPoint) as Entity;

                    //var collidingObjects = getCollidingObjects();

                    //for (int i = 0; i < collidingObjects.Count; i++)
                    //{
                    //    GO collidingGo = collidingObjects[i];

                    //    if (!ignoreList.Contains(collidingGo.GetType()) && collidingGo != m_parent)
                    //    {
                    //        collidingObject = collidingGo as Entity;
                    //    }
                    //}

                    if (collidingObject)
                    {
                        if (m_damageTime > 0f)
                        {
                            m_damageTime -= Time.deltaTime;
                            return;
                        }

                        m_damageTime = m_damageTimer;
                        collidingObject.doDamage(m_damage, Vector2.zero, m_parent, this);

                        if (!m_isColliding)
                        {
                            if (collidingObject is EntityHardBlock)
                            {
                                m_isDrillingOnHardWall = true;
                                //SoundManager.loopSound("DrillOnHardWall");
                                //SoundManager.stopSound("Drill");
                            }

                            m_isColliding = true;
                            m_parent.stopMoving();
                            m_effect.gotoAndPlay("on");
                        }

                        m_tileMap.reformLayer(Level.ENTITY_LAYER);
                    }
                    else if (m_isColliding)
                    {
                        if (m_isDrillingOnHardWall)
                        {
                            m_isDrillingOnHardWall = false;
                            //SoundManager.stopSound("DrillOnHardWall");
                            //SoundManager.loopSound("Drill");
                        }// end if
                        m_isColliding = false;
                        m_parent.startMoving();
                        m_damageTime = 0f;
                        // m_effect.gotoAndPlay("off");

                    }// end else

                    break;
                }// end case                

        }// end switch




        updatePos();

        //m_spatialHashMap.updateObject(this);

    }// end public override function doPhysics():void        
}
