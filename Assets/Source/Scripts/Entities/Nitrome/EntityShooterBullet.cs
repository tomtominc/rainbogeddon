﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityShooterBullet : EntityBullet
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_speed = 10;
    }

    public override string fireSoundID
    {
        get
        {
            return "EnemyShoot";
        }
    }

    public override string effect
    {
        get
        {
            return "ShooterImpactEffect";
        }
    }
}
