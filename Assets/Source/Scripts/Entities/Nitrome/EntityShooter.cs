﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityShooter : EntityEnemy
{
    public const float FIRE_DELAY = 1f;
    private bool m_shooting;


    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_speed = 1.0f;
        m_checkDirMaxTimeout = 2.5f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_weaponHolder = new BulletHolder(this);
        (m_weaponHolder as BulletHolder).m_fireDelay = FIRE_DELAY;
        m_checksLineOfSight = true;
        m_shooting = false;
        m_glow.m_color = 0xffe800ff;
    }

    public void shootingDone()
    {
        m_shooting = false;
        m_forceUpdateAnim = true;
    }// end public function shootingDone():void

    public override void startAlert()
    {
        base.startAlert();
        m_shooting = false;
    }// end public override function startAlert():void

    public void startShooting()
    {
        m_shooting = true;

        if (m_prevVel.x < 0)
        {
            gotoAndPlay("walk_side_shooting");

            flipX(false);
        }
        else if (m_prevVel.x > 0)
        {
            gotoAndPlay("walk_side_shooting");

            flipX(true);
        }
        else if (m_prevVel.y < 0)
        {
            gotoAndPlay("walk_down_shooting");
        }
        else if (m_prevVel.y > 0)
        {
            gotoAndPlay("walk_up_shooting");
        }// end else if             

    }// end public function startShooting():void        

    public override void playSideWalkAnim()
    {
        m_shooting = false;
        base.playSideWalkAnim();
    }// end public override function playSideWalkAnim():void

    public override void playUpWalkAnim()
    {
        m_shooting = false;
        base.playUpWalkAnim();
    }// end public override function playUpWalkAnim():void      

    public override void playDownWalkAnim()
    {
        m_shooting = false;
        base.playDownWalkAnim();
    }// end public override function playDownWalkAnim():void        

    // called by the animator!!!
    public void shoot()
    {
        m_weaponHolder.tryFire();
    }// end public function shoot():void            

    public override void whileOnLineOfSightPlayer1()
    {
        if ((!m_shooting && m_weaponHolder.canFire()) &&
            (((m_vel.x < 0) && (m_lineOfSightPlayer1Dir2 == DIR_LEFT)) ||
            ((m_vel.x > 0) && (m_lineOfSightPlayer1Dir1 == DIR_RIGHT)) ||
             ((m_vel.y < 0) && (m_lineOfSightPlayer1Dir2 == DIR_DOWN)) ||
             ((m_vel.y > 0) && (m_lineOfSightPlayer1Dir1 == DIR_UP))))
        {
            startShooting();
        }// end if

    }// end public override function whileOnLineOfSightPlayer1():void       

    public override void whileOnLineOfSightPlayer2()
    {

        if ((!m_shooting && m_weaponHolder.canFire()) &&
           (((m_vel.x < 0) && (m_lineOfSightPlayer2Dir2 == DIR_LEFT)) ||
           ((m_vel.x > 0) && (m_lineOfSightPlayer2Dir1 == DIR_RIGHT)) ||
           ((m_vel.y < 0) && (m_lineOfSightPlayer2Dir2 == DIR_DOWN)) ||
           ((m_vel.y > 0) && (m_lineOfSightPlayer2Dir1 == DIR_UP))))
        {


            startShooting();
        }

    }// end public override function whileOnLineOfSightPlayer2():void                   

    public override GameObject respawnEffectType()
    {
        return base.respawnEffectType();
    }

    public override GameObject destroyedEffectType()
    {
        return base.destroyedEffectType();
    }
}// end public class EntityShooter

