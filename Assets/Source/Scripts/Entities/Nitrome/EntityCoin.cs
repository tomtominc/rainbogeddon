﻿using UnityEngine;
using System;

public class EntityCoin : EntityCollectible
{
    public const int MAX_SIZE = 4;

    public static event Action<EntityCoin> listenerList = delegate { };

    protected int m_size;
    protected static int[] m_sizeToScoreTable;
    protected static Rect[] m_sizeToCollisionRectTable;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        m_drawFast = true;
        base.initialize(p_controller, p_tx, p_ty);
        Game.numCoinsInLevel++;

        if (m_sizeToScoreTable == null)
        {
            m_sizeToScoreTable = new int[5];

            m_sizeToScoreTable[0] = 0;
            m_sizeToScoreTable[1] = 1;
            m_sizeToScoreTable[2] = 2;
            m_sizeToScoreTable[3] = 4;
            m_sizeToScoreTable[4] = 6;

            m_sizeToCollisionRectTable = new Rect[5];

            m_sizeToCollisionRectTable[0] = new Rect(0, 0, 0, 0);
            m_sizeToCollisionRectTable[1] = new Rect(-5, -5, 10, 10);
            m_sizeToCollisionRectTable[2] = new Rect(-7.5f, -7.5f, 15, 15);
            m_sizeToCollisionRectTable[3] = new Rect(-10, -10, 20, 20);
            m_sizeToCollisionRectTable[4] = new Rect(-12.5f, -12.5f, 25, 25);
        }

    }

    public virtual void setSize(int p_size)
    {
        m_size = p_size;
        m_collisionRect = m_sizeToCollisionRectTable[m_size];
        m_score = m_sizeToScoreTable[m_size];
    }

    public override void collect()
    {
        if (!m_collected)
        {
            if (m_game.startedMoving)
            {
                m_sound.playSound("TakePill");
            }

            Game.numCoinsInLevel--;
            base.collect();
            notifyCollectedListeners();
        }
    }

    public override void doPhysics()
    {
        if (terminated)
            return;

        if ((EntityEnemy.m_numDead > m_size - 1) && (m_size < MAX_SIZE))
        {
            m_size++;
            m_score = m_sizeToScoreTable[m_size];
            m_collisionRect = m_sizeToCollisionRectTable[m_size];
            if (m_animator)
                m_animator.Play("size_" + m_size);
        }
        else if ((EntityEnemy.m_numDead < m_size - 1))
        {
            m_size--;
            m_score = m_sizeToScoreTable[m_size];
            m_collisionRect = m_sizeToCollisionRectTable[m_size];

            if (m_animator)
                m_animator.Play("size_" + m_size);
        }// end else if

    }// end public override function doPhysics():void       

    public static void addCollectedListener(Action<EntityCoin> p_action)
    {
        listenerList += p_action;
    }

    public static void removeCollectedListener(Action<EntityCoin> p_action)
    {
        listenerList -= p_action;
    }

    public void notifyCollectedListeners()
    {
        listenerList(this);
    }

}
