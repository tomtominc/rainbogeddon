﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillHolder : WeaponHolder
{
    private EntityDrill m_drill;
    private bool m_drillPlaced;
    private List<int> m_drillDamageTable;

    public DrillHolder(EntityCharacter p_parent)
        : base(p_parent)
    {
        m_drill = ((EntityPlayer)m_parent).m_drill;
        m_drillDamageTable = new List<int>();
        m_drillDamageTable.Add(0);
        m_drillDamageTable.Add(1);
        m_drillDamageTable.Add(2);
        m_drillDamageTable.Add(4);
    }

    public void activate()
    {
        m_parent.m_animModifier = "_drill_off";
        m_parent.m_forceUpdateAnim = true;
        placeDrill();

        Debug.Log(m_parent.m_animModifier);
    }

    public void placeDrill()
    {
        if (!m_drillPlaced && Game.g.m_gameObjectList != null)
        {
            m_drillPlaced = true;
            m_drill.gameObject.SetActive(true);
            Game.g.addGO(m_drill);
        }
    }

    public override bool tryFire()
    {
        if (m_drill.m_state == EntityDrill.STATE_ON)
        {
            m_parent.m_stopFire = true;
            m_parent.m_fire = false;
            return false;
        }
        else if (m_parent.m_lastDir != Entity.DIR_NONE)
        {
            m_parent.m_stopSidewaysOnCorridor = true;
            m_parent.m_fire = false;
            m_parent.m_animModifier = "_drill_on";
            m_parent.m_forceUpdateAnim = true;
            m_drill.turnOn();
            return true;
        }

        return false;
    }

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;
            m_drill.m_damage = m_drillDamageTable[m_currUpgradeStage];
            return true;
        }

        return false;
    }

    public override void terminate()
    {
        m_parent.m_stopSidewaysOnCorridor = false;
        m_parent.m_animModifier = string.Empty;
        m_parent.m_forceUpdateAnim = true;
        m_drill.terminate();
    }

    public override void tryStopFire()
    {
        m_parent.m_stopSidewaysOnCorridor = false;
        m_parent.m_stopFire = false;
        m_parent.m_animModifier = "_drill_off";
        m_parent.m_forceUpdateAnim = true;
        m_drill.turnOff();
    }
}
