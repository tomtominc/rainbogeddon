﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnExplosion
{
    public const int MAX_SIZE = 15;
    public const int STATE_EXPLODING = 0;
    public const int STATE_IMPLODING = 1;

    public const float EXPANSION_WAIT = 0.06f;
    public const float CONTRACTION_WAIT = 1f;

    public uint m_color;
    public Entity m_parent;
    public int m_currSize;
    public int m_state;
    public float m_explosionProgressStartTime;
    public bool m_respawns;

    public bool m_terminated;

    private GameObject m_respawnPrefab;

    public int tx;
    public int ty;

    public bool m_done;

    public RespawnExplosion(Entity p_parent, bool p_respawn)
    {
        m_parent = p_parent;
        tx = m_parent.tx;
        ty = m_parent.ty;
        m_color = 0xffffffff;
        m_respawns = p_respawn;

        m_currSize = 0;
        m_state = STATE_EXPLODING;
        m_explosionProgressStartTime = Game.g.timer;

        m_done = false;
        if (p_parent is EntityEnemy)
        {
            m_respawnPrefab = ((EntityEnemy)m_parent).respawnEffectType();

        }
    }

    public virtual void doPhysics()
    {
        if (m_terminated)
            return;

        if (m_state == STATE_EXPLODING)
        {
            if (Game.g.timer - m_explosionProgressStartTime > EXPANSION_WAIT)
            {
                m_explosionProgressStartTime = Game.g.timer;
                m_currSize++;

                if (m_currSize >= MAX_SIZE)
                {
                    if (m_respawns && !Game.g.completed)
                    {
                        m_currSize = MAX_SIZE - 1;
                        m_state = STATE_IMPLODING;
                    }
                    else
                    {
                        m_terminated = true;
                    }
                }
            }
        }
        else if (m_state == STATE_IMPLODING)
        {
            if (Game.g.timer - m_explosionProgressStartTime > CONTRACTION_WAIT)
            {
                m_explosionProgressStartTime = Game.g.timer;
                m_currSize--;

                if (m_currSize < 0)
                {
                    if (!Game.g.completed)
                    {
                        m_currSize = 0;
                        GameObject respawnEffect = Object.Instantiate(m_respawnPrefab);
                        respawnEffect.transform.position = new Vector3(tx, ty);
                        m_done = true;
                    }
                }

            }
        }
    }
}
