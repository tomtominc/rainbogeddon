﻿using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids2;
using UnityEngine;

public class EntityJelly : EntityEnemy
{
    public GameObject respawnEffectGreen;
    public GameObject destroyedEffectGreen;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_targetTriggerRange = 4;
        m_canDie = true;
        m_hasOnTargetRangeTrigger = true;
        m_checkDirMaxTimeout = 1.5f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_hasDecreasingTimeout = false;
        m_glow.m_color = 0xff89ff00;
        m_speed = 1.5f;
    }

    public override void onTargetInRange(Entity p_target)
    {
        m_checkDirMaxTimeout = 0.25f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
    }

    public override void onTargetOutOfRange()
    {
        m_checkDirMaxTimeout = 1.5f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
    }

    public override GameObject respawnEffectType()
    {
        return base.respawnEffectType();
    }

    public override GameObject destroyedEffectType()
    {
        return base.destroyedEffectType();
    }
}
