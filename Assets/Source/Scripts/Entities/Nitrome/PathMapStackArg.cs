﻿
public class PathMapStackArg
{
    public int x;
    public int y;
    public int dist;

    public PathMapStackArg(int p_x, int p_y, int p_dist)
    {
        x = p_x;
        y = p_y;
        dist = p_dist;
    }
}
