﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class EntitySafetyBubble : EntityCentered
{
    public const int STATE_WAITING_T0_EXPAND = 0;
    public const int STATE_EXPANDING = 1;
    public const int STATE_EXPANDED = 2;
    public const int STATE_FADING = 4;

    public const float EXPANSION_WAIT_DURATION = 0.2f;
    public const float EXPANDED_DURATION = 240f;
    public const float DETONATION_DURATION = 4f;
    public const int SIZE = 2;

    public int m_radius;
    public int m_currRadius;
    public List<GridPoint2> m_affectedTilePosList;
    public List<EntityBubbleParticle> m_particleList;
    public float m_expansionWaitStartTime;
    public float m_fadingWaitStartTime;
    public float m_expandedWaitStartTime;
    public bool m_canDisable;
    public bool m_startedAddingTiles;
    private bool m_queueFade;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_canDisable = false;
        m_startedAddingTiles = false;
        m_collisionRect.x = -2.5f;
        m_collisionRect.y = -2.5f;
        m_collisionRect.width = 5;
        m_collisionRect.height = 5;
        m_queueFade = false;
        m_canDie = false;
        m_state = STATE_WAITING_T0_EXPAND;

        expand();
    }

    public override void doPhysics()
    {

        switch (m_state)
        {

            case STATE_WAITING_T0_EXPAND:
                {


                    break;
                }// end case

            case STATE_EXPANDING:
                {



                    continueExpanding();

                    break;
                }// end case    

            case STATE_EXPANDED:
                {



                    onExpanded();

                    break;
                }// end case    

            case STATE_FADING:
                {
                    continueFading();

                    break;
                }// end case                    

        }// end switch

    }// end public override function doPhysics():void           



    public void onExpanded()
    {
        if ((Game.g.timer - m_expandedWaitStartTime > EXPANDED_DURATION) || m_queueFade)
        {
            fade();
        }
    }

    public void expand()
    {
        m_particleList = new List<EntityBubbleParticle>();
        m_currRadius = 0;
        m_state = STATE_EXPANDING;
        m_expansionWaitStartTime = 0;

        continueExpanding();

    }

    public void continueExpanding()
    {
        // TODO: set to false animate expanding, then set to true when finished
        bool done = true;

        //TODO: animate expand
        m_affectedTilePosList = m_tileMap.getRadiusWrapped(Level.ENTITY_LAYER, tx, ty, SIZE);

        for (int i = 0; i < m_affectedTilePosList.Count; i++)
        {
            if (!m_tileMap.getEntity(Level.ENTITY_LAYER, m_affectedTilePosList[i]))
            {
                EntityBubbleParticle bubble = m_game.createObject<EntityBubbleParticle>(Level.PARTICLE_LAYER,
                                                                          "BubbleParticle",
                                                                          m_affectedTilePosList[i].X,
                                                                          m_affectedTilePosList[i].Y);

                m_particleList.Add(bubble);
            }
        }

        if (done)
        {
            m_state = STATE_EXPANDED;
            m_canDisable = true;
            m_expandedWaitStartTime = Game.g.timer;
        }
    }

    public void fade()
    {

        if (m_state == STATE_EXPANDED)
        {

            m_state = STATE_FADING;
            m_fadingWaitStartTime = 0;


            continueFading();

        }
        else
        {
            m_queueFade = true;
        }// end else

    }// end public function fade():void             

    public void continueFading()
    {
        //TODO: animate particle fade

        for (int i = m_particleList.Count - 1; i > -1; i--)
        {
            m_particleList[i].terminate();
        }

        m_canDisable = true;
        m_terminated = true;
    }

}
