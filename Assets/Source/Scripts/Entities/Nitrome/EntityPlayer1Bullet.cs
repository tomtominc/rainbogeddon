﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer1Bullet : EntityBullet
{
    public override string fireSoundID
    {
        get
        {
            return "Shoot";
        }
    }

    public override string effect
    {
        get
        {
            return "Player1BulletImpactEffect";
        }
    }
}
