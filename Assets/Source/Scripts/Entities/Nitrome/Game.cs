﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Gamelogic.Grids2;

public class Game : Controller
{
    public const int MAP_WIDTH = 14;
    public const int MAP_HEIGHT = 26;

    public const int STATE_PRESENTING_LEVEL = 0;
    public const int STATE_GET_SET = 1;
    public const int STATE_PRESENTING_LEVEL_INFO = 2;
    public const int STATE_PLAYER_HEAD_START = 3;
    public const int STATE_PLAYING_NORMAL = 4;
    public const int STATE_LEVEL_END_WAIT = 5;
    public const int STATE_FINISHED = 6;

    public const int PRESENTING_DURATION = 2; //2000ms
    public const int HEAD_START_DURATION = 3; //3000ms
    public const int LEVEL_END_WAIT_DURATION = 2; //2000ms
    public const int GET_SET_DURATION = 1; //1000ms

    public const int FRAMERATE = 25;
    public const int SCALE = 25;
    public const float INV_SCALE = 1F / SCALE;
    public const float WIDTH = 550;
    public const float HEIGHT = 550;
    public const float TILE_WIDTH = 1;
    public const float TILE_HEIGHT = 1;
    public const float GAME_CONTAINER_MARGIN_X = 25;
    public const float GAME_CONTAINER_MARGIN_Y = 25;

    public const int SCORE_DIGITS = 7;
    public const int END_LEVEL_DELAY = 90;

    public bool isInit;
    public int state;
    public int frameCount;
    public bool paused;
    public int mousePressedCount;
    public bool mousePressed;
    public bool completed;
    public bool failed;
    public string startMsg;
    public float stateStartTime;
    public bool goDisappeared;

    public int endLevelCount;
    public int mapWidth;
    public int mapHeight;
    public Rect mapRect;
    public int startX;
    public int startY;
    public Vector2 mousePos;
    public int gameWidth;
    public int gameHeight;

    public static int score;
    public static int levelScore;
    public static int selectedLevel;
    public static int numCoinsInLevel;
    public static bool isTwoPlayer;
    public static bool retrying;
    public static Level currentLevel;

    public bool startedMoving;
    public EntityPlayer1 player1;
    public EntityPlayer2 player2;

    public List<GO> m_gameObjectList;

    public PathMap m_player1PathMap;
    public PathMap m_player2PathMap;
    public PathMap m_player1NoWrapPathMap;
    public PathMap m_player2NoWrapPathMap;
    public PathMap m_player1ProjectedPathMap;
    public PathMap m_player2ProjectedPathMap;

    public NitromeTileMap m_enemyTileMap;
    public NitromeTileMap m_tileMap;

    public Level m_level;

    public PowerUpManager m_powerUpManager;
    public PowerUpManager m_mainPowerUpManager;

    public float m_offsetTime;

    public static Game g;
    public static PlayMode playMode;

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);
        g = this;
        m_gameObjectList = new List<GO>();
        m_gameScreen = m_controller.getController<GameScreen>();
    }

    public void unloadRefs()
    {
        state = STATE_PRESENTING_LEVEL;
        m_powerUpManager = null;
        m_player1PathMap = null;
        m_player2PathMap = null;
        m_player1NoWrapPathMap = null;
        m_player2NoWrapPathMap = null;
        m_player1ProjectedPathMap = null;
        m_player2ProjectedPathMap = null;
        m_enemyTileMap = null;
        m_tileMap = null;
        m_level = null;
        m_powerUpManager = null;
        m_mainPowerUpManager = null;
        m_offsetTime = Time.realtimeSinceStartup;
        m_gameObjectList.Clear();
        m_gameObjectList = null;
        currentLevel = null;
        player1 = null;
        player2 = null;
        score = 0;
        numCoinsInLevel = 0;
        levelScore = 0;

        System.GC.Collect();
    }

    public override void onApplicationEnter()
    {
        // call this when going to the game
        //init();
    }

    public void init()
    {
        EntityCoin.addCollectedListener(onCollectedCoin);

        score = 0;
        levelScore = 0;
        frameCount = 0;
        failed = false;
        endLevelCount = END_LEVEL_DELAY;
        numCoinsInLevel = 0;
        paused = false;
        completed = false;
        goDisappeared = false;

        m_sound.stopAllSounds();
        m_sound.stopMusic();
        m_sound.garbageCollectChannels();
        m_sound.playMusic("gameMusic");

        mapWidth = MAP_WIDTH;
        mapHeight = MAP_HEIGHT;

        m_levelController.spawnLevel();
        m_level = m_levelController.currentLevel;

        initGameObjects();

        player1 = m_levelController.currentLevel.player1;
        player2 = m_levelController.currentLevel.player2;

        m_gameObjectList = FindObjectsOfType<GO>().ToList();

        isTwoPlayer = playMode == PlayMode.PLAYER_2_MODE;

        if (!isTwoPlayer)
        {
            player2.isDead = true;
            player2.terminated = true;

            if (player2.m_glow)
            {
                player2.m_glow.terminate();
            }

            m_level.remove(Level.ENTITY_LAYER, player2.tx, player2.ty);
        }

        isInit = true;

        startPresentingLevel();
    }

    public float timer
    {
        get { return Time.realtimeSinceStartup - m_offsetTime; }
    }

    public void setEnemyDeathSound(string p_enemyDeathSoundToPlay)
    {
        m_enemyDeathSoundToPlay = p_enemyDeathSoundToPlay;
    }

    public void setEnemyDeathMessagePlayer1(string p_enemyDeathMessageToDisplayPlayer1)
    {
        m_enemyDeathMessageToDisplayPlayer1 = p_enemyDeathMessageToDisplayPlayer1;
    }

    public void setEnemyDeathMessagePlayer2(string p_enemyDeathMessageToDisplayPlayer2)
    {
        m_enemyDeathMessageToDisplayPlayer2 = p_enemyDeathMessageToDisplayPlayer2;
    }

    private void startPresentingLevel()
    {
        state = STATE_PRESENTING_LEVEL;
        m_sound.playSound("Level" + selectedLevel + "Sound");
        m_offsetTime = Time.realtimeSinceStartup;
        stateStartTime = timer;

        Debug.LogFormat("Create text object {0}", startMsg);
    }

    //get... set... go!!! state..
    private void startGetSet()
    {
        state = STATE_GET_SET;
        stateStartTime = timer;
        Debug.Log("Create a text object saying GET SET!");
        m_sound.playSound("GetSet");
    }

    private void startPlayerHeadStart()
    {
        state = STATE_PLAYER_HEAD_START;
        stateStartTime = timer;
        player1.inputEnabled = true;

        if (isTwoPlayer)
            player2.inputEnabled = true;

        m_sound.playSound("Go");
        m_powerUpManager.start();
        //main powerup manager

    }

    private void main()
    {
        float totalTime = timer;
        float safetyMapTime;
        float mapTime;
        float drawTime;
        int currTile;

        if (!paused)
        {
            if (state == STATE_PRESENTING_LEVEL && timer - stateStartTime > PRESENTING_DURATION)
            {
                startGetSet();
            }
            else if (state == STATE_GET_SET && timer - stateStartTime > GET_SET_DURATION)
            {
                startPlayerHeadStart();

            }
            else if (state == STATE_PLAYER_HEAD_START)
            {
                if (startedMoving && !goDisappeared)
                {
                    goDisappeared = true;
                    // fade the go visual
                }

                if (timer - stateStartTime > HEAD_START_DURATION)
                {
                    state = STATE_PLAYING_NORMAL;

                    for (int i = 0; i < m_gameObjectList.Count; i++)
                    {
                        if (m_gameObjectList[i] is EntityEnemy)
                        {
                            ((EntityEnemy)m_gameObjectList[i]).canMove = true;
                        }
                    }
                }
            }
            else if (state == STATE_LEVEL_END_WAIT && timer - stateStartTime > LEVEL_END_WAIT_DURATION)
            {

            }

            frameCount++;

            mapTime = timer;

            m_enemyDeathSoundToPlay = string.Empty;
            m_enemyDeathMessageToDisplayPlayer1 = string.Empty;
            m_enemyDeathMessageToDisplayPlayer2 = string.Empty;

            //tick tilemap? 
            m_tileMap.fillMap(m_gameObjectList);
            m_tileMap.tick();

            // draw tilemap??

            // enemy tilemap

            m_enemyTileMap.m_tileMap = new List<int>(new int[mapWidth * mapHeight]);

            for (currTile = 0; currTile < m_enemyTileMap.m_tileMap.Count; currTile++)
            {
                m_enemyTileMap.m_tileMap[currTile] = PathMap.TILE_SENTINEL;
            }// end for             

            for (var currGameObj = 0; currGameObj < m_gameObjectList.Count; currGameObj++)
            {
                int index = m_gameObjectList[currGameObj].ty * mapWidth + m_gameObjectList[currGameObj].tx;

                if (index >= m_enemyTileMap.m_tileMap.Count)
                {
                    continue;
                }


                if (m_enemyTileMap.m_tileMap[index] == PathMap.TILE_SENTINEL)
                {
                    m_enemyTileMap.m_tileMap[m_gameObjectList[currGameObj].ty * mapWidth + m_gameObjectList[currGameObj].tx] = ((m_gameObjectList[currGameObj] is EntityBlock) || (m_gameObjectList[currGameObj].GetType().IsSubclassOf(typeof(EntityBlock))) || (m_gameObjectList[currGameObj] is EntityRainbowCoin) || (m_gameObjectList[currGameObj] is EntityBubbleParticle)) ? PathMap.TILE_BLOCKED : PathMap.TILE_SENTINEL;
                }// end if

            }// end for     

            for (var currGameObj = 0; currGameObj < m_gameObjectList.Count; currGameObj++)
            {

                if (m_gameObjectList[currGameObj] is EntityTail)
                {
                    var tail = m_gameObjectList[currGameObj] as EntityTail;

                    if (m_enemyTileMap.m_tileMap[tail.ty * mapWidth + tail.tx] == PathMap.TILE_SENTINEL)
                    {
                        m_enemyTileMap.m_tileMap[tail.ty * mapWidth + tail.tx] = PathMap.TILE_BLOCKED;
                    }// end if
                }// end if

            }// end for                     

            m_enemyTileMap.tick();

            m_player1PathMap.m_tileMap = new List<int>();
            m_player1PathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);
            m_player1NoWrapPathMap.m_tileMap = new List<int>();
            m_player1NoWrapPathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);
            m_player1ProjectedPathMap.m_tileMap = new List<int>();
            m_player1ProjectedPathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);

            m_player2PathMap.m_tileMap = new List<int>();
            m_player2PathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);
            m_player2NoWrapPathMap.m_tileMap = new List<int>();
            m_player2NoWrapPathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);
            m_player2ProjectedPathMap.m_tileMap = new List<int>();
            m_player2ProjectedPathMap.m_tileMap.AddRange(m_enemyTileMap.m_tileMap);

            if (player1.terminated && !player2.terminated)
            {
                m_player1PathMap.m_map = new List<int>();
                m_player1PathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player1NoWrapPathMap.m_map = new List<int>();
                m_player1NoWrapPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player1ProjectedPathMap.m_map = new List<int>();
                m_player1ProjectedPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2PathMap.floodFill((int)player2.m_cTX, (int)player2.m_cTY);
                m_player2NoWrapPathMap.floodFill((int)player2.m_cTX, (int)player2.m_cTY);
                m_player2ProjectedPathMap.floodFill(player2.m_projectedTX, player2.m_projectedTY);
            }
            else if ((!player2 || player2.terminated) && !player1.terminated)
            {
                m_player2PathMap.m_map = new List<int>();
                m_player2PathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2NoWrapPathMap.m_map = new List<int>();
                m_player2NoWrapPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2ProjectedPathMap.m_map = new List<int>();
                m_player2ProjectedPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player1PathMap.floodFill((int)player1.m_cTX, (int)player1.m_cTY);
                m_player1NoWrapPathMap.floodFill((int)player1.m_cTX, (int)player1.m_cTY);
                m_player1ProjectedPathMap.floodFill((int)player1.m_projectedTX, (int)player1.m_projectedTY);
            }
            else if (!player1.terminated && !player2.terminated)
            {
                m_player1PathMap.floodFill((int)player1.m_cTX, (int)player1.m_cTY);
                m_player2PathMap.floodFill((int)player2.m_cTX, (int)player2.m_cTY);
                m_player1NoWrapPathMap.floodFill((int)player1.m_cTX, (int)player1.m_cTY);
                m_player2NoWrapPathMap.floodFill((int)player2.m_cTX, (int)player2.m_cTY);
                m_player1ProjectedPathMap.floodFill(player1.m_projectedTX, player1.m_projectedTY);
                m_player2ProjectedPathMap.floodFill(player2.m_projectedTX, player2.m_projectedTY);
                //m_player2PathMap.floodFill2Goal(m_player1.m_cTX, m_player1.m_cTY, m_player2.m_cTX, m_player2.m_cTY);
            }
            else
            {
                m_player1PathMap.m_map = new List<int>();
                m_player1PathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player1NoWrapPathMap.m_map = new List<int>();
                m_player1NoWrapPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2PathMap.m_map = new List<int>();
                m_player2PathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2NoWrapPathMap.m_map = new List<int>();
                m_player2NoWrapPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player1ProjectedPathMap.m_map = new List<int>();
                m_player1ProjectedPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);
                m_player2ProjectedPathMap.m_map = new List<int>();
                m_player2ProjectedPathMap.m_map.AddRange(m_enemyTileMap.m_tileMap);

            }// end else


            for (int i = m_gameObjectList.Count - 1; i > -1; i--)
            {
                if (m_gameObjectList[i] == null)
                {
                    m_gameObjectList.RemoveAt(i);
                    continue;
                }
                m_gameObjectList[i].tick();
                m_gameObjectList[i].draw();

                if (m_gameObjectList[i].terminated)
                {
                    GO go = m_gameObjectList[i];
                    m_gameObjectList.RemoveAt(i);

                    Destroy(go.gameObject);
                }
            }


            failed = player1.terminated && player2.terminated;

            if (failed)
            {
                if (m_stateController.currentState == StateDefinition.GAME_UPDATE)
                {

                    EntityCoin.removeCollectedListener(onCollectedCoin);
                    m_stateController.changeState(StateDefinition.GAME_LOSE);
                }

                return;
            }

            m_powerUpManager.tick();
        }
    }

    private void Update()
    {
        if (m_stateController.currentState != StateDefinition.GAME_UPDATE)
            return;

        if (isInit)
        {
            main();
        }
    }

    private void onCollectedCoin(EntityCoin p_coin)
    {
        if (numCoinsInLevel <= 0)
        {
            EntityCoin.removeCollectedListener(onCollectedCoin);
            m_stateController.changeState(StateDefinition.GAME_WIN);
        }
    }

    public void initGameObjects()
    {
        m_player1PathMap = new PathMap(mapWidth, mapHeight);
        m_player2PathMap = new PathMap(mapWidth, mapHeight);
        m_player1NoWrapPathMap = new PathMap(mapWidth, mapHeight);
        m_player2NoWrapPathMap = new PathMap(mapWidth, mapHeight);
        m_player1NoWrapPathMap.wraps = false;
        m_player2NoWrapPathMap.wraps = false;
        m_player1ProjectedPathMap = new PathMap(mapWidth, mapHeight);
        m_player2ProjectedPathMap = new PathMap(mapWidth, mapHeight);

        m_tileMap = new NitromeTileMap(mapWidth, mapHeight);
        m_enemyTileMap = new NitromeTileMap(mapWidth, mapHeight);

        m_powerUpManager = new PowerUpManager(m_level.powerUpPosList);
        // m_mainPowerUpManager = new PowerUpManager(m_level.powerUpPosList);
    }

    public void respawnEnemy(string p_path, int p_x, int p_y)
    {
        Transform enemyPrefab = Resources.Load<Transform>(p_path);
        EntityEnemy enemy = Instantiate(enemyPrefab).GetComponent<EntityEnemy>();
        enemy.initialize(m_controller, p_x, p_y);
        enemy.onRespawn();
        m_level.spawn(Level.ENTITY_LAYER, enemy);
        addGO(enemy);
    }

    public T createObject<T>(string p_layer, string p_id, int p_x, int p_y) where T : GO
    {
        string path = string.Format("{0}/{1}", p_layer, p_id);
        Transform prefab = Resources.Load<Transform>(path);

        if (prefab == null)
        {
            Debug.LogErrorFormat("Couldn't find prefab at {0}", path);
            return null;
        }


        Transform goTransform = Instantiate(prefab);

        T go = goTransform.GetComponent<T>();

        if (go == null)
        {
            Debug.LogErrorFormat("No GO component on object {0}", path);
            return null;
        }
        go.initialize(m_controller, p_x, p_y);
        m_level.spawn(p_layer, go);

        addGO(go);

        return go;
    }

    public T createObjectWait<T>(string p_layer, string p_id, int p_x, int p_y, float p_delay = 1f) where T : GO
    {
        string path = string.Format("{0}/{1}", p_layer, p_id);
        Transform prefab = Resources.Load<Transform>(path);

        if (prefab == null)
        {
            Debug.LogErrorFormat("Couldn't find prefab at {0}", path);
            return null;
        }

        Transform goTransform = Instantiate(prefab);

        T go = goTransform.GetComponent<T>();

        if (go == null)
        {
            Debug.LogErrorFormat("No GO component on object {0}", path);
            return null;
        }
        go.initialize(m_controller, p_x, p_y);
        StartCoroutine(spawnInLevel(p_layer, go, p_delay));

        addGO(go);

        return go;
    }

    private IEnumerator spawnInLevel(string p_layer, GO p_go, float p_delay = 1f)
    {
        yield return new WaitForSeconds(p_delay);

        while (m_level.hasEntity(p_layer, new GridPoint2(p_go.tx, p_go.ty)))
        {
            yield return new WaitForEndOfFrame();
        }

        m_level.spawn(p_layer, p_go);
    }

    public void addGO(GO p_go)
    {
        if (m_gameObjectList == null)
        {
            m_gameObjectList = new List<GO>();
        }

        m_gameObjectList.Add(p_go);
    }

    private string m_enemyDeathSoundToPlay;
    private string m_enemyDeathMessageToDisplayPlayer1;
    private string m_enemyDeathMessageToDisplayPlayer2;
    public GameScreen m_gameScreen;
}
