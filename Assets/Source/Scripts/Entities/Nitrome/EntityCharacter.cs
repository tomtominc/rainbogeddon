﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class EntityCharacter : EntityCentered
{
    public int m_lastDir;
    public bool m_fire;
    public bool m_stopFire;
    public Glow m_glow;
    public string m_animModifier;
    public bool m_forceUpdateAnim;
    public WeaponHolder m_weaponHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_lastDir = DIR_NONE;
        m_fire = false;
        m_stopFire = false;
        m_animModifier = string.Empty;
        m_forceUpdateAnim = false;
        m_glow = GetComponentInChildren<Glow>();
        m_glow.initialize(m_controller, m_tx, m_ty);
        m_glow.setValues(this, 0);
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        m_isDead = true;
        m_terminated = true;
        m_glow.terminate();

        if (m_weaponHolder != null)
        {
            m_weaponHolder.terminate();
        }
    }

    public override void onGamePaused()
    {
        base.onGamePaused();
        m_animator.playbackTime = 0;
    }

    public override void onGameUnpaused()
    {
        base.onGameUnpaused();
        m_animator.playbackTime = 1;
    }

    public virtual void stopFire()
    {
        m_stopFire = true;
    }

}
