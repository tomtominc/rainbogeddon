﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartHolder : WeaponHolder
{
    private List<float> m_speedTable;

    public HeartHolder(EntityCharacter p_parent)
    : base(p_parent)
    {
        m_speedTable = new List<float>();
        m_speedTable.Add(EntityPlayer.SPEED);
        m_speedTable.Add(EntityPlayer.SPEED);
        m_speedTable.Add(EntityPlayer.SPEED * 1.25f);
        m_speedTable.Add(EntityPlayer.SPEED * 1.5f);
    }

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;
            m_parent.m_speed = m_speedTable[m_currUpgradeStage];
            return true;
        }

        return false;
    }

    public override void terminate()
    {
        m_parent.m_speed = EntityPlayer.SPEED;
    }
}
