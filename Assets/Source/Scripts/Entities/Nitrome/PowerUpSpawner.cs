﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : GO
{
    public bool isFirst;
    public List<string> m_powerUpTypes;


    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
    }

    public virtual void setPowerUpTypes(List<string> p_powerUpTypes)
    {
        m_powerUpTypes = p_powerUpTypes;
    }

    public void setIsFirstShown(bool p_isFirst)
    {
        isFirst = p_isFirst;
    }

    public virtual PowerUp createPowerUp()
    {
        int powerUpType = Random.Range(0, m_powerUpTypes.Count);
        string p_path = string.Format("{0}/PowerUp{1}", Level.ENTITY_LAYER, m_powerUpTypes[powerUpType]);
        Transform powerUpPrefab = Resources.Load<Transform>(p_path);
        LevelController levelController = m_controller.getController<LevelController>();

        if (powerUpPrefab == null)
        {
            Debug.LogWarningFormat("No powerup at path {0}", p_path);
            return null;
        }
        Transform powerUpTransform = Instantiate(powerUpPrefab);
        PowerUp powerUp = powerUpTransform.GetComponent<PowerUp>();
        powerUp.initialize(m_controller, tx, ty);
        m_game.addGO(powerUp);
        powerUpTransform.SetParent(levelController.transform);
        return powerUp;
    }
}
