﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBubble : PowerUp
{
    private SafetyBubbleHolder m_safetyBubbleHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_safetyBubbleHolder = new SafetyBubbleHolder(m_game.player1);
    }

    public override string soundID()
    {
        return "Bubble";
    }

    public override string displayName()
    {
        return "BUBBLE";
    }

    public override WeaponHolder holder()
    {
        return m_safetyBubbleHolder;
    }

    public override WeaponHolder getHolder(EntityCharacter p_parent)
    {
        return new SafetyBubbleHolder(p_parent);
    }
}
