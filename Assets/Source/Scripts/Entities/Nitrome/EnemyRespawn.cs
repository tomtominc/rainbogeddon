﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRespawn : MonoBehaviour
{
    public string prefabPath;

    public void spawnEnemy()
    {
        Game.g.respawnEnemy(prefabPath, (int)transform.position.x, (int)transform.position.y);

        Destroy(gameObject);
    }
}
