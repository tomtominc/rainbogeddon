﻿using UnityEngine;

public class BombHolder : WeaponHolder
{
    private EntityBomb m_bomb;

    public BombHolder(EntityCharacter p_parent) : base(p_parent)
    {

    }

    public override bool tryFire()
    {
        m_parent.m_fire = false;

        if (!m_bomb || m_bomb.state == EntityBomb.STATE_EXPLOSION_OVER)
        {
            if (m_parent is EntityPlayer2)
            {
                // nothing yet!
            }
            else if (m_parent is EntityPlayer1)
            {
                int placeX = (int)m_parent.tx;
                int placeY = (int)m_parent.ty;

                if (m_bomb)
                {
                    // m_bomb.kill(null, null);
                }

                m_bomb = Game.g.createObjectWait<EntityPlayer1Bomb>(Level.ENTITY_LAYER, "Player1Bomb", placeX, placeY);
                m_bomb.setValues(m_parent, m_currUpgradeStage);
            }

            SoundController.instance.playSound("DropTail");

            return true;
        }

        return false;
    }

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;
            return true;
        }

        return false;
    }
}
