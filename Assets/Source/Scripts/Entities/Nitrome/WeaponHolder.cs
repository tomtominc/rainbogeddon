﻿
public class WeaponHolder
{
    public const int MAX_UPGRADE_STAGES = 3;

    protected EntityCharacter m_parent;
    protected int m_currUpgradeStage;

    public int currUpgradeStage
    {
        get { return m_currUpgradeStage; }
    }

    public WeaponHolder(EntityCharacter p_parent)
    {
        m_parent = p_parent;
        m_currUpgradeStage = 1;
    }

    public virtual bool upgrade()
    {
        return false;
    }

    public virtual bool canFire()
    {
        return false;
    }

    public virtual bool tryFire()
    {
        return false;
    }

    public virtual void terminate()
    {

    }

    public virtual void tryStopFire()
    {
        m_parent.stopFire();
    }

    public virtual void onChangeDir()
    {

    }

    public virtual void onTeleport()
    {

    }

    public virtual void onTeleportDone()
    {

    }

    public virtual void onTeleportCancel()
    {

    }

}
