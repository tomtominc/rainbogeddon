﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer1 : EntityPlayer
{
    public override string deathSoundID()
    {
        return "PlayerDieRip1";
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);
    }

    public override void onKilledEntity(Entity p_entity)
    {
        if (p_entity is EntityEnemy)
        {
            // m_score = m_score + ((EntityEnemy)p_entity).score;
        }
    }

    public override void teleportIn()
    {
        if (m_teleporter)
        {
            m_teleporter.twin.firstCollisionP1CheckPerformed = false;
        }
        base.teleportIn();
    }
}
