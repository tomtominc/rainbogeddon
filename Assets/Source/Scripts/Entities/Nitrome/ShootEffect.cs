﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootEffect : EffectCentered
{
    private EntityCharacter m_parent;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
    }

    public virtual void setParent(EntityCharacter p_parent)
    {
        m_parent = p_parent;
    }

    public override void doPhysics()
    {
        //float posX = 0;
        //float posY = 0;

        //switch (m_parent.m_lastDir)
        //{

        //    case Entity.DIR_UP:
        //        {
        //            posX = m_parent.x;
        //            posY = m_parent.y + m_parent.m_collisionRect.y + m_parent.m_vel.y;
        //            break;
        //        }// end case

        //    case Entity.DIR_DOWN:
        //        {
        //            posX = m_parent.x;
        //            posY = m_parent.y - m_parent.m_collisionRect.y + m_parent.m_vel.y;
        //            break;
        //        }// end case                    

        //    case Entity.DIR_LEFT:
        //        {
        //            posX = m_parent.x + m_parent.m_collisionRect.x + m_parent.m_vel.x;
        //            posY = m_parent.y;
        //            break;
        //        }// end case        

        //    case Entity.DIR_RIGHT:
        //        {
        //            posX = m_parent.x - m_parent.m_collisionRect.x + m_parent.m_vel.x;
        //            posY = m_parent.y;
        //            break;
        //        }// end case                        

        //}// end switch

        //x = posX;
        //y = posY;
    }

    public void terminate()
    {
        m_terminated = true;
    }
}
