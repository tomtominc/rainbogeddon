﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityHomeyShooter : EntityEnemy
{
    public EntityHomey m_homey;
    private bool m_shooting;
    private int m_homeyOverrideDir;
    private bool m_startedShooting;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_glow.m_color = 0xffff0000;
        m_recoverWaitDuration = 4f;
        m_followsTarget = false;
        m_targetTriggerRange = 6;
        m_hasOnTargetRangeTrigger = true;
        m_checkDirMaxTimeout = 0.25f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_hitPoints = 90;
        m_maxHitPoints = m_hitPoints;
        m_homey = null;
        m_shooting = false;
        m_homeyOverrideDir = DIR_NONE;
        m_startedShooting = false;
    }
    public void shoot()
    {
        m_homey = Game.g.createObject<EntityHomey>(Level.ENTITY_LAYER, "Homey", tx, ty);
        m_homey.setParent(this);
        Game.g.m_gameObjectList.Add(m_homey);
        m_homey.applyOverrideDir(m_homeyOverrideDir);
    }

    public void shootingDone()
    {
        m_shooting = false;
        m_forceUpdateAnim = true;
        m_startedShooting = false;
        m_alertEffect = null;
    }

    public void startShooting()
    {
        m_shooting = true;
        int targetDirection = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
        m_homeyOverrideDir = targetDirection;

        //m_alertEffect = new EnemyAlertEffect(this);
        //Game.g.m_gameObjectList.Add(m_alertEffect);
        //SoundManager.playSound("EnemyAlerted");

        switch (targetDirection)
        {
            case DIR_LEFT:
                {
                    gotoAndPlay("walk_side_shooting");
                    flipX(false);
                    break;
                }// end case

            case DIR_RIGHT:
                {
                    gotoAndPlay("walk_side_shooting");
                    flipX(true);
                    break;
                }// end case                

            case DIR_UP:
                {
                    gotoAndPlay("walk_up_shooting");
                    break;
                }// end case                    

            case DIR_DOWN:
            case DIR_NONE:
                {
                    gotoAndPlay("walk_down_shooting");
                    break;
                }
        }
    }

    public override void playSideWalkAnim()
    {
        m_shooting = false;
        base.playSideWalkAnim();
    }

    public override void playUpWalkAnim()
    {
        m_shooting = false;
        base.playUpWalkAnim();
    }

    public override void playDownWalkAnim()
    {
        m_shooting = false;
        base.playDownWalkAnim();
    }

    public override void doPhysics()
    {
        if (!m_shooting)
        {
            base.doPhysics();
        }
        else
        {
            updateDamage();
            tickChildren();
            updateAbsCollisionRect();
        }
    }

    public override void onCenterNextTile()
    {
        if (!m_shooting && (!m_homey || m_homey.m_terminated))
        {
            startShooting();
        }
    }

    public override void whileTargetInRange(Entity p_target)
    {
        if (!m_shooting && (!m_homey || m_homey.m_terminated) && !m_startedShooting)
        {
            m_startedShooting = true;
            signalPosOnCenterNextTile();
        }
    }

    public override void startAlert()
    {
        base.startAlert();

        if (m_shooting)
        {
            m_shooting = false;
            m_startedShooting = false;
        }
    }

    public override GameObject respawnEffectType()
    {
        //red
        return base.respawnEffectType();
    }

    public override GameObject destroyedEffectType()
    {
        //red
        return base.destroyedEffectType();
    }

}
