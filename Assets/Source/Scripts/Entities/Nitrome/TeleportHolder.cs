﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterHolder : WeaponHolder
{
    public static float FIRE_DELAY = 0.2f;

    private float m_fireStartTime;
    private List<EntityTeleporter> m_teleporterList;

    public TeleporterHolder(EntityCharacter p_parent)
        : base(p_parent)
    {
        m_fireStartTime = 0;
        m_teleporterList = new List<EntityTeleporter>();
    }


    public override bool tryFire()
    {

        if ((m_parent as EntityPlayer).m_state != EntityPlayer.STATE_NORMAL)
        {
            return false;
        }// end if

        m_parent.m_fire = false;



        refreshList();


        var isOnTeleporter = false;

        if (m_teleporterList.Count > 0)
        {
            Vector2 tileCenterVec = new Vector2((m_teleporterList[0].m_cTX * Game.TILE_WIDTH) + (Game.TILE_WIDTH / 2), (m_teleporterList[0].m_cTY * Game.TILE_HEIGHT) + (Game.TILE_HEIGHT / 2));
            Vector2 posVec = new Vector2(m_parent.x, m_parent.y);
            Vector2 posToTileVec = posVec - tileCenterVec;

            if (posToTileVec.magnitude <= 0.1)
            {
                isOnTeleporter = true;
            }// end if          

        }// end if

        if (isOnTeleporter)
        {

            if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
            {

                m_fireStartTime = Game.g.timer;

                // player is spinning on the root teleporter, so eliminate it
                (m_parent as EntityPlayer).abortTeleport();


                terminateTeleporters();

                return true;

            }// end if


        }
        else if (m_teleporterList.Count < 2)
        {

            if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
            {
                m_fireStartTime = Game.g.timer;

                EntityTeleporter teleporter = Game.g.createObject<EntityTeleporter>(Level.PILL_LAYER, "StaticGreenTeleporter", m_parent.m_tx, m_parent.m_ty);

                Game.g.m_gameObjectList.Add(teleporter);

                if (m_teleporterList.Count == 2)
                {
                    m_teleporterList[0].twin = m_teleporterList[1];
                    m_teleporterList[1].twin = m_teleporterList[0];
                    (m_parent as EntityPlayer).teleportFrom(m_teleporterList[1], true);
                }
                else
                {
                    m_teleporterList[0].gotoAndPlay("old");
                }// end if

                return true;

            }// end if              

        }
        else if (m_teleporterList[1].m_canDisable)
        {

            m_teleporterList[1].terminate();

            if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
            {
                m_fireStartTime = Game.g.timer;

                EntityTeleporter teleporter = Game.g.createObject<EntityTeleporter>(Level.PILL_LAYER, "StaticGreenTeleporter", m_parent.m_tx, m_parent.m_ty);
                Game.g.m_gameObjectList.Add(teleporter);

                m_teleporterList[2].twin = m_teleporterList[0];
                m_teleporterList[0].twin = m_teleporterList[2];
                //trace("actually teleporting");
                (m_parent as EntityPlayer).teleportFrom(m_teleporterList[2], true);

                return true;

            }// end if              

        }// end else            

        return false;

    }// end public override function tryFire():Boolean          

    public void refreshList()
    {
        for (int i = m_teleporterList.Count - 1; i > -1; i--)
        {
            if (m_teleporterList[i].m_terminated)
            {
                m_teleporterList.RemoveAt(i);
            }// end if
        }

    }// end public function refreshList():void      

    public override void terminate()
    {
        terminateTeleporters();

    }// end public override function terminate():void               

    private void terminateTeleporters()
    {
        for (int i = m_teleporterList.Count - 1; i > -1; i--)
        {
            if (!m_teleporterList[i].m_terminated)
            {
                m_teleporterList[i].terminate();
            }// end if
        }

    }// end private function terminateTeleporters():void        

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;

            for (int i = 0; i < m_teleporterList.Count; i++)
            {
                if (m_teleporterList[i].getPowerLevel() >= 1)
                {
                    m_teleporterList[i].setPowerLevel(m_currUpgradeStage);
                }

            }

            return true;
        }
        else
        {
            return false;
        }// end else

    }// end public override function upgrade():Boolean          

    public override void onTeleportDone()
    {
        terminateTeleporters();

    }// end public override function onTeleportDone():void          

}// end public class TeleporterHolder
