﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tail
{
    public Entity m_follower;
    public Entity m_leader;
    public Entity m_root;

    public Tail(Entity p_follower, Entity p_leader, Entity p_root)
    {
        m_follower = p_follower;
        m_leader = p_leader;
        m_root = p_root;
    }

    public void doPhysics()
    {
        var leaderPos = new Vector2(m_leader.x, m_leader.y);
        var currPos = new Vector2(m_follower.x, m_follower.y);

        var changeX = false;
        var changeY = false;

        var leaderToPos = leaderPos - currPos;

        // check if the leader has wrapped around the screen
        if (Mathf.Abs(leaderToPos.x) > (Game.WIDTH / 2))
        {
            if (leaderToPos.x > 0)
            {
                leaderToPos.x -= Game.g.mapWidth * Game.TILE_WIDTH;
                leaderPos.x -= Game.g.mapWidth * Game.TILE_WIDTH;
            }
            else
            {
                leaderToPos.x += Game.g.mapWidth * Game.TILE_WIDTH;
                leaderPos.x += Game.g.mapWidth * Game.TILE_WIDTH;
            }// end else                    
        }
        else if (Mathf.Abs(leaderToPos.y) > (Game.WIDTH / 2))
        {
            if (leaderToPos.y > 0)
            {
                leaderToPos.y -= Game.g.mapHeight * Game.TILE_HEIGHT;
                leaderPos.y -= Game.g.mapHeight * Game.TILE_HEIGHT;
            }
            else
            {
                leaderToPos.y += Game.g.mapHeight * Game.TILE_HEIGHT;
                leaderPos.y += Game.g.mapHeight * Game.TILE_HEIGHT;
            }// end else
        }// end else if

        if (leaderToPos.x.isZero() || leaderToPos.y.isZero())
        {

            if (leaderToPos.magnitude > Game.TILE_WIDTH)
            {
                var leaderToPosLength = leaderToPos.magnitude;
                leaderToPos.Normalize();
                leaderToPos.scaleBy(leaderToPosLength - Game.TILE_WIDTH);
                m_follower.x += leaderToPos.x;
                m_follower.y += leaderToPos.y;
                m_follower.m_vel.x = m_leader.m_vel.x;
                m_follower.m_vel.y = m_leader.m_vel.y;

                if (!leaderToPos.x.isZero())
                {
                    changeX = true;
                }
                else if (!leaderToPos.y.isZero())
                {
                    changeY = true;
                }

            }
            else
            {
                m_follower.m_vel.x = 0;
                m_follower.m_vel.y = 0;
            }// end else

        }
        else
        {

            int tX;
            int tY;

            if (Mathf.Abs(leaderToPos.x) + Mathf.Abs(leaderToPos.y) > Game.TILE_WIDTH)
            {

                var adjLength = (Mathf.Abs(leaderToPos.x) + Mathf.Abs(leaderToPos.y)) - Game.TILE_WIDTH;
                Vector2 adjVec;
                float diff;

                if (!m_leader.m_vel.x.isZero())
                {
                    adjVec = new Vector2(0, leaderToPos.y);
                    adjVec.Normalize();
                    adjVec.scaleBy(adjLength);

                    if (m_follower.y < leaderPos.y)
                    {

                        m_follower.y += adjVec.y;
                        if (m_follower.y > leaderPos.y)
                        {
                            diff = Mathf.Abs(m_follower.y - leaderPos.y);
                            m_follower.y = leaderPos.y;
                            if (m_follower.x < leaderPos.x)
                            {
                                m_follower.x += diff;
                            }
                            else
                            {
                                m_follower.x -= diff;
                            }// end else

                        }// end if

                    }
                    else
                    {

                        m_follower.y += adjVec.y;
                        if (m_follower.y < leaderPos.y)
                        {
                            diff = Mathf.Abs(m_follower.y - leaderPos.y);
                            m_follower.y = leaderPos.y;

                            if (m_follower.x < leaderPos.x)
                            {
                                m_follower.x += diff;
                            }
                            else
                            {
                                m_follower.x -= diff;
                            }// end else                                

                        }// end if                                  

                    }// end else    

                    changeY = true;
                    m_follower.m_vel.x = 0;
                    m_follower.m_vel.y = -m_leader.m_vel.x;
                    tX = ((int)(m_follower.x / Game.TILE_WIDTH));
                    m_follower.x = (Game.TILE_WIDTH * tX) + (Game.TILE_WIDTH / 2);

                }
                else if (!m_leader.m_vel.y.isZero())
                {
                    adjVec = new Vector2(leaderToPos.x, 0);
                    adjVec.Normalize();
                    adjVec.scaleBy(adjLength);

                    if (m_follower.x < leaderPos.x)
                    {
                        m_follower.x += adjVec.x;
                        if (m_follower.x > leaderPos.x)
                        {
                            diff = Mathf.Abs(m_follower.x - leaderPos.x);
                            m_follower.x = leaderPos.x;

                            if (m_follower.y < leaderPos.y)
                            {
                                m_follower.y += diff;
                            }
                            else
                            {
                                m_follower.y -= diff;
                            }// end else                                

                        }// end if

                    }
                    else
                    {

                        m_follower.x += adjVec.x;
                        if (m_follower.x < leaderPos.x)
                        {
                            diff = Mathf.Abs(m_follower.x - leaderPos.x);
                            m_follower.x = leaderPos.x;

                            if (m_follower.y < leaderPos.y)
                            {
                                m_follower.y += diff;
                            }
                            else
                            {
                                m_follower.y -= diff;
                            }// end else                                    

                        }// end if                                  

                    }// end else                                    

                    changeX = true;
                    m_follower.m_vel.x = -m_leader.m_vel.y;
                    m_follower.m_vel.y = 0;
                    tY = (int)(m_follower.y / Game.TILE_HEIGHT);
                    m_follower.y = (Game.TILE_HEIGHT * tY) + (Game.TILE_HEIGHT / 2);
                }
                else
                {

                    if (leaderToPos.x < leaderToPos.y)
                    {
                        adjVec = new Vector2(leaderToPos.x, 0);
                        adjVec.Normalize();
                        adjVec.scaleBy(adjLength);

                        if (m_follower.x < leaderPos.x)
                        {
                            m_follower.x += adjVec.x;
                            if (m_follower.x > leaderPos.x)
                            {
                                diff = Mathf.Abs(m_follower.x - leaderPos.x);
                                m_follower.x = leaderPos.x;

                                if (m_follower.y < leaderPos.y)
                                {
                                    m_follower.y += diff;
                                }
                                else
                                {
                                    m_follower.y -= diff;
                                }// end else                                        

                            }// end if

                        }
                        else
                        {

                            m_follower.x += adjVec.x;
                            if (m_follower.x < leaderPos.x)
                            {
                                diff = Mathf.Abs(m_follower.x - leaderPos.x);
                                m_follower.x = leaderPos.x;

                                if (m_follower.y < leaderPos.y)
                                {
                                    m_follower.y += diff;
                                }
                                else
                                {
                                    m_follower.y -= diff;
                                }// end else                                        

                            }// end if                                  

                        }// end else                                        

                        changeX = true;
                        m_follower.m_vel.x = adjVec.x;
                        m_follower.m_vel.y = 0;
                        tY = (int)(m_follower.y / Game.TILE_HEIGHT);
                        m_follower.y = (Game.TILE_HEIGHT * tY) + (Game.TILE_HEIGHT / 2);
                    }
                    else //if (leaderToPos.x >= leaderToPos.y)
                    {
                        adjVec = new Vector2(0, leaderToPos.y);
                        adjVec.Normalize();
                        adjVec.scaleBy(adjLength);

                        if (m_follower.y < leaderPos.y)
                        {

                            m_follower.y += adjVec.y;
                            if (m_follower.y > leaderPos.y)
                            {
                                diff = Mathf.Abs(m_follower.y - leaderPos.y);
                                m_follower.y = leaderPos.y;

                                if (m_follower.x < leaderPos.x)
                                {
                                    m_follower.x += diff;
                                }
                                else
                                {
                                    m_follower.x -= diff;
                                }// end else                                    

                            }// end if

                        }
                        else
                        {

                            m_follower.y += adjVec.y;
                            if (m_follower.y < leaderPos.y)
                            {
                                diff = Mathf.Abs(m_follower.y - leaderPos.y);
                                m_follower.y = leaderPos.y;

                                if (m_follower.x < leaderPos.x)
                                {
                                    m_follower.x += diff;
                                }
                                else
                                {
                                    m_follower.x -= diff;
                                }// end else                                    

                            }// end if                                  

                        }// end else                                    

                        changeY = true;
                        m_follower.m_vel.x = 0;
                        m_follower.m_vel.y = adjVec.y;
                        tX = (int)(m_follower.x / Game.TILE_WIDTH);
                        m_follower.x = (Game.TILE_WIDTH * tX) + (Game.TILE_WIDTH / 2);
                    }// end else if

                }// end else

            }
            else
            {
                m_follower.m_vel.x = 0;
                m_follower.m_vel.y = 0;
            }// end else

        }// end else

        if (changeX)
        {
            m_follower.m_vel.y = 0;
            m_follower.m_vel.x = m_follower.x - currPos.x;
        }
        else if (changeY)
        {
            m_follower.m_vel.x = 0;
            m_follower.m_vel.y = m_follower.y - currPos.y;
        }// end else if

        if (m_follower.x >= Game.g.mapWidth * Game.TILE_WIDTH)
        {
            m_follower.x -= Game.g.mapWidth * Game.TILE_WIDTH;
        }
        else if (m_follower.x < 0)
        {
            m_follower.x += Game.g.mapWidth * Game.TILE_WIDTH;
        }// end else if

        if (m_follower.y >= Game.g.mapHeight * Game.TILE_HEIGHT)
        {
            m_follower.y -= Game.g.mapHeight * Game.TILE_HEIGHT;
        }
        else if (m_follower.y < 0)
        {
            m_follower.y += Game.g.mapHeight * Game.TILE_HEIGHT;
        }
    }
}
