﻿using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids2;
using UnityEngine;

public class BackgroundTile : GO
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        setGlow(Color.black);
    }

    public void setGlow(Color32 p_color)
    {
        m_renderers[1].color = p_color;
    }

    public Color32 getGlow()
    {
        return m_renderers[1].color;
    }
}

public static class GridPointUtils
{
    public static float Distance(this GridPoint2 p_origin, GridPoint2 p_point)
    {
        float l_x = Mathf.Pow(p_point.X - p_origin.X, 2);
        float l_y = Mathf.Pow(p_point.Y - p_origin.Y, 2);
        return Mathf.Sqrt(l_x + l_y);
    }

    public static GridPoint2 Direction(this GridPoint2 p_origin, GridPoint2 p_point)
    {
        return new GridPoint2(p_point.X - p_origin.X, p_point.Y - p_origin.Y);
    }
}
