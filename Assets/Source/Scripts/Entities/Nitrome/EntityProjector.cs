﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityProjector : EntityEnemy
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_speed = 2.0f;
        m_glow.m_color = 0xff00c3ff;
        m_recoverWaitDuration = 4f;
        m_checkDirMaxTimeout = 0.25f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_checksLineOfSight = true;
    }

    public override PathMap player1Map()
    {
        if (m_player1OnLineOfSight)
        {
            return Game.g.m_player1PathMap;
        }
        else
        {
            return Game.g.m_player1ProjectedPathMap;
        }
    }

    public override PathMap player2Map()
    {
        if (m_player1OnLineOfSight)
        {
            return Game.g.m_player2PathMap;
        }
        else
        {
            return Game.g.m_player2ProjectedPathMap;
        }
    }

}
