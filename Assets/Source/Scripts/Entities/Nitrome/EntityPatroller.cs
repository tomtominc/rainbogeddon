﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPatroller : EntityEnemy
{
    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        //m_speed = 1.5f;
        m_hitPoints = 45;
        m_maxHitPoints = m_hitPoints;
        m_glow.m_color = 0xff0045ff;
        m_recoverWaitDuration = 4f;
        m_checkDirMaxTimeout = 0.25f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_followsTarget = false;
    }

}
