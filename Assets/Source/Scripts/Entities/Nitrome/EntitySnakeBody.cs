﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySnakeBody : EntityEnemy
{
    public static int DAMAGE = 1;
    public Tail m_tail;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
    }

    public void setLeader(Entity p_leader, Entity p_root)
    {
        m_respawns = false;
        m_tail = new Tail(this, p_leader, p_root);
        m_collisionRect.x = -10;
        m_collisionRect.y = -10;
        m_collisionRect.width = 20;
        m_collisionRect.height = 20;
        m_damage = DAMAGE;
        m_glow.m_color = 0xffff3503;
        m_showDeathExplosion = false;
        m_canDie = false;
        m_canSmartFollow = false;
    }

    public override void onDeath(Entity p_attacker, Entity p_weapon)
    {
        base.onDeath(p_attacker, p_weapon);

        if (!m_tail.m_root.m_terminated)
        {
            m_tail.m_root.onDeath(p_attacker, p_weapon);
        }
    }

    public override void doPhysics()
    {
        if (m_tail == null)
        {
            return;
        }

        if (m_isDead)
        {
            return;
        }

        m_tail.doPhysics();

        updateDamage();

        m_prevVel.x = m_vel.x;
        m_prevVel.y = m_vel.y;

        m_cTX = x / Game.TILE_WIDTH;
        m_cTY = y / Game.TILE_HEIGHT;

        tickChildren();
        updateAnim();
        m_drawOrder = (int)(y + m_bounds.y + m_bounds.height);
    }
}
