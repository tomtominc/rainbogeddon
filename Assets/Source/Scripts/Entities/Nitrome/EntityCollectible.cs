﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityCollectible : EntityCentered
{
    protected int m_score;
    protected bool m_collected;

    public int score
    {
        get { return m_score; }
    }

    public bool collected
    {
        get { return m_collected; }
    }

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_canDie = false;
        m_collected = false;
        m_moves = false;
        m_score = 0;
    }

    public virtual void collect()
    {
        if (!m_collected)
        {
            m_collected = true;
            m_terminated = true;
            base.onDeath(null, null);
        }

    }

    public override void doPhysics()
    {
    }
}
