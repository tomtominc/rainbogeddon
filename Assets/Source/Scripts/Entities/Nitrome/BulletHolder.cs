﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHolder : WeaponHolder
{
    //public static const FIRE_DELAY:int = 250;

    public float m_fireStartTime;
    public float m_fireDelay;
    public float[] m_fireDelayTable;

    public BulletHolder(EntityCharacter p_parent)
        : base(p_parent)
    {
        m_fireStartTime = 0;
        m_fireDelayTable = new float[4];
        m_fireDelayTable[1] = 2.5f;
        m_fireDelayTable[2] = 1.5f;
        m_fireDelayTable[3] = 0.5f;
        m_fireDelay = m_fireDelayTable[1];
    }

    public override bool canFire()
    {
        //return (Game.g.timer - m_fireStartTime) > m_fireDelay;
        return (m_parent.m_lastDir != Entity.DIR_NONE && ((Game.g.timer - m_fireStartTime) > m_fireDelay));
    }// end public override function canFire():Boolean              

    public override bool tryFire()
    {
        if (canFire())
        {
            m_parent.m_fire = false;
            m_fireStartTime = Game.g.timer;

            float posX = 0;
            float posY = 0;
            float velX = 0;
            float velY = 0;

            EntityBullet bullet = null;

            if (m_parent is EntityPlayer2)
            {
                bullet = Game.g.createObject<EntityPlayer2Bullet>(Level.ENTITY_LAYER, "EntityPlayer2Bullet", m_parent.tx, m_parent.ty);
                bullet.x = m_parent.x;
                bullet.y = m_parent.y;

                bullet.setVelocity(0, 0);
                bullet.setPowerLevel(m_currUpgradeStage);

                (m_parent as EntityPlayer).doTemporaryStop();
            }
            else if (m_parent is EntityPlayer1)
            {
                bullet = Game.g.createObject<EntityPlayer2Bullet>(Level.ENTITY_LAYER, "EntityPlayer1Bullet", m_parent.tx, m_parent.ty);
                bullet.x = m_parent.x;
                bullet.y = m_parent.y;

                bullet.setVelocity(0, 0);
                bullet.setPowerLevel(m_currUpgradeStage);

                (m_parent as EntityPlayer).doTemporaryStop();
            }
            else if (m_parent is EntityShooter)
            {
                bullet = Game.g.createObject<EntityShooterBullet>(Level.ENTITY_LAYER, "EntityShooterBullet", m_parent.m_tx, m_parent.m_ty);
            }// end else if                 

            switch (m_parent.m_lastDir)
            {

                case Entity.DIR_UP:
                    {
                        //if (m_parent.m_vel.x.isZero())
                        //{
                        //    return false;
                        //}// end if
                        posX = m_parent.x;
                        posY = m_parent.y + 1f;
                        velX = 0;
                        velY = 1;
                        break;
                    }// end case

                case Entity.DIR_DOWN:
                    {
                        //if (m_parent.m_vel.x.isZero())
                        //{
                        //    return false;
                        //}// end if                      
                        posX = m_parent.x;
                        posY = m_parent.y - 1f;
                        velX = 0;
                        velY = -1;
                        break;
                    }// end case                    

                case Entity.DIR_LEFT:
                    {
                        //if (m_parent.m_vel.y.isZero())
                        //{
                        //    return false;
                        //}// end if                      
                        posX = m_parent.x - 1f;
                        posY = m_parent.y;
                        velX = -1;
                        velY = 0;
                        break;
                    }// end case        

                case Entity.DIR_RIGHT:
                    {
                        //if (m_parent.m_vel.y.isZero())
                        //{
                        //    return false;
                        //}// end if                          
                        posX = m_parent.x + 1f;
                        posY = m_parent.y;
                        velX = 1;
                        velY = 0;
                        break;
                    }// end case                        

            }// end switch


            bullet.m_vel.x = velX;
            bullet.m_vel.y = velY;
            bullet.m_vel = bullet.m_vel.normalized;
            bullet.m_vel = bullet.m_vel * bullet.m_speed;
            bullet.m_parent = m_parent;
            bullet.x = posX;
            bullet.y = posY;

            // ???
            //Entity.m_spatialHashMap.updateObject(bullet);

            if (m_parent is EntityPlayer2)
            {
                Player2ShootEffect player2ShootEffect = Game.g.createObject<Player2ShootEffect>("Effects", "Player2ShootEffect", (int)posX, (int)posY);
                player2ShootEffect.setParent(m_parent);
            }
            else if (m_parent is EntityPlayer1)
            {
                Player1ShootEffect player1ShootEffect = Game.g.createObject<Player1ShootEffect>("Effects", "Player1ShootEffect", (int)posX, (int)posY);
                player1ShootEffect.setParent(m_parent);
            }
            else if (m_parent is EntityShooter)
            {
                ShooterShootEffect shooterShootEffect = Game.g.createObject<ShooterShootEffect>("Effects", "ShooterShootEffect", (int)posX, (int)posY);
                shooterShootEffect.setParent(m_parent);
            }// end else if

            // already pushed into game object list when created
            //Game.g.m_gameObjectList.push(bullet);

            return true;

        }

        return false;

    }

    public override bool upgrade()
    {
        if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
        {
            base.upgrade();
            m_currUpgradeStage++;
            m_fireDelay = m_fireDelayTable[m_currUpgradeStage];
            return true;
        }
        else
        {
            return false;
        }
    }
}
