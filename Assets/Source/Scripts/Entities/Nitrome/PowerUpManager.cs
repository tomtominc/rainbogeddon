﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;
using System.Linq;

public class PowerUpManager
{

    private const float MIN_NO_SHOW_WAIT = 2f;
    private const float MAX_NO_SHOW_WAIT = 10f;
    private const float MIN_SHOW_WAIT = 10f;
    private const float MAX_SHOW_WAIT = 15f;
    private const float FLICKER_WAIT = 3f;

    private const int STATE_OFF = 0;
    private const int STATE_NOT_SHOWING = 1;
    private const int STATE_SHOWING = 2;
    private const int STATE_SHOWING_FLICKERING = 3;

    private float m_waitStartTime;
    private float m_waitDuration;
    private int m_state;
    private PowerUpSpawner[] m_powerUpSpawners;
    private PowerUp m_activePowerUp;
    private int m_chosenPosIndex;
    private bool m_firstShow;

    public PowerUpManager(List<GridPoint2> powerUpPosList_)
    {
        m_powerUpSpawners = Object.FindObjectsOfType<PowerUpSpawner>();
        m_powerUpSpawners = m_powerUpSpawners.OrderBy(x => x.isFirst ? 0 : 1).ToArray();
        m_state = STATE_OFF;
        m_firstShow = true;

    }

    public void start()
    {
        m_state = STATE_NOT_SHOWING;
        m_waitStartTime = Game.g.timer;
        m_waitDuration = Random.Range(MIN_NO_SHOW_WAIT, MAX_NO_SHOW_WAIT);
    }

    public void startShowing()
    {
        m_state = STATE_NOT_SHOWING;
        m_waitStartTime = Game.g.timer;
        m_waitDuration = 0;
    }

    public void tick()
    {

        if (m_powerUpSpawners == null || m_powerUpSpawners.Length <= 0)
        {
            return;
        }

        switch (m_state)
        {

            case STATE_NOT_SHOWING:
                {

                    if (Game.g.timer - m_waitStartTime > m_waitDuration)
                    {
                        m_state = STATE_SHOWING;
                        m_waitDuration = Random.Range(MIN_SHOW_WAIT, MAX_SHOW_WAIT);
                        m_waitStartTime = Game.g.timer;

                        // choose a random position from the list

                        if (m_firstShow || (m_powerUpSpawners.Length == 1))
                        {
                            m_firstShow = false;
                            m_chosenPosIndex = 0;
                        }
                        else
                        {

                            var testIndex = Random.Range(0, m_powerUpSpawners.Length);

                            while (testIndex == m_chosenPosIndex)
                            {
                                testIndex = Random.Range(0, m_powerUpSpawners.Length);
                            }// end while

                            m_chosenPosIndex = testIndex;

                        }// end else

                        PowerUpSpawner spawner = m_powerUpSpawners[m_chosenPosIndex];
                        m_activePowerUp = spawner.createPowerUp();

                    }

                    break;
                }

            case STATE_SHOWING:
                {
                    if (m_activePowerUp == null)
                    {
                        m_state = STATE_NOT_SHOWING;
                    }
                    else if ((m_waitDuration - (Game.g.timer - m_waitStartTime)) < FLICKER_WAIT)
                    {
                        m_state = STATE_SHOWING_FLICKERING;
                        m_activePowerUp.warnTermination();
                    }

                    break;
                }

            case STATE_SHOWING_FLICKERING:
                {

                    if (Game.g.timer - m_waitStartTime > m_waitDuration)
                    {
                        m_state = STATE_NOT_SHOWING;
                        m_waitDuration = Random.Range(MIN_NO_SHOW_WAIT, MAX_NO_SHOW_WAIT);
                        m_waitStartTime = Game.g.timer;
                        m_activePowerUp.collect();
                    }

                    break;
                }

        }

    }
}
