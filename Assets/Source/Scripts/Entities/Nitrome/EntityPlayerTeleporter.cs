﻿
public class EntityPlayerTeleporter : EntityTeleporter
{
    protected bool m_firstCollisionPlayerCheckPerformed;
    protected bool m_waitingPlayerUnCollide;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_firstCollisionPlayerCheckPerformed = false;
        m_waitingPlayerUnCollide = false;
    }

    public override void doPhysics()
    {
        if (m_terminated || (m_powerLevel <= 0))
        {
            return;
        }
    }

    public override void terminate()
    {
        m_terminated = true;

        if (m_powerLevel <= 0)
        {
            spawnEffect(disappearEffect());
        }

        m_tileMap.remove(Level.POWERUP_LAYER, tx, ty);
    }
}
