﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDrill : PowerUp
{
    private DrillHolder m_drillHolder;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);

        m_drillHolder = new DrillHolder(m_game.player1);
    }

    public override string soundID()
    {
        return "DrillSpeech";
    }

    public override string displayName()
    {
        return "DRILL";
    }

    public override WeaponHolder holder()
    {
        return m_drillHolder;
    }

    public override WeaponHolder getHolder(EntityCharacter p_parent)
    {
        m_drillHolder = new DrillHolder(p_parent);
        m_drillHolder.activate();
        return m_drillHolder;
    }
}
