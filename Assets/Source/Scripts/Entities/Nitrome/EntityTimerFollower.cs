﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityTimerFollower : EntityEnemy
{
    public static float PATROLLING_DURATION = 15f;
    public static float FOLLOWING_DURATION = 15f;
    public static float BREAK_FREE_WAIT_DURATION = 0.5f;
    public static float SPEED_NORMAL = 1.5f;
    public static float SPEED_FAST = EntityPlayer.SPEED * 0.7f;

    public float m_behaviorStartTime;
    public bool m_startFollowing;
    public bool m_endFollowing;
    private bool m_isStationaryWhileFree;
    private float m_stationaryWhileFreeStartTime;

    public override void initialize(ApplicationController p_controller, int p_tx, int p_ty)
    {
        base.initialize(p_controller, p_tx, p_ty);
        m_speed = SPEED_NORMAL;
        m_glow.m_color = 0xffffb500;
        m_recoverWaitDuration = 4f;
        m_checkDirMaxTimeout = 0.1f;
        m_checkDirTimeout = m_checkDirMaxTimeout;
        m_followsTarget = false;
        m_behaviorStartTime = Game.g.timer;
        m_startFollowing = false;
        m_endFollowing = false;
        m_hasBackAndForthDecPerc = false;
    }

    public override void doPhysics()
    {

        if (m_isDead)
        {
            return;
        }// end if          

        var xBefore = x;
        var yBefore = y;

        if (!m_endFollowing && !m_startFollowing)
        {


            if (!m_smartFollowing)
            {
                if (!m_followsTarget && (Game.g.timer - m_behaviorStartTime > PATROLLING_DURATION))
                {


                    startFollowing();
                    //m_behaviorStartTime = Game.g.timer;
                    //m_followsTarget = true;
                }
                else if (m_followsTarget && (Game.g.timer - m_behaviorStartTime > FOLLOWING_DURATION))
                {


                    endFollowing();
                    //m_behaviorStartTime = Game.g.timer;
                    //m_followsTarget = false;              
                }// end else if     

            }// end if

        }
        else
        {


            updateDamage();
        }// end if          


        base.doPhysics();

        // hack to stop freezing bug
        //if (!m_isStationaryWhileFree)
        //{

        //    if ((System.Math.Abs(xBefore - x) < float.Epsilon) && (System.Math.Abs(yBefore - y) < float.Epsilon))
        //    {
        //        m_isStationaryWhileFree = true;
        //        m_stationaryWhileFreeStartTime = Game.g.timer;
        //    }// end if

        //}
        //else
        //{

        //    if ((System.Math.Abs(xBefore - x) > float.Epsilon) || (System.Math.Abs(yBefore - y) > float.Epsilon))
        //    {
        //        m_isStationaryWhileFree = false;
        //    }
        //    else if ((Game.g.timer - m_stationaryWhileFreeStartTime) > BREAK_FREE_WAIT_DURATION)
        //    {
        //        //trace("TIMER FOLLOWER RESETTING");
        //        m_isStationaryWhileFree = false;
        //        m_followsTarget = false;
        //        m_targetInRange = false;
        //        m_smartFollowing = false;
        //        m_isAlerted = false;
        //        m_behaviorStartTime = Game.g.timer;
        //        m_endFollowing = false;
        //        m_startFollowing = false;
        //        m_forceUpdateAnim = true;
        //        m_speed = SPEED_NORMAL;
        //        m_vel = m_vel.normalized;
        //        m_vel.scaleBy(m_speed);
        //    }// end else if

        //}// end else            


    }// end public override function doPhysics():void

    public override void playSideWalkAnim()
    {
        if (!m_followsTarget)
        {
            gotoAndPlay("walk_side");
        }
        else
        {
            gotoAndPlay("walk_side_following");
        }// end else
    }// end public override function playSideWalkAnim():void

    public override void playUpWalkAnim()
    {
        if (!m_followsTarget)
        {
            gotoAndPlay("walk_up");
        }
        else
        {
            gotoAndPlay("walk_up_following");
        }
    }

    public override void playDownWalkAnim()
    {
        if (!m_followsTarget)
        {
            gotoAndPlay("walk_down");
        }
        else
        {
            gotoAndPlay("walk_down_following");
        }// end else                
    }// end public override function playDownWalkAnim():void            

    public void startFollowingDone()
    {
        stop();
        //trace("startFollowingDone");
        m_behaviorStartTime = Game.g.timer;
        m_startFollowing = false;
        m_endFollowing = false;
        m_forceUpdateAnim = true;
        m_glow.m_color = 0xff89ff0a;
    }// end public function startFollowingDone():void           

    public void startFollowing()
    {

        //trace("startFollowing");

        if (!m_followsTarget)
        {
            Debug.Log("getting angry!");

            gotoAndPlay("get_angry");
            m_behaviorStartTime = Game.g.timer;
            m_followsTarget = true;
            m_startFollowing = true;
            m_endFollowing = false;

        }
        else
        {
            startFollowingDone();
        }// end else

        //trace("EntityPlayer.SPEED 2");
        m_speed = SPEED_FAST;
        m_vel = m_vel.normalized;
        m_vel.scaleBy(m_speed);

    }// end public function startFollowing():void           

    public void endFollowing()
    {

        //trace("endFollowing");        

        gotoAndPlay("calm_down");
        m_behaviorStartTime = Game.g.timer;
        m_endFollowing = true;
        m_startFollowing = false;
        m_followsTarget = false;
        //trace("SPEED_NORMAL");
        m_speed = SPEED_NORMAL;
        m_vel = m_vel.normalized;
        m_vel.scaleBy(m_speed);

    }// end public function endFollowing():void     

    public void endFollowingDone()
    {
        //trace("endFollowingDone");
        stop();
        m_behaviorStartTime = Game.g.timer;
        m_endFollowing = false;
        m_startFollowing = false;
        m_forceUpdateAnim = true;
        m_glow.m_color = 0xffffb500;
    }// end public function endFollowingDone():void         

    /*public override function gotoAndPlay(frame:Object, scene:String = null):void
    {

        if (m_endFollowing || m_startFollowing)
        {
            trace("gotcha");
        }// end if

        super.gotoAndPlay(frame, scene);

    }// end public override function gotoAndPlay(frame:Object, scene:String = null):void*/

    public override void startAlert()
    {
        if (!m_endFollowing && !m_startFollowing && !m_followsTarget)
        {
            m_speed = SPEED_FAST;
            m_vel = m_vel.normalized;
            m_vel.scaleBy(m_speed);
            base.startAlert();
        }
    }

    public override void alertDone()
    {
        base.alertDone();
        startFollowing();
        m_isStationaryWhileFree = false;
    }

    public override void onStopSmartFollowing()
    {
        m_skipAnim = true;
        //trace("onStopSmartFollowing");
        endFollowing();
    }// end public override function onStopSmartFollowing():void            

    public override string alertedLabel()
    {

        if (m_followsTarget)
        {
            return "alerted_angry";
        }
        else
        {
            return "alerted";
        }// end else

    }// end public override function get alertedLabel():String      

    public override GameObject respawnEffectType()
    {
        return base.respawnEffectType();
    }

    public override GameObject destroyedEffectType()
    {
        return base.destroyedEffectType();
    }

}// end public class EntityTimerFollower
