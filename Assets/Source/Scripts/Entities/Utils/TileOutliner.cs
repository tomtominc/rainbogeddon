﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids2;

public class TileOutliner
{
    public const int LEFT = 0x1;
    public const int UP = 0x4;
    public const int RIGHT = 0x10;
    public const int DOWN = 0x40;

    public const int UPPER_LEFT = 0x2;
    public const int UPPER_RIGHT = 0x8;
    public const int LOWER_RIGHT = 0x20;
    public const int LOWER_LEFT = 0x80;

    public bool m_isWrapped;
    public int m_mapWidth;
    public int m_mapHeight;
    public Level m_level;
    public string m_tileMap;
    public int m_emptyTileValue;

    public TileOutliner()
    {
        m_isWrapped = true;
        m_tileMap = null;
        m_mapWidth = 0;
        m_mapHeight = 0;
    }

    private int getWrappedX(int x_)
    {
        if (m_isWrapped)
        {
            if (x_ >= m_mapWidth)
            {
                return x_ - m_mapWidth;
            }
            else if (x_ < 0)
            {
                return x_ + m_mapWidth;
            }
            else
            {
                return x_;
            }// end else
        }
        else
        {
            if (x_ >= m_mapWidth)
            {
                return -1;
            }
            else if (x_ < 0)
            {
                return -1;
            }
            else
            {
                return x_;
            }// end else                
        }// end else
    }// end private function getWrappedX(x_:int):int    

    private int getWrappedY(int y_)
    {
        if (m_isWrapped)
        {
            if (y_ >= m_mapHeight)
            {
                return y_ - m_mapHeight;
            }
            else if (y_ < 0)
            {
                return y_ + m_mapHeight;
            }
            else
            {
                return y_;
            }// end else
        }
        else
        {
            if (y_ >= m_mapHeight)
            {
                return -1;
            }
            else if (y_ < 0)
            {
                return -1;
            }
            else
            {
                return y_;
            }// end else                
        }// end else
    }// end private function getWrappedY(y_:int):int            

    public int getContourID(System.Type p_id, int p_tx, int p_ty)
    {
        GridPoint2 m_point = new GridPoint2(p_tx, p_tx);
        int l_north = hasEntity(p_id, new GridPoint2(m_point.X, m_point.Y + 1)) ? 1 : 0;
        int l_south = hasEntity(p_id, new GridPoint2(m_point.X, m_point.Y - 1)) ? 1 : 0;
        int l_east = hasEntity(p_id, new GridPoint2(m_point.X + 1, m_point.Y)) ? 1 : 0;
        int l_west = hasEntity(p_id, new GridPoint2(m_point.X - 1, m_point.Y)) ? 1 : 0;
        int l_northeast = hasEntity(p_id, new GridPoint2(m_point.X + 1, m_point.Y + 1)) && l_north != 0 && l_east != 0 ? 1 : 0;
        int l_northwest = hasEntity(p_id, new GridPoint2(m_point.X - 1, m_point.Y + 1)) && l_north != 0 && l_west != 0 ? 1 : 0;
        int l_southeast = hasEntity(p_id, new GridPoint2(m_point.X + 1, m_point.Y - 1)) && l_south != 0 && l_east != 0 ? 1 : 0;
        int l_southwest = hasEntity(p_id, new GridPoint2(m_point.X - 1, m_point.Y - 1)) && l_south != 0 && l_west != 0 ? 1 : 0;

        return l_west + (2 * l_northwest) + (4 * l_north) + (8 * l_northeast) + (16 * l_east) + (32 * l_southeast) + (64 * l_south) + (128 * l_southwest);
    }

    public bool hasEntity(System.Type p_id, GridPoint2 p_point)
    {
        return m_level.hasEntity(m_tileMap, p_point);
    }

    /*
     * Gets the corresponding id of the tile according to the ones surrounding it
     */
    //public int getContourID(int x_, int y_, bool corners_ = true)
    //{

    //if (m_tileMap == null)
    //{
    //    return 0;
    //}// end if

    //var iD = 0;

    //var x = getWrappedX(x_ - 1);
    //var y = getWrappedX(y_);

    //if ((x >= 0) && (y >= 0))
    //{
    //    if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //    {
    //        iD |= LEFT;
    //    }// end if
    //}// end if

    //x = getWrappedX(x_);
    //y = getWrappedX(y_ - 1);

    //if ((x >= 0) && (y >= 0))
    //{
    //    if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //    {
    //        iD |= UP;
    //    }// end if
    //}// end if          

    //x = getWrappedX(x_ + 1);
    //y = getWrappedX(y_);

    //if ((x >= 0) && (y >= 0))
    //{
    //    if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //    {
    //        iD |= RIGHT;
    //    }// end if
    //}// end if                      

    //x = getWrappedX(x_);
    //y = getWrappedX(y_ + 1);

    //if ((x >= 0) && (y >= 0))
    //{
    //    if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //    {
    //        iD |= DOWN;
    //    }// end if
    //}// end if                  


    //if (corners_)
    //{

    //    if (((iD & LEFT) == LEFT) && ((iD & UP) == UP))
    //    {

    //        x = getWrappedX(x_ - 1);
    //        y = getWrappedX(y_ - 1);

    //        if ((x >= 0) && (y >= 0))
    //        {
    //            if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //            {
    //                iD |= UPPER_LEFT;
    //            }// end if
    //        }// end if              

    //    }// end if


    //    if ((iD & RIGHT) == RIGHT && (iD & UP) == UP)
    //    {

    //        x = getWrappedX(x_ + 1);
    //        y = getWrappedX(y_ - 1);

    //        if ((x >= 0) && (y >= 0))
    //        {
    //            if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //            {
    //                iD |= UPPER_RIGHT;
    //            }// end if
    //        }// end if              

    //    }// end if          


    //    if ((iD & RIGHT) == RIGHT && (iD & DOWN) == DOWN)
    //    {

    //        x = getWrappedX(x_ + 1);
    //        y = getWrappedX(y_ + 1);

    //        if ((x >= 0) && (y >= 0))
    //        {
    //            if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //            {
    //                iD |= LOWER_RIGHT;
    //            }// end if
    //        }// end if              

    //    }// end if      


    //    if ((iD & LEFT) == LEFT && (iD & DOWN) == DOWN)
    //    {

    //        x = getWrappedX(x_ - 1);
    //        y = getWrappedX(y_ + 1);

    //        if ((x >= 0) && (y >= 0))
    //        {
    //            if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
    //            {
    //                iD |= LOWER_LEFT;
    //            }// end if
    //        }// end if              

    //    }// end if      

    //}// end if

    // return iD;

    //}// end public function getContourID(x_:int, y_:int):uint       

    /*
     * Generates all the possible ids and prints them
     * together with positions of adjacent tiles
     */
    //public void dumpIDSet(bool corners_ = true)
    //{

    //    var oldEmptyTilevalue = m_emptyTileValue;
    //    var oldMapWidth = m_mapWidth;
    //    var oldMapHeight = m_mapHeight;
    //    var oldTileMap = m_tileMap;

    //    m_emptyTileValue = 0;
    //    m_mapWidth = 3;
    //    m_mapHeight = 3;

    //    // test constants used only for internal reference
    //    // in this function when generating the test maps

    //    int LOWER_RIGHT_TEST = 0x1;
    //    int DOWN_TEST = 0x2;
    //    int LOWER_LEFT_TEST = 0x4;
    //    int RIGHT_TEST = 0x8;
    //    int LEFT_TEST = 0x20;
    //    int UPPER_RIGHT_TEST = 0x40;
    //    int UP_TEST = 0x80;
    //    int UPPER_LEFT_TEST = 0x100;

    //    int CENTER_TEST = 0x10;

    //    var generatedIDMap = new Dictionary<int, bool>();
    //    var iD = 0;
    //    var adjTileString = string.Empty;
    //    var numGeneratedIDs = 0;
    //    var lr = 0;
    //    var d = 0;
    //    var ll = 0;
    //    var r = 0;
    //    var l = 0;
    //    var ur = 0;
    //    var u = 0;
    //    var ul = 0;

    //    generatedIDMap.Add(iD, true);
    //    numGeneratedIDs++;

    //    for (var currCombination = 1; currCombination < 512; currCombination++)
    //    {

    //        adjTileString = "Adjacent tiles = ";
    //        m_tileMap = new List<int>(new int[9]);

    //        lr = 0;
    //        d = 0;
    //        ll = 0;
    //        r = 0;
    //        l = 0;
    //        ur = 0;
    //        u = 0;
    //        ul = 0;

    //        if ((currCombination & CENTER_TEST) == CENTER_TEST)
    //        {
    //            continue;
    //        }// end if

    //        if ((currCombination & DOWN_TEST) == DOWN_TEST)
    //        {
    //            d = 1;
    //            adjTileString += "DOWN, ";
    //            m_tileMap[3 * 2 + 1] = 1;
    //        }// end if  

    //        if ((currCombination & RIGHT_TEST) == RIGHT_TEST)
    //        {
    //            r = 1;
    //            adjTileString += "RIGHT, ";
    //            m_tileMap[3 * 1 + 2] = 1;
    //        }// end if  

    //        if ((currCombination & LEFT_TEST) == LEFT_TEST)
    //        {
    //            l = 1;
    //            adjTileString += "LEFT, ";
    //            m_tileMap[3 * 1 + 0] = 1;
    //        }// end if      

    //        if ((currCombination & UP_TEST) == UP_TEST)
    //        {
    //            u = 1;
    //            adjTileString += "UP, ";
    //            m_tileMap[3 * 0 + 1] = 1;
    //        }// end if              

    //        if (corners_)
    //        {

    //            if ((currCombination & LOWER_RIGHT_TEST) == LOWER_RIGHT_TEST)
    //            {
    //                lr = 1;
    //                adjTileString += "LOWER_RIGHT, ";
    //                m_tileMap[3 * 2 + 2] = 1;
    //            }// end if              

    //            if ((currCombination & LOWER_LEFT_TEST) == LOWER_LEFT_TEST)
    //            {
    //                ll = 1;
    //                adjTileString += "LOWER_LEFT, ";
    //                m_tileMap[3 * 2 + 0] = 1;
    //            }// end if                  

    //            if ((currCombination & UPPER_RIGHT_TEST) == UPPER_RIGHT_TEST)
    //            {
    //                ur = 1;
    //                adjTileString += "UPPER_RIGHT, ";
    //                m_tileMap[3 * 0 + 2] = 1;
    //            }// end if                                          

    //            if ((currCombination & UPPER_LEFT_TEST) == UPPER_LEFT_TEST)
    //            {
    //                ul = 1;
    //                adjTileString += "UPPER_LEFT, ";
    //                m_tileMap[3 * 0 + 0] = 1;
    //            }// end if                  

    //        }// end if  

    //        iD = getContourID(1, 1);

    //        if (iD > 0 && (!generatedIDMap.ContainsKey(iD) || generatedIDMap[iD] == false))
    //        {
    //            if (!generatedIDMap.ContainsKey(iD))
    //            {
    //                generatedIDMap.Add(iD, true);
    //            }

    //            generatedIDMap[iD] = true;
    //            numGeneratedIDs++;
    //        }// end if

    //    }// end for

    //    m_emptyTileValue = oldEmptyTilevalue;
    //    m_mapWidth = oldMapWidth;
    //    m_mapHeight = oldMapHeight;
    //    m_tileMap = oldTileMap;

    //}
}
