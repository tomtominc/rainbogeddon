﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[Serializable, XmlRoot("paramText")]
public class NitromeXMLParam
{
    [XmlAttribute("x")]
    public int x;
    [XmlAttribute("y")]
    public int y;
    [XmlAttribute("layer")]
    public string layer;
    [XmlText()]
    public string property;

    public override string ToString()
    {
        return string.Format("x={0}, y={1}, layer={2}, param={3}", x, y, layer, property);
    }
}
