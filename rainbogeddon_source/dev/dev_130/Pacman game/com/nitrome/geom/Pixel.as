﻿package com.nitrome.geom {
	
	/**
	 * ...
	 * @author Aaron Steed, nitrome.com
	 */
	public class Pixel {
		
		public var x:int;
		public var y:int;
		
		public function Pixel(x:int = 0, y:int = 0){
			this.x = x;
			this.y = y;
		}
		/* Manhattan distance */
		public function mDist(p:Pixel):Number{
			return Math.abs(p.x - x) + Math.abs(p.y - y);
		}
		public function toString():String {
			return "(" + x + "," + y + ")";
		}
		public function copy():Pixel{
			return new Pixel(x, y);
		}
		
	}
	
}