package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	

	public class QuitHelpButton extends BasicButton
	{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			NitromeGame.root.game.pauseGame();
		}
		
	}
	
}