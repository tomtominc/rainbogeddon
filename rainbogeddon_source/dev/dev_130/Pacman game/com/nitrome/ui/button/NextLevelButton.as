﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Advance Game.selectedLevel and goto the gameReset frame
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class NextLevelButton extends BasicButton{
		
		public function NextLevelButton() {
		}
		
		override public function onClick(e:MouseEvent):void{
			if (!NitromeGame.root.popUpHolder.keyPressed) 
			{
				super.onClick(e);
				execute();
			}// end if
		}
		public function execute():void {
			Game.selectedLevel.value++;
			NitromeGame.root.popUpHolder.hide();
			NitromeGame.root.transition.goto("gameReset");
		}
		
	}
	
}