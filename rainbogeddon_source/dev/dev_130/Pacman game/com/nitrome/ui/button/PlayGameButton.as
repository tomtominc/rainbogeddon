﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Start the game on the last level unlocked
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class PlayGameButton extends BasicButton{
			
		override public function onClick(e:MouseEvent):void{
			//Game.score.value = 0;
			super.onClick(e);
			var lastUnlocked:int = NitromeGame.getLastUnlocked();
			if(lastUnlocked < NitromeGame.totalLevels){
				Game.selectedLevel.value = lastUnlocked;
			} else {
				Game.selectedLevel.value = 1;
			}
			NitromeGame.root.transition.goto("chooseLevel");
		}
	}
	
}