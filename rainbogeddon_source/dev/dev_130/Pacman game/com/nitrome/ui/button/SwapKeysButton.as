package com.nitrome.ui.button {
	
	import com.nitrome.ui.PlayerSelectPanel;
	import flash.events.MouseEvent;

	public class SwapKeysButton extends BasicButton
	{
		
		public function SwapKeysButton()
		{
		}
		 
		override public function onClick(e:MouseEvent):void
		{
			super.onClick(e);
			execute();
		}
		
		public function execute():void
		{
			
			var buttonList:Vector.<KeyButton> = (parent as PlayerSelectPanel).keyButtonList;			
			
			for (var currButton:uint = 0; currButton < (buttonList.length/2); currButton++)
			{
				var buttonKeyTempVal:uint = buttonList[currButton].keyValue;
				buttonList[currButton].keyValue = buttonList[currButton + (buttonList.length/2)].keyValue;
				buttonList[currButton + (buttonList.length/2)].keyValue = buttonKeyTempVal;
				buttonList[currButton].deselect();
				buttonList[currButton + (buttonList.length/2)].deselect();
			}// end for
			
		}
		
	}// end public class SwapKeysButton extends BasicButton
}