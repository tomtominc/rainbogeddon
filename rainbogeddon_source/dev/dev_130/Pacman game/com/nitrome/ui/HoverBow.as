package com.nitrome.ui
{

	import flash.utils.getDefinitionByName;

	public class HoverBow extends MouseHoverElement
	{
				

		public function HoverBow()
		{
			
			m_menuLockPosList[0].x = 338; m_menuLockPosList[0].y = 338;
			m_menuLockPosList[1].x = 338; m_menuLockPosList[1].y = 373;
			m_menuLockPosList[2].x = 338; m_menuLockPosList[2].y = 404;
			m_menuLockPosList[3].x = 338; m_menuLockPosList[3].y = 435;	
			m_redOffset = 0xe8;
			m_greenOffset = 0x0;
			m_blueOffset = 0xff;			
				
		}// end public function HoverBow()
	
		public override function trailClass():Class
		{
			return getDefinitionByName("HoverBowTrail") as Class;
		}// end public override function trailClass():Class		
		
	}// end public class HoverBow extends MouseHoverElement
	
}// end package