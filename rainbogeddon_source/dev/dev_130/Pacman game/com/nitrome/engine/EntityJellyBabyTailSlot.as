package com.nitrome.engine
{
	
	
	public class EntityJellyBabyTailSlot extends EntityEnemy 
	{
			
		public var m_tail:Tail;	
		public var m_inUse:Boolean;
		public var m_index:uint;  // number indicating how close this slot is to the mother
		
		public function EntityJellyBabyTailSlot(leader_:Entity, root_:Entity, index_:uint)
		{
			m_canCollide = false;
			super(leader_.m_tx, leader_.m_ty);	
			m_tail = new Tail(this, leader_, root_);
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;	
			m_hasDecreasingTimeout = false;
			m_followsTarget = false;
			m_inUse = false;
			m_index = index_;
			m_spatialHashMap.unregisterObject(this);
		}// end public function EntityJellyBabyTailSlot(leader_:Entity, root_:Entity, index_:uint)
							
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			m_tail.doPhysics();
			
			m_prevVel.x = m_vel.x;
			m_prevVel.y = m_vel.y;			
			
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;	
					
		}// end public override function doPhysics():void					
		
	}// end public class EntityJellyBabyTailSlot
	
}// end package