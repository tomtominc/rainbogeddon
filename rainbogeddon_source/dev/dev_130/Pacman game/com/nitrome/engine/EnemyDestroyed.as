package com.nitrome.engine
{
	
	public class EnemyDestroyed extends EffectCentered
	{
				
		public function EnemyDestroyed(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);				
			
		}// end public function EnemyDestroyed(tx_:uint, ty_:uint)
		
	}// end public class EnemyDestroyed
	
}// end package