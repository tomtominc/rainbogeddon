package com.nitrome.engine
{
	
	
	public class EntityBlock extends Entity 
	{
	

		public function EntityBlock(tx_:uint, ty_:uint)
		{
			m_drawFast = true;
			super(tx_, ty_);
			m_collisionRect.x = 1;
			m_collisionRect.y = 1;
			m_collisionRect.width = 23;
			m_collisionRect.height = 23;
			m_moves = false;
			m_drawBitmap = Game.g.m_blocksBitmap;
			updateAbsCollisionRect();
			
			m_spatialHashMap.updateObject(this);
			
		}// end public function EntityBlock(tx_:uint, ty_:uint)
			
		public override function doPhysics():void
		{
			
			
		}// end override function doPhysics():void
		
	}// end public class EntityBlock
	
}// end package