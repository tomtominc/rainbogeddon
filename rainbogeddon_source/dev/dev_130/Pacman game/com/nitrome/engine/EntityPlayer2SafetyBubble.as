package com.nitrome.engine
{
		
	public class EntityPlayer2SafetyBubble extends EntitySafetyBubble
	{
				
		public function EntityPlayer2SafetyBubble(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);		

		}// end public function EntityPlayer2SafetyBubble(tx_:uint, ty_:uint)
		
		public override function get bubbleParticleType():Class
		{
			return Player2BubbleParticle;
		}// end public override function get bubbleParticleType():Class		
		
	}// end public class EntityPlayer2SafetyBubble
	
}// end package