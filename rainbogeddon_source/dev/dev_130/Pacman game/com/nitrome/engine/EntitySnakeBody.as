package com.nitrome.engine
{
	
	
	public class EntitySnakeBody extends EntityEnemy 
	{
		
		public static const DAMAGE:int = 1;
				
		public var m_tail:Tail;
		
		public function EntitySnakeBody(leader_:Entity, root_:Entity)
		{
			super(leader_.m_tx, leader_.m_ty);
			m_respawns = false;
			m_tail = new Tail(this, leader_, root_);
			m_collisionRect.x = -10;
			m_collisionRect.y = -10;
			m_collisionRect.width = 20;
			m_collisionRect.height = 20;
			m_damage = DAMAGE;		
			m_glow.m_color = 0xffff3503;
			m_showDeathExplosion = false;
			m_canDie = false;
			m_canSmartFollow = false;
			//m_glow.terminate();
		}// end public function EntitySnakeBody(leader_:Entity, root_:Entity)
			
		public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void
		{
			
			super.onDeath(attacker_, weapon_);
			if (!m_tail.m_root.m_terminated)
			{
				m_tail.m_root.onDeath(attacker_, weapon_);
			}// end if
			
		}// end public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void				
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			m_tail.doPhysics();
			
			updateDamage();
			
			m_prevVel.x = m_vel.x;
			m_prevVel.y = m_vel.y;			
			
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;			
			
			tickChildren();
			updateAnim();
			m_drawOrder = y + m_bounds.y + m_bounds.height;// m_cTY;
					
		}// end public override function doPhysics():void				
		
	}// end public class EntitySnakeBody
	
}// end package