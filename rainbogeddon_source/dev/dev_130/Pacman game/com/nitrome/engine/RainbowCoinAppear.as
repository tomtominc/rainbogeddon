package com.nitrome.engine
{
	

	
	public class RainbowCoinAppear extends GameObject
	{
				
		public function RainbowCoinAppear(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);		
			m_drawBitmap = Game.g.m_effectsBitmap;		
			
		}// end public function RainbowCoinAppear(tx_:uint, ty_:uint)
		
		public function OnAppearDone()
		{
		
			Game.g.m_gameObjectList.push(new EntityRainbowCoin(m_tx, m_ty));
			stop();
			m_terminated = true;
			
		}// end public function OnAppearDone()	
		
	}// end public class RainbowCoinAppear
	
}// end package