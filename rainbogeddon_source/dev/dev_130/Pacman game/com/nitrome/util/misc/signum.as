package com.nitrome.util.misc {
	/**
	 * Returns the sign of a number
	 */
	public function signum(num_:Number):int 
	{

		if (num_ < 0)
		{
			return -1;
		}
		else if (num_ > 0)
		{
			return 1;
		}// end else if
		
		return 0;
		
	}
}