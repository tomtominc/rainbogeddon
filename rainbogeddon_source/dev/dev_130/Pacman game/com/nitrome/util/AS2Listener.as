package com.nitrome.util {
	import flash.net.LocalConnection;
	/**
	 * ...
	 * @author Aaron Steed, nitrome.com
	 */
	public class AS2Listener {
		
		public static var localConnection:LocalConnection;
		public static var receiver:Object;
		
		public static function init(callId:String) {
			if(localConnection) localConnection.close();
			
			localConnection = new LocalConnection();
			receiver = {};
			localConnection.client = receiver;
			localConnection.connect(callId);
		}
		
		public static function close():void{
			localConnection.close();
			receiver = null;
			localConnection = null;
		}
		
		
	}

}