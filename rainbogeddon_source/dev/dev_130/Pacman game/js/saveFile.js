// This is the original file saving code - don't change it!!
// It was gathered from the following url if you need to repair it, but you need to alter the .js file to accept saving xml:
// <http://www.galasoft-lb.ch/myjavascript/CExplorer/>
var oExplorer = null;
function saveTextFile( strFullPath, strContent, bOverwrite ){
	if ( !oExplorer ){
		oExplorer = new gslb.CExplorer();
	}
	if ( window.location
		&& window.location.href
		&& window.location.href.indexOf( "http" ) == 0
		&& oExplorer.m_ePlatform == gslb.CExplorer.EPlatform.eComponents ){
		alert( "This fails in Spidermonkey in the http protocol."
		+ "\nDownload the cexplorer.demo.zip and install locally to use this in Spidermonkey." );
	}
	
	if ( oExplorer.m_ePlatform == gslb.CExplorer.EPlatform.eJava ){
		alert( "The code for Netscape 4 was commented out, however it is included in the file"
		+ "\cexplorer.extracts.js for historical reasons." );
	}
	if ( oExplorer.saveTextFile( strFullPath, strContent, bOverwrite ) ){
		//alert( "File was saved successfully" );
	} else {
		alert( "The file was NOT saved" );
	}
}
// This is just a test to see if it works
// It's really file name sensitive so watch out for that
/*
var file = window.location.href.substring(window.location.href.lastIndexOf(':')-1, window.location.href.lastIndexOf('/'));
file = file.replace(/\//g,"\\");
file = file.replace(/%20/g," ");
saveTextFile(file+"\\bloopy.txt", "testing", false);
*/
// This is the method we will use - it will account for local file saving by extrapolating the location the browser
// is launched from
// REMEMBER - THE FILE LOCATION MUST USE BACKSLASHES NOT FORWARD SLASHES
function saveXML(fileName, xmlString, overwrite){
	var file = window.location.href.substring(window.location.href.lastIndexOf(':')-1, window.location.href.lastIndexOf('/'));
	file = file.replace(/\//g,"\\");
	file = file.replace(/%20/g," ");
	if(fileName.substring(0, 1) != "\\"){
		fileName = "\\"+fileName;
	}
	fileName = fileName.replace(/\//g,"\\");
	saveTextFile(file+fileName, xmlString, overwrite);
	//alert(fileName);
	//saveTextFile(file+"\\bloopy.txt", "la", false);
}
function saveAndPlay(fileName, xmlString){
	var file = window.location.href.substring(window.location.href.lastIndexOf(':')-1, window.location.href.lastIndexOf('/'));
	file = file.replace(/\//g,"\\");
	file = file.replace(/%20/g," ");
	if(fileName.substring(0, 1) != "\\"){
		fileName = "\\"+fileName;
	}
	fileName = fileName.replace(/\//g,"\\");
	saveTextFile(file+fileName, xmlString, true);
	window.location.href='../index.html';
	//alert(fileName);
	//saveTextFile(file+"\\bloopy.txt", "la", false);
}
// convert forward slashes to backslashes and etc.
function fixFileName(fileName){
	return fileName;
}