package com.nitrome.engine
{
	
	
	public class EntityJellyMama extends EntityEnemy 
	{
		
		public var m_babySlotList:Vector.<EntityJellyBabyTailSlot>;
		public var m_babyList:Vector.<EntityJellyBaby>;
		public static var m_numInLevel:uint;
			
		public function EntityJellyMama(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);		
			m_checkDirMaxTimeout = MIN_CHECK_DIR_TIMEOUT;
			m_checkDirTimeout = m_checkDirMaxTimeout;	
			m_hasDecreasingTimeout = false;
			m_followsTarget = false;
			m_glow.m_color = 0xffe800ff;
			m_speed = 1.5;
			
			var numOrigBabies:uint = 5;
			var numBabies:uint = numOrigBabies;
			
			if (m_numInLevel)
			{
			
				// count the number of jelly babies in the level
				var numLivingBabies:uint = 0;
				
				for (var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
				{
					if ((Game.g.m_gameObjectList[currObject] is EntityJellyBaby) && !(Game.g.m_gameObjectList[currObject] as EntityJellyBaby).m_terminated)
					{
						numLivingBabies++;
					}// end if
				
				}// end for
				
				var numBabiesDiff:uint = (numBabies * m_numInLevel) - numLivingBabies;
				
				if (numBabiesDiff < numOrigBabies)
				{
					numBabies = numBabiesDiff;
				}// end if
				
			}// end if
			
			
			m_babyList = new Vector.<EntityJellyBaby>;
			
			m_babySlotList = new Vector.<EntityJellyBabyTailSlot>;
			
			if (numBabies)
			{
				
				m_babySlotList.push(new EntityJellyBabyTailSlot(this, this, m_babySlotList.length));	
				
				m_babyList.push(new EntityJellyBabyBoy(this));
				Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
				(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);				
				
				var isBoy:Boolean = false;
				
				for (var currBaby:uint = 1; currBaby < numBabies; currBaby++)
				{
					
					m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
					
					if (isBoy)
					{
						m_babyList.push(new EntityJellyBabyBoy(this));
					}
					else
					{
						m_babyList.push(new EntityJellyBabyGirl(this));
					}// end else
					
					Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
					(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);						
					
					isBoy = !isBoy;
					
				}// end for		
				
				if (numBabies < numOrigBabies)
				{
					
					var numRemainingSlots:uint = numOrigBabies - numBabies;
					
					for (var currSlot:uint = 0; currSlot < numRemainingSlots; currSlot++)
					{
						m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
					}// end for
					
				}// end if
			
			}// end if
			
			

			
			/*m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
			
			m_babyList.push(new EntityJellyBabyGirl(this));
			Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
			(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);		
	
			m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
			
			m_babyList.push(new EntityJellyBabyBoy(this));
			Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
			(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);		
			
			m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
			
			m_babyList.push(new EntityJellyBabyGirl(this));
			Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
			(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);		
						
			m_babySlotList.push(new EntityJellyBabyTailSlot(m_babySlotList[m_babySlotList.length - 1], this, m_babySlotList.length));
			
			m_babyList.push(new EntityJellyBabyBoy(this));
			Game.g.m_gameObjectList.push(m_babyList[m_babyList.length-1]);
			(m_babyList[m_babyList.length - 1] as EntityJellyBaby).attachToSlot(m_babySlotList[m_babySlotList.length - 1]);*/		
			
			
		}// end public function EntityJellyMama(tx_:uint, ty_:uint)
							
		public function removeBaby(baby_:EntityJellyBaby):void
		{
			
			for(var currChild:uint = 0; currChild < m_babyList.length; currChild++)
			{			
				if (m_babyList[currChild] == baby_)
				{
					m_babyList.splice(currChild, 1);
					break;
				}// end if
			}// end for
					
		}// end public function removeBaby(baby_:EntityJellyBaby):void			
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			super.doPhysics();
			
			// the slots aren't added to the game object list to avoid
			// interaction with the player, so they have to be updated manually
			for (var currSlot:uint = 0; currSlot < m_babySlotList.length; currSlot++)
			{
				m_babySlotList[currSlot].doPhysics();
			}// end for
					
		}// end public override function doPhysics():void			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnPink;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedPink;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityJellyMama
	
}// end package