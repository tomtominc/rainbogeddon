package com.nitrome.engine
{
	
	public class AlertBackground extends GameObject
	{
		
		public static const STATE_FADING_IN:uint  = 0;
		public static const STATE_NORMAL:uint     = 1;		
		public static const STATE_FADING_OUT:uint = 2;		
		
		public static const FADE_SPEED:Number = 0.025;		
		
		private var m_state:uint;
				
		public function AlertBackground()
		{
		
			super(0, 0);		
			alpha = 0;
			m_state = STATE_FADING_IN;
			m_drawBitmap = Game.g.m_corridorBitmap;		
			
		}// end public function AlertBackground()
		
		public override function doPhysics():void
		{
			
			
			switch(m_state)
			{
				
				case STATE_FADING_IN:
				{
				
					alpha += FADE_SPEED;
					
					if (alpha >= 1)
					{
						alpha = 1;
						m_state = STATE_NORMAL;
					}// end if
					
					break;
				}// end case
				
				case STATE_FADING_OUT:
				{
				
					alpha -= FADE_SPEED;
					
					if (alpha <= 0)
					{
						alpha = 0;
						m_terminated = true;
					}// end if								
					
					break;
				}// end case				
				
			}// end switch
									
		}// end public override function doPhysics():void					
		
		public function terminate():void
		{
			
			m_state = STATE_FADING_OUT;
									
		}// end public function terminate():void			
	
		
	}// end public class AlertBackground
	
}// end package