package com.nitrome.engine
{
	
	public class JellyBabyFoundMother extends EffectCentered
	{
		
		private var m_parentEntity:EntityJellyBaby;
		
		public function JellyBabyFoundMother(parent_:EntityJellyBaby)
		{
		
			m_drawFast = true;
			super(0, 0);		
			m_parentEntity = parent_;
			
		}// end public function JellyBabyFoundMother(parent_:EntityJellyBaby)
		
		public override function doPhysics():void
		{
			
			x = m_parentEntity.x;
			y = m_parentEntity.y - 20;
			
		}// end public override function doPhysics():void
		
	}// end public class JellyBabyFoundMother
	
}// end package