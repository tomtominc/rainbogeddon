package com.nitrome.engine
{
	
	import flash.display.MovieClip; 
		
	public class Player1BubbleParticle extends EntityBubbleParticle
	{
				
		public function Player1BubbleParticle(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);		

		}// end public function Player1BubbleParticle(tx_:uint, ty_:uint)
		
		public override function get bubbleContainerType():Class
		{
			return Player1BubbleParticleContainer;
		}// end public override function get bubbleContainerType():Class		
		
		/*public override function get container():MovieClip
		{
			return _container;
		}// end public override function get container():MovieClip*/		
		
	}// end public class Player1BubbleParticle
	
}// end package