package com.nitrome.engine
{

	
	public class EntityPlayerTeleporter extends EntityTeleporter
	{

		private var m_firstCollisionPlayerCheckPerformed:Boolean;
		private var m_waitingPlayerUnCollide:Boolean;
		
		public function EntityPlayerTeleporter(tx_:uint, ty_:uint, powerLevel_:uint)
		{
			super(tx_, ty_, powerLevel_);
			m_firstCollisionPlayerCheckPerformed = false;
			m_waitingPlayerUnCollide = false;
		}// end public function EntityPlayerTeleporter(tx_:uint, ty_:uint, powerLevel_:uint)
		
		public override function doPhysics():void
		{
			
			
			if (m_terminated || (m_powerLevel != -1))
			{
				return;
			}// end if		
			

			
			if (!m_firstCollisionP1CheckPerformed)
			{
				
				m_firstCollisionP1CheckPerformed = true;
				
				if (collidesWithObject(Game.g.m_player1))
				{
					m_waitingPlayer1UnCollide = true;
				}// end if
						
			}
			else
			{
				
				if (m_waitingPlayer1UnCollide)
				{
					if (!collidesWithObject(Game.g.m_player1))
					{
						m_waitingPlayer1UnCollide = false;
					}// end if
				}
				else
				{
					if (collidesWithObject(Game.g.m_player1))
					{
						if (!Game.g.m_player1.isTeleporting)
						{
							m_firstCollisionP1CheckPerformed = false;
							//trace("teleporting");
							Game.g.m_player1.teleportFrom(this);
						}// end if
					}// end if					
				}// end else
							
			}// end else
		

			
			if (!m_firstCollisionP2CheckPerformed)
			{
				
				m_firstCollisionP2CheckPerformed = true;
						
				if (collidesWithObject(Game.g.m_player2))
				{
					m_waitingPlayer2UnCollide = true;
				}// end if				
				
			}
			else
			{
							
				if (m_waitingPlayer2UnCollide)
				{
					if (!collidesWithObject(Game.g.m_player2))
					{
						m_waitingPlayer2UnCollide = false;
					}// end if					
				}
				else
				{
					if (collidesWithObject(Game.g.m_player2))
					{
						if (!Game.g.m_player2.isTeleporting)
						{						
							m_firstCollisionP2CheckPerformed = false;
							//trace("teleporting");
							Game.g.m_player2.teleportFrom(this);
						}// end if
					}// end if						
				}// end else				
				
			}// end else	
			
	
			
		}// end public override function doPhysics():void			
		
		public override function terminate():void
		{
			m_terminated = true;
			if (m_powerLevel == -1)
			{
				Game.g.m_gameObjectList.push(new disappearEffect(m_tx, m_ty));
			}// end if
			m_spatialHashMap.unregisterObject(this);
		}// end public override function terminate():void			
		
	}// end public class EntityPlayerTeleporter
	
}// end package