package com.nitrome.engine
{

	import flash.geom.Vector3D;
	
	public class TeleporterHolder extends WeaponHolder
	{
		
		public static const FIRE_DELAY:int = 125;
		
		private var m_fireStartTime:int;
		private var m_teleporterList:Vector.<EntityTeleporter>;
		
		public function TeleporterHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_fireStartTime = 0;
			m_teleporterList = new Vector.<EntityTeleporter>;
		}// end public function TeleporterHolder()
				
	
		public override function tryFire():Boolean
		{
			
			m_parent.m_fire = false;			

			refreshList();
			
			
			var isOnTeleporter:Boolean = false;
			
			if (m_teleporterList.length)
			{
			
				var tileCenterVec:Vector3D = new Vector3D((m_teleporterList[0].m_cTX * Game.TILE_WIDTH) + (Game.TILE_WIDTH / 2), (m_teleporterList[0].m_cTY * Game.TILE_HEIGHT) + (Game.TILE_HEIGHT / 2));
				var posVec:Vector3D = new Vector3D(m_parent.x, m_parent.y);
				var posToTileVec:Vector3D = posVec.subtract(tileCenterVec);
				
				if (posToTileVec.length <= 0.1)
				{
					
					isOnTeleporter = true;

				}// end if			
				
			}// end if
			
			if (isOnTeleporter)
			{
			
				if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
				{
					
					m_fireStartTime = Game.g.timer;				
				
					// player is spinning on the root teleporter, so eliminate it
					(m_parent as EntityPlayer).abortTeleport();
					terminateTeleporters();
					
					return true;
				
				}// end if
				
				
			}
			else if(m_teleporterList.length < 2)
			{
				
				if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
				{
					m_fireStartTime = Game.g.timer;	
					
					if (m_parent is EntityPlayer1)
					{
						m_teleporterList.push(new EntityPlayer1Teleporter(m_parent.m_tx, m_parent.m_ty, !m_teleporterList.length ? -1 : m_currUpgradeStage));
					}
					else
					{
						m_teleporterList.push(new EntityPlayer2Teleporter(m_parent.m_tx, m_parent.m_ty, !m_teleporterList.length ? -1 : m_currUpgradeStage));
					}// end else
					
					Game.g.m_gameObjectList.push(m_teleporterList[m_teleporterList.length - 1]);
					
					if (m_teleporterList.length == 2)
					{
						m_teleporterList[0].m_twin = m_teleporterList[1];
						m_teleporterList[1].m_twin = m_teleporterList[0];			
						//trace("actually teleporting");
						(m_parent as EntityPlayer).teleportFrom(m_teleporterList[1], true);
					}
					else
					{
						m_teleporterList[0].gotoAndPlay("old");
					}// end if
					
					return true;
					
				}// end if				
				
			}
			else if(m_teleporterList[1].m_canDisable)
			{
				
				m_teleporterList[1].terminate();
				
				if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
				{
					m_fireStartTime = Game.g.timer;	
					
					if (m_parent is EntityPlayer1)
					{
						m_teleporterList.push(new EntityPlayer1Teleporter(m_parent.m_tx, m_parent.m_ty, m_currUpgradeStage));
					}
					else
					{
						m_teleporterList.push(new EntityPlayer2Teleporter(m_parent.m_tx, m_parent.m_ty, m_currUpgradeStage));
					}// end else					
					
					Game.g.m_gameObjectList.push(m_teleporterList[m_teleporterList.length - 1]);
					
					m_teleporterList[2].m_twin = m_teleporterList[0];
					m_teleporterList[0].m_twin = m_teleporterList[2];
					//trace("actually teleporting");
					(m_parent as EntityPlayer).teleportFrom(m_teleporterList[2], true);
					
					return true;
					
				}// end if				
				
			}// end else			
			
			return false;
			
		}// end public override function tryFire():Boolean			
		
		public function refreshList():void
		{
			
			for (var currTeleporter:int = 0; currTeleporter < m_teleporterList.length; currTeleporter++)
			{
				if (m_teleporterList[currTeleporter].m_terminated)
				{
					m_teleporterList.splice(currTeleporter, 1);
					currTeleporter--;
				}// end if
			}// end if
			
		}// end public function refreshList():void		
		
		public override function terminate():void
		{
			
			terminateTeleporters();	
			
		}// end public override function terminate():void				
		
		private function terminateTeleporters():void
		{
			
			for (var currTeleporter:uint = 0; currTeleporter < m_teleporterList.length; currTeleporter++)
			{
				if (!m_teleporterList[currTeleporter].m_terminated)
				{
					m_teleporterList[currTeleporter].terminate();
				}// end if
			}// end if			
			
		}// end private function terminateTeleporters():void		
		
		public override function upgrade():Boolean
		{
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				for (var currTeleporter:uint = 0; currTeleporter < m_teleporterList.length; currTeleporter++)
				{
					if (m_teleporterList[currTeleporter].powerLevel >= 1)
					{
						m_teleporterList[currTeleporter].powerLevel = m_currUpgradeStage;
					}// end if
				}// end if				
				return true;
			}
			else
			{
				return false;
			}// end else
			
		}// end public override function upgrade():Boolean			
							
		public override function onTeleportDone():void
		{

			terminateTeleporters();
			
		}// end public override function onTeleportDone():void			
		
	}// end public class TeleporterHolder
	
}// end package