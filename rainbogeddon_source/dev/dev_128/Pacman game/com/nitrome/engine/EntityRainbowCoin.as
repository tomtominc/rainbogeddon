package com.nitrome.engine
{
	import flash.geom.Rectangle;
	
	
	public class EntityRainbowCoin extends EntityCoin
	{
		
		private var m_appeared:Boolean;
		private var m_disappearing:Boolean;
		private var m_intersectionLock:Boolean; // used when the player enters the area that will be occupied by a block later but not necessarily colliding with the actual coin
		private var m_intersectionLockRect:Rectangle;
		private var m_queueDisappear:Boolean;
		private var m_switchingToBlock:Boolean;
		
		public function EntityRainbowCoin(tx_:uint, ty_:uint, size_:uint = 1)
		{
			super(tx_, ty_, size_);	
			m_queueDisappear = false;
			m_appeared = false;
			m_disappearing = false;
			m_intersectionLock = false;
			m_switchingToBlock = false;
			m_intersectionLockRect = new Rectangle( -12.5, -12.5, 25, 25);
			Game.g.m_transformingRainbowCoin = true;
			Game.g.m_rainbowCoinTransMap[m_ty * Game.g.mapWidth + m_tx] = 1;
		}// end public function EntityRainbowCoin(tx_:uint, ty_:uint, size_:uint = 1)
		
		public function OnAppearDone():void
		{
		
			m_appeared = true;	
			Game.g.m_transformingRainbowCoin = false;
			Game.g.m_rainbowCoinTransMap[m_ty*Game.g.mapWidth + m_tx] = 0;			
			
		}// end public function OnAppearDone():void			
		
		public override function collect()
		{
			if (!m_collected)
			{
				super.collect();
				Game.g.m_rainbowCoinTransMap[m_ty*Game.g.mapWidth + m_tx] = 0;
			}// end if
		}// end public override function collect()			
		
		public override function doPhysics():void
		{
			
			if (m_appeared && !m_disappearing)
			{
				
				super.doPhysics();				
				
				var playerCollisionRect:Rectangle = new Rectangle();
				var thisCollisionRect:Rectangle = new Rectangle(x + m_intersectionLockRect.x, y + m_intersectionLockRect.y, m_intersectionLockRect.width, m_intersectionLockRect.height);
				var player:EntityPlayer;
				m_intersectionLock = false;
						
				if (!Game.g.m_player1.m_terminated)	
				{
						
					player = Game.g.m_player1;
					
					playerCollisionRect.x = player.x + player.m_collisionRect.x + player.m_vel.x;
					playerCollisionRect.y = player.y + player.m_collisionRect.y + player.m_vel.y;
					playerCollisionRect.width = player.m_collisionRect.width;
					playerCollisionRect.height = player.m_collisionRect.height;					
				
					if (thisCollisionRect.intersects(playerCollisionRect))
					{
				
						m_intersectionLock = true;					
					
					}// end if				
				
				}// end if
				
				if (!Game.g.m_player2.m_terminated)	
				{
						
					player = Game.g.m_player2;
					
					playerCollisionRect.x = player.x + player.m_collisionRect.x + player.m_vel.x;
					playerCollisionRect.y = player.y + player.m_collisionRect.y + player.m_vel.y;
					playerCollisionRect.width = player.m_collisionRect.width;
					playerCollisionRect.height = player.m_collisionRect.height;					
				
					if (thisCollisionRect.intersects(playerCollisionRect))
					{
				
						m_intersectionLock = true;
					
					}// end if
				
				}// end if				
				
				if (!m_intersectionLock && m_queueDisappear && !m_disappearing)
				{
					//trace("queued 2");
					switchToSoftBlock();
					
				}// end if
				
			}// end if
			
		}// end public override function doPhysics():void			
		
		public function switchToSoftBlock():void
		{
			
			if (!m_switchingToBlock)
			{
			
				m_switchingToBlock = true;
				
				if (!m_intersectionLock)
				{

					var block:EntitySoftBlock = new EntitySoftBlock(m_tx, m_ty, true);
					Game.g.m_gameObjectList.push(block);
					stop();
					Game.m_numCoinsInLevel.value--;
					Game.g.onCoinCollected();
					m_terminated = true;
					m_spatialHashMap.unregisterObject(this);
					Game.g.m_tileMap.onMapChanged();
					Game.g.m_drawTileMap.onMapChanged();
					
				}
				else
				{
					//trace("queued 1");
					m_queueDisappear = true;
				}// end else
			
			}// end if
			
		}// end public function switchToSoftBlock():void		
		
	}// end public class EntityRainbowCoin
	
}// end package