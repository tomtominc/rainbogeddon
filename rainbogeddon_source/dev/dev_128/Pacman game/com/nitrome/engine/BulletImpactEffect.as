package com.nitrome.engine
{
	
	public class BulletImpactEffect extends EffectCentered
	{
					
		public function BulletImpactEffect(posX_:Number, posY_:Number, powerLevel_:uint)
		{
		
			super(0, 0);
			x = posX_;
			y = posY_;
			
			var type:uint;

			if (powerLevel_ == 2)
			{
				type = 1 + Math.floor(Math.random() * 3);
				gotoAndPlay("level_" + powerLevel_ + "_" + type);
			}// end if
			else if(powerLevel_ == 3)
			{
				type = 1 + Math.floor(Math.random() * 5);
				gotoAndPlay("level_" + powerLevel_ + "_" + type);				
			}// end else
			
		}// end public function BulletImpactEffect(posX_:Number, posY_:Number, powerLevel_:uint)
			
	}// end public class BulletImpactEffect
	
}// end package