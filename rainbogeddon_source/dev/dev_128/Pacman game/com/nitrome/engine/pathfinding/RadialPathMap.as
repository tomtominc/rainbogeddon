package com.nitrome.engine.pathfinding 
{
	
	import com.nitrome.geom.PointUint;
	import com.nitrome.engine.Entity;
	import com.nitrome.util.misc.signum;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Vector3D;
	import flash.utils.getDefinitionByName;
	import flash.geom.*;
	
	public class RadialPathMap
	{
		
		public static const TILE_MARKED:uint      = 0xff000001;
		public static const TILE_MARKED_OLD:uint  = 0x00000002;
		
		public  static const TILE_BLOCKED:uint   = 0x00ff0000;
		private static const CIRCLE_COLOR:uint   = PathMap.TILE_SENTINEL;
		
		public  var m_map:Vector.<uint>;
		public  var m_expandingPathMap:Vector.<uint>;
		public  var m_width:uint;
		public  var m_height:uint;
		public  var m_radius:uint;
		public  var m_x:int;
		public  var m_y:int;
		public  var m_expandingRadius:uint;
		public  var m_blockedTileValue:uint;    // the value corresponding to a blocked tile
		private var m_wraps:Boolean;
		public  var m_expandingPathClip:MovieClip;
		public  var m_expandingPathBitmap:BitmapData;
		public  var m_oldExpandingPathBitmap:BitmapData;
		public  var m_mapBitmap:BitmapData;
		public  var m_tileMarkedBitmap:BitmapData;
		public  var m_finalBitmap:BitmapData;
		private var m_cellOffset:uint;
		private var m_trySetCellFunc:Function;
		//private var m_calcCellOffsetFunc:Function;
		//public  var m_getCellFunc:Function;
		//public  var m_setCellFunc:Function;
		private static var m_expandingPathBitmapList:Vector.<BitmapData>;
		private static var m_expandingPathBitmapBoundsList:Vector.<Rectangle>;
		
		public function RadialPathMap(width_:uint, height_:uint, blockedTileValue_:uint)
		{
			
			m_map = new Vector.<uint>(width_ * height_);
			m_expandingPathMap = new Vector.<uint>(width_ * height_);
			m_width  = width_;
			m_height = height_;
			m_blockedTileValue = blockedTileValue_;
			m_wraps = true;
			m_trySetCellFunc = trySetCell;
			//m_calcCellOffsetFunc = calcCellOffset;
			//m_setCellFunc = setCell;
			//m_getCellFunc = getCell;			
			m_expandingRadius = 1;
			m_expandingPathBitmap = new BitmapData(width_, height_, true, TILE_BLOCKED); // BLOCKED_COLOR
			
			if (!m_expandingPathBitmapList)
			{
				
				m_expandingPathBitmapList = new Vector.<BitmapData>;
				m_expandingPathBitmapBoundsList = new Vector.<Rectangle>;
				
				var matrix:Matrix = new Matrix();
				
				for (var currSize:uint = 0; currSize < 8; currSize++)
				{
				
					m_expandingPathClip = new (getDefinitionByName("BombCircle" + currSize) as Class)();
					m_expandingPathBitmapList.push(new BitmapData(m_expandingPathClip.width, m_expandingPathClip.height, true, 0x0));
					m_expandingPathBitmapBoundsList.push(m_expandingPathClip.getBounds(m_expandingPathClip));
					matrix.identity();
					matrix.translate(-m_expandingPathBitmapBoundsList[currSize].x, -m_expandingPathBitmapBoundsList[currSize].y);
					m_expandingPathBitmapList[currSize].draw(m_expandingPathClip, matrix);
					m_expandingPathBitmapList[currSize].threshold(m_expandingPathBitmapList[currSize], m_expandingPathBitmapList[currSize].rect, new Point(0,0), "==", 0xff000000, CIRCLE_COLOR, 0xffffffff);
				
				}// end for		
				
			}// end if
								
		}// end public function RadialPathMap(width_:uint, height_:uint, blockedTileValue_:int)
		
		public static function free():void		
		{
			m_expandingPathBitmapList = null;
			m_expandingPathBitmapBoundsList = null;
		}// end public static function free():void	
		
		/* *
		 * Returns a vector with all the new positions of the tiles
		 * that lie within the path and marks them to avoid getting the same ones
		 * the next time the function is called
		 * */
		public function getNewMarkedPathPosList():Vector.<PointUint>
		{
			
			var posX:uint = 0;
			var posY:uint = 0;
			
			var posList:Vector.<PointUint> = new Vector.<PointUint>;
			
			for (var currTile:uint = 0; currTile < m_expandingPathMap.length; currTile++)
			{
								
				if (m_expandingPathMap[currTile] == TILE_MARKED)
				{
					
					posY = currTile / m_width;
					posX = currTile - (posY * m_width);					
					
					m_expandingPathMap[currTile] = TILE_MARKED_OLD;
					posList.push(new PointUint(posX, posY));
				}// end if
				
			}// end for
			
			return posList;
			
		}// end public function getNewMarkedPathPosList():Vector.<PointUint>		
		
		public function set wraps(val_:Boolean):void
		{
			
			m_wraps = val_;
			
			if (m_wraps)
			{
				m_trySetCellFunc = trySetCell;
				//m_calcCellOffsetFunc = calcCellOffset;
				//m_setCellFunc = setCell;
				//m_getCellFunc = getCell;
			}
			else
			{
				m_trySetCellFunc = trySetCellNoWrapping;
				//m_calcCellOffsetFunc = calcCellOffsetNoWrap;
				//m_setCellFunc = setCellNoWrap;
				//m_getCellFunc = getCellNoWrap;				
			}// end else
			
		}// end public function set wraps(val_:Boolean):void
		
		public function calcCellOffset(x_:int, y_:int):Boolean
		{
			
			if (x_ < 0)
			{
				x_ += m_width;
			}
			else if (x_ >= m_width)
			{
				x_ -= m_width;
			}// end else if
			
			if (y_ < 0)
			{
				y_ += m_height;
			}
			else if (y_ >= m_height)
			{
				y_ -= m_height;
			}// end else if			
			
			m_cellOffset = (y_ * m_width) + x_;
			
			return true;
			
		}// end public function calcCellOffset(x_:int, y_:int):Boolean		
		
		public function calcCellOffsetNoWrap(x_:int, y_:int):Boolean
		{
			
			if ((x_ < 0) || (x_ >= m_width) || (y_ < 0) || (y_ >= m_height))
			{
				return false;
			}// end if
			
			m_cellOffset = (y_ * m_width) + x_;
			
			return true;
			
		}// end public function calcCellOffsetNoWrap(x_:int, y_:int):Boolean			
		
		public function getCell(x_:int, y_:int):int
		{
			
			if (x_ < 0)
			{
				x_ += m_width;
			}
			else if (x_ >= m_width)
			{
				x_ -= m_width;
			}// end else if
			
			if (y_ < 0)
			{
				y_ += m_height;
			}
			else if (y_ >= m_height)
			{
				y_ -= m_height;
			}// end else if			
			
			return m_map[(y_ * m_width) + x_];
			
		}// end public function getCell(x_:int, y_:int):int		
		
		public function setCell(x_:int, y_:int):void
		{
			
			if (x_ < 0)
			{
				x_ += m_width;
			}
			else if (x_ >= m_width)
			{
				x_ -= m_width;
			}// end else if
			
			if (y_ < 0)
			{
				y_ += m_height;
			}
			else if (y_ >= m_height)
			{
				y_ -= m_height;
			}// end else if			
			
			m_map[(y_ * m_width) + x_] = TILE_MARKED;

			
		}// end public function setCell(x_:int, y_:int):void	
		
		
		public function getCellNoWrap(x_:int, y_:int):int
		{
			
			if ((x_ < 0) || (x_ >= m_width) || (y_ < 0) || (y_ >= m_height))
			{
				return m_blockedTileValue;
			}// end if
			
			return m_map[(y_ * m_width) + x_];
			
		}// end public function getCellNoWrap(x_:int, y_:int):int		
		
		public function setCellNoWrap(x_:int, y_:int):void
		{
			
			if ((x_ < 0) || (x_ >= m_width) || (y_ < 0) || (y_ >= m_height))
			{
				return;
			}// end if
				
			m_map[(y_ * m_width) + x_] = TILE_MARKED;
			
		}// end public function setCellNoWrap(x_:int, y_:int):void	
		
		
		/*
		 * checks to make sure the path isn't blocked 
		 * and the pixel can be set
		 */
		public function trySetCell(x_:int, y_:int):Boolean
		{
			
			//m_calcCellOffsetFunc(x_, y_);
			//calcCellOffset(x_, y_);
			
			if (x_ < 0)
			{
				x_ += m_width;
			}
			else if (x_ >= m_width)
			{
				x_ -= m_width;
			}// end else if
			
			if (y_ < 0)
			{
				y_ += m_height;
			}
			else if (y_ >= m_height)
			{
				y_ -= m_height;
			}// end else if			
			
			m_cellOffset = (y_ * m_width) + x_;			
			
			/*if (m_getCellFunc(x_, y_) != m_blockedTileValue)
			{
				
				m_setCellFunc(x_, y_);
				return true;					

			}// end if*/

			if (m_map[m_cellOffset] != m_blockedTileValue)
			{
				
				m_map[m_cellOffset] = TILE_MARKED;
				return true;					

			}// end if			
			
			return false;
			
		}// end public function trySetCell(x_:int, y_:int):Boolean		
		
		/*
		 * checks to make sure the path isn't blocked 
		 * and the pixel can be set
		 */
		public function trySetCellNoWrapping(x_:int, y_:int):Boolean
		{
			
			//m_calcCellOffsetFunc(x_, y_);
			//calcCellOffset(x_, y_);
			
			if ((x_ < 0) || (x_ >= m_width) || (y_ < 0) || (y_ >= m_height))
			{
				return false;
			}// end if	
			
			m_cellOffset = (y_ * m_width) + x_;			
			
			/*if (m_getCellFunc(x_, y_) != m_blockedTileValue)
			{
				
				m_setCellFunc(x_, y_);
				return true;					

			}// end if*/

			if (m_map[m_cellOffset] != m_blockedTileValue)
			{
				
				m_map[m_cellOffset] = TILE_MARKED;
				return true;					

			}// end if			
			
			return false;
			
		}// end public function trySetCell(x_:int, y_:int):Boolean		

		/*
		 * Starts projecting several lines radially to build a navigatable area on the grid 
		 */
		public function castPath(x_:int, y_:int, radius_:uint):void
		{
			
			if ((x_ < 0) || (x_ > m_width-1) || (y_ < 0) || (y_ > m_height-1))
			{
				return;
			}// end if
			
			m_radius = radius_+1;
			m_expandingRadius = 1;
			m_x = x_;
			m_y = y_;
			
			var xTo:int = x_ + radius_;
			var yTo:int = y_ + radius_;
			
			for (; xTo > x_-radius_; xTo--)
			{
				castLine(x_ +0.5, y_ +0.5, xTo + 0.5, yTo + 0.5);
			}// end for
			
			xTo = x_ - radius_;
			yTo = y_ + radius_;
			
			for (; yTo > y_-radius_; yTo--)
			{
				castLine(x_+0.5, y_+0.5, xTo+0.5, yTo+0.5);	
			}// end for			
			
			xTo = x_ - radius_;
			yTo = y_ - radius_;
			
			for (; xTo < x_+radius_; xTo++)
			{
				castLine(x_+0.5, y_+0.5, xTo+0.5, yTo+0.5);	
			}// end for			
			
			xTo = x_ + radius_;
			yTo = y_ - radius_;
			
			for (; yTo <= y_+radius_; yTo++)
			{
				castLine(x_+0.5, y_+0.5, xTo+0.5, yTo+0.5);	
			}// end for			
			
		}// end public function castPath(x_:int, y_:int, radius_:uint):void
		
		/*
		 * Continues drawing concentric circles until the whole path area is filled
		 * Returns true when done, false otherwise
		 */
		public function continueCastingPath():Boolean
		{
						
			m_expandingPathBitmap.fillRect(m_expandingPathBitmap.rect, TILE_BLOCKED); // BLOCKED_COLOR
			
			var chosenBitmap:BitmapData = m_expandingPathBitmapList[m_expandingRadius - 1];
			var chosenBitmapBounds:Rectangle = m_expandingPathBitmapBoundsList[m_expandingRadius - 1];
			
			var destPoint:Point = new Point(m_x + chosenBitmapBounds.x, m_y + chosenBitmapBounds.y);
					
			m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);	
			
			// draw wrapped portions of the bomb
			
			if ((m_x - m_radius) < 0)
			{
				destPoint.x = m_x + chosenBitmapBounds.x + m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);	
			}
			else if ((m_x + m_radius) >= m_expandingPathBitmap.width)
			{
				destPoint.x = m_x + chosenBitmapBounds.x - m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);					
			}// end else
			
			if ((m_y - m_radius) < 0)
			{
				destPoint.x = m_x + chosenBitmapBounds.x;
				destPoint.y = m_y + chosenBitmapBounds.y + m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);				
			}
			else if ((m_y + m_radius) >= m_expandingPathBitmap.height)
			{
				destPoint.x = m_x + chosenBitmapBounds.x;
				destPoint.y = m_y + chosenBitmapBounds.y - m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);				
			}// end else			
			
			if (((m_x - m_radius) < 0) && ((m_y - m_radius) < 0))
			{
				destPoint.x = m_x + chosenBitmapBounds.x + m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y + m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);				
			}
			else if (((m_x - m_radius) < 0) && ((m_y + m_radius) >= m_expandingPathBitmap.height))
			{
				destPoint.x = m_x + chosenBitmapBounds.x + m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y - m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);					
			}
			else if (((m_x + m_radius) >= m_expandingPathBitmap.width) && ((m_y - m_radius) < 0))
			{
				destPoint.x = m_x + chosenBitmapBounds.x - m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y + m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);					
			}
			else if (((m_x + m_radius) >= m_expandingPathBitmap.width) && ((m_y + m_radius) >= m_expandingPathBitmap.height))
			{
				destPoint.x = m_x + chosenBitmapBounds.x - m_expandingPathBitmap.width;
				destPoint.y = m_y + chosenBitmapBounds.y - m_expandingPathBitmap.height;
				m_expandingPathBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, destPoint);					
			}// end else if			
			
			// replace the circle color				
			
			var bitmapVec:Vector.<uint> = m_expandingPathBitmap.getVector(m_expandingPathBitmap.rect);
			
			for (var currTile:uint = 0; currTile < m_expandingPathMap.length; currTile++)
			{
				if ((bitmapVec[currTile] == CIRCLE_COLOR) && (m_map[currTile] == TILE_MARKED) && (m_expandingPathMap[currTile] != TILE_MARKED_OLD))
				{
					m_expandingPathMap[currTile] = TILE_MARKED;
				}// end if
			}// end for
			
			m_expandingRadius++;
			
			if (m_expandingRadius > m_radius)
			{
				return true;
			}// end if
			
			return false;
			
		}// end public function continueCastingPath():Boolean		
		
		
		/* *
		 * Projects a line from the first point, marking all the grid positions
		 * as available unless it hits a blocked tile 
		 * */	
		public function castLine(x1_:Number, y1_:Number, x2_:Number, y2_:Number):void
		{
						
			var dir:Vector3D = new Vector3D(x2_ - x1_, y2_ - y1_);
			dir.normalize();
			
			var tDeltaX:Number = 1 / Math.abs(dir.x);
			var tDeltaY:Number = 1 / Math.abs(dir.y);
			
			var x:int = Math.floor(x1_);
			var y:int = Math.floor(y1_);
			
			var endX1:int = Math.floor(x2_);
			var endY1:int = Math.floor(y2_);
			
			 // decide which direction to start walking in
			var stepX:int = signum(dir.x);
			var stepY:int = signum(dir.y);
			
			var tMaxX:Number;
			var tMaxY:Number;
			
			// calculate distance to first intersection in the voxel we start from
			if (dir.x < 0)
			{
				tMaxX = new Number((Math.floor(x) - x1_) / dir.x);
			}
			else
			{
				tMaxX = new Number((Math.floor(x + 1) - x1_) / dir.x);
			}// end else

			if (dir.y < 0)
			{
				tMaxY = new Number((Math.floor(y) - y1_) / dir.y);
			}
			else
			{
				tMaxY = new Number((Math.floor(y + 1) - y1_) / dir.y);
			}// end else			
			
			 // check if first is occupied
			if (getCell(x, y) == m_blockedTileValue)
			{
				return;	
			}// end if
			
			var reachedX:Boolean = false;
			var reachedY:Boolean = false;
			
			//m_setCellFunc(x, y);
			m_trySetCellFunc(x, y);
			
			while (true)
			{
				if (tMaxX < tMaxY)
				{
					tMaxX += tDeltaX;
					x += stepX;
				}
				else
				{
					tMaxY += tDeltaY;
					y += stepY;
				}// end else
				

				if (stepX > 0.0)
				{
					if (x >= endX1)
					{
						reachedX = true;
					}// end if
				}
				else if (x <= endX1)
				{
					reachedX = true;
				}// end else if

				if (stepY > 0.0)
				{
					if (y >= endY1)
					{
						reachedY = true;
					}// end if
				}
				else if (y <= endY1)
				{
					reachedY = true;
				}// end else if

				if (!m_trySetCellFunc(x, y))
				{
					return;
				}// end if

				if (reachedX && reachedY)
				{
					break;
				}// end if
			}// end while			
			
			
		}// end public function castLine(x1:int, y1:int, x2:int, y2:int):void		
		
		
	}// end public class RadialPathMap

}// end package