package com.nitrome.engine
{
	
	public class ShooterBulletImpactEffect extends BulletImpactEffect
	{

		public function ShooterBulletImpactEffect(posX_:Number, posY_:Number, powerLevel_:uint)
		{
		
			super(posX_, posY_, 2);
			
		}// end public function ShooterBulletImpactEffect(posX_:Number, posY_:Number, powerLevel_:uint)
		
	}// end public class ShooterBulletImpactEffect
	
}// end package