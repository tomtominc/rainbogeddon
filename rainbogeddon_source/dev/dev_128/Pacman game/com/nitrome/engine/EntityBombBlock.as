package com.nitrome.engine
{
	
	
	public class EntityBombBlock extends EntityBlock
	{
			

		public function EntityBombBlock(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_canDie = false;		
			Game.g.m_tileMap.onMapChanged();
			Game.g.m_drawTileMap.onMapChanged();
		}// end function EntityBombBlock(tx_:uint, ty_:uint)				
					
		public function terminate()
		{
			m_terminated = true;
			m_spatialHashMap.unregisterObject(this);
		}// end public function terminate()			
		
		public override function draw():void
		{
			Game.g.m_tinyCorridorBitmap.setPixel32(m_tx, m_ty, 0xff000000);		
		}// end public override function draw():void
	
	}// end public class EntityBombBlock
	
}// end package