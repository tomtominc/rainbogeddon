package com.nitrome.engine
{
	
	public class PowerUpDrill extends PowerUp
	{
		
		public function PowerUpDrill(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpDrill(tx_:uint, ty_:uint)
		
		public override function get soundID():String
		{		
			return "DrillSpeech";
		}// end public override function get soundID():String			
		
		public override function get displayName():String
		{		
			return "DRILL";
		}// end public override function get displayName():String			
		
		public override function get holder():Class
		{		
			return DrillHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpDrill
	
}// end package