package com.nitrome.engine
{
	
	
	public class EntityJellyBabyBoy extends EntityJellyBaby
	{
			
		public function EntityJellyBabyBoy(mother_:EntityJellyMama)
		{
			super(mother_);
			m_glow.m_color = 0xff89ff00;
		}// end public function EntityJellyBabyBoy(mother_:EntityJellyMama)
					
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedGreen;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityJellyBabyBoy
	
}// end package