package com.nitrome.ui
{

	import editor.TextBox;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class FinalLevelCompletePanel extends MovieClip
	{
				

		public function FinalLevelCompletePanel()
		{
			

			submitScoreDisable.visible = false;
			var player1Score:uint = Game.g.m_player1.m_score.value;
			var player2Score:uint = Game.g.m_player2.m_score.value;
			
			if (Game.m_isTwoPlayer)
			{
				playerResultPanel.gotoAndStop("2_players");
			}// end if
			
			var score:uint = player1Score + player2Score;
			var levelScore:uint = NitromeGame.getLevelScore(Game.selectedLevel.value);
			var totalScoreStartCount:uint = NitromeGame.getTotalScore();
			totalScoreStartCount -= levelScore;
			var totalScoreEndCount:uint = totalScoreStartCount + score;
			
			if(score > levelScore)
			{
				NitromeGame.setLevelScore(score, Game.selectedLevel.value);
			}
			else
			{
				submitScoreDisable.visible = true;
				submitScoreButton.visible = false;
			}// end else		
			
			levelScoreTicker.startCount(score, score / 20, 0);
			totalScoreTicker.startCount(totalScoreEndCount, (totalScoreEndCount - totalScoreStartCount) / 20, totalScoreStartCount);
				
		}// end public function FinalLevelCompletePanel()
	

	}// end public class FinalLevelCompletePanel
	
}// end package