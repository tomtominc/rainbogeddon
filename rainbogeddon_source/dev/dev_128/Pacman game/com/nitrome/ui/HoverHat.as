package com.nitrome.ui
{

	import flash.utils.getDefinitionByName;

	public class HoverHat extends MouseHoverElement
	{
				

		public function HoverHat()
		{
			
			m_menuLockPosList[0].x = 198; m_menuLockPosList[0].y = 338;
			m_menuLockPosList[1].x = 198; m_menuLockPosList[1].y = 373;
			m_menuLockPosList[2].x = 198; m_menuLockPosList[2].y = 404;
			m_menuLockPosList[3].x = 198; m_menuLockPosList[3].y = 435;	
			m_redOffset = 0x4c;
			m_greenOffset = 0xe5;
			m_blueOffset = 0xe8;
				
		}// end public function HoverHat()
	
		public override function trailClass():Class
		{
			return getDefinitionByName("HoverHatTrail") as Class;
		}// end public override function trailClass():Class
		
	}// end public class HoverHat extends MouseHoverElement
	
}// end package