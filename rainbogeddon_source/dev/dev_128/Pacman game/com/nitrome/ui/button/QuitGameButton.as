﻿package com.nitrome.ui.button {
	import com.nitrome.sound.SoundManager;
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	/**
	* Goto the level_select screen
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class QuitGameButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			if (NitromeGame.root.popUpHolder) NitromeGame.root.popUpHolder.hide();
			NitromeGame.root.transition.goto("chooseLevel");
			Game.endGame();
			SoundManager.playMusic("menuMusic");
		}
		
	}
	
}