package com.nitrome.ui
{

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class EndingPanel extends MovieClip
	{
				

		public function EndingPanel()
		{
			

			submitScoreDisable.visible = false;
			var player1Score:uint = Game.g.m_player1.m_score.value;
			var player2Score:uint = 0;
			
			if (Game.m_isTwoPlayer)
			{
				player2Score = Game.g.m_player2.m_score.value;
			}// end if
			
			var score:uint = player1Score + player2Score;
			var levelScore:uint = NitromeGame.getLevelScore(Game.selectedLevel.value);
			totalScore.text = new String(NitromeGame.getTotalScore());
			
			if(score > levelScore)
			{
				NitromeGame.setLevelScore(score, Game.selectedLevel.value);
			}
			else
			{
				submitScoreDisable.visible = true;
				submitScoreButton.visible = false;
			}// end else			
			
				
		}// end public function EndingPanel()
	

	}// end public class EndingPanel
	
}// end package