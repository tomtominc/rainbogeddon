package com.nitrome.util.misc {
	/**
	 * Returns an integer with a range of bits set to 1
	 *
	 * Of course, it will only work with values 0 to 32 as params (the number of bits in a Flash int)
	 *
	 * adapted from: http://efreedom.com/Question/1-2246228/Set-Range-Bits-Ushort
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public function getBitRange(from:int, to:int):int{
		return ((2 << to) - 1) - ((1 << from) - 1);
	}

}