﻿package com.nitrome.util.clips {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	/* Pause hack - freezes gfx and all of its children */
	public function startClips(gfx:DisplayObjectContainer):void{
		for(var i:int = 0; i < gfx.numChildren; i++){
			if (gfx.getChildAt(i) is MovieClip){
				(gfx.getChildAt(i) as MovieClip).gotoAndPlay((gfx.getChildAt(i) as MovieClip).currentFrame);
			}
			if(gfx.getChildAt(i) is DisplayObjectContainer){
				startClips(gfx.getChildAt(i) as DisplayObjectContainer);
			}
		}
	}
}