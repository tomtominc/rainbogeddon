package com.nitrome.ui {
	import flash.events.Event;
	/**
	 * ...
	 * @author Aaron Steed, nitrome.com
	 */
	public class RainbogeddonTextBox extends TextBox{
		
		public function RainbogeddonTextBox() {
			id = "RainbogeddonFont";
			mixedCase = false;
			super();
			whitespaceLength = 8;
		}
		
		// tracking
		[Inspectable(defaultValue=2)]
		override public function get tracking():int{
			return _tracking;
		}
		override public function set tracking(n:int):void{
			_tracking = n;
			if(_wordWrap) updateText();
			draw();
		}
		
		// lineSpacing
		[Inspectable(defaultValue=23)]
		override public function get lineSpacing():int{
			return _lineSpacing;
		}
		override public function set lineSpacing(n:int):void{
			_lineSpacing = n;
			draw();
		}
	}

}