﻿package editor {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class ThemeSelectionButton extends MovieClip {
		
		public var hover:MovieClip;
		
		public function ThemeSelectionButton() {
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
			setTheme(0);
			
			useHandCursor = buttonMode = true;
			hover.visible = false;
		}
		
		public function mouseDown(e:MouseEvent):void {
			setTheme((getTheme() + 1) % totalFrames);
		}
		public function mouseOver(e:MouseEvent):void {
			hover.visible = true;
		}
		public function mouseOut(e:MouseEvent):void {
			hover.visible = false;
		}
		
		public function setTheme(n:Number):void {
			gotoAndStop(n+1);
		}
		public function getTheme():Number {
			return currentFrame - 1;
		}
		
	}

}