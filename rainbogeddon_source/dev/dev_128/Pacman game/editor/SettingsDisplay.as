﻿package editor {
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.NumericStepper;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class SettingsDisplay extends Sprite {
		
		public static var instance:SettingsDisplay;
		
		public var msg:TextField;
		public var background:ComboBox;
		public var btnClose:Button;
		
		private var fieldsUpdated:Boolean;
		
		public function SettingsDisplay() {
			instance = this;
			Controller.root.addChild(this);
			addEventListener(Event.ENTER_FRAME, enterFrame);
			
			btnClose.addEventListener(MouseEvent.CLICK, onClose);
			
			enterFrame(null);
		}
		
		private function updateFields():void{
			
			background.selectedIndex = Controller.levelSettings.background;
			
			msg.text = Controller.levelSettings.msg;
		}
		
		public function destroy():void {
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			btnClose.removeEventListener(MouseEvent.CLICK, onClose);
			
			if(parent) parent.removeChild(this);
			if(instance == this) instance = null;
		}
		
		public function enterFrame(e:Event):void {
			// flash's components are waiting till the last moment to update, so I wait for them
			// to get their shit together and then insert values into them
			if(!fieldsUpdated){
				/*if(thumper.minimum != 0){
					updateFields();
					fieldsUpdated = true;
				}*/
			}
			
			
			x = Math.floor(Controller.stage.stageWidth / 2);
			y = Math.floor(Controller.stage.stageHeight / 2);
		}
		
		public function onClose(e:MouseEvent):void {
			
			Controller.levelSettings.background = background.selectedIndex;
			
			Controller.levelSettings.msg = msg.text;
			
			destroy();
		}
		
	}
	
}