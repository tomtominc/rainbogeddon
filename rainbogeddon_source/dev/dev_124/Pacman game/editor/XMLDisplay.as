﻿package editor {
	import fl.controls.Button;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class XMLDisplay extends Sprite {
		
		public static var instance:XMLDisplay;
		
		public var txtXML:TextField;
		public var btnClipboard:Button;
		public var btnClose:Button;
		
		public function XMLDisplay() {
			instance = this;
			Controller.root.addChild(this);
			addEventListener(Event.ENTER_FRAME, enterFrame);
			btnClipboard.addEventListener(MouseEvent.CLICK, onClipboard);
			btnClose.addEventListener(MouseEvent.CLICK, onClose);
			
			//btnClipboard.tabEnabled = false;
			//btnClose.tabEnabled = false;
			
			enterFrame(null);
		}
		
		public function destroy():void {
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			btnClipboard.removeEventListener(MouseEvent.CLICK, onClipboard);
			btnClose.removeEventListener(MouseEvent.CLICK, onClose);
			
			if(parent) parent.removeChild(this);
			if(instance == this) instance = null;
		}
		
		public function enterFrame(e:Event):void {
			x = Math.floor(Controller.stage.stageWidth / 2);
			y = Math.floor(Controller.stage.stageHeight / 2);
		}
		
		public function onClipboard(e:MouseEvent):void {
			System.setClipboard(txtXML.text);
		}
		public function onClose(e:MouseEvent):void {
			destroy();
		}
		
	}
	
}