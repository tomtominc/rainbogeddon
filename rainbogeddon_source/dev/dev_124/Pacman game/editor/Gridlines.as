﻿package editor {
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Gridlines extends Sprite {
		
		public function Gridlines() {
			redraw();
		}
		
		public function redraw():void {
			graphics.clear();
			graphics.lineStyle(1, 0xC0C0FF);
			
			const shw:Number = (Controller.stage.stageWidth / 2) + Controller.W;
			const shh:Number = (Controller.stage.stageHeight / 2) + Controller.H;
			
			var w:Number = Controller.W * Controller.zoomLevel;
			var h:Number = Controller.H * Controller.zoomLevel;
			
			var leftX:Number = -Math.floor(shw / w) * w;
			var rightX:Number = Math.ceil(shw / w) * w;
			for(var x:Number = leftX; x <= rightX; x += w) {
				graphics.moveTo(x, -shh);
				graphics.lineTo(x, shh);
			}
			
			var topY:Number = -Math.floor(shh / h) * h;
			var bottomY:Number = Math.ceil(shh / h) * h;
			for(var y:Number = topY; y <= bottomY; y += h) {
				graphics.moveTo(-shw, y);
				graphics.lineTo(shw, y);
			}
			
			slide();
		}
		
		public function wrapToRange(value:Number, low:Number, high:Number):Number {
			value = ((value - low) % (high - low)) + low;
			while(value <= low) value += high - low;
			while(value >= high) value -= high - low;
			return value;
		}
		
		public function slide():void {
			var offsetX:Number = Controller.stage.stageWidth / 2;
			offsetX -= wrapToRange(Controller.scrollX, 0, Controller.W) * Controller.zoomLevel;
			
			var offsetY:Number = (Controller.stage.stageHeight - Controller.panel.background.height) / 2;
			offsetY -= wrapToRange(Controller.scrollY, 0, Controller.H) * Controller.zoomLevel;
			
			x = offsetX;
			y = offsetY;
		}
		
	}
	
}