package editor {
	import editor.Global;
	import editor.Key;
	import fl.controls.Button;
	import flash.display.BlendMode;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.System;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLNode;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Controller {
		
		public static var root:MovieClip;
		public static var stage:Stage;
		public static var panel:Panel;
		
		public static var container:Sprite;
		
		public static var dragging:Boolean = false;
		
		public static const W:Number = 25;
		public static const H:Number = 25;
		
		public static var layers:Object = {};
		public static var layerList:Array /*Layer*/ = [];
		public static var drawingLayer:Layer = null;
		public static var drawingTile:Class;
		public static var gridlines:Gridlines;
		public static var hoverSprite:Sprite = null;
		
		public static var selectionBuffer:Object;
		public static var selectionRect:Rectangle;
		public static var selectionSprite:Sprite;
		
		public static var fromMouseX:Number;
		public static var fromMouseY:Number;
		public static var scrollX:Number = 0;
		public static var scrollY:Number = 0;
		
		public static var zoomLevel:Number = 1;
		public static var zoomLevelTarget:Number = 1;
		
		public static var spaceHeldDown:Boolean = false;
		public static var lastMode:Number = 0;
		public static var targetMode:Number = 0;
		public static var erasingLayer:Layer = null;
		
		public static var limitingHorizontal:Boolean = false;
		public static var limitingVertical:Boolean = false;
		public static var limitKeyDown:Boolean = false;
		public static var limitHorizontalTile:Number;
		public static var limitVerticalTile:Number;
		
		public static var startDrawingTileX:Number;
		public static var startDrawingTileY:Number;
		
		public static var testLoader:Loader;
		public static var btnEndTest:Button;
		
		public static var borderSprite:Sprite;
		
		public static var pathLayer:Sprite;
		public static var pathEditingTile:Tile = null;
		public static var pathEditingPoint:Number = -1;
		public static var pathEditingConnection:Boolean = false;
		
		public static var paramTile:Tile = null;
		
		public static const GAME_NAME:String = "Rainbowgeddon";
		public static const PATH_LAYER:String = "girder";
		public static const DOUBLE_TILE_LAYER:String = "bt";
		
		public static const PATH_THICKNESS_DEFAULT:Number = 3;
		public static const PATH_THICKNESS_ACTIVE:Number = 4;
		public static const PATH_COL:uint = 0xFF00FF;
		
		// rubble trouble settings
		public static var levelSettings:Object = {
			"background":0,
			"msg":""
		}
		
		
		public static function setup(panelRef:Panel):void {
			panel = panelRef;
			root = panel.root as MovieClip;
			stage = panel.stage;
			stage.tabChildren = false;
			
			stage.showDefaultContextMenu = false;
			
			container = new Sprite;
			root.addChildAt(container, 0);
			gridlines = new Gridlines;
			root.addChildAt(gridlines, 0);
			
			Alert.setup(root);
			
			try {
				if(!ExternalInterface.call("communicationOK")) {
					Alert.alert("Browser communication failed -- SAVING WON'T WORK!");
				}
			} catch(e:Error) {
				Alert.alert("Browser communication failed -- SAVING WON'T WORK!");
			}
			
			if(ExternalInterface.available) ExternalInterface.addCallback("getGameName", function():String { return GAME_NAME; });
			
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, onStageResize);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			//stage.showDefaultContextMenu=false;
			
			createLayer("tile", "Tile");
			createLayer("entity", "Entity");
			createLayer("moreEntities", "Entity");
			createLayer("powerup", "PowerUp");
			createLayer("levelControl", "LevelControl");
			
			borderSprite = new Sprite;
			borderSprite.visible = false;
			container.addChild(borderSprite);
			
			selectionSprite = new Sprite;
			selectionSprite.visible = false;
			container.addChild(selectionSprite);
			
			pathLayer = new Sprite;
			container.addChild(pathLayer);
			
			updateLayerPositions();
			
			panel.setup();
			
			onStageResize(null);
		}
		public static function createLayer(name:String, prefix:String = ""):void {
			layerList.push(layers[name] = new Layer(name, prefix)); // && fries
		}
		
		public static function onStageResize(e:Event):void {
			gridlines.redraw();
			panel.resize();
			updateLayerPositions();
		}
		
		public static function isSignTile(c:Class):Boolean {
			
			//return false;
			
			var className:String = getQualifiedClassName(c);
			if (className.indexOf("_param_") != -1) { //  && className.indexOf("signal") == -1) {
				return true;
			} else {
				return false;
			}
		}
		
		public static function onMouseDown(e:MouseEvent):void {
			if(panel.hitTestPoint(root.mouseX, root.mouseY)) return;
			if(XMLDisplay.instance && XMLDisplay.instance.hitTestPoint(root.mouseX, root.mouseY)) return;
			if(SettingsDisplay.instance && SettingsDisplay.instance.hitTestPoint(root.mouseX, root.mouseY)) return;
			if(testLoader) return;
			
			if(panel.mode.index == Panel.MODE_PATH) {
				var tx:Number = Math.floor(container.mouseX / W);
				var ty:Number = Math.floor(container.mouseY / H);
				if(!pathEditingTile) {
					//for(var n:Number = layerList.length - 1; n >= 0; n --) {
						//var t:Tile = layerList[n].getTile(tx, ty);
						var t:Tile = layers[PATH_LAYER].getTile(tx, ty);
						if(t) {
							pathEditingTile = t;
							pathEditingPoint = -1;
							showPaths(true);
							return;
						}
					//}
					return;
				}
				
				if(tx == pathEditingTile.connectedToX && ty == pathEditingTile.connectedToY
				&& ((container.mouseX / W) - tx > 0.6) && ((container.mouseY / H) - ty < 0.4)) {
					pathEditingConnection = true;
				} else {
					// drag an existing point
					var closestPoint:Number = 0;
					var closestPointSquareDistance:Number = Infinity;
					
					for(var n:Number = 1; n < pathEditingTile.pathPoints.length; n ++) {
						if(pathEditingTile.pathPoints[n].x == tx
						&& pathEditingTile.pathPoints[n].y == ty) {
							pathEditingPoint = n;
						}
						
						var dx:Number = pathEditingTile.pathPoints[n].x - tx;
						var dy:Number = pathEditingTile.pathPoints[n].y - ty;
						var squareDistance:Number = (dx*dx) + (dy * dy);
						
						if(squareDistance < closestPointSquareDistance) {
							closestPointSquareDistance = squareDistance;
							closestPoint = n;
						}
					}
					// create a point
					if(pathEditingPoint == -1) {
						var before:PathPoint = pathEditingTile.pathPoints[closestPoint - 1];
						var after:PathPoint = pathEditingTile.pathPoints[closestPoint + 1];
						var closest:PathPoint = pathEditingTile.pathPoints[closestPoint];
						if(!before) before = closest;
						if(!after) after = closest;
						
						var toX:Number = after.x - before.x;
						var toY:Number = after.y - before.y;
						var closestDot:Number = (toX * closest.x) + (toY * closest.y);
						var mouseDot:Number = (toX * (container.mouseX / W)) + (toY * (container.mouseY / H));
						
						var newPathPoint:PathPoint = new PathPoint(tx, ty);
						if((mouseDot > closestDot) || (pathEditingTile.pathPoints.length < 2)) {
							pathEditingTile.pathPoints.splice(closestPoint + 1, 0, newPathPoint);
							pathEditingPoint = closestPoint + 1;
						} else {
							pathEditingTile.pathPoints.splice(closestPoint, 0, newPathPoint);
							pathEditingPoint = closestPoint;
						}
					}
				}
				showPaths();
			}
			
			dragging = true;
			fromMouseX = container.mouseX;
			fromMouseY = container.mouseY;
			
			if(panel.mode.index == Panel.MODE_ERASE)
				erasingLayer = null;
			if(panel.mode.index == Panel.MODE_SCROLL) {
				redrawBorder();
				borderSprite.visible = true;
				Mouse.hide();
			} else {
				if(!panel.autosaveTimer.running)
					panel.autosaveTimer.start();
			}
			if(panel.mode.index == Panel.MODE_DRAW) {
				startDrawingTileX = Math.floor(container.mouseX / Controller.W);
				startDrawingTileY = Math.floor(container.mouseY / Controller.H);
			}
			
			if(panel.mode.index == Panel.MODE_SELECT){
				startDrawingTileX = Math.floor(container.mouseX / Controller.W);
				startDrawingTileY = Math.floor(container.mouseY / Controller.H);
				selectionRect = new Rectangle(startDrawingTileX, startDrawingTileY, 1, 1);
				redrawSelection();
				selectionSprite.visible = true;
			}
			
			onMouseMove(e);
		}
		
		public static function onMouseMove(e:MouseEvent):void {
			if(testLoader) return;
			
			if(hoverSprite) {
				Controller.container.removeChild(hoverSprite);
				hoverSprite = null;
			}
			if((!dragging) && panel.mode.index == Panel.MODE_DRAW && (!panel.hitTestPoint(root.mouseX, root.mouseY))) {
				try {
					var classRef:Class = drawingTile; //getDefinitionByName(drawingTile) as Class;
					hoverSprite = new classRef;
					hoverSprite.x = Math.floor(container.mouseX / Controller.W) * Controller.W;
					hoverSprite.y = Math.floor(container.mouseY / Controller.H) * Controller.H;
					hoverSprite.alpha = 0.3;
					hoverSprite.blendMode = BlendMode.LAYER;
					if (isSignTile(drawingTile) && (hoverSprite as MovieClip).textField) {
						(hoverSprite as MovieClip).textField
							.text = panel.signPopup.signText.text.split("|").join("\n");
					}
					Controller.container.addChild(hoverSprite);
				} catch(e:Error) { }
			}
			
			if(dragging) {
				if(panel.mode.index == Panel.MODE_SCROLL) {
					var sx:Number = container.mouseX - fromMouseX;
					var sy:Number = container.mouseY - fromMouseY;
					
					scrollX = Math.floor(scrollX - sx);
					scrollY = Math.floor(scrollY - sy);
					updateLayerPositions();
				} else {
					var tx:Number = Math.floor(container.mouseX / Controller.W);
					var ty:Number = Math.floor(container.mouseY / Controller.H);
					var lastTX:Number = Math.floor(fromMouseX / Controller.W);
					var lastTY:Number = Math.floor(fromMouseY / Controller.H);
					
					if((panel.mode.index == Panel.MODE_DRAW || panel.mode.index == Panel.MODE_ERASE) && limitKeyDown) {
						if((!limitingHorizontal) && (!limitingVertical)) {
							if(lastTY == ty && lastTX == tx) {
								// don't limit yet
							} else if(Math.abs(lastTY - ty) < Math.abs(lastTX - tx)) {
								limitingVertical = true;
								limitVerticalTile = ty;
							} else {
								limitingHorizontal = true;
								limitHorizontalTile = tx;
							}
						} else {
							if(limitingHorizontal)
								tx = lastTX = limitHorizontalTile;
							else
								ty = lastTY = limitVerticalTile;
						}
					}
					
					if(panel.mode.index == Panel.MODE_GRAB) {
						targetMode = Panel.MODE_ERASE;
						for(var n:Number = layerList.length - 1; n >= 0; n --) {
							if(layerList[n].getTile(tx, ty)) {
								var tileType:Class = layerList[n].getTile(tx, ty).getType();
								panel.palette.setSelected(tileType);
								panel.palette.enforceCorrectLayer();
								if (isSignTile(tileType)) {
									paramTile = layerList[n].getTile(tx, ty);
									if (layerList[n].getTile(tx, ty).signText)
									{
										panel.signPopup.signText.text = layerList[n].getTile(tx, ty).signText;
									}// end if
								}
								targetMode = Panel.MODE_DRAW;
								break;
							}
						}
						panel.mode.index = Panel.MODE_GRAB;
					} else if(panel.mode.index == Panel.MODE_DRAW) {
						/* TODO: work this out
						drawingLayer = panel.palette.getByClassName(drawingTile).getTargetLayer();
						drawLine(lastTX, lastTY, tx, ty, drawingLayer, drawingTile);
						//drawingLayer.drawTile(tx, ty, drawingTile);
						*/
						if (!drawingLayer)
						{
							return;
						}// end if
						if(drawingLayer.layerName == DOUBLE_TILE_LAYER) {
							tx = (Math.floor((tx - startDrawingTileX) >> 1) << 1) + startDrawingTileX;
							ty = (Math.floor((ty - startDrawingTileY) >> 1) << 1) + startDrawingTileY;
							lastTX = tx;
							lastTY = ty;
						}
						drawLine(lastTX, lastTY, tx, ty, drawingLayer, drawingTile);
					} else if(panel.mode.index == Panel.MODE_ERASE) {
						if(!erasingLayer) {
							erasingLayer = topLayerAtCell(tx, ty);
							if(erasingLayer) {
								for(var li:Number = 0; li < layerList.length; li ++)
									if(layerList[li] != erasingLayer)
										layerList[li].alpha = 0.5;
							}
						}
						if(erasingLayer) {
							drawLine(lastTX, lastTY, tx, ty, erasingLayer, null);
						}
						//drawingLayer.eraseTile(tx, ty);
						showPaths();
					} else if(panel.mode.index == Panel.MODE_PATH) {
						if(pathEditingTile && (pathEditingPoint != -1)) {
							pathEditingTile.pathPoints[pathEditingPoint].x = tx;
							pathEditingTile.pathPoints[pathEditingPoint].y = ty;
							showPaths();
						} else if(pathEditingTile && pathEditingConnection) {
							pathEditingTile.connectedToX = tx;
							pathEditingTile.connectedToY = ty;
							showPaths();
						}
					} else if(panel.mode.index == Panel.MODE_SELECT){
						var width:int = 1 + Math.abs(tx - startDrawingTileX);
						var height:int = 1 + Math.abs(ty - startDrawingTileY);
						selectionRect = new Rectangle(Math.min(tx, startDrawingTileX), Math.min(ty, startDrawingTileY), width, height);
						redrawSelection();
					}
				}
				fromMouseX = container.mouseX;
				fromMouseY = container.mouseY;
			}
			if(e) e.updateAfterEvent();
		}
		public static function drawLine(
			fromTileX:Number, fromTileY:Number, toTileX:Number, toTileY:Number, layer:Layer, tile:Class):void {
			var tx:Number, ty:Number;
			var temp:Number;
			var s:Number;
			
			if(Math.abs(fromTileX - toTileX) >= Math.abs(fromTileY - toTileY)) {
				if(fromTileX > toTileX) {
					temp = fromTileX; fromTileX = toTileX; toTileX = temp;
					temp = fromTileY; fromTileY = toTileY; toTileY = temp;
				}
				for(tx = fromTileX; tx <= toTileX; tx ++) {
					s = (tx - fromTileX) / (toTileX - fromTileX);
					if(toTileX == fromTileX) s = 0;
					ty = fromTileY + Math.round((toTileY - fromTileY) * s);
					layer.drawTile(tx, ty, tile);
				}
			} else {
				if(fromTileY > toTileY) {
					temp = fromTileX; fromTileX = toTileX; toTileX = temp;
					temp = fromTileY; fromTileY = toTileY; toTileY = temp;
				}
				for(ty = fromTileY; ty <= toTileY; ty ++) {
					s = (ty - fromTileY) / (toTileY - fromTileY);
					if(toTileY == fromTileY) s = 0;
					tx = fromTileX + Math.round((toTileX - fromTileX) * s);
					layer.drawTile(tx, ty, tile);
				}
			}
		}
		
		public static function onMouseUp(e:MouseEvent):void {
			if(!dragging) return;
			dragging = false;
			if(panel.mode.index == Panel.MODE_GRAB) panel.mode.index = targetMode;
			borderSprite.visible = false;
			limitingHorizontal = false;
			limitingVertical = false;
			Mouse.show();
			
			for(var n:Number = 0; n < layerList.length; n ++) {
				if(layerList[n].alpha != 1)
					layerList[n].alpha = 1;
			}
			
			if(panel.mode.index == 4) {
				if(pathEditingTile && (pathEditingPoint != -1)) {
					var editing:PathPoint = pathEditingTile.pathPoints[pathEditingPoint];
					var before:PathPoint = pathEditingTile.pathPoints[pathEditingPoint - 1];
					var after:PathPoint = pathEditingTile.pathPoints[pathEditingPoint + 1];
					if(editing && before && editing.x == before.x && editing.y == before.y) {
						if(editing && after && editing.x == after.x && editing.y == after.y) {
							pathEditingTile.pathPoints.splice(pathEditingPoint, 2);
						} else {
							pathEditingTile.pathPoints.splice(pathEditingPoint, 1);
						}
					} else if(editing && after && editing.x == after.x && editing.y == after.y) {
						pathEditingTile.pathPoints.splice(pathEditingPoint, 1);
					}
				}
				pathEditingPoint = -1;
				pathEditingConnection = false;
				showPaths();
			}
		}
		
		public static function onEnterFrame(e:Event):void {
			if(zoomLevel != zoomLevelTarget) {
				zoomLevel = ((zoomLevel - zoomLevelTarget) * 0.7) + zoomLevelTarget;
				if(Math.abs(zoomLevel - zoomLevelTarget) < 0.001) {
					zoomLevel = zoomLevelTarget;
				}
				gridlines.redraw();
				updateLayerPositions();
			}
			//trace(System.totalMemory);
			panel.advance();
		}
		
		public static function onKeyDown(e:KeyboardEvent):void {
			if(TextBox.typing) return;
			if(SettingsDisplay.instance && stage.focus == SettingsDisplay.instance.msg) return;
			if(testLoader) {
				if(e.keyCode == Keyboard.ESCAPE)
					endTest(null);
				return;
			}
			
			if(e.keyCode == Keyboard.SPACE && !spaceHeldDown) {
				spaceHeldDown = true;
				lastMode = panel.mode.index;
				panel.mode.index = Panel.MODE_SCROLL;
				paramTile = null;
				if(dragging) Mouse.hide();
			}
			
			if(e.keyCode == "S".charCodeAt(0)) panel.mode.index = Panel.MODE_SCROLL;
			if(e.keyCode == "D".charCodeAt(0)) panel.mode.index = Panel.MODE_DRAW;
			if(e.keyCode == "E".charCodeAt(0)) panel.mode.index = Panel.MODE_ERASE;
			if(e.keyCode == "G".charCodeAt(0)) panel.mode.index = Panel.MODE_GRAB;
			if(e.keyCode == "P".charCodeAt(0)) panel.mode.index = Panel.MODE_PATH;
			if(e.keyCode == "B".charCodeAt(0)) panel.mode.index = Panel.MODE_SELECT;
			if(e.keyCode == "X".charCodeAt(0)) cutSelection();
			if(e.keyCode == "C".charCodeAt(0)) copySelection();
			if(e.keyCode == "V".charCodeAt(0)) pasteBuffer();
			if(dragging) {
				if(panel.mode.index != Panel.MODE_SCROLL) Mouse.show(); else Mouse.hide();
			}
			
			if(e.keyCode == Key.NUMPAD_ADD || e.keyCode == Key.EQUAL)
				panel.zoom.select(panel.zoom.index + 1);
			if(e.keyCode == Key.NUMPAD_SUBTRACT || e.keyCode == Key.MINUS)
				panel.zoom.select(panel.zoom.index - 1);
			
			if(e.keyCode == Key.T) { startTest(); }
			
			if(e.keyCode == Keyboard.SHIFT) limitKeyDown = true;
		}
		
		public static function onKeyUp(e:KeyboardEvent):void {
			if(e.keyCode == Keyboard.SPACE && spaceHeldDown) {
				spaceHeldDown = false;
				panel.mode.index = lastMode;
				if (panel.mode.index != Panel.MODE_GRAB)
				{
					paramTile = null;
				}// end if
				Mouse.show();
			}
			if(e.keyCode == Keyboard.SHIFT) {
				limitKeyDown = false;
				limitingHorizontal = false;
				limitingVertical = false;
			}
		}
		
		public static function topLayerAtCell(x:Number, y:Number):Layer {
			for(var n:Number = layerList.length - 1; n >= 0; n --) {
				if(layerList[n].getTile(x, y)) return layerList[n];
			}
			return null;
		}
		
		public static function updateLayerPositions():void {
			container.x = Math.floor((stage.stageWidth / 2) - (scrollX * zoomLevel));
			container.y = Math.floor(((stage.stageHeight - panel.background.height) / 2) - (scrollY * zoomLevel));
			container.scaleX = zoomLevel;
			container.scaleY = zoomLevel;
			gridlines.slide();
			
			for(var a:String in layers) {
				for(var n:Number = 0; n < layers[a].tileList.length; n ++) {
					if(layers[a].tileList[n].infinitelyWideSprite) {
						layers[a].tileList[n].infinitelyWideSprite.x = (-Math.floor(container.x / W) * W) - W;
					}
				}
			}
		}
		
		public static function setZoomLevel(zl:Number):void {
			zoomLevelTarget = zl;
		}
		
		public static function clearEverything():void {
			for(var a in layers) {
				layers[a].clear();
			}
		}
		
		public static function showPaths(show:Boolean = true):void {
			pathLayer.graphics.clear();
			pathLayer.graphics.lineStyle(PATH_THICKNESS_DEFAULT, PATH_COL);
			for(var layerName in layers) {
				var layer:Layer = layers[layerName];
				for(var ti:Number = 0; ti < layer.tileList.length; ti ++) {
					var t:Tile = layer.tileList[ti];
					var pl:/*PathPoint*/Array = (t.pathPoints && t.pathPoints.length > 1)
						? t.pathPoints : [new PathPoint(t.gridX, t.gridY)];
					
					if(show && t == pathEditingTile) {
						pathLayer.graphics.lineStyle(PATH_THICKNESS_ACTIVE, PATH_COL);
					} else {
						pathLayer.graphics.lineStyle(PATH_THICKNESS_DEFAULT, PATH_COL, 0.5);
					}
					
					pathLayer.graphics.moveTo((pl[0].x + 0.5) * W, (pl[0].y + 0.5) * H);
					for(var n:Number = 1; n < pl.length; n ++) {
						pathLayer.graphics.lineTo((pl[n].x + 0.5) * W, (pl[n].y + 0.5) * H);
					}
					
					if(show && t == pathEditingTile) {
						for(n = 0; n < pl.length; n ++) {
							pathLayer.graphics.drawCircle((pl[n].x + 0.5) * W, (pl[n].y + 0.5) * H, 2);
							if(pathEditingPoint == n)
								pathLayer.graphics.drawRect((pl[n].x + 0.35) * W, (pl[n].y + 0.35) * H, 0.3 * W, 0.3 * H);
						}
					}
					
					if(show && t == pathEditingTile) {
						pathLayer.graphics.lineStyle(PATH_THICKNESS_ACTIVE, PATH_COL);
						pathLayer.graphics.drawCircle((t.gridX + 0.8) * W, (t.gridY + 0.2) * H, 2);
						pathLayer.graphics.drawCircle((t.connectedToX + 0.8) * W, (t.connectedToY + 0.2) * H, 2);
						if(pathEditingConnection)
							pathLayer.graphics.drawRect(
								(t.connectedToX + 0.65) * W, (t.connectedToY + 0.05) * H, 0.3 * W, 0.3 * H);
					} else {
						pathLayer.graphics.lineStyle(PATH_THICKNESS_DEFAULT, PATH_COL, 0.5);
					}
					pathLayer.graphics.moveTo((t.gridX + 0.8) * W, (t.gridY + 0.2) * H);
					pathLayer.graphics.lineTo((t.connectedToX + 0.8) * W, (t.connectedToY + 0.2) * H);
				}
			}
		}
		
		public static function fromXML(xml:XML):void {
			var node:XML, tx:Number, ty:Number, tile:Tile, n:Number;
			
			clearEverything();
			
			var w:Number = Number(xml.@width);
			var h:Number = Number(xml.@height);
			
			levelSettings.background = int(xml.@background);
			levelSettings.msg = String(xml["msg"]);
			
			for(var a in layers)
				layers[a].unserializeRange(xml[a], 0, 0, w, h);
			
			for(n = 0; n < xml.paramText.length(); n ++) {
				node = xml.paramText[n];
				tx = Number(node.@x);
				ty = Number(node.@y);
				tile = layers[node.@layer].tileGrid[tx][ty];
				if(tile) tile.setSignText(node);
			}
			for(n = 0; n < xml.path.length(); n ++) {
				node = xml.path[n];
				tx = Number(node.@x);
				ty = Number(node.@y);
				tile = layers[PATH_LAYER].tileGrid[tx][ty];
				if(tile) tile.setPathXML(node);
			}
			for(n = 0; n < xml.conn.length(); n ++) {
				node = xml.conn[n];
				tx = Number(node.@sx);
				ty = Number(node.@sy);
				tile = layers[PATH_LAYER].tileGrid[tx][ty];
				if(tile) tile.setConnectionXML(node);
			}
			
			scrollX = w * W / 2;
			scrollY = h * H / 2;
			updateLayerPositions();
			
			pathEditingTile = null;
			pathEditingPoint = -1;
			pathEditingConnection = false;
			paramTile = null;
			showPaths();
		}
		
		public static function toXML():XML {
			var resultingXML:XML = <level />;
			
			var n:Number;
			
			var left:Number = Global.safeMinimum(Global.childrenList(layerList, "leftmostX")) || 0;
			var right:Number = Global.safeMaximum(Global.childrenList(layerList, "rightmostX")) || 0;
			var top:Number = Global.safeMinimum(Global.childrenList(layerList, "topmostY")) || 0;
			var bottom:Number = Global.safeMaximum(Global.childrenList(layerList, "bottommostY")) || 0;
			
			// fetch the starting camera position by locating the start position tile
			var startX:int = 0;
			var startY:int = 0;
			if(layers.levelControl.hasAny()){
				var str:String;
				var tileList:Array = layers.levelControl.tileList;
				for(n = 0; n < tileList.length; n ++) {
					str = tileList[n].getTypeName();
					if(str && str == "StartMC") {
						startX = tileList[n].gridX - left;
						startY = tileList[n].gridY - top;
						break;
					}
				}
			}
			
			resultingXML.@width = 1 + right - left;
			resultingXML.@height = 1 + bottom - top;
			resultingXML.@startX = startX;
			resultingXML.@startY = startY;
			
			resultingXML.@background = levelSettings.background;
			
			for(var l:Number = 0; l < layerList.length; l ++) {
				var a:String = layerList[l].layerName;
				if(!layers[a].hasAny()) continue;
				resultingXML[a] = layers[a].serializeRange(left, right, top, bottom);
				
				for(n = 0; n < layers[a].tileList.length; n ++) {
					var pathNode:XML = layers[a].tileList[n].getPathXML(-left, -top);
					if(pathNode) resultingXML.appendChild(pathNode);
					var connNode:XML = layers[a].tileList[n].getConnectionXML(-left, -top);
					if(connNode) resultingXML.appendChild(connNode);
					
					var tile:Tile = layers[a].tileList[n];
					if (tile.signText && isSignTile(tile.type)) {
						var s:XML = <paramText>{tile.signText}</paramText>;
						s.@x = tile.gridX - left;
						s.@y = tile.gridY - top;
						s.@layer = a;
						resultingXML.appendChild(s);
					}
				}
			}
			
			var msgStr:String = levelSettings.msg;
			var msgXML:XML =<msg>{msgStr}</msg>;
			
			resultingXML.appendChild(msgXML);
			
			return resultingXML;
		}
		
		public static function startTest():void {
			container.visible = false;
			panel.visible = false;
			gridlines.visible = false;
			
			testLoader = new Loader();
			testLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
				root.addChildAt(testLoader, 0);
				//getIngamePlayerNum().value = panel.twoPlayer.selected;
				getIngameController().xml = toXML();
				//getIngameController().loadedLevelNumber = 1; //.value = 1;
				//getIngameController().selectedLevel = 1;
				//(testLoader.content as MovieClip).gotoAndStop("game");
			});
			testLoader.load(new URLRequest("game.swf"), new LoaderContext(false, new ApplicationDomain()));
			
			btnEndTest = new Button();
			btnEndTest.x = 0;
			btnEndTest.y = 0;
			btnEndTest.width = 100;
			btnEndTest.height = 30;
			btnEndTest.label = "End test (ESC)";
			root.addChild(btnEndTest);
			btnEndTest.addEventListener(MouseEvent.CLICK, endTest);
			
			if(ExternalInterface.available) {
				ExternalInterface.call("resizeElementToHeight", 550);
			}
		}
		
		public static function getIngameController():Class {
			return testLoader.contentLoaderInfo.applicationDomain
				.getDefinition("Game") as Class;
		}
		
		public static function endTest(e:Event):void {
			if(testLoader == null) return;
			
			try {
				getIngameController().endGame();
				//getIngameController().kill();
			} catch(e:Error) { Alert.alert(e.toString()); }
			
			if(btnEndTest && btnEndTest.parent) btnEndTest.parent.removeChild(btnEndTest);
			if(testLoader && testLoader.parent) testLoader.parent.removeChild(testLoader);
			testLoader.unloadAndStop(true);
			testLoader = null;
			
			System.gc();
			System.gc();
			
			panel.visible = true;
			container.visible = true;
			gridlines.visible = true;
			
			Mouse.show();
			if (ExternalInterface.available)
			{
				ExternalInterface.call("resizeElementToHeight", 0);
			}// end if
		}
		
		public static function redrawBorder():void {
			var left:Number = Global.safeMinimum(Global.childrenList(layerList, "leftmostX")) || 0;
			var right:Number = Global.safeMaximum(Global.childrenList(layerList, "rightmostX")) || 0;
			var top:Number = Global.safeMinimum(Global.childrenList(layerList, "topmostY")) || 0;
			var bottom:Number = Global.safeMaximum(Global.childrenList(layerList, "bottommostY")) || 0;
			
			borderSprite.graphics.clear();
			
			if(left == right) return;
			
			borderSprite.graphics.lineStyle(2, 0xFF0000);
			borderSprite.graphics.beginFill(0xFF0000, 0.1);
			borderSprite.graphics.drawRect(left * W, top * H, (1 + right - left) * W, (1 + bottom - top) * H);
			borderSprite.graphics.endFill();
		}
		
		public static function cutSelection():void{
			if(!selectionRect) return;
			
			copySelection();
			
			var r:int, c:int, key:String;
			
			for(key in layers) {
				for(r = 0; r < selectionRect.height; r++){
					for(c = 0; c < selectionRect.width; c++){
						layers[key].drawTile(selectionRect.x + c, selectionRect.y + r, null);
					}
				}
			}
		}
		
		public static function copySelection():void{
			if(!selectionRect) return;
			
			var r:int, c:int, i:int, layerTiles:Array, tile:Tile;
			selectionBuffer = {};
			
			for(i = layerList.length - 1; i >= 0; i--) {
				layerTiles = [];
				for(r = 0; r < selectionRect.height; r++){
					layerTiles[r] = [];
					for(c = 0; c < selectionRect.width; c++){
						tile = layerList[i].getTile(selectionRect.x + c, selectionRect.y + r);
						layerTiles[r][c] = tile ? tile.getType() : null;
					}
				}
				selectionBuffer[layerList[i].layerName] = layerTiles;
			}
		}
		
		public static function pasteBuffer():void{
			if(!selectionBuffer) return;
			
			var r:int, c:int, key:String, layerTiles:Array;
			var tx:int = Math.floor(container.mouseX / Controller.W);
			var ty:int = Math.floor(container.mouseY / Controller.H);
			
			for(key in selectionBuffer) {
				layerTiles = selectionBuffer[key];
				for(r = 0; r < selectionRect.height; r++){
					for(c = 0; c < selectionRect.width; c++){
						layers[key].drawTile(tx + c, ty + r, layerTiles[r][c]);
					}
				}
			}
		}
		
		public static function redrawSelection():void{
			selectionSprite.graphics.clear();
			selectionSprite.graphics.lineStyle(2, 0x6600FF);
			selectionSprite.graphics.beginFill(0xFF0000, 0.1);
			selectionSprite.graphics.drawRect(selectionRect.x * Controller.W, selectionRect.y * Controller.H, selectionRect.width * W, selectionRect.height * H);
			selectionSprite.graphics.endFill();
		}
		
	}
	
}
