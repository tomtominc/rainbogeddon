﻿package editor {
	import fl.motion.Color;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class PaletteSection {
		
		public var palette:Palette;
		public var separator:PaletteSeparator;
		public var contentClip:MovieClip;
		public var targetLayer:Layer;
		
		public function updateGraphics(newHover:Class, newSelected:Class, clear:Class):void {
			var childList:Array = [];
			
			for(var n:Number = 0; n < contentClip.numChildren; n ++) {
				var child:Sprite = contentClip.getChildAt(n) as Sprite;
				if(!child) continue;
				if((newHover && child is newHover) || (newSelected && child is newSelected) || (clear && child is clear))
					childList.push(child);
			}
			
			for(n = 0; n < childList.length; n ++) {
				child = childList[n];
				if(newSelected && child is newSelected) {
					child.parent.setChildIndex(child, child.parent.numChildren - 1);
					child.filters = [new GlowFilter(0xFF0000, 1, 6, 6, 3)];
					child.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 64, 0, 0);
				} else if(newHover && child is newHover) {
					child.parent.setChildIndex(child, child.parent.numChildren - 1);
					child.filters = [new GlowFilter(0xFF0000, 1, 3, 3, 2)];
					child.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 32, 0, 0);
				} else if(clear && child is clear) {
					child.filters = [];
					child.transform.colorTransform = new ColorTransform;
				}
			}
		}
		
		public function containsOneOf(c:Class):Boolean {
			for(var n:Number = 0; n < contentClip.numChildren; n ++) {
				if(contentClip.getChildAt(n) is c) return true;
			}
			return false;
		}
		
	}
	
}