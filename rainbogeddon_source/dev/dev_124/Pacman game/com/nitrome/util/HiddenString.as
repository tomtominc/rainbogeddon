﻿package com.nitrome.util {
	
	/**
	* A wrapper for Strings to protect them from tools like CheatEngine
	*
	* uses crummilicious XOR encryption :D
	*
	* @author Aaron Steed
	*/
	public class HiddenString {

		private var Value:String;
		private var r:int;

		public function HiddenString(string:String = ""){
			value = string;
		}

		public function set value(newValue:String):void {
			Value = "";
			r = Math.random() * 1000000;
			for(var i:int = 0; i < newValue.length; i++){
				Value += String.fromCharCode(newValue.charCodeAt(i) ^ r);
			}
		}

		public function get value():String {
			var result:String = "";
			for(var i:int = 0; i < Value.length; i++){
				result += String.fromCharCode(Value.charCodeAt(i) ^ r);
			}
				return result;
		}

	}
	
}