﻿package com.nitrome.util.clips {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	/* Separates all DisplayObjectContainers from their children from the bottom up */
	public function explodeClips(gfx:DisplayObjectContainer):void{
		for(var i:int = gfx.numChildren - 1; i > -1; i--){
			if(gfx.getChildAt(i) is DisplayObjectContainer){
				if((gfx.getChildAt(i) as DisplayObjectContainer).numChildren){
					explodeClips(gfx.getChildAt(i) as DisplayObjectContainer);
				} else {
					(gfx as DisplayObjectContainer).removeChildAt(i);
				}
			}
		}
	}

}