﻿package com.nitrome.util.clips {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	/* Pause hack - freezes gfx and all of its children */
	public function stopClips(gfx:DisplayObjectContainer):void{
		for(var i:int = 0; i < gfx.numChildren; i++){
			if (gfx.getChildAt(i) is MovieClip){
				(gfx.getChildAt(i) as MovieClip).stop();
			}
			if(gfx.getChildAt(i) is DisplayObjectContainer){
				stopClips(gfx.getChildAt(i) as DisplayObjectContainer);
			}
		}
	}

}