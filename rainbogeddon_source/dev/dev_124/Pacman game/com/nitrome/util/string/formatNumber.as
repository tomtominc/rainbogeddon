﻿package com.nitrome.util.string {
	
	/* Convert a number to a string to so many decimal places */
	public function formatNumber(value:Number, decimalPlaces:int):String{
		var str:String = "" + value;
		var stop:int = str.indexOf(".");
		if(stop > -1){
			str = str.substr(0, stop + decimalPlaces + 1);
		}
		return str;
	}

}