package com.nitrome.util.misc {
	/**
	 * I got bored of writing 
	 * 
	 * @author Aaron Steed, nitrome.com
	 */
	public function randomInt(start:int, finish:int):int {
		return start + Math.random() * (finish - start);
	}
}