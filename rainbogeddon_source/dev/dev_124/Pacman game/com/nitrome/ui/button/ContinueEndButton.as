﻿package com.nitrome.ui.button {
	import com.nitrome.sound.SoundManager;
	import flash.events.MouseEvent;

	/**
	* End of game button
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class ContinueEndButton extends BasicButton{
		
		public function ContinueEndButton() {
		}
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			execute();
		}
		
		public function execute():void
		{
			if(NitromeGame.root.popUpHolder) NitromeGame.root.popUpHolder.hide();
			NitromeGame.root.transition.goto("congrats");
			Game.clearStaticData();
			SoundManager.stopAllLoops();
			SoundManager.startLoops();
			SoundManager.garbageCollectChannels();	
		}		
		
	}
	
}