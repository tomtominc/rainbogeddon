﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Goto credits frame
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class CreditsButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void {
			super.onClick(e);
			NitromeGame.root.transition.goto("credits");
		}
		
		override protected function onMouseOver(e:MouseEvent):void{
			super.onMouseOver(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.menuPosLock(2);
				NitromeGame.root.hatNBowContainer.bow.menuPosLock(2);
			}// end if
		}
		override protected function onMouseOut(e:MouseEvent):void{
			super.onMouseOut(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.abortMenuPosLock();
				NitromeGame.root.hatNBowContainer.bow.abortMenuPosLock();			
			}// end if
		}		
		
	}
	
}