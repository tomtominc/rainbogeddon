/**
* Manipulates the music settings for the game and stores those settings in NitromeGame
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.button {
	import com.nitrome.sound.SoundManager;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class MusicToggle extends BasicButton{
		
		public function MusicToggle(){
			//show the right thing at the start...
			if(!NitromeGame.getMusic()){
				gotoAndStop("offUp");
			}else{
				gotoAndStop("onUp");
			}
			// add event listeners
			addEventListener(MouseEvent.CLICK, onClick);
		}
		override protected function updateGraphic():void {
			if(over){
				if (SoundManager.music){
					gotoAndStop("onOver");
				} else if (!SoundManager.music){
					gotoAndStop("offOver");
				}
			} else {
				if (SoundManager.music){
					gotoAndStop("onUp");
				} else if (!SoundManager.music){
					gotoAndStop("offUp");
				}
			}
		}
		
		override public function onClick(e:MouseEvent):void {
			super.onClick(e);
			SoundManager.toggleMusic();
			updateGraphic();
		}

		
	}
	
}
