package com.nitrome.ui
{
	
	import flash.display.MovieClip;
	import flash.ui.Keyboard;
	import flash.events.MouseEvent;
	import com.nitrome.ui.button.KeyButton;
		
	public class PlayerSelectPanel extends MovieClip
	{
				

		public function PlayerSelectPanel()
		{
			
			addEventListener(MouseEvent.CLICK, onClick, false, 0, true);		
		}// end public function PlayerSelectPanel()

		public function get keyButtonList():Vector.<KeyButton>
		{		
			var buttonList:Vector.<KeyButton> = new Vector.<KeyButton>;
			buttonList.push(p1_fire_key as KeyButton);
			buttonList.push(p1_up_key as KeyButton);
			buttonList.push(p1_down_key as KeyButton);
			buttonList.push(p1_left_key as KeyButton);
			buttonList.push(p1_right_key as KeyButton);
			buttonList.push(p2_fire_key as KeyButton);
			buttonList.push(p2_up_key as KeyButton);
			buttonList.push(p2_down_key as KeyButton);
			buttonList.push(p2_left_key as KeyButton);
			buttonList.push(p2_right_key as KeyButton);		
			return buttonList;
		}// end public function get keyButtonList():Vector.<KeyButton>		
		
		public function onClick(e:MouseEvent):void
		{
			
			var buttonList:Vector.<KeyButton> = keyButtonList;		
			
			for (var currButton:uint = 0; currButton < buttonList.length; currButton++)
			{
				buttonList[currButton].deselect();
			}// end for		
			
		}// end public function onClick(e:MouseEvent):void		
		
		
	}// end public class PlayerSelectPanel
	
}// end package