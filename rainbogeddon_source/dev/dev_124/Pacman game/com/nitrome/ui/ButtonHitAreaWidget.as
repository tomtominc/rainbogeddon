﻿package com.nitrome.ui {
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * Changes the hit area of whatever object it's placed in to its own hit area
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class ButtonHitAreaWidget extends Sprite{
		
		public function ButtonHitAreaWidget(){
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}
		
		public function init(e:Event = null):void{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			if(parent && parent is Sprite) (parent as Sprite).hitArea = this;
		}
		
	}

}