package com.nitrome.engine
{
	
	
	public class EntityPlayer1 extends EntityPlayer 
	{
			
		public function EntityPlayer1(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_upKey = new Vector.<int>(2);
			m_downKey = new Vector.<int>(2);
			m_leftKey = new Vector.<int>(2);
			m_rightKey = new Vector.<int>(2);
			m_fireKey = new Vector.<int>(2);			
			m_upKey[0]    = Game.g.m_p1UpKey;
			m_downKey[0]  = Game.g.m_p1DownKey;
			m_leftKey[0]  = Game.g.m_p1LeftKey;
			m_rightKey[0] = Game.g.m_p1RightKey;
			m_fireKey[0]  = Game.g.m_p1FireKey;
			if (!Game.m_isTwoPlayer)
			{
				m_upKey[1]    = Game.g.m_p2UpKey;
				m_downKey[1]  = Game.g.m_p2DownKey;
				m_leftKey[1]  = Game.g.m_p2LeftKey;
				m_rightKey[1] = Game.g.m_p2RightKey;
				m_fireKey[1]  = Game.g.m_p2FireKey;
			}
			else
			{
				m_upKey[1]    = -1;
				m_downKey[1]  = -1;
				m_leftKey[1]  = -1;
				m_rightKey[1] = -1;
				m_fireKey[1]  = -1;				
			}// end else
			m_weaponHolder = null;//new TailHolder(this);//new BulletHolder(this);//new BombHolder(this);// new TailHolder(this);
			NitromeGame.root.hudPanel.player1ScoreEnabled = true;
			NitromeGame.root.hudPanel.player1Score = 0;
		}
			
		public override function onHitUpdateHUD():void
		{		
			NitromeGame.root.hudPanel.player1PowerUp(null);
		}// end public override function onHitUpdateHUD():void			
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);	
			NitromeGame.root.hudPanel.player1ScoreEnabled = false;
			NitromeGame.root.hudPanel.player1PowerUp(null);
			
		}// end public override function onDeath(attacker_:Entity = null):void			
		
		public override function onKilledEntity(entity_:Entity):void
		{
			if (entity_ is EntityEnemy)
			{
				m_score.value = m_score.value + (entity_ as EntityEnemy).m_score.value;
				NitromeGame.root.hudPanel.player1Score = m_score.value;
			}// end if
		}// end public override function onKilledEntity(entity_:Entity):void		
		
		public override function onCollectUpdateHUD():void
		{
			NitromeGame.root.hudPanel.player1Score = m_score.value;
		}// end public override function onCollectUpdateHUD():void		
		
		public override function onApplyPowerUpUpdateHUD():void
		{		
			NitromeGame.root.hudPanel.player1PowerUp(m_weaponHolder);
		}// end public override function onApplyPowerUpUpdateHUD():void			
		
		public override function teleportIn():void
		{
			if (m_teleporter)
			{
				m_teleporter.m_twin.m_firstCollisionP1CheckPerformed = false;
			}// end if
			super.teleportIn();
		}// end public override function teleportIn():void				
		
		public override function get ringEffectType():Class
		{		
			return Player1PowerUpRingEffect;
		}// end public override function get ringEffectType():Class	
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedLightBlue;
		}// end public override function get destroyedEffectType():Class			
		
	}// end public class EntityPlayer1
	
}// end package