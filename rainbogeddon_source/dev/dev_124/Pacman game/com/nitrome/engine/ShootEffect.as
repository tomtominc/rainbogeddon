package com.nitrome.engine
{
	
	public class ShootEffect extends EffectCentered
	{
		
		private var m_parent:EntityCharacter;
				
		public function ShootEffect(posX_:Number, posY_:Number, parent_:EntityCharacter)
		{
		
			super(0, 0);
			x = posX_;
			y = posY_;
			m_parent = parent_;
			
		}// end public function ShootEffect(posX_:Number, posY_:Number, parent_:EntityCharacter)
		
		public override function doPhysics():void
		{
		
			var posX:Number = 0;
			var posY:Number = 0;							
			
			switch(m_parent.m_lastDir)
			{
				
				case Entity.DIR_UP:
				{
					posX = m_parent.x;
					posY = m_parent.y + m_parent.m_collisionRect.y + m_parent.m_vel.y;					
					break;
				}// end case

				case Entity.DIR_DOWN:
				{
					posX = m_parent.x;
					posY = m_parent.y - m_parent.m_collisionRect.y + m_parent.m_vel.y;													
					break;
				}// end case					
				
				case Entity.DIR_LEFT:
				{
					posX = m_parent.x + m_parent.m_collisionRect.x + m_parent.m_vel.x;
					posY = m_parent.y;														
					break;
				}// end case		
				
				case Entity.DIR_RIGHT:
				{
					posX = m_parent.x - m_parent.m_collisionRect.x + m_parent.m_vel.x;
					posY = m_parent.y;														
					break;
				}// end case						
				
			}// end switch
				
			x = posX;
			y = posY;			
					
		}// end public override function doPhysics():void			
		
	}// end public class ShootEffect
	
}// end package