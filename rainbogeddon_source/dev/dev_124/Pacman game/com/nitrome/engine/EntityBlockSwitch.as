package com.nitrome.engine
{
	
	
	public class EntityBlockSwitch extends Entity 
	{
	
		public static const STATE_OFF:uint           = 0;
		public static const STATE_SWITCHING_ON:uint  = 1;	
		public static const STATE_ON:uint            = 2;	
		public static const STATE_WARNING:uint       = 3;
		public static const STATE_SWITCHING_OFF:uint = 4;
		
		public static const ON_DURATION:int      = 3000; //10000
		public static const WARNING_DURATION:int = 3000;
		
		private var m_startTime:int;
		
		public function EntityBlockSwitch(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_state = STATE_OFF;
			gotoAndStop("off");
			m_collisionRect.x = 4;
			m_collisionRect.y = 4;
			m_collisionRect.width = 20;
			m_collisionRect.height = 20;
			m_moves = false;
			m_canDie = false;
			m_drawBitmap = Game.g.m_secondaryEntityBitmap;
			m_spatialHashMap.updateObject(this);
		}// end public function EntityBlockSwitch(tx_:uint, ty_:uint)
			
		
		public function onSwitchOffDone():void
		{
			m_state = STATE_OFF;
			gotoAndStop("off");
		}// end public function onSwitchOffDone():void

		public function onSwitchOnDone():void
		{
			m_state = STATE_ON;
			m_startTime = Game.g.timer;
			gotoAndPlay("on");
		}// end public function onSwitchOnDone():void	
		
		public function switchOn():void
		{
			m_state = STATE_SWITCHING_ON;
			gotoAndPlay("switching_on");	
		}// end public function switchOn():void			
		
		public override function doPhysics():void
		{
			
			var objectList:Vector.<GameObject> = Game.g.m_gameObjectList;
			var currObject:uint;
			
			switch(m_state)
			{
				
				case STATE_ON:
				{
					
					if (Game.g.timer - m_startTime > ON_DURATION)
					{
						m_startTime = Game.g.timer;
						m_state = STATE_WARNING;
						gotoAndPlay("warning");
					}// end if
					
					break;
				}// end case
				
				case STATE_WARNING:
				{
					
					if (Game.g.timer - m_startTime > WARNING_DURATION)
					{
						m_state = STATE_SWITCHING_OFF;
						gotoAndPlay("switching_back");
						objectList = Game.g.m_gameObjectList;
						for (currObject = 0; currObject < objectList.length; currObject++)
						{
							if (objectList[currObject] is EntityRainbowCoin)
							{
								(objectList[currObject] as EntityRainbowCoin).switchToSoftBlock();
							}// end if
						}// end for						
					}// end if					
					
					break;
				}// end case			
			
				case STATE_OFF:
				{
					
					if ((!Game.g.m_player1.m_terminated && collidesWithObject(Game.g.m_player1)) || (!Game.g.m_player2.m_terminated && collidesWithObject(Game.g.m_player2)))
					{
						switchOn();
						objectList = Game.g.m_gameObjectList;
						for (currObject = 0; currObject < objectList.length; currObject++)
						{
							if (objectList[currObject] is EntitySoftBlock)
							{
								(objectList[currObject] as EntitySoftBlock).switchToTempCoin();
							}
							else if ((objectList[currObject] != this) && (objectList[currObject] is EntityBlockSwitch))
							{
								(objectList[currObject] as EntityBlockSwitch).switchOn();
							}// end else if
						}// end for
					}// end if
					
					break;
				}// end case				
				
			}// end switch
			
		
			
		}// end override function doPhysics():void
		
	}// end public class EntityBlockSwitch
	
}// end package