package com.nitrome.engine
{
	import flash.geom.Vector3D;

	
	public class EntityDrill extends EntityCentered
	{
			
		public static const DAMAGE:int = 1;
		
		public static const STATE_OFF:int = 0;
		public static const STATE_ON:int  = 1;
		
		public var  m_damage:int;
		public var  m_wrapped:Boolean;
		public var  m_wrapTimes:uint;
		public var  m_parent:Entity;
		public var  m_isColliding:Boolean;
		private var m_effect:DrillEffect;
		
		public function EntityDrill(parent_:EntityCharacter)
		{
			super(0, 0);
			m_collisionRect.x = -2.5;
			m_collisionRect.y = -2.5;
			m_collisionRect.width = 5;
			m_collisionRect.height = 5;
			m_damage = DAMAGE;
			m_wrapped = false;
			m_parent = parent_;
			m_wrapTimes = 0;
			m_isColliding = false;
			
			if (parent_ is EntityPlayer1)
			{
				m_effect = new Player1DrillEffect();
			}
			else if (parent_ is EntityPlayer2)
			{
				m_effect = new Player2DrillEffect();
			}// end else
			
			m_frontChildGameObjectList.push(m_effect);				
		}// end public function EntityDrill()
				
		public function terminate():void
		{

			m_terminated = true;
			m_spatialHashMap.unregisterObject(this);
			m_parent.startMoving();
			
		}// end public function terminate():void			
		
		public function turnOn():void
		{

			if (m_state != STATE_ON)
			{			
				m_state = STATE_ON;
				gotoAndPlay("on");
			}// end if
			
		}// end public function turnOn():void	
		
		public function turnOff():void
		{
			
			if (m_state != STATE_OFF)
			{
				m_state = STATE_OFF;
				gotoAndPlay("idle");
				m_effect.gotoAndPlay("off");
				m_isColliding = false;
				m_parent.startMoving();
			}// end if
			
		}// end public function turnOff():void		
		
		public function goUp():void
		{
			
			x = m_parent.x;
			y = m_parent.y - 20;		
			//rotation = 0;
			m_effect.x = x;
			m_effect.y = y;		
			m_effect.rotation = 90;
			
		}// end public function goUp():void		
		
		public function goDown():void
		{
			
			x = m_parent.x;
			y = m_parent.y + 20;														
			//rotation = 180;
			m_effect.x = x;
			m_effect.y = y;		
			m_effect.rotation = 270;			
			
		}// end public function goDown():void	
		
		public function goLeft():void
		{
			
			x = m_parent.x - 20;
			y = m_parent.y;	
			//rotation = 270;
			m_effect.x = x;
			m_effect.y = y;		
			m_effect.rotation = 0;				
			
		}// end public function goLeft():void			
		
		public function goRight():void
		{
			
			x = m_parent.x + 20;
			y = m_parent.y;	
			//rotation = 90;
			m_effect.x = x;
			m_effect.y = y;		
			m_effect.rotation = 180;				
			
		}// end public function goRight():void			
		
		public override function doPhysics():void
		{
			
			if (m_terminated)
			{
				return;
			}// end if
			
			var posX:Number = 0;
			var posY:Number = 0;								
			
			var parentPlayer:EntityPlayer = m_parent as EntityPlayer;
			var parentVel:Vector3D = parentPlayer.m_vel;
			

			
			if (parentVel.x > 0)
			{
				goRight();
			}
			else if (parentVel.x < 0)
			{
				goLeft();
			}
			else if (parentVel.y < 0)
			{
				goUp();
			}
			else if (parentVel.y > 0)
			{
				goDown();
			}
			else if ((parentVel.x == 0) && (parentVel.y == 0))
			{
			
				switch((m_parent as EntityPlayer).m_lastDir)
				{
					
					case Entity.DIR_UP:
					case Entity.DIR_NONE:
					{
						goUp();
						break;
					}// end case

					case Entity.DIR_DOWN:
					{
						goDown();
						break;
					}// end case					
					
					case Entity.DIR_LEFT:
					{
						goLeft();
						break;
					}// end case		
					
					case Entity.DIR_RIGHT:
					{
						goRight();
						break;
					}// end case						
					
				}// end switch						
				
				
			}// end else if
			
			
			switch(m_state)
			{
				
				case STATE_OFF:
				{
					
					break;
				}// end case
				
				case STATE_ON:
				{
					
					var ignoreList:Vector.<Class> = new Vector.<Class>;
					ignoreList.push(EntityCollectible);
					ignoreList.push(EntityBullet);
					ignoreList.push(EntityBubbleParticle);
					ignoreList.push(EntitySafetyBubble);
					ignoreList.push(PowerUp);
					
					var collidingObject:Entity;


					collidingObject = getCollidingObject(ignoreList, m_parent);

					if (collidingObject)
					{
						
						collidingObject.doDamage(m_damage, null, m_parent);	
						
						if (!m_isColliding)
						{
							m_isColliding = true;
							m_parent.stopMoving();
							m_effect.gotoAndPlay("on");
						}// end if
						
					}
					else if(m_isColliding)
					{
						
						m_isColliding = false;
						m_parent.startMoving();
						m_effect.gotoAndPlay("off");
						
					}// end else
					
					break;
				}// end case				
				
			}// end switch
		
			
			updatePos();
			
			m_spatialHashMap.updateObject(this);
			
		}// end public override function doPhysics():void		
			
		
	}// end public class EntityDrill
	
}// end package