package com.nitrome.engine
{
	
	public class PowerUpBubble extends PowerUp
	{
		
		public function PowerUpBubble(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpBubble(tx_:uint, ty_:uint)
		
		public override function get holder():Class
		{		
			return SafetyBubbleHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpBubble
	
}// end package