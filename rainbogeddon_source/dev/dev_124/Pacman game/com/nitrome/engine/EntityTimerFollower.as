package com.nitrome.engine
{
	
	
	public class EntityTimerFollower extends EntityEnemy 
	{
			
		public static const PATROLLING_DURATION:int = 15000;
		public static const FOLLOWING_DURATION:int = 15000;
		public static const	BREAK_FREE_WAIT_DURATION:int = 2500;
		public static const	SPEED_NORMAL:Number = 1.5;
		public static const	SPEED_FAST:Number = EntityPlayer.SPEED*0.7;
		
		public  var m_behaviorStartTime:int;
		public  var m_startFollowing:Boolean;
		public  var m_endFollowing:Boolean;		
		private var m_isStationaryWhileFree:Boolean;
		private var m_stationaryWhileFreeStartTime:int;				
		
		public function EntityTimerFollower(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = SPEED_NORMAL;
			m_glow.m_color = 0xffffb500;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 100;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_followsTarget = false;
			m_behaviorStartTime = Game.g.timer;
			m_startFollowing = false;
			m_endFollowing = false;
			m_hasBackAndForthDecPerc = false;
			
		}// end public function EntityTimerFollower(tx_:uint, ty_:uint)
					
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			var xBefore:Number = x;
			var yBefore:Number = y;			
			
			if (!m_endFollowing && !m_startFollowing)
			{
				super.doPhysics();
				
				if (!m_smartFollowing)
				{
				
					if (!m_followsTarget && (Game.g.timer - m_behaviorStartTime > PATROLLING_DURATION))
					{
						startFollowing();
						//m_behaviorStartTime = Game.g.timer;
						//m_followsTarget = true;
					} 
					else if (m_followsTarget && (Game.g.timer - m_behaviorStartTime > FOLLOWING_DURATION))
					{
						endFollowing();
						//m_behaviorStartTime = Game.g.timer;
						//m_followsTarget = false;				
					}// end else if		
					
				}// end if
				
			}
			else
			{					
				updateDamage();
			}// end if			
			
			
			// hack to stop freezing bug
			if (!m_isStationaryWhileFree)
			{
			
				if ((xBefore == x) && (yBefore == y))
				{
					m_isStationaryWhileFree = true;
					m_stationaryWhileFreeStartTime = Game.g.timer;
				}// end if
				
			}
			else
			{
				
				if ((xBefore != x) || (yBefore != y))
				{
					m_isStationaryWhileFree = false;
				}
				else if((Game.g.timer - m_stationaryWhileFreeStartTime) > BREAK_FREE_WAIT_DURATION)
				{
					//trace("TIMER FOLLOWER RESETTING");
					m_isStationaryWhileFree = false;
					m_followsTarget = false;	
					m_targetInRange = false;
					m_smartFollowing = false;
					m_isAlerted = false;
					m_behaviorStartTime = Game.g.timer;
					m_endFollowing = false;
					m_startFollowing = false;		
					m_forceUpdateAnim = true;
					m_speed = SPEED_NORMAL;
					m_vel.normalize();
					m_vel.scaleBy(m_speed);
				}// end else if
				
			}// end else			
			
			
		}// end public override function doPhysics():void
		
		public override function playSideWalkAnim():void
		{
			if (!m_followsTarget)
			{
				gotoAndPlay("walk_side");
			}
			else
			{
				gotoAndPlay("walk_side_following");
			}// end else
		}// end public override function playSideWalkAnim():void
		
		public override function playUpWalkAnim():void
		{	
			if (!m_followsTarget)
			{
				gotoAndPlay("walk_up");
			}
			else
			{
				gotoAndPlay("walk_up_following");
			}// end else			
		}// end public override function playUpWalkAnim():void		
		
		public override function playDownWalkAnim():void
		{
			if (!m_followsTarget)
			{
				gotoAndPlay("walk_down");
			}
			else
			{
				gotoAndPlay("walk_down_following");
			}// end else				
		}// end public override function playDownWalkAnim():void			
		
		public function startFollowingDone():void
		{		
			stop();
			//trace("startFollowingDone");
			m_behaviorStartTime = Game.g.timer;
			m_startFollowing = false;
			m_endFollowing = false;
			m_forceUpdateAnim = true;
			m_glow.m_color = 0xff89ff0a;
		}// end public function startFollowingDone():void			
		
		public function startFollowing():void
		{		
			
			//trace("startFollowing");
			
			if (!m_followsTarget)
			{
				
				gotoAndPlay("get_angry");
				m_behaviorStartTime = Game.g.timer;
				m_followsTarget = true;			
				m_startFollowing = true;	
				m_endFollowing = false;
				
			}
			else
			{
				startFollowingDone();
			}// end else
			
			//trace("EntityPlayer.SPEED 2");
			m_speed = SPEED_FAST;
			m_vel.normalize();
			m_vel.scaleBy(m_speed);			
					
		}// end public function startFollowing():void			
		
		public function endFollowing():void
		{		
			
			//trace("endFollowing");		
			
			gotoAndPlay("calm_down");				
			m_behaviorStartTime = Game.g.timer;
			m_endFollowing = true;
			m_startFollowing = false;
			m_followsTarget = false;	
			//trace("SPEED_NORMAL");
			m_speed = SPEED_NORMAL;
			m_vel.normalize();
			m_vel.scaleBy(m_speed);			
			
		}// end public function endFollowing():void		
		
		public function endFollowingDone():void
		{		
			//trace("endFollowingDone");
			stop();
			m_behaviorStartTime = Game.g.timer;
			m_endFollowing = false;
			m_startFollowing = false;
			m_forceUpdateAnim = true;
			m_glow.m_color = 0xffffb500;
		}// end public function endFollowingDone():void			
		
		/*public override function gotoAndPlay(frame:Object, scene:String = null):void
		{
			
			if (m_endFollowing || m_startFollowing)
			{
				trace("gotcha");
			}// end if
			
			super.gotoAndPlay(frame, scene);
			
		}// end public override function gotoAndPlay(frame:Object, scene:String = null):void*/ 		
		
		public override function startAlert():void
		{		
			if (!m_endFollowing && !m_startFollowing && !m_followsTarget)
			{
				//trace("EntityPlayer.SPEED");
				m_speed = SPEED_FAST;
				m_vel.normalize();
				m_vel.scaleBy(m_speed);				
				super.startAlert();	
			}// end if
		}// end public override function startAlert():void			
		
		public override function alertDone():void
		{		
			super.alertDone();
			startFollowing();
			m_isStationaryWhileFree = false;
			//trace("alertDone");
		}// end public override function alertDone():void		
		
		public override function onStopSmartFollowing():void
		{
			m_skipAnim = true;
			//trace("onStopSmartFollowing");
			endFollowing();
		}// end public override function onStopSmartFollowing():void			
		
		public override function get alertedLabel():String
		{		
			
			if (m_followsTarget)
			{
				return "alerted_angry";
			}
			else
			{
				return "alerted";
			}// end else
			
		}// end public override function get alertedLabel():String		
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnOrange;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedOrange;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityTimerFollower
	
}// end package