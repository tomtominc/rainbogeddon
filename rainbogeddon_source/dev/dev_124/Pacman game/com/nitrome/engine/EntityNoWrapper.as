package com.nitrome.engine
{
	
	import com.nitrome.engine.pathfinding.PathMap;
	
	public class EntityNoWrapper extends EntityEnemy 
	{
		
		public static const ANIM_SAD:uint          = 4;
		public static const SAD_TRIGGER_DIST:uint  = 5;
		
		public var m_sad:Boolean;
		public var m_prevAnimLabel:String;
		
		public function EntityNoWrapper(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = 2.25;
			m_glow.m_color = 0xffffb500;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 1000;
			m_canWrap = false;
			m_sad = false;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			
			if (!EntityPlayer.m_onWrappedListenerList)
			{
				EntityPlayer.m_onWrappedListenerList = new Vector.<Function>;
			}// end if		
			
			EntityPlayer.m_onWrappedListenerList.push(onPlayerWrapped);
			
		}// end public function EntityNoWrapper(tx_:uint, ty_:uint)
							
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);
			if (EntityPlayer.m_onWrappedListenerList)
			{
				for (var currListener:uint = 0; currListener < EntityPlayer.m_onWrappedListenerList.length; currListener++ )
				{
					if (EntityPlayer.m_onWrappedListenerList[currListener] == onPlayerWrapped)
					{
						EntityPlayer.m_onWrappedListenerList.splice(currListener, 1);
						break;
					}// end if
				}// end for
				
			}// end if				
			
		}// end public override function onDeath(attacker_:Entity = null):void						
		
		public function onPlayerWrapped():void
		{
			
			if (m_followsTarget && (m_distFromTarget <= SAD_TRIGGER_DIST) && !m_isAlerted)
			{
				gotoAndPlay("sad");
				m_sad = true;
				m_currAnim = ANIM_SAD;
				m_velChangeStartTime = 0;
				m_prevVel.x = 0;
				m_prevVel.y = 0;
			}// end if
			
		}// end public function onPlayerWrapped():void			
		
		public function onSadEnded():void
		{
			m_sad = false;
		}// end public function onSadEnded():void	
		
		public override function startAlert():void
		{		
			super.startAlert();
			m_sad = false;
		}// end public override function startAlert():void				
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			if (!m_sad)
			{
				super.doPhysics();
			}
			else
			{
				updateDamage();
			}// end else
			
		}// end if
		
		public override function get player1Map():PathMap
		{
			return Game.g.m_player1NoWrapPathMap;
		}// end public override function get player1Map():PathMap		
		
		public override function get player2Map():PathMap
		{
			return Game.g.m_player2NoWrapPathMap;
		}// end public override function get player2Map():PathMap	
		
		public override function get player1SafetyMap():PathMap
		{
			return Game.g.m_safetyPlayer1NoWrapPathMap;
		}// end public override function get player1SafetyMap():PathMap		
		
		public override function get player2SafetyMap():PathMap
		{
			return Game.g.m_safetyPlayer2NoWrapPathMap;
		}// end public override function get player2SafetyMap():PathMap			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnOrange;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedOrange;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityNoWrapper
	
}// end package