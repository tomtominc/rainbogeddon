package com.nitrome.engine
{
	import flash.geom.Vector3D;
	
	
	public class EntityHomeyShooter extends EntityEnemy 
	{
			
		public  var m_homey:EntityHomey;	
		private var m_shooting:Boolean;
		private var m_homeyOverrideDir:int;
		private var m_startedShooting:Boolean;
		
		public function EntityHomeyShooter(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			//m_speed = 1.5;
			m_glow.m_color = 0xffff0000;
			m_recoverWaitDuration = 4000;
			m_followsTarget = false;
			m_targetTriggerRange = 6;
			m_hasOnTargetRangeTrigger = true;	
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_hitPoints = 90;
			m_maxHitPoints = m_hitPoints;
			m_homey = null;
			//m_backChildGameObjectList = new Vector.<GameObject>;
			m_shooting = false;
			m_homeyOverrideDir = DIR_NONE;
			m_startedShooting = false;
		}// end public function EntityHomeyShooter(tx_:uint, ty_:uint)
				
		public function shoot():void
		{
			m_homey = new EntityHomey(this);
			Game.g.m_gameObjectList.push(m_homey);
			//m_backChildGameObjectList.push(m_homey);
			m_homey.applyOverrideDir(m_homeyOverrideDir);
		}// end public function shoot():void		
		
		public function shootingDone():void
		{
			m_shooting = false;	
			m_forceUpdateAnim = true;
			m_startedShooting = false;
			m_alertEffect = null;
		}// end public function shootingDone():void	
		
		public function startShooting():void
		{
			
			
			m_shooting = true;
			
			var targetDirection:int = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
			
			m_homeyOverrideDir = targetDirection;
		
			m_alertEffect = new EnemyAlertEffect(this);
			Game.g.m_gameObjectList.push(m_alertEffect);			
			
			switch(targetDirection)
			{
				
				case DIR_LEFT:
				{
					gotoAndPlay("walk_side_shooting");
					flipX(false);					
					break;
				}// end case
				
				case DIR_RIGHT:
				{
					gotoAndPlay("walk_side_shooting");
					flipX(true);					
					break;
				}// end case				
				
				case DIR_UP:
				{
					gotoAndPlay("walk_up_shooting");
					break;
				}// end case					
				
				case DIR_DOWN:
				case DIR_NONE:
				{
					gotoAndPlay("walk_down_shooting");
					break;
				}// end case					
				
			}// end switch
			
		}// end public function startShooting():void			
		
		public override function playSideWalkAnim():void
		{
			m_shooting = false;
			super.playSideWalkAnim();
		}// end public override function playSideWalkAnim():void
		
		public override function playUpWalkAnim():void
		{	
			m_shooting = false;
			super.playUpWalkAnim();
		}// end public override function playUpWalkAnim():void		
		
		public override function playDownWalkAnim():void
		{
			m_shooting = false;
			super.playDownWalkAnim();			
		}// end public override function playDownWalkAnim():void			
		
		public override function doPhysics():void
		{
			if (!m_shooting)
			{
				super.doPhysics();
			}
			else
			{
				updateDamage();
				tickChildren();
				updateAbsCollisionRect();
			}// end else
		}// end if		
	
		public override function onCenterNextTile():void
		{		
			if (!m_shooting && (!m_homey || m_homey.m_terminated))
			{
			
				//trace("stopped at tile");
				startShooting();
			
			}// end if				
		}// end public override function onCenterNextTile():void
		
		public override function whileTargetInRange(target_:Entity):void
		{
			
			//trace("m_homey = " + m_homey.m_terminated);
			if (!m_shooting && (!m_homey || m_homey.m_terminated) && !m_startedShooting)
			{
				//startShooting();
				//trace("start shooting with m_homey = " + m_homey + ", m_shooting = " + m_shooting + ", m_stoppingAtTile = " + m_stoppingAtTile);
				m_startedShooting = true;
				signalPosOnCenterNextTile();
			}// end if
			
		}// end public override function whileTargetInRange(target_:Entity):void				
		
		public override function startAlert():void
		{		
			super.startAlert();
			
			if (m_shooting)
			{
				m_shooting = false;
				m_startedShooting = false;
			}// end if
			
		}// end public override function startAlert():void		
				
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnRed;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedRed;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityHomeyShooter
	
}// end package