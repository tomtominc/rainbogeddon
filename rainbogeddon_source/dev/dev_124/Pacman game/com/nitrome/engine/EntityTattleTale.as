package com.nitrome.engine
{
	
	import com.nitrome.engine.pathfinding.PathMap;
	
	public class EntityTattleTale extends EntityEnemy 
	{
	
		public static const ALERT_COUNTDOWN_DURATION:int = EntityEnemy.SMART_FOLLOW_DURATION;
		public static const ALERT_STATE_NONE:uint                   = 0;
		public static const ALERT_STATE_ALERTING:uint               = 1;
		public static const ALERT_STATE_COUNTDOWN:uint              = 2;
		public static const ALERT_STATE_COUNTDOWN_DONE_WAITING:uint = 3;
		
		public  var m_alertState:uint;
		private var m_currAlertTargetPlayerNumber:uint;
		private var m_currAlertTarget:Entity;
	    private var m_alertRing:AlertRing;
		private var m_alertBackground:AlertBackground;
		private var m_targetMarker:TargetMarker;
		private var m_waitToSignal:Boolean;
		private var m_signalSent:Boolean;
		private var m_countDownSent:Boolean;
		public  var m_pathMap:PathMap;
		
		public function EntityTattleTale(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			//m_speed = 1.5;
			m_glow.m_color = 0xffff3503;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = MIN_CHECK_DIR_TIMEOUT;
			m_followsTarget = false;
			m_checkDirTimeout = m_checkDirMaxTimeout;		
			m_targetTriggerRange = 3;
			m_hasOnTargetRangeTrigger = true;			
			m_canSmartFollow = false;
			m_alertState = ALERT_STATE_NONE;
			m_alertRing = null;
			m_alertBackground = null;
			m_targetMarker = null;
			m_currAlertTarget = null;
			m_waitToSignal = false;
			m_signalSent = false;
			m_countDownSent = false;
			m_hitPoints = 180;
			m_maxHitPoints = m_hitPoints;
			m_pathMap = new PathMap(Game.g.mapWidth, Game.g.mapHeight);
		}// end public function EntityTattleTale(tx_:uint, ty_:uint)
				
		public override function doPhysics():void
		{
			
			switch(m_alertState)
			{
				
				case ALERT_STATE_NONE:
				{
					
					super.doPhysics();
					
					break;
				}// end case				
				
						
				default:
				{
					
					updateMap();
					updateTarget();
					updateTargetRange();
					updateDamage();					
					
					var currAlertTargetDist:uint;
					
					if (m_currAlertTarget is EntityPlayer1)
					{
						currAlertTargetDist = m_player1PathMap.getDistFromTarget(m_cTX, m_cTY);
					}
					else if (m_currAlertTarget is EntityPlayer2)
					{
						currAlertTargetDist = m_player2PathMap.getDistFromTarget(m_cTX, m_cTY);
					}// end else if
					
					var enemy:EntityEnemy;
					var distFromTarget:uint;
					var distFromThis:uint;
					
					
					if (m_alertState == ALERT_STATE_COUNTDOWN)
					{
					
						if (Game.g.timer - m_smartFollowCountdownStartTime > ALERT_COUNTDOWN_DURATION)
						{
							onAlertCountdownEnded();
						}
						else
						{
							
							if ((m_distFromTarget == PathMap.TILE_BLOCKED) || ((m_chosenTarget != m_currAlertTarget) && (currAlertTargetDist == PathMap.TILE_BLOCKED)))
							{
								
								// target is unreachable
								onAlertCountdownEnded();
								
								// stop alert state on enemies
								for(var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
								{
									if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
									{
										(Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
									}// end if
								}// end for										
								
							}
							else
							{
					
								// broadcast message to all other new enemies to start following
								// the spotted player
								for (currObject = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
								{
									if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
									{
										
										enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;
										
										distFromThis = m_pathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
										
										if (m_currAlertTargetPlayerNumber == 1)
										{
											distFromTarget = m_player1PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
										}
										else
										{
											distFromTarget = m_player2PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
										}// end else											
										
										if (enemy.m_canSmartFollow && (!enemy.m_smartFollowing || (m_currAlertTargetPlayerNumber != enemy.m_smartFollowingPlayerNumber)) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
										{
											enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
											enemy.startSmartPlayerFollowCountdown();
											enemy.m_smartFollowCountdownStartTime = m_smartFollowCountdownStartTime;
										}
										else if (enemy.m_canSmartFollow && enemy.m_smartFollowing && (distFromTarget == PathMap.TILE_BLOCKED) && (distFromThis == PathMap.TILE_BLOCKED))
										{
											enemy.stopSmartPlayerFollow();
										}// end else if
										
									}// end if
								}// end for
							
							}// end else
						
						}// end else
						
					}
					else if ((m_alertState == ALERT_STATE_ALERTING) && m_signalSent)
					{
						
						if ((m_distFromTarget == PathMap.TILE_BLOCKED) || ((m_chosenTarget != m_currAlertTarget) && (currAlertTargetDist == PathMap.TILE_BLOCKED)))
						{
							
							// target is unreachable
							onAlertCountdownEnded();
							
							// stop alert state on enemies
							for(currObject = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
							{
								if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
								{
									(Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
								}// end if
							}// end for										
							
						}
						else
						{						
					
							// broadcast message to all other new enemies to start following
							// the spotted player
							for(currObject = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
							{
								if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
								{
									
									enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;
									
									distFromThis = m_pathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
									
									if (m_currAlertTargetPlayerNumber == 1)
									{
										distFromTarget = m_player1PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
									}
									else
									{
										distFromTarget = m_player2PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
									}// end else									
									
									if (enemy.m_canSmartFollow && (!enemy.m_smartFollowing || (m_currAlertTargetPlayerNumber != enemy.m_smartFollowingPlayerNumber)) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
									{
										enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
									}
									else if (enemy.m_canSmartFollow && enemy.m_smartFollowing && (distFromTarget == PathMap.TILE_BLOCKED) && (distFromThis == PathMap.TILE_BLOCKED))
									{
										enemy.stopSmartPlayerFollow();
									}// end else if									
									
								}// end if
							}// end for		
					
						}// end else
						
					}// end else if
					
					break;
				}// end case
				
			}// end switch
			
			m_pathMap.m_tileMap = new Vector.<uint>;
						
			if (Game.g.m_safetyTileMap.m_active)
			{
				m_pathMap.m_tileMap = m_pathMap.m_tileMap.concat(Game.g.m_safetyTileMap.m_tileMap);
			}
			else
			{
				m_pathMap.m_tileMap = m_pathMap.m_tileMap.concat(Game.g.m_enemyTileMap.m_tileMap);
			}// end else
			
			
			m_pathMap.floodFill(m_cTX, m_cTY);				
			
		}// end public override function doPhysics():void
		
		public override function playSideWalkAnim():void
		{
			if (m_alertState == ALERT_STATE_NONE)
			{
				gotoAndPlay("walk_side");
			}// end if
		}// end public override function playSideWalkAnim():void
		
		public override function playUpWalkAnim():void
		{	
			if (m_alertState == ALERT_STATE_NONE)
			{
				gotoAndPlay("walk_up");
			}// end if		
		}// end public override function playUpWalkAnim():void		
		
		public override function playDownWalkAnim():void
		{
			if (m_alertState == ALERT_STATE_NONE)
			{
				gotoAndPlay("walk_down");
			}// end if		
		}// end public override function playDownWalkAnim():void			
		
		public function onAlertCountdownEnded():void
		{
			m_alertState = ALERT_STATE_COUNTDOWN_DONE_WAITING;
			//trace("ended countdown");
		}// end public function onAlertCountdownEnded():void
		
		public function doneStartAlerting():void
		{
			if (!m_alertRing)
			{
				m_alertRing = new AlertRing(this);
				Game.g.m_gameObjectList.push(m_alertRing);
				m_alertBackground = new AlertBackground();
				Game.g.m_gameObjectList.push(m_alertBackground);
			}// end if			
			
			if (m_waitToSignal && !m_countDownSent)
			{
			
				m_signalSent = true;
				//trace("alerted enemies after wait");
				// broadcast message to all other enemies to start following
				// the spotted player
				for(var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
				{
					if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
					{
						(Game.g.m_gameObjectList[currObject] as EntityEnemy).startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
					}// end if
				}// end for		
			
			}// end if			
			
		}// end public function doneStartAlerting():void		
		
		public override function startAlert():void
		{		
			//super.startAlert();
			
		}// end public override function startAlert():void		
		
		public override function onTargetInRange(target_:Entity):void
		{
		
			//if (m_alertState != ALERT_STATE_ALERTING)
			//{
			
				var playerNumber:uint = target_ is EntityPlayer1 ? 1 : 2;
				m_currAlertTargetPlayerNumber = playerNumber;
				if (target_ != m_currAlertTarget)
				{
					if (m_targetMarker)
					{
						m_targetMarker.m_terminated = true;
					}// end if
					m_targetMarker = new TargetMarker(target_);
					Game.g.m_gameObjectList.push(m_targetMarker);
					m_currAlertTarget = target_;
				}// end if
				
				m_waitToSignal = false;
				m_signalSent = false;
				
				if ((m_alertState != ALERT_STATE_COUNTDOWN) && (m_alertState != ALERT_STATE_ALERTING))
				{
					gotoAndPlay("start_alert");
					m_waitToSignal = true;
					m_alertEffect = new EnemyAlertEffect(this);
					Game.g.m_gameObjectList.push(m_alertEffect);
				}// end if
				
				m_countDownSent = false;
				m_alertState = ALERT_STATE_ALERTING;
				var enemy:EntityEnemy;
				var distFromTarget:uint;
				var distFromThis:uint;				
				//trace("started alert");				
				
				if (!m_waitToSignal)
				{
				
					m_signalSent = true;
					//trace("alerted enemies immediately");
					// broadcast message to all other enemies to start following
					// the spotted player
					for(var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
					{
						if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
						{
							
							enemy = Game.g.m_gameObjectList[currObject] as EntityEnemy;
							
							distFromThis = m_pathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
							
							if (m_currAlertTargetPlayerNumber == 1)
							{
								distFromTarget = m_player1PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
							}
							else
							{
								distFromTarget = m_player2PathMap.getDistFromTarget(enemy.m_cTX, enemy.m_cTY);
							}// end else									
							
							if ((enemy.m_canSmartFollow && !enemy.m_smartFollowing) && (distFromTarget != PathMap.TILE_BLOCKED) && (distFromThis != PathMap.TILE_BLOCKED))
							{
								enemy.startSmartPlayerFollow(m_currAlertTargetPlayerNumber);
							}// end if
							
						}// end if
					}// end for		
				
				}// end if
				
			//}// end if
			
		}// end public override function onTargetInRange(target_:Entity):void			
		
		public function onAlertEnded():void
		{
			m_alertState = ALERT_STATE_NONE;
			m_forceUpdateAnim = true;	
			m_alertRing.m_terminated = true;
			m_alertRing = null;
			m_alertBackground.terminate();
			m_alertBackground = null;
			m_currAlertTarget = null;
			m_targetMarker.m_terminated = true;
			m_targetMarker = null;
			m_countDownSent = false;
			//trace("ended alert animation");
		}// end public function onAlertEnded():void		
		
		public function checkAlert():void
		{
			if (m_alertState != ALERT_STATE_COUNTDOWN_DONE_WAITING)
			{			
				gotoAndPlay("alerting");
			}
			else
			{
				gotoAndPlay("end_alert");
			}// end else
		}// end public function checkAlert():void
		
		public override function updateTargetRange():void
		{
			
			if (!m_hasOnTargetRangeTrigger)
			{
				return;
			}// end if
			
			if (m_distFromTarget <= m_targetTriggerRange)
			{
				
				if (!m_targetInRange || ((m_chosenTarget != m_currInRangeTarget) && (m_distFromTarget < m_currInRangeTargetDist)))
				{
					m_currInRangeTarget = m_chosenTarget;
					m_currInRangeTargetDist = m_distFromTarget;
					m_targetInRange = true;
					onTargetInRange(m_chosenTarget);	
					whileTargetInRange(m_chosenTarget);
				}
				else
				{	
					whileTargetInRange(m_chosenTarget);
				}// end else
			
				if (m_currInRangeTarget == Game.g.m_player1)
				{
					m_currInRangeTargetDist = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
				}
				else if (m_currInRangeTarget == Game.g.m_player2)
				{
					m_currInRangeTargetDist = m_player2PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
				}// end else if				
				
			}
			else if (m_targetInRange && (m_distFromTarget > m_targetTriggerRange))
			{
				m_currInRangeTarget = null;
				m_targetInRange = false;
				onTargetOutOfRange();
			}// end else if
			
		}// end public override function updateTargetRange():void		
		
		public override function onTargetOutOfRange():void
		{
			
			if (m_alertState == ALERT_STATE_ALERTING)
			{
			
				m_alertState = ALERT_STATE_COUNTDOWN;
				m_smartFollowCountdownStartTime = Game.g.timer;
				m_countDownSent = true;
				//trace("started countdown");
				// start alert countdown on enemies
				for(var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
				{
					if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
					{
						(Game.g.m_gameObjectList[currObject] as EntityEnemy).startSmartPlayerFollowCountdown();
					}// end if
				}// end for				
			
			}// end if
				
		}// end public override function onTargetOutOfRange():void		
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);
			
			if (m_alertRing)
			{
				m_alertRing.m_terminated = true;
			}// end if
			
			if (m_alertBackground)
			{
				m_alertBackground.terminate();
			}// end if
			
			if (m_targetMarker)
			{
				m_targetMarker.m_terminated = true;		
			}// end if
			
			if (m_targetInRange)
			{
			
				// stop alert state on enemies
				for(var currObject:uint = 0; currObject < Game.g.m_gameObjectList.length; currObject++)
				{
					if (Game.g.m_gameObjectList[currObject] is EntityEnemy)
					{
						(Game.g.m_gameObjectList[currObject] as EntityEnemy).stopSmartPlayerFollow();
					}// end if
				}// end for			
				
			}// end if
			
		}// end public override function onDeath(attacker_:Entity = null):void					
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnRed;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedRed;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityTattleTale
	
}// end package