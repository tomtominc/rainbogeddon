package com.nitrome.engine
{
	import com.nitrome.util.HiddenInt;
	import flash.geom.Rectangle;
	
	
	public class EntityCoin extends EntityCollectible
	{
		
		public static const MAX_SIZE:uint = 4;
		
		private static var m_listenerList:Vector.<Function>;
		public  var m_size:uint;
		private static var m_sizeToScoreTable:Object;
		private static var m_sizeToCollisionRectTable:Object;
		
		public function EntityCoin(tx_:uint, ty_:uint, size_:uint = 1)
		{
			m_drawFast = true;
			super(tx_, ty_);	
			/*m_collisionRect.x = -5;
			m_collisionRect.y = -5;
			m_collisionRect.width = 10;
			m_collisionRect.height = 10;*/
			m_size = size_;
			Game.m_numCoinsInLevel.value++;
			
			if (!m_sizeToScoreTable)
			{
			
				m_sizeToScoreTable = new Object();
				
				m_sizeToScoreTable[1] = new HiddenInt(1);
				m_sizeToScoreTable[2] = new HiddenInt(2);
				m_sizeToScoreTable[3] = new HiddenInt(4);
				m_sizeToScoreTable[4] = new HiddenInt(6);
				
				m_sizeToCollisionRectTable = new Object();
				
				m_sizeToCollisionRectTable[1] = new Rectangle( -5, -5, 10, 10);
				m_sizeToCollisionRectTable[2] = new Rectangle( -7.5, -7.5, 15, 15);
				m_sizeToCollisionRectTable[3] = new Rectangle( -10, -10, 20, 20);
				m_sizeToCollisionRectTable[4] = new Rectangle( -12.5, -12.5, 25, 25);
			
			}// end if
			
			m_collisionRect = m_sizeToCollisionRectTable[m_size];
			m_spatialHashMap.updateObject(this);
			m_score.value = m_sizeToScoreTable[m_size].value;
			
		}// end public function EntityCoin(tx_:uint, ty_:uint)
		
		public static function free():void
		{
			m_listenerList = null;
			m_sizeToScoreTable = null;
			m_sizeToCollisionRectTable = null;
		}// end public static function free():void
		
		public override function collect()
		{
			if (!m_collected)
			{
				Game.m_numCoinsInLevel.value--;
				super.collect();
				notifyCollectedListeners();
			}// end if
		}// end public override function collect()			
		
		public override function doPhysics():void
		{
			
			if ((EntityEnemy.m_numDead > m_size-1) && (m_size < MAX_SIZE))
			{
				m_size++;
				m_score.value = m_sizeToScoreTable[m_size].value;
				m_collisionRect = m_sizeToCollisionRectTable[m_size];
				gotoAndPlay("size_" + m_size);
				m_spatialHashMap.updateObject(this);
			}
			else if ((EntityEnemy.m_numDead < m_size-1))
			{
				m_size--;
				m_score.value = m_sizeToScoreTable[m_size].value;
				m_collisionRect = m_sizeToCollisionRectTable[m_size];
				gotoAndPlay("size_" + m_size);
				m_spatialHashMap.updateObject(this);
			}// end else if
			
		}// end public override function doPhysics():void		
		
		public static function addCollectedListener(listener_:Function):void
		{
			
			if (!m_listenerList)
			{
				m_listenerList = new Vector.<Function>;
			}// end if			
			
			m_listenerList.push(listener_);
		}// end public static function addCollectedListener(listener_:Function):void				
		

		public static function removeCollectedListener(listener_:Function):void
		{
			for (var currListener:uint = 0; currListener < m_listenerList.length; currListener++)
			{
				
				if (m_listenerList[currListener] == listener_)
				{
					m_listenerList.splice(currListener, 1);
					return;
				}// end if
					
			}// end for			
		}// end public static function removeCollectedListener(listener_:Function):void		
		
		private function notifyCollectedListeners():void
		{
			if (m_listenerList)
			{
				var lengthBefore:uint = m_listenerList.length;
				for (var currListener:uint = 0; currListener < m_listenerList.length; currListener++)
				{
					lengthBefore = m_listenerList.length; 
					m_listenerList[currListener]();
					if ((m_listenerList.length < lengthBefore) && (lengthBefore >= 2))
					{
						currListener--;
					}// end if
				}// end for
			}// end if
		}// end private function notifyCollectedListeners():void		
		
		
	}// end public class EntityCoin
	
}// end package