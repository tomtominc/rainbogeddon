package com.nitrome.engine
{
	import com.nitrome.engine.pathfinding.RadialPathMap;
	import flash.display.BitmapData;
	import com.nitrome.engine.pathfinding.PathMap;
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	import com.nitrome.geom.PointUint;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class EntityBomb extends EntityCentered
	{
		
		public static const STATE_WAITING_T0_EXPLODE:uint     = 0;
		public static const STATE_EXPLODING:uint              = 1;
		public static const STATE_EXPLOSION_FADING:uint       = 2;
		public static const STATE_EXPLOSION_OVER:uint         = 3;
		
		public static const EXPLOSION_DURATION:int = 700;

		public var  m_tileMapBitmap:BitmapData;
		public var  m_bombClip:MovieClip;
		public var  m_radialPathMap:RadialPathMap;
		public var  m_radius:uint;
		public var  m_size:uint;
		public var  m_accAffectedTilePosList:Vector.<Vector.<PointUint>>;
		public var  m_expandLayer:Boolean;
		public var  m_stoppedClips:Boolean;
		private var m_powerLevel:uint;
		private var m_startedAddingTiles:Boolean;
		public static var m_explosionParticleTileMap:TileMap;
		public static var m_explosionTilePosList:Vector.<Vector.<Vector.<PointUint>>>;
		public var m_explosionParticleList:Vector.<ExplosionParticle>;
		public var m_numFadedParticles:uint;
		public var m_numFadedInParticles:uint;
		public var m_explosionStartTime:int;
		public var m_currExplodingTile:uint;
		public var m_currMainAnimFrameOffset:uint;
		private var m_currMainAnimFrameOffsetCreated:Boolean;		
		private var m_parent:Entity;
		public var m_explodingNormal:Boolean;
		public var m_exploded:Boolean;
		private var m_block:EntityBombBlock;
		private var m_addedBlock:Boolean;
		
		public function EntityBomb(tx_:uint, ty_:uint, powerLevel_:uint, parent_:Entity = null)
		{
			
			super(tx_, ty_);
			m_parent = parent_;
			m_collisionRect.x = 1;
			m_collisionRect.y = 1;
			m_collisionRect.width = 23;
			m_collisionRect.height = 23;
			m_expandLayer = false;
			var powerLevelToSize:Object = new Object();
			m_powerLevel = powerLevel_;
			m_exploded = false;
			powerLevelToSize[1] = 2;
			powerLevelToSize[2] = 3;
			powerLevelToSize[3] = 4;
			gotoAndPlay("level_" + powerLevel_);
			m_size = powerLevelToSize[powerLevel_];
			m_stoppedClips = false;
			m_bombClip = new (getDefinitionByName("BombCircle" + m_size) as Class)();
			m_startedAddingTiles = false;
			m_numFadedParticles = 0;
			m_numFadedInParticles = 0;
			m_currExplodingTile = 0;
			m_canDie = false;
			m_currMainAnimFrameOffsetCreated = false;
			m_addedBlock = false;
			
			if (!m_explosionTilePosList)
			{
				m_explosionParticleTileMap = new TileMap(Game.g.mapWidth, Game.g.mapHeight);
				m_explosionTilePosList = new Vector.<Vector.<Vector.<PointUint>>>;
			}// end if
			
			m_explosionParticleList = new Vector.<ExplosionParticle>;
			
			var tileBlockedColor:uint = 0xffff0000;
			var sentinelColor:uint    = 0xff00ff00;			
			
			// bomb width and height need to be an odd number
			m_radius = (m_bombClip.width - 1) / 2;
			m_radialPathMap = new RadialPathMap(Game.g.mapWidth, Game.g.mapHeight, RadialPathMap.TILE_BLOCKED);
			
			
			m_tileMapBitmap = new BitmapData(Game.g.mapWidth, Game.g.mapHeight, true, tileBlockedColor);
			
			var matrix:Matrix = new Matrix();
			
			matrix.translate(m_tx, m_ty);
			
			m_tileMapBitmap.draw(m_bombClip, matrix);		
			
			// draw wrapped portions of the bomb
			
			if ((m_tx - m_radius) < 0)
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty);
				m_tileMapBitmap.draw(m_bombClip, matrix);
			}
			else if ((m_tx + m_radius) >= m_tileMapBitmap.width)
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty);
				m_tileMapBitmap.draw(m_bombClip, matrix);				
			}// end else
			
			if ((m_ty - m_radius) < 0)
			{
				matrix.identity();
				matrix.translate(m_tx, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);
			}
			else if ((m_ty + m_radius) >= m_tileMapBitmap.height)
			{
				matrix.identity();
				matrix.translate(m_tx, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);				
			}// end else			
			
			if (((m_tx - m_radius) < 0) && ((m_ty - m_radius) < 0))
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);						
			}
			else if (((m_tx - m_radius) < 0) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);				
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty - m_radius) < 0))
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);				
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bombClip, matrix);				
			}// end else if
			
			// replace the circle color with the sentinel color
			m_tileMapBitmap.threshold(m_tileMapBitmap, m_tileMapBitmap.rect, new Point(0,0), "==", 0xff000000, sentinelColor, 0xffffffff);
			
									
			m_state = STATE_WAITING_T0_EXPLODE;			
			
			m_block = null;
			
		}// end public function EntityBomb(tx_:uint, ty_:uint)
				
		public static function free():void
		{
			m_explosionParticleTileMap = null;
			m_explosionTilePosList = null;
		}// end public static function free():void		
		
		public override function doPhysics():void
		{
			
			switch(m_state)
			{
				
		
				case STATE_WAITING_T0_EXPLODE:
				{
					
					if (!m_addedBlock)
					{
					
						var ignoreList:Vector.<Class> = new Vector.<Class>;
						ignoreList.push(EntityCollectible);
						ignoreList.push(EntityBullet);
						ignoreList.push(EntityBubbleParticle);
						ignoreList.push(EntitySafetyBubble);
						ignoreList.push(EntityBomb);
						ignoreList.push(PowerUp);					

						
						var collidingObject:Entity = getCollidingObject(ignoreList);
						
						if (!collidingObject)
						{
							m_block = new EntityBombBlock(m_tx, m_ty);
							Game.g.m_gameObjectList.push(m_block);
							m_addedBlock = true;
						}// end if
						
					}// end if
					
					break;
				}// end case					
								
				case STATE_EXPLODING:
				{
					
					continueExploding();
					
					break;
				}// end case	
				
				case STATE_EXPLOSION_FADING:
				{
				
					m_currMainAnimFrameOffset++;
					
					if (m_currMainAnimFrameOffset >= ExplosionParticle.NUM_MAIN_ANIM_LOOP_FRAMES)
					{
						m_currMainAnimFrameOffset = 0;
					}// end if					
					
					var explosionRect:Rectangle = new Rectangle();
					var gameObjectList:Vector.<Entity>;
					explosionRect.width = Game.TILE_WIDTH;
					explosionRect.height = Game.TILE_HEIGHT;	
					
					var lastExplodingTile:uint = m_currExplodingTile + 20;
					var reset:Boolean = false;
					
					if (lastExplodingTile >= m_explosionParticleList.length)
					{
						lastExplodingTile = m_explosionParticleList.length;
						reset = true;
					}// end if
					
					for (m_currExplodingTile = 0; m_currExplodingTile < m_explosionParticleList.length; m_currExplodingTile++) // var currExplodingTile:uint = 0;
					{
						

						gameObjectList = m_spatialHashMap.getMatchingList(m_explosionParticleList[m_currExplodingTile]);
						
						for (var currGameObj:uint = 0; currGameObj < gameObjectList.length; currGameObj++)
						{
										 
							if (!(gameObjectList[currGameObj] is ExplosionParticle))
							{
								
								gameObjectList[currGameObj].kill(m_parent);
								
							}// end if
							 
						}// end for
						
					}// end for					
					
					if (reset)
					{
						m_currExplodingTile = 0;
					}// end if
					
					if (Game.g.timer - m_explosionStartTime > EXPLOSION_DURATION)
					{
						m_state = STATE_EXPLOSION_OVER;				
						
						if (m_explosionParticleList.length)
						{
							for (var currParticle:uint = 0; currParticle < m_explosionParticleList.length; currParticle++)
							{
								m_explosionParticleList[currParticle].fade();
							}// end for
						}
						else
						{
							m_terminated = true;
							super.onDeath();
						}// end else
						
					}// end if
					

					
					break;
				}// end case				
				
			}// end switch
			
		}// end public override function doPhysics():void			
		
		public function onFaded():void
		{
			
			if (!m_terminated)
			{
				//trace("onFaded");
				m_numFadedParticles++;
				
				if (m_numFadedParticles >= m_explosionParticleList.length)
				{
				
					for (var currList:uint = 0; currList < m_explosionTilePosList.length; currList++ )
					{
						if (m_explosionTilePosList[currList] == m_accAffectedTilePosList)
						{
							//trace("deleted list");
							m_explosionTilePosList.splice(currList, 1);
							break;
						}// end if
					}// end for			
					m_terminated = true;
					super.onDeath();
				
				}// end if
				
			}// end if
			
		}// end public function onFaded():void
		
		public function explode():void
		{
				
			if (m_block)
			{
				m_block.terminate();
				m_block = null;
			}// end if
			
			// merge the game object map with the bomb map
			// using only the hard tiles since these are the only
			// ones that don't get destroyed
			
			var tileBlockedColor:uint = 0xffff0000;
			var sentinelColor:uint    = 0xff00ff00;					
			
			var tileMap:Vector.<uint> = new Vector.<uint>(m_tileMapBitmap.width * m_tileMapBitmap.height);
			
			for (var currTile:uint = 0; currTile < tileMap.length; currTile++)
			{
				tileMap[currTile] = sentinelColor;
			}// end for				
			
			var gameObjectList:Vector.<GameObject> =  Game.g.m_gameObjectList;
			
			for (var currGameObj:uint = 0; currGameObj < gameObjectList.length; currGameObj++)
			{
				
				if (tileMap[gameObjectList[currGameObj].m_ty * m_tileMapBitmap.width + gameObjectList[currGameObj].m_tx] == sentinelColor)
				{
					tileMap[gameObjectList[currGameObj].m_ty * m_tileMapBitmap.width + gameObjectList[currGameObj].m_tx] = ((gameObjectList[currGameObj] is EntityHardBlock) || (gameObjectList[currGameObj] is EntityGateBlock) || (gameObjectList[currGameObj] is EntityBubbleParticle))? tileBlockedColor : sentinelColor;		
				}// end if
				
			}// end for					
			
			var bombMapList:Vector.<uint> = m_tileMapBitmap.getVector(m_tileMapBitmap.rect);
			
			for (currTile = 0; currTile < bombMapList.length; currTile++)
			{
				if ((bombMapList[currTile] == sentinelColor) && (tileMap[currTile] == tileBlockedColor))
				{
					bombMapList[currTile] = tileBlockedColor;
				}// end if
			}// end for
			
				
			
	
			for (currTile = 0; currTile < m_radialPathMap.m_map.length; currTile++)
			{
				if (bombMapList[currTile] == tileBlockedColor)
				{
					m_radialPathMap.m_map[currTile] = RadialPathMap.TILE_BLOCKED;
				}// end if
			}// end for			
			
			
			m_radialPathMap.castPath(m_tx, m_ty, m_radius);
			m_state = STATE_EXPLODING;
			
			m_accAffectedTilePosList = new Vector.<Vector.<PointUint>>;
			continueExploding();
			Game.g.m_gameScreen.shake();
			m_exploded = true;
				
		}// end public function explode():void			

		public function onParticleFadeIn():void
		{
			
			if (!m_currMainAnimFrameOffsetCreated)
			{
				m_currMainAnimFrameOffsetCreated = true;
				m_currMainAnimFrameOffset = 0;
			}// end if
			
			m_numFadedInParticles++;
			if (m_numFadedInParticles >= m_explosionParticleList.length)
			{		
				
				m_currMainAnimFrameOffset = 0;
				m_explodingNormal = true;
				// search for other active bombs and synchronize with them				
				
				var gameObjectList:Vector.<GameObject> = Game.g.m_gameObjectList;
				
				for (var currObject:uint = 0; currObject < gameObjectList.length; currObject++)
				{
					if ((gameObjectList[currObject] != this) && (gameObjectList[currObject] is EntityBomb))
					{
						var bomb:EntityBomb = gameObjectList[currObject] as EntityBomb;
						
						if (bomb.m_explodingNormal)
						{
							m_currMainAnimFrameOffset = bomb.m_currMainAnimFrameOffset;
							break;
						}// end if
						
					}// end if
				}// end for
				
				for (var currParticle:uint = 0; currParticle < m_explosionParticleList.length; currParticle++)
				{
					m_explosionParticleList[currParticle].startSyncedAnimLoop();
				}// end for				
				
			}// end if
			
		}// end public function onParticleFadeIn():void			
	
		
		public function continueExploding():void
		{
			
	
			var entityCollisionRect:Rectangle = new Rectangle();
			var gameObjectList:Vector.<GameObject> =  Game.g.m_gameObjectList;
			
			var done:Boolean = false;
			var addClips:Boolean = false;
			
			var affectedTilePosList:Vector.<PointUint> = new Vector.<PointUint>;
			var fadeInClip:ExplosionFadeInGuideParticle;
			

			m_expandLayer = false;
			done = m_radialPathMap.continueCastingPath();
			affectedTilePosList = m_radialPathMap.getNewMarkedPathPosList();
			m_accAffectedTilePosList.push(affectedTilePosList);
			addClips = true;	
			fadeInClip = new ExplosionFadeInGuideParticle();
			
			if (!m_startedAddingTiles)
			{					
				m_startedAddingTiles = true;
				m_explosionTilePosList.push(m_accAffectedTilePosList);
			}// end if				

			
			for (var currExplodingTile:uint = 0; currExplodingTile < affectedTilePosList.length; currExplodingTile++)
			{
				
				var absPosX:int = affectedTilePosList[currExplodingTile].m_x * Game.TILE_WIDTH;
				var absPosY:int = affectedTilePosList[currExplodingTile].m_y * Game.TILE_HEIGHT;				
				
				//var posX:int = absPosX - (m_tx * Game.TILE_WIDTH) - Math.floor(Game.TILE_WIDTH/2);
				//var posY:int = absPosY - (m_ty * Game.TILE_HEIGHT) - Math.floor(Game.TILE_HEIGHT/2);
				 
				if (addClips)
				{
				
					var explosion:ExplosionParticle = new ExplosionParticle(affectedTilePosList[currExplodingTile].m_x, affectedTilePosList[currExplodingTile].m_y, this, fadeInClip);//new (getDefinitionByName("ExplosionLevel" + m_powerLevel) as Class)();
					Game.g.m_gameObjectList.push(explosion);
					m_explosionParticleList.push(explosion);				
					
				}// end if
				
				/*var explosionRect:Rectangle = new Rectangle(absPosX, absPosY, Game.TILE_WIDTH, Game.TILE_HEIGHT);
				 
				for (var currGameObj:uint = 0; currGameObj < gameObjectList.length; currGameObj++)
				{
								 
					if ((!(gameObjectList[currGameObj] is EntityCoin)) && (gameObjectList[currGameObj] is Entity))
					{
						
						var entity:Entity = gameObjectList[currGameObj] as Entity;
						
						//entityCollisionRect.x = entity.x + entity.m_collisionRect.x + entity.m_vel.x;
						//entityCollisionRect.y = entity.y + entity.m_collisionRect.y + entity.m_vel.y;
						//entityCollisionRect.width = entity.m_collisionRect.width;
						//entityCollisionRect.height = entity.m_collisionRect.height;					
					
						if (explosionRect.intersects(entity.m_absCollisionRect))//entityCollisionRect))
						{
					
							entity.kill();
						
						}// end if
						
					}// end if
					 
				}// end for*/
				
			}// end for		
			
			if (done)
			{
								
				m_explosionStartTime = Game.g.timer;
				m_state = STATE_EXPLOSION_FADING;
				
			}// end if
			
			
		}// end public function continueExploding():void		
		
		public override function onGamePaused():void
		{
			super.onGamePaused();
			stop();
			if (m_state != STATE_EXPLOSION_FADING)
			{
				for (var currChild:uint = 0; currChild < numChildren; currChild++)
				{
					if (getChildAt(currChild) is MovieClip)
					{
						(getChildAt(currChild) as MovieClip).stop();
					}// end if
				}// end for
			}// end if			
		}// end public override function onGamePaused():void		
		
		public override function onGameUnpaused():void
		{
			super.onGameUnpaused();		
			if (m_state == STATE_WAITING_T0_EXPLODE)
			{
				play();
			}// end if
			if (m_state != STATE_EXPLOSION_FADING)
			{
				for (var currChild:uint = 0; currChild < numChildren; currChild++)
				{
					if (getChildAt(currChild) is MovieClip)
					{
						(getChildAt(currChild) as MovieClip).play();
					}// end if
				}// end for
			}// end if			
		}// end public override function onGameUnpaused():void				
		
		public override function draw():void
		{		
			
			super.draw();
			
			// do this so the pattern will be drawn on the same tile
			Game.g.m_tileMap.m_tileMap[(m_ty * Game.g.mapWidth) + m_tx] = PathMap.TILE_SENTINEL;
			
		}// end public override function draw():void
		
	}// end public class EntityBomb
	
}// end package