package com.nitrome.engine
{
	
	public class TeleporterDisappearEffect extends EffectCentered
	{
				
		public function TeleporterDisappearEffect(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function TeleporterDisappearEffect(tx_:uint, ty_:uint)
		
	}// end public class TeleporterDisappearEffect
	
}// end package