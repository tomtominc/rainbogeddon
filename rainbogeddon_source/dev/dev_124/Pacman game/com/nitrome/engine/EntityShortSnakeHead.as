package com.nitrome.engine
{
	
	
	public class EntityShortSnakeHead extends EntityEnemy 
	{
		
		public var m_tail:EntitySnakeTail;
		public var m_bodySegmentList:Vector.<EntitySnakeBody>;
			
		public function EntityShortSnakeHead(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			
			m_glow.m_color = 0xffff3503;
			m_followsTarget = false;
			m_checkDirMaxTimeout = MIN_CHECK_DIR_TIMEOUT;
			m_checkDirTimeout = m_checkDirMaxTimeout;				
			
			m_bodySegmentList = new Vector.<EntitySnakeBody>;
			m_bodySegmentList.push(new EntitySnakeBody(this, this));
			m_bodySegmentList[m_bodySegmentList.length - 1].m_glow.terminate();
			//m_backChildGameObjectList = new Vector.<GameObject>;
			Game.g.m_gameObjectList.push(m_bodySegmentList[m_bodySegmentList.length - 1]);
			m_bodySegmentList.push(new EntitySnakeBody(m_bodySegmentList[m_bodySegmentList.length - 1], this));
			m_bodySegmentList[m_bodySegmentList.length - 1].m_glow.terminate();
			Game.g.m_gameObjectList.push(m_bodySegmentList[m_bodySegmentList.length - 1]);
			
			m_bodySegmentList.push(new EntitySnakeBody(m_bodySegmentList[m_bodySegmentList.length - 1], this));
			Game.g.m_gameObjectList.push(m_bodySegmentList[m_bodySegmentList.length - 1]);						
			
			m_tail = new EntitySnakeTail(m_bodySegmentList[m_bodySegmentList.length - 1], this);
			Game.g.m_gameObjectList.push(m_tail);
			
		}// end public function EntityShortSnakeHead(tx_:uint, ty_:uint)
					
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);
			
			for (var currSegment:uint = 0; currSegment < m_bodySegmentList.length; currSegment++)
			{
				if (!m_bodySegmentList[currSegment].m_terminated)
				{
					m_bodySegmentList[currSegment].onDeath(attacker_);
				}// end if
			}// end for
			
			m_tail.onDeath(attacker_);
			
		}// end public override function onDeath(attacker_:Entity = null):void			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnRed;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedRed;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityShortSnakeHead
	
}// end package