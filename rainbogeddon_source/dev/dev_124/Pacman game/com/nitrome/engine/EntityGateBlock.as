package com.nitrome.engine
{
	
	import flash.geom.Point;
	
	public class EntityGateBlock extends EntityBlock
	{
		
		private var m_digitList:Vector.<GateBlockDigit>;
		private var m_posList:Vector.<Vector.<Point>>;
		private var m_count:uint;
			
		public function EntityGateBlock(tx_:uint, ty_:uint, count_:String)
		{
			super(tx_, ty_);
			m_count = new uint(count_);
			m_canDie = false;
			m_digitList = new Vector.<GateBlockDigit>;
			m_posList = new Vector.<Vector.<Point>>(3);
			m_posList[0] = new Vector.<Point>(1);
			m_posList[0][0] = new Point(10,9); 
			m_posList[1] = new Vector.<Point>(2);
			m_posList[1][0] = new Point(7,9); m_posList[1][1] = new Point(13,9); 
			m_posList[2] = new Vector.<Point>(3);
			m_posList[2][0] = new Point(4,9); m_posList[2][1] = new Point(10,9); m_posList[2][2] = new Point(16,9);
						
			m_digitList.push(new GateBlockDigit());
			m_digitList[0].m_drawBitmap = m_drawBitmap;
			m_digitList.push(new GateBlockDigit());
			m_digitList[1].m_drawBitmap = m_drawBitmap;
			m_digitList.push(new GateBlockDigit());	
			m_digitList[2].m_drawBitmap = m_drawBitmap;
			
			m_frontChildGameObjectList.push(m_digitList[0]);
			m_frontChildGameObjectList.push(m_digitList[1]);
			m_frontChildGameObjectList.push(m_digitList[2]);
			
			updateText();
			
			EntityCoin.addCollectedListener(onCoinCollected);
			
		}// end function EntityGateBlock(tx_:uint, ty_:uint)				
		
		public function updateText():void
		{
						
			var countText:String = new String(m_count);
			var length:uint = countText.length;
			
			// clear text first
			for (var currDigit:uint = 0; currDigit < m_digitList.length; currDigit++)
			{
				m_digitList[currDigit].gotoAndStop(11);
			}// end for					
			
			for (var currChar:uint = 0; currChar < length; currChar++)
			{
				m_digitList[currChar].gotoAndStop(new uint(countText.charAt(currChar)) + 1);
				m_digitList[currChar].x = m_posList[length - 1][currChar].x + x;
				m_digitList[currChar].y = m_posList[length - 1][currChar].y + y;
			}// end for					
						
		}// end function updateText():void			
		
		public function onCoinCollected():void
		{
			
			m_count--;
			updateText();
			
			if (m_count <= 0)
			{
				
				m_terminated = true;
				Game.g.m_gameObjectList.push(new ShooterSoftBlockDestroyed(m_tx, m_ty));
				EntityCoin.removeCollectedListener(onCoinCollected);
				m_spatialHashMap.unregisterObject(this);
				Game.g.m_tileMap.onMapChanged();
				Game.g.m_drawTileMap.onMapChanged();
				//m_posRegTable[m_ty * Game.g.mapWidth + m_tx] = null;
				
			}// end if
						
		}// end public function onCoinCollected():void		
		
		public override function doPhysics():void
		{
						
					
						
		}// end override function doPhysics():void				
		
	}// end public class EntityGateBlock
	
}// end package