package com.nitrome.engine
{
	
	
	public class EntityEvader extends EntityEnemy 
	{
		
		public static const	BREAK_FREE_WAIT_DURATION:int = 2500;
		public static const	FOLLOWING_DURATION:int = 12000;
		
		public  var m_startFollowing:Boolean;
		public  var m_endFollowing:Boolean;
		private var m_isStationaryWhileFree:Boolean;
		private var m_stationaryWhileFreeStartTime:int;	
		private var m_followingStartTime:int;
		
		public function EntityEvader(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = 1.5;
			m_glow.m_color = 0xff89ff00;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_followsTarget = false;
			m_evadesTarget = true;
			m_targetTriggerRange = 4;
			m_hasOnTargetRangeTrigger = true;	
			m_hasDecreasingTimeout = false;
			m_hasBackAndForthDecPerc = false;
			m_endFollowing = false;
			m_startFollowing = false;
		}// end public function EntityEvader(tx_:uint, ty_:uint)
					
		public override function playSideWalkAnim():void
		{
			if (m_isAlerted || m_endFollowing || m_startFollowing)
			{
				//trace("trying to interrupt");
				return;
			}// end if				
			if (m_evadesTarget && !m_smartFollowing)
			{
				gotoAndPlay("walk_side");
			}
			else
			{
				gotoAndPlay("walk_side_angry");
			}// end else
		}// end public override function playSideWalkAnim():void
		
		public override function playUpWalkAnim():void
		{
			if (m_isAlerted || m_endFollowing || m_startFollowing)
			{
				//trace("trying to interrupt");
				return;
			}// end if			
			if (m_evadesTarget && !m_smartFollowing)
			{
				gotoAndPlay("walk_up");
			}
			else
			{
				gotoAndPlay("walk_up_angry");
			}// end else
		}// end public override function playUpWalkAnim():void		
		
		public override function playDownWalkAnim():void
		{
			if (m_isAlerted || m_endFollowing || m_startFollowing)
			{
				//trace("trying to interrupt");
				return;
			}// end if
			if (m_evadesTarget && !m_smartFollowing)
			{
				gotoAndPlay("walk_down");
			}
			else
			{
				gotoAndPlay("walk_down_angry");
			}// end else
		}// end public override function playDownWalkAnim():void				
		
		/*public override function gotoAndPlay(frame:Object, scene:String = null):void
		{
			
			if (m_endFollowing || m_startFollowing)
			{
				trace("gotcha");
			}// end if
			
			super.gotoAndPlay(frame, scene);
			
		}// end public override function gotoAndPlay(frame:Object, scene:String = null):void*/ 
		
		public override function doPhysics():void
		{
			
			var xBefore:Number = x;
			var yBefore:Number = y;
						
			
			if (!m_endFollowing && !m_startFollowing)
			{
				
				if (!m_smartFollowing && !m_evadesTarget && (Game.g.timer - m_followingStartTime > FOLLOWING_DURATION))
				{
					//trace("ended timer");
					endFollowing();
				}// end if				
				
				super.doPhysics();
				
			}
			else
			{				
				updateDamage();
				updateAbsCollisionRect();
			}// end if
			
		
			// hack to "fix" freezing bug
			if (!m_isStationaryWhileFree)
			{
			
				if ((xBefore == x) && (yBefore == y))
				{
					m_isStationaryWhileFree = true;
					m_stationaryWhileFreeStartTime = Game.g.timer;
				}// end if
				
			}
			else
			{
				
				if ((xBefore != x) || (yBefore != y))
				{
					m_isStationaryWhileFree = false;
				}
				else if((Game.g.timer - m_stationaryWhileFreeStartTime) > BREAK_FREE_WAIT_DURATION)
				{
					//trace("EVADER RESETTING");
					m_isStationaryWhileFree = false;
					m_followsTarget = false;	
					m_targetInRange = false;
					m_smartFollowing = false;
					m_isAlerted = false;
					m_endFollowing = false;
					m_startFollowing = false;		
					m_evadesTarget = true;
					m_forceUpdateAnim = true;
				}// end else if
				
			}// end else			
			
			
		}// end public override function doPhysics():void
		
		public override function onStopSmartFollowing():void
		{
			m_skipAnim = true;
			//trace("onStopSmartFollowing");
			endFollowing();
		}// end public override function onStopSmartFollowing():void				
		
		public override function alertDone():void
		{		
			super.alertDone();
			startFollowing();
			m_isStationaryWhileFree = false;
			//trace("alertDone");
		}// end public override function alertDone():void
		
		public function endFollowing():void
		{		
			
			//trace("endFollowing");
			
			if (m_prevVel.x < 0)
			{
				gotoAndPlay("calm_down_side");
				flipX(false);
			}
			else if (m_prevVel.x > 0)
			{
				gotoAndPlay("calm_down_side");
				flipX(true);
			}
			else if (m_prevVel.y < 0)
			{
				gotoAndPlay("calm_down_up");
			}
			else if ((m_prevVel.y > 0) || (!m_prevVel.x && !m_prevVel.y))
			{
				gotoAndPlay("calm_down_down");
			}// end else if		
			
			m_endFollowing = true;
			m_startFollowing = false;
			m_followsTarget = false;
			m_evadesTarget = true;			
			
			
		}// end public function endFollowing():void		
		
		public function endFollowingDone():void
		{		
			//trace("endFollowingDone");
			stop();
			m_endFollowing = false;
			m_startFollowing = false;
			m_forceUpdateAnim = true;
			m_glow.m_color = 0xff89ff00;
		}// end public function endFollowingDone():void			
		
		public function getAngryDone():void
		{		
			stop();
			//trace("getAngryDone");
			m_startFollowing = false;
			m_endFollowing = false;
			m_forceUpdateAnim = true;
			m_glow.m_color = 0xffff3503;
			m_followingStartTime = Game.g.timer;
		}// end public function getAngryDone():void				
		
		public function startFollowing():void
		{		
			
			//trace("startFollowing");
			
			if (!m_followsTarget)
			{
			
			
				if (m_prevVel.x < 0)
				{
					gotoAndPlay("get_angry_side");
					flipX(false);
				}
				else if (m_prevVel.x > 0)
				{
					gotoAndPlay("get_angry_side");
					flipX(true);
				}
				else if (m_prevVel.y < 0)
				{
					gotoAndPlay("get_angry_up");
				}
				else if ((m_prevVel.y > 0) || (!m_prevVel.x && !m_prevVel.y))
				{
					gotoAndPlay("get_angry_down");
				}// end else if	
			
				m_startFollowing = true;
				m_followsTarget = true;
				m_evadesTarget = false;					
				
			}
			else
			{
				getAngryDone();
			}// end else
			
		}// end public function startFollowing():void			
		
		public override function startAlert():void
		{		
			if (!m_endFollowing && !m_startFollowing)
			{
				//trace("startAlert");
				super.startAlert();	
			}// end if
		}// end public override function startAlert():void		
		
		public override function get alertedLabel():String
		{		
			
			if (m_followsTarget)
			{
				return "alerted_angry";
			}
			else
			{
				return "alerted";
			}// end else
			
		}// end public override function get alertedLabel():String			
		
		public override function onTargetInRange(target_:Entity):void
		{
			if (!m_followsTarget)
			{
				if (!m_endFollowing && !m_smartFollowing)
				{
					startFollowing();
				}
				else
				{
					m_targetInRange = false;
				}// end else
			}// end if		
		}// end public override function onTargetInRange(target_:Entity):void			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnGreen;
		}// end public override function get respawnEffectType():Class		
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedGreen;
		}// end public override function get destroyedEffectType():Class					
					
	}// end public class EntityEvader
	
}// end package