package com.nitrome.engine
{
	
	
	public class EntityShooterBullet extends EntityBullet
	{
			
		public function EntityShooterBullet(velX_:Number, velY_:Number)
		{
			super(velX_, velY_, 2);
		}// end public function EntityShooterBullet(velX_:Number, velY_:Number)	
		
		public override function get effect():Class
		{
			return ShooterBulletImpactEffect;
		}// end public override function get effect():Class		
		
	}// end public class EntityShooterBullet
	
}// end package