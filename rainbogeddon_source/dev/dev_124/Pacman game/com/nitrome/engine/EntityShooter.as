package com.nitrome.engine
{
	
	
	public class EntityShooter extends EntityEnemy 
	{
		
		public static const FIRE_DELAY:int = 1000;
		
		private var m_shooting:Boolean;

		public function EntityShooter(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);		
			m_speed = 1.0;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;	
			m_weaponHolder = new BulletHolder(this);
			(m_weaponHolder as BulletHolder).m_fireDelay = FIRE_DELAY;
			m_checksLineOfSight = true;
			m_shooting = false;
			m_glow.m_color = 0xffe800ff;
		}// end public function EntityShooter(tx_:uint, ty_:uint)		
			
		public function shootingDone():void
		{
			m_shooting = false;
			m_forceUpdateAnim = true;
		}// end public function shootingDone():void
		
		public override function startAlert():void
		{		
			super.startAlert();
			m_shooting = false;
		}// end public override function startAlert():void
		
		public function startShooting():void
		{
			
			m_shooting = true;
						
			if (m_prevVel.x < 0)
			{
				gotoAndPlay("walk_side_shooting");
				flipX(false);
			}
			else if (m_prevVel.x > 0)
			{
				gotoAndPlay("walk_side_shooting");
				flipX(true);
			}
			else if (m_prevVel.y < 0)
			{
				gotoAndPlay("walk_up_shooting");
			}
			else if (m_prevVel.y > 0)
			{
				gotoAndPlay("walk_down_shooting");
			}// end else if				
			
		}// end public function startShooting():void		
		
		public override function playSideWalkAnim():void
		{
			m_shooting = false;
			super.playSideWalkAnim();
		}// end public override function playSideWalkAnim():void
		
		public override function playUpWalkAnim():void
		{	
			m_shooting = false;
			super.playUpWalkAnim();
		}// end public override function playUpWalkAnim():void		
		
		public override function playDownWalkAnim():void
		{
			m_shooting = false;
			super.playDownWalkAnim();			
		}// end public override function playDownWalkAnim():void		
		
		public function shoot():void
		{
			m_weaponHolder.tryFire();
		}// end public function shoot():void			
		
		public override function whileOnLineOfSightPlayer1():void
		{
					
			if ((!m_shooting && m_weaponHolder.canFire()) &&
				(((m_vel.x < 0) && (m_lineOfSightPlayer1Dir2 == DIR_LEFT))  || 
				((m_vel.x > 0) && (m_lineOfSightPlayer1Dir1 == DIR_RIGHT))  ||
				((m_vel.y < 0) && (m_lineOfSightPlayer1Dir2 == DIR_UP))     ||
				((m_vel.y > 0) && (m_lineOfSightPlayer1Dir1 == DIR_DOWN))))
			{
				startShooting();
			}// end if

		}// end public override function whileOnLineOfSightPlayer1():void		
		
		public override function whileOnLineOfSightPlayer2():void
		{
			
			if ((!m_shooting && m_weaponHolder.canFire()) &&
			   (((m_vel.x < 0) && (m_lineOfSightPlayer2Dir2 == DIR_LEFT)) ||
			   ((m_vel.x > 0) && (m_lineOfSightPlayer2Dir1 == DIR_RIGHT)) ||
			   ((m_vel.y < 0) && (m_lineOfSightPlayer2Dir2 == DIR_UP))    ||
			   ((m_vel.y > 0) && (m_lineOfSightPlayer2Dir1 == DIR_DOWN))))
			{
				startShooting();
			}// end if

		}// end public override function whileOnLineOfSightPlayer2():void					
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnPink;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedPink;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityShooter
	
}// end package