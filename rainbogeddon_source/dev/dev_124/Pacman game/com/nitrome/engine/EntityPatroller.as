package com.nitrome.engine
{
	
	
	public class EntityPatroller extends EntityEnemy 
	{
			
		public function EntityPatroller(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			//m_speed = 1.5;
			m_hitPoints = 45;
			m_maxHitPoints = m_hitPoints;
			m_glow.m_color = 0xff0045ff;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_followsTarget = false;					
			
		}// end public function EntityPatroller(tx_:uint, ty_:uint)
					
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnDarkBlue;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedDarkBlue;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityPatroller
	
}// end package