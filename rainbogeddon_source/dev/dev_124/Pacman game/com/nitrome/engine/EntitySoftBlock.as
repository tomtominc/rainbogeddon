package com.nitrome.engine
{
	
	import flash.geom.*;
	
	public class EntitySoftBlock extends EntityBlock
	{
			
		public static const STATE_NORMAL:uint    = 0;	
		public static const STATE_RECOILING:uint = 1;
		
		public static const HIT_POINTS:int = 30;	
		public static const RECOIL_DURATION:int = 100;	
		
		private var m_recoilVel:Vector3D;
		private var m_recoilStartTime:int;
		private var m_recoilDuration:int;
		public  var m_canFlash:Boolean;
		public  var m_firstTime:Boolean;
		public  var m_currContour:uint;
		private var m_appearing:Boolean;
		
		public function EntitySoftBlock(tx_:uint, ty_:uint, appear_:Boolean = false)
		{
			super(tx_, ty_);
			m_hitPoints = HIT_POINTS;
			m_maxHitPoints = HIT_POINTS;
			m_state = STATE_NORMAL;
			m_recoilVel = new Vector3D();
			m_canFlash = true;
			m_firstTime = true;
			m_appearing = false;
			
			if (!appear_)
			{
				var contourID:uint = Game.g.m_tileOutliner.getContourID(m_tx, m_ty);
				_container.gotoAndStop("t_" + contourID);
				m_currContour = contourID;
			}
			else
			{
				
				_container.gotoAndPlay("appear");
				m_appearing = true;
				Game.g.m_tileMap.onMapChanged();
				Game.g.m_drawTileMap.onMapChanged();
				Game.g.m_transformingRainbowCoin = true;
				Game.g.m_rainbowCoinTransMap[m_ty*Game.g.mapWidth + m_tx] = 1;							
				
			}// end else
			
		}// end public function EntitySoftBlock(tx_:uint, ty_:uint, appear_:Boolean = false)
		
		public function OnAppearDone():void
		{
		
			m_appearing = false;
			var contourID:uint = Game.g.m_tileOutliner.getContourID(m_tx, m_ty);
			_container.gotoAndStop("t_" + contourID);
			m_currContour = contourID;		
			Game.g.m_tileMap.onMapChanged();
			Game.g.m_drawTileMap.onMapChanged();
			Game.g.m_transformingRainbowCoin = false;
			Game.g.m_rainbowCoinTransMap[m_ty*Game.g.mapWidth + m_tx] = 0;					
			
		}// end public function OnAppearDone():void			
		
		public override function doPhysics():void
		{
			
			if (m_appearing)
			{
				return;
			}// end if
			
			
			
			if (Game.g.m_tileMap.m_changed || m_firstTime)
			{
				m_firstTime = false;
				var contourID:uint = Game.g.m_tileOutliner.getContourID(m_tx, m_ty);
				_container.gotoAndStop("t_" + contourID);
				m_currContour = contourID;
			}// end if
			
			switch(m_state)
			{
				
				case STATE_RECOILING:
				{
					
					if (Game.g.timer - m_recoilStartTime > m_recoilDuration)
					{
					
						_container.x -= m_recoilVel.x;
						_container.y -= m_recoilVel.y;					
						
						m_state = STATE_NORMAL;
					
					}// end if
					
					break;
				}// end case
				
				
			}// end switch
			
			updateRecovery();
			
		}// end override function doPhysics():void		
		
		public function recoil(vel_:Vector3D, duration_:int):void
		{
		
			if (m_state != STATE_RECOILING)
			{
			
				m_state = STATE_RECOILING;
				
				m_recoilVel.x = 0;
				m_recoilVel.y = 0;			
				
				if (vel_.x > 0)
				{
					m_recoilVel.x = 1;
				}
				else if (vel_.x < 0)
				{
					m_recoilVel.x = -1;
				}
				else if (vel_.y > 0)
				{
					m_recoilVel.y = 1;
				}
				else if (vel_.y < 0)
				{
					m_recoilVel.y = -1;
				}// end else if
				
				_container.x += m_recoilVel.x;
				_container.y += m_recoilVel.y;
				
				m_recoilStartTime = Game.g.timer;
				m_recoilDuration = duration_;
				
			}// end if
			
		}// end public function recoil(vel_:Vector3D):void				
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath();
            if (attacker_)
			{
				
				if (attacker_ is EntityPlayer1)
				{
					Game.g.m_gameObjectList.push(new Player1SoftBlockDestroyed(m_tx, m_ty));
				}
				else if (attacker_ is EntityPlayer2)
				{
					Game.g.m_gameObjectList.push(new Player2SoftBlockDestroyed(m_tx, m_ty));
				}
				else if (attacker_ is EntityShooter)
				{
					Game.g.m_gameObjectList.push(new ShooterSoftBlockDestroyed(m_tx, m_ty));
				}// end if
				
			}// end if
			
			Game.g.m_gameObjectList.push(new EntityNormalCoin(m_tx, m_ty));
			
			m_isDead = true;
			m_terminated = true;
			Game.g.m_tileMap.onMapChanged();
			Game.g.m_drawTileMap.onMapChanged();
			Game.g.m_rainbowCoinTransMap[m_ty*Game.g.mapWidth + m_tx] = 0;
			
		}// end public override function onDeath(attacker_:Entity = null):void		
		
		public function switchToTempCoin():void
		{
		
			Game.g.m_gameObjectList.push(new EntityRainbowCoin(m_tx, m_ty));
			
			m_isDead = true;
			m_terminated = true;
			Game.g.m_tileMap.onMapChanged();
			Game.g.m_drawTileMap.onMapChanged();
			super.onDeath();
			
		}// end public function switchToTempCoin():void			
		
		public override function onDamage(vel_:Vector3D = null):void
		{
				
			if (vel_)
			{
				recoil(vel_, RECOIL_DURATION);
			}
			else
			{
				
				var result:uint = Math.floor(Math.random() * 4);
				var randomRecoilVel:Vector3D = new Vector3D();
				
				if(result == 0)
				{
					randomRecoilVel.x = -1;
				}
				else if(result == 1)
				{
					randomRecoilVel.x = 1;
				}
				else if (result == 2)
				{
					randomRecoilVel.y = -1;
				}
				else 
				{
					randomRecoilVel.y = 1;
				}// end else
				
				recoil(randomRecoilVel, RECOIL_DURATION/4);
				
			}// end else
			
			super.onDamage(vel_);
			
		}// end public override function onDamage(vel_:Vector3D = null):void				
		
		public override function draw():void
		{
			
			if (m_currContour != 255)
			{
				super.draw();
			}
			else
			{
				
				Game.g.m_tinyBlocksBitmap.setPixel32(m_tx, m_ty, 0xff000000);
						
			}// end else
			
			
		}// end public override function draw():void
		
	}// end public class EntitySoftBlock
	
}// end package