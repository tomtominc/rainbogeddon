package com.nitrome.engine
{
	
	public class EnemyAlertEffect extends EffectCentered
	{
				
		public function EnemyAlertEffect(parent_:EntityEnemy)
		{
		
			super(0, 0);
			x = parent_.x;
			y = parent_.y;
			
		}// end public function EnemyAlertEffect(tx_:uint, ty_:uint)
		
	}// end public class EnemyAlertEffect
	
}// end package