package com.nitrome.gfx {
	/**
	 * Draws a filled pixelated circle to a Vector of pixels
	 *
	 * code is a trimmed version of the code found here:
	 * http://actionsnippet.com/?p=1658
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	
	public function bresenhamCircle(x:int, y:int, radius:int, pixels:Vector.<uint>, width:int, height:int, col:uint = 0x00000000):void{
		
		var xoff:int =0;
		var yoff:int = radius;
		var balance:int = -radius;
		var i:int, p0:int, p1:int, w0:int, w1:int, index:int;
		var size:int = width * height;
		
		while(xoff <= yoff){
			p0 = x - xoff;
			p1 = x - yoff;
			w0 = xoff + xoff;
			w1 = yoff + yoff;
			
			index = p0 + (y + yoff) * width;
			for (i = 0; i < w0; i++){
				index++;
				if(index > -1 && index < size) pixels[index] = col;
			}
			
			index = p0 + (y - yoff) * width;
			for (i = 0; i < w0; i++){
				index++;
				if(index > -1 && index < size) pixels[index] = col;
			}
			
			index = p1 + (y + xoff) * width;
			for (i = 0; i < w1; i++){
				index++;
				if(index > -1 && index < size) pixels[index] = col;
			}
			
			index = p1 + (y - xoff) * width;
			for (i = 0; i < w1; i++){
				index++;
				if(index > -1 && index < size) pixels[index] = col;
			}

			balance += xoff + xoff;
			xoff++;
			if (balance >= 0){
				yoff--;
				balance -= yoff + yoff;
			}
			
		}
		
	}

}