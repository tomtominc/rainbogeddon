﻿fl.outputPanel.clear();

var _doc = fl.getDocumentDOM();
var _lib = _doc.library;



var fontId = prompt("Enter the font id (eg:MyFont): ");

if(fontId  != null && fontId .length > 0) {

	var bitmapList = new Array;
	var clipList = new Array;
	var assetsClipName = fontId+"_Assets";
	
	bitmapList = _lib.getSelectedItems();
	
	_lib.addNewItem("movie clip", assetsClipName);
	newItem_it = _lib.getSelectedItems()[0];
	newItem_it.linkageExportForAS = true;
	newItem_it.linkageExportInFirstFrame = false;
	newItem_it.linkageBaseClass = "flash.display.Sprite"
	
	var x = 0;

	for(var i in bitmapList) {
		
		var charName = bitmapList[i].name;
		
		// format the charName
		if(endsWith(charName, ".gif")) charName = charName.substring(0, charName.length-4);
		else if(endsWith(charName, ".png")) charName = charName.substring(0, charName.length-4);
		if(charName.lastIndexOf("/") != -1)
			charName = charName.substring(charName.lastIndexOf("/")+1);
		// mixed-case fonts only
		/*if(charName != charName.toUpperCase()){
			charName += "_LOWER";
		}*/
		charName = charName.toUpperCase();
		if(charName <= 0 || charName >= 0) charName = "NUMBER_"+charName;
		charName = removeWhiteSpace(charName);
		var clipName = "char_"+charName;
		
		_lib.addNewItem("movie clip", clipName);
		newItem_it = _lib.getSelectedItems()[0];
		
		clipList.push({clip:newItem_it, name:charName});
		
		_lib.editItem(clipName);
		_doc.addItem({x:0, y:0}, bitmapList[i]);
		_doc.selectAll();
		
		var inst = _doc.selection[0];
		_doc.moveSelectionBy({x:-inst.left, y:-inst.top});
	
		_lib.editItem(assetsClipName);
		_doc.addItem({x:0, y:0}, newItem_it);
		
		var inst = _doc.selection[0];
		
		inst.x = x;
		inst.y = 0;
		inst.name = charName;
		x += inst.width;
	}
	
}

function startsWith(searchString, startString) {
	return (searchString.substring(0, startString.length) == startString);
}
function endsWith(searchString, endString) {
	return (searchString.substring(searchString.length - endString.length) == endString);
}
function removeWhiteSpace(str){
	var newStr = "";
	for(var i = 0; i < str.length; i++){
		if(str.charAt(i) != " "){
			newStr += str.charAt(i);
		}
	}
	return newStr;
}
