﻿package com.nitrome.util {
	
	/**
	* A wrapper for Numbers to protect them from tools like CheatEngine
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class HiddenNumber {
		
		private var Value:Number;
		private var r:Number;
		function HiddenNumber(startValue:Number = 0){
			r = (int)(Math.random()*2000000)-1000000;
			Value = r + startValue;
		}
		// Getter setters for value
		public function set value(v:Number):void{
			r = (int)(Math.random()*2000000)-1000000;
			Value = r + v;
		}
		public function get value():Number{
			return Value-r;
		}
		
	}
	
}