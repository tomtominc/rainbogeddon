package com.nitrome.gfx {
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class PrerenderedFrame {
		
		public var data:BitmapData;
		public var dataRect:Rectangle;
		public var offsetX:Number;
		public var offsetY:Number;
		
		public function setupFrom(d:DisplayObject):void {
			var dRect:Rectangle = d.getBounds(d);
			dRect.left = Math.floor(dRect.left);
			dRect.right = Math.ceil(dRect.right);
			dRect.top = Math.floor(dRect.top);
			dRect.bottom = Math.ceil(dRect.bottom);
			
			if(dRect.width < 1 || dRect.height < 1) return;
			
			offsetX = dRect.left;
			offsetY = dRect.top;
			
			data = new BitmapData(dRect.width, dRect.height, true, 0x0);
			data.draw(d, new Matrix(1, 0, 0, 1, -offsetX, -offsetY));
			dataRect = data.rect;
		}
		
		public function equals(frame:PrerenderedFrame):Boolean {
			if(!data || !frame.data) return false;
			
			if(!dataRect.equals(frame.dataRect)) return false;
			if(offsetX != frame.offsetX) return false;
			if(offsetY != frame.offsetY) return false;
			if(data.compare(frame.data) != 0) return false;
			return true;
		}
		
		public function clone():PrerenderedFrame {
			var result:PrerenderedFrame = new PrerenderedFrame;
			result.data = data.clone();
			result.dataRect = result.data.rect;
			result.offsetX = offsetX;
			result.offsetY = offsetY;
			return result;
		}
		public function flipX():void {
			var newData:BitmapData = new BitmapData(data.width, data.height, true, 0x0);
			newData.draw(data, new Matrix(-1, 0, 0, 1, data.width, 0));
			data = newData;
			offsetX = -offsetX - data.width;
		}
		
	}

}