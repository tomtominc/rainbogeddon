package com.nitrome.engine {
	/**
	 * Wrapper class for embedding levels
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class Levels{
		
		[Embed(source = "/levels/level1.xml", mimeType = "application/octet-stream")] public static var Level1:Class;
		[Embed(source = "/levels/level2.xml", mimeType = "application/octet-stream")] public static var Level2:Class;
		[Embed(source = "/levels/level3.xml", mimeType = "application/octet-stream")] public static var Level3:Class;
		[Embed(source = "/levels/level4.xml", mimeType = "application/octet-stream")] public static var Level4:Class;
		[Embed(source = "/levels/level5.xml", mimeType = "application/octet-stream")] public static var Level5:Class;
		[Embed(source = "/levels/level6.xml", mimeType = "application/octet-stream")] public static var Level6:Class;
		[Embed(source = "/levels/level7.xml", mimeType = "application/octet-stream")] public static var Level7:Class;
		[Embed(source = "/levels/level8.xml", mimeType = "application/octet-stream")] public static var Level8:Class;
		[Embed(source = "/levels/level9.xml", mimeType = "application/octet-stream")] public static var Level9:Class;
		[Embed(source = "/levels/level10.xml", mimeType = "application/octet-stream")] public static var Level10:Class;
		[Embed(source = "/levels/level11.xml", mimeType = "application/octet-stream")] public static var Level11:Class;
		[Embed(source = "/levels/level12.xml", mimeType = "application/octet-stream")] public static var Level12:Class;
		[Embed(source = "/levels/level13.xml", mimeType = "application/octet-stream")] public static var Level13:Class;
		[Embed(source = "/levels/level14.xml", mimeType = "application/octet-stream")] public static var Level14:Class;
		[Embed(source = "/levels/level15.xml", mimeType = "application/octet-stream")] public static var Level15:Class;
		[Embed(source = "/levels/level16.xml", mimeType = "application/octet-stream")] public static var Level16:Class;
		[Embed(source = "/levels/level17.xml", mimeType = "application/octet-stream")] public static var Level17:Class;
		[Embed(source = "/levels/level18.xml", mimeType = "application/octet-stream")] public static var Level18:Class;
		[Embed(source = "/levels/level19.xml", mimeType = "application/octet-stream")] public static var Level19:Class;
		[Embed(source = "/levels/level20.xml", mimeType = "application/octet-stream")] public static var Level20:Class;
		[Embed(source = "/levels/level21.xml", mimeType = "application/octet-stream")] public static var Level21:Class;
		[Embed(source = "/levels/level22.xml", mimeType = "application/octet-stream")] public static var Level22:Class;
		[Embed(source = "/levels/level23.xml", mimeType = "application/octet-stream")] public static var Level23:Class;
		[Embed(source = "/levels/level24.xml", mimeType = "application/octet-stream")] public static var Level24:Class;
		[Embed(source = "/levels/level25.xml", mimeType = "application/octet-stream")] public static var Level25:Class;
		[Embed(source = "/levels/level26.xml", mimeType = "application/octet-stream")] public static var Level26:Class;
		[Embed(source = "/levels/level27.xml", mimeType = "application/octet-stream")] public static var Level27:Class;
		[Embed(source = "/levels/level28.xml", mimeType = "application/octet-stream")] public static var Level28:Class;
		[Embed(source = "/levels/level29.xml", mimeType = "application/octet-stream")] public static var Level29:Class;
		[Embed(source = "/levels/level30.xml", mimeType = "application/octet-stream")] public static var Level30:Class;
		[Embed(source = "/levels/level31.xml", mimeType = "application/octet-stream")] public static var Level31:Class;
		[Embed(source = "/levels/level32.xml", mimeType = "application/octet-stream")] public static var Level32:Class;
		[Embed(source = "/levels/level33.xml", mimeType = "application/octet-stream")] public static var Level33:Class;
		[Embed(source = "/levels/level34.xml", mimeType = "application/octet-stream")] public static var Level34:Class;
		[Embed(source = "/levels/level35.xml", mimeType = "application/octet-stream")] public static var Level35:Class;
		[Embed(source = "/levels/level36.xml", mimeType = "application/octet-stream")] public static var Level36:Class;
		[Embed(source = "/levels/level37.xml", mimeType = "application/octet-stream")] public static var Level37:Class;
		[Embed(source = "/levels/level38.xml", mimeType = "application/octet-stream")] public static var Level38:Class;
		[Embed(source = "/levels/level39.xml", mimeType = "application/octet-stream")] public static var Level39:Class;
		[Embed(source = "/levels/level40.xml", mimeType = "application/octet-stream")] public static var Level40:Class;
		
		public static const LEVEL_CLASSES:Array = [null, Level1, Level2, Level3, Level4, Level5, Level6, Level7, Level8, Level9, Level10, Level11, Level12, Level13, Level14, Level15, Level16, Level17, Level18, Level19, Level20, Level21, Level22, Level23, Level24, Level25, Level26, Level27, Level28, Level29, Level30, Level31, Level32, Level33, Level34, Level35, Level36, Level37, Level38, Level39, Level40];
		
		public static function getLevel(n:int):XML{
			var xml:XML = XML(new LEVEL_CLASSES[n]());
			return xml;
		}
		
	}

}