package com.nitrome.engine
{
	
	
	public class EntityHardExplodey extends EntityExplodey 
	{
			
		public function EntityHardExplodey(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_glow.m_color = 0xffff3503;
		}// end public function EntityHardExplodey(tx_:uint, ty_:uint)
					
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnRed;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedRed;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityHardExplodey
	
}// end package