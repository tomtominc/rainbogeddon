package com.nitrome.engine
{

	
	public class EntityTeleporter extends EntityCentered
	{

		
		public static const INFINITE_SPIN_TIMES:uint = uint.MAX_VALUE;

		//public var m_parent:Entity;
		public var m_canDisable:Boolean;
		public var m_twin:EntityTeleporter;
		protected var m_powerLevel:int;
		public var m_spinTimes:uint;
		public var m_powerToSpinTimesTable:Object;
		public var m_firstCollisionP1CheckPerformed:Boolean;
		public var m_firstCollisionP2CheckPerformed:Boolean;
		public var m_waitingPlayer1UnCollide:Boolean;
		public var m_waitingPlayer2UnCollide:Boolean;			
		
		public function EntityTeleporter(tx_:uint, ty_:uint, powerLevel_:int)
		{
			super(tx_, ty_);
			m_collisionRect.x = -12.5;
			m_collisionRect.y = -12.5;
			m_collisionRect.width = 25;
			m_collisionRect.height = 25;
			m_canDie = false;
			//m_parent = null;
			m_canDisable = true;
			m_powerLevel = powerLevel_;
			m_powerToSpinTimesTable = new Object();
			m_powerToSpinTimesTable[1] = 2;
			m_powerToSpinTimesTable[2] = 1;
			m_powerToSpinTimesTable[3] = 0;
			m_powerToSpinTimesTable[-1] = INFINITE_SPIN_TIMES;
			m_spinTimes = m_powerToSpinTimesTable[m_powerLevel];
			m_firstCollisionP1CheckPerformed = false;
			m_firstCollisionP2CheckPerformed = false;
			m_waitingPlayer1UnCollide = false;
			m_waitingPlayer2UnCollide = false;						
		}// end public function EntityTeleporter(tx_:uint, ty_:uint)
		
			
		public function set powerLevel(powerLevel_:int):void
		{
			m_powerLevel = powerLevel_;
			m_spinTimes = m_powerToSpinTimesTable[m_powerLevel];
		}// end public function set powerLevel(powerLevel_:uint):void		
		
		public function get powerLevel():int
		{
			return m_powerLevel;
		}// end public function get powerLevel():Class			
		
		public function get disappearEffect():Class
		{
			return null;
		}// end public function get disappearEffect():Class	
		
		public function terminate():void
		{
			m_terminated = true;
			if (m_twin)
			{
				m_twin.m_twin = null;
			}// end if
			Game.g.m_gameObjectList.push(new disappearEffect(m_tx, m_ty));
			m_spatialHashMap.unregisterObject(this);
		}// end public function terminate():void		
		
	}// end public class EntityTeleporter
	
}// end package