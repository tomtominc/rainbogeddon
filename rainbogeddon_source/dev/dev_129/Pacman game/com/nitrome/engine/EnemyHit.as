package com.nitrome.engine
{
	
	public class EnemyHit extends EffectCentered
	{
				
		public function EnemyHit(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);				
			
		}// end public function EnemyHit(tx_:uint, ty_:uint)
		
	}// end public class EnemyHit
	
}// end package