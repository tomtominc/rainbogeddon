package com.nitrome.engine
{
		
	import com.nitrome.engine.utils.TileOutliner;
	import com.nitrome.engine.pathfinding.PathMap;
	import flash.display.MovieClip;
	
	public class EntityBubbleParticle extends Entity
	{
				
		public static var m_tileOutliner:TileOutliner;
		public static var m_animGuideParticle:MovieClip;
		
		public function EntityBubbleParticle(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);					
			m_drawBitmap = Game.g.m_entityBitmap;
			m_canDie = false;
			m_collisionRect.x = 0;
			m_collisionRect.y = 0;
			m_collisionRect.width = 25;
			m_collisionRect.height = 25;			
			if (!m_tileOutliner)
			{
				m_tileOutliner = new TileOutliner();
				m_tileOutliner.m_tileMap = Game.g.m_safetyParticleTileMap.m_tileMap;
				m_tileOutliner.m_mapWidth  = Game.g.mapWidth;
				m_tileOutliner.m_mapHeight = Game.g.mapHeight;
				m_tileOutliner.m_emptyTileValue = PathMap.TILE_SENTINEL;				
			}// end if
			
			if (!m_animGuideParticle)
			{
				m_animGuideParticle = new bubbleContainerType();
			}// end if
			
		}// end public function EntityBubbleParticle(tx_:uint, ty_:uint)
		
		public static function free():void
		{
			m_tileOutliner = null;
			m_animGuideParticle = null;
		}// end public static function free():void
		
		public function terminate():void
		{
			
			m_terminated = true;
			m_spatialHashMap.unregisterObject(this);
			
		}// end public function terminate():void
		
		public function get bubbleContainerType():Class
		{
			return null;
		}// end public function get bubbleContainerType():Class
		
		public function get container():MovieClip
		{
			return null;
		}// end public function get container():MovieClip		
		
		public override function doPhysics():void
		{
			

			var contourID:uint = m_tileOutliner.getContourID(m_tx, m_ty);
			
			gotoAndStop("t_" + contourID); 
			var finalFrame:uint = currentFrame + (m_animGuideParticle.currentFrame-1); 
			gotoAndStop(finalFrame); 			
			
			if (contourID == 255)
			{
				m_draw = false;
			}
			else
			{
				m_draw = true;
			}// end else
			
		}// end public override function doPhysics():void				
		
	}// end public class EntityBubbleParticle
	
}// end package