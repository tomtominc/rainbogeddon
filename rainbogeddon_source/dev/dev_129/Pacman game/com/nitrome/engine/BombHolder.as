package com.nitrome.engine
{

	import com.nitrome.sound.SoundManager;
	
	public class BombHolder extends WeaponHolder
	{
		

		private var m_bomb:EntityBomb
		
		public function BombHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_bomb = null;
		}// end public function BombHolder()
				
	
		public override function tryFire():Boolean
		{
			
			m_parent.m_fire = false;
			
			if (!m_bomb || m_bomb.m_exploded)
			{
				
				if (m_parent is EntityPlayer2)
				{
					m_bomb = new EntityPlayer2Bomb(m_parent.m_tx, m_parent.m_ty, m_currUpgradeStage, m_parent as EntityPlayer2);
				}
				else if(m_parent is EntityPlayer1)
				{
					m_bomb = new EntityPlayer1Bomb(m_parent.m_tx, m_parent.m_ty, m_currUpgradeStage, m_parent as EntityPlayer1);
				}// end else if						
				
				Game.g.m_gameObjectList.push(m_bomb);
				SoundManager.playSound("DropTail");
				
				return true;
				
			}// end if	
			
			return false;
			
		}// end public override function tryFire():Boolean			
		
		public override function upgrade():Boolean
		{
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				return true;
			}
			else
			{
				return false;
			}// end else
		}// end public override function upgrade():Boolean			
		
	}// end public class BombHolder
	
}// end package