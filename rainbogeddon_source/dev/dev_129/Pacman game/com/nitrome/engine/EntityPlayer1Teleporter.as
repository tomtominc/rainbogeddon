package com.nitrome.engine
{

	
	public class EntityPlayer1Teleporter extends EntityPlayerTeleporter
	{

		public function EntityPlayer1Teleporter(tx_:uint, ty_:uint, powerLevel_:uint)
		{
			super(tx_, ty_, powerLevel_);
		}// end public function EntityPlayer1Teleporter(tx_:uint, ty_:uint, powerLevel_:uint)
			
		public override function get disappearEffect():Class
		{
			return Player1TeleporterDisappearEffect;
		}// end public override function get disappearEffect():Class		
		
	}// end public class EntityPlayer1Teleporter
	
}// end package