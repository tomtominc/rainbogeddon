package com.nitrome.engine
{
	import flash.geom.Vector3D;
	import com.nitrome.engine.pathfinding.PathMap;
	import com.nitrome.sound.SoundManager;
	
	public class EntityJellyBaby extends EntityEnemy 
	{
			
		public static const DAMAGE:int = 1;
		public static const TAIL_REJOIN_DIST:Number = 25;
		public static const TAIL_SNAP_TO_SLOT_DIST:Number = 2.0;
		public static const TAIL_REJOIN_SPEED_FACTOR:Number = 2.0; // TAIL_REJOIN_SPEED_FACTOR:Number = 1.5;
		public static const FOLLOW_MOTHER_RANGE:uint = 6;
		public static const FOLLOW_PLAYER_RANGE:uint = 3;
		public static const STOP_FOLLOWING_PLAYER_RANGE:uint = 10;
		public static const	BREAK_FREE_WAIT_DURATION:int = 1500;
				
		public var m_mother:EntityJellyMama;
		public var m_slot:EntityJellyBabyTailSlot;
		public var m_followingMama:Boolean;
		public var m_rejoining:Boolean;
		//public var m_movingUpTail:Boolean;
		//public var m_motherPathMap:PathMap;
		public var m_slotPathMap:PathMap;
		public var m_playerNearAlerted:Boolean;
		private var m_isStationaryWhileFree:Boolean;
		private var m_stationaryWhileFreeStartTime:int;
		private var m_playerNearAlertedStartTime:int;
		
		public function EntityJellyBaby(mother_:EntityJellyMama)
		{
			super(mother_.m_tx, mother_.m_ty);	
			m_drawOrder = 1;
			m_respawns = false;
			m_collisionRect.x = -5;
			m_collisionRect.y = -5;
			m_collisionRect.width = 10;
			m_collisionRect.height = 10;				
			m_mother = mother_;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;	
			m_hasDecreasingTimeout = false;
			m_followingMama = true;
			m_isStationaryWhileFree = false;
			m_hasOnTargetRangeTrigger = true;
			m_targetTriggerRange = FOLLOW_PLAYER_RANGE;
			m_rejoining = false;
			//m_movingUpTail = false;
			m_slotPathMap = new PathMap(Game.g.mapWidth, Game.g.mapHeight);
			//m_motherPathMap = new PathMap(Game.g.mapWidth, Game.g.mapHeight);
			m_spatialHashMap.updateObject(this);
			m_playerNearAlerted = false;
			m_followsTarget = false;
			m_hitPoints = 20;
		}// end public function EntityJellyBaby(mother_:EntityJellyMama)
							
		public override function doPhysics():void
		{
			
			if (m_isDead || m_playerNearAlerted)
			{
				
				if (m_playerNearAlerted && (Game.g.timer - m_playerNearAlertedStartTime > BREAK_FREE_WAIT_DURATION))
				{
					m_playerNearAlerted = false;
				}// end if
				
				return;
			}// end if			
			
			var slotPos:Vector3D = new Vector3D();
			var oppositeSlotPos:Vector3D = new Vector3D();
			var thisPos:Vector3D = new Vector3D(x, y);
			var thisToSlotVec:Vector3D = new Vector3D();
			var thisToOppositeSlotVec:Vector3D = new Vector3D();
			
			if (!m_followingMama)
			{
				
				m_speed = m_mother.m_speed;
				
				var xBefore:Number = x;
				var yBefore:Number = y;
				
				super.doPhysics();
				
			
				if (!m_isStationaryWhileFree)
				{
				
					if ((xBefore == x) && (yBefore == y))
					{
						m_isStationaryWhileFree = true;
						m_stationaryWhileFreeStartTime = Game.g.timer;
					}// end if
					
				}
				else
				{
					
					if ((xBefore != x) || (yBefore != y))
					{
						m_isStationaryWhileFree = false;
					}
					else if((Game.g.timer - m_stationaryWhileFreeStartTime) > BREAK_FREE_WAIT_DURATION)
					{
						m_isStationaryWhileFree = false;
						m_playerNearAlerted = false;
						m_followsTarget = false;	
						m_rejoining = false;
						m_targetInRange = false;
						m_smartFollowing = false;
						m_isAlerted = false;
					}// end else if
					
				}// end else
				
				//trace("patrolling");
				
				/*if (m_state == STATE_PATROLLING_CORRIDORS)
				{
					m_followsTarget = false;
				}// end if*/
				
				if (!m_followsTarget && !m_smartFollowing)
				{
					
					var gameObjectList:Vector.<GameObject> = Game.g.m_gameObjectList;
					var mama:EntityJellyMama;
					
					// check all the baby slots on all the mothers in the level
					for(var currObject:uint = 0; currObject < gameObjectList.length; currObject++)
					{
						
						if (gameObjectList[currObject] is EntityJellyMama)
						{
							
							mama = gameObjectList[currObject] as EntityJellyMama;
							
							if (!mama.m_isDead)
							{
								
								// look for a free slot
								for (var currSlot:uint = 0; currSlot < mama.m_babySlotList.length; currSlot++)
								{
									
									if (!mama.m_babySlotList[currSlot].m_inUse)
									{
									
										slotPos.x = mama.m_babySlotList[currSlot].x;
										slotPos.y = mama.m_babySlotList[currSlot].y;
										thisToSlotVec = slotPos.subtract(thisPos);
										
										if (thisToSlotVec.length <= TAIL_REJOIN_DIST)
										{
										
											if (mama != m_mother)
											{
												m_mother.removeBaby(this);
												mama.m_babyList.push(this);
												m_mother = mama;
											}// end if
											
											//trace("attaching to mother");
											attachToSlot(mama.m_babySlotList[currSlot]);
										
											break;
											
										}// end if
	
									}// end if
									
								}// end for
							
							}// end if
							
						}// end if
						
					}// end for
					
					
				}// end if
				
			}
			else
			{
			
				tickChildren();
				
				if (!m_rejoining)
				{
				
					//trace("following tail");
					
					updateReactionToHit();
					
					updateMap();
					
					updateTarget();
					
					updateTargetRange();
					
					updateDamage();
					
					// copy over position information from slot
					

					if (m_followingMama)
					{
					
						x = m_slot.x;
						y = m_slot.y;
						
						m_vel.x = m_slot.m_vel.x;
						m_vel.y = m_slot.m_vel.y;
						
						m_prevVel.x = m_vel.x;
						m_prevVel.y = m_vel.y;			
						
						m_cTX = m_slot.m_cTX;
						m_cTY = m_slot.m_cTY;	

					}// end if
					
					updateTilePos();
					
					// check the next slot to see if it's unoccupied
					// if so, start moving towards it
				
					if (m_slot && (m_slot.m_index > 0))
					{
						if (!m_mother.m_babySlotList[m_slot.m_index - 1].m_inUse)
						{
							//trace("moving up in the tail");
							m_isStationaryWhileFree = false;
							m_slot.m_inUse = false;
							m_slot = m_mother.m_babySlotList[m_slot.m_index - 1];
							m_slot.m_inUse = true;
							m_rejoining = true;
							/*if (!m_followsTarget)
							{
								trace("gotcha");
							}// end if*/
							m_followsTarget = true;
							//m_movingUpTail = true;
							m_speed = m_mother.m_speed * TAIL_REJOIN_SPEED_FACTOR;
							m_hasOnTargetRangeTrigger = false;
							m_checkDirMaxTimeout = 50;
							m_checkDirTimeout = m_checkDirMaxTimeout;							
						}// end if
					}// end if
					
				}
				else
				{
					
					
					//trace("rejoining");
					
					m_slotPathMap.m_tileMap = new Vector.<uint>;
								
					if (Game.g.m_safetyTileMap.m_active)
					{
						m_slotPathMap.m_tileMap = m_slotPathMap.m_tileMap.concat(Game.g.m_safetyTileMap.m_tileMap);
					}
					else
					{
						m_slotPathMap.m_tileMap = m_slotPathMap.m_tileMap.concat(Game.g.m_enemyTileMap.m_tileMap);
					}// end else
					
					
					m_slotPathMap.floodFill(m_slot.m_cTX, m_slot.m_cTY);					
					
	
					// cheat a little by moving in a straight path towards the slot
					
					slotPos.x = m_slot.x;
					slotPos.y = m_slot.y;
					thisToSlotVec = slotPos.subtract(thisPos);
									
					var rejoined:Boolean = false;
					
					if (thisToSlotVec.length <= TAIL_SNAP_TO_SLOT_DIST)
					{
						//trace("snapped to slot");
						m_isStationaryWhileFree = false;
						m_followsTarget = false;
						rejoined = true;
						m_rejoining = false;
						m_speed = m_mother.m_speed;	
						m_hasOnTargetRangeTrigger = true;
						m_checkDirMaxTimeout = 250;
						m_checkDirTimeout = m_checkDirMaxTimeout;		
						m_targetTriggerRange = FOLLOW_PLAYER_RANGE;
						//m_movingUpTail = false;
					}// end if
					
					xBefore = x;
					yBefore = y;

					super.doPhysics();

					
					if (!m_isStationaryWhileFree)
					{
					
						if ((xBefore == x) && (yBefore == y))
						{
							m_isStationaryWhileFree = true;
							m_stationaryWhileFreeStartTime = Game.g.timer;
						}// end if
						
					}
					else
					{
						
						if ((xBefore != x) || (yBefore != y))
						{
							m_isStationaryWhileFree = false;
						}
						else if((Game.g.timer - m_stationaryWhileFreeStartTime) > BREAK_FREE_WAIT_DURATION)
						{
							m_isStationaryWhileFree = false;
							m_playerNearAlerted = false;
							m_followsTarget = false;	
							m_rejoining = false;
							m_targetInRange = false;
							m_smartFollowing = false;
							m_isAlerted = false;
						}// end else if
						
					}// end else					
					
					
					if (!rejoined && (m_distFromTarget > m_targetTriggerRange))
					{
						//trace("slot too far");
						m_isStationaryWhileFree = false;
						m_followingMama = false;
						m_rejoining = false;
						m_speed = m_mother.m_speed;	
						m_hasOnTargetRangeTrigger = true;
						m_checkDirMaxTimeout = 250;
						m_checkDirTimeout = m_checkDirMaxTimeout;						
						m_slot.m_inUse = false;
						m_slot = null;			
						m_targetTriggerRange = FOLLOW_PLAYER_RANGE;
					}// end if
				
				}// end else
				
				if (m_mother.m_isDead)
				{
					m_followingMama = false;
					m_slot.m_inUse = false;
					m_slot = null;		
					m_targetTriggerRange = FOLLOW_PLAYER_RANGE;
				}// end if
				

				updateAnim();

				updateAbsCollisionRect();
				m_spatialHashMap.updateObject(this);
				
			}// end else
			
			updateRecovery();
			
			m_drawOrder = y + m_bounds.y + m_bounds.height;// m_cTY;
					
		}// end public override function doPhysics():void					
		
		public override function gotoAndPlay(frame:Object, scene:String = null):void
		{
			
			/*if ((currentLabel == "alerted") && m_isAlerted && (frame != "alerted"))
			{
				trace("gotcha");
			}// end if*/			
			
			super.gotoAndPlay(frame, scene);
		
		}// end public override function gotoAndPlay(frame:Object, scene:String = null):void
		
		public override function updateAnim():void
		{	
			
			if (!m_isAlerted)
			{
				super.updateAnim();
			}// end if
			
		}// end public override function updateAnim():void
		
		public override function onTargetInRange(target_:Entity):void
		{
			
			//if (m_followingMama && !m_rejoining)
			//{
				//trace("onTargetInRange");
				gotoAndPlay("player_near_alerted");
				m_playerNearAlerted = true;
				m_playerNearAlertedStartTime = Game.g.timer;
				m_alertEffect = new EnemyAlertEffect(this);
				SoundManager.playSound("EnemyAlerted");
				Game.g.m_gameObjectList.push(m_alertEffect);	
			//}// end if

			
			m_speed = m_mother.m_speed;	
			m_followsTarget = true;
			m_followingMama = false;
			m_targetTriggerRange = STOP_FOLLOWING_PLAYER_RANGE;
			
			if (m_slot)
			{
				m_slot.m_inUse = false;
				m_slot = null;
			}// end if
			
			
		}// end public override function onTargetInRange(target_:Entity):void			
		
		public override function onTargetOutOfRange():void
		{
			
			//trace("target out of range");
			m_followsTarget = false;
			m_targetTriggerRange = FOLLOW_PLAYER_RANGE;
			
		}// end public override function onTargetOutOfRange():void			
	
		public function playerNearAlertDone():void
		{
			
			m_playerNearAlerted = false;
			m_forceUpdateAnim = true;
			
		}// end public function playerNearAlertDone():void			
		
		public override function OnTargetInaccessible():void
		{
		
			m_followsTarget = false;
			
		}// end public override function OnTargetInaccessible():void				
			
		
		public override function updateTargetRange():void
		{
			
			if (m_rejoining)
			{
				return;
			}// end if
			
			if (m_distFromTarget <= m_targetTriggerRange)
			{
				
				if (!m_targetInRange && !m_isAlerted)
				{
					m_currInRangeTarget = m_chosenTarget;
					onTargetInRange(m_chosenTarget);	
					whileTargetInRange(m_chosenTarget);
					m_targetInRange = true;
				}
				else
				{	
					whileTargetInRange(m_chosenTarget);
				}// end else
			
			}
			else if (m_targetInRange && (m_distFromTarget > m_targetTriggerRange))
			{
				m_currInRangeTarget = null;
				m_targetInRange = false;
				onTargetOutOfRange();
			}// end eelse if
			
		}// end public override function updateTargetRange():void		
		
		public function attachToSlot(slot_:EntityJellyBabyTailSlot):void
		{
			
			if ((x != slot_.x) || (y != slot_.y))
			{
				m_rejoining = true;
				/*if (!m_followsTarget)
				{
					trace("gotcha");
				}// end if*/				
				m_followsTarget = true;
				m_slotPathMap.m_tileMap = new Vector.<uint>;	
				
				if (Game.g.m_safetyTileMap.m_active)
				{
					m_slotPathMap.m_tileMap = m_slotPathMap.m_tileMap.concat(Game.g.m_safetyTileMap.m_tileMap);
				}
				else
				{
					m_slotPathMap.m_tileMap = m_slotPathMap.m_tileMap.concat(Game.g.m_tileMap.m_tileMap);
				}// end else
				
				m_slotPathMap.floodFill(slot_.m_cTX, slot_.m_cTY);	
				m_speed = m_mother.m_speed * TAIL_REJOIN_SPEED_FACTOR;
				m_checkDirMaxTimeout = 50;
				m_checkDirTimeout = m_checkDirMaxTimeout;				
				m_hasOnTargetRangeTrigger = false;
				m_targetTriggerRange = FOLLOW_MOTHER_RANGE;
			}// end if
			
			if (m_slot)
			{
				m_slot.m_inUse = false;
				m_slot = null;
			}// end if			
			
			m_slot = slot_;
			m_slot.m_inUse = true;
			m_followingMama = true;
			m_isStationaryWhileFree = false;
			Game.g.m_gameObjectList.push(new JellyBabyFoundMother(this));		
					
		}// end public function attachToSlot(slot_:EntityJellyBabyTailSlot):void
		
		public override function get player1Map():PathMap
		{
			if (m_followingMama && m_rejoining)
			{
				//trace("getting slot path map");
				return m_slotPathMap;
			}
			else
			{
				//trace("getting player path map");
				return super.player1Map;
			}// end else
		}// end public override function get player1Map():PathMap		
		
		public override function get player2Map():PathMap
		{
			if (m_followingMama && m_rejoining)
			{
				//trace("getting slot path map");
				return m_slotPathMap;
			}
			else
			{
				//trace("getting player path map");
				return super.player2Map;
			}// end else
		}// end public override function get player2Map():PathMap	
		
		public override function get player1SafetyMap():PathMap
		{
			if (m_followingMama && m_rejoining)
			{
				//trace("getting slot path map");
				return m_slotPathMap;
			}
			else
			{
				//trace("getting player path map");
				return Game.g.m_safetyPlayer1PathMap;
			}// end else			
		}// end public override function get player1SafetyMap():PathMap		
		
		public override function get player2SafetyMap():PathMap
		{
			if (m_followingMama && m_rejoining)
			{
				//trace("getting slot path map");
				return m_slotPathMap;
			}
			else
			{
				//trace("getting player path map");
				return Game.g.m_safetyPlayer2PathMap;
			}// end else		
		}// end public override function get player2SafetyMap():PathMap				
		
		public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void
		{
			super.onDeath(attacker_, weapon_);
			if (m_slot)
			{
				m_slot.m_inUse = false;
			}// end if
		}// end public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void			
		
	}// end public class EntityJellyBaby
	
}// end package