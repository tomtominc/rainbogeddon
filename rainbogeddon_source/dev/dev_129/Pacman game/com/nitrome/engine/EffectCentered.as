package com.nitrome.engine
{
		
	public class EffectCentered extends GameObject
	{
				
		public function EffectCentered(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);		
			m_drawBitmap = Game.g.m_effectsBitmap;
			x = (Game.TILE_WIDTH * m_tx)  + (Game.TILE_WIDTH / 2);
			y = (Game.TILE_HEIGHT * m_ty) + (Game.TILE_HEIGHT / 2);				
			
		}// end public function EffectCentered(tx_:uint, ty_:uint)
		
	}// end public class EffectCentered
	
}// end package