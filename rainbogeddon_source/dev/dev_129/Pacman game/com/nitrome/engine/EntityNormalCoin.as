package com.nitrome.engine
{
	
	
	public class EntityNormalCoin extends EntityCoin
	{
		
	
		public function EntityNormalCoin(tx_:uint, ty_:uint, size_:uint = 1)
		{
			super(tx_, ty_, size_);	
			m_collisionRect.x = -5;
			m_collisionRect.y = -5;
			m_collisionRect.width = 10;
			m_collisionRect.height = 10;
			
		}// end public function EntityNormalCoin(tx_:uint, ty_:uint, size_:uint = 1)
		

		
	}// end public class EntityNormalCoin
	
}// end package