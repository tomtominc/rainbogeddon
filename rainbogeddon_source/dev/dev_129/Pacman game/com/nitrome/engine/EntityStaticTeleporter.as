package com.nitrome.engine
{

	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;	
	
	public class EntityStaticTeleporter extends EntityTeleporter
	{
		
	

		public function EntityStaticTeleporter(tx_:uint, ty_:uint)
		{
			super(tx_, ty_, 3);
		}// end public function EntityStaticTeleporter(tx_:uint, ty_:uint)
			
		public override function doPhysics():void
		{
			
			
			if (m_terminated)
			{
				return;
			}// end if
			
			if (!m_firstCollisionP1CheckPerformed)
			{
				
				m_firstCollisionP1CheckPerformed = true;
				
				if (collidesWithObject(Game.g.m_player1))
				{
					m_waitingPlayer1UnCollide = true;
				}// end if
						
			}
			else
			{
				
				if (m_waitingPlayer1UnCollide)
				{
					if (!collidesWithObject(Game.g.m_player1))
					{
						m_waitingPlayer1UnCollide = false;
					}// end if
				}
				else
				{
					if (collidesWithObject(Game.g.m_player1))
					{
						if (m_twin)
						{
							m_firstCollisionP1CheckPerformed = false;
							Game.g.m_player1.teleportFrom(this);
						}// end if
					}// end if					
				}// end else
							
			}// end else
			
			if (!m_firstCollisionP2CheckPerformed)
			{
				
				m_firstCollisionP2CheckPerformed = true;
						
				if (collidesWithObject(Game.g.m_player2))
				{
					m_waitingPlayer2UnCollide = true;
				}// end if				
				
			}
			else
			{
							
				if (m_waitingPlayer2UnCollide)
				{
					if (!collidesWithObject(Game.g.m_player2))
					{
						m_waitingPlayer2UnCollide = false;
					}// end if					
				}
				else
				{
					if (collidesWithObject(Game.g.m_player2))
					{
						if (m_twin)
						{			
							m_firstCollisionP2CheckPerformed = false;
							Game.g.m_player2.teleportFrom(this);
						}// end if
					}// end if						
				}// end else				
				
			}// end else						
			
			
			if (!m_twin)
			{
			
				var gameObjectList:Vector.<GameObject> = Game.g.m_gameObjectList;
				
				for (var currObject:uint = 0; currObject < gameObjectList.length; currObject++)
				{
					if ((gameObjectList[currObject] != this) && (gameObjectList[currObject] is (getDefinitionByName(getQualifiedClassName(this)) as Class)))
					{
						m_twin = gameObjectList[currObject] as EntityTeleporter;
						(gameObjectList[currObject] as EntityTeleporter).m_twin = this;
						this.gotoAndPlay("new");
						(gameObjectList[currObject] as EntityTeleporter).gotoAndPlay("old");
						break;
					}// end if
				}// end for
				
			}// end if
			
		}// end public override function doPhysics():void
		
		public override function get disappearEffect():Class
		{
			return Player1TeleporterDisappearEffect;
		}// end public override function get disappearEffect():Class		
		
	}// end public class EntityStaticTeleporter
	
}// end package