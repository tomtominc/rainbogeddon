package com.nitrome.ui
{
	import com.nitrome.engine.BulletHolder;
	import com.nitrome.engine.BombHolder;
	import com.nitrome.engine.DrillHolder;
	import com.nitrome.engine.SafetyBubbleHolder;
	import com.nitrome.engine.TailHolder;
	import com.nitrome.engine.TeleporterHolder;
	import com.nitrome.engine.HeartHolder;
	import com.nitrome.engine.WeaponHolder;
	import editor.TextBox;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class HUDPanel extends MovieClip
	{
				

		public function HUDPanel()
		{
			
			player_1_power_up_indicator.stop();
			player_2_power_up_indicator.stop();
			player_1_power_up_level.text = "";
			player_2_power_up_level.text = "";
			player_1_score_title.text = "";
			player_2_score_title.text = "";
			player_1_score.text = "";
			player_2_score.text = "";
				
		}// end public function HUDPanel()
	
		private function playerPowerUp(weaponHolder_:WeaponHolder, powerUpIndicator_:MovieClip, powerUpLevel_:TextField):void
		{
			
			if (!weaponHolder_)
			{
				powerUpIndicator_.gotoAndStop("none");
				powerUpLevel_.text = "";
				return;
			}// end if
			
			if (weaponHolder_ is BulletHolder)
			{
				powerUpIndicator_.gotoAndStop("gun");	
			}
			else if (weaponHolder_ is BombHolder)
			{
				powerUpIndicator_.gotoAndStop("bomb");				
			}
			else if (weaponHolder_ is DrillHolder)
			{
				powerUpIndicator_.gotoAndStop("drill");	
			}
			else if (weaponHolder_ is SafetyBubbleHolder)
			{
				powerUpIndicator_.gotoAndStop("bubble");		
			}
			else if (weaponHolder_ is TeleporterHolder)
			{
				powerUpIndicator_.gotoAndStop("teleporter");		
			}
			else if (weaponHolder_ is TailHolder)
			{
				powerUpIndicator_.gotoAndStop("tail");
			}
			else if (weaponHolder_ is HeartHolder)
			{
				powerUpIndicator_.gotoAndStop("heart");
			}// end else if
			
			powerUpLevel_.text = weaponHolder_.m_currUpgradeStage.toString();
			
		}// end private function playerPowerUp(weaponHolder_:WeaponHolder, powerUpIndicator_:MovieClip, powerUpLevel_:TextField):void		
		
		
		public function player1PowerUp(weaponHolder_:WeaponHolder):void
		{
			playerPowerUp(weaponHolder_, player_1_power_up_indicator, player_1_power_up_level);
		}// end public function player1PowerUp(weaponHolder_:WeaponHolder):void	
		
		public function player2PowerUp(weaponHolder_:WeaponHolder):void
		{
			playerPowerUp(weaponHolder_, player_2_power_up_indicator, player_2_power_up_level);	
		}// end public function player2PowerUp(weaponHolder_:WeaponHolder):void	
		
		public function set player1ScoreEnabled(option_:Boolean):void
		{
			player_1_score_title.visible = option_;
			player_1_score_title.text = "P1";
			player_1_score.visible = option_;
		}// end public function set player1ScoreEnabled(option_:Boolean):void
		
		public function set player2ScoreEnabled(option_:Boolean):void
		{
			player_2_score_title.visible = option_;
			player_2_score_title.text = "P2";
			player_2_score.visible = option_;			
		}// end public function set player2ScoreEnabled(option_:Boolean):void	
		
		public function set player1Score(score_:uint):void
		{
			player_1_score.text = score_.toString();
		}// end public function set player1Score(score_:uint):void
		
		public function set player2Score(score_:uint):void
		{
			player_2_score.text = score_.toString();
		}// end public function set player2Score(score_:uint):void	
		
	}// end public class HUDPanel
	
}// end package