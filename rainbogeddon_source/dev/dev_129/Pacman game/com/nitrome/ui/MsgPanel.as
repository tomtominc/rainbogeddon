﻿package com.nitrome.ui {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	
	/**
	* A panel to tell us what level we're on and some other witticism if need be
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class MsgPanel extends MovieClip{
		
		public var count:int;
		public var state:int;
		public var step:Number;
		public var m_fadeNow:Boolean;
		public var m_fadeDelay:int;
		
		public static const VISIBLE:int = 1;
		public static const FADE_IN:int = 2;
		public static const FADE_OUT:int = 4;
		public static const HIDDEN:int = 8;
		public static const VISIBLE_DELAY:int = 30;
		public static const FADE_DELAY:int = 6; // 12
		
		public function MsgPanel() {
			visible = false;
			alpha = 0;
			state = HIDDEN;
			count = 0;
			step = 1.0 / FADE_DELAY;
		}
		/* Set the message and cue the fade in and out */
		public function setMsg(text:String = "", visibleDelay_:int = 0):void {
			levelNum.text = text;
			state = FADE_IN;
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			step = (1.0 / FADE_DELAY);
			count = (visibleDelay_ == 0)? VISIBLE_DELAY : visibleDelay_;
			visible = true;
			m_fadeNow = false;
		}
		public function onEnterFrame(e:Event):void {
			if(state == FADE_IN) {
				alpha += step;
				if(alpha >= 1.0) {
					state = VISIBLE;
				}
			} else if(state == VISIBLE) {
				if((count-- <= 0) || m_fadeNow) state = FADE_OUT;
			} else if(state == FADE_OUT) {
				alpha -= step;
				if(alpha <= 0) {
					state = HIDDEN;
					removeEventListener(Event.ENTER_FRAME, onEnterFrame);
					visible = false;
				}
			}
		}
	}
	
}