/**
* Base class for interface buttons
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.button {
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.nitrome.sound.SoundManager;

	public class BasicButton extends MovieClip{
		
		public var over:Boolean=false;
		
		public function BasicButton(){
			mouseChildren = false;
			buttonMode = true;
			// add event listeners
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut, false, 0, true);
			addEventListener(MouseEvent.CLICK, onClick, false, 0, true);
		}
		/* Update graphic for this button - this is what you will override for more
		 * complex buttons
		 */
		protected function updateGraphic():void{
			if (over) {
				gotoAndStop("over");
			} else {
				gotoAndStop("up");
			}
		}
		
		protected function onMouseOver(e:MouseEvent):void {
			SoundManager.playSound("ButtonRollover");
			over = true;
			updateGraphic();
		}
		protected function onMouseOut(e:MouseEvent):void{
			over = false;
			updateGraphic();
		}
		public function onClick(e:MouseEvent):void {
			SoundManager.playSound(clickSound);
			stage.focus = stage;
		}
		public function get clickSound():String
		{
			return "ButtonPress";
		}		
		
	}
	
}
