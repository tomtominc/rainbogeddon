﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Pause game on help screen
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class GameHelpButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			NitromeGame.root.game.pauseGame("help");
		}
		
	}
	
}