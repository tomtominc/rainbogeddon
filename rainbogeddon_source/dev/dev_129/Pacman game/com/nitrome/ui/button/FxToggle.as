/**
* Manipulates the sound fx settings for the game and stores those settings in NitromeGame
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.button {
	import com.nitrome.sound.SoundManager;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class FxToggle extends BasicButton{
		
		public function FxToggle(){
			//show the right thing at the start...
			if(!NitromeGame.getSfx()){
				gotoAndStop("offUp");
			}else{
				gotoAndStop("onUp");
			}
			// add event listeners
			addEventListener(MouseEvent.CLICK, onClick);
		}
		override protected function updateGraphic():void {
			if (over){
				if (SoundManager.sfx){
					gotoAndStop("onOver");
				} else if (!SoundManager.sfx){
					gotoAndStop("offOver");
				}
			} else {
				if (SoundManager.sfx){
					gotoAndStop("onUp");
				} else if (!SoundManager.sfx){
					gotoAndStop("offUp");
				}
			}
		}
		
		override public function onClick(e:MouseEvent):void {
			super.onClick(e);
			SoundManager.toggleSfx();
			updateGraphic();
		}
	}
	
}
