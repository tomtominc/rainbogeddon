package com.nitrome.ui.button 
{
	
	import com.nitrome.ui.PlayerSelectPanel;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import com.nitrome.ui.Key;
	import flash.ui.Keyboard;
	import flash.events.KeyboardEvent;
	import flash.events.Event;

	public class KeyButton extends BasicButton
	{
		
		private var m_selected:Boolean;
		private var m_textUnder:TextField;

		public function KeyButton()
		{
			m_selected = false;
			(parent.getChildByName(name + "TextUnder") as TextField).text = Key.keyString(NitromeGame.getValue(name));
			m_textUnder = (parent.getChildByName(name + "TextUnder") as TextField);
			//var value:uint = NitromeGame.getValue(name);
			//trace("set text to " + Key.keyString(NitromeGame.getValue(name)));
			NitromeGame.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);			
		}
		 
		private function onRemovedFromStage(e:Event = null):void
		{
			NitromeGame.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			NitromeGame.stage.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}// end private function onRemovedFromStage(e:Event = null):void
		
		private function onKeyDown(e_:KeyboardEvent = null):void
		{
			if (!m_selected)
			{
				return;
			}// end if
			setKeyValue(e_.keyCode);

		}		
		
		override public function onClick(e_:MouseEvent):void
		{
			super.onClick(e_);
			execute();
			e_.stopPropagation();
		}
		
		public function setKeyValue(key_:uint):void
		{
			
			var buttonList:Vector.<KeyButton> = (parent as PlayerSelectPanel).keyButtonList;		
			
			for (var currKey:uint = 0; currKey < buttonList.length; currKey++)
			{
				if ((buttonList[currKey] != this) && (buttonList[currKey].keyValue == key_))
				{
					buttonList[currKey].keyValue = NitromeGame.getValue(name);
				}// end if
			}// end for
			
			keyValue = key_;
		
		}// end public function setKeyValue(key_:uint):void			
		
		public function get keyValue():uint
		{		
			return NitromeGame.getValue(name);
		}// end public function get keyValue():uint
		
		public function set keyValue(key_:uint):void
		{
			
			var keyString:String = Key.keyString(key_);
			if ((keyString != "") && (key_ != Key.P))
			{
				if (m_selected)
				{
					selectedKey.visible = true;
					selectedKey.text = keyString;
				}// end if
				m_textUnder.visible = true;	
				m_textUnder.text = keyString;
				NitromeGame.setValue(name, key_);
				//trace("set key value to " + key_ + ", string = " + keyString);
			}// end if
		
		}// end public function set keyValue(key_:uint):void		
		
		public function reset():void
		{
			keyValue = NitromeGame.getValue(name + "_default");
		}// end public function reset():void
		
		public function execute():void
		{
			gotoAndStop("selected");
			m_selected = true;
			selectedKey.visible = false;
			selectedKey.text = Key.keyString(NitromeGame.getValue(name));
			m_textUnder.visible = false;
			deselectOthers();
		}
		
		private function deselectOthers():void
		{
			var buttonList:Vector.<KeyButton> = (parent as PlayerSelectPanel).keyButtonList;
			
			for (var currKey:uint = 0; currKey < buttonList.length; currKey++)
			{
				if ((buttonList[currKey] != this))
				{
					buttonList[currKey].deselect();
				}// end if
			}// end for
			
		}// end private function deselectOthers():void				
		
		protected override function onMouseOver(e:MouseEvent):void
		{
			if (!m_selected)
			{
				super.onMouseOver(e);
				selectedKey.text = Key.keyString(NitromeGame.getValue(name));
				selectedKey.visible = true;
				//trace("set mouse over text to " + selectedKey.text);
			}// end if
		}
		
		protected override function onMouseOut(e:MouseEvent):void
		{
			if (!m_selected)
			{
				super.onMouseOut(e);
			}// end if
		}		
		
		public function deselect():void
		{		
			gotoAndStop("up");
			m_selected = false;
			m_textUnder.visible = true;	
			m_textUnder.text = Key.keyString(NitromeGame.getValue(name));;			
		}// end public function deselect():void
		
	}// end public class KeyButton extends BasicButton
}