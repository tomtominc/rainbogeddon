package com.nitrome.ui
{

	import editor.TextBox;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class LevelFailedPanel extends MovieClip
	{
				

		public function LevelFailedPanel()
		{
			

			var player1Score:uint = Game.g.m_player1.m_score.value;
			var player2Score:uint = Game.g.m_player2.m_score.value;
			
			if (Game.m_isTwoPlayer)
			{
				playerFailedResultPanel.gotoAndStop("2_players");
			}// end if
			
			var score:uint = player1Score + player2Score;
			
			levelScoreTicker.startCount(score, score / 20, 0);
				
		}// end public function LevelFailedPanel()
	

	}// end public class LevelFailedPanel
	
}// end package