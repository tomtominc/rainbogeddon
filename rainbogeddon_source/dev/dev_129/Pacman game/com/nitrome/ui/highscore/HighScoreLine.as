﻿/**
* Displays rank, score and name of a highscore entry
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class HighScoreLine extends Sprite{
		private var highScoreBoard:HighScoreBoard;
		private var hidden:Boolean;
		private const FADE_SPEED:Number = 0.3;
		private var fadeDelay:int;
		private var fadeCount:int;
		public function HighScoreLine(){
			visible = false;
			hidden = false;
			// Locate instances
			highScoreBoard = parent as HighScoreBoard;
		}
		/* Feed the HighScoreLine data to be displayed and initialise it's fade-in variables */
		public function displayData(rank:int, nametext:String, scoretext:String):void{
			trace(name+" displayData:");
			rankText.text=String(rank+".");
			
			nameText.text=nametext.toUpperCase();
			
			trace(rank+","+nametext+","+scoretext);
			
			if(highScoreBoard.zeroFill){
				while(scoretext.length < highScoreBoard.MAX_DIGITS) scoretext = "0" + scoretext;
				scoreText.text=scoretext;
			}
			
			scoreText.text=scoretext;
			
			alpha = 0;
			visible=true;
			
			// Calculate fade delay:
			// what this this next odd equation does is figure out how long we should wait to fade
			// in this line based on rank, fade_speed and the number of lines - this keeps our
			// system flexible
			fadeDelay = ((rank-1)%highScoreBoard.NUM_LINES)*int(1.0/FADE_SPEED);
			fadeCount = 0;
			addEventListener(Event.ENTER_FRAME, performFade);
		}
		/* Keep this line hidden as it's not going to be used */
		public function hide():void{
			trace(name+" hide");
			visible=false;
			hidden = true;
		}
		
		private function performFade(e:Event):void{
			if(fadeCount >= fadeDelay){
				alpha += FADE_SPEED;
				if(alpha >= 1.0){
					e.target.removeEventListener(Event.ENTER_FRAME, performFade);
				}
			}
			fadeCount++;
		}
			
	}
	
}
