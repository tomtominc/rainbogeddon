/**
* Clears the name_text field
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import com.nitrome.ui.button.BasicButton;
	import flash.events.MouseEvent;

	public class ClearButton extends BasicButton{
		private var scoreSubmitPanel:ScoreSubmitPanel;
		
		public function ClearButton(){
			scoreSubmitPanel = parent as ScoreSubmitPanel;
		}
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			// clear the name_text field
			scoreSubmitPanel.clearName();
		}
		
	}
	
}
