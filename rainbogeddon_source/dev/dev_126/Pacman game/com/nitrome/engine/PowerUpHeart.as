package com.nitrome.engine
{
	
	public class PowerUpHeart extends PowerUp
	{
		
		public function PowerUpHeart(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpHeart(tx_:uint, ty_:uint)
		
		public override function get soundID():String
		{		
			return "Heart";
		}// end public override function get soundID():String			
		
		public override function get holder():Class
		{		
			return HeartHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpHeart
	
}// end package