package com.nitrome.engine
{

	
	public class EntityPlayer2Teleporter extends EntityPlayerTeleporter
	{

		public function EntityPlayer2Teleporter(tx_:uint, ty_:uint, powerLevel_:uint)
		{
			super(tx_, ty_, powerLevel_);
		}// end public function EntityPlayer2Teleporter(tx_:uint, ty_:uint, powerLevel_:uint)
			
		public override function get disappearEffect():Class
		{
			return Player2TeleporterDisappearEffect;
		}// end public override function get disappearEffect():Class			
		
	}// end public class EntityPlayer2Teleporter
	
}// end package