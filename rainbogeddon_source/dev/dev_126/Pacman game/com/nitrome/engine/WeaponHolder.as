package com.nitrome.engine
{

	import com.nitrome.sound.SoundManager;	
	
	public class WeaponHolder
	{
		
		public static const MAX_UPGRADE_STAGES:int = 3;
		
		public  var m_parent:EntityCharacter;
		public  var m_currUpgradeStage:uint;
		
		public function WeaponHolder(parent_:EntityCharacter)
		{
			m_parent = parent_;
			m_currUpgradeStage = 1;
		}// end public function WeaponHolder()
				
		public function upgrade():void
		{
			
			SoundManager.playSound("LevelUp");
			
		}// end public function upgrade():void			
	
		public function canFire():Boolean
		{
			return false;
		}// end public function canFire():Boolean			
		
		public function tryFire():Boolean
		{
			return false;
		}// end public function tryFire():Boolean	
		
		public function terminate():void
		{
			
			
		}// end public function terminate():void		
		
		public function tryStopFire():void
		{
			m_parent.m_stopFire = false;
			
		}// end public function tryStopFire():void	
		
		public function onChangeDir():void
		{
			
			
		}// end public function onChangeDir():void		
		
		public function onTeleport():void
		{

			
		}// end public function onTeleport():void	
		
		public function onTeleportDone():void
		{

			
		}// end public function onTeleportDone():void		
		
		public function onTeleportCancel():void
		{

			
		}// end public function onTeleportCancel():void				
		
	}// end public class WeaponHolder
	
}// end package