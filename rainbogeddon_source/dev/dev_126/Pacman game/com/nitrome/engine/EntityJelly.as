package com.nitrome.engine
{
	
	
	public class EntityJelly extends EntityEnemy 
	{
			
		public function EntityJelly(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_targetTriggerRange = 4;
			m_hasOnTargetRangeTrigger = true;			
			m_checkDirMaxTimeout = 1500; 
			m_checkDirTimeout = m_checkDirMaxTimeout;	
			m_hasDecreasingTimeout = false;
			m_glow.m_color = 0xff89ff00;
		}// end public function EntityJelly(tx_:uint, ty_:uint)
					
		public override function onTargetInRange(target_:Entity):void
		{
		
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;		
			
		}// end public override function onTargetInRange(target_:Entity):void			
		
		public override function onTargetOutOfRange():void
		{
			
			m_checkDirMaxTimeout = 1500;
			m_checkDirTimeout = m_checkDirMaxTimeout;				
			
		}// end public override function onTargetOutOfRange():void				
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnGreen;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedGreen;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityJelly
	
}// end package