package com.nitrome.engine
{

	import com.nitrome.geom.PointUint;
	import flash.geom.Vector3D;
	
	public class EntityTail extends EntityCentered
	{
			
		public static const DAMAGE:int = 1;
		public static const NUM_STATE_ANIM_FRAMES:uint = 13;
		public static const TOTAL_ANIM_FRAMES:uint = 26;
		public static const WAIT_TO_FADE_DURATION:int = 4000;
		public static const FADE_DURATION:int = 1000;
		
		public static const STATE_NORMAL:uint = 0;
		public static const STATE_SNAPPING_TO_GRID:uint = 1;
		public static const STATE_WAITING_TO_FADE:uint = 2;
		public static const STATE_FADING:uint = 3;
		
		
		public var m_damage:int;		
		public var m_leader:Entity;
		public var m_root:Entity;
		public var m_tail:Tail;
		public var m_glow:Glow;
		private var m_waitStartTime:int;
		
		public function EntityTail(leader_:Entity, root_:Entity)
		{
			super(leader_.m_tx, leader_.m_ty);
			m_drawOrder = 1;
			x = leader_.x;
			y = leader_.y;
			m_tail = new Tail(this, leader_, root_);
			m_collisionRect.x = -2.5;
			m_collisionRect.y = -2.5;
			m_collisionRect.width = 5;
			m_collisionRect.height = 5;
			m_leader = leader_;
			m_root = root_;
			m_damage = DAMAGE;
			m_canDie = false;
			m_glow = new Glow(this);
			m_glow.m_color = 0xffffffff;
			//m_frontChildGameObjectList.push(m_glow);
			Game.g.m_gameObjectList.push(m_glow);
			m_state = STATE_NORMAL;
			if (m_leader != m_root)
			{
				
				var startFrame:uint = m_leader.currentFrame + NUM_STATE_ANIM_FRAMES;
				
				if (startFrame > TOTAL_ANIM_FRAMES)
				{
					startFrame = startFrame - TOTAL_ANIM_FRAMES;
				}// end if
				
				//trace("starting at frame " + startFrame);
				
				gotoAndPlay(startFrame);
			}// end if
		}// end public function EntityTail(leader_:Entity, root_:Entity)
	
			
		public override function doPhysics():void
		{
			
			if (m_terminated)
			{
				return;
			}// end if
			
			switch(m_state)
			{
				
				case STATE_NORMAL:
				{
					m_tail.doPhysics();			
					break;
				}// end case
				
				case STATE_SNAPPING_TO_GRID:
				{
					var tileCenterVec:Vector3D = new Vector3D((m_cTX * Game.TILE_WIDTH) + (Game.TILE_WIDTH / 2), (m_cTY * Game.TILE_HEIGHT) + (Game.TILE_HEIGHT / 2));
					var posVec:Vector3D = new Vector3D(x, y);
					var tileToPosVec:Vector3D = tileCenterVec.subtract(posVec);
					tileToPosVec.normalize();
					tileToPosVec.scaleBy(1.5);
					
					x += tileToPosVec.x;
					y += tileToPosVec.y;
					
					posVec.x = x;
					posVec.y = y;
					
					tileToPosVec = tileCenterVec.subtract(posVec);
					
					if (tileToPosVec.length <= 1.5)
					{
						x = tileCenterVec.x;
						y = tileCenterVec.y;
						m_state = STATE_WAITING_TO_FADE;
						m_waitStartTime = Game.g.timer;
					}// end if						
					
					break;
				}// end case	
				
				case STATE_WAITING_TO_FADE:
				{
					
					if ((Game.g.timer - m_waitStartTime) > WAIT_TO_FADE_DURATION)
					{
						m_state = STATE_FADING;
						m_waitStartTime = Game.g.timer;
						m_flicker = true;
					}// end if
					
					break;
				}// end case	
				
				case STATE_FADING:
				{
				
					if ((Game.g.timer - m_waitStartTime) > FADE_DURATION)
					{
						m_terminated = true;
						m_spatialHashMap.unregisterObject(this);
					}// end if					
					
					break;
				}// end case					
				
			}// end switch
			
			
								
			/*var collidingObjectList:Vector.<Entity>;
			
			collidingObjectList = getCollidingObjects(null, null, EntityEnemy);

			for (var currObject:uint = 0; currObject < collidingObjectList.length; currObject++)
			{
				collidingObjectList[currObject].doDamage(m_damage, m_vel, m_root);
			}// end for*/		
			
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;	
			
			tickChildren();
			
			m_spatialHashMap.updateObject(this);
			
		}// end public override function doPhysics():void		
		
		public function detach():void
		{		
			if (m_state == STATE_NORMAL)
			{
				m_state = STATE_SNAPPING_TO_GRID;
			}// end if
		}// end public function detach():void
		
		public function startTeleport():void
		{
			
			if (currentLabel == "state_1")
			{
				gotoAndPlay("state_1_teleporting");
			}
			else
			{
				gotoAndPlay("state_2_teleporting");
			}// end else			
			
		}// end public function startTeleport():void
		
		public function onTeleportCancel():void
		{
			
			resetAnimation();
			
		}// end public function onTeleportCancel():void	
		
		public function resetAnimation():void
		{
			
			if (m_leader != m_root)
			{
				
				var startFrame:uint = m_leader.currentFrame + NUM_STATE_ANIM_FRAMES;
				
				if (startFrame > TOTAL_ANIM_FRAMES)
				{
					startFrame = startFrame - TOTAL_ANIM_FRAMES;
				}// end if

				gotoAndPlay(startFrame);
			}
			else
			{
				
				gotoAndPlay(1);
			
			}// end else				
			
		}// end public function resetAnimation():void			
		
		public function onTeleportDone():void
		{
			
			resetAnimation();
			x = m_root.x;
			y = m_root.y;
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;	
			m_spatialHashMap.updateObject(this);
			
		}// end public function onTeleportDone():void			
		
	}// end public class EntityTail
	
}// end package