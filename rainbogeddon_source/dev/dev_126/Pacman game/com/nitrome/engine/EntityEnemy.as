package com.nitrome.engine
{
	
	import com.nitrome.engine.pathfinding.PathMap;
	import com.nitrome.engine.EntityPlayer;
	import com.nitrome.util.HiddenInt;
	import flash.geom.*;
	import com.nitrome.sound.SoundManager;
	
	public class EntityEnemy extends EntityCharacter
	{
			
		public static const STATE_FOLLOWING:uint            = 0;
		public static const STATE_PATROLLING_CORRIDORS:uint = 1;
		
		public static const STATE_SMART_FOLLOWING_NORMAL:uint    = 0;
		public static const STATE_SMART_FOLLOWING_COUNTDOWN:uint = 1;		
		
		public static const GOING_STRAIGHT_MIN_DURATION:int = 1000;// 10000;
		public static const GOING_STRAIGHT_MAX_DURATION:int = 3000;// 20000;
		public static const SMART_FOLLOW_DURATION:int = 10000; // 15000;
		public static const MIN_TURN_TIMES:uint  = 2;
		public static const MAX_TURN_TIMES:uint  = 5;
		public static const CW:uint  = 0;	
		public static const CCW:uint = 1;	
		public static const HIT_POINTS:int = 30;
		public static const SPEED:Number = 2.25; //1.5
		public static const MIN_CHECK_DIR_TIMEOUT:uint = 250;
		public static const VEL_CHANGE_TIMEOUT:int = 150;
		public static const MAX_PATH_LOOK_AHEAD_TILES:uint = 5;
		public static const SEPARATING_FROM_ENEMY_MIN_DURATION:int = 2000;
		public static const SEPARATING_FROM_ENEMY_MAX_DURATION:int = 4000;
		public static const REACTING_TO_HIT_DURATION:int = 250;
		
		public static const ANIM_WALK_LEFT:uint  = 0;
		public static const ANIM_WALK_RIGHT:uint = 1;
		public static const ANIM_WALK_UP:uint    = 2;
		public static const ANIM_WALK_DOWN:uint  = 3;		
		
		public  var m_player1PathMap:PathMap;	
		public  var m_player2PathMap:PathMap;	
		private var m_dirTable:Object;
		public  var m_velChangeStartTime:int;
		public  var m_smartFollowingPlayerNumber:uint;
		public  var m_smartFollowing:Boolean;
		private var m_smartFollowingState:uint;
		public  var m_smartFollowCountdownStartTime:int;
		private var m_prevRandVel:Vector3D;
		private var m_qCorridorDir:uint;
		private var m_numTurnTimes:uint;
		private var m_maxTurnTimes:uint;	
		private var m_goingStraightTryingToTurn:Boolean;
		private var m_goingStraightStartTime:int;
		private var m_goingStraightDuration:int;
		private var m_waitingForCornerStop:Boolean;		
		private static var m_corridorDirTable:Object;
		public  var m_prevVel:Vector3D;
		private var m_prevNonZeroVel:Vector3D;
		public  var m_damage:int;
		public  var m_checkDirStartTime:int;
		public  var m_currAnim:uint;
		public  var m_checkDirTimeout:int;
		public  var m_checkDirMaxTimeout:int;
		public  var m_checkDirSmartFollowingStartTime:int;
		public  var m_checkDirSmartFollowTimeout:int;
		public  var m_checkDirSmartFollowMaxTimeout:int;		
		public  var m_maxDistFactor:Number;
		public  var m_backForthTimes:uint;
		public  var m_targetTriggerRange:uint;
		public  var m_hasOnTargetRangeTrigger:Boolean;
		public  var m_targetInRange:Boolean;
		public  var m_hasLifetime:Boolean;
		public  var m_lifetime:int;
		public  var m_lifeStartTime:int;
		public  var m_backForthDecPerc:Number;
		public  var m_evadesTarget:Boolean;
		public  var m_followsTarget:Boolean;
		public  var m_alerted:Alerted;
		public  var m_isAlerted:Boolean;
		public  var m_alertEffect:EnemyAlertEffect;
		public  var m_hasDecreasingTimeout:Boolean;
		public  var m_hasBackAndForthDecPerc:Boolean;
		public  var m_canSmartFollow:Boolean;
		public  var m_checksLineOfSight:Boolean;
		public  var m_player1OnLineOfSight:Boolean;
		public  var m_player2OnLineOfSight:Boolean;
		public  var m_lineOfSightPlayer1Dir1:uint;
		public  var m_lineOfSightPlayer1Dir2:uint;
		public  var m_lineOfSightPlayer2Dir1:uint;
		public  var m_lineOfSightPlayer2Dir2:uint;		
		public  var m_chosenTarget:Entity;
		public  var m_currInRangeTarget:Entity;
		public  var m_currInRangeTargetDist:uint;
		public  var m_fromTileX:int;
		public  var m_fromTileY:int;
		public  var m_distFromTarget:uint;
		public  var m_chosenPathMap:PathMap;		
		public  var m_prevFollowsTargetVal:Boolean;
		public  var m_prevHasDecreasingTimeoutVal:Boolean;
		public  var m_prevHasBackAndForthDecPercVal:Boolean;
		public  var m_respawns:Boolean;
		public  var m_showDeathExplosion:Boolean;
		public  var m_applyOverrideDir:Boolean; 
		public  var m_overrideDir:int;
		public  var m_skipAnim:Boolean;
		public  var m_lifeTimeEnded:Boolean;
		public  static var m_numDead:uint;
		public  var m_score:HiddenInt;
		public  var m_separatingFromEnemyStartTime:int;
		public  var m_separatingFromEnemyDuration:int;
		public  var m_isSeparatingFromEnemy:Boolean;
		private var m_reactingToHit:Boolean;
		private var m_reactingToHitStartTime:int;
		public  var m_canMove:Boolean;
		
		public function EntityEnemy(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);			
			m_score = new HiddenInt(10);
			m_speed = SPEED;
			m_state = STATE_FOLLOWING;			
			m_isSeparatingFromEnemy = false;
			m_collisionRect.x = -9;
			m_collisionRect.y = -9;
			m_collisionRect.width = 18;
			m_collisionRect.height = 18;	
			m_hitPoints = HIT_POINTS;
			m_maxHitPoints = HIT_POINTS;
			m_goingStraightTryingToTurn = false;
			m_player1PathMap = Game.g.m_player1PathMap;
			m_player2PathMap = Game.g.m_player2PathMap;
			m_drawOrder = 2;
			m_prevRandVel = new Vector3D();
			m_glow.m_color = 0xffff0000;
			m_evadesTarget = false;
			m_followsTarget = true;
			m_checkDirStartTime = 0;
			m_checkDirMaxTimeout = 6000;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_checkDirSmartFollowMaxTimeout = 100;	
			m_checkDirSmartFollowTimeout = m_checkDirSmartFollowMaxTimeout;
			m_hasOnTargetRangeTrigger = false;
			m_targetInRange = false;
			m_targetTriggerRange = 0;
			m_hasBackAndForthDecPerc = true;
			m_hasLifetime = false;
			m_lifetime = 0;			
			m_lifeStartTime = 0;
			m_backForthDecPerc = 0.1;
			m_backForthTimes = 0;
			m_maxDistFactor = 20;
			m_smartFollowing = false;
			m_canSmartFollow = true;
			//m_frontChildGameObjectList.push(m_glow);	
			Game.g.m_gameObjectList.push(m_glow);
			m_prevVel = new Vector3D();
			m_prevNonZeroVel = new Vector3D();
			m_velChangeStartTime = Game.g.timer;
			gotoAndPlay("walk_down");
			m_currAnim = ANIM_WALK_DOWN;
			m_alerted = new Alerted(this);
			m_frontChildGameObjectList.push(m_alerted);	
			m_hasDecreasingTimeout = true;
			m_checksLineOfSight = false;
			m_player1OnLineOfSight = false;
			m_player2OnLineOfSight = false;
			m_showDeathExplosion = true;
			m_applyOverrideDir = false;
			m_overrideDir = DIR_NONE;
			m_currInRangeTarget = null;
			m_currInRangeTargetDist = uint.MAX_VALUE;
			m_chosenTarget = null;
			m_dirTable = new Object();
			m_lifeTimeEnded = false;
			m_isAlerted = false;
			m_alertEffect = null;		
			m_skipAnim = false;
			m_respawns = true;
			if (Game.g.state == Game.STATE_PLAYING_NORMAL)
			{
				m_canMove = true;
			}
			else
			{
				m_canMove = false;
			}// end else
			
					
			if (!m_corridorDirTable)
			{
				m_corridorDirTable = new Object();
				
				m_corridorDirTable[DIR_LEFT]  = new Object();
				
				m_corridorDirTable[DIR_LEFT][DIR_UP]   = CW;
				m_corridorDirTable[DIR_LEFT][DIR_DOWN] = CCW;
				
				m_corridorDirTable[DIR_RIGHT] = new Object();
				
				m_corridorDirTable[DIR_RIGHT][DIR_UP]   = CCW;
				m_corridorDirTable[DIR_RIGHT][DIR_DOWN] = CW;
				
				m_corridorDirTable[DIR_UP]    = new Object();
				
				m_corridorDirTable[DIR_UP][DIR_LEFT]  = CCW;
				m_corridorDirTable[DIR_UP][DIR_RIGHT] = CW;				
				
				m_corridorDirTable[DIR_DOWN]  = new Object();
				
				m_corridorDirTable[DIR_DOWN][DIR_LEFT]  = CW;
				m_corridorDirTable[DIR_DOWN][DIR_RIGHT] = CCW;						
				
			}// end if
			
			m_dirTable[DIR_NONE] = "DIR_NONE";
			m_dirTable[DIR_LEFT] = "DIR_LEFT";
			m_dirTable[DIR_RIGHT] = "DIR_RIGHT";
			m_dirTable[DIR_UP] = "DIR_UP";
			m_dirTable[DIR_DOWN] = "DIR_DOWN";
			
		}// end public function EntityEnemy(tx_:uint, ty_:uint)
		
		public static function free():void		
		{
			m_numDead = 0;
			m_corridorDirTable = null;
		}// end public static function free():void		
		
		public function onRespawn():void
		{		
		
			if (m_numDead > 0)
			{
				
				m_numDead--;				
			
				if (m_numDead <= 0)
				{
					m_numDead = 0;
					SoundManager.playSound("EnemyRespawnEndOfMultiplier");
				}
				else if (m_numDead < EntityCoin.MAX_SIZE)
				{
					SoundManager.playSound("EnemyRespawnMultiplier" + (m_numDead+1));
				}// end else if
				
			}// end if			
			
			//trace(this + " Respawn, numDead = " + m_numDead);
			
		}// end public function onRespawn():void		
		
		public function startAlert():void
		{		
			m_isAlerted = true;
			m_alertEffect = new EnemyAlertEffect(this);
			Game.g.m_gameObjectList.push(m_alertEffect);
			gotoAndPlay(alertedLabel);
		}// end public function startAlert():void
		
		public function get alertedLabel():String
		{		
			return "alerted";
		}// end public function get alertedLabel():String
		
		public function applyOverrideDir(dir_:int):void
		{		
			m_applyOverrideDir = true;
			m_overrideDir = dir_;
		}// end public function applyOverrideDir(dir_:int):void
		
		public function onLifetimeEnded():void
		{
			
			
		}// end public function onLifetimeEnded():void				
		
		public function onTargetOutOfRange():void
		{
			
			
		}// end public function onTargetOutOfRange():void				
		
		public function onTargetInRange(target_:Entity):void
		{
			
			
		}// end public function onTargetInRange(target_:Entity):void				
		
		public function whileTargetInRange(target_:Entity):void
		{
			
			
		}// end public function whileTargetInRange(target_:Entity):void		
		
		public function alertDone():void
		{
			stop();
			m_isAlerted = false;
			m_alertEffect = null;		
			m_forceUpdateAnim = true;
		}// end public function alertDone():void			
		
		private function isOtherEnemyInFollowRoute():Boolean
		{
			
			
			if (!(m_chosenTarget is EntityPlayer))
			{
				return false;
			}// end if
			
			
			// inspect a specified number of tiles ahead in target path
			// and check if an enemy is already on one of those tiles and in following mode
			
			var lookAheadTileList:Vector.<Vector.<uint>> = new Vector.<Vector.<uint>>;
			
			var currTX:uint = m_fromTileX;
			var currTY:uint = m_fromTileY;
			
			lookAheadTileList.push(new Vector.<uint>);
			lookAheadTileList[lookAheadTileList.length - 1].push(currTX);
			lookAheadTileList[lookAheadTileList.length - 1].push(currTY);			
			
			for (var currLookAheadTile:uint = 0; currLookAheadTile < MAX_PATH_LOOK_AHEAD_TILES; currLookAheadTile++)
			{
				
				var dir:uint = m_chosenPathMap.getNextGoalDir(currTX, currTY);
				
				if (dir == DIR_NONE)
				{
					break;
				}// end if
				
				switch(dir)
				{
					
					case DIR_LEFT:
					{
						
						currTX = getWrappedTileX(currTX - 1);
						
						break;
					}// end case
					
					case DIR_RIGHT:
					{
						
						currTX = getWrappedTileX(currTX + 1);
						
						break;
					}// end case	
					
					case DIR_UP:
					{
						
						currTY = getWrappedTileY(currTY - 1);
						
						break;
					}// end case						
					
					case DIR_DOWN:
					{
						
						currTY = getWrappedTileY(currTY + 1);
						
						break;
					}// end case						
					
				}// end switch
				
				lookAheadTileList.push(new Vector.<uint>);
				lookAheadTileList[lookAheadTileList.length - 1].push(currTX);
				lookAheadTileList[lookAheadTileList.length - 1].push(currTY);
				
			}// end for
			
			if (!lookAheadTileList.length)
			{
				return false;
			}// end if
			
			var gameObjectList:Vector.<GameObject> = Game.g.m_gameObjectList;
			
			for (var currObject:uint = 0; currObject < gameObjectList.length; currObject++)
			{
				if ((gameObjectList[currObject] is EntityEnemy) && (gameObjectList[currObject] != this))
				{
					
					var enemy:EntityEnemy = gameObjectList[currObject] as EntityEnemy;
					
					if ((enemy.m_state != STATE_FOLLOWING) || (!(enemy.m_chosenTarget is EntityPlayer)))
					{
						continue;
					}// end if
					
					// check enemy position against look ahead tile list
					for (var currTile:uint = 0; currTile < lookAheadTileList.length; currTile++)
					{
						
						if ((enemy.m_tx == lookAheadTileList[currTile][0]) && (enemy.m_ty == lookAheadTileList[currTile][1]))
						{
							return true;
						}// end if
						
					}// end for
					
				}// end if
			}// end for
			
			return false;
			
		}// end private function isOtherEnemyInFollowRoute():Boolean
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);
			
			
			if (m_respawns)
			{
				m_numDead++;
				if ((m_numDead <= EntityCoin.MAX_SIZE) && !Game.g.completed)
				{
					SoundManager.playSound("EnemyDieMultiplier" + (m_numDead+1));
				}
				else
				{
					SoundManager.playSound("EnemyDie");
				}// end else
			}
			else
			{
				SoundManager.playSound("EnemyDie");
			}// end else
			
			if (m_showDeathExplosion)
			{
				Game.g.m_gameObjectList.push(new destroyedEffectType(m_cTX, m_cTY));
			}// end if
			
			if (Game.g.completed)
			{
				Game.g.m_gameObjectList.push(new RespawnExplosion(this, false));
			}
			else if (m_respawns)
			{
				//trace(this + " dead, starting respawn");
				Game.g.m_gameObjectList.push(new RespawnExplosion(this));
			}// end if

			
		}// end public override function onDeath(attacker_:Entity = null):void				
		
		public function onStopSmartFollowing():void
		{
			
		}// end public function onStopSmartFollowing():void			
		
		public function get destroyedEffectType():Class
		{
			return null;
		}// end public function get destroyedEffectType():Class		
		
		public function get player1Map():PathMap
		{
			return Game.g.m_player1PathMap;
		}// end public function get player1Map():PathMap		
		
		public function get player2Map():PathMap
		{
			return Game.g.m_player2PathMap;
		}// end public function get player2Map():PathMap	
		
		public function get player1SafetyMap():PathMap
		{
			return Game.g.m_safetyPlayer1PathMap;
		}// end public function get player1SafetyMap():PathMap		
		
		public function get player2SafetyMap():PathMap
		{
			return Game.g.m_safetyPlayer2PathMap;
		}// end public function get player2SafetyMap():PathMap		
		
		public function updateMap():void
		{
			
			m_fromTileX = Math.floor(x/Game.TILE_WIDTH);
			m_fromTileY = Math.floor(y/Game.TILE_HEIGHT);						
			
			m_fromTileX = getWrappedTileX(m_fromTileX);
			m_fromTileY = getWrappedTileY(m_fromTileY);					
			
			if (Game.g.m_safetyTileMap.m_active)
			{
				// if the enemy is in a blocked tile it means it's inside the safe zone already so
				// there's no need to change the map
				if (Game.g.m_safetyTileMap.canGoToTile(m_fromTileX, m_fromTileY))
				{
					m_tileMap = Game.g.m_safetyTileMap;
					m_player1PathMap = player1SafetyMap;
					m_player2PathMap = player2SafetyMap;
					
				}
				else
				{
					m_tileMap = Game.g.m_enemyTileMap; //Game.g.m_tileMap;				
					m_player1PathMap = player1Map;	
					m_player2PathMap = player2Map;	
				}// end else
			}
			else
			{
				m_tileMap = Game.g.m_enemyTileMap; //Game.g.m_tileMap;	
				m_player1PathMap = player1Map;	
				m_player2PathMap = player2Map;		
			}// end else				
			
		}// end public function updateMap():void	
		
		public function updateTarget():void
		{
			
					
			var distFromPlayer1:uint = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);	
			var distFromPlayer2:uint = m_player2PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);	
			
			if (m_smartFollowing)
			{
								
				if (m_smartFollowingPlayerNumber == 1)
				{
					m_distFromTarget = distFromPlayer1;
					m_chosenPathMap = m_player1PathMap;
					m_chosenTarget = Game.g.m_player1;					
				}
				else
				{
					m_distFromTarget = distFromPlayer2;
					m_chosenPathMap = m_player2PathMap;	
					m_chosenTarget = Game.g.m_player2;					
				}// end else
				
			}
			else
			{
			
				if (distFromPlayer1 < distFromPlayer2)
				{
					m_distFromTarget = distFromPlayer1;
					m_chosenPathMap = m_player1PathMap;
					m_chosenTarget = Game.g.m_player1;
				}
				else if (distFromPlayer1 > distFromPlayer2)
				{
					m_distFromTarget = distFromPlayer2;
					m_chosenPathMap = m_player2PathMap;	
					m_chosenTarget = Game.g.m_player2;
				}
				else if(Math.floor(Math.random()*2) == 1)
				{
					m_distFromTarget = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
					m_chosenPathMap = m_player1PathMap;	
					m_chosenTarget = Game.g.m_player1;
				}
				else
				{
					m_distFromTarget = m_player2PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
					m_chosenPathMap = m_player2PathMap;
					m_chosenTarget = Game.g.m_player2;
				}// end else
			
			}// end else				
			
				
		}// end public function updateTarget():void			
		
		public function updateTargetRange():void
		{
			
			if (!m_hasOnTargetRangeTrigger)
			{
				return;
			}// end if
			
			if (m_distFromTarget <= m_targetTriggerRange)
			{
				
				if (!m_targetInRange)
				{
					m_currInRangeTarget = m_chosenTarget;
					onTargetInRange(m_chosenTarget);	
					whileTargetInRange(m_chosenTarget);
					m_targetInRange = true;
				}
				else
				{	
					whileTargetInRange(m_chosenTarget);
				}// end else
			
			}
			else if (m_targetInRange && (m_distFromTarget > m_targetTriggerRange))
			{
				m_currInRangeTarget = null;
				m_targetInRange = false;
				onTargetOutOfRange();
			}// end eelse if
			
		}// end public function updateTargetRange():void
		
		public function isInLineOfSightPlayer(player_:EntityPlayer):Boolean
		{
			
			if ((m_tx != player_.m_tx) && (m_ty != player_.m_ty))
			{
				return false;
			}// end if
			
			var testX:uint;
			var testY:uint;
			var dir1Blocked:Boolean = false;
			var dir2Blocked:Boolean = false;
			
			if (player_ is EntityPlayer1)
			{
				m_lineOfSightPlayer1Dir1 = DIR_NONE;
				m_lineOfSightPlayer1Dir2 = DIR_NONE;
			}
			else
			{
				m_lineOfSightPlayer2Dir1 = DIR_NONE;
				m_lineOfSightPlayer2Dir2 = DIR_NONE;				
			}// end else
			
			// check if there are any obstacles between the player and the enemy
			if (m_ty == player_.m_ty)
			{
				
				for (testX = m_tx; testX != player_.m_tx;)
				{
					
					testX++;
					testX = getWrappedTileX(testX);
				
					if (m_tileMap.m_tileMap[m_tileMap.m_width * m_ty + testX] == PathMap.TILE_BLOCKED)
					{
						dir1Blocked = true;
						break;
					}// end if
				
				}// end for
				
				if (testX == player_.m_tx)
				{
					if (player_ is EntityPlayer1)
					{
						m_lineOfSightPlayer1Dir1 = DIR_RIGHT;
					}
					else
					{
						m_lineOfSightPlayer2Dir1 = DIR_RIGHT;		
					}// end else					
				}// end if
				
				for (testX = m_tx; testX != player_.m_tx;)
				{
					
					testX--;
					testX = getWrappedTileX(testX);
				
					if (m_tileMap.m_tileMap[m_tileMap.m_width * m_ty + testX] == PathMap.TILE_BLOCKED)
					{
						dir2Blocked = true;
						break;
					}// end if
				
				}// end for		
				
				if (testX == player_.m_tx)
				{
					if (player_ is EntityPlayer1)
					{
						m_lineOfSightPlayer1Dir2 = DIR_LEFT;
					}
					else
					{
						m_lineOfSightPlayer2Dir2 = DIR_LEFT;		
					}// end else					
				}// end if				
				
				if (dir1Blocked && dir2Blocked)
				{
					return false;
				}// end if
				
			}
			else 
			{
				
				for (testY = m_ty; testY != player_.m_ty;)
				{
					
					testY++;
					testY = getWrappedTileY(testY);
				
					if (m_tileMap.m_tileMap[m_tileMap.m_width * testY + m_tx] == PathMap.TILE_BLOCKED)
					{
						dir1Blocked = true;
						break;
					}// end if
				
				}// end for
				
				if (testY == player_.m_ty)
				{
					if (player_ is EntityPlayer1)
					{
						m_lineOfSightPlayer1Dir1 = DIR_DOWN;
					}
					else
					{
						m_lineOfSightPlayer2Dir1 = DIR_DOWN;		
					}// end else					
				}// end if				
				
				for (testY = m_ty; testY != player_.m_ty;)
				{
					
					testY--;
					testY = getWrappedTileY(testY);
				
					if (m_tileMap.m_tileMap[m_tileMap.m_width * testY + m_tx] == PathMap.TILE_BLOCKED)
					{
						dir2Blocked = true;
						break;
					}// end if
				
				}// end for		
				
				if (testY == player_.m_ty)
				{
					if (player_ is EntityPlayer1)
					{
						m_lineOfSightPlayer1Dir2 = DIR_UP;
					}
					else
					{
						m_lineOfSightPlayer2Dir2 = DIR_UP;		
					}// end else					
				}// end if					
				
				if (dir1Blocked && dir2Blocked)
				{
					return false;
				}// end if				
				
			}// end else
			
			return true;
			
		}// end public function isInLineOfSightPlayer():Boolean	
		
		public function isInLineOfSightPlayer1():Boolean
		{
			return isInLineOfSightPlayer(Game.g.m_player1);
		}// end public function isInLineOfSightPlayer1():Boolean			
		
		public function isInLineOfSightPlayer2():Boolean
		{
			return isInLineOfSightPlayer(Game.g.m_player2);
		}// end public function isInLineOfSightPlayer2():Boolean			
	
		public function OutOfLineOfSightPlayer1():void
		{
			
		}// end public function OutOfLineOfSightPlayer1():void		
		
		public function OutOfLineOfSightPlayer2():void
		{
			
		}// end public function OutOfLineOfSightPlayer2():void		
		
		public function OnLineOfSightPlayer1():void
		{
			
		}// end public function OnLineOfSightPlayer1():void		
		
		public function OnLineOfSightPlayer2():void
		{
			
		}// end public function OnLineOfSightPlayer2():void	
		
		public function whileOnLineOfSightPlayer1():void
		{
			
		}// end public function whileOnLineOfSightPlayer1():void		
		
		public function whileOnLineOfSightPlayer2():void
		{
			
		}// end public function whileOnLineOfSightPlayer2():void			
		
		public function stopSmartPlayerFollow():void
		{
			
			if (m_smartFollowing)
			{
				m_smartFollowing = false;
				m_checkDirStartTime = Game.g.timer;	
				onStopSmartFollowing();
			}// end if
			
		}// end public function stopSmartPlayerFollow():void			
		
		public function startSmartPlayerFollow(playerNumber_:uint):void
		{
			
			if (m_canSmartFollow)
			{
				
				var distFromTarget:uint;
				
				if (playerNumber_ == 1)
				{
					distFromTarget = m_player1PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);	
				}
				else
				{
					distFromTarget = m_player2PathMap.getDistFromTarget(m_fromTileX, m_fromTileY);
				}// end else
				
				if (distFromTarget == PathMap.TILE_BLOCKED)
				{
					return;
				}// end if
				
				if (!m_smartFollowing)
				{
					startAlert();
				}// end if
				//m_alerted.flash();
				m_smartFollowingPlayerNumber = playerNumber_;
				m_smartFollowing = true;
				m_smartFollowingState = STATE_SMART_FOLLOWING_NORMAL;	
				m_checkDirSmartFollowingStartTime = Game.g.timer;
				m_isSeparatingFromEnemy = false;
			
			}// end if
			
		}// end public function startSmartPlayerFollow(playerNumber_:uint):void	
		
		public function startSmartPlayerFollowCountdown():void
		{
		
			if (m_canSmartFollow)
			{			
			
				//trace("ENEMY starting smart follow countdown");
				m_smartFollowCountdownStartTime = Game.g.timer;
				m_smartFollowingState = STATE_SMART_FOLLOWING_COUNTDOWN;
			
			}// end if
			
		}// end public function startSmartPlayerFollowCountdown():void						
		
		public override function onDamage(vel_:Vector3D = null):void
		{
			super.onDamage(vel_);
			
			if (!m_reactingToHit)
			{
				Game.g.m_gameObjectList.push(new EnemyHit(m_cTX, m_cTY));
				m_reactingToHit = true;
				m_reactingToHitStartTime = Game.g.timer;
			}// end if
			
		}// end public override function onDamage(vel_:Vector3D = null):void					
		
		public function getNextCorridorDir(dir1_:uint, dir2_:uint):uint
		{
			if (m_corridorDirTable[dir1_][dir2_] == CW)
			{
				return getNextCorridorDirCW(dir2_);
			}
			else // m_corridorDirTable[dir1_][dir2_] == CCW
			{
				return getNextCorridorDirCCW(dir2_);
			}// end else
		}// end public function getNextCorridorDir(dir1_:uint, dir2_:uint):uint		
		
		public function playSideWalkAnim():void
		{
			gotoAndPlay("walk_side");
		}// end public function playSideWalkAnim():void
		
		public function playUpWalkAnim():void
		{
			gotoAndPlay("walk_up");
		}// end public function playUpWalkAnim():void		
		
		public function playDownWalkAnim():void
		{
			gotoAndPlay("walk_down");
		}// end public function playDownWalkAnim():void				
		
		public function updateAnim():void
		{
				
			if (m_prevVel.x < 0)
			{
				if ((m_currAnim != ANIM_WALK_LEFT) || m_forceUpdateAnim)
				{				
					m_forceUpdateAnim = false;
					playSideWalkAnim();
					m_currAnim = ANIM_WALK_LEFT;
					flipX(false);
				}// end if
			}
			else if (m_prevVel.x > 0)
			{
				if ((m_currAnim != ANIM_WALK_RIGHT) || m_forceUpdateAnim)
				{					
					m_forceUpdateAnim = false;
					playSideWalkAnim();
					m_currAnim = ANIM_WALK_RIGHT;
					flipX(true);
				}// end if
			}
			else if (m_prevVel.y < 0)
			{
				if ((m_currAnim != ANIM_WALK_UP) || m_forceUpdateAnim)
				{				
					m_forceUpdateAnim = false;
					playUpWalkAnim();
					m_currAnim = ANIM_WALK_UP;
				}// end if
			}
			else if (m_prevVel.y > 0)
			{
				if ((m_currAnim != ANIM_WALK_DOWN) || m_forceUpdateAnim)
				{					
					m_forceUpdateAnim = false;
					playDownWalkAnim();
					m_currAnim = ANIM_WALK_DOWN;
				}// end if
			}// end else if		
			
		}// end public function updateAnim():void				
		
		public function getNextCorridorDirCCW(dir_:uint):uint
		{
			if (dir_ == DIR_LEFT)
			{
				return DIR_DOWN;
			}
			else if (dir_ == DIR_DOWN)
			{
				return DIR_RIGHT;
			}
			else if (dir_ == DIR_RIGHT)
			{
				return DIR_UP;
			}
			else // dir == DIR_UP
			{
				return DIR_LEFT;
			}// end else
			
		}// end public function getNextCorridorDirCCW(dir_:uint):uint
		
		public function getOppositeDir(dir_:uint):uint
		{
			if (dir_ == DIR_LEFT)
			{
				return DIR_RIGHT;
			}
			else if (dir_ == DIR_DOWN)
			{
				return DIR_UP;
			}
			else if (dir_ == DIR_RIGHT)
			{
				return DIR_LEFT;
			}
			else // dir == DIR_UP
			{
				return DIR_DOWN;
			}// end else
			
		}// end public function getOppositeDir(dir_:uint):uint		
		
		public function getNextCorridorDirCW(dir_:uint):uint
		{
			if (dir_ == DIR_LEFT)
			{
				return DIR_UP;
			}
			else if (dir_ == DIR_UP)
			{
				return DIR_RIGHT;
			}
			else if (dir_ == DIR_RIGHT)
			{
				return DIR_DOWN;
			}
			else // dir == DIR_DOWN
			{
				return DIR_LEFT;
			}// end else
			
		}// end public function getNextCorridorDirCW(dir_:uint):uint		
		
		public function chooseRandomCorridorDir(fromTileX_:int, fromTileY_:int):uint
		{
		
			var dirTable:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(4);
			var corridorDirTable:Vector.<Vector.<int>> = new Vector.<Vector.<int>>(4);

			var canGoDir1:Boolean;
			var canGoDir2:Boolean
			
			corridorDirTable[0] = Vector.<int>([-1, 0]);
			corridorDirTable[1] = Vector.<int>([1, 0]);
			corridorDirTable[2] = Vector.<int>([0, -1]);
			corridorDirTable[3] = Vector.<int>([0, 1]);				
			
			setDirTable(m_prevRandVel, dirTable);
			
			if (!m_vel.x && !m_vel.y)
			{
				
			
				m_goingStraightTryingToTurn = false;
				m_goingStraightStartTime = Game.g.timer;
				m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));								
				
				m_waitingForCornerStop = false;
				
				canGoDir1 = m_tileMap.canGoToTile(fromTileX_ + dirTable[2][0], fromTileY_ + dirTable[2][1]);
				canGoDir2 = m_tileMap.canGoToTile(fromTileX_ + dirTable[3][0], fromTileY_ + dirTable[3][1]);
				
				if (!canGoDir1 && canGoDir2)
				{
					m_prevRandVel.x = dirTable[3][0];
					m_prevRandVel.y = dirTable[3][1];	
					m_qCorridorDir = dirTable[0][2];
					return dirTable[3][2];					
				}
				else if (!canGoDir2 && canGoDir1)
				{
					m_prevRandVel.x = dirTable[2][0];
					m_prevRandVel.y = dirTable[2][1];	
					m_qCorridorDir = dirTable[0][2];
					return dirTable[2][2];					
				}
				else if(!canGoDir1 && !canGoDir2)
				{
					
					m_prevRandVel.x = dirTable[1][0];
					m_prevRandVel.y = dirTable[1][1];	
					
					if (Math.floor(Math.random() * 2) == 0)
					{
						m_qCorridorDir = dirTable[2][2];
					}
					else
					{
						m_qCorridorDir = dirTable[3][2];
					}// end else
					
					//trace("TURNING BACK");
					return dirTable[1][2];										
					
				}
				else
				{
				
					if (Math.floor(Math.random() * 2) == 0)
					{
						m_prevRandVel.x = dirTable[2][0];
						m_prevRandVel.y = dirTable[2][1];	
						m_qCorridorDir = dirTable[0][2];
						return dirTable[2][2];						
					}
					else
					{
						m_prevRandVel.x = dirTable[3][0];
						m_prevRandVel.y = dirTable[3][1];	
						m_qCorridorDir = dirTable[0][2];
						return dirTable[3][2];						
					}// end else
					
				}// end else
								
			}
			else if((m_qCorridorDir != DIR_NONE) && m_tileMap.canGoToTile(fromTileX_ + corridorDirTable[m_qCorridorDir-1][0], fromTileY_ + corridorDirTable[m_qCorridorDir-1][1]) && !m_waitingForCornerStop)
			{
			
				m_goingStraightTryingToTurn = false;
				m_goingStraightStartTime = Game.g.timer;
				m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));														
				
				m_numTurnTimes++;
				
				if (m_numTurnTimes > m_maxTurnTimes)
				{
					m_numTurnTimes = 0;
					m_maxTurnTimes = MIN_TURN_TIMES + Math.floor(Math.random() * (MAX_TURN_TIMES - MIN_TURN_TIMES));
					m_qCorridorDir = DIR_NONE;
					return dirTable[0][2];					
				}// end if
							
				m_waitingForCornerStop = true;
				var newCorridorDir:uint = m_qCorridorDir;
				
				var projTX:int = getWrappedTileX(fromTileX_ + corridorDirTable[m_qCorridorDir - 1][0]);
				var projTY:int = getWrappedTileY(fromTileY_ + corridorDirTable[m_qCorridorDir - 1][1]);				
				
				m_qCorridorDir = getNextCorridorDir(dirTable[0][2], m_qCorridorDir);
				
				m_prevRandVel.x = corridorDirTable[newCorridorDir - 1][0];
				m_prevRandVel.y = corridorDirTable[newCorridorDir - 1][1];		
				
				var oppositeDir:uint = getOppositeDir(m_qCorridorDir);

				canGoDir2 = m_tileMap.canGoToTile(projTX + corridorDirTable[oppositeDir - 1][0], projTY + corridorDirTable[oppositeDir - 1][1]);				
				
				if (!canGoDir2 && (Math.floor(Math.random() * 2) == 0))
				{
					m_qCorridorDir = oppositeDir;	
				}// end if
				
				return newCorridorDir;
				
			}
			else
			{
				
				if (!m_goingStraightTryingToTurn)
				{
			
					if ((m_qCorridorDir != DIR_NONE) && !m_tileMap.canGoToTile(fromTileX_ + corridorDirTable[m_qCorridorDir - 1][0], fromTileY_ + corridorDirTable[m_qCorridorDir - 1][1]) && m_waitingForCornerStop)
					{
						m_waitingForCornerStop = false;
					}// end if				
					
					if (Game.g.timer - m_goingStraightStartTime > m_goingStraightDuration)
					{
						m_goingStraightTryingToTurn = true;
						m_qCorridorDir = DIR_NONE;
					}// end if
					
				}
				else
				{
					
					canGoDir1 = m_tileMap.canGoToTile(fromTileX_ + dirTable[2][0], fromTileY_ + dirTable[2][1]);
					canGoDir2 = m_tileMap.canGoToTile(fromTileX_ + dirTable[3][0], fromTileY_ + dirTable[3][1]);
					
					if (canGoDir1 && !canGoDir2)
					{
						m_goingStraightTryingToTurn = false;
						m_goingStraightStartTime = Game.g.timer;
						m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));																				
						m_prevRandVel.x = dirTable[2][0];
						m_prevRandVel.y = dirTable[2][1];	
						return dirTable[2][2];
					}
					else if (!canGoDir1 && canGoDir2)
					{
						m_goingStraightTryingToTurn = false;
						m_goingStraightStartTime = Game.g.timer;
						m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));										
						m_prevRandVel.x = dirTable[3][0];
						m_prevRandVel.y = dirTable[3][1];
						return dirTable[3][2];
					}
					else if (canGoDir1 && canGoDir2)
					{
						
						if (Math.floor(Math.random() * 2) == 0)
						{
							m_goingStraightTryingToTurn = false;
							m_goingStraightStartTime = Game.g.timer;
							m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));											
							m_prevRandVel.x = dirTable[2][0];
							m_prevRandVel.y = dirTable[2][1];	
							return dirTable[2][2];						
						}
						else
						{
							m_goingStraightTryingToTurn = false;
							m_goingStraightStartTime = Game.g.timer;
							m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));											
							m_prevRandVel.x = dirTable[3][0];
							m_prevRandVel.y = dirTable[3][1];	
							return dirTable[3][2];						
						}// end else
						
					}// end else if
					
				}// end else
				

				
				return dirTable[0][2];
				
			}// end else
			
		}// end public function chooseRandomCorridorDir():uint			
		
		public function setDirTable(chosenVel_:Vector3D, dirTable_:Vector.<Vector.<int>>):void
		{
		
			if (chosenVel_.x < 0)
			{
				dirTable_[0] = Vector.<int>([-1, 0, DIR_LEFT]);
				dirTable_[1] = Vector.<int>([1, 0,  DIR_RIGHT]);
				dirTable_[2] = Vector.<int>([0, -1, DIR_UP]);
				dirTable_[3] = Vector.<int>([0, 1,  DIR_DOWN]);									
			}
			else if (chosenVel_.x > 0)
			{
				dirTable_[0] = Vector.<int>([1, 0,  DIR_RIGHT]);
				dirTable_[1] = Vector.<int>([-1, 0, DIR_LEFT]);
				dirTable_[2] = Vector.<int>([0, 1,  DIR_DOWN]);
				dirTable_[3] = Vector.<int>([0, -1, DIR_UP]);					
			}
			else if (chosenVel_.y < 0)
			{
				dirTable_[0] = Vector.<int>([0, -1, DIR_UP]);
				dirTable_[1] = Vector.<int>([0, 1,  DIR_DOWN]);
				dirTable_[2] = Vector.<int>([-1, 0, DIR_LEFT]);
				dirTable_[3] = Vector.<int>([1, 0,  DIR_RIGHT]);				
			}
			else if (chosenVel_.y >= 0)
			{
				dirTable_[0] = Vector.<int>([0, 1,  DIR_DOWN]);
				dirTable_[1] = Vector.<int>([0, -1, DIR_UP]);
				dirTable_[2] = Vector.<int>([1, 0,  DIR_RIGHT]);
				dirTable_[3] = Vector.<int>([-1, 0, DIR_LEFT]);					
			}// end else if			
			
		}// end public function setDirTable(chosenVel_:Vector3D, dirTable_:Vector.<Vector.<int>>):void			
		
		public function get respawnEffectType():Class
		{
			return null;
		}// end public function get respawnEffectType():Class
		
		public function updateDamage():void
		{
		
			if (!Game.g.m_player1.m_terminated && collidesWithObject(Game.g.m_player1))
			{
				Game.g.m_player1.doDamage(m_damage, m_vel);
			}// end if
			
			if (!Game.g.m_player2.m_terminated && collidesWithObject(Game.g.m_player2))
			{
				Game.g.m_player2.doDamage(m_damage, m_vel);
			}// end if			
					
		}// end public function updateDamage():void			
		

		public function OnTargetInaccessible():void
		{
		
			
		}// end public function OnTargetInaccessible():void		
		
		public function updateLifeTime():void
		{
		
			if (m_hasLifetime && !m_lifeTimeEnded && ((Game.g.timer - m_lifeStartTime) > m_lifetime))
			{
				m_lifeTimeEnded = true;
				onLifetimeEnded();
			}// end if		
					
		}// end public function updateLifeTime():void				
			
		public function updateReactionToHit():void
		{
			if (m_reactingToHit && (Game.g.timer - m_reactingToHitStartTime > REACTING_TO_HIT_DURATION))
			{			
				m_reactingToHit = false;
			}// end if			
		}// end public function updateReactionToHit():void
		
		public override function doPhysics():void
		{
			
			m_skipAnim = false;
			
			updateReactionToHit();			
			
			if (m_isAlerted || !m_canMove)
			{
				updateDamage();
				return;
			}// end if
						
			updateMap();
			
			updateTarget();
						
			
			var qDir:uint;
			
			switch(m_state)
			{
				
				case STATE_FOLLOWING:
				{
					
					if (!m_followsTarget && !m_smartFollowing && !m_evadesTarget)
					{
						
						/*if ((this is EntityJellyBaby) && ((this as EntityJellyBaby).m_rejoining))
						{
							trace("switch to STATE_PATROLLING_CORRIDORS");
						}// end if*/
						
						m_state = STATE_PATROLLING_CORRIDORS;
					}
					else
					{
						
						var distFactor:Number;
						var lastQDir:uint;
						
						if ((!m_smartFollowing)  && (Game.g.timer - m_checkDirStartTime > m_checkDirTimeout))
						{
						
							
							m_checkDirStartTime = Game.g.timer;						
							
							if (m_hasDecreasingTimeout)
							{
							
								if (m_distFromTarget > m_maxDistFactor)
								{
									m_distFromTarget = m_maxDistFactor;
								}// end if
								
								distFactor = m_distFromTarget / m_maxDistFactor;
								m_checkDirTimeout = m_checkDirMaxTimeout * distFactor;
							
							}// end if
												
							lastQDir = m_qDir;
							
							if (!m_applyOverrideDir)
							{
							
								if (m_followsTarget)
								{
									m_qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
								}
								else if (m_evadesTarget)
								{
									m_qDir = m_chosenPathMap.getNextEvasionDir(m_fromTileX, m_fromTileY);						
								}// end else if
								
							}
							else
							{
								m_applyOverrideDir = false;
								m_qDir = m_overrideDir;
							}// end else
													
							if (m_hasBackAndForthDecPerc)
							{
							
								m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);
								if (m_checkDirTimeout < MIN_CHECK_DIR_TIMEOUT)
								{
									m_checkDirTimeout = MIN_CHECK_DIR_TIMEOUT;
								}// end if	
							
							}// end if
						
						}
						else if ((m_smartFollowing) && (Game.g.timer - m_checkDirSmartFollowingStartTime > m_checkDirSmartFollowTimeout))
						{
							
							m_checkDirSmartFollowingStartTime = Game.g.timer;
							
							if (m_distFromTarget > m_maxDistFactor)
							{
								m_distFromTarget = m_maxDistFactor;
							}// end if
							
							distFactor = m_distFromTarget / m_maxDistFactor;
							m_checkDirTimeout = m_checkDirMaxTimeout * distFactor;
									
							lastQDir = m_qDir;
							
							if (!m_applyOverrideDir)
							{
								m_qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
							}
							else
							{
								m_applyOverrideDir = false;
								m_qDir = m_overrideDir;
							}// end else
													
							m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);
							if (m_checkDirTimeout < 0)
							{
								m_checkDirTimeout = 0;
							}// end if				
							
							if ((m_smartFollowingState == STATE_SMART_FOLLOWING_COUNTDOWN) && (Game.g.timer - m_smartFollowCountdownStartTime > SMART_FOLLOW_DURATION))
							{
								//trace("ENEMY stopped smart following in countdown");
								onStopSmartFollowing();
								m_smartFollowing = false;
								m_checkDirStartTime = Game.g.timer;
							}// end if
							
						}// end else if
						
						var cantReachTarget:Boolean = (m_qDir == DIR_NONE) && (m_distFromTarget > 1);
						var isOtherEnemyInFollowRouteRes:Boolean = isOtherEnemyInFollowRoute();
						
						if (cantReachTarget || (!m_smartFollowing && isOtherEnemyInFollowRouteRes))
						{
							
							//trace("patrolling with smartFollow = " + m_smartFollowing + ", and target dist = " + m_distFromTarget);
							if (cantReachTarget)
							{
								OnTargetInaccessible();
							}// end if
							
							if (isOtherEnemyInFollowRouteRes)
							{
								m_separatingFromEnemyStartTime = Game.g.timer;
								m_isSeparatingFromEnemy = true;
								//trace("SEPARATING");
								m_separatingFromEnemyDuration = SEPARATING_FROM_ENEMY_MIN_DURATION + Math.random() * (SEPARATING_FROM_ENEMY_MAX_DURATION - SEPARATING_FROM_ENEMY_MIN_DURATION);
							}// end if
							
							m_prevRandVel = m_vel.clone();
							m_prevRandVel.normalize();
							m_state = STATE_PATROLLING_CORRIDORS;
							
							/*if ((this is EntityJellyBaby) && ((this as EntityJellyBaby).m_rejoining))
							{
								trace("dir is NONE, switch to STATE_PATROLLING_CORRIDORS");
							}// end if*/							
							
							m_qCorridorDir = DIR_NONE;
							m_goingStraightTryingToTurn = false;
							m_goingStraightStartTime = Game.g.timer;
							m_goingStraightDuration = GOING_STRAIGHT_MIN_DURATION + Math.floor(Math.random() * (GOING_STRAIGHT_MAX_DURATION - GOING_STRAIGHT_MIN_DURATION));						
							m_numTurnTimes = 0;
							m_maxTurnTimes = MIN_TURN_TIMES + Math.floor(Math.random() * (MAX_TURN_TIMES - MIN_TURN_TIMES));			
							m_checkDirStartTime = Game.g.timer;	
							m_checkDirTimeout = m_checkDirMaxTimeout;
								
						}// end if
					}// end if
					break;
				}// end case
							
				
				case STATE_PATROLLING_CORRIDORS:
				{
					

					if (m_isSeparatingFromEnemy && (Game.g.timer - m_separatingFromEnemyStartTime > m_separatingFromEnemyDuration))
					{
						m_isSeparatingFromEnemy = false;
						//trace("STOPPED SEPARATING");
					}// end if
					
					
					if (Game.g.timer - m_checkDirStartTime > m_checkDirTimeout)
					{
					
						m_checkDirStartTime = Game.g.timer;	
						
						/*if (m_followsTarget)
						{
							qDir = chosenPathMap.getNextGoalDir(fromTileX, fromTileY);
						}*/
						if (m_evadesTarget)
						{
							qDir = m_chosenPathMap.getNextEvasionDir(m_fromTileX, m_fromTileY);
						}
						else
						{
							qDir = m_chosenPathMap.getNextGoalDir(m_fromTileX, m_fromTileY);
						}// end else
						
						if ((qDir != DIR_NONE) && (((m_followsTarget || m_evadesTarget) && !m_isSeparatingFromEnemy) || m_smartFollowing))
						{
							//trace("stopped patrolling");
							m_state = STATE_FOLLOWING;
							m_isSeparatingFromEnemy = false;
							m_checkDirTimeout = m_checkDirMaxTimeout;
							
							if (!m_applyOverrideDir)
							{
								m_qDir = qDir;
							}
							else
							{
								m_applyOverrideDir = false;
								m_qDir = m_overrideDir;
							}// end else
						}
						else
						{			
				
							m_smartFollowing = false;
							
							if (!m_applyOverrideDir)
							{							
								m_qDir = chooseRandomCorridorDir(m_fromTileX, m_fromTileY);	
							}
							else
							{
								m_applyOverrideDir = false;
								m_qDir = m_overrideDir;								
							}// end elese
						}// end else
						
						if (m_hasBackAndForthDecPerc)
						{
						
							m_checkDirTimeout -= m_checkDirTimeout * (m_backForthDecPerc * m_backForthTimes);
							if (m_checkDirTimeout < MIN_CHECK_DIR_TIMEOUT)
							{
								m_checkDirTimeout = MIN_CHECK_DIR_TIMEOUT;
							}// end if	
							
							if (!m_backForthTimes)
							{
								m_checkDirTimeout = m_checkDirMaxTimeout;
							}// end if
						
						}// end if						
					
					}// end if
					
					break;
				}// end case				
				
				
			}// end switch
	
			if (m_qDir != DIR_NONE)
			{
				m_lastDir = m_qDir;
			}// end if				
			
			if (!m_skipAnim && ((((m_prevVel.x != m_vel.x) || (m_prevVel.y != m_vel.y)) && (Game.g.timer - m_velChangeStartTime > VEL_CHANGE_TIMEOUT)) || m_forceUpdateAnim))
			{
				
				// check if the enemy has turned back on itself and if so, decrease
				// the timeout for the next check
				
				if (((m_vel.x > 0) && (m_prevNonZeroVel.x < 0)) || 
					((m_vel.x < 0) && (m_prevNonZeroVel.x > 0)) ||
					((m_vel.y < 0) && (m_prevNonZeroVel.y > 0)) ||
					((m_vel.y > 0) && (m_prevNonZeroVel.y < 0)))
				{
					m_backForthTimes++;
				}
				else if(m_vel.x || m_vel.y)
				{
					m_backForthTimes = 0;
				}// end else				
				
				m_velChangeStartTime = Game.g.timer;
				m_prevVel.x = m_vel.x;
				m_prevVel.y = m_vel.y;
				
				if (m_vel.x || m_vel.y)
				{
					m_prevNonZeroVel.x = m_vel.x;
					m_prevNonZeroVel.y = m_vel.y;						
				}// end if
								
				updateAnim();
				
			}// end if				
			
			updateTargetRange();
			
			updateLifeTime();
			
			/*if (m_hasLifetime && (Game.g.timer - m_lifeStartTime) > m_lifetime)
			{
				onLifetimeEnded();
			}// end if*/
			
			updateDamage();
															
			super.doPhysics();
			
			if (m_checksLineOfSight)
			{
			
				if (Game.g.m_player1 && !Game.g.m_player1.m_terminated)
				{
					
					var player1OnSightCheck:Boolean = isInLineOfSightPlayer1();
					
					if (!m_player1OnLineOfSight && player1OnSightCheck)
					{
						m_player1OnLineOfSight = true;
						OnLineOfSightPlayer1();
					}
					else if(m_player1OnLineOfSight)
					{
						if (!player1OnSightCheck)
						{
							m_player1OnLineOfSight = false;
							OutOfLineOfSightPlayer1();	
						}
						else
						{
							whileOnLineOfSightPlayer1();
						}// end else
					}// end else
				}// end if
				
				if (Game.g.m_player2 && !Game.g.m_player2.m_terminated)
				{
					
					var player2OnSightCheck:Boolean = isInLineOfSightPlayer2();
					
					if (!m_player2OnLineOfSight && player2OnSightCheck)
					{
						m_player2OnLineOfSight = true;
						OnLineOfSightPlayer2();
					}
					else if(m_player2OnLineOfSight)
					{
						if (!player2OnSightCheck)
						{
							m_player2OnLineOfSight = false;
							OutOfLineOfSightPlayer2();
						}
						else
						{
							whileOnLineOfSightPlayer2();
						}// end else
					}// end else					
					
				}// end if			
			
			}// end if		
			
			updateAbsCollisionRect();
			
		}// end public override function doPhysics():void				
		
	}// end public class EntityEnemy
	
}// end package