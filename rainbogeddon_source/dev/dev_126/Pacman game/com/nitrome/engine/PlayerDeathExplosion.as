package com.nitrome.engine
{
	
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.utils.getTimer;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import fl.motion.Color;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class PlayerDeathExplosion extends GameObject
	{
				
		public static const MAX_SIZE:uint = 15;
		public static const STATE_EXPLODING:uint = 0;
		
		public static const EXPANSION_WAIT:int   = 60;
		
		public  var m_color:uint;
		private var m_parent:Entity;
		private var m_currSize:int;
		private var m_state:uint;
		private var m_explosionProgressStartTime:int;
		private static var m_ringClip:MovieClip;		
		private static var m_ringBitmapList:Vector.<BitmapData>;
		private static var m_ringBitmapBoundsList:Vector.<Rectangle>;	
		
		public function PlayerDeathExplosion(parent_:Entity)
		{
		
			super(parent_.m_cTX, parent_.m_cTY);		
			m_parent = parent_;
			m_color = 0xffffffff;
			
			m_drawBitmap = Game.g.m_respawnExplosionBitmap;
			
			if (!m_ringBitmapList)
			{
				
				var alphaTable:Vector.<Number> = new Vector.<Number>(MAX_SIZE);
				
				var color:uint = m_color & 0x00ffffff;
				var currAlpha:uint;
				var currLighterAlpha:uint;
				
				alphaTable[0] = 1.0;
				alphaTable[1] = 1.0;
				alphaTable[2] = 1.0;
				alphaTable[3] = 1.0;
				alphaTable[4] = 0.85;
				alphaTable[5] = 0.85;
				alphaTable[6] = 0.65;
				alphaTable[7] = 0.65;
				alphaTable[8] = 0.45;
				alphaTable[9] = 0.45;
				alphaTable[10] = 0.35;
				alphaTable[11] = 0.35;
				alphaTable[12] = 0.25;		
				alphaTable[13] = 0.25;
				alphaTable[14] = 0.15;					
				
				m_ringBitmapList = new Vector.<BitmapData>;
				m_ringBitmapBoundsList = new Vector.<Rectangle>;
				
				for (var currSize:uint = 0; currSize < MAX_SIZE; currSize++)
				{
				
					currAlpha = (0xff * alphaTable[currSize]);
					currLighterAlpha = (0xff * alphaTable[currSize] * 0.5);
					
					m_ringClip = new (getDefinitionByName("Ring" + currSize) as Class)();
					// width and height need to be an odd number	
					m_ringBitmapList.push(new BitmapData(m_ringClip.width, m_ringClip.height, true, 0));
					m_ringBitmapBoundsList.push(m_ringClip.getBounds(m_ringClip));
					
					var matrix:Matrix = new Matrix();
					
					matrix.identity();
					matrix.translate(-m_ringBitmapBoundsList[m_ringBitmapBoundsList.length-1].x, -m_ringBitmapBoundsList[m_ringBitmapBoundsList.length-1].y);
					m_ringBitmapList[m_ringBitmapList.length - 1].draw(m_ringClip, matrix);
					//var tempVec:Vector.<uint> = m_ringBitmapList[m_ringBitmapList.length - 1].getVector(m_ringBitmapList[m_ringBitmapList.length - 1].rect);
					m_ringBitmapList[m_ringBitmapList.length - 1].threshold(m_ringBitmapList[m_ringBitmapList.length - 1], m_ringBitmapList[m_ringBitmapList.length - 1].rect, new Point(0, 0), "==", 0xff000000, (currAlpha << 24) + color, 0xffffffff);	
					m_ringBitmapList[m_ringBitmapList.length - 1].threshold(m_ringBitmapList[m_ringBitmapList.length - 1], m_ringBitmapList[m_ringBitmapList.length - 1].rect, new Point(0, 0), "==", 0xb3000000, (currLighterAlpha << 24) + color, 0xffffffff);
					
					
				}// end for
				
			}// end if			
			
			m_currSize = 0;
			m_state = STATE_EXPLODING;
			m_explosionProgressStartTime = Game.g.timer;			
			
		}// end public function PlayerDeathExplosion(parent_:Entity)
				
		public static function free():void		
		{
			m_ringClip = null;
			m_ringBitmapList = null;
			m_ringBitmapBoundsList = null;
		}// end public static function free():void	
		
		public override function doPhysics():void
		{
		
			if (m_terminated)
			{
				return;
			}// end if
			
			switch(m_state)
			{
				
				case STATE_EXPLODING:
				{
					
					if (Game.g.timer - m_explosionProgressStartTime > EXPANSION_WAIT)
					{
						m_explosionProgressStartTime = Game.g.timer;
						m_currSize++;
						
						if (m_currSize >= MAX_SIZE)
						{
							m_currSize = MAX_SIZE-1;
							m_terminated = true;
							Game.g.onPlayerDead();
						}// end if
						
					}// end if
					
					
					break;
				}// end case
				
					
			}// end switch
			
									
		}// end public override function doPhysics():void			
				
		
		public function terminate():void
		{
			
			m_terminated = true;
									
		}// end public function terminate():void			
		
		public override function draw():void
		{
			
			var destPoint:Point = new Point(m_tx + m_ringBitmapBoundsList[m_currSize].x, m_ty + m_ringBitmapBoundsList[m_currSize].y);
			m_drawBitmap.copyPixels(m_ringBitmapList[m_currSize], m_ringBitmapList[m_currSize].rect, destPoint, null, null, true);	
			
		}// end public override function draw():void
		
	}// end public class PlayerDeathExplosion
	
}// end package