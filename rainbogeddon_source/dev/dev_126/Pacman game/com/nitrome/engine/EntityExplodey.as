package com.nitrome.engine
{
	
	import com.nitrome.engine.pathfinding.RadialPathMap;
	import flash.display.BitmapData;
	import com.nitrome.engine.pathfinding.PathMap;
	import com.nitrome.sound.SoundManager;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;	
	import com.nitrome.geom.PointUint;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;	
	
	public class EntityExplodey extends EntityEnemy 
	{
	
		public static const STATE_WAITING_T0_EXPLODE:uint = STATE_PATROLLING_CORRIDORS+1;			

	
		private var m_explodeDeath:Boolean;
		public var m_expandLayer:Boolean;
		public var m_stoppedClips:Boolean;
		
		public function EntityExplodey(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = 1.5;
			m_glow.m_color = 0xffff00e4;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 500;
			m_checkDirTimeout = m_checkDirMaxTimeout;			
			m_followsTarget = false;
			m_targetTriggerRange = 4;
			m_hasOnTargetRangeTrigger = true;	
			m_explodeDeath = false;
			m_showDeathExplosion = false;			
			
		}// end public function EntityExplodey(tx_:uint, ty_:uint)
			
		public override function onStoppedAtTile():void
		{
					
			flipX(false);
					
			if (m_explodeDeath)
			{
				gotoAndPlay("explode_fast");
			}
			else
			{
				m_alertEffect = new EnemyAlertEffect(this);
				Game.g.m_gameObjectList.push(m_alertEffect);					
				gotoAndPlay("explode_normal");
				SoundManager.playSound("EnemyAlerted");
			}// end else
			
		}// end public override function onStoppedAtTile():void				
		
		public override function onTargetInRange(target_:Entity):void
		{
			
			m_hasOnTargetRangeTrigger = false;
			m_state = STATE_WAITING_T0_EXPLODE;
			m_qDir = DIR_NONE;
			m_canSmartFollow = false;			
			stopAtNextTile();
		
		}// end public override function onTargetInRange(target_:Entity):void	
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
  			m_state = STATE_WAITING_T0_EXPLODE;
			m_qDir = DIR_NONE;
			m_canSmartFollow = false;			
			m_explodeDeath = true;
			stopAtNextTile();		
					
		}// end public override function onDeath(attacker_:Entity = null):void			
		
		
		public function explode():void
		{
				
			m_terminated = true;
			super.onDeath();
			
			var bomb:EntityExplodeyBomb = new EntityExplodeyBomb(m_cTX, m_cTY);
			Game.g.m_gameObjectList.push(bomb);
				
		}// end public function explode():void			
		
		public function expandLayer():void
		{
			m_expandLayer = true;
		}// end public function expandLayer():void				
						
		public override function onGamePaused():void
		{
			super.onGamePaused();
			stop();
			var child:DisplayObject;
			//if (m_state != STATE_EXPLOSION_FADING)
			//{
				for (var currChild:uint = 0; currChild < numChildren; currChild++)
				{
					child = getChildAt(currChild);
					if (child is MovieClip)
					{
						(child as MovieClip).stop();
					}// end if
				}// end for
			//}// end if
		}// end public override function onGamePaused():void		
		
		public override function onGameUnpaused():void
		{
			super.onGameUnpaused();		
			play();
			var child:DisplayObject;
			//if (m_state != STATE_EXPLOSION_FADING)
			//{			
				for (var currChild:uint = 0; currChild < numChildren; currChild++)
				{
					child = getChildAt(currChild);
					if (child is MovieClip)
					{
						(child as MovieClip).play();
					}// end if
				}// end for		
			//}// end if
		}// end public override function onGameUnpaused():void			
		
	}// end public class EntityExplodey
	
}// end package