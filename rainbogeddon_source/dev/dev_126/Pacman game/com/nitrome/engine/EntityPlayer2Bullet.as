package com.nitrome.engine
{
	
	
	public class EntityPlayer2Bullet extends EntityBullet
	{
			
		public function EntityPlayer2Bullet(velX_:Number, velY_:Number, powerLevel_:uint)
		{
			super(velX_, velY_, powerLevel_);
		}// end public function EntityPlayer2Bullet(velX_:Number, velY_:Number, powerLevel_:uint)	
		
		public override function get fireSoundID():String
		{
			return "Shoot";
		}// end public override function get fireSoundID():String
		
		public override function get effect():Class
		{
			return Player2BulletImpactEffect;
		}// end public override function get effect():Class				
		
	}// end public class EntityPlayer2Bullet
	
}// end package