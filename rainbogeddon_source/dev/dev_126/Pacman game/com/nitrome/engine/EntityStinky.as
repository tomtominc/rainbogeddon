package com.nitrome.engine
{
	
	//import com.nitrome.engine.pathfinding.PathMap;
	
	public class EntityStinky extends EntityEnemy 
	{
			

		public static const NORMAL_DURATION:int = 10000;
		
		
		public  var m_behaviorStartTime:int;		
		private var m_isPooping:Boolean;
		private var m_poopingStarted:Boolean;
		
		public function EntityStinky(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
		
			
			//m_speed = 1.0;
			m_glow.m_color = 0xffff9000;
			m_glow.ShiftColor(0xffffff00, 0.1);  // 0xff996f14
			m_checkDirMaxTimeout = 700;
			m_checkDirTimeout = m_checkDirMaxTimeout;			
			m_behaviorStartTime = Game.g.timer;
			m_isPooping = false;
			m_poopingStarted = false;
			
		}// end public function EntityStinky(tx_:uint, ty_:uint)			
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			
			if (!m_isPooping)
			{
				super.doPhysics();
				
				if (!m_poopingStarted && (Game.g.timer - m_behaviorStartTime > NORMAL_DURATION))
				{
					m_poopingStarted = true;
					//trace("signal center")
					signalPosOnCenterNextTile();
					//m_isPooping = true;
					//gotoAndPlay("pooping");
				}// end if
				
			}
			else
			{
				updateDamage();
			}// end else			
			
		}// end public override function doPhysics():void
		
		public override function onCenterNextTile():void
		{		
			if (!m_isPooping)
			{
				m_isPooping = true;
				//trace("starting poop");
				gotoAndPlay("pooping");
			}// end if				
		}// end public override function onCenterNextTile():void		
		
		public function poopingDone():void
		{
			m_isPooping = false;
			m_behaviorStartTime = Game.g.timer;
			m_poopingStarted = false;
			m_forceUpdateAnim = true;
			var poop:EntityPoop = new EntityPoop(m_cTX, m_cTY);
			Game.g.m_gameObjectList.push(poop);
			m_spatialHashMap.registerObject(poop);
		}// end public function poopingDone():void
		
		public override function startAlert():void
		{		
			super.startAlert();
			
			m_behaviorStartTime = Game.g.timer;
			m_poopingStarted = false;
			m_isPooping = false;
			m_signalPosOnCenterNextTile = false;
			
		}// end public override function startAlert():void				
		
		public override function onDeath(attacker_:Entity = null):void
		{
			super.onDeath(attacker_);
		}// end public override function onDeath(attacker_:Entity = null):void			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnOrange;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedOrange;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityStinky
	
}// end package