package com.nitrome.engine
{
	
	import com.nitrome.ui.Key;
	import flash.geom.Vector3D;
	import flash.geom.Point;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	import flash.display.BitmapData;
	
	public class Flash extends GameObject
	{
			
		public static const STATE_OFF:uint      = 0;
		public static const STATE_FLASHING:uint = 1;
				
		private var m_parent:Entity; // the parent object used as a mask when drawing
		public  var m_canFlash:Boolean;
		public  var m_state:uint;
		public  var m_mask:BitmapData;
		public  var m_thisBitmap:BitmapData;
		
		public function Flash(parent_:Entity)
		{
			super(0, 0);
			m_state = STATE_OFF;
			m_parent = parent_;
			m_drawBitmap = m_parent.m_drawBitmap;
			m_mask = new BitmapData(width, height, true, 0x0);
			m_thisBitmap = new BitmapData(width, height, true, 0x0);
		}// end public function Flash(parent_:Entity)
						
		public function flash():void
		{
			
			if (m_canFlash)
			{
				m_canFlash = false;
				m_state = STATE_FLASHING;
				gotoAndPlay("flash");
			}// end if
			
		}// end public function flash():void			
		
		public function stopFlashing():void
		{
			m_state = STATE_OFF;
			gotoAndStop("off");
		}// end public function stopFlashing():void			
		
		public override function draw():void
		{
			
			if (m_state == STATE_FLASHING)
			{
			
				m_mask.fillRect(m_mask.rect, 0x0);
				m_thisBitmap.fillRect(m_thisBitmap.rect, 0x0);
				
				// draw the parent onto the mask
				
				var parentBounds:Rectangle = m_parent.getBounds(m_parent);
				var matrix:Matrix = new Matrix();
				 
				matrix.scale(m_parent.scaleX, m_parent.scaleY);
				matrix.rotate(m_parent.rotation*(Math.PI/180));
				matrix.translate(-parentBounds.x, -parentBounds.y);				
				
				m_mask.draw(m_parent, matrix);
				
				matrix.identity();
				matrix.scale(scaleX, scaleY);
				matrix.rotate(rotation*(Math.PI/180));
				
				m_thisBitmap.draw(this, matrix);
				
				m_bounds = getBounds(this);
				
				var finalPos:Point = new Point();
				finalPos.x = m_parent.x + parentBounds.x;
				finalPos.y = m_parent.y + parentBounds.y;
				
				m_drawBitmap.copyPixels(m_thisBitmap, m_drawBitmap.rect, finalPos, m_mask, new Point(0, 0), true);
				
				// draw the wrapping parts
				
				if (!m_drawWrapped)
				{
					return;
				}// end if
				
				if (m_parent.x + (parentBounds.x + parentBounds.width) > m_drawBitmap.width)
				{		
					finalPos.x = m_parent.x+parentBounds.x - m_drawBitmap.width;
					finalPos.y = m_parent.y+parentBounds.y;					
					m_drawBitmap.copyPixels(m_thisBitmap, m_drawBitmap.rect, finalPos, m_mask, new Point(0, 0), true);
				}
				else if ((m_parent.x + parentBounds.x) < 0)
				{	
					finalPos.x = m_parent.x+parentBounds.x + m_drawBitmap.width;
					finalPos.y = m_parent.y+parentBounds.y;							
					m_drawBitmap.copyPixels(m_thisBitmap, m_drawBitmap.rect, finalPos, m_mask, new Point(0, 0), true);
				}
				else if (m_parent.y + (parentBounds.y + parentBounds.height) > m_drawBitmap.height)
				{
					finalPos.x = m_parent.x+parentBounds.x;
					finalPos.y = m_parent.y+parentBounds.y - m_drawBitmap.height;						
					m_drawBitmap.copyPixels(m_thisBitmap, m_drawBitmap.rect, finalPos, m_mask, new Point(0, 0), true);
				}
				else if ((m_parent.y + parentBounds.y) < 0)
				{
					finalPos.x = m_parent.x+parentBounds.x;
					finalPos.y = m_parent.y+parentBounds.y + m_drawBitmap.height;					
					m_drawBitmap.copyPixels(m_thisBitmap, m_drawBitmap.rect, finalPos, m_mask, new Point(0, 0), true);
				}// end else if
				
			}// end if
			
		}// end public override function draw():void					
		
	}// end public class Flash
	
}// end package