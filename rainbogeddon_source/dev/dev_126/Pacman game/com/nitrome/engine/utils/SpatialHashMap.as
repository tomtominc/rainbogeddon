package com.nitrome.engine.utils
{
	
	import com.nitrome.engine.Entity;
	import com.nitrome.engine.ExplosionParticle;
	import com.nitrome.engine.GameObject;
	
	public class SpatialHashMap
	{

		private var m_map:Object;
		private var m_sceneWidth:Number;
		private var m_cellSize:Number;
		private var m_widthFactor:Number;
		
		public function SpatialHashMap(sceneWidth_:Number, cellSize_:Number)
		{
		
			m_sceneWidth = sceneWidth_;
			m_cellSize = cellSize_;
			m_widthFactor = sceneWidth_ / cellSize_;
			clear();
			
		}// end public function SpatialHashMap(sceneWidth_:Number, cellSize_:Number)
		
		public function clear():void
		{		
			
			m_map = new Object();
			
		}// end public function clear():void
		
		public function registerObjects(objectList_:Vector.<GameObject>):void
		{		
			
			for (var currObject:uint = 0; currObject < objectList_.length; currObject++)
			{
				
				if (!(objectList_[currObject] is Entity))
				{
					continue;
				}// end if
				
				var object:Entity = objectList_[currObject] as Entity;
				
				if (!object.m_canCollide)
				{
					continue;
				}// end if				
				
				var id:uint;
				var x:Number;
				var y:Number;
				
				// get upper left corner
				x = Entity.getWrappedX(object.x + object.m_collisionRect.x + object.m_vel.x);
				y = Entity.getWrappedY(object.y + object.m_collisionRect.y + object.m_vel.y);
				
				object.m_iDUL = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);
				
				addToList(object.m_iDUL, object);
				
				
				// get upper right corner
				x = Entity.getWrappedX(object.x + object.m_collisionRect.x + object.m_collisionRect.width + object.m_vel.x);
				
				object.m_iDUR = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);
				
				addToList(object.m_iDUR, object);	
				
				
				// get lower left corner
				x = Entity.getWrappedX(object.x + object.m_collisionRect.x + object.m_vel.x);
				y = Entity.getWrappedY(object.y + object.m_collisionRect.y + object.m_collisionRect.height + object.m_vel.y);
				
				object.m_iDLL = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);
				
				addToList(object.m_iDLL, object);							
				
				
				// get lower right corner
				x = Entity.getWrappedX(object.x + object.m_collisionRect.x + object.m_collisionRect.width + object.m_vel.x);
				
				object.m_iDLR = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);
				
				addToList(object.m_iDLR, object);				
				
			}// end for
			
		}// end public function registerObjects(objectList_:Vector.<GameObject>):void	
		
		public function registerObject(object_:Entity):void
		{
			
			var list:Vector.<GameObject> = new Vector.<GameObject>;
			list.push(object_);
			registerObjects(list);
			
		}// end public function registerObject(object_:Entity):void
		
		private function addToList(id_:uint, object_:Entity):void
		{		
			
			/*if ((object_ is ExplosionParticle) && (id_ == 195))
			{
				trace("gotcha");
			}// end if*/
			if (!m_map[id_])
			{
				m_map[id_] = new Vector.<Entity>;
				m_map[id_].push(object_);
			}
			else
			{
				
				for (var currObject:uint = 0; currObject < m_map[id_].length; currObject++)
				{
					if (object_ == m_map[id_][currObject])
					{
						return;
					}// end if
				}// end for
				
				m_map[id_].push(object_);
				
			}// end else
			
		}// end private function addToList(id_:uint, object_:Entity):void	
		
		private function removeFromList(id_:uint, object_:Entity):void
		{		
			
			
			if (m_map[id_])
			{
			
				for (var currObject:uint = 0; currObject < m_map[id_].length; currObject++)
				{
					if (object_ == m_map[id_][currObject])
					{
						m_map[id_].splice(currObject, 1);
						if (!(m_map[id_].length))
						{
							/*if (object_ is ExplosionParticle)
							{
								trace("ExplosionParticle with x = " + object_.x + " and y = " + object_.y + " deleted list");
							}// end if*/							
							m_map[id_] = null;
						}// end if
						return;
					}// end if
				}// end for
			
			}// end if
			
		}// end private function removeFromList(id_:uint, object_:Entity):void			
		
		public function unregisterObject(object_:Entity):void
		{		
						
			removeFromList(object_.m_iDUL, object_);
			removeFromList(object_.m_iDUR, object_);
			removeFromList(object_.m_iDLL, object_);
			removeFromList(object_.m_iDLR, object_);
			
			object_.m_iDUL = uint.MAX_VALUE;
			object_.m_iDUR = uint.MAX_VALUE;
			object_.m_iDLL = uint.MAX_VALUE;
			object_.m_iDLR = uint.MAX_VALUE;
			
		}// end public function unregisterObject(object_:Entity):void
		
		public function updateObject(object_:Entity):void
		{					
			
			if (!object_.m_canCollide)
			{
				return;
			}// end if				
			
			var iDUL:uint;
			var x:Number;
			var y:Number;
			
			// get upper left corner
			x = Entity.getWrappedX(object_.x + object_.m_collisionRect.x + object_.m_vel.x);
			y = Entity.getWrappedY(object_.y + object_.m_collisionRect.y + object_.m_vel.y);
			
			iDUL = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);
		
			// get upper right corner
			x = Entity.getWrappedX(object_.x + object_.m_collisionRect.x + object_.m_collisionRect.width + object_.m_vel.x);
			
			var iDUR:uint = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);			
			
			// get lower left corner
			x = Entity.getWrappedX(object_.x + object_.m_collisionRect.x + object_.m_vel.x);
			y = Entity.getWrappedY(object_.y + object_.m_collisionRect.y + object_.m_collisionRect.height + object_.m_vel.y);
			
			var iDLL:uint = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);			
			
			// get lower right corner
			x = Entity.getWrappedX(object_.x + object_.m_collisionRect.x + object_.m_collisionRect.width + object_.m_vel.x);
			
			var iDLR:uint = (Math.floor(x / m_cellSize)) + ((Math.floor(y / m_cellSize)) * m_widthFactor);			
			
			/*if ((object_ is ExplosionParticle) && (object_.m_terminated == true))
			{
				trace("ExplosionParticle with x = " + object_.x + " and y = " + object_.y + " trying to update after killed");
			}// end if*/					
	
			
			if (iDUL != object_.m_iDUL)
			{
				if ((object_.m_iDUL != iDUR) && (object_.m_iDUL != iDLL) && (object_.m_iDUL != iDLR))
				{
					removeFromList(object_.m_iDUL, object_);
				}// end if
				addToList(iDUL, object_);
				object_.m_iDUL = iDUL;
			}// end if
			

			
			if (iDUR != object_.m_iDUR)
			{
				if ((object_.m_iDUR != iDUL) && (object_.m_iDUR != iDLL) && (object_.m_iDUR != iDLR))
				{
					removeFromList(object_.m_iDUR, object_);
				}// end if
				addToList(iDUR, object_);
				object_.m_iDUR = iDUR;
			}// end if
			
			

			
			if (iDLL != object_.m_iDLL)
			{
				if ((object_.m_iDLL != iDUL) && (object_.m_iDLL != iDUR) && (object_.m_iDLL != iDLR))
				{
					removeFromList(object_.m_iDLL, object_);
				}// end if
				addToList(iDLL, object_);
				object_.m_iDLL = iDLL;
			}// end if
			

			
			if (iDLR != object_.m_iDLR)
			{
				if ((object_.m_iDLR != iDUL) && (object_.m_iDLR != iDUR) && (object_.m_iDLR != iDLL))
				{
					removeFromList(object_.m_iDLR, object_);
				}// end if
				addToList(iDLR, object_);
				object_.m_iDLR = iDLR;
			}// end if			
			
			
		}// end public function updateObject(object_:Entity):void
		
		public function getMatchingList(object_:Entity):Vector.<Entity>
		{		
			
			if (!object_.m_canCollide)
			{
				return null;
			}// end if	
		
			/*if ((object_ is ExplosionParticle) && !m_map[object_.m_iDUL])
			{
				trace("ExplosionParticle with x = " + object_.x + " and y = " + object_.y + " has empty map");
			}// end if*/				
			
			
			var objectList:Vector.<Entity> = new Vector.<Entity>;
			objectList = objectList.concat(m_map[object_.m_iDUL]);
			
			
			var auxObjectList:Vector.<Entity> = new Vector.<Entity>;	
			auxObjectList = auxObjectList.concat(m_map[object_.m_iDUR]);	
			
			var insertObject:Boolean;
			
			if (object_.m_iDUR != object_.m_iDUL)
			{
			
				for (var currObject:uint = 0; currObject < auxObjectList.length; currObject++)
				{
					
					insertObject = true;				
					for (var currStoredObject:uint = 0; currStoredObject < objectList.length; currStoredObject++)
					{
						if (objectList[currStoredObject] == auxObjectList[currObject])
						{
							insertObject = false;
							break;
						}// end if
					}// end for				
					
					if (insertObject)
					{
						objectList.push(auxObjectList[currObject]);
					}// end if
					
				}// end for
				
			}// end if
			

			
			auxObjectList = new Vector.<Entity>;	
			auxObjectList = auxObjectList.concat(m_map[object_.m_iDLL]);
			
			if ((object_.m_iDLL != object_.m_iDUR) && (object_.m_iDLL != object_.m_iDUL))
			{
			
				for (currObject = 0; currObject < auxObjectList.length; currObject++)
				{
					
					insertObject = true;
					for (currStoredObject = 0; currStoredObject < objectList.length; currStoredObject++)
					{
						if (objectList[currStoredObject] == auxObjectList[currObject])
						{
							insertObject = false;
							break;
						}// end if
					}// end for				
					
					if (insertObject)
					{
						objectList.push(auxObjectList[currObject]);
					}// end if
					
				}// end for						
			
			}// end if
			

			auxObjectList = new Vector.<Entity>;
			auxObjectList = auxObjectList.concat(m_map[object_.m_iDLR]);
			
			
			if ((object_.m_iDLR != object_.m_iDLL) && (object_.m_iDLR != object_.m_iDUR) && (object_.m_iDLR != object_.m_iDUL))
			{
			
				for (currObject = 0; currObject < auxObjectList.length; currObject++)
				{
					
					insertObject = true;
					for (currStoredObject = 0; currStoredObject < objectList.length; currStoredObject++)
					{
						if (objectList[currStoredObject] == auxObjectList[currObject])
						{
							insertObject = false;
							break;
						}// end if
					}// end for				
					
					if (insertObject)
					{
						objectList.push(auxObjectList[currObject]);
					}// end if
					
				}// end for	
				
			}// end if
			
			return objectList;
			
		}// end public function getMatchingList(object_:Entity):Vector.<Entity>				
		
	}// end public class SpatialHashMap
	
}// end package