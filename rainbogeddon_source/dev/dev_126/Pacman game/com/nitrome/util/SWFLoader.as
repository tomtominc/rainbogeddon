package com.nitrome.util {
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.Security;
	
	/**
	 * Loads in SWF files
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class SWFLoader extends Sprite{
		
		public var swfWidth:Number;
		public var swfHeight:Number;
		public var url:String;
		public var addToStage:Boolean;
		public var testCard:Sprite;
		public var ready:Boolean;
		
		private var swfLoader:Loader;
		
		public function SWFLoader(width:Number, height:Number, url:String, sameApplicationDomain:Boolean = false, addToStage:Boolean = false) {
			this.swfWidth = width;
			this.swfHeight = height;
			this.url = url;
			this.addToStage = addToStage;
			ready = false;
			
			swfLoader = new Loader();
			addChild(swfLoader);
			swfLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onSwfLoadError, false, 0 , true);
			swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete, false, 0 , true);
			
			trace("attempting to load: " + url);
			
			if(sameApplicationDomain){
				swfLoader.load(new URLRequest(url), new LoaderContext(false, ApplicationDomain.currentDomain));
			} else {
				swfLoader.load(new URLRequest(url), new LoaderContext(false, new ApplicationDomain()));
			}
			
			addTestCard();
		}
		
		public static function init(domain:String):void{
			// Cross domain calls initialised here
			Security.allowDomain(domain);
		}
		
		/* This must be called to garbage collect the SWFLoader */
		public function destroy():void{
			removeSwf();
			if(parent) parent.removeChild(this);
		}
		
		/* Garbage collection for the player */
		private function removeSwf():void{
			while(numChildren) removeChildAt(0);
			if(swfLoader){
				swfLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onSwfLoadError);
				swfLoader.unloadAndStop(true);
			}
			swfLoader = null;
			testCard = null;
			ready = false;
		}
		
		private function onLoadComplete(e:Event):void {
			trace("swf loaded");
			if(testCard){
				if(testCard.parent) testCard.parent.removeChild(testCard);
				testCard = null;
			}
			swfLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
			if(!addToStage){
				swfLoader.scaleX = swfWidth / swfLoader.contentLoaderInfo.width;
				swfLoader.scaleY = swfHeight / swfLoader.contentLoaderInfo.height;
			} else {
				NitromeGame.root.addChildAt(this, 0);
				this.visible = false;
			}
			ready = true;
		}
		
		private function onSwfLoadError(e:Event):void{
			trace("Loading the swf failed");
			removeSwf();
			addTestCard();
		}
		
		/* When any errors occur, we need to show a test card */
		private function addTestCard():void{
			testCard = new TestCardMC();
			testCard.width = swfWidth;
			testCard.height = swfHeight;
			addChild(testCard);
			ready = true;
		}
		
	}

}