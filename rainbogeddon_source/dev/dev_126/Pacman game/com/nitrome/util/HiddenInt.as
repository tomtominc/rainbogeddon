﻿package com.nitrome.util {
	
	/**
	* A wrapper for ints to protect them from tools like CheatEngine
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class HiddenInt {
		
		private var Value:int;
		private var r:int;
		
		public function HiddenInt(startValue:int = 0){
			r = (Math.random()*2000000)-1000000;
			Value = startValue ^ r;
		}
		// Getter setters for value
		public function set value(v:int):void{
			r = (Math.random()*2000000)-1000000;
			Value = v ^ r;
		}
		public function get value():int{
			return Value ^ r;
		}
	}
	
}