﻿package com.nitrome.util.string {
	
	/* Convert a number to a string with a minimum number of trailing zeros */
	public function formatInt(value:int, digits:int):String{
		var str:String = "" + value;
		while(str.length < digits){
			str = "0" + str;
		}
		return str;
	}

}