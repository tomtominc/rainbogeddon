﻿package com.nitrome.util.misc {
	import flash.display.Sprite;
	
	/* A check to see if (x,y) is on screen plus a border */
	public function onScreen(x:Number, y:Number, canvas:Sprite, border:Number):Boolean{
		return x + border >= -canvas.x && y + border >= -canvas.y && x - border < -canvas.x + Game.WIDTH && y - border < -canvas.y + Game.HEIGHT;
	}

}