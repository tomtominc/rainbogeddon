package com.nitrome.ui.highscore {
	import com.nitrome.ui.button.BasicButton;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.display.MovieClip;

	public class LetterButtonUnder extends MovieClip
	{
	
		
		public function LetterButtonUnder(){
			// add event listeners
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void
		{
			letter.text = name.replace("_under", "");
            e.target.removeEventListener(Event.ADDED_TO_STAGE , init);
        }	
		
	}// end public class LetterButtonUnder extends MovieClip
	
}// end package com.nitrome.ui.highscore