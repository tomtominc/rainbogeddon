package com.nitrome.ui
{

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.*;
	import flash.utils.getTimer;

	public class MouseHoverElement extends MovieClip
	{

		private static const STATE_FOLLOWING_MOUSE:uint     = 0;
		private static const STATE_LOCKING_TO_MENU_POS:uint = 1;
		private static const STATE_LOCKED_TO_MENU_POS:uint  = 2;
		private static const HOVER_POINT_DURATION:int = 250;		

		private var m_currMouseHoverPoint:Vector3D;
		protected var m_menuLockPosList:Vector.<Vector3D>;
		private var m_trailList:Vector.<MovieClip>
		private var m_currPosLockIndex:uint;
		private var m_vel:Vector3D;
		private var m_accel:Vector3D;
		private var m_chooseHoverPointStartTime:int;
		private var m_state:uint;
		private var m_velT:Number;
		private var m_accelScale:Number;
		private var m_bounceTimes:uint;
		protected var m_redOffset:uint;
		protected var m_greenOffset:uint;
		protected var m_blueOffset:uint;
		
		public function MouseHoverElement()
		{
			
			mouseEnabled = false;
			addEventListener(Event.ENTER_FRAME, doPhysics);
			m_currMouseHoverPoint = new Vector3D();
			updateHoverPoint();
			m_vel = new Vector3D();
			var hoverPointPos:Vector3D = new Vector3D(m_currMouseHoverPoint.x, m_currMouseHoverPoint.y);
			var thisPos:Vector3D = new Vector3D(x, y);
			var thisToHoverPointVec:Vector3D = hoverPointPos.subtract(thisPos);
			m_vel.x = thisToHoverPointVec.x;
			m_vel.y = thisToHoverPointVec.y;
			m_vel.normalize();
			m_accel = new Vector3D(m_vel.x, m_vel.y);
			m_accel.scaleBy(1.02);
			m_vel.scaleBy(1.005);
			m_chooseHoverPointStartTime = getTimer();
			m_state = STATE_FOLLOWING_MOUSE;
			m_trailList = new Vector.<MovieClip>;
			m_menuLockPosList = new Vector.<Vector3D>(4);
			m_menuLockPosList[0] = new Vector3D();
			m_menuLockPosList[1] = new Vector3D();
			m_menuLockPosList[2] = new Vector3D();
			m_menuLockPosList[3] = new Vector3D();
			m_bounceTimes = 0;
			parent.mouseEnabled = false;
			parent.mouseChildren = false;
			
				
		}// end public function MouseHoverElement()
	
		public function doPhysics(e_:Event)
		{		
			
	
			switch(m_state)
			{
				
				case STATE_FOLLOWING_MOUSE:
				{
				
					var hoverPointPos:Vector3D = new Vector3D(m_currMouseHoverPoint.x, m_currMouseHoverPoint.y);
					var thisPos:Vector3D = new Vector3D(x, y);
					var thisToHoverPointVec:Vector3D = hoverPointPos.subtract(thisPos);		
					m_accel.x = thisToHoverPointVec.x;
					m_accel.y = thisToHoverPointVec.y;
					m_accel.normalize();
					m_accel.scaleBy(1.002);
					m_vel = m_vel.add(m_accel);
					if (m_vel.length > 10)
					{
						m_vel.normalize();
						m_vel.scaleBy(10);
					}// end if
					x += m_vel.x;
					y += m_vel.y;
					
					if ((getTimer() - m_chooseHoverPointStartTime) > HOVER_POINT_DURATION)
					{
						m_chooseHoverPointStartTime = getTimer();
						updateHoverPoint();
					}// end if
					
					addTrailParticle();				
					
					break;
				}// end case
				
				case STATE_LOCKING_TO_MENU_POS:
				{
				
					var lockPointPos:Vector3D = new Vector3D(m_menuLockPosList[m_currPosLockIndex].x, m_menuLockPosList[m_currPosLockIndex].y);
					thisPos = new Vector3D(x, y);
					var thisToLockPointVec:Vector3D = lockPointPos.subtract(thisPos);	
					m_accel.x = thisToLockPointVec.x;
					m_accel.y = thisToLockPointVec.y;
					m_accel.normalize();
					m_accel.scaleBy(m_accelScale);
					m_accelScale += 0.45; // 0.45
					m_vel.x *= (1 - m_velT);
					m_vel.y *= (1 - m_velT);
					m_accel.x *= m_velT;
					m_accel.y *= m_velT;
					m_vel = m_vel.add(m_accel);
					
					if (m_vel.length > 20)
					{
						m_vel.normalize();
						m_vel.scaleBy(20);						
					}

					x += m_vel.x;
					y += m_vel.y;					
					
					thisPos.x = x;
					thisPos.y = y;
					thisToLockPointVec = lockPointPos.subtract(thisPos);
					
					if (thisToLockPointVec.length < m_vel.length)
					{
						
						m_bounceTimes++;
						m_velT = 1/((5-(m_bounceTimes-1))+16);
						
						if (m_bounceTimes > 5)
						{
							x = lockPointPos.x;
							y = lockPointPos.y;
							m_vel.x = 0;
							m_vel.y = 0;
							m_state = STATE_LOCKED_TO_MENU_POS;							
						}// end if
						
					}// end if
			
					
					m_velT += 0.026;
					
					if (m_velT > 1)
					{
						m_velT = 1;
					}// end if
					
					addTrailParticle();
					
					break;
				}// end case				
				
			}// end switch
			
			for (var currTrailObject:int = 0; currTrailObject < m_trailList.length; currTrailObject++)
			{
				m_trailList[currTrailObject].alpha -= 0.05;
				if (m_trailList[currTrailObject].alpha <= 0)
				{
					parent.removeChild(m_trailList[currTrailObject]);
					m_trailList.splice(currTrailObject, 1);
					currTrailObject--;
				}// end if
			}// end for
			
		}// end public function doPhysics(e_:Event)
		
		public function trailClass():Class
		{
			return null;
		}// end public function trailClass():Class				
		
		private function updateHoverPoint():void
		{
		
			var xRel:Number = (Math.random() * 10) - 5;
			var yRel:Number = (Math.random() * 10) - 5;
			m_currMouseHoverPoint.x = NitromeGame.stage.mouseX + xRel;
			m_currMouseHoverPoint.y = NitromeGame.stage.mouseY + yRel;
			
		}// end private function updateHoverPoint():void
		
		public function menuPosLock(index_:uint):void
		{
			
			m_state = STATE_LOCKING_TO_MENU_POS;
			m_currPosLockIndex = index_;
			m_velT = 0;
			m_accelScale = 1.002;
			m_bounceTimes = 0;
			
		}// end public function menuPosLock(index_:uint):void
		
		public function abortMenuPosLock():void
		{
		
			m_state = STATE_FOLLOWING_MOUSE;
			m_chooseHoverPointStartTime = getTimer();
			updateHoverPoint();			
			
		}// end public function abortMenuPosLock():void		
		
		public function addTrailParticle():void
		{
		
			m_trailList.push(new (trailClass())());
			//NitromeGame.root.addChildAt(m_trailList[m_trailList.length - 1], getChildIndex(this) - 1);
			var trailIndex:uint = m_trailList.length - 1;
			parent.addChild(m_trailList[trailIndex]);
			parent.setChildIndex(this, parent.numChildren-1);
			m_trailList[trailIndex].gotoAndPlay(currentFrame);
			var colorTransform:ColorTransform = m_trailList[trailIndex].transform.colorTransform;
			colorTransform.redMultiplier = 0;
			colorTransform.greenMultiplier = 0;
			colorTransform.blueMultiplier = 0;
			colorTransform.redOffset = m_redOffset;
			colorTransform.greenOffset = m_greenOffset;
			colorTransform.blueOffset = m_blueOffset;
			m_trailList[trailIndex].transform.colorTransform = colorTransform;
			m_trailList[trailIndex].x = x;
			m_trailList[trailIndex].y = y;	
			m_trailList[trailIndex].mouseEnabled = false;
			m_trailList[trailIndex].alpha = 0.5;
			
		}// end public function addTrailParticle():void		
		
	}// end public class MouseHoverElement
	
}// end package