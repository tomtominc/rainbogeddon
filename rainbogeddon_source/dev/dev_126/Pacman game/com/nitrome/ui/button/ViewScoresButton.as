﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	import com.nitrome.sound.SoundManager;

	/**
	* Goes to view_scores frame
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class ViewScoresButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			SoundManager.playSound("Scores");
			NitromeGame.root.transition.goto("viewScores");
		}
		
		override protected function onMouseOver(e:MouseEvent):void{
			super.onMouseOver(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.menuPosLock(3);
				NitromeGame.root.hatNBowContainer.bow.menuPosLock(3);
			}// end if
		}
		override protected function onMouseOut(e:MouseEvent):void{
			super.onMouseOut(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.abortMenuPosLock();
				NitromeGame.root.hatNBowContainer.bow.abortMenuPosLock();		
			}// end if
		}				
		
	}
	
}