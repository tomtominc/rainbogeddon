﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Unpause
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class ContinueGameButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			NitromeGame.root.game.pauseGame();
		}
		
	}
	
}