package com.nitrome.ui.button {
	
	import com.nitrome.ui.PlayerSelectPanel;
	import flash.events.MouseEvent;
	

	public class ResetKeysButton extends BasicButton
	{
		
		public function ResetKeysButton()
		{
		}
		 
		override public function onClick(e:MouseEvent):void
		{
			super.onClick(e);
			execute();
		}
		
		public function execute():void
		{
		
			var buttonList:Vector.<KeyButton> = (parent as PlayerSelectPanel).keyButtonList;		
			
			for (var currButton:uint = 0; currButton < buttonList.length; currButton++)
			{
				buttonList[currButton].reset();
				buttonList[currButton].deselect();
			}// end for			
			
		}
		
	}// end public class ResetKeysButton extends BasicButton
}