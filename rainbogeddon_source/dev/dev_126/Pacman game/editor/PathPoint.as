﻿
package editor {
	public class PathPoint {
		public var x;
		public var y;
		
		public function PathPoint(nx:Number = 0, ny:Number = 0) {
			x = nx;
			y = ny;
		}
	}
}