﻿package editor {
	import flash.display.BitmapData;
	import flash.display.FrameLabel;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Global {
		
		public static function findInList(item, list:Array):Number {
			for(var n:Number = 0; n < list.length; n ++)
				if(list[n] == item) return n;
			return -1;
		}
		public static function isInList(item, list:Array):Boolean {
			for(var n:Number = 0; n < list.length; n ++)
				if(list[n] == item) return true;
			return false;
		}
		
		public static function removeFromList(item, list:Array):void {
			for(var n:Number = list.length - 1; n >= 0; n --) {
				if(list[n] == item) list.splice(n, 1);
			}
		}
		
		public static function removeOnceFromList(item, list:Array):void {
			for(var n:Number = list.length - 1; n >= 0; n --) {
				if(list[n] == item) {
					list.splice(n, 1);
					return;
				}
			}
		}
		
		public static function slide(from:Number, to:Number, by:Number):Number {
			if(from == to) return to;
			if(from < to) {
				from += by;
				if(from > to) from = to;
				return from;
			} else {
				from -= by;
				if(from < to) from = to;
				return from;
			}
		}
		public static function lerp(from:Number, to:Number, s:Number):Number {
			return (from * (1-s)) + (to * s);
		}
		public static function alerp(from:Number, to:Number, result:Number):Number {
			return (result - from) / (to - from);
		}
		
		public static function deceleratingSweep(current:Number, target:Number, speed:Number,
												 acceleration:Number, deceleration:Number):Number {
			// returns the next change in speed
			// (the acceleration and deceleration values should both be given as positive)
			// e.g. velocityY += deceleratingSweep(y, targetY, velocityY, 0.2, 0.2);
			
			var stopForce:Number = (speed > 0) ? -deceleration : deceleration;
			var atHalt:Number = (speed * speed) / (2 * -stopForce); // -s² / 2(-d)
			atHalt += current;
			if(current < target) {
				if(atHalt > target)
					return -deceleration;
				else
					return acceleration;
			} else {
				if(atHalt < target)
					return deceleration;
				else
					return -acceleration;
			}
		}
		
		public static function positiveModulo(a:Number, b:Number):Number {
			if(a >= 0)
				return a%b;
			else
				return (a%b) + b;
		}
		public static function negativeModulo(a:Number, b:Number):Number {
			if(a >= 0)
				return (a%b) - b;
			else
				return a%b;
		}
		
		public static function sign(n:Number):Number {
			if(n > 0) return 1;
			if(n < 0) return -1;
			return 0;
		}
		
		public static function copyColorTransform(ct:ColorTransform):ColorTransform {
			return new ColorTransform(ct.redMultiplier, ct.greenMultiplier,
				ct.blueMultiplier, ct.alphaMultiplier,
				ct.redOffset, ct.greenOffset, ct.blueOffset, ct.alphaOffset);
		}
		
		public static function whiteOut(visibility:Number):ColorTransform {
			if(visibility > 0.5) {
				var m:Number = (visibility - 0.5) * 2;
				var o:Number = 255 - (m*255);
				return new ColorTransform(m, m, m, 1, o, o, o, 0);
			} else {
				return new ColorTransform(0, 0, 0, visibility * 2, 255, 255, 255, 0);
			}
		}
		public static function blackOut(visibility:Number):ColorTransform {
			if(visibility > 0.5) {
				var m:Number = (visibility - 0.5) * 2;
				return new ColorTransform(m, m, m, 1, 0, 0, 0, 0);
			} else {
				return new ColorTransform(0, 0, 0, visibility * 2, 0, 0, 0, 0);
			}
		}
		
		public static function trim(s:String):String {
			var from:Number = 0;
			var to:Number = s.length - 1;
			
			while(s.charAt(from) == ' ' || s.charAt(from) == '\t'
				|| s.charAt(from) == '\n' || s.charAt(from) == '\r') from ++;
			while(s.charAt(to) == ' ' || s.charAt(to) == '\t'
				|| s.charAt(to) == '\n' || s.charAt(to) == '\r') to --;
			
			return s.substring(from, to + 1);
		}
		
		public static function repeatString(string:String, multiple:Number):String {
			if(multiple < 1) return "";
			var result:String = string;
			for(var n:Number = 1; n < multiple; n ++) {
				result += string;
			}
			return result;
		}
		
		public static function childrenList(list, childName:String):Array {
			var result:Array = [];
			for(var n in list) result.push(list[n][childName]);
			return result;
		}
		
		public static function safeMinimum(list:Array):Number {
			var result:Number = NaN;
			for(var n in list)
				if(isNaN(result) || list[n] < result) result = list[n];
			return result;
		}
		
		public static function safeMaximum(list:Array):Number {
			var result:Number = NaN;
			for(var n in list)
				if(isNaN(result) || list[n] > result) result = list[n];
			return result;
		}
		
		public static function isNumeric(string:String):Boolean {
			return string == String(Number(string));
		}
		
		public static function textToBitmap(target:BitmapData, text:String,
			x:Number = 0, y:Number = 0, color:Number = 0xFFFFFF,
			center:Boolean = false, textFormat:TextFormat = null):void {
				
			var tf:TextField = new TextField();
			tf.autoSize = TextFieldAutoSize.LEFT;
			if(textFormat)
				tf.defaultTextFormat = textFormat;
			else
				tf.defaultTextFormat = new TextFormat("_sans", 10);
			tf.textColor = color;
			tf.text = text;
			
			if(center)
				target.draw(tf, new Matrix(1,0,0,1,x - int(tf.width / 2),y - int(tf.height / 2)));
			else
				target.draw(tf, new Matrix(1,0,0,1,x,y));
			
		}
		
		public static function enforcePlaces(number:Number, places:Number):String {
			if(number < 0) return "-" + enforcePlaces(-number, places);
			var integerPart:String = String(Math.floor(number));
			var decimalPart:String = String(Math.floor((number % 1) * Math.pow(10, places)));
			while(decimalPart.length < places) decimalPart = "0" + decimalPart;
			return integerPart + "." + decimalPart;
		}
		
		public static function ordinal(number:Number):String {
			if((number % 100) > 10 && (number % 100) < 15) return number.toString() + "th";
			if(number % 10 == 1) return number.toString() + "st";
			if(number % 10 == 2) return number.toString() + "nd";
			if(number % 10 == 3) return number.toString() + "rd";
			return number.toString() + "th";
		}
		
		public static function createGrid(x:Number, y:Number, defaultItem:* = null):Array {
			var result:Array = [];
			var column:Array = [];
			var n:Number;
			
			for(n = 0; n < y; n ++) column.push(defaultItem);
			for(n = 0; n < x; n ++) result.push(column.slice());
			return result;
		}
		
		public static function createList(count:Number, defaultContent:* = null):Array {
			// only really makes sense with defaultContent filled in
			var result:Array = [];
			for(var n:Number = 0; n < count; n ++)
				result.push(defaultContent);
			return result;
		}
		
		public static function copyArrayDeep(a:Array):Array {
			var result:Array = [];
			for(var n:Number = 0; n < a.length; n ++) {
				if(a[n] is Array)
					result.push(copyArrayDeep(a[n]));
				else
					result.push(a[n]);
			}
			return result;
		}
		
		public static function findLabel(mc:MovieClip, labelName:String):FrameLabel {
			for(var n:Number = 0; n < mc.currentLabels.length; n ++) {
				if(mc.currentLabels[n].name == labelName) return mc.currentLabels[n];
			}
			return null;
		}
		
		// only works on arrays where unique items will be next to each other (such as sorted ones)
		public static function unique(a:Array):Array {
			if(a.length < 1) return [];
			
			var result:Array = [a[0]];
			for(var n:Number = 1; n < a.length; n ++) {
				if(a[n-1] != a[n]) result.push(a[n]);
			}
			return result;
		}
		
	}
	
}