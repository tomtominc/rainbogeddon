﻿package editor {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLNode;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Tile {
		
		public var gridX:Number;
		public var gridY:Number;
		public var layer:Layer;
		
		public var type:Class;
		public var typeName:String;
		public var sprite:Sprite;
		
		public var signText:String;
		
		public var pathPoints:Array;
		public var connectedToX:Number;
		public var connectedToY:Number;
		
		public var infinitelyWideSprite:Sprite;
		
		public function Tile(tx:Number, ty:Number, layer:Layer) {
			gridX = tx;
			gridY = ty;
			this.layer = layer;
			
			pathPoints = [new PathPoint(tx, ty)];
			connectedToX = tx;
			connectedToY = ty;
		}
		
		public function getType():Class {
			return type;
		}
		public function getTypeName():String {
			if(typeName.substr(0, layer.tilePrefix.length) == layer.tilePrefix)
				return typeName.substr(layer.tilePrefix.length)
			else
				return typeName;
		}
		public function setType(newClass:Class):void {
			type = newClass;
			typeName = getQualifiedClassName(type);
			
			
			if(sprite) {
				sprite.parent.removeChild(sprite);
				sprite = null;
			}
			resetInfiniteSprite();
			if(newClass) {
				sprite = new newClass;
				sprite.x = gridX * Controller.W;
				sprite.y = gridY * Controller.H;
				layer.addChild(sprite);
				//if(sprite is MovieClip)
					//(sprite as MovieClip).stop();
				
				if (Controller.isSignTile(type)) {
					if (signText)
					{
						Controller.panel.signPopup.signText.text = signText;
					}// end if
					//setSignText(Controller.panel.signPopup.signText.text);
					Controller.paramTile = this;
				}
				
				if(typeName.substr(-9) == "deathsand") {
					try {
						var c:Class = getDefinitionByName(typeName + "_infinite") as Class;
						infinitelyWideSprite = new c;
						infinitelyWideSprite.x = -Controller.container.x;
						infinitelyWideSprite.y = sprite.y;
						layer.addChildAt(infinitelyWideSprite, 0);
						
						(sprite as MovieClip).gotoAndStop(2);
					} catch(e:Error) { trace(e); }
				}
			}
		}
		public function setInvalidType(name:String):void {
			sprite = new Sprite;
			sprite.graphics.beginFill(0xFF0000, 0.5);
			sprite.graphics.drawRect(0, 0, Controller.W, Controller.H);
			sprite.graphics.endFill();
			
			var tf:TextField = new TextField;
			tf.text = name.toUpperCase();
			
			sprite.addChild(tf);
			tf.setTextFormat(new TextFormat("_sans", 10, 0xFFFFFF, true));
			tf.background = true;
			tf.backgroundColor = 0xFF0000;
			tf.width = tf.textWidth + 4;
			tf.height = tf.textHeight + 4;
			tf.selectable = false;
			tf.mouseEnabled = false;
			tf.x = Math.floor((Controller.W - tf.width) / 2);
			tf.y = Math.floor((Controller.H - tf.height) / 2);
			
			sprite.x = gridX * Controller.W;
			sprite.y = gridY * Controller.H;
			layer.addChild(sprite);
			
			type = null;
			typeName = name;
			
			resetInfiniteSprite();
		}
		
		public function resetInfiniteSprite():void {
			if(infinitelyWideSprite) {
				infinitelyWideSprite.parent.removeChild(infinitelyWideSprite);
				infinitelyWideSprite = null;
			}
		}
		
		public function setSignText(s:String):void {
			signText = s;
			
			if (!sprite) return;
			if(!sprite is MovieClip) return;
			if(!(sprite as MovieClip).textField) return;
			(sprite as MovieClip).textField.text = s.split("|").join("\n");
		}
		
		public function get leftmostX():Number {
			var a:Array = [gridX, connectedToX];
			if(pathPoints && pathPoints.length > 1)
				for(var n:Number = 0; n < pathPoints.length; n ++) a.push(pathPoints[n].x);
			return Global.safeMinimum(a);
		}
		public function get rightmostX():Number {
			var a:Array = [gridX, connectedToX];
			if(pathPoints && pathPoints.length > 1)
				for(var n:Number = 0; n < pathPoints.length; n ++) a.push(pathPoints[n].x);
			return Global.safeMaximum(a);
		}
		public function get topmostY():Number {
			var a:Array = [gridY, connectedToY];
			if(pathPoints && pathPoints.length > 1)
				for(var n:Number = 0; n < pathPoints.length; n ++) a.push(pathPoints[n].y);
			return Global.safeMinimum(a);
		}
		public function get bottommostY():Number {
			var a:Array = [gridY, connectedToY];
			if(pathPoints && pathPoints.length > 1)
				for(var n:Number = 0; n < pathPoints.length; n ++) a.push(pathPoints[n].y);
			return Global.safeMaximum(a);
		}
		
		public function getPathXML(dx:Number, dy:Number):XML {
			if(!pathPoints) return null;
			if(pathPoints.length < 2) return null;
			
			var pathEntries:Array = [];
			for(var n:Number = 0; n < pathPoints.length; n ++) {
				pathEntries.push("[" + (pathPoints[n].x + dx) + "," + (pathPoints[n].y + dy) + "]");
			}
			var pathXML:XML = <path x={ gridX+dx } y={ gridY+dy } pts={ pathEntries.join(">") } />;
			return pathXML;
		}
		public function getConnectionXML(dx:Number, dy:Number):XML {
			if(connectedToX == gridX && connectedToY == gridY) return null;
			
			var connXML:XML = <conn sx={ gridX+dx } sy={ gridY+dy } mx={ connectedToX+dx } my={ connectedToY+dy } />;
			return connXML;
		}
		
		public function setPathXML(xml:XML):void {
			pathPoints = [];
			
			var pathString:String = xml.@pts;
			pathString = pathString.split("[").join("");
			pathString = pathString.split("]").join("");
			
			var elements:Array = pathString.split(">");
			for(var n:Number = 0; n < elements.length; n ++) {
				var coords:Array = elements[n].split(",");
				pathPoints.push(new PathPoint(coords[0], coords[1]));
			}
		}
		public function setConnectionXML(xml:XML):void {
			connectedToX = Number(xml.@mx);
			connectedToY = Number(xml.@my);
		}
		
	}
	
}