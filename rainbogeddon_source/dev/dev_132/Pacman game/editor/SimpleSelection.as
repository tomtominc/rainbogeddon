﻿package editor {
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class SimpleSelection extends Sprite {
		
		public var boxWidth:Number = 210;
		
		private var fields:Array /*TextField*/ = [];
		private var selectedIndex:Number = 0;
		private var selectedSprite:Sprite;
		private var hoverIndex:Number = -1;
		private var hoverSprite:Sprite;
		public var onChange:Function = null;
		
		private var boldFormat:TextFormat;
		private var nonBoldFormat:TextFormat;
		
		public function SimpleSelection() {
			addChild(selectedSprite = new Sprite);
			addChild(hoverSprite = new Sprite);
			
			drawSelection();
			drawBackground();
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			this.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			this.addEventListener(MouseEvent.ROLL_OUT, rollout);
			
			nonBoldFormat = new TextFormat("_sans");
			nonBoldFormat.align = TextFormatAlign.CENTER;
			nonBoldFormat.bold = false;
			boldFormat = new TextFormat("_sans");
			boldFormat.align = TextFormatAlign.CENTER;
			boldFormat.bold = true;
		}
		
		public function addOption(text:String):void {
			var newTextField:TextField = new TextField();
			newTextField.text = text;
			newTextField.setTextFormat(nonBoldFormat);
			newTextField.y = 3;
			newTextField.x = 6;
			if(fields.length > 0) {
				newTextField.x = fields[fields.length - 1].x + fields[fields.length - 1].width + 2;
			}
			newTextField.width = newTextField.textWidth + 8;
			newTextField.height = newTextField.textHeight + 5;
			newTextField.selectable = false;
			newTextField.mouseEnabled = false;
			newTextField.tabEnabled = false;
			addChild(newTextField);
			fields.push(newTextField);
			
			drawSelection();
		}
		
		public function get index():Number {
			return selectedIndex;
		}
		public function set index(newIndex:Number):void {
			selectedIndex = newIndex;
			drawSelection();
			if(onChange != null) onChange();
		}
		
		public function select(newIndex:Number):void {
			if(newIndex < 0) return;
			if(newIndex >= fields.length) return;
			index = newIndex;
		}
		
		public function mouseDown(e:MouseEvent):void {
			var localX:Number = globalToLocal(new Point(e.stageX, e.stageY)).x;
			var selected:Number = -1;
			for(var n:Number = 0; n < fields.length; n++) {
				var field:TextField = fields[n];
				if(field.x < localX && field.x+field.width > localX) selected = n;
			}
			if(selected >= 0) {
				selectedIndex = selected;
				drawSelection();
				if(onChange != null) onChange();
			}
			mouseMove(e);
		}
		public function mouseMove(e:MouseEvent):void {
			var hand:Boolean = false;
			if(fields.length > 0) {
				if(mouseX > fields[0].x && mouseX < fields[fields.length - 1].x + fields[fields.length - 1].width
				&& mouseY > fields[0].y && mouseY < fields[0].y + fields[0].height) {
					hand = true;
				}
			}
			
			var newHoverIndex = -1;
			for(var n:Number = 0; n < fields.length; n++) {
				var field:TextField = fields[n];
				if(field.x < mouseX && field.x+field.width > mouseX
				&& field.y < mouseY && field.y+field.height > mouseY) {
					newHoverIndex = n;
					if(newHoverIndex == selectedIndex) hand = false;
				}
			}
			if(newHoverIndex != hoverIndex) {
				hoverIndex = newHoverIndex;
				drawSelection();
			}
			
			useHandCursor = hand;
			buttonMode = hand;
		}
		
		public function rollout(e:MouseEvent):void {
			mouseMove(e);
		}
		
		public function drawSelection():void {
			var field:TextField;
			
			for each(field in fields) {
				field.textColor = 0x000000;
				field.setTextFormat(nonBoldFormat);
			}
			
			field = fields[selectedIndex];
			selectedSprite.graphics.clear();
			
			if(field != null) {
				field.textColor = 0xFFFFFF;
				field.setTextFormat(boldFormat);
				
				selectedSprite.graphics.beginFill(0x3C8AFF);
				selectedSprite.graphics.lineStyle(1, 0x0066FF);
				selectedSprite.graphics.drawRect(field.x, field.y, field.width, field.height - 2);
				selectedSprite.graphics.endFill();
			}
			
			hoverSprite.graphics.clear();
			
			if(hoverIndex >= 0 && hoverIndex != selectedIndex) {
				field = fields[hoverIndex];
				
				hoverSprite.graphics.beginFill(0xb4cbee);
				hoverSprite.graphics.lineStyle(1, 0x7fadf4);
				hoverSprite.graphics.drawRect(field.x, field.y, field.width, field.height - 2);
				hoverSprite.graphics.endFill();
			}
		}
		
		public function drawBackground():void {
			graphics.clear();
			
			var m:Matrix = new Matrix;
			m.createGradientBox(boxWidth, 25, 90);
			graphics.beginGradientFill("linear", [0xF8F8F8, 0xE0E0E0], [1,1], [0,255], m);
			
			graphics.lineStyle(1, 0xC0C0C0);
			graphics.moveTo(boxWidth, 0);
			graphics.lineTo(boxWidth, 25);
			graphics.lineTo(0, 25);
			graphics.lineStyle(1, 0xFFFFFF);
			graphics.lineTo(0, 0);
			graphics.lineTo(boxWidth, 0);
			
			graphics.endFill();
		}
		
	}
	
}