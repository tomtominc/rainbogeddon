﻿package editor {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Autoarranger extends MovieClip {
		
		public var thisColumnX:Number;
		public var nextColumnX:Number;
		public var nextY:Number;
		
		public const OUTER_PADDING:Number = 5;
		public const INNER_PADDING_Y:Number = 5;
		public const INNER_PADDING_X:Number = 5;
		
		public function Autoarranger(prefix:String, items:Array = null) {
			thisColumnX = 0;
			nextColumnX = 0;
			nextY = 0;
			
			for(var n:Number = 0; n < items.length; n ++) {
				if(items[n] is String) {
					addItem(prefix + items[n]);
				} else if(items[n] == 0) {
					separate();
				} else if(items[n].items) {
					addMacroBlock(prefix, items[n]);
				} else {
					var listPrefix:String = items[n].prefix ? items[n].prefix : "";
					var listSuffix:String = items[n].suffix ? items[n].suffix : "";
					var listFirst:Number = Number(items[n].first);
					var listLast:Number = Number(items[n].last);
					
					for(var i:Number = listFirst; i <= listLast; i ++) {
						addItem(prefix + listPrefix + String(i) + listSuffix);
					}
				}
			}
		}
		
		private function separate():void {
			thisColumnX = nextColumnX + 5;
			nextColumnX = thisColumnX;
			nextY = 0;
		}
		
		private function addMacroBlock(prefix:String, data:Object):void {
			thisColumnX = nextColumnX;
			nextY = 0;
			
			var itemList:Array = data.items;
			var w:Number = data.w;
			var h:Number = Math.ceil(itemList.length / w);
			
			var maxHeight:Number = 158 - (OUTER_PADDING * 3);
			var yOffset:Number = 0;
			
			for(var n:Number = 0; n < itemList.length; n ++) {
				var classRef:Class;
				try {
					classRef = getDefinitionByName(prefix + itemList[n]) as Class;
				} catch(e:Error) {
					continue;
				}
				
				var item:Sprite = new classRef;
				item.x = thisColumnX + (int(n % w) * Controller.W);
				item.y = yOffset + (int(n / w) * Controller.H);
				addChild(item);
			}
			
			thisColumnX += w * Controller.W;
			thisColumnX += INNER_PADDING_X;
			thisColumnX = Math.ceil(thisColumnX);
			nextColumnX = thisColumnX;
		}
		
		private function addItem(itemName:String):void {
			var classRef:Class;
			try {
				classRef = getDefinitionByName(itemName) as Class;
			} catch(e:Error) {
				return;
			}
			
			var maxHeight:Number = 158 - (OUTER_PADDING * 2);
			
			var item:Sprite = new classRef;
			var itemBounds:Rectangle = item.getBounds(item);
			
			if(item.height > maxHeight) {
				item.scaleX = item.scaleY = maxHeight / item.height;
				itemBounds.x *= item.scaleX;
				itemBounds.y *= item.scaleX;
				itemBounds.width *= item.scaleX;
				itemBounds.height *= item.scaleX;
			}
			
			if(nextY + itemBounds.height < maxHeight) {
				item.x = thisColumnX - itemBounds.left;
				item.y = nextY - itemBounds.top;
				nextY += item.height + INNER_PADDING_Y;
				nextColumnX = Math.max(nextColumnX, thisColumnX + item.width + INNER_PADDING_X);
			} else {
				item.x = nextColumnX - itemBounds.left;
				item.y = -itemBounds.top;
				nextY = item.height + INNER_PADDING_Y;
				thisColumnX = Math.ceil(nextColumnX);
				nextColumnX = thisColumnX + item.width + INNER_PADDING_X;
			}
			addChild(item);
		}
		
	}
	
}
