package com.nitrome.ui
{

	import editor.TextBox;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class PanelHeader extends MovieClip
	{
				

		public function PanelHeader()
		{
			
			level_number.text = new String(Game.selectedLevel.value);
			high_score.text = new String(NitromeGame.getTotalScore());
				
		}// end public function PanelHeader()
	

	}// end public class PanelHeader
	
}// end package