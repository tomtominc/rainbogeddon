package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	
	public class PlayerSelectContinueButton extends BasicButton
	{
		
		override public function onClick(e:MouseEvent):void
		{
			super.onClick(e);
			NitromeGame.root.transition.goto("chooseLevel");
		}
		
		
	}// end public class PlayerSelectContinueButton extends BasicButton
	
}