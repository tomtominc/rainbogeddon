﻿package com.nitrome.ui.button {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	/**
	* Buttons that advance help movie
	*
	* Requires the button have the name "right" or "left" to determine its behaviour
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class HelpArrowButton extends BasicButton{
		
		public function HelpArrowButton(){
			init();
		}
		
		public function init():void{
			if(NitromeGame.root.currentLabel == "help"){
				gotoAndStop("inTitle");
			} else {
				gotoAndStop("inGame");
			}
			
			visible = true;
			if(name == "left"){
				if((parent as MovieClip).currentFrame == 1){
					visible = false;
					parent.parent.getChildByName("left").visible = false;
				}
			} else if(name == "right"){
				if((parent as MovieClip).currentFrame == (parent as MovieClip).totalFrames){
					visible = false;
					parent.parent.getChildByName("right").visible = false;
				}
			}
		}
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			if(name == "left"){
				(parent as MovieClip).right.visible = true;
				parent.parent.getChildByName("right").visible = true;
				if((parent as MovieClip).currentFrame == 2){
					visible = false;
					parent.parent.getChildByName("left").visible = false;
				}
				(parent as MovieClip).gotoAndStop(
					(parent as MovieClip).currentFrame - 1
				);
			} else if(name == "right"){
				(parent as MovieClip).left.visible = true;
				parent.parent.getChildByName("left").visible = true;
				if((parent as MovieClip).currentFrame == (parent as MovieClip).totalFrames - 1){
					visible = false;
					//var parentClip:MovieClip = parent.parent as MovieClip;
					//var child:DisplayObject = parent.parent.getChildByName("right");
					parent.parent.getChildByName("right").visible = false;
				}
				(parent as MovieClip).gotoAndStop(
					(parent as MovieClip).currentFrame + 1
				);
			}
		}
		
	}
	
}