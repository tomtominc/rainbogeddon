﻿/**
* Manages submitting scores to the server
* and creates a keyboard event listener to add letters
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.net.URLVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.ui.Keyboard;

	public class ScoreSubmitPanel extends MovieClip{
		private const MAX_LENGTH:Number=10;
		private var loadingClip:MovieClip;
		private var submitLoader:URLLoader;
		private var submitVars:URLVariables;
		private var submitRequest:URLRequest;
		private var submitted:Boolean;
		
		private var hackTimer:int;
		
		public function ScoreSubmitPanel(){
			if(NitromeGame.verify()){
				// Locate instances
				submitted = false;
				// apply ADDED_TO_STAGE event so the TextComponent can initialise
				addEventListener(Event.ADDED_TO_STAGE, init);
				hackTimer = 30;
			} else {
				gotoAndStop("hide");
			}
		}
		/* Add a letter to name_text */
		public function addLetter(l:String):void{
			var str:String=nameText.text;
			if(str.length<MAX_LENGTH){
				l = l.toUpperCase();
				var newStr:String=str+l;
				nameText.text=newStr;
				submitButton.enable();
			}
		}
		/* Clear name_text and disable the submit_button */
		public function clearName():void{
			submitButton.disable();
			nameText.text="";
		}
		/* Send a request to the server containing the submitted score */
		public function submitScore():void{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
			//called when the loading anim is fully visible
			var nameEntered:String = nameText.text.toUpperCase();
			if (nameEntered != ""){
				// initialise our communication objects
				submitLoader = new URLLoader();
				submitVars = new URLVariables();
				submitRequest = new URLRequest(NitromeGame.SUBMIT_URL);
				submitRequest.method = URLRequestMethod.POST;
				submitRequest.data = submitVars;
				submitLoader.addEventListener(Event.COMPLETE, submitSuccessful);
				submitLoader.addEventListener(IOErrorEvent.IO_ERROR, submitFailed);
				
				// load our variables into submitVars
				// create the encrypted data string
				submitVars.data_string=NitromeGame.getScoreData(NitromeGame.getTotalScore()/*Game.score.value*/, nameEntered, NitromeGame.gameId);

				// is it a time based score? Send "0" or "1"
				submitVars.time_based="0";
				
				// load data into the request object
				submitRequest.data = submitVars;

				// submit the score
				submitLoader.load(submitRequest);
			}else{
				loadingBlack.gotoAndPlay(2);
			}
		}
		
		private function init(e:Event):void{
			addEventListener(Event.ENTER_FRAME, textHack, false, 0, true);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
            e.target.removeEventListener(Event.ADDED_TO_STAGE , init);
        }
		// Fucking textComponent won't play ball
		private function textHack(e:Event):void{
			if(!scoreText || hackTimer-- <= 0) removeEventListener(Event.ENTER_FRAME, textHack);
			else scoreText.text = "YOUR SCORE IS " + NitromeGame.getTotalScore();//Game.score.value;
		}
		private function keyPressed(e:KeyboardEvent):void{
			var code:int = e.charCode;
			// make sure a letter only is pressed
			if((code >= 65 && code <= 90) || (code >= 97 && code <= 122)){
				addLetter(String.fromCharCode(code));
			}
			if(e.keyCode == Keyboard.BACKSPACE || e.keyCode == Keyboard.DELETE){
				clearName();
			}
		}
		private function submitSuccessful(e:Event):void{
			trace("Score submitted");
			//send the player to view the high score board
			loadingBlack.gotoAndPlay(2);
		}
		private function submitFailed(e:IOErrorEvent):void{
			trace("Submit score failed: "+e.text);
			trace();
			//error submitting score - oh well nevermind...
			loadingBlack.gotoAndPlay(2);
		}

	}
}
