﻿package com.nitrome.ui.button{
	import flash.events.MouseEvent;
	
	/**
	* Goes to title screen
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class BackButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void {
			super.onClick(e);
			NitromeGame.root.transition.goto("titleScreen");
		}
		
	}
	
}