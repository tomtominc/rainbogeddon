package com.nitrome.ui.button {
	import flash.events.Event;
	/**
	 * A Button that can fade into view and out of view on request. Utilises the visible state to disable
	 * its mouselistener
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class FadeButton extends BasicButton{
		
		public var state:int;
		private var active:Boolean = false;
		
		public static const HIDE:int = 1;
		public static const SHOW:int = 2;
		
		public function FadeButton(){
			hide(true);
		}
		
		/* Fade in the button - force will hide the button without an animation */
		public function show(force:Boolean = false):void{
			if(state == SHOW) return;
			state = SHOW;
			if(force){
				alpha = 1.0;
				visible = true;
			} else {
				if(!visible){
					alpha = 0;
					visible = true;
				}
				if(!active){
					addEventListener(Event.ENTER_FRAME, main, false, 0, true);
					active = true;
				}
			}
		}
		
		/* Fade out the button - force will hide the button without an animation */
		public function hide(force:Boolean = false):void{
			if(state == HIDE) return;
			state = HIDE;
			if(force){
				alpha = 0;
				visible = false;
			} else {
				if(!active){
					addEventListener(Event.ENTER_FRAME, main, false, 0, true);
					active = true;
				}
			}
		}
		
		/* Updates the action of showing or hiding this button */
		private function main(e:Event):void{
			if(state == HIDE){
				if(alpha > 0){
					alpha -= 0.1;
				} else {
					alpha = 0;
					visible = false;
					active = false;
					removeEventListener(Event.ENTER_FRAME, main);
				}
			} else if(state == SHOW){
				if(alpha < 1.0){
					alpha += 0.1;
				} else {
					alpha = 1.0;
					active = false;
					removeEventListener(Event.ENTER_FRAME, main);
				}
			}
		}
		
	}

}