package com.nitrome.engine
{

	
	public class BulletHolder extends WeaponHolder
	{
		
		//public static const FIRE_DELAY:int = 250;
		
		public var  m_fireStartTime:int;
		public var  m_fireDelay:int;
		private var m_fireDelayTable:Object;
		
		public function BulletHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_fireStartTime = 0;
			m_fireDelayTable = new Object();
			m_fireDelayTable[1] = 250;
			m_fireDelayTable[2] = 150;
			m_fireDelayTable[3] = 50;
			m_fireDelay = m_fireDelayTable[1];
		}// end public function BulletHolder()
				
		public override function canFire():Boolean
		{
			return (m_parent.m_lastDir != Entity.DIR_NONE && ((Game.g.timer - m_fireStartTime) > m_fireDelay));
		}// end public override function canFire():Boolean				
		
		public override function tryFire():Boolean
		{
			
			
			if (canFire())
			{
				m_parent.m_fire = false;
				m_fireStartTime = Game.g.timer;
				
				var posX:Number = 0;
				var posY:Number = 0;					
				var velX:Number = 0;
				var velY:Number = 0;				
				
				var bullet:EntityBullet;
				
				if (m_parent is EntityPlayer2)
				{
					bullet = new EntityPlayer2Bullet(0, 0, m_currUpgradeStage);
					(m_parent as EntityPlayer).doTemporaryStop();
				}
				else if(m_parent is EntityPlayer1)
				{
					bullet = new EntityPlayer1Bullet(0, 0, m_currUpgradeStage);
					(m_parent as EntityPlayer).doTemporaryStop();
				}
				else if (m_parent is EntityShooter)
				{
					bullet = new EntityShooterBullet(0, 0);
				}// end else if					
						
				switch(m_parent.m_lastDir)
				{
					
					case Entity.DIR_UP:
					{
						if (m_parent.m_vel.x)
						{
							return false;
						}// end if
						posX = m_parent.x;
						posY = m_parent.y + m_parent.m_collisionRect.y - (bullet.m_collisionRect.y + bullet.m_collisionRect.height) + m_parent.m_vel.y;
						velX  = 0;
						velY  = -1;							
						break;
					}// end case

					case Entity.DIR_DOWN:
					{
						if (m_parent.m_vel.x)
						{
							return false;
						}// end if						
						posX = m_parent.x;
						posY = m_parent.y - m_parent.m_collisionRect.y + (bullet.m_collisionRect.y + bullet.m_collisionRect.height) + m_parent.m_vel.y;						
						velX  = 0;
						velY  = 1;								
						break;
					}// end case					
					
					case Entity.DIR_LEFT:
					{
						if (m_parent.m_vel.y)
						{
							return false;
						}// end if						
						posX = m_parent.x + m_parent.m_collisionRect.x - (bullet.m_collisionRect.x + bullet.m_collisionRect.width) + m_parent.m_vel.x;
						posY = m_parent.y;						
						velX  = -1;
						velY  = 0;								
						break;
					}// end case		
					
					case Entity.DIR_RIGHT:
					{
						if (m_parent.m_vel.y)
						{
							return false;
						}// end if							
						posX = m_parent.x - m_parent.m_collisionRect.x + (bullet.m_collisionRect.x + bullet.m_collisionRect.width) + m_parent.m_vel.x;
						posY = m_parent.y;							
						velX  = 1;
						velY  = 0;									
						break;
					}// end case						
					
				}// end switch
								
				
				bullet.m_vel.x = velX;
				bullet.m_vel.y = velY;
				bullet.m_vel.normalize();
				bullet.m_vel.scaleBy(bullet.m_speed);
				bullet.m_parent = m_parent;
				bullet.x = posX;
				bullet.y = posY;
				Entity.m_spatialHashMap.updateObject(bullet);
				
				
				if (m_parent is EntityPlayer2)
				{
					Game.g.m_gameObjectList.push(new Player2ShootEffect(posX, posY, m_parent));
				}
				else if (m_parent is EntityPlayer1)
				{
					Game.g.m_gameObjectList.push(new Player1ShootEffect(posX, posY, m_parent));
				}
				else if (m_parent is EntityShooter)
				{
					Game.g.m_gameObjectList.push(new ShooterShootEffect(posX, posY, m_parent));
				}// end else if
				
				Game.g.m_gameObjectList.push(bullet);
				
				return true;
				
			}// end if			
			
			return false;
			
		}// end public override function tryFire():Boolean			
	
		public override function upgrade():Boolean
		{
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				m_fireDelay = m_fireDelayTable[m_currUpgradeStage];
				return true;
			}
			else
			{
				return false;
			}// end else
		}// end public override function upgrade():Boolean			
		
	}// end public class BulletHolder
	
}// end package