package com.nitrome.engine
{
		
	public class FloatingTextMessage extends GameObject
	{
				
		public function FloatingTextMessage(x_:Number, y_:Number, text_:String)
		{
		
			super(0, 0);		
			m_drawBitmap = Game.g.m_effectsBitmap;
			x = x_;
			y = y_ - 30;			
			message.text = text_;
			
		}// end public function FloatingTextMessage(x_:Number, y_:Number, text_:String)
		
		public override function doPhysics():void
		{
			
			if (m_terminated)
			{
				return;
			}// end if
			
			y -= 1.5;
			alpha -= 0.01;
			if (alpha <= 0)
			{
				m_terminated = true;
			}// end if
									
		}// end public override function doPhysics():void					
		
	}// end public class FloatingTextMessage
	
}// end package