package com.nitrome.engine
{
	
	
	public class EntityPlayer1Bullet extends EntityBullet
	{
			
		public function EntityPlayer1Bullet(velX_:Number, velY_:Number, powerLevel_:uint)
		{
			super(velX_, velY_, powerLevel_);
		}// end public function EntityPlayer1Bullet(velX_:Number, velY_:Number, powerLevel_:uint)	
		
		public override function get fireSoundID():String
		{
			return "Shoot";
		}// end public override function get fireSoundID():String			
		
		public override function get effect():Class
		{
			return Player1BulletImpactEffect;
		}// end public override function get effect():Class		
		
	}// end public class EntityPlayer1Bullet
	
}// end package