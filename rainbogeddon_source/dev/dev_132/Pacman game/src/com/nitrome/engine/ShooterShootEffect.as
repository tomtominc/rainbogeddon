package com.nitrome.engine
{
	
	public class ShooterShootEffect extends ShootEffect
	{

		public function ShooterShootEffect(posX_:Number, posY_:Number, parent_:EntityCharacter)
		{
		
			super(posX_, posY_, parent_);
			
		}// end public function ShooterShootEffect(posX_:Number, posY_:Number, parent_:EntityCharacter)
		
	}// end public class ShooterShootEffect
	
}// end package