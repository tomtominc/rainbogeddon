package com.nitrome.engine
{
	
	
	public class EntityEasyExplodey extends EntityExplodey 
	{
			
		public function EntityEasyExplodey(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_hasOnTargetRangeTrigger = false;
			m_glow.m_color = 0xff0045ff;
		}// end public function EntityEasyExplodey(tx_:uint, ty_:uint)
					
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnDarkBlue;
		}// end public override function get respawnEffectType():Class		
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedDarkBlue;
		}// end public override function get destroyedEffectType():Class			
		
	}// end public class EntityEasyExplodey
	
}// end package