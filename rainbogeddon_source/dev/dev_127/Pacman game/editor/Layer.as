﻿package editor {
	import editor.Global;
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Layer extends Sprite {
		
		public var layerName:String;
		
		public var tilePrefix:String = "";
		//public var tileArray:Array = null;
		
		public var tileGrid:Array /*Array*/;
		public var tileList:Array /*Tile*/;
		
		//public static var ENFORCED_WIDTH_LIMIT:Number = 30;
		//public static var ENFORCED_HEIGHT_LIMIT:Number = 25;
		
		public function Layer(name:String, prefix:String = "") {
			layerName = name;
			tilePrefix = prefix;
			
			tileGrid = [];
			tileList = [];
			
			blendMode = BlendMode.LAYER;
			
			Controller.container.addChild(this);
		}
		override public function toString():String {
			return "[Layer " + layerName + "]";
		}
		
		public function getTile(x:Number, y:Number):Tile {
			if(!tileGrid[x]) return null;
			return tileGrid[x][y];
		}
		public function getOrMakeTile(x:Number, y:Number):Tile {
			var t:Tile;
			
			t = getTile(x, y);
			if(t) return t;
			
			t = new Tile(x, y, this);
			tileList.push(t);
			if(!tileGrid[x]) tileGrid[x] = [];
			tileGrid[x][y] = t;
			
			return t;
		}
		
		public function drawTile(x:Number, y:Number, type:Class):void {
			if(type) {
				getOrMakeTile(x, y).setType(type);
			} else {
				eraseTile(x, y);
			}
		}
		
		public function eraseTile(x:Number, y:Number):void {
			var t:Tile = getTile(x, y);
			if(!t) return;
			
			t.setType(null);
			Global.removeOnceFromList(t, tileList);
			tileGrid[x][y] = null;
		}
		
		public function clear():void {
			for(var n:Number = 0; n < tileList.length; n ++) {
				tileList[n].setType(null);
			}
			tileList = [];
			tileGrid = [];
		}
		
		public function get leftmostX():Number {
			return Global.safeMinimum(Global.childrenList(tileList, "leftmostX"));
		}
		public function get rightmostX():Number {
			//var max:Number = Global.safeMaximum(Global.childrenList(tileList, "gridX"));
			//var min:Number = Global.safeMinimum(Global.childrenList(tileList, "gridX"));
			//return Math.min(min + ENFORCED_WIDTH_LIMIT - 1, max);
			return Global.safeMaximum(Global.childrenList(tileList, "rightmostX"));
		}
		public function get topmostY():Number {
			return Global.safeMinimum(Global.childrenList(tileList, "topmostY"));
		}
		public function get bottommostY():Number {
			//var max:Number = Global.safeMaximum(Global.childrenList(tileList, "gridY"));
			//var min:Number = Global.safeMinimum(Global.childrenList(tileList, "gridY"));
			//return Math.min(min + ENFORCED_HEIGHT_LIMIT - 1, max);
			return Global.safeMaximum(Global.childrenList(tileList, "bottommostY"));
		}
		
		public function minimizeName(name:String):String {
			if(tilePrefix && tilePrefix != "") {
				if(name.substr(0, tilePrefix.length) == tilePrefix)
					name = name.substr(tilePrefix.length);
			}
			/*if(tileArray) {
				for(var n:Number = 0; n < tileArray.length; n ++) {
					if(name == tileArray[n]) {
						name = n.toString();
						break;
					}
				}
			}*/
			return name;
		}
		
		public function hasAny():Boolean {
			for(var n:Number = 0; n < tileList.length; n ++) {
				var s:String = tileList[n].getTypeName();
				if(s && s != "-" && s != "") {
					return true;
				}
			}
			return false;
		}
		
		public function serializeRange(left:Number, right:Number, top:Number, bottom:Number):String {
			var list:Array = [];
			for(var y:Number = top; y <= bottom; y ++) {
				for(var x:Number = left; x <= right; x ++) {
					if (getTile(x, y)) {
						var typeName:String = Tile(tileGrid[x][y]).getTypeName();
						list.push(minimizeName(typeName));
					} else {
						list.push("-");
					}
				}
			}
			return serialize(list);
		}
		public function serialize(a:Array):String {
			var s:String = "";
			var repeatCount:Number = 0;
			for(var n=0; n<a.length; n++) {
				repeatCount ++;
				if(a[n] != a[n+1]) {
					s += a[n];
					if(repeatCount > 1) s += ":" + repeatCount;
					if(n < a.length-1) s += ",";
					repeatCount = 0;
				}
			}
			return s;
		}
		
		public function unserializeRange(s:String, firstX:Number, firstY:Number, width:Number, height:Number):void {
			var list:Array = unserialize(s);
			var n:Number = 0;
			for(var y:Number = firstY; y < firstY + height; y ++) {
				for(var x:Number = firstX; x < firstX + width; x ++) {
					if(list[n] && list[n] != "-") {
						//if(tileArray)
						//	drawTile(x, y, tilePrefix + tileArray[Number(list[n])]);
						//else
						//	drawTile(x, y, tilePrefix + list[n]);
						
						try {
							var classRef:Class = getDefinitionByName(tilePrefix + list[n]) as Class;
							drawTile(x, y, classRef);
						} catch(e:Error) {
							Alert.alert("setType() failed with obj " +
								(tilePrefix + list[n]).toUpperCase() + " on layer " + layerName.toUpperCase());
							getOrMakeTile(x, y).setInvalidType(tilePrefix + list[n]);
						}
					}
					n ++;
				}
			}
		}
		public function unserialize(s:String):Array {
			var splitApart:Array = s.split(",");
			var unserialized:Array = [];
			for(var n:Number = 0; n < splitApart.length; n++) {
				var entry:String = splitApart[n];
				if(entry.indexOf(":") == -1) {
					unserialized.push(Global.trim(entry));
				} else {
					var entrySplitApart:Array = entry.split(":");
					var repeatedEntry:String = Global.trim(entrySplitApart[0]);
					var repeatCount:Number = Number(entrySplitApart[1]);
					for(var repeat:Number = 0; repeat < repeatCount; repeat ++) {
						unserialized.push(repeatedEntry);
					}
				}
			}
			return unserialized;
		}
		
	}
	
}
