﻿/**
* TextComponent
* @author Aaron Steed, nitrome.com
* @version 3.0
*
* This component coverts a string into a collection of Sprites
*/

package com.nitrome.ui {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.events.Event;
	import flash.display.DisplayObject;

	public class TextBox extends Sprite{
		
		public var lines:Array;							// a 2D array of all the bitmapDatas used, in lines
		public var lineWidths:Array;					// the width of each line of text (used for alignment)
		public var textLines:Array;						// a 2D array of the characters used (used for fetching offset and kerning data)
		
		protected var id:String;						// font id
		protected var mixedCase:Boolean;				// does the font have upper and lower case letters
		
		public var bitmapData:BitmapData;				// the image the font is painted to
		public var bitmap:Bitmap;						// the image holder
		
		// Editable properties
		protected var _text:String;						// text: the string the component displays
		protected var _tracking:int;					// tracking: the spacing between letters
		protected var _align:String;					// align: whether the text is centered, left or right aligned
		protected var _alignVert:String;				// align_vert: vertical alignment of the text
		protected var _lineSpacing:int;					// line_spacing: distance between each line of copy
		protected var _colorInt:uint;					// the actual uint of the color being applied
		protected var _color:ColorTransform;			// a color transform object that is applied to the whole TextBox
		protected var _wordWrap:Boolean;				// turns wordWrap on and off
		
		protected var whitespaceLength:int;				// the distance a whitespace takes up
		
		private var previewing:Boolean;
		private var _verifyFont:Boolean;
		
		public var characters:Array/*BitmapData*/;
		public var offsets:Array/*Point*/;
		public var kerning:Array/*Point*/;
		
		protected static var initSets:Array = [];
		protected static var characterSets:Array = [];
		protected static var offsetsSets:Array = [];
		protected static var kerningSets:Array = [];
		
		// =====================================================================================
		
		// COMPONENT OVERRIDES
		
		// Component variable override
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		
		// NB Do not override getter setters for width and height - it breaks the live preview functionality
		
		// Overrides hidden component function
		public function setSize(w:Number, h:Number):void {
			_width = w;
			_height = h;
			if(_wordWrap) updateText();
			draw();
		}
		
		// =====================================================================================
		
		public function TextBox() {
			if(!initSets[id]) initCharacterSet(id);
			init();
			draw();
		}
		
		// Initialise
		private function init():void{
			_width = width;
			_height = height;
			scaleX = scaleY = 1;
			// delete the avatar
			removeChildAt(0);
			previewing = true;
			lines = [];
			textLines = [];
			lineWidths = [];
			_text = "";
			_verifyFont = false;
			characters = characterSets[id];
			offsets = offsetsSets[id];
			kerning = kerningSets[id];
		}
		
		/* Creates an ascociative array for fetching bitmapDatas with a character
		 * And uses the assets object to calculate the offset required
		 * on each bitmapData*/
		private static function initCharacterSet(id:String):void{
			
			var assetsClipClass:Class = getDefinitionByName(id + "_Assets") as Class;
			var assetsClip:Sprite = new assetsClipClass();
			
			var i:int, j:int, clip:MovieClip, bitmapData:BitmapData, bitmap:Bitmap, charName:String, bounds:Rectangle;
			var characters = [], offsets = [], kerning = null;
			
			for(i = 0; i < assetsClip.numChildren; i++){
				clip = assetsClip.getChildAt(i) as MovieClip;
				
				// guess what folks, that bitmap you put in the assets clip, that's not a bitmap
				// it's A SHAPE!
				
				// for some reason, BitmapDatas are banned in component land - to the extent that there's
				// some really weird crap going on, (a BitmapData class in the Library won't even compile)
				// but we're going to soldier on and get a goddamned BitmapData by drawing the stupid Shape.
				
				bounds = clip.getBounds(clip);
				bitmapData = new BitmapData(Math.ceil(bounds.width), Math.ceil(bounds.height), true, 0x00000000);
				bitmapData.draw(clip, new Matrix(1, 0, 0, 1, -bounds.left, -bounds.top));
				
				charName = clip.name;
				
				// instances on the stage are not allowed to be named numbers
				if(charName.indexOf("NUMBER_") > -1) charName = charName.substr(charName.indexOf("NUMBER_") + 7);
				
				characters[charName] = bitmapData;
				offsets[charName] = new Point(bounds.left, bounds.top);
				
				// search for a kerning clip
				for(j = 0; j < clip.numChildren; j++){
					if(clip.getChildAt(j) as MovieClip){
						if(!kerning) kerning = [];
						kerning[charName] = new Point();
						kerning[charName].x = clip.getChildAt(j).x - characters[charName].width;
					}
				}
			}
			characterSets[id] = characters;
			offsetsSets[id] = offsets;
			kerningSets[id] = kerning;
			initSets[id] = true;
			
			//trace("TextBox " + id + " characters initialised");
		}
		
		// Getter Setters
		
		/* Because Flash components call their inspectable properties sometimes long after they have been added to the stage
		*  we have a previewText property. This shows the placeholder text for the component. If runtime code alters the "text"
		*  property, any future calls from the inspectable "previewText" will be ignored. */
		
		// preview_text
		[Inspectable(defaultValue = "enter text")]
		public function get previewText():String{
			return _text;
		}
		public function set previewText(str:String):void{
			if(previewing){
				_text = str;
				updateText();
				draw();
			}
		}
		
		public function get text():String{
			return _text;
		}
		public function set text(str:String):void{
			previewing = false;
			_text = str;
			updateText();
			draw();
		}
		
		// tracking
		public function get tracking():int{
			return _tracking;
		}
		public function set tracking(n:int):void{
			_tracking = n;
			updateText();
			draw();
		}
		
		// line_spacing
		public function get lineSpacing():int{
			return _lineSpacing;
		}
		public function set lineSpacing(n:int):void{
			_lineSpacing = n;
			draw();
		}
		
		// align
		[Inspectable(defaultValue="center", type=Array, enumeration="left,center,right")]
		public function get align():String{
			return _align;
		}
		public function set align(str:String){
			_align = str;
			draw();
		}
		
		// alignVert
		[Inspectable(defaultValue="center", type=Array, enumeration="top,center,bottom")]
		public function get alignVert():String{
			return _alignVert;
		}
		public function set alignVert(str:String){
			_alignVert = str;
			draw();
		}
		
		// color
		[Inspectable(type=Color, defaultValue=FFFFFF)]
		public function get color():uint {
			return _colorInt;
		}
		public function set color(c:uint):void {
			_colorInt = c;
			if(c == 0xFFFFFF) {
				_color = null;
			} else {
				_color = new ColorTransform(
					((c >> 16) % 256) / 255,
					((c >> 8) % 256) / 255,
					(c % 256) / 255);
			}
			if(_color && bitmap) bitmap.transform.colorTransform = _color;
		}
		
		// word wrap
		[Inspectable(defaultValue=true)]
		public function get wordWrap():Boolean {
			return _wordWrap;
		}
		public function set wordWrap(value:Boolean):void {
			_wordWrap = value;
			updateText();
			draw();
		}
		
		/* Calculates an array of BitmapDatas needed to render the text */
		protected function updateText():void{
			
			// we create an array called lines that holds references to all of the
			// bitmapDatas needed and structure it like the text
			
			// the lines property is public so it can be used to ticker text
			lines = [];
			lineWidths = [];
			textLines = [];
			
			var currentLine:Array = [];
			var currentTextLine:Array = [];
			var wordBeginning:int = 0;
			var currentLineWidth:int = 0;
			var completeWordsWidth:int = 0;
			var wordWidth:int = 0;
			var newLine:Array = [];
			var newTextLine:Array = [];
			var c:String;
			
			if(!_text) _text = "";
			
			var upperCaseText:String = _text.toUpperCase();
			
			for(var i:int = 0; i < upperCaseText.length; i++){
				
				c = upperCaseText.charAt(i);
				
				// Flash does not recognise case when naming stage variables
				// so lower case letters must be identified in another way
				if(mixedCase){
					if(c != _text.charAt(i)){
						c += "_LOWER";
					}
				}
				
				// next we swap the special characters for descriptive strings
				if(c == " ") c = "SPACE";
				else if(c == ".") c = "PERIOD";
				else if(c == "?") c = "QUESTION";
				else if(c == ",") c = "COMMA";
				else if(c == "!") c = "EXCLAMATION";
				else if(c == "\\") c = "BACKSLASH";
				else if(c == "/") c = "FORWARDSLASH";
				else if(c == "=") c = "EQUALS";
				else if(c == "+") c = "PLUS";
				else if(c == "(") c = "LEFT_BRACKET";
				else if(c == ")") c = "RIGHT_BRACKET";
				else if(c == "-") c = "HYPHEN";
				else if(c == "\"") c = "QUOTES";
				else if(c == ":") c = "COLON";
				else if(c == "£") c = "POUND";
				else if(c == "_") c = "UNDERSCORE";
				else if(c == "'") c = "APOSTROPHE";
				else if(c == "@") c = "AT";
				else if(c == "&") c = "AMPERSAND";
				else if(c == "$") c = "DOLLAR";
				else if(c == "*") c = "ASTERISK";
				else if(c == ";") c = "SEMICOLON";
				else if(c == "%") c = "PERCENT";
				else if(c == "~") c = "TILDE";
				else if(c == "{") c = "LEFT_BRACE";
				else if(c == "}") c = "RIGHT_BRACE";
				
				// new line characters
				if(c == "\n" || c == "\r" || c == "|"){
					lines.push(currentLine);
					textLines.push(currentTextLine);
					lineWidths.push(currentLineWidth);
					currentLineWidth = 0;
					completeWordsWidth = 0;
					wordBeginning = 0;
					wordWidth = 0;
					currentLine = [];
					currentTextLine = [];
					continue;
				}
				
				// push a character into the array
				if(characters[c]){
					// check we're in the middle of a word - spaces are null
					if(currentLine.length > 0 && currentLine[currentLine.length -1]){
						currentLineWidth += tracking;
						wordWidth += tracking;
						// the previous character will provide kerning data (if this font has any)
						if(kerning && kerning[currentTextLine[currentTextLine.length - 1]]){
							currentLineWidth += kerning[currentTextLine[currentTextLine.length - 1]].x;
							wordWidth += kerning[currentTextLine[currentTextLine.length - 1]].x;
						}
					}
					wordWidth += characters[c].width
					currentLineWidth += characters[c].width;
					currentLine.push(characters[c]);
					currentTextLine.push(c);
				
				// the character is a SPACE or unrecognised and will be treated as a SPACE
				} else {
					if(currentLine.length > 0 && currentLine[currentLine.length - 1]){
						completeWordsWidth = currentLineWidth;
					}
					currentLineWidth += whitespaceLength;
					currentLine.push(null);
					currentTextLine.push(null);
					wordBeginning = currentLine.length;
					wordWidth = 0;
				}
				
				// if the length of the current line exceeds the width, we splice it into the next line
				// effecting word wrap
				
				if(currentLineWidth > _width && _wordWrap){
					// in the case where the word is larger than the text field we take back the last character
					// and jump to a new line with it
					if(wordBeginning == 0 && currentLine[currentLine.length - 1]){
						currentLineWidth -= tracking + currentLine[currentLine.length - 1].width;
						// the previous character will provide kerning data (if this font has any)
						if(kerning && currentTextLine.length > 1 && kerning[currentTextLine[currentTextLine.length - 2]]){
							currentLineWidth -= kerning[currentTextLine[currentTextLine.length - 2]].x;
						}
						// now we take back the offending last character
						var lastBitmapData:BitmapData = currentLine.pop();
						var lastChar:String = currentTextLine.pop();
						
						lines.push(currentLine);
						textLines.push(currentTextLine);
						lineWidths.push(currentLineWidth);
						
						currentLineWidth = lastBitmapData.width;
						completeWordsWidth = 0;
						wordBeginning = 0;
						wordWidth = lastBitmapData.width;
						currentLine = [lastBitmapData];
						currentTextLine = [lastChar];
						continue;
					}
					
					newLine = currentLine.splice(wordBeginning, currentLine.length - wordBeginning);
					newTextLine = currentTextLine.splice(wordBeginning, currentTextLine.length - wordBeginning);
					lines.push(currentLine);
					textLines.push(currentTextLine);
					lineWidths.push(completeWordsWidth);
					completeWordsWidth = 0;
					wordBeginning = 0;
					currentLine = newLine;
					currentTextLine = newTextLine;
					currentLineWidth = wordWidth;
				}
			}
			// save the last line
			lines.push(currentLine);
			textLines.push(currentTextLine);
			lineWidths.push(currentLineWidth);
		}
		
		/* Render */
		public function draw():void{
			
			if(bitmap) removeChild(bitmap);
			bitmapData = new BitmapData(_width, _height, true, 0x00000000);
			bitmap = new Bitmap(bitmapData);
			addChild(bitmap);
			
			var i:int, j:int;
			var point:Point = new Point();
			var x:int;
			var y:int = 0;
			var alignX:int;
			var alignY:int;
			var char:BitmapData;
			var offset:Point;
			var wordBeginning:int = 0;
			var linesHeight:int = lineSpacing * lines.length;
			
			for(i = 0; i < lines.length; i++, point.y += lineSpacing){
				x = 0;
				wordBeginning = 0;
				for(j = 0; j < lines[i].length; j++){
					char = lines[i][j];
					
					// alignment to bitmap
					if(_align == "left"){
						alignX = 0;
					} else if(_align == "center"){
						alignX = _width * 0.5 - lineWidths[i] * 0.5;
					} else if(_align == "right"){
						alignX = _width - lineWidths[i];
					}
					if(_alignVert == "top"){
						alignY = 0;
					} else if(_alignVert == "center"){
						alignY = _height * 0.5 - linesHeight * 0.5;
					} else if(_alignVert == "bottom"){
						alignY = _height - linesHeight;
					}
					
					// print to bitmapdata
					if(char){
						if(j > wordBeginning){
							x += tracking;
							if(kerning){
								if(kerning[textLines[i][j - 1]]){
									x += kerning[textLines[i][j - 1]].x;
								}
							}
						}
						offset = offsets[textLines[i][j]];
						point.x = alignX + x + offset.x;
						point.y = alignY + y + offset.y;
						bitmapData.copyPixels(char, char.rect, point, null, null, true);
						x += char.width;
					} else {
						x += whitespaceLength;
						wordBeginning = j + 1;
					}
				}
				y += lineSpacing;
			}
			
			if(_color) bitmap.transform.colorTransform = _color;
			
			// if the component is a Live Preview and there is no text, draw a box
			var isLivePreview:Boolean = (parent != null && getQualifiedClassName(parent) == "fl.livepreview::LivePreviewParent");
			if(isLivePreview && _text == ""){
				bitmapData.fillRect(bitmapData.rect, 0xFF000000);
				bitmapData.fillRect(new Rectangle(1, 1, bitmapData.width - 2, bitmapData.height - 2), 0x88FFFF99);
			}
		}
	}
}
