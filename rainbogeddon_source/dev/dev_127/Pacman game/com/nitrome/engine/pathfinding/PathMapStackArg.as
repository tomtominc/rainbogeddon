package com.nitrome.engine.pathfinding 
{
	
	public class PathMapStackArg
	{
		
		public  var m_x:int;
		public  var m_y:int;
		public  var m_dist:uint;
		
		public function PathMapStackArg(x_:int, y_:int, dist_:uint)
		{
			
			m_x = x_;
			m_y = y_;
			m_dist = dist_;
			
		}// end public function PathMapStackArg(x_:uint, y_:uint, dist_:uint)
	
		
	}// end public class PathMapStackArg

}// end package