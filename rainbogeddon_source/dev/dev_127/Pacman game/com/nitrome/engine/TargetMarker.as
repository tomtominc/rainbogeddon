package com.nitrome.engine
{
	
	public class TargetMarker extends EffectCentered
	{
		
		private var m_parent:Entity;
				
		public function TargetMarker(parent_:Entity)
		{
		
			super(0, 0);		
			m_parent = parent_;
			
		}// end public function TargetMarker(parent_:Entity)
		
		public override function doPhysics():void
		{
			
			x = m_parent.x;
			y = m_parent.y;
											
		}// end public override function doPhysics():void				
	
		
	}// end public class TargetMarker
	
}// end package