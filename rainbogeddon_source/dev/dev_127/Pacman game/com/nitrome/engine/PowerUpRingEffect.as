package com.nitrome.engine
{
	
	public class PowerUpRingEffect extends EffectCentered
	{
		
		public function PowerUpRingEffect()
		{
		
			super(0, 0);
			
		}// end public function PowerUpRingEffect()
				
		public function setPowerlevel(powerLevel_:uint)
		{
			gotoAndPlay("level_" + powerLevel_);
		}// end public function setPowerlevel(powerLevel_:uint)
		
	}// end public class PowerUpRingEffect
	
}// end package