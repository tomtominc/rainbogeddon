package com.nitrome.engine
{

	import com.nitrome.sound.SoundManager;
	
	public class EntityBullet extends EntityCentered
	{
			
		public static const SPEED:Number = 14.5, //8.5
							DAMAGE:int = 10;	
		
		public  var m_damage:int;
		public  var m_wrapped:Boolean;
		public  var m_wrapTimes:uint;
		public  var m_parent:Entity;
		public  var m_damageTable:Object;
		private var m_powerLevel:uint;
		
		public function EntityBullet(velX_:Number, velY_:Number, powerLevel_:uint)
		{
			super(0, 0);
			m_speed = SPEED;
			m_collisionRect.x = -5;
			m_collisionRect.y = -5;
			m_collisionRect.width = 10;
			m_collisionRect.height = 10;
			m_vel.x = velX_;
			m_vel.y = velY_;
			m_vel.normalize();
			m_vel.scaleBy(SPEED);
			m_damage = DAMAGE;
			m_wrapped = false;
			m_parent = null;
			m_wrapTimes = 0;
			m_canDie = false;
			m_damageTable = new Object();
			m_damageTable[1] = 10;
			m_damageTable[2] = 20;
			m_damageTable[3] = 30;
			m_damage = m_damageTable[powerLevel_];
			gotoAndPlay("level_" + powerLevel_);
			m_powerLevel = powerLevel_;
			SoundManager.playSound(fireSoundID);
			//m_spatialHashMap.updateObject(this);
		}// end public function EntityBullet(tx_:uint, ty_:uint, powerLevel_:uint)
				
	
		public override function doPhysics():void
		{
			
			var ignoreList:Vector.<Class> = new Vector.<Class>;
			ignoreList.push(EntityCollectible);
			ignoreList.push(EntityBullet);				
			
			var collidingObject:Entity;
			
			if(!m_wrapped)
			{
				
				/*if (m_parent is EntityPlayer)
				{
					ignoreList.push(EntityPlayer);	
				}// end if*/
				
				collidingObject = getCollidingObject(ignoreList, m_parent);
			}
			else
			{
				
				/*if (m_parent is EntityPlayer1)
				{
					collidingObject = getCollidingObject(ignoreList, Game.g.m_player2);
				}
				else if (m_parent is EntityPlayer2)
				{
					collidingObject = getCollidingObject(ignoreList, Game.g.m_player1);
				}
				else
				{*/
					collidingObject = getCollidingObject(ignoreList);
				//}// end else
			
			}// end else
			
			

			if (collidingObject)
			{
				
   				collidingObject.doDamage(m_damage, m_vel, m_parent, this);	
				m_terminated = true;
				m_spatialHashMap.unregisterObject(this);
				
				if (collidingObject is EntityHardBlock)
				{
					SoundManager.playSound("BulletHitHardWall");
				}
				else if (collidingObject is EntityEnemy)
				{
					SoundManager.playSound("EnemyHitByBullet");
				}
				else
				{
					SoundManager.playSound("BulletHitWall");
				}// end else
				
				Game.g.m_gameObjectList.push(new effect(x + m_vel.x, y + m_vel.y, m_powerLevel));
				return;
				
			}// end if
			
			updatePos();
			
			if (m_vel.x > 0)
			{
				rotation = 180;
			}
			else if (m_vel.x < 0)
			{
				rotation = 0;
			}
			else if (m_vel.y < 0)
			{
				rotation = 90;
			}
			else if (m_vel.y > 0)
			{
				rotation = 270;
			}// end else if
			
			m_spatialHashMap.updateObject(this);
			
		}// end public override function doPhysics():void		
		
		public function get fireSoundID():String
		{
			return null;
		}// end public function get fireSoundID():String		
		
		public function get effect():Class
		{
			return null;
		}// end public function get effect():Class
		
		public override function onWrapped():void
		{
			m_wrapped = true;
			m_wrapTimes++;
			m_drawWrapped = false;
			
			if (m_wrapTimes >= 2)
			{
				m_terminated = true;
				super.onDeath();
			}// end if
			
		}// end public override function onWrapped():void			
		
	}// end public class EntityBullet
	
}// end package