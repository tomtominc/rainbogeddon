package com.nitrome.engine
{
	
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.utils.getTimer;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import fl.motion.Color;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import com.nitrome.sound.SoundManager;
	
	public class EnemyRespawn extends EffectCentered
	{
				
		private var m_parent:Entity;
		public var m_timelineCode:Boolean;

		public function EnemyRespawn(parent_:Entity)
		{
		
			m_timelineCode = false;
			m_drawFast = true;
			super(parent_.m_cTX, parent_.m_cTY);		
			m_parent = parent_;	
			m_timelineCode = true;
			SoundManager.playSound("EnemyRespawnSound");
			
		}// end public function EnemyRespawn(parent_:Entity)
		
		public function respawnDone()
		{
			
			if (m_timelineCode)
			{
			
				m_terminated = true;

				// create an enemy of the same type as the parent
				var enemy:EntityEnemy = new (getDefinitionByName(getQualifiedClassName(m_parent)) as Class)(m_tx, m_ty);
				
				Game.g.m_gameObjectList.push(enemy);	
				Entity.m_spatialHashMap.registerObject(enemy);
				enemy.onRespawn();
						
				
			}// end if
			
		}// end public function respawnDone()
		
	}// end public class EnemyRespawn
	
}// end package