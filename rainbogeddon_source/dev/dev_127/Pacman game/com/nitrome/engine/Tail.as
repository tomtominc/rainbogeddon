package com.nitrome.engine
{
	import flash.geom.Vector3D;

	
	public class Tail
	{
		
		public var m_follower:Entity;
		public var m_leader:Entity;
		public var m_root:Entity;
		
		public function Tail(follower_:Entity, leader_:Entity, root_:Entity)
		{
			m_leader = leader_;
			m_root = root_;
			m_follower = follower_;
		}// end public function Tail(follower_:Entity, leader_:Entity, root_:Entity)
	
			
		public function doPhysics():void
		{
			
			var leaderPos:Vector3D = new Vector3D(m_leader.x, m_leader.y);
			var currPos:Vector3D = new Vector3D(m_follower.x, m_follower.y);
			
			var changeX:Boolean = false;
			var changeY:Boolean = false;
			
			var leaderToPos:Vector3D = leaderPos.subtract(currPos);
			
			// check if the leader has wrapped around the screen
			if (Math.abs(leaderToPos.x) > (Game.WIDTH / 2))
			{
				if (leaderToPos.x > 0)
				{
					leaderToPos.x -= Game.g.mapWidth * Game.TILE_WIDTH;
					leaderPos.x -= Game.g.mapWidth * Game.TILE_WIDTH;
				}
				else
				{
					leaderToPos.x += Game.g.mapWidth * Game.TILE_WIDTH;
					leaderPos.x += Game.g.mapWidth * Game.TILE_WIDTH;
				}// end else					
			}
			else if (Math.abs(leaderToPos.y) > (Game.WIDTH / 2))
			{
				if (leaderToPos.y > 0)
				{
					leaderToPos.y -= Game.g.mapHeight * Game.TILE_HEIGHT;
					leaderPos.y -= Game.g.mapHeight * Game.TILE_HEIGHT;
				}
				else
				{
					leaderToPos.y += Game.g.mapHeight * Game.TILE_HEIGHT;
					leaderPos.y += Game.g.mapHeight * Game.TILE_HEIGHT;
				}// end else
			}// end else if
			
			if (!leaderToPos.x || !leaderToPos.y)
			{
				
				if (leaderToPos.length > Game.TILE_WIDTH)
				{
					var leaderToPosLength:Number = leaderToPos.length;
					leaderToPos.normalize();
					leaderToPos.scaleBy(leaderToPosLength - Game.TILE_WIDTH);
					m_follower.x += leaderToPos.x;
					m_follower.y += leaderToPos.y;
					m_follower.m_vel.x = m_leader.m_vel.x;
					m_follower.m_vel.y = m_leader.m_vel.y;	
					
					if (leaderToPos.x)
					{
						changeX = true;
					}
					else if (leaderToPos.y)
					{
						changeY = true;
					}// end else if
					
				}
				else
				{
					m_follower.m_vel.x = 0;
					m_follower.m_vel.y = 0;
				}// end else
				
			}
			else
			{
				
				var tX:int;
				var tY:int;
				
				if (Math.abs(leaderToPos.x) + Math.abs(leaderToPos.y) > Game.TILE_WIDTH)
				{
										
					var adjLength:Number = (Math.abs(leaderToPos.x) + Math.abs(leaderToPos.y)) - Game.TILE_WIDTH;
					var adjVec:Vector3D;
					var diff:Number;
					
					if (m_leader.m_vel.x)
					{
						adjVec = new Vector3D(0, leaderToPos.y);
						adjVec.normalize();
						adjVec.scaleBy(adjLength);
						
						if (m_follower.y < leaderPos.y)
						{
							
							m_follower.y += adjVec.y;		
							if (m_follower.y > leaderPos.y)
							{
								diff = Math.abs(m_follower.y - leaderPos.y);
								m_follower.y = leaderPos.y;
								if (m_follower.x < leaderPos.x)
								{
									m_follower.x += diff;
								}
								else
								{
									m_follower.x -= diff;
								}// end else
								
							}// end if
							
						}
						else
						{
							
							m_follower.y += adjVec.y;					
							if (m_follower.y < leaderPos.y)
							{
								diff = Math.abs(m_follower.y - leaderPos.y);
								m_follower.y = leaderPos.y;
								
								if (m_follower.x < leaderPos.x)
								{
									m_follower.x += diff;
								}
								else
								{
									m_follower.x -= diff;
								}// end else								
								
							}// end if									
							
						}// end else	
						
						changeY = true;
						m_follower.m_vel.x = 0;
						m_follower.m_vel.y = -m_leader.m_vel.x;			
						tX = m_follower.x / Game.TILE_WIDTH;
						m_follower.x = (Game.TILE_WIDTH * tX) + (Game.TILE_WIDTH / 2);								
						
					}
					else if (m_leader.m_vel.y)
					{
						adjVec = new Vector3D(leaderToPos.x, 0);
						adjVec.normalize();
						adjVec.scaleBy(adjLength);		
						
						if (m_follower.x < leaderPos.x)
						{
							m_follower.x += adjVec.x;					
							if (m_follower.x > leaderPos.x)
							{
								diff = Math.abs(m_follower.x - leaderPos.x);
								m_follower.x = leaderPos.x;
								
								if (m_follower.y < leaderPos.y)
								{
									m_follower.y += diff;
								}
								else
								{
									m_follower.y -= diff;
								}// end else								
								
							}// end if
							
						}
						else
						{
							
							m_follower.x += adjVec.x;					
							if (m_follower.x < leaderPos.x)
							{
								diff = Math.abs(m_follower.x - leaderPos.x);
								m_follower.x = leaderPos.x;
								
								if (m_follower.y < leaderPos.y)
								{
									m_follower.y += diff;
								}
								else
								{
									m_follower.y -= diff;
								}// end else									
								
							}// end if									
							
						}// end else									
						
						changeX = true;
						m_follower.m_vel.x = -m_leader.m_vel.y;
						m_follower.m_vel.y = 0;	
						tY = m_follower.y / Game.TILE_HEIGHT;
						m_follower.y = (Game.TILE_HEIGHT * tY) + (Game.TILE_HEIGHT / 2);								
					}
					else
					{
						
						if (leaderToPos.x < leaderToPos.y)
						{
							adjVec = new Vector3D(leaderToPos.x, 0);
							adjVec.normalize();
							adjVec.scaleBy(adjLength);								
							
							if (m_follower.x < leaderPos.x)
							{
								m_follower.x += adjVec.x;					
								if (m_follower.x > leaderPos.x)
								{
									diff = Math.abs(m_follower.x - leaderPos.x);
									m_follower.x = leaderPos.x;
									
									if (m_follower.y < leaderPos.y)
									{
										m_follower.y += diff;
									}
									else
									{
										m_follower.y -= diff;
									}// end else										
									
								}// end if
								
							}
							else
							{
								
								m_follower.x += adjVec.x;					
								if (m_follower.x < leaderPos.x)
								{
									diff = Math.abs(m_follower.x - leaderPos.x);
									m_follower.x = leaderPos.x;
									
									if (m_follower.y < leaderPos.y)
									{
										m_follower.y += diff;
									}
									else
									{
										m_follower.y -= diff;
									}// end else										
									
								}// end if									
								
							}// end else										
							
							changeX = true;
							m_follower.m_vel.x = adjVec.x;
							m_follower.m_vel.y = 0;	
							tY = m_follower.y / Game.TILE_HEIGHT;
							m_follower.y = (Game.TILE_HEIGHT * tY) + (Game.TILE_HEIGHT / 2);									
						}
						else //if (leaderToPos.x >= leaderToPos.y)
						{
							adjVec = new Vector3D(0, leaderToPos.y);
							adjVec.normalize();
							adjVec.scaleBy(adjLength);										
							
							if (m_follower.y < leaderPos.y)
							{
								
								m_follower.y += adjVec.y;					
								if (m_follower.y > leaderPos.y)
								{
									diff = Math.abs(m_follower.y - leaderPos.y);
									m_follower.y = leaderPos.y;
									
									if (m_follower.x < leaderPos.x)
									{
										m_follower.x += diff;
									}
									else
									{
										m_follower.x -= diff;
									}// end else									
									
								}// end if
								
							}
							else
							{
								
								m_follower.y += adjVec.y;					
								if (m_follower.y < leaderPos.y)
								{
									diff = Math.abs(m_follower.y - leaderPos.y);
									m_follower.y = leaderPos.y;
									
									if (m_follower.x < leaderPos.x)
									{
										m_follower.x += diff;
									}
									else
									{
										m_follower.x -= diff;
									}// end else									
									
								}// end if									
								
							}// end else									
							
							changeY = true;
							m_follower.m_vel.x = 0;
							m_follower.m_vel.y = adjVec.y;		
							tX = m_follower.x / Game.TILE_WIDTH;
							m_follower.x = (Game.TILE_WIDTH * tX) + (Game.TILE_WIDTH / 2);										
						}// end else if
						
					}// end else
										
				}
				else
				{
					m_follower.m_vel.x = 0;
					m_follower.m_vel.y = 0;								
				}// end else
				
			}// end else
			
			if (changeX)
			{
				m_follower.m_vel.y = 0;
				m_follower.m_vel.x = m_follower.x - currPos.x;
			}
			else if (changeY)
			{
				m_follower.m_vel.x = 0;
				m_follower.m_vel.y = m_follower.y - currPos.y;				
			}// end else if
			
			if (m_follower.x >= Game.g.mapWidth * Game.TILE_WIDTH)
			{
				m_follower.x -=  Game.g.mapWidth * Game.TILE_WIDTH;
			}
			else if (m_follower.x < 0)
			{
				m_follower.x += Game.g.mapWidth * Game.TILE_WIDTH;
			}// end else if
		
			if (m_follower.y >= Game.g.mapHeight * Game.TILE_HEIGHT)
			{
				m_follower.y -=  Game.g.mapHeight * Game.TILE_HEIGHT;
			}
			else if (m_follower.y < 0)
			{
				m_follower.y += Game.g.mapHeight * Game.TILE_HEIGHT;
			}// end else if				
			
		}// end public function doPhysics():void		
				
		
	}// end public class Tail
	
}// end package