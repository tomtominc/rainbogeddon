package com.nitrome.engine
{
	
	
	public class EntityCentered extends Entity 
	{
			
		
		public function EntityCentered(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);			
			x = (Game.TILE_WIDTH * tx_)  + (Game.TILE_WIDTH / 2);
			y = (Game.TILE_HEIGHT * ty_) + (Game.TILE_HEIGHT / 2);	
			
		}// end public function EntityCentered(tx_:uint, ty_:uint)
		
		public override function updatePosFromTile():void
		{
			x = (Game.TILE_WIDTH * m_tx) + (Game.TILE_WIDTH / 2);
			y = (Game.TILE_HEIGHT * m_ty) + (Game.TILE_HEIGHT / 2);			
		}// end public override function updatePosFromTile():void			
		
		public override function updateTilePos():void
		{				
			m_tx = (x - (Game.TILE_WIDTH / 2))/Game.TILE_WIDTH;
			m_ty = (y - (Game.TILE_HEIGHT / 2)) / Game.TILE_HEIGHT;			
		}// end public override function updateTilePos():void		
		
		public override function getTileXforX(x_:Number):int
		{			
			return Math.floor((x_ - (Game.TILE_WIDTH / 2))/Game.TILE_WIDTH);	
		}// end public function getTileXforX(x_:Number):int	
		
		public override function getTileYforY(y_:Number):int
		{
			return Math.floor((y_ - (Game.TILE_HEIGHT / 2))/Game.TILE_HEIGHT);	
		}// end public function getTileYforY(y_:Number):int	
				
		public function flipX(setting_:Boolean):void
		{

			if (setting_)
			{
				scaleX = -1;
			}
			else
			{
				scaleX = 1;
			}// end else	
			
		}// end public function flipX(setting_:Boolean):void	
		
		public function flipY(setting_:Boolean):void
		{
			
			if (setting_)
			{
				scaleY = -1;
			}
			else
			{
				scaleY = 1;
			}// end else	
			
		}// end public function flipY(setting_:Boolean):void			
		
	}// end public class EntityCentered
	
}// end package