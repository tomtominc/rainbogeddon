package com.nitrome.engine
{
	
	public class PowerUpTail extends PowerUp
	{
		
		public function PowerUpTail(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpTail(tx_:uint, ty_:uint)
		
		public override function get soundID():String
		{		
			return "TailSound";
		}// end public override function get soundID():String			
		
		public override function get holder():Class
		{		
			return TailHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpTail
	
}// end package