package com.nitrome.engine
{
	import com.nitrome.engine.utils.SpatialHashMap;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	
	
	public class Entity extends GameObject 
	{
			
		public static const DIR_NONE:uint  = 0;
		public static const DIR_LEFT:uint  = 1;
		public static const DIR_RIGHT:uint = 2;
		public static const DIR_UP:uint    = 3;
		public static const DIR_DOWN:uint  = 4;
										
		public static const SUB_STATE_WAITING_RECOVER:uint = 0;
		public static const SUB_STATE_RECOVERED:uint       = 1;	
		
		public static const RECOVER_WAIT_DURATION:int = 2000;
		public static const STOP_ON_HIT_DURATION:int  = 500;
		
							
		public  var m_qDir:uint;  // the queued direction in which the object should move
		public  var m_prevQDir:uint; // same but the previous direction 
		public  var m_state:uint;
		public  var m_speed:Number;
		public  var m_collisionRect:Rectangle;
		public  var m_absCollisionRect:Rectangle;
		public  var m_hitPoints:int;
		public  var m_isDead:Boolean;
		public  var m_canDie:Boolean;
		public  var m_stopAtTile:Boolean;
		public  var m_stopAtNextTile:Boolean;
		public  var m_stopTileX:int;
		public  var m_stopTileY:int;
		public  var m_prevPrevTileX:int;
		public  var m_prevPrevTileY:int;		
		public  var m_oldPrevTileX:int;
		public  var m_oldPrevTileY:int;			
		public  var m_prevTileX:int;
		public  var m_prevTileY:int;		
		public  var m_oldTileX:int;
		public  var m_oldTileY:int;	
		public  var m_backChildGameObjectList:Vector.<GameObject>;
		public  var m_frontChildGameObjectList:Vector.<GameObject>;
		public  var m_cTX:int;
		public  var m_cTY:int;		
		public  var m_recoverSubState:uint;
		private var m_recoverWaitStartTime:int;
		public  var m_recoverWaitDuration:int;
		public  var m_maxHitPoints:uint;
		public  var m_signalPosOnCenterNextTile:Boolean;
		public  var m_flash:Flash;
		public  var m_canWrap:Boolean;
		public  var m_moves:Boolean;
		public  var m_iDUL:uint;
		public  var m_iDUR:uint;
		public  var m_iDLL:uint;
		public  var m_iDLR:uint;
		public  var m_tileMap:TileMap; // reference to the tile map used
		public  var m_triggeredDeath:Boolean;
		public  var m_canCollide:Boolean;
		public  static var m_spatialHashMap:SpatialHashMap;
		public  var m_stoppingOnHit:Boolean;
		public  var m_stopOnHitStartTime:int;
		public  var m_stopMoving:Boolean;
		public  var m_stopSidewaysOnCorridor:Boolean;
		
		public function Entity(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_canCollide = true;
			m_drawBitmap = Game.g.m_entityBitmap;
			m_qDir = DIR_NONE;
			m_prevQDir = DIR_NONE;
			m_speed = 0;
			m_collisionRect = new Rectangle();
			m_absCollisionRect = new Rectangle();
			m_isDead = false;
			m_canDie = true;
			m_tileMap = Game.g.m_tileMap;
			m_stopAtTile = false;
			m_stopAtNextTile = false;
			m_backChildGameObjectList = null;
			m_flash = new Flash(this);
			m_frontChildGameObjectList = new Vector.<GameObject>;
			m_frontChildGameObjectList.push(m_flash);	
			m_recoverSubState = SUB_STATE_RECOVERED;
			m_recoverWaitDuration = RECOVER_WAIT_DURATION;
			m_prevTileX = m_tx;
			m_prevTileY = m_ty;
			m_prevPrevTileX = m_tx;
			m_prevPrevTileY = m_ty;		
			m_oldPrevTileX = m_tx;
			m_oldPrevTileY = m_ty;		
			m_canWrap = true;
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;					
			m_triggeredDeath = false;
			m_signalPosOnCenterNextTile = false;
			m_moves = true;
			m_stoppingOnHit = false;
			m_stopMoving = false;
			m_stopSidewaysOnCorridor = false;
			
			if (!m_spatialHashMap)
			{
				m_spatialHashMap = new SpatialHashMap(Game.g.m_gameWidth, Game.TILE_WIDTH);
			}// end if
			
			m_spatialHashMap.registerObject(this);
			
		}// end public function Entity(tx_:uint, ty_:uint)
			
		public static function free():void
		{
			m_spatialHashMap = null;
		}// end public static function free():void		
		
		public function stopMoving():void
		{
			m_stopMoving = true;
		}// end public function stopMoving():void
		
		public function startMoving():void
		{
			m_stopMoving = false;
		}// end public function startMoving():void		
		
		public override function onGamePaused():void
		{
			if (m_backChildGameObjectList)
			{
				for (var currChild:uint = 0; currChild < m_backChildGameObjectList.length; currChild++)
				{
					m_backChildGameObjectList[currChild].onGamePaused();
				}// end for
			}// end if
			
			if (m_frontChildGameObjectList)
			{
				for (currChild = 0; currChild < m_frontChildGameObjectList.length; currChild++)
				{
					m_frontChildGameObjectList[currChild].onGamePaused();
				}// end for				
			}// end if			
			
		}// end public override function onGamePaused():void		
		
		public function updateAbsCollisionRect():void
		{		
			m_absCollisionRect.x = x + m_collisionRect.x + m_vel.x;
			m_absCollisionRect.y = y + m_collisionRect.y + m_vel.y;
			m_absCollisionRect.width = m_collisionRect.width;
			m_absCollisionRect.height = m_collisionRect.height;
		}// end public function updateAbsCollisionRect():void
		
		public override function onGameUnpaused():void
		{
			if (m_backChildGameObjectList)
			{
				for (var currChild:uint = 0; currChild < m_backChildGameObjectList.length; currChild++)
				{
					m_backChildGameObjectList[currChild].onGameUnpaused();
				}// end for
			}// end if
			
			if (m_frontChildGameObjectList)
			{
				for (currChild = 0; currChild < m_frontChildGameObjectList.length; currChild++)
				{
					m_frontChildGameObjectList[currChild].onGameUnpaused();
				}// end for				
			}// end if				
		}// end public override function onGameUnpaused():void					
		
		public function signalPosOnCenterNextTile():void
		{
			m_signalPosOnCenterNextTile = true;
		}// end public function signalPosOnCenterNextTile():void
		
		public function doDamage(hitPoints_:uint, vel_:Vector3D = null, attacker_:Entity = null, weapon_:Entity = null):void
		{
			
			if (!m_isDead && m_canDie && !m_triggeredDeath)
			{
			
				//m_flash.flash();
				m_hitPoints -= hitPoints_;
				onDamage(vel_);
				
				if (m_hitPoints <= 0)
				{
					onDeath(attacker_, weapon_);
					if (attacker_)
					{					
						attacker_.onKilledEntity(this);
					}// end if
					m_triggeredDeath = true;
				}// end if
				
			}// end if
			
		}// end function doDamage(hitPoints_:uint, vel_:Vector3D = null, attacker_:Entity = null, weapon_:Entity = null):void 
		
		public function stopAtTile(tx_:int, ty_:int):void
		{
			if ((m_tx == tx_) && (m_ty == ty_) && !m_vel.x && !m_vel.y)
			{				
				onStoppedAtTile();
			}
			else
			{
				m_stopAtTile = true;
				m_stopTileX = tx_;
				m_stopTileY = ty_;					
			}// end else
		}// end function stopAtTile(tx_:int, ty_:int):void		
		
		public function stopAtNextTile():void
		{
			if (!m_vel.x && !m_vel.y)
			{						
				onStoppedAtTile();
			}
			else
			{
				m_stopAtNextTile = true;
			}// end else
		}// end function stopAtNextTile(tx_:int, ty_:int):void				
		
		public function onStoppedAtTile():void
		{
			
			
		}// end function onStoppedAtTile():void		
		
		public function onCenterNextTile():void
		{
			
			
		}// end function onCenterNextTile():void			
		
		public function onDamage(vel_:Vector3D = null):void
		{
		
			startRecovery();
			stopOnHit();
			
		}// end function onDamage(vel_:Vector3D = null):void			
		
		public function onDeath(attacker_:Entity = null, weapon_:Entity = null):void
		{
			stop();
			m_spatialHashMap.unregisterObject(this);
		}// end function onDeath(attacker_:Entity = null, weapon_:Entity = null):void	
		
		public function kill(attacker_:Entity = null, weapon_:Entity = null):void
		{
			if (!m_isDead && m_canDie)
			{			
				m_flash.flash();
				m_hitPoints = 0;
				onDeath(attacker_, weapon_);
				if (attacker_)
				{
					attacker_.onKilledEntity(this);
				}// end if
			}// end if
		}// end function kill(attacker_:Entity = null, weapon_:Entity = null):void			
		
		public function onKilledEntity(entity_:Entity):void
		{
			
		}// end public function onKilledEntity(entity_:Entity):void
		
		public override function tick():void
		{

			doInput();
			
			if (!m_isDead)
			{
				doPhysics();	
			}// end if
			
		}// end public override function tick():void		
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if
			
			// code to fix strange uint wrapping bug
			if (m_tx > Game.g.mapWidth)
			{
				m_tx = 0;
			}// end if
			
			if (m_ty > Game.g.mapHeight)
			{
				m_ty = 0;
			}// end if		
			
			var tempX:Number = x;
			var tempY:Number = y;					
			
			var toX:Number = x;
			var toY:Number = y;
			var fromX:Number = x;
			var fromY:Number = y;					
			
			if (m_vel.x < 0)
			{
				toX += m_vel.x;
			}
			else if (m_vel.x > 0)
			{
				toX += (Game.TILE_WIDTH + m_vel.x);
				fromX += Game.TILE_WIDTH;
			}
			else if (m_vel.y < 0)
			{
				toY += m_vel.y;				
			}
			else if (m_vel.y > 0)
			{
				toY += (Game.TILE_HEIGHT + m_vel.y);
				fromY += Game.TILE_HEIGHT;
			}// end else if
					
			var toTileX:int = getTileXforX(toX);
			var toTileY:int = getTileYforY(toY);	
			var fromTileX:int = getTileXforX(fromX);
			var fromTileY:int = getTileYforY(fromY);								
			
			var canGo:Boolean = false;
			var skipCollision:Boolean = false;

			switch(m_qDir)
			{
				
				case DIR_LEFT:
				{
					
					if ((m_vel.x >= 0) && (m_vel.y == 0))
					{								
						
						if (m_tileMap.canGoLeft(m_tx, m_ty, m_canWrap))
						{
							m_vel.x = -m_speed;
							m_vel.y = 0;
							skipCollision = true;
						}// end if								
						
					}
					else if (fromTileY != toTileY)
					{
						
						canGo = m_tileMap.canGoLeft(m_tx, fromTileY, m_canWrap);
						
						if (canGo || (!canGo && m_stopSidewaysOnCorridor))
						{
							if (canGo)
							{
								m_vel.x = -m_speed;
								m_vel.y = 0;
								skipCollision = true;	
							}
							else
							{
								m_vel.x = 0;
								m_vel.y = 0;								
							}// end else
							m_tx = m_cTX;
							m_ty = fromTileY;							
							if (m_ty < 0)
							{
								m_ty = m_tileMap.m_height - 1;
							}
							else if (m_ty > m_tileMap.m_height - 1)
							{
								m_ty = 0;
							}// end else if
							updatePosFromTile();
														
						}// end if								
						
					}// end else if
					
					break;
				}// end case
		
				case DIR_RIGHT:
				{
					
					if ((m_vel.x <= 0) && (m_vel.y == 0))
					{
						
						if (m_tileMap.canGoRight(m_tx, m_ty, m_canWrap))
						{
							m_vel.x = m_speed;
							m_vel.y = 0;
							skipCollision = true;
						}// end if								
						
					}
					else if (fromTileY != toTileY)
					{
						
						canGo = m_tileMap.canGoRight(m_tx, fromTileY, m_canWrap);
						
						if (canGo || (!canGo && m_stopSidewaysOnCorridor))
						{
							if (canGo)
							{
								m_vel.x = m_speed;
								m_vel.y = 0;
								skipCollision = true;
							}
							else
							{
								m_vel.x = 0;
								m_vel.y = 0;								
							}// end else
							m_tx = m_cTX;
							m_ty = fromTileY;	
							if (m_ty < 0)
							{
								m_ty = m_tileMap.m_height - 1;
							}
							else if (m_ty > m_tileMap.m_height - 1)
							{
								m_ty = 0;
							}// end else if
							updatePosFromTile();							
						}// end if									
						
					}// end else if
					
					break;
				}// end case						
				
				case DIR_UP:
				{
				
					if ((m_vel.y >= 0) && (m_vel.x == 0))
					{
						if (m_tileMap.canGoUp(m_tx, m_ty, m_canWrap))
						{
							m_vel.y = -m_speed;
							m_vel.x = 0;
							skipCollision = true;
						}// end if									
					}
					else if (fromTileX != toTileX)
					{
						
						canGo = m_tileMap.canGoUp(fromTileX, m_ty, m_canWrap);
						
						if (canGo || (!canGo && m_stopSidewaysOnCorridor))
						{
							if(canGo)
							{
								m_vel.x = 0;
								m_vel.y = -m_speed;
								skipCollision = true;		
							}
							else
							{
								m_vel.x = 0;
								m_vel.y = 0;								
							}// end else
							m_ty = m_cTY;
							m_tx = fromTileX;	
							if (m_tx < 0)
							{
								m_tx = m_tileMap.m_width - 1;
							}
							else if (m_tx > m_tileMap.m_width - 1)
							{
								m_tx = 0;
							}// end else if
							updatePosFromTile();
													
						}// end if								
						
					}// end else if
					
					break;
				}// end case			
				
				case DIR_DOWN:
				{
			
					if ((m_vel.y <= 0) && (m_vel.x == 0))
					{
						if (m_tileMap.canGoDown(m_tx, m_ty, m_canWrap))
						{
							m_vel.y = m_speed;
							m_vel.x = 0;
							skipCollision = true;
						}// end if								
					}
					else if (fromTileX != toTileX)
					{
						
						canGo = m_tileMap.canGoDown(fromTileX, m_ty, m_canWrap);
						
						if (canGo || (!canGo && m_stopSidewaysOnCorridor))
						{
							if (canGo)
							{
								m_vel.x = 0;
								m_vel.y = m_speed;
								skipCollision = true;	
							}
							else
							{
								m_vel.x = 0;
								m_vel.y = 0;								
							}// end else
							m_ty = m_cTY;
							m_tx = fromTileX;	
							if (m_tx < 0)
							{
								m_tx = m_tileMap.m_width - 1;
							}
							else if (m_tx > m_tileMap.m_width - 1)
							{
								m_tx = 0;
							}// end else if	
							updatePosFromTile();
														
						}// end if								
						
					}// end else if							
					
					break;
				}// end case							
				
			}// end switch

			

			if (m_stopAtTile)
			{
				
				var blockTileX:int = m_stopTileX;
				var blockTileY:int = m_stopTileY;
				
				if (m_vel.x < 0)
				{
					blockTileX--;					
				}
				else if (m_vel.x > 0)
				{
					blockTileX++;		
				}
				else if (m_vel.y < 0)
				{
					blockTileY--;								
				}
				else if (m_vel.y > 0)
				{
					blockTileY++;							
				}// end else if		
				
				if ((blockTileX != m_stopTileX) || (blockTileY != m_stopTileY))
				{
					
					if ((blockTileX == toTileX) && (blockTileY == toTileY))
					{
						m_tx = m_stopTileX;
						m_ty = m_stopTileY;		
						m_vel.x = 0;
						m_vel.y = 0;
						updatePosFromTile();						
						m_stopAtTile = false;
						skipCollision = true;							
						onStoppedAtTile();						
					}// end if					
					
				}// end if
				
			}// end if			
			
			
			if ((!skipCollision && !m_tileMap.canGoToTile(toTileX, toTileY, m_canWrap)) || (m_stopAtNextTile && ((fromTileX != toTileX) || (fromTileY != toTileY))))
			{
				
				if (m_vel.x < 0)
				{			
					m_tx = toTileX + 1;
					m_ty = toTileY;
				}
				else if (m_vel.x > 0)
				{								
					m_tx = toTileX - 1;
					m_ty = toTileY;							
				}
				else if (m_vel.y < 0)
				{								
					m_tx = toTileX;
					m_ty = toTileY + 1;									
				}
				else if (m_vel.y > 0)
				{							
					m_tx = toTileX;
					m_ty = toTileY - 1;								
				}// end else if						
				
				m_vel.x = 0;
				m_vel.y = 0;
				
				updatePosFromTile();
				
				if (m_stopAtNextTile)
				{
					m_stopAtNextTile = false;						
					onStoppedAtTile();	
				}// end if			
				
			}// end if
					
			updatePos();
						
									
			m_oldTileX = m_cTX;
			m_oldTileY = m_cTY;			
			
			m_cTX = x / Game.TILE_WIDTH;
			m_cTY = y / Game.TILE_HEIGHT;				
			
			if (!((m_oldTileY == m_cTY) && (m_oldTileX == m_cTX)))
			{
				if (((m_oldTileX != m_cTX) && (m_oldTileY == m_cTY)) || ((m_oldTileY != m_cTY) && (m_oldTileX == m_cTX)))
				{
					if (!((m_prevTileX == m_cTX) && (m_prevTileY == m_cTY)))
					{
						m_oldPrevTileX = m_prevTileX;
						m_oldPrevTileY = m_prevTileY;
						m_prevTileX = m_oldTileX;
						m_prevTileY = m_oldTileY;
						
						if (((m_oldPrevTileX != m_prevTileX) && (m_oldPrevTileY == m_prevTileY)) || ((m_oldPrevTileY != m_prevTileY) && (m_oldPrevTileX == m_prevTileX)))
						{
							m_prevPrevTileX = m_oldPrevTileX;
							m_prevPrevTileY = m_oldPrevTileY;
						}// end if	
					}// end if
				}// end if
			}// end if						
			
			
			if (m_signalPosOnCenterNextTile)
			{
			
				var tileCenterVec:Vector3D = new Vector3D((m_cTX * Game.TILE_WIDTH) + (Game.TILE_WIDTH / 2), (m_cTY * Game.TILE_HEIGHT) + (Game.TILE_HEIGHT / 2));
				var posVec:Vector3D = new Vector3D(x, y);
				var posToTileVec:Vector3D = posVec.subtract(tileCenterVec);
				
				if (posToTileVec.length <= m_speed)
				{
					
					m_signalPosOnCenterNextTile = false;
					onCenterNextTile();

				}// end if				
				
			}// end if
			
			
			updateRecovery();
			
			tickChildren();
			
			if (m_isDead || m_terminated)
			{
				return;
			}// end if
			
			if (m_moves)
			{
				m_spatialHashMap.updateObject(this);	
			}// end if
			
			if (m_tx > Game.g.mapWidth)
			{
				m_tx = 0;
			}// end if
			
			if (m_ty > Game.g.mapHeight)
			{
				m_ty = 0;
			}// end if			
			
			m_drawOrder = y + m_bounds.y + m_bounds.height;// m_cTY;
			
		}// end public override function doPhysics():void		
		
		
		public function updateRecovery():void
		{	
		
			switch(m_recoverSubState)
			{
				
				case SUB_STATE_WAITING_RECOVER:
				{
					
					if (Game.g.timer - m_recoverWaitStartTime > m_recoverWaitDuration)
					{
									
						m_recoverWaitStartTime = Game.g.timer;
						m_hitPoints += m_maxHitPoints * 0.3;
						
						if (m_hitPoints >= m_maxHitPoints)
						{
							m_hitPoints = m_maxHitPoints;
							m_recoverSubState = SUB_STATE_RECOVERED;
						}// end if
					
					}// end if
					
					break;
				}// end case
				
			}// end switch				
			
		}// end public function updateRecovery():void
		
			
		public function tickChildren():void
		{
			if (m_backChildGameObjectList)
			{
				for (var currChild:uint = 0; currChild < m_backChildGameObjectList.length; currChild++)
				{
					m_backChildGameObjectList[currChild].doPhysics();
					if (currChild < m_backChildGameObjectList.length)
					{
						if (m_backChildGameObjectList[currChild].m_terminated)
						{
							m_backChildGameObjectList.splice(currChild, 1);
						}// end if
					}// end if
				}// end for
			}// end if
			
			if (m_frontChildGameObjectList)
			{
				for (currChild = 0; currChild < m_frontChildGameObjectList.length; currChild++)
				{
					m_frontChildGameObjectList[currChild].doPhysics();
					if (currChild < m_frontChildGameObjectList.length)
					{					
						if (m_frontChildGameObjectList[currChild].m_terminated)
						{
							m_frontChildGameObjectList.splice(currChild, 1);
						}// end if
					}// end if
				}// end for
			}// end if				
		}// end public function tickChildren():void			
		
		public function stopOnHit():void
		{
			
			m_stoppingOnHit = true;
			m_stopOnHitStartTime = Game.g.timer;
			
		}// end public function stopOnHit():void
		
		public function startRecovery():void
		{
			m_recoverWaitStartTime = Game.g.timer;
			m_recoverSubState = SUB_STATE_WAITING_RECOVER;
		}// end public function startRecovery():void			
		
		public function updatePos():void
		{
			
			if (m_stoppingOnHit)
			{
				
				if (Game.g.timer - m_stopOnHitStartTime > STOP_ON_HIT_DURATION)
				{
					m_stoppingOnHit = false;
				}// end if
				
			}
			else if(!m_stopMoving)
			{
				x += m_vel.x;
				y += m_vel.y;				
			}// end else
			
			
			if (x >= Game.g.mapWidth * Game.TILE_WIDTH)
			{
				x -=  Game.g.mapWidth * Game.TILE_WIDTH;
				onWrapped();
			}
			else if (x < 0)
			{
				x += Game.g.mapWidth * Game.TILE_WIDTH;
				onWrapped();
			}// end else if
		
			if (y >= Game.g.mapHeight * Game.TILE_HEIGHT)
			{
				y -=  Game.g.mapHeight * Game.TILE_HEIGHT;
				onWrapped();
			}
			else if (y < 0)
			{
				y += Game.g.mapHeight * Game.TILE_HEIGHT;
				onWrapped();
			}// end else if					
			
			updateTilePos();			
			
		}// end public function updatePos():void		
		
		public function onWrapped():void
		{
			
		}// end public function onWrapped():void		
		
		public function getTileXforX(x_:Number):int
		{
			return Math.floor(x_/Game.TILE_WIDTH);	
		}// end public function getTileXforX(x_:Number):int	
		
		public function getTileYforY(y_:Number):int
		{
			return Math.floor(y_/Game.TILE_HEIGHT);	
		}// end public function getTileYforY(y_:Number):int			
		
		public static function getWrappedX(x_:Number):Number
		{			
			
			if (x_ < 0)
			{
				return x_ + Game.g.m_gameWidth;
			}
			else if (x_ > Game.g.m_gameWidth)
			{
				return x_ - Game.g.m_gameWidth;
			}// end else if			
						
			return x_;
			
		}// end public static function getWrappedX(x_:Number):Number
		
		public static function getWrappedY(y_:Number):Number
		{			
			
			if (y_ < 0)
			{
				return y_ + Game.g.m_gameHeight;
			}
			else if (y_ > Game.g.m_gameHeight)
			{
				return y_ - Game.g.m_gameHeight;
			}// end else if			
						
			return y_;
			
		}// end public static function getWrappedY(y_:Number):Number	
		
		public function getWrappedTileX(x_:int):int
		{			
			
			if (x_ < 0)
			{
				return m_tileMap.m_width - 1;
			}
			else if (x_ > m_tileMap.m_width - 1)
			{
				return 0;
			}// end else if			
			
			return x_;
			
		}// end public function getWrappedTileX(x_:int):int
		
		public function getWrappedTileY(y_:int):int
		{			
			if (y_ < 0)
			{
				return m_tileMap.m_height - 1;
			}
			else if (y_ > m_tileMap.m_height - 1)
			{
				return 0;
			}// end else if			
			
			return y_;
		}// end public function getWrappedTileY(y_:int):int
		
		public function getOppositeX(x_:Number):Number
		{			
			
			if (x_ < (Game.g.m_gameWidth/2))
			{
				return x_ + Game.g.m_gameWidth;
			}// end if
			
			return x_ - Game.g.m_gameWidth;
			
		}// end public function getOppositeX(x_:Number):Number
		
		public function getOppositeY(y_:Number):Number
		{			
			
			if (y_ < (Game.g.m_gameHeight/2))
			{
				return y_ + Game.g.m_gameHeight;
			}// end if
			
			return y_ - Game.g.m_gameHeight;
			
		}// end public function getOppositeY(y_:Number):Number
		
		public function collidesWithObject(gameObject_:Entity):Boolean
		{			
		
			var numCollisionTests:int = 1;
			
			var collisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			collisionRectList[0] = new Rectangle(x + m_collisionRect.x + m_vel.x, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);
			
			// test for wrapping
			
			if (x + m_vel.x + (m_collisionRect.x + m_collisionRect.width) > Game.g.m_gameWidth)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x - Game.g.m_gameWidth, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);			
			}
			else if ((x + m_vel.x + m_collisionRect.x) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x + Game.g.m_gameWidth, y + m_vel.y + m_collisionRect.y, m_collisionRect.width, m_collisionRect.height);		
			}
			else if (y + m_vel.y + (m_collisionRect.y + m_collisionRect.height) > Game.g.m_gameHeight)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y - Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);					
			}
			else if ((y + m_vel.y + m_collisionRect.y) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y + Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);	
			}// end else if			
			
			var otherCollisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			var numOtherCollisionTests:int = 1;
			
			otherCollisionRectList[0] = new Rectangle();
			otherCollisionRectList[1] = new Rectangle();
			
	
			if (gameObject_ == this)
			{
				return false;
			}// end if
			
			numOtherCollisionTests = 1;
			
			otherCollisionRectList[0].x = gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x;
			otherCollisionRectList[0].y = gameObject_.y + gameObject_.m_vel.y + gameObject_.m_collisionRect.y;
			otherCollisionRectList[0].width = gameObject_.m_collisionRect.width;
			otherCollisionRectList[0].height = gameObject_.m_collisionRect.height;
			
			// test for wrapping
			
			if (gameObject_.x + gameObject_.m_vel.x + (gameObject_.m_collisionRect.x + gameObject_.m_collisionRect.width) > Game.g.m_gameWidth)
			{
				numOtherCollisionTests = 2;
				otherCollisionRectList[1].x = gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x - Game.g.m_gameWidth;
				otherCollisionRectList[1].y = gameObject_.y + gameObject_.m_vel.y + gameObject_.m_collisionRect.y;
				otherCollisionRectList[1].width = gameObject_.m_collisionRect.width;
				otherCollisionRectList[1].height = gameObject_.m_collisionRect.height;			
			}
			else if ((gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x) < 0)
			{
				numOtherCollisionTests = 2;
				otherCollisionRectList[1].x = gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x + Game.g.m_gameWidth;
				otherCollisionRectList[1].y = gameObject_.y + gameObject_.m_vel.y + gameObject_.m_collisionRect.y;
				otherCollisionRectList[1].width = gameObject_.m_collisionRect.width;
				otherCollisionRectList[1].height = gameObject_.m_collisionRect.height;		
			}
			else if (gameObject_.y + gameObject_.m_vel.y + (gameObject_.m_collisionRect.y + gameObject_.m_collisionRect.height) > Game.g.m_gameHeight)
			{
				numOtherCollisionTests = 2;
				otherCollisionRectList[1].x = gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x;
				otherCollisionRectList[1].y = gameObject_.y + gameObject_.m_vel.y + gameObject_.m_collisionRect.y - Game.g.m_gameHeight;
				otherCollisionRectList[1].width = gameObject_.m_collisionRect.width;
				otherCollisionRectList[1].height = gameObject_.m_collisionRect.height;					
			}
			else if ((y + gameObject_.m_vel.y + m_collisionRect.y) < 0)
			{
				numOtherCollisionTests = 2;
				otherCollisionRectList[1].x = gameObject_.x + gameObject_.m_vel.x + gameObject_.m_collisionRect.x;
				otherCollisionRectList[1].y = gameObject_.y + gameObject_.m_vel.y + gameObject_.m_collisionRect.y + Game.g.m_gameHeight;
				otherCollisionRectList[1].width = gameObject_.m_collisionRect.width;
				otherCollisionRectList[1].height = gameObject_.m_collisionRect.height;	
			}// end else if				
			
			for(var currRect = 0; currRect < numCollisionTests; currRect++)
			{
				
				for(var otherCurrRect = 0; otherCurrRect < numOtherCollisionTests; otherCurrRect++)
				{
					
					if (collisionRectList[currRect].intersects(otherCollisionRectList[otherCurrRect]))
					{
						
						return true;
						
					}// end if
					
				}// end for					
				
			}// end for
				

			return false;
			
		}// end public function collidesWithObject(object_:Entity):Boolean			
		
		public function getCollidingObject(ignoreList_:Vector.<Class> = null, ignoreObject_:GameObject = null, collideOnlyType_:Class = null, gameObjectList_:Vector.<Entity> = null):Entity
		{			
		
			var gameObjectList:Vector.<Entity> = gameObjectList_ ? gameObjectList_ : m_spatialHashMap.getMatchingList(this);// Game.g.m_gameObjectList;
			var currObject:uint = 0;
			
			/*if (this is EntityPlayer1)
			{
				
				trace("BEGIN LIST");
				
				for (currObject = 0; currObject < gameObjectList.length; currObject++)
				{
					trace(gameObjectList[currObject]);
				}// end for
				
				trace("END LIST");
				
			}// end if*/
			
			//gameObjectList = m_spatialHashMap.getMatchingList(m_explosionParticleList[m_currExplodingTile]);
			
			var numCollisionTests:int = 1;
			
			var collisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			collisionRectList[0] = new Rectangle(x + m_collisionRect.x + m_vel.x, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);
			
			// test for wrapping
			
			if (x + m_vel.x + (m_collisionRect.x + m_collisionRect.width) > Game.g.m_gameWidth)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x - Game.g.m_gameWidth, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);			
			}
			else if ((x + m_vel.x + m_collisionRect.x) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x + Game.g.m_gameWidth, y + m_vel.y + m_collisionRect.y, m_collisionRect.width, m_collisionRect.height);		
			}
			else if (y + m_vel.y + (m_collisionRect.y + m_collisionRect.height) > Game.g.m_gameHeight)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y - Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);					
			}
			else if ((y + m_vel.y + m_collisionRect.y) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y + Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);	
			}// end else if			
			
			var otherCollisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			var numOtherCollisionTests:int = 1;
			
			otherCollisionRectList[0] = new Rectangle();
			otherCollisionRectList[1] = new Rectangle();
			
			var isIgnored:Boolean = false;
			
			var collidingChildObject:Entity;
			
			for(currObject = 0; currObject < gameObjectList.length; currObject++)
			{
				
				if (!(gameObjectList[currObject] is Entity) || (gameObjectList[currObject] == this))
				{
					continue;
				}// end if
				
				var gameObject:Entity = gameObjectList[currObject] as Entity;
				
				/*if (gameObject.m_canCollide == false)
				{
					continue;
				}// end if*/
				
				isIgnored = false;
				
				if (!collideOnlyType_)
				{
				
					if (ignoreList_)
					{
						
						for (var currIgnore:uint = 0; currIgnore < ignoreList_.length; currIgnore++)
						{
							if (gameObject is ignoreList_[currIgnore])
							{
								isIgnored = true;
								break;
							}// end if
						}// end for
						
					}// end if
						
					if ((gameObject == this) || isIgnored || (ignoreObject_ && (gameObject == ignoreObject_)))
					{
						continue;
					}// end if
					
				}
				else if((!gameObject is collideOnlyType_) || (gameObject == this))
				{
					continue;
				}// end else
				
				numOtherCollisionTests = 1;
				
				otherCollisionRectList[0].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
				otherCollisionRectList[0].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
				otherCollisionRectList[0].width = gameObject.m_collisionRect.width;
				otherCollisionRectList[0].height = gameObject.m_collisionRect.height;
				
				// test for wrapping
				
				if (gameObject.x + gameObject.m_vel.x + (gameObject.m_collisionRect.x + gameObject.m_collisionRect.width) > Game.g.m_gameWidth)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x - Game.g.m_gameWidth;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;			
				}
				else if ((gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x) < 0)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x + Game.g.m_gameWidth;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;		
				}
				else if (gameObject.y + gameObject.m_vel.y + (gameObject.m_collisionRect.y + gameObject.m_collisionRect.height) > Game.g.m_gameHeight)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y - Game.g.m_gameHeight;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;					
				}
				else if ((y + gameObject.m_vel.y + m_collisionRect.y) < 0)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y + Game.g.m_gameHeight;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;	
				}// end else if				
				
				for(var currRect = 0; currRect < numCollisionTests; currRect++)
				{
					
					for(var otherCurrRect = 0; otherCurrRect < numOtherCollisionTests; otherCurrRect++)
					{
						
						if (collisionRectList[currRect].intersects(otherCollisionRectList[otherCurrRect]))
						{
							
							return gameObject;
							
						}// end if
						
					}// end for					
					
				}// end for
				
				var childEntityList:Vector.<Entity> = new Vector.<Entity>;
				var currChildObject:uint = 0;
				
				if (gameObject.m_backChildGameObjectList)
				{
					
					for (currChildObject = 0; currChildObject < gameObject.m_backChildGameObjectList.length; currChildObject++)
					{
						if (gameObject.m_backChildGameObjectList[currChildObject] is Entity)
						{
							childEntityList.push(gameObject.m_backChildGameObjectList[currChildObject] as Entity);
						}// end if
					
					}// end for
					
					if (childEntityList.length)
					{
					
						collidingChildObject = getCollidingObject(ignoreList_, ignoreObject_, collideOnlyType_, childEntityList);// gameObject.m_backChildGameObjectList);
						if (collidingChildObject)
						{
							return collidingChildObject;
						}// end if
						
					}// end if
					
				}// end if
				
				
				if (gameObject.m_frontChildGameObjectList)
				{
					
					childEntityList = new Vector.<Entity>;
					
					for (currChildObject = 0; currChildObject < gameObject.m_frontChildGameObjectList.length; currChildObject++)
					{
						if (gameObject.m_frontChildGameObjectList[currChildObject] is Entity)
						{
							childEntityList.push(gameObject.m_frontChildGameObjectList[currChildObject] as Entity);
						}// end if
					
					}// end for					
					
					if (childEntityList.length)
					{
					
						collidingChildObject = getCollidingObject(ignoreList_, ignoreObject_, collideOnlyType_, childEntityList);// gameObject.m_frontChildGameObjectList);
						if (collidingChildObject)
						{
							return collidingChildObject;
						}// end if
					
					}// end if
					
				}// end if				
				
			}// end for
			
			return null;
			
		}// end public function getCollidingObject():Entity		
		
		public function getCollidingObjects(ignoreList_:Vector.<Class> = null, ignoreObject_:GameObject = null, collideOnlyType_:Class = null, gameObjectList_:Vector.<Entity> = null):Vector.<Entity>
		{			
		
			var gameObjectList:Vector.<Entity> = gameObjectList_ ? gameObjectList_ : m_spatialHashMap.getMatchingList(this); // Game.g.m_gameObjectList;
			
			var collidingObjectList:Vector.<Entity> = new Vector.<Entity>;
			
			var numCollisionTests:int = 1;
			
			var collisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			collisionRectList[0] = new Rectangle(x + m_collisionRect.x + m_vel.x, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);
			
			// test for wrapping
			
			if (x + m_vel.x + (m_collisionRect.x + m_collisionRect.width) > Game.g.m_gameWidth)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x - Game.g.m_gameWidth, y + m_collisionRect.y + m_vel.y, m_collisionRect.width, m_collisionRect.height);			
			}
			else if ((x + m_vel.x + m_collisionRect.x) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_collisionRect.x + m_vel.x + Game.g.m_gameWidth, y + m_vel.y + m_collisionRect.y, m_collisionRect.width, m_collisionRect.height);		
			}
			else if (y + m_vel.y + (m_collisionRect.y + m_collisionRect.height) > Game.g.m_gameHeight)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y - Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);					
			}
			else if ((y + m_vel.y + m_collisionRect.y) < 0)
			{
				numCollisionTests = 2;
				collisionRectList[1] = new Rectangle(x + m_vel.x + m_collisionRect.x, y + m_vel.y + m_collisionRect.y + Game.g.m_gameHeight, m_collisionRect.width, m_collisionRect.height);	
			}// end else if			
			
			var otherCollisionRectList:Vector.<Rectangle> = new Vector.<Rectangle>(2);
			
			var numOtherCollisionTests:int = 1;
			
			otherCollisionRectList[0] = new Rectangle();
			otherCollisionRectList[1] = new Rectangle();
			
			var collidingChildObjectList:Vector.<Entity>;
			
			for(var currObject = 0; currObject < gameObjectList.length; currObject++)
			{
				
				if (!(gameObjectList[currObject] is Entity))
				{
					continue;
				}// end if				
				
				var gameObject:Entity = gameObjectList[currObject] as Entity;
				
				var isIgnored:Boolean = false;
				
				if (!collideOnlyType_)
				{
				
					if (ignoreList_)
					{
						
						for (var currIgnore:uint = 0; currIgnore < ignoreList_.length; currIgnore++)
						{
							if (gameObject is ignoreList_[currIgnore])
							{
								isIgnored = true;
								break;
							}// end if
						}// end for
						
					}// end if
						
					if ((gameObject == this) || isIgnored || (ignoreObject_ && (gameObject == ignoreObject_)))
					{
						continue;
					}// end if
					
				}
				else if(!(gameObject is collideOnlyType_) || (gameObject == this))
				{
					continue;
				}// end else
				
				numOtherCollisionTests = 1;
				
				otherCollisionRectList[0].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
				otherCollisionRectList[0].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
				otherCollisionRectList[0].width = gameObject.m_collisionRect.width;
				otherCollisionRectList[0].height = gameObject.m_collisionRect.height;
				
				// test for wrapping
				
				if (gameObject.x + gameObject.m_vel.x + (gameObject.m_collisionRect.x + gameObject.m_collisionRect.width) > Game.g.m_gameWidth)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x - Game.g.m_gameWidth;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;			
				}
				else if ((gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x) < 0)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x + Game.g.m_gameWidth;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;		
				}
				else if (gameObject.y + gameObject.m_vel.y + (gameObject.m_collisionRect.y + gameObject.m_collisionRect.height) > Game.g.m_gameHeight)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y - Game.g.m_gameHeight;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;					
				}
				else if ((y + gameObject.m_vel.y + m_collisionRect.y) < 0)
				{
					numOtherCollisionTests = 2;
					otherCollisionRectList[1].x = gameObject.x + gameObject.m_vel.x + gameObject.m_collisionRect.x;
					otherCollisionRectList[1].y = gameObject.y + gameObject.m_vel.y + gameObject.m_collisionRect.y + Game.g.m_gameHeight;
					otherCollisionRectList[1].width = gameObject.m_collisionRect.width;
					otherCollisionRectList[1].height = gameObject.m_collisionRect.height;	
				}// end else if				
				
				for(var currRect = 0; currRect < numCollisionTests; currRect++)
				{
					
					for(var otherCurrRect = 0; otherCurrRect < numOtherCollisionTests; otherCurrRect++)
					{
						
						if (collisionRectList[currRect].intersects(otherCollisionRectList[otherCurrRect]))
						{
							
							collidingObjectList.push(gameObject);
							
						}// end if
						
					}// end for					
					
				}// end for
				
				var childEntityList:Vector.<Entity> = new Vector.<Entity>;
				var currChildObject:uint = 0;				
				
				if (gameObject.m_backChildGameObjectList)
				{
						
					for (currChildObject = 0; currChildObject < gameObject.m_backChildGameObjectList.length; currChildObject++)
					{
						if (gameObject.m_backChildGameObjectList[currChildObject] is Entity)
						{
							childEntityList.push(gameObject.m_backChildGameObjectList[currChildObject] as Entity);
						}// end if
					
					}// end for					
					
					if (childEntityList.length)
					{
					
						collidingChildObjectList = getCollidingObjects(ignoreList_, ignoreObject_, collideOnlyType_, childEntityList);
						collidingObjectList = collidingObjectList.concat(collidingChildObjectList);						
											
					}// end if					

					
				}// end if	
				
				if (gameObject.m_frontChildGameObjectList)
				{
					
					childEntityList = new Vector.<Entity>;
									
					
					for (currChildObject = 0; currChildObject < gameObject.m_frontChildGameObjectList.length; currChildObject++)
					{
						if (gameObject.m_frontChildGameObjectList[currChildObject] is Entity)
						{
							childEntityList.push(gameObject.m_frontChildGameObjectList[currChildObject] as Entity);
						}// end if
					
					}// end for					
					
					if (childEntityList.length)
					{
					
						collidingChildObjectList = getCollidingObjects(ignoreList_, ignoreObject_, collideOnlyType_, childEntityList);
						collidingObjectList = collidingObjectList.concat(collidingChildObjectList);						
											
					}// end if							
					
				}// end if						
				
			}// end for
			
			return collidingObjectList;
			
		}// end public function getCollidingObjects(ignoreList_:Vector.<Class> = null, ignoreObject_:GameObject = null, collideOnlyType_:Class = null):Vector.<Entity>		
		
		
		public override function draw():void
		{
			
			if (m_backChildGameObjectList)
			{	
				for (var currChild:uint = 0; currChild < m_backChildGameObjectList.length; currChild++)
				{
					m_backChildGameObjectList[currChild].draw();
				}// end for
			}// end if
			
			super.draw();
			
			if (m_frontChildGameObjectList)
			{
				for (currChild = 0; currChild < m_frontChildGameObjectList.length; currChild++)
				{
					m_frontChildGameObjectList[currChild].draw();
				}// end for
			}// end if			
			
		}// end public override function draw():void					
		
	}// end public class Entity
	
}// end package