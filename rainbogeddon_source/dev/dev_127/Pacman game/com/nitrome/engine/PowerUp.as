package com.nitrome.engine
{
	

	
	public class PowerUp extends Entity
	{
		
		public var m_enabled:Boolean;
		
		public function PowerUp(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);			
			m_collisionRect.x = 0;
			m_collisionRect.y = 0;
			m_collisionRect.width = 25;
			m_collisionRect.height = 25;
			m_canDie = false;
			m_enabled = true;
			m_drawBitmap = Game.g.m_effectsBitmap;
			
		}// end public function PowerUp(tx_:uint, ty_:uint)
		
		public override function doPhysics():void
		{
			
			if (!m_enabled)
			{
				return;
			}// end if
		
			var apply:Boolean = false;
			var player:EntityPlayer;
			
			if (!Game.g.m_player1.m_terminated && collidesWithObject(Game.g.m_player1))
			{
				apply = true;
				player = Game.g.m_player1;
			}// end if
			
			if (!Game.g.m_player2.m_terminated && collidesWithObject(Game.g.m_player2))
			{
				apply = true;
				player = Game.g.m_player2;
			}// end if			
			
			if (apply)
			{
				player.applyPowerUp(this);
				terminate();
			}// end if
					
		}// end public override function doPhysics():void	
		
		public function onAppeared():void
		{		
			m_drawBitmap = Game.g.m_entityBitmap;
			gotoAndPlay("normal");
		}// end public function onAppeared():void		
		
		public function warnTermination():void
		{		
			m_flicker = true;
		}// end public function warnTermination():void			
		
		public function get soundID():String
		{		
			return null;
		}// end public function get soundID():String			
		
		public function terminate():void
		{		
			m_flicker = false;
			m_drawBitmap = Game.g.m_effectsBitmap;
			gotoAndPlay("disappear");
			m_enabled = false;
			m_spatialHashMap.unregisterObject(this);
		}// end public function terminate():void		
		
		public function get holder():Class
		{		
			return null;
		}// end public function get holder():Class
		
	}// end public class PowerUp
	
}// end package