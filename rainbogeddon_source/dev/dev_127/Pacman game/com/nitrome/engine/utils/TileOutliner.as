package com.nitrome.engine.utils
{

	
	public class TileOutliner
	{

		public static const LEFT:uint  = 0x1;	
		public static const UP:uint    = 0x4;
		public static const RIGHT:uint = 0x10;
		public static const DOWN:uint  = 0x40;
		
		public static const UPPER_LEFT:uint   = 0x2;
		public static const UPPER_RIGHT:uint  = 0x8;
		public static const LOWER_RIGHT:uint  = 0x20;
		public static const LOWER_LEFT:uint   = 0x80;

		public var m_isWrapped:Boolean;
		public var m_mapWidth:uint;
		public var m_mapHeight:uint;
		public var m_tileMap:Vector.<uint>;   // reference to the tile map used
		public var m_emptyTileValue:uint;
		
		public function TileOutliner()
		{
			m_isWrapped = true;
			m_tileMap = null;
			m_mapWidth = 0;
			m_mapHeight = 0;
		}// end public function TileContourer()
				
		private function getWrappedX(x_:int):int
		{
			if (m_isWrapped)
			{
				if (x_ >= m_mapWidth)
				{
					return x_ - m_mapWidth;
				}
				else if (x_ < 0)
				{
					return x_ + m_mapWidth;
				}
				else
				{
					return x_;
				}// end else
			}
			else
			{
				if (x_ >= m_mapWidth)
				{
					return -1;
				}
				else if (x_ < 0)
				{
					return -1;
				}
				else
				{
					return x_;
				}// end else				
			}// end else
		}// end private function getWrappedX(x_:int):int	
		
		private function getWrappedY(y_:int):int
		{
			if (m_isWrapped)
			{
				if (y_ >= m_mapHeight)
				{
					return y_ - m_mapHeight;
				}
				else if (y_ < 0)
				{
					return y_ + m_mapHeight;
				}
				else
				{
					return y_;
				}// end else
			}
			else
			{
				if (y_ >= m_mapHeight)
				{
					return -1;
				}
				else if (y_ < 0)
				{
					return -1;
				}
				else
				{
					return y_;
				}// end else				
			}// end else
		}// end private function getWrappedY(y_:int):int			
		
		/*
		 * Gets the corresponding id of the tile according to the ones surrounding it
		 */
		public function getContourID(x_:int, y_:int, corners_:Boolean = true):uint
		{
			
			if (!m_tileMap)
			{
				return 0;
			}// end if
			
			var iD:uint = 0;
			
			var x:int = getWrappedX(x_-1);
			var y:int = getWrappedX(y_);
			
			if ((x >= 0) && (y >= 0))
			{
				if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
				{
					iD |= LEFT;
				}// end if
			}// end if
			
			x = getWrappedX(x_);
			y = getWrappedX(y_-1);
			
			if ((x >= 0) && (y >= 0))
			{
				if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
				{
					iD |= UP;
				}// end if
			}// end if			
			
			x = getWrappedX(x_+1);
			y = getWrappedX(y_);
			
			if ((x >= 0) && (y >= 0))
			{
				if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
				{
					iD |= RIGHT;
				}// end if
			}// end if						
				
			x = getWrappedX(x_);
			y = getWrappedX(y_+1);
			
			if ((x >= 0) && (y >= 0))
			{
				if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
				{
					iD |= DOWN;
				}// end if
			}// end if					
			
			
			if (corners_)
			{
			
				if ((iD & LEFT) && (iD & UP))
				{
				
					x = getWrappedX(x_-1);
					y = getWrappedX(y_-1);
					
					if ((x >= 0) && (y >= 0))
					{
						if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
						{
							iD |= UPPER_LEFT;
						}// end if
					}// end if				
					
				}// end if
				
				
				if ((iD & RIGHT) && (iD & UP))
				{
				
					x = getWrappedX(x_+1);
					y = getWrappedX(y_-1);
					
					if ((x >= 0) && (y >= 0))
					{
						if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
						{
							iD |= UPPER_RIGHT;
						}// end if
					}// end if				
					
				}// end if			
				
				
				if ((iD & RIGHT) && (iD & DOWN))
				{
				
					x = getWrappedX(x_+1);
					y = getWrappedX(y_+1);
					
					if ((x >= 0) && (y >= 0))
					{
						if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
						{
							iD |= LOWER_RIGHT;
						}// end if
					}// end if				
					
				}// end if		
				
				
				if ((iD & LEFT) && (iD & DOWN))
				{
				
					x = getWrappedX(x_-1);
					y = getWrappedX(y_+1);
					
					if ((x >= 0) && (y >= 0))
					{
						if (m_tileMap[m_mapWidth * y + x] != m_emptyTileValue)
						{
							iD |= LOWER_LEFT;
						}// end if
					}// end if				
					
				}// end if		
				
			}// end if
			
			return iD;
			
		}// end public function getContourID(x_:int, y_:int):uint		
		
		/*
		 * Generates all the possible ids and prints them
		 * together with positions of adjacent tiles
		 */
		public function dumpIDSet(corners_:Boolean = true):void
		{
			
			var oldEmptyTilevalue:uint = m_emptyTileValue;
			var oldMapWidth:uint = m_mapWidth;
			var oldMapHeight:uint = m_mapHeight;
			var oldTileMap:Vector.<uint> = m_tileMap;
			
			m_emptyTileValue = 0;
			m_mapWidth  = 3;
			m_mapHeight = 3;
		
			// test constants used only for internal reference
			// in this function when generating the test maps
			
			const LOWER_RIGHT_TEST:uint = 0x1;
			const DOWN_TEST:uint        = 0x2;
			const LOWER_LEFT_TEST:uint  = 0x4;	
			const RIGHT_TEST:uint       = 0x8;
			const LEFT_TEST:uint        = 0x20;
			const UPPER_RIGHT_TEST:uint = 0x40;
			const UP_TEST:uint          = 0x80;
			const UPPER_LEFT_TEST:uint  = 0x100;
			
			const CENTER_TEST:uint      = 0x10;
			
			var generatedIDMap:Object = new Object();
			var iD:uint = 0;
			var adjTileString:String = new String();
			var numGeneratedIDs:uint = 0;
			var lr:uint = 0;
			var d:uint  = 0;
			var ll:uint = 0;
			var r:uint  = 0;
			var l:uint  = 0;
			var ur:uint = 0;
			var u:uint  = 0;
			var ul:uint = 0;
			
			trace("--- NEW ID ---");			
			trace("First center tile iD is 0");
			trace(ul + ","  + u   + "," + ur);
			trace(l +  ","  + "1" + "," + r);
			trace(ll + ","  + d   + "," + lr);			
			generatedIDMap[iD] = true;
			numGeneratedIDs++;			
			
			for (var currCombination:uint = 1; currCombination < 512; currCombination++)
			{
				
				adjTileString = new String("Adjacent tiles = ");
				m_tileMap = new Vector.<uint>(9);
				
				lr = 0;
				d  = 0;
				ll = 0;
				r  = 0;
				l  = 0;
				ur = 0;
				u  = 0;
				ul = 0;				
				
				if (currCombination & CENTER_TEST)
				{
					continue;
				}// end if
				
				if (currCombination & DOWN_TEST)
				{
					d = 1;
					adjTileString += "DOWN, ";
					m_tileMap[3 * 2 + 1] = 1;
				}// end if	
								
				if (currCombination & RIGHT_TEST)
				{
					r = 1;
					adjTileString += "RIGHT, ";
					m_tileMap[3 * 1 + 2] = 1;
				}// end if	
				
				if (currCombination & LEFT_TEST)
				{
					l = 1;
					adjTileString += "LEFT, ";
					m_tileMap[3 * 1 + 0] = 1;
				}// end if		
				
				if (currCombination & UP_TEST)
				{
					u = 1;
					adjTileString += "UP, ";
					m_tileMap[3 * 0 + 1] = 1;
				}// end if				
				
				if (corners_)
				{
				
					if (currCombination & LOWER_RIGHT_TEST)
					{
						lr = 1;
						adjTileString += "LOWER_RIGHT, ";
						m_tileMap[3 * 2 + 2] = 1;
					}// end if				
					
					if (currCombination & LOWER_LEFT_TEST)
					{
						ll = 1;
						adjTileString += "LOWER_LEFT, ";
						m_tileMap[3 * 2 + 0] = 1;
					}// end if					
					
					if (currCombination & UPPER_RIGHT_TEST)
					{
						ur = 1;
						adjTileString += "UPPER_RIGHT, ";
						m_tileMap[3 * 0 + 2] = 1;
					}// end if											
					
					if (currCombination & UPPER_LEFT_TEST)
					{
						ul = 1;
						adjTileString += "UPPER_LEFT, ";
						m_tileMap[3 * 0 + 0] = 1;
					}// end if					
				
				}// end if	
					
				iD = getContourID(1,1);
				
				if (iD && !generatedIDMap[iD])
				{
					generatedIDMap[iD] = true;
					numGeneratedIDs++;
					trace("--- NEW ID ---");
					trace("tile_" + iD);
					trace(ul + ","  + u   + "," + ur);
					trace(l +  ","  + "1" + "," + r);
					trace(ll + ","  + d   + "," + lr);
					trace(adjTileString);
				}// end if
				
			}// end for
			
			trace("--- END ---");
			trace("total generated IDs " + numGeneratedIDs);
			
			m_emptyTileValue = oldEmptyTilevalue;
			m_mapWidth = oldMapWidth;
			m_mapHeight = oldMapHeight;
			m_tileMap = oldTileMap;			
			
		}// end public function dumpIDSet():void			
		
	}// end public class TileOutliner
	
}// end package