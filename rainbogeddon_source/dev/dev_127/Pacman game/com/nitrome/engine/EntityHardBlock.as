package com.nitrome.engine
{
	
	
	public class EntityHardBlock extends EntityBlock
	{
			

		public function EntityHardBlock(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_canDie = false;
			var contourID:uint = Game.g.m_tileOutliner.getContourID(m_tx, m_ty, false);
			gotoAndStop("t_" + contourID);			
		}// end function EntityHardBlock(tx_:uint, ty_:uint)				
		
		public override function doPhysics():void
		{
						
			var contourID:uint = Game.g.m_tileOutliner.getContourID(m_tx, m_ty, false);
			gotoAndStop("t_" + contourID);			
						
		}// end override function doPhysics():void				
		
	}// end public class EntityHardBlock
	
}// end package