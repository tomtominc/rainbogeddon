package com.nitrome.engine
{
	
	
	public class EntityJellyBabyGirl extends EntityJellyBaby
	{
			
		public function EntityJellyBabyGirl(mother_:EntityJellyMama)
		{
			super(mother_);
			m_glow.m_color = 0xffe800ff;
		}// end public function EntityJellyBabyGirl(mother_:EntityJellyMama)
							
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedPink;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityJellyBabyGirl
	
}// end package