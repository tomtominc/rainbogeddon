﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	import com.nitrome.sound.SoundManager;
	
	/**
	* Goto the chooseLevel frame
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class PlayButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			SoundManager.playSound("Play");
			NitromeGame.root.transition.goto("options");
		}
		
		override protected function onMouseOver(e:MouseEvent):void{
			super.onMouseOver(e);
			if (NitromeGame.root.hatNBowContainer)
			{			
				NitromeGame.root.hatNBowContainer.hat.menuPosLock(0);
				NitromeGame.root.hatNBowContainer.bow.menuPosLock(0);
			}// end if
		}
		override protected function onMouseOut(e:MouseEvent):void{
			super.onMouseOut(e);
			if (NitromeGame.root.hatNBowContainer)
			{
				NitromeGame.root.hatNBowContainer.hat.abortMenuPosLock();
				NitromeGame.root.hatNBowContainer.bow.abortMenuPosLock();
			}// end if
		}		
		
	}
	
}