package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	import flash.utils.getDefinitionByName;
	import com.nitrome.sound.SoundManager;

	public class SelectPlayerButton extends BasicButton
	{
		
		private var m_selected:Boolean;
		
		public function SelectPlayerButton()
		{
			m_selected = false;
			if (name == "select1Player")
			{
				gotoAndStop("selected");
				m_selected = true;
				Game.m_isTwoPlayer = false;
			}// end if
		}
		 
		override public function onClick(e:MouseEvent):void
		{
			super.onClick(e);
			execute();
		}
		
		public function execute():void
		{
			gotoAndStop("selected");
			m_selected = true;
			var sibling:SelectPlayerButton;
			if (name == "select1Player")
			{
				Game.m_isTwoPlayer = false;
				sibling = parent.getChildByName("select2Player") as SelectPlayerButton;
				sibling.deselect();
				SoundManager.playSound("OnePlayer");
			}
			else
			{
				Game.m_isTwoPlayer = true;
				sibling = parent.getChildByName("select1Player") as SelectPlayerButton;
				sibling.deselect();		
				SoundManager.playSound("TwoPlayer");
			}// end else
		}
		
		protected override function onMouseOver(e:MouseEvent):void
		{
			if (!m_selected)
			{
				super.onMouseOver(e);
			}// end if
		}
		protected override function onMouseOut(e:MouseEvent):void
		{
			if (!m_selected)
			{
				super.onMouseOut(e);
			}// end if
		}		
		
		public function deselect():void
		{		
			gotoAndStop("up");
			m_selected = false;
		}// end public function deselect():void
		
	}// end public class SelectPlayerButton extends BasicButton
}