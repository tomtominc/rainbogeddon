﻿package com.nitrome.ui {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.ui.Keyboard;
	
	/**
	* Manages the in game pop-ups
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class PopUpHolder extends MovieClip{
		
		public var id:String;
		public var keyPressed:Boolean;
		public var scrollLock:Boolean;
		
		private var nextId:String;
		private var currentStyle:String;
		private var nextStyle:String;
		private var nextLockScroll:Boolean;
		
		public static const DROP:String = "drop";
		public static const DEFAULT:String = DROP;
		
		public function PopUpHolder(){
			id = "wait";
			nextId = "";
			nextStyle = "";
			scrollLock = false;
		}
		
		/* Launch a pop-up and goes to the id frame in the pop-up
		 *
		 * noAnim skips the animation for bringing the pop-up in and lockScrolling determines whether the view can be
		 * scrolled with the pop up active */
		public function show(id:String, style:String = DEFAULT, noAnim:Boolean = false, lockScrolling:Boolean = true):void{
			// queue the pop-up if it is busy
			if(currentLabel.indexOf("Stopped") > -1){
				queue(id, style, lockScrolling);
				return;
			}
			scrollLock = lockScrolling;
			this.id = id;
			currentStyle = style;
			keyPressed = false;
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			if(!noAnim) gotoAndPlay(currentStyle + "In");
			else popUp.gotoAndStop(id);
		}
		/* Hides the pop-up */
		public function hide():void{
			keyPressed = true;
			scrollLock = false;
			id = "wait";
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			gotoAndPlay(currentStyle + "Out");
		}
		
		/* Sets the next pop-up to open and hides the current */
		public function queue(id:String, style:String = DEFAULT, lockScrolling:Boolean = true):void{
			nextId = id;
			nextStyle = style;
			nextLockScroll = lockScrolling;
			hide();
		}
		
		/* Launches a pop-up or hides it based on it's name - can also be used to queue another pop-up if
		 * a pop-up is already open */
		public function toggle(id:String, style:String = DEFAULT, lockScrolling:Boolean = true):void{
			if(id == this.id){
				hide();
			} else {
				show(id, style, false, lockScrolling);
			}
		}
		
		/* Checks the queue for another pop-up in waiting and launches it if found - returns if the action launched */
		private function checkQueue():Boolean{
			if(nextId != ""){
				show(nextId, nextStyle, false, nextLockScroll);
				nextId = "";
				return true;
			}
			return false;
		}
		
		/* Listen for the space bar being pressed to trigger the default action for the current pop-up */
		private function onEnterFrame(e:Event):void {
			
			//if(currentLabel.substr(currentLabel.indexOf("_") + 1) == "Stopped" && Key.isDown(Keyboard.SPACE) && !keyPressed){
			if((currentLabel.indexOf("Stopped") > -1) && Key.isDown(Keyboard.SPACE) && !keyPressed){
				keyPressed = true;
				if(id == "levelComplete"){
					popUp.levelCompletePanel.nextLevelButton.execute();
				} else if(id == "gameOver"){
					popUp.levelFailedPanel.tryAgainButton.execute();
				} else if(id == "finalLevelComplete"){
					popUp.finalLevelCompletePanel.continueEndButton.execute();
				} else if (id.indexOf("_info") > -1) {
					popUp.weaponInfoOKButton.execute();
				}// end else if
			}
			if(currentLabel == "wait" && !Key.isDown(Keyboard.SPACE)){
				keyPressed = false;
			}
		}
		
	}
	
}