﻿/**
* Pulls the highscores off of the server and lists them in HighScoreLine objects
*
* @author Aaron Steed, nitrome.com
* @version 0.1
*/

package com.nitrome.ui.highscore {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;

	public class HighScoreBoard extends MovieClip{
		public const MAX_DIGITS:int = 8;
		public const NUM_LINES:int = 10;
		private const MAX_RANK:int = 100;
		private var _zeroFill:Boolean = false;
		private var savedMinRank:int;
		private var scoreLines:Array;
		//private var prev_arrow:ArrowButton;
		//private var next_arrow:ArrowButton;
		private var scoresLoader:URLLoader;
		private var scoresVars:URLVariables;
		private var scoresRequest:URLRequest;
		private var loaded:Boolean;
		
		public function HighScoreBoard(){
			scoreLines = new Array(NUM_LINES);
			if(NitromeGame.verify()){
				// Locate instances
				for(var i:int = 0; i < scoreLines.length; i++){
					scoreLines[i] = getChildByName("scoreLine"+(i+1)) as HighScoreLine;
				}
				loaded = false;
				// wait for this object to be added to stage before initialising
				addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
				//addEventListener(Event.ENTER_FRAME, loadText, false, 0, true);
			} else {
				gotoAndStop("hide");
			}
			
			left.visible = false;
			right.visible = false;
			
			
		}
		// Getter setters
		public function get zeroFill():Boolean {
			return _zeroFill;
		}
		public function set zeroFill(newZeroFill:Boolean):void{
			_zeroFill = newZeroFill;
		}
		/* Pull in the highscores for this game from the server */
		public function loadHighScores(minRank:int):void {
			//save the minRank - we will need it later to display the ranking on the board
			savedMinRank = minRank;
			// initialise our communication objects
			scoresLoader = new URLLoader();
			scoresVars = new URLVariables();
			scoresRequest = new URLRequest(NitromeGame.RETRIEVE_URL);
			scoresRequest.method = URLRequestMethod.POST;
			scoresRequest.data = scoresVars;
			scoresLoader.dataFormat = URLLoaderDataFormat.VARIABLES;
			scoresLoader.addEventListener(Event.COMPLETE, retrieveSuccessful);
			scoresLoader.addEventListener(IOErrorEvent.IO_ERROR, retrieveFailed);
			
			// load our variables into scoresVars
			scoresVars.min_rank=String(minRank);

			// game name - The leader board presented is the one for the current level
			scoresVars.game_name=NitromeGame.gameId;
			
			// is the score time based? send "1" for yes and "0" for no
			scoresVars.time_based=NitromeGame.timeBased ? "1" : "0";

			// send request
			scoresLoader.load(scoresRequest);
		}
		/* Iterate through the scoreLines array add the correct rank, name and score for each */
		public function displayHighScores(dataString:String):void {
			if (dataString != "0"){
				for (var i:int = 0; i<scoreLines.length; i++){
					var scoreObj:Object=NitromeGame.getHighScoreLine(dataString, i+1);
					if (scoreObj == null){
						scoreLines[i].hide();
					} else {
						scoreLines[i].displayData(savedMinRank+i, scoreObj.username, scoreObj.score);
					}
				}
				//display the next button?
				if(NitromeGame.displayNextButton(dataString)){
					nextArrow.show();
					right.visible = true;
				}else{
					nextArrow.hide();
					right.visible = false;
				}
				//display the previous button?
				if(NitromeGame.displayPreviousButton(dataString)){
					prevArrow.show();
					left.visible = true;
				}else{
					prevArrow.hide();
					left.visible = false;
				}
				//gotoAndPlay("reveal");
			}else{
				//error getting highscores - oh dear
			}
		}
		/* Go to the next page of highscores */
		public function shiftScoresPrev():void {
			//gotoAndStop("loading");
			var min:int = savedMinRank-10;
			if (min<1){
				min = 1;
			}
			loadHighScores(min);
		}
		/* Go to the previous page of highscores */
		public function shiftScoresNext():void {
			//gotoAndStop("loading");
			var min:int = savedMinRank+10;
			loadHighScores(min);
		}
		
		///////////////////////////////////////////////////////////////
		//used with MTV's GameService api/////////////////////////////
		
		// Note - not yet tested - I've just updated the syntax for a head start
		public function displayHighScoresMTV(leaderboard:Array):void{
			if (leaderboard.length > 0){
				for (var i:int=0; i<=9; i ++){
					var n:String= String(leaderboard[i].user_name);
					var s:String=String(leaderboard[i].score);
					
					if(n=="" || n==null){
						scoreLines[i].hide();
						nextArrow.hide();
					}else{
						scoreLines[i].displayData(savedMinRank+i, n, s);
					}
				}
				//gotoAndPlay("reveal");
			}else{
				trace("no leaderboard data");
			}
			
		}
		
		private function init(e:Event):void{
			loadHighScores(1);
            e.target.removeEventListener(Event.ADDED_TO_STAGE , init);
		}
		/*private function loadText(e:Event):void{
			if(loaded && loadingText.alpha > 0){
				loadingText.alpha -= 0.1;
			} else if(loaded && loadingText.alpha <= 0){
				e.target.removeEventListener(Event.ENTER_FRAME, loadText);
			}
		}*/
		private function retrieveSuccessful(e:Event):void{
			var dataString:String = e.target.data.result;
			trace("Scores retrieved: (" + dataString + ")");
			displayHighScores(dataString);
			
			// update the highscore buffer
			/*if(savedMinRank == 1){
				NitromeGame.high_score_buffer[NitromeGame.selectedLevel - 1] = data_string;
			}*/
			
			loaded = true;
		}
		private function retrieveFailed(e:IOErrorEvent):void{
			trace("Retrieve scores failed: "+e.text);
		}
	}
	
}
