﻿package com.nitrome.ui {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	
	/**
	* ValueTicker
	*
	* modified this to count up as well as down
	*
	* to count down set a negative inc value and a high start value (eg: what you could have scored
	* if you hadn't of screwed up)
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class ValueTicker extends MovieClip{
		
		public var value:int;
		public var prefix:String;
		
		private var countInterval:int;
		private var total:int;
		private var inc:int;
		private var nextTicker:String;
		private var nextTotal:int;
		private var nextInc:int;
		private var nextStartVal:int;
		private var counting:Boolean;
		private var count:int;
		
		public function ValueTicker(){
			counting = false;
			prefix = "";
		}
		
		/* Starts a ScoreTicker called clip on the parent to start a count when this finishes */
		public function startOnFinish(nextTicker:String, target:int, inc:int, startValue:int = 0):void{
			this.nextTicker = nextTicker;
			nextTotal = target;
			if(inc == 0) inc = startValue > target ? -1 : 1;
			nextInc = inc;
			nextStartVal = startValue;
			if(startValue > 0){
				parent[nextTicker].setValue(startValue);
			}
		}
		
		/* Start counting in increments of inc from a start value of startVal */
		public function startCount(target:int, inc:int, startValue:int = 0, startDelay:int = 0):void{
			total = target;
			if (inc == 0) inc = startValue > target ? -1 : 1;
			this.inc = inc;
			count = startDelay;
			setValue(startValue);
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			counting = true;
		}
		
		/* Initiate counting from the current value to a target */
		public function setTarget(target:int, inc:int):void{
			total = target;
			if (inc == 0) inc = value > target ? -1 : 1;
			else inc = value > target ? -inc : inc;
			this.inc = inc;
			if(!counting){
				addEventListener(Event.ENTER_FRAME, update, false, 0, true);
				counting = true;
			}
		}
		
		private function update(e:Event):void{
			if(count){
				count--;
				return;
			}
			//play a sound?
			value += inc;
			if((inc > 0 && value >= total) || (inc < 0 && value <= total)){
				value = total;
				removeEventListener(Event.ENTER_FRAME, update);
				counting = false;
				onFinish();
			}
			scoreTextField.text = prefix + Math.abs(value);
		}
		
		private function onFinish():void{
			if(nextTicker != null){
				if(parent && parent[nextTicker]){
					parent[nextTicker].startCount(nextTotal, nextInc, nextStartVal);
				}
			} else {
				// playSound
			}
		}
		
		/* Force a score value */
		public function setValue(n:int):void {
			value = n;
			scoreTextField.text = prefix + Math.abs(value);
		}
		
		public function get scoreTextField():TextField 
		{
			return null;
		}// end public function get scoreTextField():TextField 		
		
	}
	
}