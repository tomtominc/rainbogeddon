﻿package com.nitrome.util.clips {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	
	/* Pause hack - freezes gfx and all of its children */
	public function countClips(gfx:DisplayObjectContainer, counter:Object):void{
		counter.n += gfx.numChildren;
		for(var i:int = 0; i < gfx.numChildren; i++){
			if(gfx.getChildAt(i) is DisplayObjectContainer){
				countClips(gfx.getChildAt(i) as DisplayObjectContainer, counter);
			}
		}
	}
}