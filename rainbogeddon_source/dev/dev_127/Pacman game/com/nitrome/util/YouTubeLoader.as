package com.nitrome.util {
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.Security;
	
	/**
	 * Loads in videos from YouTube and plays them
	 *
	 * YouTube API docs: http://code.google.com/apis/youtube/flash_api_reference.html
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class YouTubeLoader extends Sprite{
			
		public var player:Object; // This will hold the API player instance once it is initialized.
		public var playerWidth:Number;
		public var playerHeight:Number;
		public var playList:Array/*String*/;
		public var playListIndex:int;
		public var currentVideoId:String;
		public var muted:Boolean;
		public var ready:Boolean;
		public var failed:Boolean;
		
		private var playerLoader:Loader;
		
		public static const VIDEO_ID_STEAMLANDS:String = "aU_X2GCSk8M";
		public static const VIDEO_ID_DRAMATIC:String = "jHjFxJVeCQs";
		public static const VIDEO_ID_LEEK:String = "1wnE4vF9CQ4";
		
		// arrays are not allowed to be set in the parameters
		private static const PLAY_LIST_TEST:Array = [VIDEO_ID_DRAMATIC, VIDEO_ID_LEEK];
		
		// youtube player states:
		public static const UNSTARTED:int = -1;
		public static const ENDED:int = 0;
		public static const PLAYING:int = 1;
		public static const PAUSED:int = 2;
		public static const BUFFERING:int = 3;
		public static const VIDEO_CUED:int = 5;
		
		// youtube error codes:
		public static const ERROR_INVALID_ID:int = 2;
		public static const ERROR_VIDEO_NOT_FOUND:int = 100;
		public static const ERROR_EMBED_NOT_ALLOWED:int = 101;
		public static const ERROR_EMBED_NOT_ALLOWED_2:int = 150;
		
		public function YouTubeLoader(width:Number, height:Number, playList:Array = null, muted:Boolean = true) {
			this.playerWidth = width;
			this.playerHeight = height;
			if(!playList) playList = PLAY_LIST_TEST;
			this.playList = [];
			this.playListIndex = 0;
			this.muted = muted;
			ready = false;
			failed = false;
			
			playerLoader = new Loader();
			addChild(playerLoader);
			playerLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onPlayerLoadError, false, 0 , true);
			playerLoader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit, false, 0 , true);
			playerLoader.load(new URLRequest("http://www.youtube.com/apiplayer?version=3"));
			addEventListener(Event.ENTER_FRAME, monitor, false, 0, true);
		}
		
		public static function init():void{
			// The player SWF file on www.youtube.com needs to communicate with your host
			// SWF file. Your code must call Security.allowDomain() to allow this
			// communication.
			Security.allowDomain("www.youtube.com");
		}
		
		public function pause():void{
			if(player && player.getPlayerState() != PAUSED) player.pauseVideo();
		}
		
		public function unpause():void{
			if(player && player.getPlayerState() == PAUSED) player.playVideo();
		}
		
		/* This must be called to garbage collect the YouTubeLoader */
		public function destroy():void{
			removePlayer();
			while(numChildren) removeChildAt(0);
			removeEventListener(Event.ENTER_FRAME, monitor);
			ready = false;
		}
		
		/* Garbage collection for the player */
		private function removePlayer():void{
			if(playerLoader){
				if(playerLoader.content){
					playerLoader.content.removeEventListener("onReady", onPlayerReady);
					playerLoader.content.removeEventListener("onError", onPlayerError);
					playerLoader.content.removeEventListener("onStateChange", onPlayerStateChange);
					playerLoader.content.removeEventListener("onPlaybackQualityChange",
						onVideoPlaybackQualityChange);
				}
				playerLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onPlayerLoadError);
			}
			if(player){
				player.destroy();
			}
			if(playerLoader){
				playerLoader.unload();
			}
			player = null;
			playerLoader = null;
		}
		
		private function onLoaderInit(e:Event):void {
			playerLoader.content.addEventListener("onReady", onPlayerReady, false, 0 , true);
			playerLoader.content.addEventListener("onError", onPlayerError, false, 0 , true);
			playerLoader.content.addEventListener("onStateChange", onPlayerStateChange, false, 0 , true);
			playerLoader.content.addEventListener("onPlaybackQualityChange",
				onVideoPlaybackQualityChange, false, 0 , true);
		}

		private function onPlayerReady(e:Event):void {
			// Event.data contains the event parameter, which is the Player API ID
			trace("youtube ready:", Object(e).data);
			
			// Once this event has been dispatched by the player, we can use
			// cueVideoById, loadVideoById, cueVideoByUrl and loadVideoByUrl
			// to load a particular YouTube video.
			player = playerLoader.content;
			// Set appropriate player dimensions for your application
			// (small quality defaults to 320, 240)
			player.setSize(playerWidth, playerHeight);
			player.setPlaybackQuality("small");
			if(muted) player.mute();
			currentVideoId = playList[playListIndex++]
			player.cueVideoById(currentVideoId);
			player.playVideo();
			ready = true;
		}

		private function onPlayerError(e:Event):void {
			// Event.data contains the event parameter, which is the error code
			trace("youtube error:", Object(e).data);
			
			var errorCode:int = int(Object(e).data);
			if(errorCode == ERROR_EMBED_NOT_ALLOWED || errorCode == ERROR_EMBED_NOT_ALLOWED_2){
				trace("Embed not allowed:" + currentVideoId);
			} else if(errorCode == ERROR_INVALID_ID){
				trace("Invalid ID:" + currentVideoId);
			} else if(errorCode == ERROR_VIDEO_NOT_FOUND){
				trace("Video not found:" + currentVideoId);
			}
			
			showVideoSourceError();
		}
		
		private function onPlayerLoadError(e:Event):void{
			trace("Loading the YouTube player failed");
			removePlayer();
			failed = true;
			showVideoSourceError();
		}
		
		/* When any errors occur, we need to show a test card */
		private function showVideoSourceError():void{
			removePlayer();
			ready = true;
			var error:Sprite = new VideoSourceError();
			error.width = playerWidth;
			error.height = playerHeight;
			addChild(error);
		}

		private function onPlayerStateChange(e:Event):void {
			// Event.data contains the event parameter, which is the new player state
			//trace("youtube state:", Object(e).data);
			
			// when the video has ended, roll on to
			if(player.getPlayerState() == ENDED){
				playNextVideo();
			}
		}
		
		/* Queues the next video */
		private function playNextVideo():void{
			if(playListIndex >= playList.length) playListIndex = 0;
			currentVideoId = playList[playListIndex++];
			if(playList.length > 1){
				player.cueVideoById(currentVideoId);
				player.playVideo();
			} else {
				player.seekTo(0);
			}
		}

		private function onVideoPlaybackQualityChange(e:Event):void {
			// Event.data contains the event parameter, which is the new video quality
			//trace("youtube quality:", Object(e).data);
		}
		
		/* State change events sometimes don't fire, so the player must be monitored */
		private function monitor(e:Event):void{
			if(player){
				if(player.getPlayerState() == PLAYING && player.getCurrentTime() == player.getDuration()){
					playNextVideo();
				}
			}
		}
		
		
	}

}