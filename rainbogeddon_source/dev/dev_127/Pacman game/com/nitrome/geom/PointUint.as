package com.nitrome.geom
{
	
	
	public class PointUint
	{
		
		public  var m_x:uint;
		public  var m_y:uint;
		
		public function PointUint(x_:uint = 0, y_:uint = 0)
		{
			m_x = x_;
			m_y = y_;
		}// end 		
		
	}// end public class PointUint

}// end package