package com.nitrome.engine
{
	
	public class PowerUpGun extends PowerUp
	{
		
		public function PowerUpGun(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpGun(tx_:uint, ty_:uint)
		
		public override function get soundID():String
		{		
			return "Bullet";
		}// end public override function get soundID():String			
		
		public override function get holder():Class
		{		
			return BulletHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpGun
	
}// end package