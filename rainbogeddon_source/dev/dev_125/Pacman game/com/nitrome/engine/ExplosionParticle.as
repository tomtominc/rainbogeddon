package com.nitrome.engine
{
		
	import com.nitrome.engine.utils.TileOutliner;
	import com.nitrome.engine.pathfinding.PathMap;
	import flash.display.MovieClip;
	import flash.display.BitmapData;
	import flash.geom.*;
	
	public class ExplosionParticle extends Entity
	{
			
		public static const STATE_FADING_IN:uint  = 0;	
		public static const STATE_NORMAL:uint     = 1;	
		public static const STATE_FADING_OUT:uint = 2;	
		
		public static const NUM_MAIN_ANIM_LOOP_FRAMES:uint = 7;
		
		public static var m_tileOutliner:TileOutliner;
		public var m_fadeInAnimGuideParticle:ExplosionFadeInGuideParticle;
		public static var m_mainLoopAnimGuideParticle:ExplosionMainGuideParticle;
		public static var m_fadeOutAnimGuideParticle:ExplosionFadeOutGuideParticle;
		public var m_parent:EntityBomb;
		public var m_timelineCode:Boolean;
		public var m_startedFadingIn:Boolean;
		private var m_contourID:uint;
		//private var m_firstFrame:uint;
		private static var m_colorTable:Vector.<uint>;
		
		public function ExplosionParticle(tx_:uint, ty_:uint, parent_:EntityBomb, fadeInGuideClip_:ExplosionFadeInGuideParticle)
		{
		
			m_timelineCode = false;
			m_drawFast = true;
			super(tx_, ty_);					
			m_collisionRect.x = 1;
			m_collisionRect.y = 1;
			m_collisionRect.width = 23;
			m_collisionRect.height = 23;
			m_drawBitmap = Game.g.m_effectsBitmap;	
			m_parent = parent_;
			m_timelineCode = true;
			m_startedFadingIn = false;
			m_moves = false;
			m_canDie = false;
			m_spatialHashMap.updateObject(this);
			if (!m_tileOutliner)
			{
				m_tileOutliner = new TileOutliner();
				m_tileOutliner.m_tileMap = Game.g.m_safetyParticleTileMap.m_tileMap;
				m_tileOutliner.m_mapWidth  = Game.g.mapWidth;
				m_tileOutliner.m_mapHeight = Game.g.mapHeight;
				m_tileOutliner.m_emptyTileValue = PathMap.TILE_SENTINEL;	
				
				m_colorTable = new Vector.<uint>(7);
				
				m_colorTable[0] = 0xffff0017;
				m_colorTable[1] = 0xffff6e00;
				m_colorTable[2] = 0xffffe700;
				m_colorTable[3] = 0xff04FF01;
				m_colorTable[4] = 0xff0091ff;
				m_colorTable[5] = 0xff8E00ff;
				m_colorTable[6] = 0xffeb00c0;
				
			}// end if
			
			m_fadeInAnimGuideParticle = fadeInGuideClip_;
			
			if (!m_mainLoopAnimGuideParticle)
			{
				//m_fadeInAnimGuideParticle   = new ExplosionFadeInGuideParticle();
				m_mainLoopAnimGuideParticle = new ExplosionMainGuideParticle();
				m_fadeOutAnimGuideParticle  = new ExplosionFadeOutGuideParticle();
				m_mainLoopAnimGuideParticle.gotoAndPlay("t_0");
			}// end if
			
			m_state = STATE_FADING_IN;
			
		}// end public function ExplosionParticle(tx_:uint, ty_:uint, parent_:EntityBomb)
		
		public static function free():void		
		{
			m_tileOutliner = null;
			m_mainLoopAnimGuideParticle = null;
			m_fadeOutAnimGuideParticle = null;
			m_colorTable = null;
		}// end public static function free():void		
		
		public function fadeOutDone():void
		{
			//trace("fadeOutDone");
			if (m_timelineCode)
			{
				m_parent.onFaded();
				stop();
				m_terminated = true;
				m_spatialHashMap.unregisterObject(this);
			}// end if
		}// end public function fadeOutDone():void		
		
		public function startSyncedAnimLoop():void
		{
			if (m_timelineCode)
			{
				var contourID:uint = m_tileOutliner.getContourID(m_tx, m_ty);
				m_contourID = contourID;
				gotoAndStop("t_" + contourID);
				//m_firstFrame = currentFrame;
			}// end if
		}// end public function startSyncedAnimLoop():void		
		
		public override function onGamePaused():void
		{		
			stop();
		}// end public override function onGamePaused():void
		
		public override function onGameUnpaused():void
		{		
			if ((m_state == STATE_FADING_IN) || (m_state == STATE_FADING_OUT))
			{			
				play();
			}// end if			
		}// end public override function onGameUnpaused():void		
		
		public function fade():void
		{
			//trace("fade");
			m_state = STATE_FADING_OUT;
			var contourID:uint = m_tileOutliner.getContourID(m_tx, m_ty);
			gotoAndPlay("t_" + contourID + "_fade_out");
			m_contourID = 0;
		}// end public function fade():void	
		
		public function endFadeIn():void
		{
			if (m_timelineCode)
			{
				m_state = STATE_NORMAL;
				var contourID:uint = m_tileOutliner.getContourID(m_tx, m_ty);
				gotoAndStop("t_" + contourID);
				m_parent.onParticleFadeIn();
			}// end if
		}// end public function endFadeIn():void			
		
		public override function doPhysics():void
		{
			
			if (m_terminated)
			{
				return;
			}// end if

			var contourID:uint;

			if (m_state == STATE_FADING_IN)
			{
				
				if (!m_startedFadingIn)
				{
					m_startedFadingIn = true;
					contourID = m_tileOutliner.getContourID(m_tx, m_ty);
					gotoAndPlay("t_" + contourID + "_fade_in");
				}// end if
				
			}
			else if (m_state == STATE_NORMAL)
			{
				contourID = m_tileOutliner.getContourID(m_tx, m_ty);
				if (contourID != m_contourID)
				{
					gotoAndStop("t_" + contourID);
					m_contourID = contourID;
				}// end if
			}// end else if
			
		}// end public override function doPhysics():void				
		
		public override function draw():void
		{
			
			if (m_state != STATE_NORMAL)
			{
				super.draw();
			}
			else
			{
				
				m_bounds = m_boundsList[currentFrame + m_parent.m_currMainAnimFrameOffset];
				
				if (m_contourID == 255)
				{
				
					var color:uint = m_colorTable[m_parent.m_currMainAnimFrameOffset]; // m_colorTable[currentFrame - m_firstFrame];
					
					m_drawBitmap.fillRect(new Rectangle(x,y,m_bounds.width, m_bounds.height), color);
					
					if (x + (m_bounds.x + m_bounds.width) > m_drawBitmap.width)
					{
						m_drawBitmap.fillRect(new Rectangle(x - m_drawBitmap.width,y,m_bounds.width, m_bounds.height), color);
					}
					else if ((x + m_bounds.x) < 0)
					{
						m_drawBitmap.fillRect(new Rectangle(x + m_drawBitmap.width,y,m_bounds.width, m_bounds.height), color);		
					}
					else if (y + (m_bounds.y + m_bounds.height) > m_drawBitmap.height)
					{
						m_drawBitmap.fillRect(new Rectangle(x,y - m_drawBitmap.height,m_bounds.width, m_bounds.height), color);			
					}
					else if ((y + m_bounds.y) < 0)
					{
						m_drawBitmap.fillRect(new Rectangle(x,y + m_drawBitmap.height,m_bounds.width, m_bounds.height), color);
					}// end else if			
					
				}
				else
				{
				

					if (!m_bounds.width || !m_bounds.height)
					{
						return;
					}// end if
						
					var chosenBitmap:BitmapData = m_frameBitmapList[currentFrame + m_parent.m_currMainAnimFrameOffset - 1];
				
					var finalX:Number = x;
					var finalY:Number = y;
					
					m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x, finalY + m_bounds.y), null, null, true);					
					
				
				}// end else
			
			}// end else
			
			
		}// end public override function draw():void		
		
	}// end public class ExplosionParticle
	
}// end package