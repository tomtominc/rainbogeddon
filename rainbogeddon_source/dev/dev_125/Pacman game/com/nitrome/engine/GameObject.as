package com.nitrome.engine
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.geom.Matrix;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.geom.*;
	
	public class GameObject extends MovieClip 
	{
		
		public  var  m_vel:Vector3D;
		public  var  m_tx:uint;
		public  var  m_ty:uint;
		public  var  m_drawBitmap:BitmapData;
		public  var  m_bounds:Rectangle;
		public  var  m_terminated:Boolean;
		public  var  m_drawWrapped:Boolean;
		public  var  m_draw:Boolean;
		public  var  m_drawOrder:uint;
		public  var  m_flicker:Boolean;
		public  var  m_drawFast:Boolean;
		public  var  m_animInitialized:Boolean;
		private var  m_frameClip:MovieClip; // clip that actually contains the frames of animation
		public  var  m_frameBitmapList:Vector.<BitmapData>;
		private var  m_flippedFrameBitmapList:Vector.<BitmapData>;
		private static var m_animationLibrary:Object;
		public  var  m_boundsList:Vector.<Rectangle>;
		public  var  m_time:int;
		
		public function GameObject(tx_:uint, ty_:uint)
		{
			m_vel = new Vector3D();
			x = (Game.TILE_WIDTH * tx_);
			y = (Game.TILE_HEIGHT * ty_);	
			m_tx = tx_;
			m_ty = ty_;
			m_bounds = getBounds(this);
			m_drawWrapped = true;
			m_draw = true;
			m_drawOrder = 0;
			m_flicker = false;
			m_animInitialized = false;
						
			if (numChildren && (getChildAt(0) is MovieClip))
			{
				m_frameClip = getChildAt(0) as MovieClip;
			}
			else
			{
				m_frameClip = this;
			}// end else
			
			if (m_drawFast)
			{
			
				var thisClass:Class = Object(this).constructor;
				
				if (!m_animationLibrary)
				{
					m_animationLibrary = new Object();
				}// end if
				
				if (m_animationLibrary[thisClass])
				{
					m_frameBitmapList = m_animationLibrary[thisClass][0];
					m_flippedFrameBitmapList = m_animationLibrary[thisClass][1];
					m_boundsList = m_animationLibrary[thisClass][2];
				}
				else
				{
					

					m_animationLibrary[thisClass] = new Object();
					m_animationLibrary[thisClass][0] = new Vector.<BitmapData>;
					m_animationLibrary[thisClass][1] = new Vector.<BitmapData>;		
					m_animationLibrary[thisClass][2] = new Vector.<Rectangle>;
				
					m_frameBitmapList = m_animationLibrary[thisClass][0];
					m_flippedFrameBitmapList = m_animationLibrary[thisClass][1];
					m_boundsList = m_animationLibrary[thisClass][2];
					
					for (var currFrame:uint = 1; currFrame <= m_frameClip.totalFrames; currFrame++)
					{
						
						m_frameClip.gotoAndStop(currFrame);
						m_bounds = m_frameClip.getBounds(m_frameClip);
						m_boundsList.push(m_frameClip.getBounds(m_frameClip));
						if (m_bounds.width && m_bounds.height)
						{
							m_frameBitmapList.push(new BitmapData(m_bounds.width, m_bounds.height, true, 0));
							m_frameBitmapList[m_frameBitmapList.length - 1].draw(this, new Matrix(1, 0, 0, 1, -m_bounds.x, -m_bounds.y));
							
							m_flippedFrameBitmapList.push(new BitmapData(m_bounds.width, m_bounds.height, true, 0));
							m_flippedFrameBitmapList[m_frameBitmapList.length - 1].draw(this, new Matrix(-1, 0, 0, 1, -m_bounds.x, -m_bounds.y));
						}
						else
						{
							m_frameBitmapList.push(null);
							m_flippedFrameBitmapList.push(null);
						}// end else
						
						
					}// end for
				
					m_frameClip.gotoAndPlay(1);
				
				}// end else
				
			}// end if
			
			m_terminated = false;
			m_animInitialized = true;
			
		}// end public function GameObject(tx_:uint, ty_:uint)
		
		public static function free():void		
		{
			m_animationLibrary = null;
		}// end public static function free():void	
		
		public function onGameEnded():void
		{
			stop();
		}// end public function onGameEnded():void				
		
		public function onGamePaused():void
		{
			
		}// end public function onGamePaused():void		
		
		public function onGameUnpaused():void
		{
			
		}// end public function onGameUnpaused():void					
		
		public function tick():void
		{

			doInput();
			m_time = getTimer();
			doPhysics();			
			m_time = getTimer() - m_time;
			
		}// end public function tick():void
		
		public function updatePosFromTile():void
		{
			x = (Game.TILE_WIDTH * m_tx);
			y = (Game.TILE_HEIGHT * m_ty);			
		}// end public function updatePosFromTile():void		
		
		public function updateTilePos():void
		{
			
		}// end public function updateTilePos():void			
		
		public function doPhysics():void
		{
			
		}// end public function doPhysics():void		
		
		public function doInput():void
		{
			
		}// end public function doInput():void			
		
		public function draw():void
		{
			
			if (!m_draw || (m_flicker && (Game.g.frameCount % 2)))
			{
				return;
			}// end if
			
			m_bounds = m_frameClip.getBounds(m_frameClip);
			

			if (m_drawFast)
			{
				
				if (!m_bounds.width || !m_bounds.height)
				{
					return;
				}// end if
				
				
				var chosenBitmap:BitmapData;
				
				if (scaleX >= 0)
				{
					chosenBitmap = m_frameBitmapList[m_frameClip.currentFrame-1];
				}
				else
				{
					chosenBitmap = m_flippedFrameBitmapList[m_frameClip.currentFrame-1];
				}// end else
				
				var finalX:Number = x;
				var finalY:Number = y;
				
				if (m_frameClip != this)
				{
					finalX += m_frameClip.x;
					finalY += m_frameClip.y;
				}// end if
				
				m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x, finalY + m_bounds.y), null, null, true);
				
				if (finalX + (m_bounds.x + m_bounds.width) > m_drawBitmap.width)
				{
					m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x - m_drawBitmap.width, finalY + m_bounds.y), null, null, true);			
				}
				else if ((finalX + m_bounds.x) < 0)
				{
					m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x + m_drawBitmap.width, finalY + m_bounds.y), null, null, true);		
				}
				else if (finalY + (m_bounds.y + m_bounds.height) > m_drawBitmap.height)
				{
					m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x, finalY + m_bounds.y - m_drawBitmap.height), null, null, true);			
				}
				else if ((finalY + m_bounds.y) < 0)
				{
					m_drawBitmap.copyPixels(chosenBitmap, chosenBitmap.rect, new Point(finalX + m_bounds.x, finalY + m_bounds.y + m_drawBitmap.height), null, null, true);
				}// end else if				
				
				
			}
			else
			{
			
				var matrix:Matrix = new Matrix();
				
				matrix.scale(scaleX, scaleY);
				matrix.rotate(rotation*(Math.PI/180));
				matrix.translate(x, y);
				
				m_drawBitmap.draw(this, matrix, new ColorTransform(1,1,1,alpha));
				
				// draw the wrapping parts
				
				if (!m_drawWrapped)
				{
					return;
				}// end if
				
				if (x + (m_bounds.x + m_bounds.width) > m_drawBitmap.width)
				{
					matrix.identity();
					matrix.scale(scaleX, scaleY);
					matrix.rotate(rotation*(Math.PI/180));
					matrix.translate(x - m_drawBitmap.width, y);
					m_drawBitmap.draw(this, matrix, new ColorTransform(1,1,1,alpha));				
				}
				else if ((x + m_bounds.x) < 0)
				{
					matrix.identity();
					matrix.scale(scaleX, scaleY);
					matrix.rotate(rotation*(Math.PI/180));
					matrix.translate(x + m_drawBitmap.width, y);
					m_drawBitmap.draw(this, matrix, new ColorTransform(1,1,1,alpha));				
				}
				else if (y + (m_bounds.y + m_bounds.height) > m_drawBitmap.height)
				{
					matrix.identity();
					matrix.scale(scaleX, scaleY);
					matrix.rotate(rotation*(Math.PI/180));
					matrix.translate(x, y - m_drawBitmap.height);
					m_drawBitmap.draw(this, matrix, new ColorTransform(1,1,1,alpha));				
				}
				else if ((y + m_bounds.y) < 0)
				{
					matrix.identity();
					matrix.scale(scaleX, scaleY);
					matrix.rotate(rotation*(Math.PI/180));
					matrix.translate(x, y + m_drawBitmap.height);
					m_drawBitmap.draw(this, matrix, new ColorTransform(1,1,1,alpha));	
				}// end else if
			
			}// end else
				
		}// end public function draw():void			
		
	}// end public class GameObject
	
}// end package