package com.nitrome.engine
{

	import com.nitrome.geom.PointUint;
	
	public class PowerUpManager
	{
	
		private static const MIN_NO_SHOW_WAIT:int = 2000;
		private static const MAX_NO_SHOW_WAIT:int = 10000;		
		private static const MIN_SHOW_WAIT:int = 4000;
		private static const MAX_SHOW_WAIT:int = 15000;	
		private static const FLICKER_WAIT:int = 3000;
		
		private static const STATE_OFF:uint                 = 0;
		private static const STATE_NOT_SHOWING:uint         = 1;
		private static const STATE_SHOWING:uint             = 2;
		private static const STATE_SHOWING_FLICKERING:uint  = 3;
		
		private var m_waitStartTime:int;
		private var m_waitDuration:int;
		private var m_state:uint;
		private var m_powerUpPosList:Vector.<PointUint>;
		private var m_powerUpTypeList:Vector.<Class>;
		private var m_activePowerUp:PowerUp;  
		private var m_chosenPosIndex:uint;
		private var m_firstShow:Boolean;
	
		public function PowerUpManager(powerUpPosList_:Vector.<PointUint>, powerUpTypeList_:Vector.<Class>)
		{
			
			m_powerUpPosList = new Vector.<PointUint>;
			m_powerUpTypeList = new Vector.<Class>;
			
			m_powerUpPosList = m_powerUpPosList.concat(powerUpPosList_);
			m_powerUpTypeList = m_powerUpTypeList.concat(powerUpTypeList_);
			
			
			m_state = STATE_OFF;
			//m_waitStartTime = Game.g.timer;
			//m_waitDuration = MIN_NO_SHOW_WAIT + Math.random() * (MAX_NO_SHOW_WAIT - MIN_NO_SHOW_WAIT);
			m_firstShow = true;
				
		}// end public function PowerUpManager(powerUpPosList_:Vector.<PointUint>, powerUpTypeList_:Vector.<Class>)
			
		/*
		 * Starts the manager cycle
		 */
		public function start()
		{			
			m_state = STATE_NOT_SHOWING;
			m_waitStartTime = Game.g.timer;
			m_waitDuration = MIN_NO_SHOW_WAIT + Math.random() * (MAX_NO_SHOW_WAIT - MIN_NO_SHOW_WAIT);	
		}// end public function start()
		
		/*
		 * Starts the manager cycle showing the first powerup with zero delay
		 */		
		public function startShowing()
		{			
			m_state = STATE_NOT_SHOWING;
			m_waitStartTime = Game.g.timer;
			m_waitDuration = 0;			
		}// end public function startShowing()		
		
		public function tick()
		{	

			if (!m_powerUpPosList.length)
			{
				return;
			}// end if
			
			switch(m_state)
			{
				
				case STATE_NOT_SHOWING:
				{
					
					if (Game.g.timer - m_waitStartTime > m_waitDuration)
					{
						m_state = STATE_SHOWING;
						m_waitDuration = MIN_SHOW_WAIT + Math.random() * (MAX_SHOW_WAIT - MIN_SHOW_WAIT);
						m_waitStartTime = Game.g.timer;
						
						// choose a random position from the list
						
						if (m_firstShow || (m_powerUpPosList.length == 1))
						{
							m_firstShow = false;
							m_chosenPosIndex = Math.floor(Math.random() * m_powerUpPosList.length);
						}
						else
						{
							var testIndex:uint = Math.floor(Math.random() * m_powerUpPosList.length);
							
							while (testIndex == m_chosenPosIndex)
							{
								testIndex = Math.floor(Math.random() * m_powerUpPosList.length);
							}// end while
							
							m_chosenPosIndex = testIndex;
							
						}// end else
						
						// choose a random type from the list
						var typeIndex:uint = Math.floor(Math.random() * m_powerUpTypeList.length);
						
						m_activePowerUp = new m_powerUpTypeList[typeIndex](m_powerUpPosList[m_chosenPosIndex].m_x, m_powerUpPosList[m_chosenPosIndex].m_y);
						Game.g.m_gameObjectList.push(m_activePowerUp);
		
					}// end if
					
					break;
				}// end case
			
				case STATE_SHOWING:
				{
					
					if ((m_waitDuration - (Game.g.timer - m_waitStartTime)) < FLICKER_WAIT)
					{
						m_state = STATE_SHOWING_FLICKERING;
						m_activePowerUp.warnTermination();
					}// end if					
					
					break;
				}// end case		
				
				case STATE_SHOWING_FLICKERING:
				{
					
					if (Game.g.timer - m_waitStartTime > m_waitDuration)
					{
						m_state = STATE_NOT_SHOWING;
						m_waitDuration = MIN_NO_SHOW_WAIT + Math.random() * (MAX_NO_SHOW_WAIT - MIN_NO_SHOW_WAIT);
						m_waitStartTime = Game.g.timer;
						m_activePowerUp.terminate();
					}// end if					
					
					break;
				}// end case				
				
			}// end switch
			
		}// end public function tick()
			
	}// end public class PowerUpManager
	
}// end package