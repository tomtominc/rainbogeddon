package com.nitrome.engine
{
	
	
	public class EntityPoop extends EntityEnemy 
	{
		
		public static const FOLLOWING_DURATION:int = 15000;
		
		public  var m_behaviorStartTime:int;		
		
		public function EntityPoop(tx_:uint, ty_:uint)
		{
			
			super(tx_, ty_);
			m_respawns = false;
			m_drawOrder = 0;
			m_glow.m_color = 0xffff9000;
			m_hasLifetime = true;
			m_lifeStartTime = Game.g.timer;
			m_lifetime = 10000;	
			m_canSmartFollow = false;
			
		}// end public function EntityPoop(tx_:uint, ty_:uint)			
		
		public function onDisappeared():void
		{
			m_showDeathExplosion = false;
			onDeath();
		}// end public function onDisappeared():void			
		
		public override function onLifetimeEnded():void
		{
			//trace("onLifetimeEnded");
			gotoAndPlay("disappearing");
		}// end public override function onLifetimeEnded():void			
		
		public override function doPhysics():void
		{
			
			if (m_isDead)
			{
				return;
			}// end if			
			
			updateLifeTime();
			updateDamage();
			tickChildren();
			updateAbsCollisionRect();
			
		}// end public override function doPhysics():void

		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedOrange;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityPoop
	
}// end package