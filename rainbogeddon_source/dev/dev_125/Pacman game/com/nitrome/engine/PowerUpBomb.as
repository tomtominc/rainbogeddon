package com.nitrome.engine
{
	
	public class PowerUpBomb extends PowerUp
	{
		
		public function PowerUpBomb(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpBomb(tx_:uint, ty_:uint)
		
		public override function get holder():Class
		{		
			return BombHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpBomb
	
}// end package