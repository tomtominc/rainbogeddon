package com.nitrome.engine
{
	
	import com.nitrome.engine.pathfinding.PathMap;
	
	public class EntityProjector extends EntityEnemy 
	{
			
		public function EntityProjector(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = 2.0;
			m_glow.m_color = 0xff00c3ff;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;
			m_checksLineOfSight = true;
		}// end public function EntityProjector(tx_:uint, ty_:uint)
		
		public override function get player1Map():PathMap
		{
			
			if (m_player1OnLineOfSight)
			{
				return Game.g.m_player1PathMap;
			}
			else
			{
				return Game.g.m_player1ProjectedPathMap;
			}// end else
			
		}// end public override function get player1Map():PathMap		
		
		public override function get player2Map():PathMap
		{

			if (m_player2OnLineOfSight)
			{
				return Game.g.m_player2PathMap;
			}
			else
			{
				return Game.g.m_player2ProjectedPathMap;
			}// end else
			
		}// end public override function get player2Map():PathMap	
		
		public override function get player1SafetyMap():PathMap
		{
			
			if (m_player1OnLineOfSight)
			{
				return Game.g.m_safetyPlayer1PathMap;
			}
			else
			{
				return Game.g.m_safetyPlayer1ProjectedPathMap;
			}// end else
			
		}// end public override function get player1SafetyMap():PathMap		
		
		public override function get player2SafetyMap():PathMap
		{
			
			if (m_player2OnLineOfSight)
			{
				return Game.g.m_safetyPlayer2PathMap;
			}
			else
			{
				return Game.g.m_safetyPlayer2ProjectedPathMap;
			}// end else
			
		}// end public override function get player2SafetyMap():PathMap			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnLightBlue;
		}// end public override function get respawnEffectType():Class			
	
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedLightBlue;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityProjector
	
}// end package