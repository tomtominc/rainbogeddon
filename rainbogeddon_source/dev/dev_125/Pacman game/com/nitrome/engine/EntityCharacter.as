package com.nitrome.engine
{
	
	
	public class EntityCharacter extends EntityCentered 
	{
		
		public var m_lastDir:uint;
		public var m_fire:Boolean;
		public var m_stopFire:Boolean;
		public var m_glow:Glow;
		public var m_animModifier:String;
		public var m_forceUpdateAnim:Boolean;
		public var m_weaponHolder:WeaponHolder;
			
		public function EntityCharacter(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);		
			m_lastDir = DIR_NONE;
			m_fire = false;
			m_stopFire = false;
			m_animModifier = "";
			m_forceUpdateAnim = false;
			m_glow = new Glow(this);
		}// end public function EntityCharacter(tx_:uint, ty_:uint)		
		
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath();
			m_isDead = true;
			m_terminated = true;
			m_glow.terminate();
			if (m_weaponHolder)
			{
				m_weaponHolder.terminate();
			}// end if			
			
		}// end public override function onDeath(attacker_:Entity = null):void				
		
		public override function onGamePaused():void
		{
			super.onGamePaused();
			stop();		
		}// end public override function onGamePaused():void		
		
		public override function onGameUnpaused():void
		{
			super.onGameUnpaused();		
			play();		
		}// end public override function onGameUnpaused():void					
		
	}// end public class EntityCharacter
	
}// end package