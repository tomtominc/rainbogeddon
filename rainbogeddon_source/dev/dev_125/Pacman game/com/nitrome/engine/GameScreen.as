package com.nitrome.engine
{
	
	import flash.display.Sprite;
	
	public class GameScreen extends Sprite
	{

		private var m_yVel:Number;
		private var m_force:Number;
		private var m_shaking:Boolean;
		private var m_val:Number;

		public function GameScreen()
		{
			
			m_force = 0;
			m_yVel = 0;
			m_shaking = false;
			
		}// end public function GameScreen()
						
		public function shake():void
		{
			
			m_force = 5;
			m_shaking = true;
			m_val = Math.PI/2;
			
		}// end public function shake():void			
				
		public function doPhysics():void
		{
			
			if (m_shaking)
			{
				y = Math.sin(m_val) * m_force;
				NitromeGame.root.gameEdge.y = y;
				m_val += 1.5;
				m_force -= 0.1;
				if (m_force <= 0)
				{
					y = 0;
					NitromeGame.root.gameEdge.y = 0;
					m_shaking = false;
				}// end if
			}// end if
		
			
		}// end public function doPhysics():void		
		
		
	}// end public class GameScreen
	
}// end package