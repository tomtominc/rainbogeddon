package com.nitrome.engine
{
	
	import com.nitrome.ui.Key;
	import com.nitrome.util.HiddenInt;
	import com.nitrome.sound.SoundManager;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import com.nitrome.engine.pathfinding.PathMap;
	
	public class EntityPlayer extends EntityCharacter
	{
			
		public static const SPEED:Number = 4.6;// 4.0; // 2.8
		public static const STATE_NORMAL:uint               = 0;
		public static const STATE_TELEPORTING_OUT:uint      = 1;
		public static const STATE_TELEPORTING_IN:uint       = 2;
		public static const STATE_STOPPING_FOR_VICTORY:uint = 3;
		public static const STATE_VICTORY:uint              = 4; 
		public static const STATE_STARTED_TELEPORTING:uint  = 5;
		public static const HIT_POINTS:int = 100;
		
		public static const ANIM_WALK_LEFT:uint   = 0;
		public static const ANIM_WALK_RIGHT:uint  = 1;
		public static const ANIM_WALK_UP:uint     = 2;
		public static const ANIM_WALK_DOWN:uint   = 3;
		public static const ANIM_TELEPORTING:uint = 4;
		
		public static const RECOVER_WAIT_DURATION:int = 2000;
		public static const INVISIBILITY_DURATION:int = 2000;
		public static const POS_PROJECTION_LENGTH:uint = 7;
		public static const TEMPORARY_STOP_DURATION:int = 50;
		
		public var m_invincibilitySource:Entity;
		public var m_upKey:Vector.<int>;
		public var m_downKey:Vector.<int>;
		public var m_leftKey:Vector.<int>;
		public var m_rightKey:Vector.<int>;		
		public var m_canPressUp:Boolean;
		public var m_canPressDown:Boolean;
		public var m_canPressLeft:Boolean;
		public var m_canPressRight:Boolean;			
		public var m_fireKey:Vector.<int>;	
		public var m_canPressFire:Boolean;
		public var m_currAnim:uint;
		private var m_prevVel:Vector3D;
		public var m_teleportOldDir:uint;
		public var m_teleporter:EntityTeleporter;
		private var m_oldVel:Vector3D;
		public var m_currTileMarker:Marker;
		public var m_oldTileMarker:Marker;
		public var m_oldPrevTileMarker:Marker;
		public var m_projectedTX:uint;
		public var m_projectedTY:uint;
		public var m_currTeleporterSpin:uint;
		public var m_safetyProjectedTX:uint;
		public var m_safetyProjectedTY:uint;	
		public var m_powerUpRingEffect:PowerUpRingEffect;
		public var m_invisible:Boolean;
		public var m_invisibleStartTime:int;
		public var m_reactingToHit:Boolean;
		public static var m_onWrappedListenerList:Vector.<Function>;
		public var m_score:HiddenInt;
		private var m_doingTemporaryStop:Boolean;
		private var m_temporaryStopStartTime:int;
		public  var m_inputEnabled:Boolean;
		private var m_lastCollectedObject:EntityCollectible;
		
		public function EntityPlayer(tx_:uint, ty_:uint)
		{			
			super(tx_, ty_);
			m_speed = SPEED;
			m_state = STATE_NORMAL;
			m_collisionRect.x = -11;
			m_collisionRect.y = -11;
			m_collisionRect.width = 22;
			m_collisionRect.height = 22;
			m_canPressFire = true;
			m_canPressUp = true;
			m_canPressDown = true;
			m_canPressLeft = true;
			m_canPressRight = true;						
			m_hitPoints = HIT_POINTS;
			m_maxHitPoints = HIT_POINTS;
			m_invincibilitySource = null;
			m_oldVel = new Vector3D();
			m_prevVel = new Vector3D();
			gotoAndPlay("walk_down");
			m_currAnim = ANIM_WALK_DOWN;
			m_glow.m_color = 0xffffffff;
			if (!m_onWrappedListenerList)
			{
				m_onWrappedListenerList = new Vector.<Function>;
			}// end if
			//m_frontChildGameObjectList.push(m_glow);
			Game.g.m_gameObjectList.push(m_glow);
			m_projectedTX = m_tx;
			m_projectedTY = m_ty;
			m_safetyProjectedTX = m_tx;
			m_safetyProjectedTY = m_ty;			
			m_drawOrder = 3;
			m_weaponHolder = null;
			m_powerUpRingEffect = null;
			m_backChildGameObjectList = new Vector.<GameObject>;
			m_invisible = false;
			m_reactingToHit = false;
			m_score = new HiddenInt();
			m_doingTemporaryStop = false;
			m_inputEnabled = false;
			m_lastCollectedObject = null;			
		}// end public function EntityPlayer(tx_:uint, ty_:uint)
				
		public static function free():void		
		{
			m_onWrappedListenerList = null
		}// end public static function free():void	
		
		public function doTemporaryStop():void
		{
			m_doingTemporaryStop = true;
			m_temporaryStopStartTime = Game.g.timer;
		}// end public function doTemporaryStop():void

		public override function doInput():void
		{
			
			if (!m_inputEnabled)
			{
				return;
			}// end if
			
			var keyPressed:Boolean = false;
			
			for (var currKeyIndex:uint = 0; currKeyIndex < 2; currKeyIndex++)
			{
				
				if (m_upKey[currKeyIndex] == -1)
				{
					continue;
				}// end if
				
				if (keyPressed)
				{
					break;
				}// end if
				
				if (Key.isDown(m_upKey[currKeyIndex]) || Key.isDown(m_downKey[currKeyIndex]) || Key.isDown(m_leftKey[currKeyIndex]) || Key.isDown(m_rightKey[currKeyIndex]) || Key.isDown(m_fireKey[currKeyIndex]))
				{
					keyPressed = true;
				}// end if
			
				if (Key.isDown(m_upKey[currKeyIndex]) && m_canPressUp)
				{
					m_qDir = DIR_UP;
					m_canPressUp = false;
				}
				else if (!Key.isDown(m_upKey[currKeyIndex]) && !m_canPressUp)
				{
					m_canPressUp = true;
					
					if (Key.isDown(m_downKey[currKeyIndex]) && !m_canPressDown)
					{
						m_canPressDown = true;
					}
					
					if (Key.isDown(m_leftKey[currKeyIndex]) && !m_canPressLeft)
					{
						m_canPressLeft = true;
					}
					
					if (Key.isDown(m_rightKey[currKeyIndex]) && !m_canPressRight)
					{
						m_canPressRight = true;
					}// end else if
					
				}// end else if
				
				if (Key.isDown(m_downKey[currKeyIndex]) && m_canPressDown)
				{	
					m_qDir = DIR_DOWN;
					m_canPressDown = false;
				}
				else if (!Key.isDown(m_downKey[currKeyIndex]) && !m_canPressDown)
				{
					m_canPressDown = true;
					
					if (Key.isDown(m_upKey[currKeyIndex]) && !m_canPressUp)
					{
						m_canPressUp = true;
					}
					
					if (Key.isDown(m_leftKey[currKeyIndex]) && !m_canPressLeft)
					{
						m_canPressLeft = true;
					}
					
					if (Key.isDown(m_rightKey[currKeyIndex]) && !m_canPressRight)
					{
						m_canPressRight = true;
					}// end else if				
					
				}// end else if
				
				if (Key.isDown(m_leftKey[currKeyIndex]) && m_canPressLeft)
				{
					m_qDir = DIR_LEFT;
					m_canPressLeft = false;
				}
				else if (!Key.isDown(m_leftKey[currKeyIndex]) && !m_canPressLeft)
				{
					m_canPressLeft = true;
					
					if (Key.isDown(m_downKey[currKeyIndex]) && !m_canPressDown)
					{
						m_canPressDown = true;
					}
					
					if (Key.isDown(m_upKey[currKeyIndex]) && !m_canPressUp)
					{
						m_canPressUp = true;
					}
					
					if (Key.isDown(m_rightKey[currKeyIndex]) && !m_canPressRight)
					{
						m_canPressRight = true;
					}// end else if				
					
				}// end else if
				
				if (Key.isDown(m_rightKey[currKeyIndex]) && m_canPressRight)
				{
					m_qDir = DIR_RIGHT;
					m_canPressRight = false;
				}
				else if (!Key.isDown(m_rightKey[currKeyIndex]) && !m_canPressRight)
				{
					m_canPressRight = true;
					
					if (Key.isDown(m_downKey[currKeyIndex]) && !m_canPressDown)
					{
						m_canPressDown = true;
					}
					
					if (Key.isDown(m_leftKey[currKeyIndex]) && !m_canPressLeft)
					{
						m_canPressLeft = true;
					}
					
					if (Key.isDown(m_upKey[currKeyIndex]) && !m_canPressUp)
					{
						m_canPressUp = true;
					}// end else if				
					
				}// end else if
				
				if (Key.isDown(m_fireKey[currKeyIndex]) && m_canPressFire)
				{
					m_fire = true;
					m_canPressFire = false;
				}
				else if (!Key.isDown(m_fireKey[currKeyIndex]) && !m_canPressFire)
				{
					m_stopFire = true;
					m_canPressFire = true;
				}// end else if
			
			}// end for	
				
		}// end public override function doInput():void		
		
		public override function onWrapped():void
		{

			for (var currListener:uint = 0; currListener < m_onWrappedListenerList.length; currListener++)
			{
				m_onWrappedListenerList[currListener]();
			}// end for			
			
		}// end public override function onWrapped():void			
		
		public function updateAnim():void
		{
		
			if ((m_prevVel.x != m_vel.x) || (m_prevVel.y != m_vel.y) || m_forceUpdateAnim)
			{
				
				m_prevVel.x = m_vel.x;
				m_prevVel.y = m_vel.y;
				
				if (m_vel.x < 0)
				{
					m_forceUpdateAnim = false;
					gotoAndPlay("walk_side" + m_animModifier);
					m_currAnim = ANIM_WALK_LEFT;
					flipX(false);					
				}
				else if (m_vel.x > 0)
				{
					m_forceUpdateAnim = false;
					gotoAndPlay("walk_side" + m_animModifier);
					m_currAnim = ANIM_WALK_RIGHT;
					flipX(true);					
				}
				else if (m_vel.y < 0)
				{
					m_forceUpdateAnim = false;
					gotoAndPlay("walk_up" + m_animModifier);
					m_currAnim = ANIM_WALK_UP;
				}
				else if (m_vel.y > 0)
				{
					m_forceUpdateAnim = false;
					gotoAndPlay("walk_down" + m_animModifier);
					m_currAnim = ANIM_WALK_DOWN;
				}// end else if
				
			}// end if
			
			
			if (!m_vel.x && !m_vel.y)// && (m_prevQDir != m_lastDir))
			{
				
				var isTeleporting:Boolean = (m_state == STATE_TELEPORTING_OUT) || (m_state == STATE_TELEPORTING_IN);
				
				switch(m_lastDir)
				{
					
					case DIR_LEFT:
					{
						if (((m_currAnim != ANIM_WALK_LEFT) || m_forceUpdateAnim) && (!(isTeleporting && !m_tileMap.canGoLeft(m_cTX, m_cTY))))
						{
							m_forceUpdateAnim = false;
							gotoAndPlay("walk_side" + m_animModifier);
							m_currAnim = ANIM_WALK_LEFT;
							flipX(false);			
						}// end if
						break;
					}// end case
					
					case DIR_RIGHT:
					{
						if (((m_currAnim != ANIM_WALK_RIGHT) || m_forceUpdateAnim) && (!(isTeleporting && !m_tileMap.canGoRight(m_cTX, m_cTY))))
						{			
							m_forceUpdateAnim = false;
							gotoAndPlay("walk_side" + m_animModifier);
							m_currAnim = ANIM_WALK_RIGHT;
							flipX(true);							
						}// end if
						break;
					}// end case	
					
					case DIR_UP:
					{
						if (((m_currAnim != ANIM_WALK_UP) || m_forceUpdateAnim) && (!(isTeleporting && !m_tileMap.canGoUp(m_cTX, m_cTY))))
						{		
							m_forceUpdateAnim = false;
							gotoAndPlay("walk_up" + m_animModifier);
							m_currAnim = ANIM_WALK_UP;
						}// end if
						break;
					}// end case	
					
					case DIR_DOWN:
					{
						if (((m_currAnim != ANIM_WALK_DOWN) || m_forceUpdateAnim) && (!(isTeleporting && !m_tileMap.canGoDown(m_cTX, m_cTY))))
						{		
							m_forceUpdateAnim = false;
							gotoAndPlay("walk_down" + m_animModifier);
							m_currAnim = ANIM_WALK_DOWN;
						}// end if
						break;
					}// end case	
					
					case DIR_NONE:
					{
						if (m_forceUpdateAnim)
						{		
							m_forceUpdateAnim = false;
							gotoAndPlay("walk_down" + m_animModifier);
							m_currAnim = ANIM_WALK_DOWN;
						}// end if
						break;
					}// end case						
					
				}// end switch
				
			}// end if
			
		}// end public function updateAnim():void			
		
		public function updateProjectedPos():void
		{
			
			var incX:int = 0;
			var incY:int = 0;
			
			m_projectedTX = m_cTX;
			m_projectedTY = m_cTY;	
			
			if (m_vel.x < 0)
			{
				incX = -1;
			}
			else if (m_vel.x > 0)
			{
				incX = 1;	
			}
			else if (m_vel.y < 0)
			{
				incY = -1;
			}
			else if (m_vel.y > 0)
			{
				incY = 1;	
			}// end else if	
			
			if (!incX && !incY)
			{
				return;
			}// end if
			
			
			
			var testX:int;
			var testY:int;
			
			for (var incPos:uint = 0; incPos < POS_PROJECTION_LENGTH; incPos++)
			{
				
				if (incX)
				{
					testX = getWrappedTileX(m_projectedTX + incX);
					
					if (Game.g.m_tileMap.m_tileMap[Game.g.m_tileMap.m_width * m_ty + testX] == PathMap.TILE_BLOCKED)
					{
						break;
					}// end if
					
					m_projectedTX = testX;
					
				}
				else
				{
					testY = getWrappedTileY(m_projectedTY + incY);
					
					if (Game.g.m_tileMap.m_tileMap[Game.g.m_tileMap.m_width * testY + m_tx] == PathMap.TILE_BLOCKED)
					{
						break;
					}// end if
					
					m_projectedTY = testY;
					
				}// end else if
				
			}// end for
			
			m_safetyProjectedTX = m_cTX;
			m_safetyProjectedTY = m_cTY;				
			
			if (!Game.g.m_safetyTileMap.m_active)
			{
				return;
			}// end if
			
			for (incPos = 0; incPos < POS_PROJECTION_LENGTH; incPos++)
			{
				
				if (incX)
				{
					testX = getWrappedTileX(m_safetyProjectedTX + incX);
					
					if (Game.g.m_safetyTileMap.m_tileMap[Game.g.m_safetyTileMap.m_width * m_ty + testX] == PathMap.TILE_BLOCKED)
					{
						break;
					}// end if
					
					m_safetyProjectedTX = testX;
					
				}
				else
				{
					testY = getWrappedTileY(m_safetyProjectedTY + incY);
					
					if (Game.g.m_safetyTileMap.m_tileMap[Game.g.m_safetyTileMap.m_width * testY + m_tx] == PathMap.TILE_BLOCKED)
					{
						break;
					}// end if
					
					m_safetyProjectedTY = testY;
					
				}// end else if
				
			}// end for			
			
		}// end public function updateProjectedPos():void				
		
		public function applyPowerUp(powerUp_:PowerUp)
		{
			if (m_weaponHolder)
			{
				if (m_weaponHolder is powerUp_.holder)
				{
					m_weaponHolder.upgrade();
				}
				else
				{
					m_weaponHolder.terminate();
					SoundManager.playSound("LevelUp");
					m_weaponHolder = new powerUp_.holder(this);
				}// end else
				
				m_powerUpRingEffect.setPowerlevel(m_weaponHolder.m_currUpgradeStage);
			}
			else
			{
				m_weaponHolder = new powerUp_.holder(this);
				m_powerUpRingEffect = new ringEffectType();
				Game.g.m_gameObjectList.push(m_powerUpRingEffect);
				SoundManager.playSound(powerUp_.soundID);
			}// end else
			onApplyPowerUpUpdateHUD();
		}// end public function applyPowerUp(powerUp_:PowerUp)
		
		public function onApplyPowerUpUpdateHUD():void
		{		
			
		}// end public function onApplyPowerUpUpdateHUD():void			
		
		public function get ringEffectType():Class
		{		
			return null;
		}// end public function get ringEffectType():Class	
			
		public override function onDeath(attacker_:Entity = null):void
		{
			
			super.onDeath(attacker_);
		
			SoundManager.playSound(deathSoundID);
			m_score.value = 0;
			Game.g.m_gameObjectList.push(new destroyedEffectType(m_cTX, m_cTY));
			Game.g.m_gameObjectList.push(new PlayerDeathExplosion(this));
			
		}// end public override function onDeath(attacker_:Entity = null):void				
		
		public function get deathSoundID():String
		{
			return null;
		}// end public function get deathSoundID():String			
		
		public function get destroyedEffectType():Class
		{
			return null;
		}// end public function get destroyedEffectType():Class			
		
		public override function doDamage(hitPoints_:uint, vel_:Vector3D = null, attacker_:Entity = null):void
		{
			
			if (!m_isDead && !m_triggeredDeath && !m_invisible && !m_reactingToHit)
			{			
			
				
				Game.g.m_gameObjectList.push(new PlayerHit(m_cTX, m_cTY));
				
				m_reactingToHit = true;
				
				
				if ((m_state == STATE_TELEPORTING_OUT) || (m_state == STATE_TELEPORTING_IN))
				{
					abortTeleport();
					if (m_state == STATE_TELEPORTING_OUT)
					{
						tryTerminateTeleporter();
					}// end if													
				}// end if					
				
				
				if (m_vel.x < 0)
				{
					gotoAndPlay("hit_side");
					flipX(false);					
				}
				else if (m_vel.x > 0)
				{
					gotoAndPlay("hit_side");
					flipX(true);					
				}
				else if (m_vel.y < 0)
				{
					gotoAndPlay("hit_up");
				}
				else if (m_vel.y > 0)
				{
					gotoAndPlay("hit_down");
				}// end else if

						
				
				if (!m_vel.x && !m_vel.y)
				{
								
					switch(m_lastDir)
					{
						
						case DIR_LEFT:
						{
							gotoAndPlay("hit_side");
							flipX(false);	
							break;
						}// end case
						
						case DIR_RIGHT:
						{
							gotoAndPlay("hit_side");
							flipX(true);							
							break;
						}// end case	
						
						case DIR_UP:
						{
							gotoAndPlay("hit_up");
							break;
						}// end case	
						
						case DIR_DOWN:
						case DIR_NONE:
						{
							gotoAndPlay("hit_down");
							break;
						}// end case	
						
					}// end switch
					
				}// end else if				
				
				
				m_invisible = true;
				m_flicker = true;
				m_invisibleStartTime = Game.g.timer;
						
				
				if (m_weaponHolder)
				{
					
					SoundManager.playSound("PlayerHitAndLosePowerUp");
					terminateWeapon();
					
				}
				else
				{
					
					onDeath(attacker_);
					m_triggeredDeath = true;				
					
				}// end else
				
			
			}// end if
			
		}// end public override function doDamage(hitPoints_:uint, vel_:Vector3D = null, attacker_:Entity = null):void 		
		
		public function terminateWeapon():void
		{		
			m_weaponHolder.terminate();
			m_weaponHolder = null;
			m_powerUpRingEffect.m_terminated = true;
			m_powerUpRingEffect = null;
			onHitUpdateHUD();				
		}// end public function terminateWeapon():void				
		
		public function onHitUpdateHUD():void
		{		
				
		}// end public function onHitUpdateHUD():void		
		
		public function	startVictorySequence():void
		{
			m_state = STATE_STOPPING_FOR_VICTORY;
			m_inputEnabled = false;
			stopAtNextTile();
		}// end public function	startVictorySequence():void
		
		public function hitDone():void
		{		
			stop();
			m_reactingToHit = false;
			m_forceUpdateAnim = true;
			m_invisible = true;
			m_invisibleStartTime = Game.g.timer;			
		}// end public function hitDone():void
		
		public override function kill(attacker_:Entity = null):void
		{
			if (!m_isDead && !m_triggeredDeath)
			{			
				
				m_triggeredDeath = true;	
				
				if (m_weaponHolder)
				{
				
					m_weaponHolder.terminate();
					m_weaponHolder = null;
					m_powerUpRingEffect.m_terminated = true;
					m_powerUpRingEffect = null;
					
				}// end if
				
				onDeath(attacker_);		
				
			}// end if
		}// end public override function kill(attacker_:Entity = null):void			
		
		public override function doPhysics():void
		{
								
			
			/*if (m_reactingToHit)
			{
				m_qDir = DIR_NONE;
			}// end if*/
			
			if (m_qDir != DIR_NONE)
			{
				m_prevQDir = m_lastDir;
				m_lastDir = m_qDir;
			}// end if
			
			if ((m_state == STATE_TELEPORTING_OUT) && ((m_vel.x || m_vel.y) || (m_teleporter && m_teleporter.m_terminated)))
			{
				abortTeleport();
				if (m_teleporter && !m_teleporter.m_terminated)
				{
					tryTerminateTeleporter();	
				}// end if
				/*teleportingDone();
				if (m_weaponHolder)
				{
					m_weaponHolder.onTeleportCancel();
					if (m_teleporter is EntityPlayerTeleporter)
					{
						m_teleporter.terminate();
						m_teleporter = null;
					}// end if					
				}// end if*/						
			}else if ((m_state == STATE_TELEPORTING_IN) && (m_vel.x || m_vel.y))
			{
				teleportingDone();
			}// end else if
			
			if (m_weaponHolder)
			{
			
				if (m_fire)
				{
					m_weaponHolder.tryFire();	
				}// end if
				
				if (m_stopFire)
				{
					m_weaponHolder.tryStopFire();
				}// end if	
				
				if (m_lastDir != m_prevQDir)
				{
					m_weaponHolder.onChangeDir();
				}// end if
				
			}
			else
			{
				m_fire = false;
			}// end elee
			
			var collidingObject:Entity = getCollidingObject(null, null, EntityCollectible);
			
			if (collidingObject)
			{
				
				if ((collidingObject is EntityCollectible) && (!(collidingObject as EntityCollectible).m_collected))
				{
					m_score.value = m_score.value + (collidingObject as EntityCollectible).m_score.value;
					(collidingObject as EntityCollectible).collect();	
					onCollectUpdateHUD();
					m_lastCollectedObject = collidingObject as EntityCollectible;
				}// end if
				
			}// end if
			
			updateProjectedPos();
			
			if (!m_reactingToHit)
			{			
				updateAnim();
			}// end if
			
			if (m_powerUpRingEffect)
			{
				m_powerUpRingEffect.x = x;
				m_powerUpRingEffect.y = y;
			}// end if
			
			if (m_invisible && (Game.g.timer - m_invisibleStartTime > INVISIBILITY_DURATION))
			{
				m_invisible = false;
				m_flicker = false;
			}// end if
			
			if (!m_reactingToHit && !m_doingTemporaryStop)
			{
				super.doPhysics();
			}// end if
			
			if (m_doingTemporaryStop && Game.g.timer - m_temporaryStopStartTime > TEMPORARY_STOP_DURATION)
			{
				m_doingTemporaryStop = false;
			}// end if
			
			updateAbsCollisionRect();
			
		}// end public override function doPhysics():void		
		
	
		public function tryTerminateTeleporter():void
		{
			if ((m_teleporter is EntityPlayerTeleporter) && (m_teleporter.powerLevel != -1))
			{
				m_teleporter.terminate();
				m_teleporter = null;
			}// end if							
		}// end public function tryTerminateTeleporter():void		
		
		public function abortTeleport():void
		{
			teleportingDone();
			if (m_weaponHolder)
			{
				m_weaponHolder.onTeleportCancel();			
			}// end if							
		}// end public function abortTeleport():void				
		
		public function onCollectUpdateHUD():void
		{
			
			
		}// end public function onCollectUpdateHUD():void				
		
		public override function onStoppedAtTile():void
		{
			
			if (m_state != STATE_STOPPING_FOR_VICTORY)
			{
							
				m_currAnim = ANIM_TELEPORTING;
				m_state = STATE_TELEPORTING_OUT;
				m_teleportOldDir = m_qDir;
				m_qDir = DIR_NONE;		
				m_lastDir = DIR_NONE;
				m_currTeleporterSpin = 0;				
				
				//trace("teleporting out at tx = " + m_teleporter.m_tx + ", ty = " + m_teleporter.m_ty);
				if (m_teleporter.m_spinTimes > 0)
				{
					gotoAndPlay("spin_in_teleporter" + m_animModifier);
				}
				else
				{
					
					gotoAndPlay("teleporting_out" + m_animModifier);
					SoundManager.playSound("EnterTeleporter");
					
					if (m_weaponHolder)
					{
						m_weaponHolder.onTeleport();
					}// end if							
					
				}// end else
			
			}
			else
			{
				
				gotoAndPlay("victory");
				m_state = STATE_VICTORY;
				m_qDir = DIR_NONE;		
				//Game.g.levelComplete();
			
			}// end else
			
		}// end public override function onStoppedAtTile():void				
		
		public function teleportFrom(teleporter_:EntityTeleporter, nextTile_:Boolean = false):void
		{
			
			m_oldVel.x = m_vel.x;
			m_oldVel.y = m_vel.y;			
			m_teleporter = teleporter_;
			m_state = STATE_STARTED_TELEPORTING;
			if (!nextTile_)
			{
				stopAtTile(teleporter_.m_tx, teleporter_.m_ty);
			}
			else
			{
				stopAtNextTile();
			}// end else
			
		}// end public function teleportFrom(teleporter_:EntityTeleporter, nextTile_:Boolean = false):void	
	
		
		public function get isTeleporting():Boolean
		{		
			return ((m_state == STATE_STARTED_TELEPORTING) || (m_state == STATE_TELEPORTING_OUT) || (m_state == STATE_TELEPORTING_IN));
		}// end if
		
		public function teleportIn():void
		{
			//trace("teleporting in at tx = " + m_teleporter.m_twin.m_tx + ", ty = " + m_teleporter.m_twin.m_ty);
			gotoAndPlay("teleporting_in" + m_animModifier); // ""
			SoundManager.playSound("ExitTeleporter");
			m_state = STATE_TELEPORTING_IN;
			
			if (!m_vel.x && !m_vel.y)
			{
				m_tx = m_teleporter.m_twin.m_tx;
				m_ty = m_teleporter.m_twin.m_ty;
			}// end if
			
			updatePosFromTile();
			
			if (m_weaponHolder)
			{
				m_weaponHolder.onTeleportDone();
			}// end if					
			
			if (m_teleporter is EntityPlayerTeleporter)
			{
				m_teleporter.terminate();
				m_teleporter = null;
			}// end if
			
		}// end public function teleportIn():void		
		
		public function spinDone():void
		{
			
			if (!m_teleporter)
			{
				return;
			}// end if
			
			m_currTeleporterSpin++;
			
			if ((m_teleporter.m_spinTimes == EntityTeleporter.INFINITE_SPIN_TIMES) || (m_currTeleporterSpin < m_teleporter.m_spinTimes))
			{
				gotoAndPlay("spin_in_teleporter" + m_animModifier);	
			}
			else
			{
				gotoAndPlay("teleporting_out" + m_animModifier);
				SoundManager.playSound("EnterTeleporter");
			}// end else
			
			if ((m_teleporter.m_spinTimes != EntityTeleporter.INFINITE_SPIN_TIMES) && m_weaponHolder)
			{
				m_weaponHolder.onTeleport();
			}// end if
			
			
		}// end public function spinDone():void				
		
		public function teleportingDone():void
		{

			gotoAndPlay("walk_down" + m_animModifier);
			m_state = STATE_NORMAL;
			
			if (!m_vel.x && !m_vel.y)
			{
				m_vel.x = m_oldVel.x;
				m_vel.y = m_oldVel.y;
				m_qDir = m_teleportOldDir;
				m_lastDir = m_qDir;
			}// end if			
			
			
		}// end public function teleportingDone():void				
		
	}// end public class EntityPlayer
	
}// end package