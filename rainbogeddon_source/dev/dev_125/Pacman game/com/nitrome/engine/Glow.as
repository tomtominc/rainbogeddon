package com.nitrome.engine
{
	import com.nitrome.engine.pathfinding.RadialPathMap;
	import flash.display.BitmapData;
	import com.nitrome.engine.pathfinding.PathMap;
	import com.nitrome.geom.PointUint;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.utils.getTimer;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import fl.motion.Color;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class Glow extends GameObject
	{
				
		public static const SIZE:uint = 3;
		
		public var m_tileMapBitmap:BitmapData;
		public var m_radialPathMap:RadialPathMap;
		public var m_color:uint;
		public var m_affectedTilePosList:Vector.<Vector.<PointUint>>;
		public var m_parent:Entity;
		public var m_ranFirstTime:Boolean;
		public var m_hasColorShift:Boolean;
		private var m_colorShiftSpeed:Number;
		private var m_colorFrom:uint;
		private var m_colorTo:uint;
		private var m_colorShiftPos:Number;
		private var m_shiftingColorRight:Boolean;
		private var m_colorShiftList:Vector.<uint>;
		private static var m_glowClip:MovieClip;	
		private static var m_radius:uint;		
		private static var m_glowBitmap:BitmapData;
		private static var m_glowBitmapBounds:Rectangle;	
		
		public function Glow(parent_:Entity)
		{
			super(parent_.m_cTX, parent_.m_cTY);		
			m_parent = parent_;
			
			m_drawBitmap = Game.g.m_corridorBitmap;
			m_tileMapBitmap = new BitmapData(Game.g.mapWidth, Game.g.mapHeight, true, 0x0);
			m_ranFirstTime = false;
			m_hasColorShift = false;
			m_colorShiftPos = 0;
			
			if (!m_glowBitmap)
			{
				
				m_glowClip = new (getDefinitionByName("BombCircle" + SIZE) as Class)();
				// glow width and height need to be an odd number
				m_radius = (m_glowClip.width - 1) / 2;				
				m_glowBitmap = new BitmapData(m_glowClip.width, m_glowClip.height);
				m_glowBitmapBounds = m_glowClip.getBounds(m_glowClip);
				
				var matrix:Matrix = new Matrix();
				
				matrix.identity();
				matrix.translate(-m_glowBitmapBounds.x, -m_glowBitmapBounds.y);
				m_glowBitmap.draw(m_glowClip, matrix);
				m_glowBitmap.threshold(m_glowBitmap, m_glowBitmap.rect, new Point(0,0), "==", 0xff000000, PathMap.TILE_SENTINEL, 0xffffffff);	
				
			}// end if			
			
		}// end public function Glow(tx_:uint, ty_:uint)
		
		public static function free():void		
		{
			m_glowClip = null;
			m_glowBitmap = null;
			m_glowBitmapBounds = null;
		}// end public static function free():void	
		
		public function ShiftColor(colorTo_:uint, speed_:Number):void
		{
			m_hasColorShift = true;
			m_colorShiftSpeed = speed_;
			m_colorTo = colorTo_;
			m_colorFrom = m_color;
			m_colorShiftPos = 0;
			m_shiftingColorRight = true;
		}// end public function ShiftColor(colorTo_:uint, speed_:Number ):void
		
		public function StopShiftColor():void
		{
			m_hasColorShift = false;
			m_color = m_colorFrom;
		}// end public function StopShiftColor():void		
	
		public override function doPhysics():void
		{
			
			if ((m_tx != m_parent.m_cTX) || (m_ty != m_parent.m_cTY) || !m_ranFirstTime || Game.g.m_tileMap.m_changed)
			{
				m_ranFirstTime = true;
				m_tx = m_parent.m_cTX;
				m_ty = m_parent.m_cTY;
				generate();
			}// end if
			
			if (m_hasColorShift)
			{
			
				m_color = Color.interpolateColor(m_colorFrom, m_colorTo, m_colorShiftPos);
				
				if (m_shiftingColorRight)
				{
					m_colorShiftPos += m_colorShiftSpeed;
					
					if (m_colorShiftPos >= 1)
					{
						m_colorShiftPos = 1.0;
						m_shiftingColorRight = false;
					}// end if
					
				}
				else
				{
					m_colorShiftPos -= m_colorShiftSpeed;
					
					if (m_colorShiftPos < 0)
					{
						m_colorShiftPos = 0;
						m_shiftingColorRight = true;
					}// end if
					
				}// end else
				
			}// end if
									
		}// end public override function doPhysics():void			
				
						
		
		public function generate():void
		{
			
			
			var tileBlockedColor:uint = PathMap.TILE_BLOCKED;// RadialPathMap.TILE_BLOCKED;
			var sentinelColor:uint    = PathMap.TILE_SENTINEL;// 0xff000000;			
			
			m_radialPathMap = new RadialPathMap(Game.g.mapWidth, Game.g.mapHeight, RadialPathMap.TILE_BLOCKED);
			m_tileMapBitmap.fillRect(m_tileMapBitmap.rect, tileBlockedColor);
			
			
			var destPoint:Point = new Point(m_tx + m_glowBitmapBounds.x, m_ty + m_glowBitmapBounds.y);
					
			m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);	
			
			// draw wrapped portions of the glow
			
			if ((m_tx - m_radius) < 0)
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x + m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);	
			}
			else if ((m_tx + m_radius) >= m_tileMapBitmap.width)
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x - m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);					
			}// end else
			
			if ((m_ty - m_radius) < 0)
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x;
				destPoint.y = m_ty + m_glowBitmapBounds.y + m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);				
			}
			else if ((m_ty + m_radius) >= m_tileMapBitmap.height)
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x;
				destPoint.y = m_ty + m_glowBitmapBounds.y - m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);				
			}// end else			
			
			if (((m_tx - m_radius) < 0) && ((m_ty - m_radius) < 0))
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x + m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y + m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);				
			}
			else if (((m_tx - m_radius) < 0) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x + m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y - m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);					
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty - m_radius) < 0))
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x - m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y + m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);					
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				destPoint.x = m_tx + m_glowBitmapBounds.x - m_tileMapBitmap.width;
				destPoint.y = m_ty + m_glowBitmapBounds.y - m_tileMapBitmap.height;
				m_tileMapBitmap.copyPixels(m_glowBitmap, m_glowBitmap.rect, destPoint);					
			}// end else if
			
			
			
			// merge the game tile map with the glow map
			// using both hard tiles and soft tiles
						
			var glowMapList:Vector.<uint> = m_tileMapBitmap.getVector(m_tileMapBitmap.rect);
			var tileMap:Vector.<uint> = Game.g.m_tileMap.m_tileMap;
			
			for (var currTile:uint = 0; currTile < m_radialPathMap.m_map.length; currTile++)
			{
				if ((glowMapList[currTile] == tileBlockedColor) || (tileMap[currTile] == tileBlockedColor))
				{
					m_radialPathMap.m_map[currTile] = RadialPathMap.TILE_BLOCKED;
				}// end if
			}// end for			

			
			
			m_affectedTilePosList = new Vector.<Vector.<PointUint>>;
			m_radialPathMap.castPath(m_tx, m_ty, m_radius);				
			
			var done:Boolean = false;	
				
			while(!done)
			{
			
				done = m_radialPathMap.continueCastingPath();
				m_affectedTilePosList.push(m_radialPathMap.getNewMarkedPathPosList());
				
				for (currTile = 0; currTile < m_affectedTilePosList[m_affectedTilePosList.length-1].length; currTile++)
				{
					
					var absPosX:Number = m_affectedTilePosList[m_affectedTilePosList.length - 1][currTile].m_x;
					var absPosY:Number = m_affectedTilePosList[m_affectedTilePosList.length - 1][currTile].m_y;					
					
					m_affectedTilePosList[m_affectedTilePosList.length - 1][currTile].m_x = absPosX;
					m_affectedTilePosList[m_affectedTilePosList.length - 1][currTile].m_y = absPosY;
					
				}// end for
			
			}// end while
			
		}// end public function generate():void			
		
		public function terminate():void
		{
			
			m_terminated = true;
									
		}// end public function terminate():void			
		
		public override function draw():void
		{
			
			if (!m_ranFirstTime)
			{
				return;
			}// end if
			
			var currAlphaFactor:Number = 1.0;
			var color:uint;
			var mapColor:uint;
			var mapColorAlpha:uint;
			var thisAlpha:uint;
			var glowRect:Rectangle = new Rectangle(0, 0, Game.TILE_WIDTH, Game.TILE_HEIGHT);
			
			var totalAlpha:uint;
			var totalR:uint;
			var totalG:uint;
			var totalB:uint;		
			
			var thisR:uint; 
			var thisG:uint; 
			var thisB:uint;			
			
			var mapColorR:uint;
			var mapColorG:uint;
			var mapColorB:uint;			
			
			thisR = (m_color & 0x00ff0000) >> 16; 
			thisG = (m_color & 0x0000ff00) >> 8; 
			thisB =  m_color & 0x000000ff;			
			
			for (var currLayer:uint = 0; currLayer < m_affectedTilePosList.length; currLayer++)
			{
				
				thisAlpha = (0xff * currAlphaFactor);
				color = m_color & 0x00ffffff;	
					
				for (var currTile:uint = 0; currTile < m_affectedTilePosList[currLayer].length; currTile++)
				{
			
					glowRect.x = m_affectedTilePosList[currLayer][currTile].m_x;
					glowRect.y = m_affectedTilePosList[currLayer][currTile].m_y;
					//mapColor = Game.g.m_glowMap[Game.g.mapWidth * glowRect.y + glowRect.x];
					mapColor = Game.g.m_glowBitmap.getPixel(glowRect.x, glowRect.y);
					mapColorAlpha = ((mapColor & 0xff000000) >> 24) & 0x000000ff;
					
					/*mapColorR = (mapColor & 0x00ff0000) >> 16;
					mapColorG = (mapColor & 0x0000ff00) >> 8;
					mapColorB =  mapColor & 0x000000ff;*/
					
					//totalAlpha = (255 - (((255 - thisAlpha) * (255 - mapColorAlpha)) >> 8));
					totalAlpha = Math.max(thisAlpha, mapColorAlpha);
					/*totalR = (255 - (((255 - thisR) * (255 - mapColorR)) >> 8));
					totalG = (255 - (((255 - thisG) * (255 - mapColorG)) >> 8));
					totalB = (255 - (((255 - thisB) * (255 - mapColorB)) >> 8));
				
					Game.g.m_glowMap[Game.g.mapWidth * glowRect.y + glowRect.x] = (totalAlpha << 24) + (totalR << 16) + (totalG << 8) + totalB;*/
					
					if (mapColor == 0)
					{
						Game.g.m_glowBitmap.setPixel32(glowRect.x, glowRect.y, (thisAlpha << 24) + (thisR << 16) + (thisG << 8) + thisB);
						Game.g.m_glowMap[Game.g.mapWidth * glowRect.y + glowRect.x] = (thisAlpha << 24) + (thisR << 16) + (thisG << 8) + thisB;
					}
					else
					{
						var interpColor:uint = Color.interpolateColor((thisAlpha << 24) + (thisR << 16) + (thisG << 8) + thisB, mapColor, 0.5);
						interpColor &= 0x00ffffff;
						interpColor += totalAlpha << 24;
						Game.g.m_glowBitmap.setPixel32(glowRect.x, glowRect.y, interpColor)
						Game.g.m_glowMap[Game.g.mapWidth * glowRect.y + glowRect.x] = interpColor;
					}// end else
					
				}// end for		
				
				currAlphaFactor *= 0.65;
				
			}// end for			
			
		}// end public override function draw():void
		
	}// end public class EntitySafetyBubble
	
}// end package