package com.nitrome.engine
{
		
	public class EntityStaticSafetyBubble extends EntitySafetyBubble
	{
				
		public function EntityStaticSafetyBubble(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_, 0);		

		}// end public function EntityStaticSafetyBubble(tx_:uint, ty_:uint)
		
		public override function onExpanded():void
		{
			
			
		}// end public override function onExpanded():void			
		
		public override function get bubbleParticleType():Class
		{
			return Player1BubbleParticle;
		}// end public override function get bubbleParticleType():Class		
		
	}// end public class EntityStaticSafetyBubble
	
}// end package