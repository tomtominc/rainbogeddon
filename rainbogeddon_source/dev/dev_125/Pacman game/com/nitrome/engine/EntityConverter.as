﻿package com.nitrome.engine {
	import com.nitrome.geom.Pixel;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.geom.Matrix;
	import flash.utils.getDefinitionByName;
	
	/**
	* Converts xml data into Entities and thier derivatives
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class EntityConverter {
		
		protected var g:Game;
		
		private var item:Class;   // DisplayObject;
		private var n:int;
		//private var gfx:DisplayObject;
		
		private var i:int, r:int, c:int;
		private var rows:int, cols:int;
		private var index:int;
		private var dir:int;
		private var obj:Object;
		public  var paths:Array;
		public  var params:Object;
		
		private static const UP:int = 1;
		private static const RIGHT:int = 2;
		private static const DOWN:int = 4;
		private static const LEFT:int = 8;
		public static const EMPTY:int = 0;
		
		public function EntityConverter(g:Game){
			this.g = g;
			paths = [];
			params = new Object();
		}
		
		/* Converts a string in one of the map layers into an Entity
		 *
		 * sizeTest is a parameter used for detecting elements that will span over multiple tiles - thus
		 * the map may need expanding in order to accomodate these elements */
		public function getEntity(str:String, depth_:uint, row_:uint, column_:uint):Class{
			
			
			var indexMatch:Array = str.match(/\d+/);
			var index:int;
			var prefix:String;
			var item:Class = null;
			//gfx = null;
			
			if(indexMatch){
				index = str.match(/\d+/)[0];
				prefix = str.substr(0, str.indexOf(String(index)));
			}
			
			if(str){
				try {
					
					
					var paramIndex:int = str.indexOf("_param_");
					if (paramIndex != -1)
					{
						str = str.substr(0, paramIndex);
					}// end if					
					
					var objClass:Class = getDefinitionByName("com.nitrome.engine." + str) as Class;
					item = objClass;
				} catch(e:Error){
					trace(e.message);
				}
			}
			
			if(prefix == ""){
				
			}
			
			return item;
		}
		
	}
	
}