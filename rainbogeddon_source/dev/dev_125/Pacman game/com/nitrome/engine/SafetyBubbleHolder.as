package com.nitrome.engine
{

	import com.nitrome.sound.SoundManager;
	
	public class SafetyBubbleHolder extends WeaponHolder
	{
		
		public static const FIRE_DELAY:int = 250;
		
		private var m_fireStartTime:int;
		private var m_maxBubbles:uint;
		private var m_bubbleList:Vector.<EntitySafetyBubble>;
		
		public function SafetyBubbleHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_fireStartTime = 0;
			m_bubbleList = new Vector.<EntitySafetyBubble>;
			m_maxBubbles = 1;
		}// end public function SafetyBubbleHolder()
				
	
		public override function tryFire():Boolean
		{
			m_parent.m_fire = false;
			
			refreshList();
			
			if (m_bubbleList.length < m_maxBubbles)
			{
				
				if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
				{
					m_fireStartTime = Game.g.timer;	
					SoundManager.playSound("Bubble");
					
					if (m_parent is EntityPlayer1)
					{
						m_bubbleList.push(new EntityPlayer1SafetyBubble(m_parent.m_tx, m_parent.m_ty));
					}
					else
					{
						m_bubbleList.push(new EntityPlayer2SafetyBubble(m_parent.m_tx, m_parent.m_ty));
					}// end else if
					
					Game.g.m_gameObjectList.push(m_bubbleList[m_bubbleList.length - 1]);
					return true;
				}// end if				
				
			}
			else if((m_bubbleList.length < m_maxBubbles+1) && m_bubbleList[0].m_canDisable)
			{
				
				m_bubbleList[0].fade();
				
				
				if ((Game.g.timer - m_fireStartTime) > FIRE_DELAY)
				{
					m_fireStartTime = Game.g.timer;	
					SoundManager.playSound("CreateBubble");
					
					if (m_parent is EntityPlayer1)
					{
						m_bubbleList.push(new EntityPlayer1SafetyBubble(m_parent.m_tx, m_parent.m_ty));
					}
					else
					{
						m_bubbleList.push(new EntityPlayer2SafetyBubble(m_parent.m_tx, m_parent.m_ty));
					}// end else if					
					
					Game.g.m_gameObjectList.push(m_bubbleList[m_bubbleList.length - 1]);
					return true;
				}// end if				
				
			}// end else		
			
			return false;
			
		}// end public override function tryFire():Boolean			
		
		public function refreshList():void
		{
			
			for (var currBubble:uint = 0; currBubble < m_bubbleList.length; currBubble++)
			{
				if (m_bubbleList[currBubble].m_terminated)
				{
					m_bubbleList.splice(currBubble, 1);
				}// end if
			}// end if
			
		}// end public function refreshList():void			
		
		public override function terminate():void
		{
			
			for (var currBubble:uint = 0; currBubble < m_bubbleList.length; currBubble++)
			{
				m_bubbleList[currBubble].fade();
			}// end if		
			
		}// end public override function terminate():void				
		
		public override function upgrade():void
		{
			
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				m_maxBubbles++;
			}// end if			
		}// end public override function upgrade():void			
		
	}// end public class SafetyBubbleHolder
	
}// end package