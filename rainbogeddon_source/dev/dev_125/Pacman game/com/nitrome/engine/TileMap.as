package com.nitrome.engine 
{
	
	import com.nitrome.geom.PointUint;
	import com.nitrome.engine.pathfinding.PathMap;
	
	public class TileMap
	{
		
		public  var m_width:uint;
		public  var m_height:uint;
		public  var m_tileMap:Vector.<uint>;
		public  var m_active:Boolean;
		public  var m_changed:Boolean;
		public  var m_changeNotifyTimes:uint;
		
		public function TileMap(width_:uint, height_:uint)
		{
			
			m_tileMap = new Vector.<uint>(width_*height_);
			m_width  = width_;
			m_height = height_;
			m_active = true;
			m_changed = false;
			m_changeNotifyTimes = 0;
									
		}// end public function PathMap(width_:uint, height_:uint)
		
		public function fillMap(gameObjectList_:Vector.<GameObject>)
		{
			
			m_tileMap = new Vector.<uint>(m_width * m_height);
			
			for (var currTile:uint = 0; currTile < m_tileMap.length; currTile++)
			{
				m_tileMap[currTile] = PathMap.TILE_SENTINEL;
			}// end for				
			
			for (var currGameObj:uint = 0; currGameObj < gameObjectList_.length; currGameObj++)
			{
				
				if (m_tileMap[gameObjectList_[currGameObj].m_ty * m_width + gameObjectList_[currGameObj].m_tx] == PathMap.TILE_SENTINEL)
				{
					m_tileMap[gameObjectList_[currGameObj].m_ty * m_width + gameObjectList_[currGameObj].m_tx] = gameObjectList_[currGameObj] is EntityBlock? PathMap.TILE_BLOCKED : PathMap.TILE_SENTINEL;		
				}// end if
				
			}// end for			
									
		}// end public function fillMap(gameObjectList_:Vector.<GameObject>)
			
		public function tick():void
		{
			
			if (m_changed && m_changeNotifyTimes++ > 4)
			{
				m_changed = false;
				m_changeNotifyTimes = 0;
			}// end if
			
		}// end public function tick():void			
		
		public function onMapChanged():void
		{
			
			m_changed = true;
			m_changeNotifyTimes = 0;
			
		}// end public function onMapChanged():void				
		
		public function canGoToTile(toX_:int, toY_:int, canWrap_:Boolean = true):Boolean
		{
		
			var finalToX:int = toX_;
			var finalToY:int = toY_;
			
			if(finalToX < 0)
			{
				if (canWrap_)
				{
					finalToX = m_width - 1;
				}
				else
				{
					return false;
				}// end else
			}
			else if(finalToX > m_width - 1)
			{
				if (canWrap_)
				{
					finalToX = 0;
				}
				else
				{
					return false;
				}// end else
			}// end else
			
			if(finalToY < 0)
			{
				if (canWrap_)
				{
					finalToY = m_height - 1;
				}
				else
				{
					return false;
				}// end else
			}
			else if(finalToY > m_height - 1)
			{
				if (canWrap_)
				{
					finalToY = 0;
				}
				else
				{
					return false;
				}// end else
			}// end else			
			
			if (m_tileMap[(finalToY) * m_width + finalToX] == PathMap.TILE_SENTINEL)
			{
				
				return true;
				
			}// end if
			
			return false;
			
		}// end public function canGoToTile(toX_:int, toY_:int):Boolean		
		
		public function canGoUp(fromX_:uint, fromY_:uint, canWrap_:Boolean = true):Boolean
		{
			
			var finalY:uint = fromY_ -1;
			var finalX:uint = fromX_;
			
			if (fromY_ == 0)
			{
				if (canWrap_)
				{
					finalY = m_height - 1;
				}
				else
				{
					return false;
				}// end else
			}// end if
			
			if (finalX > m_width-1)
			{
				finalX = 0;
			}// end if			
			
			if (finalX < 0)
			{
				finalX = m_width-1;
			}// end if			
			
			if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
			{
				
				return true;
				
			}// end if
			
			return false;
			
		}// end public function canGoUp(fromX_:uint, fromY_:uint):Boolean
		
		public function canGoDown(fromX_:uint, fromY_:uint, canWrap_:Boolean = true):Boolean
		{
			
			var finalY:uint = fromY_ +1;
			var finalX:uint = fromX_;
			
			if (fromY_ == m_height-1)
			{
				if (canWrap_)
				{
					finalY = 0;
				}
				else
				{
					return false;
				}// end else
			}// end if
			
			if (finalX > m_width-1)
			{
				finalX = 0;
			}// end if			
			
			if (finalX < 0)
			{
				finalX = m_width-1;
			}// end if				
			
			if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
			{
				
				return true;
				
			}// end if
			
			return false;
			
		}// end public function canGoUp(fromX_:uint, fromY_:uint):Boolean		
		
		public function canGoLeft(fromX_:uint, fromY_:uint, canWrap_:Boolean = true):Boolean
		{
			
			var finalX:uint = fromX_ -1;
			var finalY:uint = fromY_;
			
			if (fromX_ == 0)
			{
				if (canWrap_)
				{
					finalX = m_width - 1;
				}
				else
				{
					return false;
				}// end else
			}// end if
			
			if (finalY > m_height-1)
			{
				finalY = 0;
			}// end if			
			
			if (finalY < 0)
			{
				finalY = m_height-1;
			}// end if			
			
			if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
			{
				
				return true;
				
			}// end if
			
			return false;
			
		}// end public function canGoLeft(fromX_:uint, fromY_:uint):Boolean			
		
		public function canGoRight(fromX_:uint, fromY_:uint, canWrap_:Boolean = true):Boolean
		{
			
			var finalX:uint = fromX_ +1;
			var finalY:uint = fromY_;
			
			if (fromX_ == m_width-1)
			{
				if (canWrap_)
				{
					finalX = 0;
				}
				else
				{
					return false;
				}// end else
			}// end if
			
			if (finalY > m_height-1)
			{
				finalY = 0;
			}// end if			
			
			if (finalY < 0)
			{
				finalY = m_height-1;
			}// end if			
			
			if (m_tileMap[(finalY) * m_width + finalX] == PathMap.TILE_SENTINEL)
			{
				
				return true;
				
			}// end if
			
			return false;
			
		}// end public function canGoRight(fromX_:uint, fromY_:uint):Boolean			
		
	}// end public class PathMap

}// end package