/**
* Acts as a cue for the score submission process
* this button is disabled when there is no name entered
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import com.nitrome.ui.button.BasicButton;
	import flash.events.MouseEvent;

	public class SubmitButton extends BasicButton{
	private var pressed:Boolean=false;
	private var disabled:Boolean=true;
	private var scoreSubmitPanel:ScoreSubmitPanel;
		
		public function SubmitButton(){
			scoreSubmitPanel = parent as ScoreSubmitPanel;
		}
		/* Enable use of this button */
		public function enable():void{
			disabled=false;
			useHandCursor=true;
			buttonMode=true;
			updateGraphic();
		}
		/* Disable use of this button */
		public function disable():void{
			disabled=true;
			useHandCursor=false;
			buttonMode=false;
			updateGraphic();
		}
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			if(disabled){
				//do nothing
			}else{
				if(!pressed){
					scoreSubmitPanel.submitScore();
					pressed = true;
				}
			}
		}
	}
	
}
