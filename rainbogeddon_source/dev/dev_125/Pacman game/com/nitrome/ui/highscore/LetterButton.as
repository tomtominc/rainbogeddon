﻿/**
* Adds a letter to name_text in the ScoreSubmitPanel
* Reads the letter it represents from its instance name to make deployment quicker
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import com.nitrome.ui.button.BasicButton;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.display.MovieClip;

	public class LetterButton extends BasicButton{
		private var scoreSubmitPanel:ScoreSubmitPanel;
		//private var letter:TextField;
		private var letterText:String;
		
		public function LetterButton(){
			scoreSubmitPanel = parent as ScoreSubmitPanel;
			letterText = name;
			// add event listeners
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void{
			//letter.text = letterText.toUpperCase();
			//(parent.getChildByName(name + "_under") as MovieClip).text = letterText.toUpperCase();
            e.target.removeEventListener(Event.ADDED_TO_STAGE , init);
        }
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			// Add a letter to name_text
			scoreSubmitPanel.addLetter(letterText);
		}
		
		protected override function onMouseOver(e:MouseEvent):void
		{
		
			super.onMouseOver(e);
			letter.text = letterText.toUpperCase();
		
		}		
		
	}
	
}
