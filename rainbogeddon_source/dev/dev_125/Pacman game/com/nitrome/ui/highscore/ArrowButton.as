/**
* Advances through the highscore board ranks
*
* @author Aaron Steed, nitrome.com
* @version 2.1
*/

package com.nitrome.ui.highscore {
	import com.nitrome.ui.button.BasicButton;
	import flash.events.MouseEvent;

	public class ArrowButton extends BasicButton{
		var highScoreBoard:HighScoreBoard;
		
		public function ArrowButton(){
			highScoreBoard = parent as HighScoreBoard;
			visible = false;
		}
		/* Make button invisible */
		public function hide():void{
			visible = false;
		}
		/* Make button visible */
		public function show():void{
			visible = true;
		}
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			// Move up and down the ranks depending upon the instance name of this button
			if(name=="prevArrow"){
				highScoreBoard.shiftScoresPrev();
			}else if(name=="nextArrow"){
				highScoreBoard.shiftScoresNext();
			}else{
			}
		}
	}
	
}
