package com.nitrome.ui
{

	import editor.TextBox;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.StaticText;
	import flash.text.TextField;
		
	public class TwoPlayerResultPanel extends MovieClip
	{
				

		public function TwoPlayerResultPanel()
		{
			
			var player1Score:uint = Game.g.m_player1.m_score.value;
			var player2Score:uint = Game.g.m_player2.m_score.value;
			
			NitromeGame.setValue("player_1_level_" + Game.selectedLevel.value + "_score", player1Score);
			NitromeGame.setValue("player_2_level_" + Game.selectedLevel.value + "_score", player2Score);
			
			var numPlayer1Wins:uint = 0;
			var numPlayer2Wins:uint = 0;
			
			for (var currLevel:uint = 1; currLevel <= Game.selectedLevel.value; currLevel++)
			{
				
				if (!NitromeGame.getValue("player_1_level_" + currLevel + "_score"))
				{
					continue;
				}
				else
				{
					
					var player1LevelScore:uint = NitromeGame.getValue("player_1_level_" + currLevel + "_score");
					var player2LevelScore:uint = NitromeGame.getValue("player_2_level_" + currLevel + "_score");
					
					if (player1LevelScore > player2LevelScore)
					{
						numPlayer1Wins++;
					}
					else if (player1LevelScore < player2LevelScore)
					{
						numPlayer2Wins++;
					}
					else // player1LevelScore == player2LevelScore
					{
						numPlayer1Wins++;
						numPlayer2Wins++;
					}// end else
					
				}// end else
				
			}// end for		
			
			player1RoundsWon.text = new String(numPlayer1Wins);
			player2RoundsWon.text = new String(numPlayer2Wins);
			
			if (player1Score > player2Score)
			{
				player1Result.gotoAndStop("win");
				player2Result.gotoAndStop("lose");
			}
			else if (player1Score < player2Score)
			{
				player1Result.gotoAndStop("lose");
				player2Result.gotoAndStop("win");				
			}
			else // player1Score == player2Score
			{
				player1Result.gotoAndStop("draw");
				player2Result.gotoAndStop("draw");						
			}// end else
				
		}// end public function TwoPlayerResultPanel()
	

	}// end public class TwoPlayerResultPanel
	
}// end package