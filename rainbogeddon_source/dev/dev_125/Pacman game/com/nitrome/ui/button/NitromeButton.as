﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	/**
	* Send us to nitrome.com
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class NitromeButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void {
			super.onClick(e);
			navigateToURL(new URLRequest("http://www.nitrome.com/") , "_blank");
		}
		
	}
	
}