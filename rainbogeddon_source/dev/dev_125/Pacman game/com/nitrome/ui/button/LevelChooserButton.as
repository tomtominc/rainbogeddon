﻿package com.nitrome.ui.button {
	import com.nitrome.ui.RainbogeddonTextBox;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	
	/**
	* Changes NitromeGame.selected_level to the id of this button
	* and then goes to the game frame
	*
	* The TextField in the clip is called levelNum
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class LevelChooserButton extends BasicButton{
		
		private var id:int;
		private var active:Boolean;
		public var num:String;
		private var m_lockUnder:MovieClip;
		private var m_levelNumUnder:RainbogeddonTextBox;
		
		public static const upCol:int = 0xFFC300;
		public static const overCol:int = 0xC74E00;
		
		public function LevelChooserButton() 
		{
			
			id = name.match(/\d+/)[0];
			m_lockUnder = (parent.getChildByName("level_" + id + "_under") as MovieClip).getChildByName("lock") as MovieClip;
			m_levelNumUnder = (parent.getChildByName("level_" + id + "_under") as MovieClip).getChildByName("levelNum") as RainbogeddonTextBox;
			//is this level unlocked?
			if(NitromeGame.getLevelUnlocked(id)) {
				active = true;
				useHandCursor = true;
				m_lockUnder.visible = false;
			}else {
				buttonBase.visible = false;
				levelNum.visible = false;
				m_levelNumUnder.visible = false;
				active = false;
				useHandCursor = false;
			}
			updateGraphic();
		}
		
		override protected function updateGraphic():void{
			if(active){
				if(over) {
					levelNum.visible = true;
					buttonBase.visible = true;
				} else {
					levelNum.visible = false;
					buttonBase.visible = false;
				}
				var num:String = String(id);
				if(id < 10) num = "0" + num;
				levelNum.text = num;
				m_levelNumUnder.text = num;
				
			} /*else {
				levelNum.text = "??";
			}*/
		}
		
		override public function onClick(e:MouseEvent):void{
			if(active){
				Game.selectedLevel.value = id;
				Game.score.value = 0;
				NitromeGame.root.transition.goto("gamePlay");
			}
			updateGraphic();
		}
	}
	
}