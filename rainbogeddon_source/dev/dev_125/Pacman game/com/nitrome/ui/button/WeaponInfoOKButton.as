package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import com.nitrome.ui.Key;	
	import flash.ui.Keyboard;
	
	/**
	* Continues towards game after showing weapon info
	*
	* 
	*/
	public class WeaponInfoOKButton extends BasicButton
	{
		
		public function WeaponInfoOKButton()
		{
			super();
		
		}// end public function WeaponInfoOKButton()		
		
		
		public function execute():void
		{
			NitromeGame.root.game.weaponInfoEnd();
		}//	end public function execute():void	
		
		override public function onClick(e:MouseEvent):void
		{
			super.onClick(e);
			execute();
			
		}
		
	}
	
}