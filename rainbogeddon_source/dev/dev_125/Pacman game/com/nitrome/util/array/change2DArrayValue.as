﻿package com.nitrome.util.array {
	
	public function change2DArrayValue(oldValue:*, newValue:*, a:Array){
		var r:int, c:int;
		for(r = 0; r < a.length; r++){
			for(c = 0; c < a[r].length; c++){
				if(a[r][c] == oldValue) a[r][c] = newValue;
			}
		}
	}

}