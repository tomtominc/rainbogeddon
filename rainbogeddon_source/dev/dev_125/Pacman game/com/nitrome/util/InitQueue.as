package com.nitrome.util {
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * Staggers a series of anonymous functions over a period of time - used to overcome the 15 second timeout
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public class InitQueue extends Sprite{
		
		public var list:Array;// bug in Flash Player 10.0 with Vector.unshift() DO NOT OPTIMISE!
		public var listLength:int;
		public var updateCallback:Function;
		public var completionCallback:Function;
		
		public function InitQueue(completionCallback:Function = null, updateCallback:Function = null) {
			list = [];
			this.completionCallback = completionCallback;
			this.updateCallback = updateCallback;
		}
		
		public function add(f:Function):void{
			list.push(f);
		}
		
		public function start():void{
			listLength = list.length;
			trace("init queue length: " + listLength);
			trace("percent per function: " + (100 / listLength));
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		private function update(e:Event):void{
			if(list.length){
				var f:Function = list.shift();
				f();
				if(Boolean(updateCallback)) updateCallback();
			} else {
				list = null;
				removeEventListener(Event.ENTER_FRAME, update);
				if(Boolean(completionCallback)) completionCallback();
				completionCallback = null;
				updateCallback = null;
			}
		}
		
		public function getUpdatePercent():int{
			var ratio:Number = list.length / listLength
			return 100 - int(100 * ratio);
		}
		
	}

}