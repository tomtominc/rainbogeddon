package com.nitrome.util.misc {
	/**
	 * Returns the number of bits set to 1 in an integer - also known as the Hamming weight
	 *
	 * from: http://stackoverflow.com/questions/109023/best-algorithm-to-count-the-number-of-set-bits-in-a-32-bit-integer
	 * and: http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public function countBitsSet(n:int):int{
		n = n - ((n >> 1) & 0x55555555);
		n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
		return ((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
	}

}