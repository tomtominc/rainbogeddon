﻿/**
* A wrapper for handling the game's shared object
* as well as managing the highscores submissions
*
* @author Aaron Steed, nitrome.com
* @version 3.0
*/

package  {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Stage;
	import flash.display.StageScaleMode;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.navigateToURL;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.ui.ContextMenu;
	import flash.external.ExternalInterface;

	public class NitromeGame {
		
		public static var root:MovieClip;
		public static var stage:Stage
		public static var contextMenu:ContextMenu;
		public static var sharedObjectCache:Object = {};
		
		public static var levelsUnlocked:Array;
		public static var levelsCompleted:Array;
		public static var gameId:String;
		public static var saveId:int;
		public static var totalLevels:int;
		
		// highscore encryption vars
		private static var ar_1:Array = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "/", ":", ".", "_", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-"];
		private static var ar_2:Array = ["_", "7", "c", "2", "l", "r", "a", "h", "i", ".", "g", "m", "v", "1", "b", "q", "3", "z", "w", "o", "u", "t", "s", "0", "d", "f", "8", "n", "5", "k", ":", "j", "p", "/", "4", "6", "e", "9", "y", "x", "-"];
		private static var arKey:String = "ctdngevfaqki8_lb:psoj90ux127hm/4w5y3rz.6-";
		private static var adj:Number = 1.75;
		private static var ff:Array = [];
		public static var timeBased:Boolean = false;
		
		public static var localTesting:Boolean = false;
		public static var rescaleMode:String = StageScaleMode.NO_BORDER;
		
		private static var redirectRequested:Boolean = false;
		
		public static const NITROME_URL:Array = [
			"http://www.nitrome.com/",
			"http://cdn.nitrome.com/",
			"http://nitrome.com/"
		];
		public static const SUBMIT_URL:String="http://www.nitrome.com/php/submit_score.php";
		public static const RETRIEVE_URL:String = "http://www.nitrome.com/php/retrieve_scores.php";
		
		
		/* Must be called early on the game's timeline to allow access to methods and utilities
		 *
		 * If you want to initialise a load of things like sound engines, renderers and keys then
		 * I would advise adding something like a static method to your controller class that does all this
		 * after NitromeGame has been initialised
		 */
		public static function init(_root:MovieClip, _gameId:String = "", _totalLevels:int = 0):void {
			
			root = _root;
			stage = _root.stage;
			totalLevels = _totalLevels;
			gameId = _gameId.toLowerCase();
			saveId = 1;
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.addEventListener(Event.RESIZE, deactivateNoScaleMode);
			
			// set context menu
			// attach a listener to ContextMenuEvent.MENU_SELECT to detect the user right clicking
			// (the context menu can be used to jump the mouse position)
			//if(stage.showDefaultContextMenu){
				contextMenu = new ContextMenu();
				contextMenu.hideBuiltInItems();
				root.contextMenu = contextMenu;
			//}
			
			if(totalLevels){
				cacheLevelsUnlocked();
				cacheLevelsCompleted();
			}
		}
		
		/* This does some url checking */
		public static function loadGameData():void{
			if(!verify()){
				for(var i:int = 0; i < root.numChildren; i++){
					if(root.getChildAt(i) is NitromeGameMsg) return;
				}
				var msg:NitromeGameMsg = new NitromeGameMsg();
				// resize to fit
				msg.background.width = stage.stageWidth;
				msg.background.height = stage.stageHeight;
				msg.info.x = stage.stageWidth * 0.5;
				msg.info.y = stage.stageHeight * 0.5;
				root.addChild(msg);
				SoundMixer.soundTransform = new SoundTransform(0);
				msg.addEventListener(Event.ENTER_FRAME, function(e:Event):void{
					NitromeGame.stage.addChild(e.target as DisplayObject);
					NitromeGame.stopClips(NitromeGame.stage);
				});
				if(!redirectRequested){
					navigateToURL(new URLRequest("http://www.nitrome.com/games/" + gameId), "_blank");
					redirectRequested = true;
				}
			}
		}
		
		/* Performs a recursive dive into MovieClips and stops all animations */
		public static function stopClips(mc:DisplayObjectContainer):void{
			for(var i:int = 0; i < mc.numChildren; i++){
				if (mc.getChildAt(i) is MovieClip){
					(mc.getChildAt(i) as MovieClip).stop();
				}
				if(mc.getChildAt(i) is DisplayObjectContainer){
					stopClips(mc.getChildAt(i) as DisplayObjectContainer);
				}
			}
		}
		
		/* No-Scale-Mode offers some cpu speed, but if the swf is scaled by the client, no-scale needs to be turned off
		 *
		 * The default rescaling mode is NO_BORDER, which is best for release but not for debugging. Change rescaleMode to another
		 * stage scale mode on init for debugging */
		private static function deactivateNoScaleMode(e:Event):void{
			trace("No-Scale-Mode deactivated, game will run slower");
			stage.removeEventListener(Event.RESIZE, deactivateNoScaleMode);
			stage.scaleMode = rescaleMode;
			loadGameData();
		}
		
		public static function setSaveId(n:int):void{
			saveId = n;
		}
		
		/* Are we at a given url? (Used for game-bricking)
		 * accepts arrays of urls as its parameters, these are already present as constants above
		 */
		public static function verify(... urls):Boolean{
			
			if(urls.length == 0) urls.push(NITROME_URL);
			var url:String = root.loaderInfo.url;
			var i:int, j:int;
			
			// if the protocol is file:// then we're running locally, verify as true
			if(localTesting && url.substr(0, 7) == "file://") return true;
			
			// availability of the ExternalInterface implies the game is running
			// in the IDE or a browser
			if(ExternalInterface.available){
				// both urls must pass
				var loc:String = String(ExternalInterface.call("window.location.href.toString"));
				var pass:int = 0;
				
				check1: for(i = 0; i < urls.length; i++){
					for(j = 0; j < urls[i].length; j++){
						if(compareURLTo(urls[i][j], url)){
							pass++;
							break check1;
						}
					}
				}
				check2: for(i = 0; i < urls.length; i++){
					for(j = 0; j < urls[i].length; j++){
						if(compareURLTo(urls[i][j], loc)){
							pass++;
							break check2;
						}
					}
				}
				
				return pass == 2;
			} else {
				// it's possible that ExternalInterface is disabled
				
				for(i = 0; i < urls.length; i++){
					for(j = 0; j < urls[i].length; j++){
						if(compareURLTo(urls[i][j], url)){
							return true;
						}
					}
				}
			}
			return false;
		}
		/* This function will take a url pattern and return true if our url matches it.
			You can pass in wildcards (*) to match any subdomain, for example:
			["http://nitrome.com", "http://*.nitrome.com"]
		*/
		private static function compareURLTo(pattern:String, url:String):Boolean {
			var protocol:String = "http://";
			if(url.substr(0, protocol.length) != protocol) return false;
			
			// remove http protocol from url and pattern
			var workingURL:String = url.substr(protocol.length);
			pattern = pattern.substr(protocol.length);
			// and any possible extra slashes
			while(workingURL.charAt(0) == "/") workingURL = workingURL.substr(1);
			while(pattern.charAt(0) == "/") pattern = pattern.substr(1);
			
			// I'm splitting the url into sections separated by /. We check each one aggressively.
			// This lets us wildcard subdomains while at the same time avoiding sneaky things like:
				// http://www.nitrome.com.someone-else.com/
				// http://www.someone-else.com/something.www.nitrome.com/
			
			var urlParts:Array/*String*/ = workingURL.split("/");
			var patternParts:Array/*String*/ = pattern.split("/");
			
			for(var n:Number = 0; n < patternParts.length; n ++) {
				if(patternParts[n].length < 1) continue;
				
				var starPosition:Number = patternParts[n].indexOf("*");
				if(starPosition != -1) {
					// there's a * in the pattern so check for anything in that gap
					var beforeStar:String = patternParts[n].substr(0, starPosition);
					var afterStar:String = patternParts[n].substr(starPosition + 1);
					if(urlParts[n].substr(0, beforeStar.length) != beforeStar) return false;
					if(urlParts[n].substr(-afterStar.length) != afterStar) return false;
				} else {
					// there's no * so just check they match
					if(patternParts[n] != urlParts[n]) return false;
				}
			}
			
			// if we got here, then the url matches
			return true;
		}
		
		/* Returns the SharedObject associated with the saveId
		 *
		 * Some versions of the Flash IDE crash if you try to maintain multiple SharedObjects without
		 * calling close() between them:
		 * http://www.flashdaweb.com/2008/01/actionscript-3-sharedobject-crashing-flash-cs-ide/
		 *
		 * Hence I'm calling so.close() everywhere, now you know why
		 *
		 * Also: Flash Player 10.0 crashes if the SharedObject is accessed twice in a row with the same String
		 * Thus we have to cache the SharedObject data retrieved */
		public static function getSharedObject(saveId:int = 1):*{
			loadGameData()
			var name:String = "so" + gameId + saveId;
			if(sharedObjectCache.hasOwnProperty(name)){
				return sharedObjectCache[name];
			} else {
				sharedObjectCache[name] = SharedObject.getLocal(name);
			}
			return sharedObjectCache[name];
		}
		
		/* Unlock a level in the saved game object */
		public static function setLevelUnlocked(n:int, saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			levelsUnlocked[n - 1] = 1;
			so.data.levelsUnlocked = levelsUnlocked;
			so.flush();
			so.close();
		}
		
		/* Query level unlocked status, requires getLevelsUnlocked be called beforehand to cache the results */
		public static function getLevelUnlocked(n:int, saveId:int = 1):Boolean{
			return Boolean(levelsUnlocked[n - 1]);
		}
		
		/* Called on init to make sure we don't have to call SharedObject for info all the time */
		public static function cacheLevelsUnlocked(saveId:int = 1):Array{
			var so:SharedObject = getSharedObject(saveId);
			if(so.data.levelsUnlocked){
				levelsUnlocked = so.data.levelsUnlocked;
			} else {
				levelsUnlocked = [1];
			}
			so.close();
			return levelsUnlocked;
		}
		
		/* Called on init to make sure we don't have to call SharedObject for info all the time */
		public static function cacheLevelsCompleted(saveId:int = 1):Array{
			var so:SharedObject = getSharedObject(saveId);
			if(so.data.levelsCompleted){
				levelsCompleted = so.data.levelsCompleted;
			} else {
				levelsCompleted = [];
			}
			so.close();
			return levelsCompleted;
		}
		
		/* Find the last unlocked level number */
		public static function getLastUnlocked(saveId:int = 1):int{
			var best:int = 0;
			for(var key:String in levelsUnlocked){
				if(int(key) > best) best = int(key);
			}
			return best + 1;
		}
		
		/* Debug tool - locks all levels */
		public static function lockAllLevels(saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			delete(so.data.levelsUnlocked);
			levelsUnlocked = [1];
			levelsCompleted = [];
			so.flush();
			so.close();
		}
		
		/* Debug tool - unlocks all levels */
		public static function unlockAllLevels(saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			levelsUnlocked = new Array(totalLevels);
			levelsCompleted = new Array(totalLevels);
			for(var i:int = 0; i < levelsUnlocked.length; i++){
				levelsUnlocked[i] = 1;
				levelsCompleted[i] = 1;
			}
			so.data.levelsUnlocked = levelsUnlocked;
			so.data.levelsCompleted = levelsCompleted;
			so.flush();
			so.close();
		}
		
		
		/* Unlock a level in the saved game object */
		public static function setLevelCompleted(n:int, saveId:int = 1):void {
			var so:SharedObject = getSharedObject(saveId);
			levelsCompleted[n - 1] = 1;
			so.data.levelsCompleted = levelsCompleted;
			so.flush();
			so.close();
		}
		
		/* Query level completed status */
		public static function getLevelCompleted(n:int, _saveId:int = 1):Boolean {
			return Boolean(levelsCompleted[n - 1]);
		}
		
		/* Called on init to cache the levels completed */
		public static function getLevelsCompleted(saveId:int = 1):Array {
			var so:SharedObject = getSharedObject(saveId);
			if(so.data.levelsCompleted){
				levelsCompleted = so.data.levelsCompleted;
			} else {
				levelsUnlocked = [];
			}
			so.close();
			return levelsUnlocked;
		}
		
		/* Set a score for a specific level */
		public static function setLevelScore(score:int, levelNumber:int, saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			if(!so.data.levelScores){
				so.data.levelScores = [];
			}
			so.data.levelScores[levelNumber] = score;
			so.flush();
			so.close();
		}
		
		/* Get the score for a specific level */
		public static function getLevelScore(levelNumber:int, saveId:int = 1):int{
			var so:SharedObject = getSharedObject(saveId);
			if(so.data.levelScores){
				return int(so.data.levelScores[levelNumber]);
			}
			return 0;
			so.close();
		}
		
		/* Write an arbitrary value or object to the SharedObject */
		public static function setValue(name:String, value:*, saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			so.data[name] = value;
			so.flush();
			so.close();
		}
		
		/* Retrieve an arbitrary value from the SharedObject */
		public static function getValue(name:String, saveId:int = 1):*{
			var so:SharedObject = getSharedObject(saveId);
			var item:* = so.data[name];
			so.close();
			return item;
		}
		
		/* Find a value in the SharedObject and delete it */
		public static function deleteValue(name:String, saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			delete(so.data[name]);
			so.flush();
			so.close();
		}
		
		/* Delete the SharedObject */
		public static function deleteSharedObject(saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			so.clear();
			so.flush();
			so.close();
		}
		
		/* Get the total of the saved level scores */
		public static function getTotalScore(saveId:int = 1):int{
			var so:SharedObject = getSharedObject(saveId);
			if(so.data.levelScores){
				var value:int;
				for(var key:String in so.data.levelScores){
					value += int(so.data.levelScores[key]);
				}
				so.close();
				return value;
			}
			so.close();
			return 0;
		}
		
		/* Get the last saved score */
		public static function getLastSavedScore(saveId:int = 1):int{
			var so:SharedObject = getSharedObject(saveId);
			var value:int = so.data.lastSavedScore;
			so.close();
			return value;
		}
		
		/* Set the last saved score */
		public static function setLastSavedScore(score:int, saveId:int = 1):void{
			var so:SharedObject = getSharedObject(saveId);
			so.data.lastSavedScore = score;
			so.flush();
			so.close();
		}
		
		/* Return an object containing data about the highscore table */
		public static function getHighScoreLine(dataString:String, pos:int):Object{
			var scores:Array=dataString.split("|");
			var scoreLine:String=scores[pos-1];
			if(scoreLine=="0" || scoreLine=="1" || scoreLine==null || scoreLine==""){
				return(null);
			}else{
				var scoreArray:Array=scoreLine.split("_");
				if(scoreArray[1]=="n" || scoreArray[2]=="n"){
					return(null);
				}else{
					var o:Object=new Object();
					o.username=scoreArray[2];
					o.score=scoreArray[1];
					o.rank=scoreArray[0];
					return(o);
				}
			}
		}
		
		/* Is the next button to be displayed on the highscores? */
		public static function displayNextButton(dataString:String):Boolean{
			var scores:Array=dataString.split("|");
			var s:String=scores[10];
			if(s=="1"){
				return true;
			}else if(s=="0"){
				return false;
			}
			return false;
		}
		
		/* Is the previous button to be displayed on the highscores? */
		public static function displayPreviousButton(dataString:String):Boolean{
			var scores:Array=dataString.split("|");
			var s:String=scores[11];
			if(s=="1"){
				return true;
			}else if(s=="0"){
				return false ;
			}
			return false;
		}
		
		/* Return score data and print a debug report */
		public static function getScoreData(score:int, username:String, _gameId:String = ""):String {
			if(_gameId == "") _gameId = gameId;
			var ss:String = String(score)+"_"+_gameId+"_"+(username.toLowerCase());
			trace("encrypting: "+ss);
			var eSs:String = encryptString(arKey, ss);
			trace("encrypted: "+eSs);
			//decrypt for fun
			trace("decrypted: "+decryptString(arKey, eSs));
			return (eSs);
		}
		
		// ================================================================================================
		// Encryption methods - these routines are performed in tandem on the server
		// ================================================================================================
		private static function encryptString(key:String, s:String):String {
			adj=1.75;
			ff = convertKey(key);
			var target:String = "";
			var factor2:Number = 0;
			for (var i:int = 0; i<s.length; i++) {
				var char1:String = s.substr(i, 1);
				var num1:int;
				for (var j:int= 0; j<ar_1.length; j++) {
					if (ar_1[j] == char1) {
						num1 = j;
						break;
					}
				}
				adj = applyFudgeFactor();
				var factor1:Number = factor2+adj;
				var num2:int = Math.round(factor1)+num1;
				num2 = checkRange(num2);
				factor2 = factor1+num2;
				var char2:String = ar_2[num2];
				target += char2;
			}
			return target;
		}
		private static function convertKey(key:String):Array {
			var ar:Array = new Array();
			ar.push(key.length);
			var tot:int = 0;
			for (var i:int = 0; i<key.length; i++) {
				var char:String = key.substr(i, 1);
				var num:int;
				for (var j:int = 0; j<ar_1.length; j++) {
					if (ar_1[j] == char) {
						num = j;
						break;
					}
				}
				ar.push(num);
				tot += num;
			}
			ar.push(tot);
			return ar;
		}
		private static function applyFudgeFactor():Number {
			var fudge:Number = Number(ff.shift());
			fudge = fudge+adj;
			ff.push(fudge);
			return fudge;
		}
		private static function checkRange(num:int):int {
			num = Math.round(num);
			var limit:int = ar_1.length;
			while (num>=limit) {
				num = num-limit;
			}
			while (num<0) {
				num = num+limit;
			}
			return (num);
		}
		private static function decryptString(key:String, s:String):String {
			adj=1.75;
			ff = convertKey(key);
			var target:String = "";
			var factor2:Number = 0;
			for (var i:int = 0; i<s.length; i++) {
				var char2:String = s.substr(i, 1);
				var num2:int;
				for (var j:int = 0; j<ar_2.length; j++) {
					if (ar_2[j] == char2) {
						num2 = j;
						break;
					}
				}
				adj = applyFudgeFactor();
				var factor1:Number = factor2+adj;
				var num1:int = num2-Math.round(factor1);
				num1 = checkRange(num1);
				factor2 = factor1+num2;
				var char1:String = ar_1[num1];
				target += char1;
			}
			return (target);
		}
		
		/* Music and Sound fx getter / setters
		 * These are heavy methods - I advise copying the getter values into dummy variables if you want
		 * something to reference quickly - or use the values in SoundManager
		 */
		
		public static function setMusic(value:Boolean):void {
			var so:SharedObject = getSharedObject();
			so.data.musicOn = value;
			so.flush();
			so.close();
		}
		public static function getMusic():Boolean {
			var so:SharedObject = getSharedObject();
			var value:Boolean;
			if(so.data.musicOn is Boolean)
				value = so.data.musicOn;
			else
				value = true;
			so.close();
			return value;
		}
		public static function setSfx(value:Boolean):void {
			var so:SharedObject = getSharedObject();
			so.data.sfxOn = value;
			so.flush();
			so.close();
		}
		public static function getSfx():Boolean {
			var so:SharedObject = getSharedObject();
			var value:Boolean;
			if(so.data.sfxOn is Boolean)
				value = so.data.sfxOn;
			else
				value = true;
			so.close();
			return value;
		}
		
		// Highscore buffer methods
		
		public static var highScoreBuffer:Array;
		public static var currentLevelLoading:int;
		
		public static function initHighScoreBuffer():void{
			highScoreBuffer = [];
			for(var i:int = 0; i < totalLevels; i++) {
				// fill the buffer with failed load data
				highScoreBuffer[i] = "0";
			}
			trace("Requesting " + totalLevels + " high score tables - fasten pantyhose...");
			loadHighScores(1);
		}
		
		/* Pull in the highscores for this game from the server */
		public static function loadHighScores(n:int):void {
			// initialise our communication objects
			var scoresLoader:URLLoader = new URLLoader();
			var scoresVars:URLVariables = new URLVariables();
			var scoresRequest:URLRequest = new URLRequest(NitromeGame.RETRIEVE_URL);
			scoresRequest.method = URLRequestMethod.POST;
			scoresRequest.data = scoresVars;
			scoresLoader.dataFormat = URLLoaderDataFormat.VARIABLES;
			scoresLoader.addEventListener(Event.COMPLETE, retrieveSuccessful);
			scoresLoader.addEventListener(IOErrorEvent.IO_ERROR, retrieveFailed);
			
			// load our variables into scoresVars
			scoresVars.minRank=String(1);

			scoresVars.gameName = gameId + n;
			
			currentLevelLoading = n;
			
			// is the score time based? send "1" for yes and "0" for no
			scoresVars.timeBased=NitromeGame.timeBased ? "1" : "0";
			
			// send request
			scoresLoader.load(scoresRequest);
		}
		
		public static function retrieveSuccessful(e:Event = null):void{
			var dataString:String = e.target.data.result;
			trace("Scores retrieved: (" + dataString + ") for level:"+currentLevelLoading);
			highScoreBuffer[currentLevelLoading - 1] = dataString;
			
			if(currentLevelLoading < totalLevels) loadHighScores(currentLevelLoading + 1);
			else trace("Request complete");
		}
		
		public static function retrieveFailed(e:IOErrorEvent = null):void{
			trace("Retrieve scores failed: " + e.text + " for level:"+currentLevelLoading);
		}
	}
	
}
