﻿package editor {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filters.ColorMatrixFilter;
	import flash.text.TextField;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Alert extends MovieClip {
		
		private static var rootMovieClip:MovieClip;
		
		public var txt:TextField;
		
		public static function setup(rootClip:DisplayObject) {
			rootMovieClip = rootClip as MovieClip;
		}
		
		
		public static function alert(str:String, happy:Boolean = false):void {
			trace("# " + str);
			
			var a:Alert = new Alert();
			a.txt.text = str;
			a.x = rootMovieClip.stage.stageWidth / 2;
			a.y = rootMovieClip.stage.stageHeight / 2;
			if(happy) {
				a.filters = [new ColorMatrixFilter([
					0, 1, 0, 0, 0,
					1, 0, 0, 0, 0,
					0, 0, 1, 0, 0,
					0, 0, 0, 1, 0
				])];
			}
			rootMovieClip.addChild(a);
			
			a.addEventListener(Event.ENTER_FRAME, a.enterFrame);
		}
		
		public function enterFrame(e:Event):void {
			x = rootMovieClip.stage.stageWidth / 2;
			y = rootMovieClip.stage.stageHeight / 2;
		}
		
	}
	
}