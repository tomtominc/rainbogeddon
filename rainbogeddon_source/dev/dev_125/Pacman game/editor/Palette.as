﻿package editor {
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class Palette extends MovieClip {
		
		public var maxWidth:Number = 528;
		public var maxHeight:Number = 118;
		
		public var sections:/*PaletteSection*/Array = [];
		
		public var scrollPosition:Number = 0;
		public var scrollSpeed:Number = 0;
		public var dragging:Boolean = false;
		public var lastMouseX:Number;
		public var targetScrollPosition:Number;
		public var targeting:Boolean = false;
		public var willScrollNotClick:Boolean = false;
		
		public var separatorLayer:Sprite;
		public var contentLayer:Sprite;
		public var contentMask:Sprite;
		
		public var scrollIndicator:PaletteScrollIndicator;
		public var scrollIndicatorVisibility:Number = 0;
		
		public var hoverType:Class;
		public var selectedType:Class;
		
		public var onChange:Function = null;
		
		public const SEPARATOR_WIDTH:Number = 19;
		public const SECTION_PADDING:Number = 5;
		
		public function setup():void {
			stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			addEventListener(Event.ENTER_FRAME, enterFrame);
			
			addChild(contentLayer = new Sprite);
			addChild(separatorLayer = new Sprite);
			
			if(!contentMask) contentMask = new Sprite;
			parent.addChild(contentMask);
			mask = contentMask;
			
			addChild(scrollIndicator = new PaletteScrollIndicator);
			scrollIndicator.y = maxHeight;
		}
		
		public function setSize(w:Number, h:Number):void {
			maxWidth = w;
			maxHeight = h;
			
			if(!contentMask) contentMask = new Sprite;
			contentMask.graphics.clear();
			contentMask.graphics.beginFill(0x000000);
			contentMask.graphics.drawRect(0, 0, maxWidth, maxHeight + 1);
			contentMask.graphics.endFill();
			contentMask.x = x;
			contentMask.y = y;
			
			for(var n:Number = 0; n < sections.length; n ++) {
				setSeparatorHeight(sections[n].separator);
			}
			
			if(scrollIndicator) scrollIndicator.y = maxHeight;
		}
		public function setSeparatorHeight(sep:PaletteSeparator):void {
			sep.height = maxHeight;
			sep.separatorTitle.scaleX = 116 / maxHeight;
			sep.separatorTitle.width = maxHeight;
		}
		
		public function addSection(t:String, m:MovieClip, layer:Layer):void {
			var separator:PaletteSeparator = new PaletteSeparator;
			separator.x = 0;
			separator.separatorTitle.text = t;
			separator.useHandCursor = true;
			separator.buttonMode = true;
			separator.tabEnabled = false;
			separator.separatorTitle.mouseEnabled = false;
			setSeparatorHeight(separator);
			separatorLayer.addChild(separator);
			contentLayer.addChild(m);
			
			var section:PaletteSection = new PaletteSection;
			section.palette = this;
			section.separator = separator;
			section.targetLayer = layer;
			section.contentClip = m;
			
			sections.push(section);
			
			arrange();
		}
		
		public function arrange():void {
			var contentWidth:Number = maxWidth - (sections.length * SEPARATOR_WIDTH);
			
			var visibleX:Number = -Math.round(scrollPosition);
			var totalWidth:Number = 0;
			for(var n:Number = 0; n < sections.length; n ++) {
				var s:PaletteSection = sections[n];
				if(visibleX < (n * SEPARATOR_WIDTH))
					s.separator.x = n * SEPARATOR_WIDTH;
				else if(visibleX > contentWidth + (n * SEPARATOR_WIDTH))
					s.separator.x = contentWidth + (n * SEPARATOR_WIDTH);
				else
					s.separator.x = visibleX;
				s.contentClip.x = visibleX + SEPARATOR_WIDTH + SECTION_PADDING;
				s.contentClip.y = Math.floor((maxHeight - s.contentClip.height) / 2);
				
				visibleX += SEPARATOR_WIDTH + Math.ceil(s.contentClip.width) + (SECTION_PADDING * 2);
				totalWidth += SEPARATOR_WIDTH + Math.ceil(s.contentClip.width) + (SECTION_PADDING * 2);
			}
			
			scrollIndicator.width = (maxWidth - 10) * maxWidth / totalWidth;
			scrollIndicator.x = 5 + ((maxWidth - 10) * scrollPosition / totalWidth);
		}
		
		public function mouseDown(e:MouseEvent):void {
			if(mouseX < 0 || mouseX >= maxWidth) return;
			if(mouseY < 0 || mouseY >= maxHeight) return;
			
			var targetX:Number = 0;
			for(var n:Number = 0; n < sections.length; n ++) {
				if(sections[n].separator.hitTestPoint(e.stageX, e.stageY)) {
					targetScrollPosition = targetX;
					targetScrollPosition = Math.min(targetX, maxScrollPosition());
					targeting = true;
					return;
				}
				targetX += Math.ceil(sections[n].contentClip.width) + (SECTION_PADDING * 2);
			}
			
			dragging = true;
			willScrollNotClick = false;
			lastMouseX = mouseX;
			scrollSpeed = 0;
		}
		public function mouseUp(e:MouseEvent):void {
			if(!dragging) return;
			dragging = false;
			
			if(!willScrollNotClick)
				for(var n:Number = 0; n < sections.length; n ++) {
					for(var i:Number = 0; i < sections[n].contentClip.numChildren; i ++) {
						var child:Sprite = sections[n].contentClip.getChildAt(i) as Sprite;
						if(!child) continue; // not a movieclip, disregard
						if(child.hitTestPoint(stage.mouseX, stage.mouseY)) {
							setSelected(child["constructor"]);
							setSelectedLayer(sections[n].targetLayer);
							return;
						}
					}
				}
			
			Mouse.show();
		}
		public function maxScrollPosition():Number {
			var maxX:Number = 0;
			for(var n:Number = 0; n < sections.length; n ++)
				maxX += SEPARATOR_WIDTH + sections[n].contentClip.width + (SECTION_PADDING * 2);
			return maxX - maxWidth;
		}
		public function enterFrame(e:Event):void {
			var n:Number;
			
			if(targeting) {
				scrollSpeed = (targetScrollPosition - scrollPosition) * 0.3;
				
				if(Math.abs(scrollPosition - targetScrollPosition) < 1) {
					scrollPosition = targetScrollPosition;
					scrollSpeed = 0;
					targeting = false;
				}
			} else if(dragging) {
				var deltaX:Number = mouseX - lastMouseX;
				if(willScrollNotClick || Math.abs(deltaX) >= 5) {
					willScrollNotClick = true;
					lastMouseX = mouseX;
					scrollSpeed = -deltaX;
					Mouse.hide();
					setHovering(null);
				}
			} else {
				scrollSpeed *= 0.9;
			}
			
			var maxX:Number = maxScrollPosition();
			
			scrollPosition += scrollSpeed;
			if(scrollPosition > maxX) {
				scrollPosition = maxX;
				if(scrollSpeed > 0) scrollSpeed = 0;
			}
			if(scrollPosition < 0) {
				scrollPosition = 0;
				if(scrollSpeed < 0) scrollSpeed = 0;
			}
			
			arrange();
			
			if(Math.abs(scrollSpeed) > 0.25 || (dragging && willScrollNotClick)) {
				if(scrollIndicatorVisibility < 2)
					scrollIndicatorVisibility += 0.25;
			} else {
				if(scrollIndicatorVisibility > 0)
					scrollIndicatorVisibility -= 0.25;
			}
			if(scrollIndicatorVisibility > 1)
				scrollIndicator.alpha = 1;
			else
				scrollIndicator.alpha = scrollIndicatorVisibility;
			
			var hoveringOver:Class = null;
			
			findHover:
			if((!dragging || !willScrollNotClick) && this.mouseX > 0 && this.mouseX < maxWidth) {
				for(n = 0; n < sections.length; n ++) {
					if(sections[n].separator.hitTestPoint(stage.mouseX, stage.mouseY)) break findHover;
				}
				for(n = 0; n < sections.length; n ++) {
					for(var i:Number = 0; i < sections[n].contentClip.numChildren; i ++) {
						var child:Sprite = sections[n].contentClip.getChildAt(i) as Sprite;
						if(!child) continue; // not a movieclip, disregard
						
						if(child.hitTestPoint(stage.mouseX, stage.mouseY)) {
							hoveringOver = child["constructor"];
							break findHover;
						}
					}
				}
			}
			
			if(hoveringOver != hoverType) setHovering(hoveringOver);
		}
		public function setHovering(newType:Class):void {
			for(var n:Number = 0; n < sections.length; n ++)
				sections[n].updateGraphics(newType, selectedType, hoverType);
			hoverType = newType;
		}
		public function setSelected(newType:Class):void {
			for(var n:Number = 0; n < sections.length; n ++)
				sections[n].updateGraphics(hoverType, newType, selectedType);
			selectedType = newType;
			
			if(onChange != null) onChange();
		}
		public function setSelectedLayer(newLayer:Layer):void {
			Controller.drawingLayer = newLayer;
		}
		
		public function enforceCorrectLayer():void {
			var possibleCorrectLayer:Layer = null;
			for(var n:Number = 0; n < sections.length; n ++) {
				if(sections[n].containsOneOf(selectedType)) {
					if(sections[n].targetLayer == Controller.drawingLayer)
						return;
					else if(!possibleCorrectLayer)
						possibleCorrectLayer = sections[n].targetLayer;
				}
			}
			Controller.drawingLayer = possibleCorrectLayer;
		}
		
	}
	
}
