﻿package editor {
	import fl.managers.FocusManager;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class TextBox extends MovieClip {
		
		public var textField:TextField;
		private var _text:String;
		private var _editing:Boolean = false;
		
		public static var typing:Boolean = false;
		public static var typingBox:TextBox = null;
		
		public var onChange:Function = null;
		
		public function TextBox() {
			stop();
			
			_text = "";
			
			this.useHandCursor = true;
			this.buttonMode = true;
			textField.mouseEnabled = false;
			this.tabEnabled = false;
			this.tabChildren = false;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		public function addedToStage(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN, click);
			this.addEventListener(MouseEvent.ROLL_OVER, rollover);
			this.addEventListener(MouseEvent.ROLL_OUT, rollout);
			textField.addEventListener(Event.CHANGE, change);
		}
		
		public function click(e:MouseEvent):void {
			var hit:Boolean = this.hitTestPoint(e.stageX, e.stageY);
			if(hit && !_editing) {
				this.useHandCursor = false;
				this.buttonMode = false;
				textField.mouseEnabled = true;
				
				gotoAndStop("editable");
				textField.type = TextFieldType.INPUT;
				textField.setSelection(0, textField.text.length);
				textField.selectable = true;
				_editing = true;
				typing = true;
				typingBox = this;
				
				try {
					new FocusManager(this).setFocus(textField);
				} catch(e:Error) {}
			} else if(!hit && _editing) {
				gotoAndStop("static");
				textField.type = TextFieldType.DYNAMIC;
				textField.setSelection(0, 0);
				textField.selectable = false;
				_editing = false;
				if(typingBox == this) {
					typing = false;
					typingBox = null;
				}
				
				this.useHandCursor = true;
				this.buttonMode = true;
				textField.mouseEnabled = false;
			}
		}
		
		public function rollover(e:MouseEvent):void {
			if(!_editing) gotoAndStop("over");
		}
		public function rollout(e:MouseEvent):void {
			if(!_editing) gotoAndStop("static");
		}
		
		public function change(e:Event):void {
			_text = e.target.text;
			if(onChange != null) onChange();
		}
		
		public function get editing():Boolean {
			return _editing;
		}
		public function get text():String {
			return _text;
		}
		public function set text(s:String):void {
			textField.text = _text = s;
		}
		
	}
	
}