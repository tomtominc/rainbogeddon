﻿package editor {
	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.ComboBox;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.net.LocalConnection;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.getQualifiedClassName;
	import flash.utils.Timer;
	
	/**
	* ...
	* @author Chris Burt-Brown
	*/
	public class Panel extends Sprite {
		
		public var fileOpen:Button;
		public var fileSave:Button;
		public var fileRaw:Button;
		public var setData:Button;
		public var filename:TextBox;
		public var test:Button;
		public var background:Sprite;
		public var mode:SimpleSelection;
		public var zoom:SimpleSelection;
		public var palette:Palette;
		public var paletteBackground:MovieClip;
		
		public var signPopup:SignPopup;
		public var signPopupVis:Number = 0;
		
		public var autosavedFilename:String = null;
		public var autosaveTimer:Timer;
		
		public static const MODE_SCROLL:int = 0;
		public static const MODE_DRAW:int = 1;
		public static const MODE_ERASE:int = 2;
		public static const MODE_GRAB:int = 3;
		public static const MODE_PATH:int = 4;
		public static const MODE_SELECT:int = 5;
		
		public function Panel() {
			Controller.setup(this);
		}
		
		public function setup():void {
			mode.addOption("Scrl");
			mode.addOption("Draw");
			mode.addOption("Erase");
			mode.addOption("Grab");
			mode.addOption("Path");
			mode.addOption("Bo");
			mode.onChange = onModeChange;
			mode.index = 0;
			
			zoom.addOption("25%");
			zoom.addOption("33%");
			zoom.addOption("50%");
			zoom.addOption("100%");
			zoom.index = 3;
			zoom.onChange = onZoomChange;
			
			signPopup.y = 0;
			
			//palette.entryScale = 0.5;
			
			try {
				createPalette();
			} catch(e:Error) {
				Alert.alert("Error creating palette items. (" + e.message + ")");
			}
			
			//palette.onChange = onPaletteChange;
			//palette.onChange(); // make sure everything knows about the initial selection
			
			fileOpen.addEventListener(MouseEvent.CLICK, onLoadClick);
			fileSave.addEventListener(MouseEvent.CLICK, onSaveClick);
			fileRaw.addEventListener(MouseEvent.CLICK, onRawClick);
			setData.addEventListener(MouseEvent.CLICK, onSetDataClick);
			test.addEventListener(MouseEvent.CLICK, onTestClick);
			
			fileOpen.tabEnabled = fileOpen.focusEnabled = false;
			fileSave.tabEnabled = fileSave.focusEnabled = false;
			fileRaw.tabEnabled = fileRaw.focusEnabled = false;
			test.tabEnabled = test.focusEnabled = false;
			
			signPopup.signText.onChange = onSignTextChanged;
			
			autosaveTimer = new Timer(10000);
			if(ExternalInterface.available) autosaveTimer.addEventListener(TimerEvent.TIMER, autosave);
		}
		protected function createPalette():void {
			palette.setup();
			
			/*palette.addSection("Tile", new Autoarranger(Controller.layers.tile.tilePrefix, [
				"HardBlock",
			]), Controller.layers.tile);*/
			
			palette.addSection("Entity", new Autoarranger(Controller.layers.entity.tilePrefix, [
				"HardBlock", "SoftBlock", "GateBlock_param_", "Jelly", "Shooter", "JellyMama", "TattleTale", "SnakeHead", "ShortSnakeHead", "Patroller", "Evader", "HomeyShooter", "Projector", "TimerFollower", "NoWrapper", "Smarty", "HardExplodey", "EasyExplodey", "Stinky", "Player1", "Player2"
			]), Controller.layers.entity);
			
			palette.addSection("Lower Layer Entities", new Autoarranger(Controller.layers.moreEntities.tilePrefix, [
				"BlockSwitch", "NormalCoin", "StaticGreenTeleporter", "StaticOrangeTeleporter", "StaticSafetyBubble"
			]), Controller.layers.moreEntities);			
			
			palette.addSection("PowerUp", new Autoarranger(Controller.layers.powerup.tilePrefix, [
				"Gun", "Bomb", "Drill", "Teleporter", "Tail", "Bubble", "Heart", "Gun_param_", "Bomb_param_", "Drill_param_", "Teleporter_param_", "Tail_param_", "Bubble_param_", "Heart_param_"
			]), Controller.layers.powerup);
			
			/*palette.addSection("LevelControl", new Autoarranger(Controller.layers.levelControl.tilePrefix, [
				"SpacerMC", "StartMC"
			]), Controller.layers.levelControl);*/
			
			
			palette.onChange = onPaletteChange;
			palette.onChange();
		}
		
		public function onModeChange():void {
			if(mode.index == MODE_PATH) Controller.pathEditingTile = null;
			Controller.showPaths(mode.index == MODE_PATH);
			Controller.selectionSprite.visible = mode.index == MODE_SELECT;
		}
		
		public function onZoomChange():void {
			var zooms:Array = [0.25, 0.33, 0.5, 1];
			Controller.setZoomLevel(zooms[zoom.index]);
		}
		
		public function onPaletteChange():void {
			/*var selectedEntry:PaletteItem = palette.getSelectedEntry();
			if(!selectedEntry) return;
			
			Controller.drawingTile = selectedEntry.getClassName();
			Controller.drawingLayer = selectedEntry.getTargetLayer();
			mode.index = 1;*/
			
			Controller.drawingTile = palette.selectedType;
			Controller.drawingLayer = Controller.layers.t;
			mode.index = 1;
		}
		
		public function resize():void {
			y = stage.stageHeight - background.height;
			background.width = stage.stageWidth;
			paletteBackground.width = stage.stageWidth - palette.x - 4;
			palette.setSize(paletteBackground.width - 2, paletteBackground.height - 2);
			//palette.setSize(stage.stageWidth - palette.x - palette.y, palette.height);
		}
		
		public function onLoadClick(e:MouseEvent):void {
			var loader:URLLoader = new URLLoader;
			var fn:String = "levels/" + filename.text;
			if(fn.substr(-4).toLowerCase() != ".xml") fn += ".xml";
			loader.addEventListener(Event.COMPLETE, function(e:Event):void {
				Controller.fromXML(new XML(e.target.data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function(e:Event):void {
				Alert.alert("Unable to load file (" + fn.substr(7).toUpperCase() + ")!");
			});
			loader.load(new URLRequest(fn));
		}
		
		public function onSaveClick(e:MouseEvent):void {
			if(filename.text == "") {
				Alert.alert("Save error (no filename)!");
				return;
			}
			
			var fn:String = this.loaderInfo.loaderURL;
			fn = fn.substring(0, fn.lastIndexOf("/"));
			fn += "/levels/" + filename.text;
			if(fn.substr(-4).toLowerCase() != ".xml") fn += ".xml";
			
			var content:String = Controller.toXML().toXMLString();
			
			if(ExternalInterface.call("saveFile", fn, content, false, true)) {
				removeAutosave();
			} else {
				Alert.alert("Save error!");
			}
		}
		
		public function onRawClick(e:MouseEvent):void {
			var xmlDisplay:XMLDisplay = XMLDisplay.instance;
			if(!XMLDisplay.instance) xmlDisplay = new XMLDisplay;
			
			xmlDisplay.txtXML.text = Controller.toXML().toXMLString();
		}
		
		public function onSetDataClick(e:MouseEvent):void {
			var settingsDisplay:SettingsDisplay = SettingsDisplay.instance;
			if(!SettingsDisplay.instance) settingsDisplay = new SettingsDisplay;
		}
		
		protected function autosave(e:TimerEvent):void {
			var fn:String = this.loaderInfo.loaderURL;
			fn = fn.substring(0, fn.lastIndexOf("/"));
			fn += "/levels/" + filename.text;
			if(fn.substr(-4).toLowerCase() == ".xml") fn = fn.substr(0, fn.length - 4);
			fn += "-autosave.xml";
			
			var content:String = Controller.toXML().toXMLString();
			
			if(ExternalInterface.call("saveFile", fn, content, true, false)) {
				autosavedFilename = fn;
				autosaveTimer.stop();
			}
		}
		protected function removeAutosave():void {
			if(autosavedFilename) {
				ExternalInterface.call("removeFile", autosavedFilename);
				autosavedFilename = null;
			}
			autosaveTimer.stop();
		}
		
		public function onTestClick(e:MouseEvent):void {
			/*if(filename.text == "") {
				Alert.alert("Save error (no filename)!");
				return;
			}
			
			autosave(null);*/
			
			Controller.startTest();
			/*var fn:String = filename.text;
			if(fn.substr(-4).toLowerCase() == ".xml")
				fn = fn.substr(0, fn.length - 4);
			
			var url:String = "http://editorstorage.brokenbeta.co.uk/action/save.php";
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();
			requestVars.targetFilename = fn;
			requestVars.saveData = Controller.toXML().toXMLString();
			request.data = requestVars;
			request.method = URLRequestMethod.POST;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, function(e:Event) {
				Alert.alert("Upload successful!", true);
			}, false, 0, true);
			try {
				urlLoader.load(request);
			} catch(e:Error) {
				Alert.alert("Unable to upload!");
			}*/
		}
		
		public function onSignTextChanged():void {
			if (Controller.paramTile)
			{
				Controller.paramTile.setSignText(signPopup.signText.text);
			}// end if
			Controller.onMouseMove(null);
		}
		
		public function randomString():String {
			var result:String = "";
			for(var n:Number = 0; n < 16; n ++) {
				var x:Number = Math.floor(Math.random() * 16);
				result += "0123456789abcdef".charAt(x);
			}
			return result;
		}
		
		public function advance():void {
			var targetVis:Number = 0;
			if(getQualifiedClassName(Controller.drawingTile).indexOf("_param_") != -1) {
				targetVis = 1;
			}
			
			if(signPopupVis != targetVis) {
				signPopupVis = ((signPopupVis - targetVis) * 0.7) + targetVis;
				if(Math.abs(targetVis - signPopupVis) < 0.01)
					signPopupVis = targetVis;
				signPopup.y = Math.round(-27 * signPopupVis);
			}
		}
		
	}
	
}