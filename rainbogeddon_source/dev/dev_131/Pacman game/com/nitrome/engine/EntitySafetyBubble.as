package com.nitrome.engine
{
	import com.nitrome.engine.pathfinding.RadialPathMap;
	import flash.display.BitmapData;
	import com.nitrome.engine.pathfinding.PathMap;
	import com.nitrome.geom.PointUint;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class EntitySafetyBubble extends EntityCentered
	{
		
		public static const STATE_WAITING_T0_EXPAND:uint      = 0;
		public static const STATE_EXPANDING:uint              = 1;
		public static const STATE_EXPANDED:uint               = 2;
		public static const STATE_FADING:uint                 = 4;
		//public static const STATE_FADING_OVER_WAITING:uint    = 5;
		
		public static const EXPANSION_WAIT_DURATION:int = 200;
		public static const EXPANDED_DURATION:int       = 240000;
		public static const DETONATION_DURATION:int     = 4000;
		public static const SIZE:uint                   = 2;
		
		//public var m_detonationStartTime:int;		
		public var  m_tileMapBitmap:BitmapData;
		public var  m_bubbleClip:MovieClip;
		public var  m_radialPathMap:RadialPathMap;
		public var  m_radius:uint;
		public var  m_currRadius:uint;
		public var  m_affectedTilePosList:Vector.<Vector.<PointUint>>;
		public var  m_particleList:Vector.<Vector.<EntityBubbleParticle>>;
		public var  m_expansionWaitStartTime:int;
		public var  m_fadingWaitStartTime:int;
		public var  m_expandedWaitStartTime:int;
		public var  m_canDisable:Boolean;
		public var  m_startedAddingTiles:Boolean;
		private var m_queueFade:Boolean;
		
		
		public function EntitySafetyBubble(tx_:uint, ty_:uint, size_:uint = SIZE)
		{
			super(tx_, ty_);
			m_canDisable = false;
			m_startedAddingTiles = false;
			m_collisionRect.x = -2.5;
			m_collisionRect.y = -2.5;
			m_collisionRect.width = 5;
			m_collisionRect.height = 5;
			m_queueFade = false;
			m_canDie = false;
			m_bubbleClip = new (getDefinitionByName("BombCircle" + size_) as Class)();
			var tileBlockedColor:uint = 0xffff0000;
			var sentinelColor:uint    = 0xff00ff00;			
			
			// bomb width and height need to be an odd number
			m_radius = (m_bubbleClip.width - 1) / 2;
			
			m_radialPathMap = new RadialPathMap(Game.g.mapWidth, Game.g.mapHeight, RadialPathMap.TILE_BLOCKED);
			
			m_tileMapBitmap = new BitmapData(Game.g.mapWidth, Game.g.mapHeight, true, tileBlockedColor);
			
			var matrix:Matrix = new Matrix();
			
			matrix.translate(m_tx, m_ty);
			
			m_tileMapBitmap.draw(m_bubbleClip, matrix);		
			
			// draw wrapped portions of the bomb
			
			if ((m_tx - m_radius) < 0)
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);
			}
			else if ((m_tx + m_radius) >= m_tileMapBitmap.width)
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);				
			}// end else
			
			if ((m_ty - m_radius) < 0)
			{
				matrix.identity();
				matrix.translate(m_tx, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);
			}
			else if ((m_ty + m_radius) >= m_tileMapBitmap.height)
			{
				matrix.identity();
				matrix.translate(m_tx, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);				
			}// end else			
			
			if (((m_tx - m_radius) < 0) && ((m_ty - m_radius) < 0))
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);						
			}
			else if (((m_tx - m_radius) < 0) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				matrix.identity();
				matrix.translate(m_tx + m_tileMapBitmap.width, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);				
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty - m_radius) < 0))
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty + m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);				
			}
			else if (((m_tx + m_radius) >= m_tileMapBitmap.width) && ((m_ty + m_radius) >= m_tileMapBitmap.height))
			{
				matrix.identity();
				matrix.translate(m_tx - m_tileMapBitmap.width, m_ty - m_tileMapBitmap.height);
				m_tileMapBitmap.draw(m_bubbleClip, matrix);				
			}// end else if
			
			// replace the circle color with the sentinel color
			m_tileMapBitmap.threshold(m_tileMapBitmap, m_tileMapBitmap.rect, new Point(0,0), "==", 0xff000000, sentinelColor, 0xffffffff);
			
			
			// merge the game object map with the bubble map
			// using both hard tiles and soft tiles
			
			var tileMap:Vector.<uint> = new Vector.<uint>(m_tileMapBitmap.width * m_tileMapBitmap.height);
			
			for (var currTile:uint = 0; currTile < tileMap.length; currTile++)
			{
				tileMap[currTile] = sentinelColor;
			}// end for				
			
			var gameObjectList:Vector.<GameObject> =  Game.g.m_gameObjectList;
			
			for (var currGameObj:uint = 0; currGameObj < gameObjectList.length; currGameObj++)
			{
				
				if (tileMap[gameObjectList[currGameObj].m_ty * m_tileMapBitmap.width + gameObjectList[currGameObj].m_tx] == sentinelColor)
				{
					tileMap[gameObjectList[currGameObj].m_ty * m_tileMapBitmap.width + gameObjectList[currGameObj].m_tx] = ((gameObjectList[currGameObj] is EntityBlock) || (gameObjectList[currGameObj] is EntityRainbowCoin))? tileBlockedColor : sentinelColor;		
				}// end if
				
			}// end for					
			
			var bombMapList:Vector.<uint> = m_tileMapBitmap.getVector(m_tileMapBitmap.rect);
			
			for (currTile = 0; currTile < bombMapList.length; currTile++)
			{
				if ((bombMapList[currTile] == sentinelColor) && (tileMap[currTile] == tileBlockedColor))
				{
					bombMapList[currTile] = tileBlockedColor;
				}// end if
			}// end for
			
				
			
	
			for (currTile = 0; currTile < m_radialPathMap.m_map.length; currTile++)
			{
				if (bombMapList[currTile] == tileBlockedColor)
				{
					m_radialPathMap.m_map[currTile] = RadialPathMap.TILE_BLOCKED;
				}// end if
			}// end for
						
			m_state = STATE_WAITING_T0_EXPAND;
			//m_detonationStartTime = Game.g.timer;	
			expand();
			
		}// end public function EntitySafetyBubble(tx_:uint, ty_:uint)
				
	
		public override function doPhysics():void
		{
			
			switch(m_state)
			{
				
				case STATE_WAITING_T0_EXPAND:
				{
				
					
					break;
				}// end case
				
				case STATE_EXPANDING:
				{
					
					continueExpanding();
					
					break;
				}// end case	
				
				case STATE_EXPANDED:
				{
					
					onExpanded();
					
					break;
				}// end case	
				
				case STATE_FADING:
				{
					
					continueFading();
					
					break;
				}// end case					
				
			}// end switch
						
		}// end public override function doPhysics():void			
				
		
		
		public function onExpanded():void
		{
			
			if ((Game.g.timer - m_expandedWaitStartTime > EXPANDED_DURATION) || m_queueFade)
			{
				fade();	
			}// end if
			
		}// end public function onExpanded():void				
		
		public function expand():void
		{
			
			m_affectedTilePosList = new Vector.<Vector.<PointUint>>;
			m_particleList = new Vector.<Vector.<EntityBubbleParticle>>;
			m_currRadius = 0;
			m_radialPathMap.castPath(m_tx, m_ty, m_radius);
			m_state = STATE_EXPANDING;
			m_expansionWaitStartTime = 0;
			continueExpanding();
			
		}// end public function expand():void			
		
		public function continueExpanding():void
		{
			
			
			var done:Boolean = false;
			var addClips:Boolean = false;
			
			if (Game.g.timer - m_expansionWaitStartTime > EXPANSION_WAIT_DURATION)
			{
			
				m_expansionWaitStartTime = Game.g.timer;
				done = m_radialPathMap.continueCastingPath();
				m_affectedTilePosList.push(m_radialPathMap.getNewMarkedPathPosList());
				
				if (!m_startedAddingTiles)
				{					
					m_startedAddingTiles = true;
					Game.g.m_safetyTilePosList.push(m_affectedTilePosList);
				}// end if
				
				m_particleList.push(new Vector.<EntityBubbleParticle>);
				addClips = true;
				m_currRadius++;
			
			}// end if
						
		
			
			for (var currLayer:uint = 0; currLayer < m_affectedTilePosList.length; currLayer++)
			{
			
				for (var currExplodingTile:uint = 0; currExplodingTile < m_affectedTilePosList[currLayer].length; currExplodingTile++)
				{
					
					if (addClips && (currLayer == (m_affectedTilePosList.length - 1)))
					{
					
						var particle:EntityBubbleParticle = new bubbleParticleType(m_affectedTilePosList[currLayer][currExplodingTile].m_x, m_affectedTilePosList[currLayer][currExplodingTile].m_y);
						Game.g.m_gameObjectList.push(particle);
						m_particleList[currLayer].push(particle);
					
					}// end if
					
				}// end for		
				
			}// end for			
			
			
			if (done)
			{
				
				m_state = STATE_EXPANDED;
				m_canDisable = true;
				m_expandedWaitStartTime = Game.g.timer;
				
			}// end if
			
			
		}// end public function continueExploding():void		
		
		public function fade():void
		{
			
			if (m_state == STATE_EXPANDED)
			{

				m_state = STATE_FADING;
				m_fadingWaitStartTime = 0;
				continueFading();
				
			}
			else
			{
				m_queueFade = true;
			}// end else
			
		}// end public function fade():void				
		
		public function continueFading():void
		{
			
	
			var entityCollisionRect:Rectangle = new Rectangle();
			var gameObjectList:Vector.<GameObject> =  Game.g.m_gameObjectList;
			
			
			if (Game.g.timer - m_fadingWaitStartTime > EXPANSION_WAIT_DURATION)
			{
			
				m_fadingWaitStartTime = Game.g.timer;
				
				for (var currParticle:uint = 0; currParticle < m_particleList[0].length; currParticle++)
				{
					
					//m_particleList[0][currParticle].gotoAndPlay("fading");
					m_particleList[0][currParticle].terminate();
					
				}// end for
				
				m_particleList.splice(0, 1);
				m_affectedTilePosList.splice(0, 1);
				
				if (m_particleList.length == 0)
				{
					//m_state = STATE_FADING_OVER_WAITING;	
					m_terminated = true;
					
					// remove reference from list
					for (var currList:uint = 0; currList < Game.g.m_safetyTilePosList.length; currList++)
					{
						if (Game.g.m_safetyTilePosList[currList] == m_affectedTilePosList)
						{
							Game.g.m_safetyTilePosList.splice(currList, 1);
							break;
						}// end if
					}// end for							
					
				}// end if
			
			}// end if
						
			
		}// end public function continueFading():void				
		
		public function get bubbleParticleType():Class
		{
			return null;
		}// end public function get bubbleParticleType():Class			
				
	}// end public class EntitySafetyBubble
	
}// end package