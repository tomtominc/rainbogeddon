package com.nitrome.engine
{
	
	import com.nitrome.geom.PointUint;
	import com.nitrome.sound.SoundManager;
	
	public class TailHolder extends WeaponHolder
	{
		
		private var m_tailList:Vector.<EntityTail>;
		private var m_detachedTailList:Vector.<EntityTail>;
		private var m_powerToTailLengthTable:Object;
		public var  m_fireStartTime:int;
		public var  m_fireDelay:int;	
		
		public function TailHolder(parent_:EntityCharacter)
		{
			super(parent_);
			
			m_fireStartTime = 0;
			m_fireDelay = 250;			
			
			m_tailList = new Vector.<EntityTail>;
			m_detachedTailList = new Vector.<EntityTail>;
			
			m_powerToTailLengthTable = new Object();
			
			m_powerToTailLengthTable[1] = 2;
			m_powerToTailLengthTable[2] = 4;
			m_powerToTailLengthTable[3] = 8;
			
			var tailClass:Class; 
			
			if (parent_ is EntityPlayer1)
			{
				tailClass = EntityPlayer1Tail;
			}
			else
			{
				tailClass = EntityPlayer2Tail;
			}// end else
			
			//Game.g.m_safetyTilePosList.push(m_affectedTilePosList);
			
			m_tailList.push(new tailClass(m_parent, m_parent));
			//parent_.m_backChildGameObjectList.push(m_tailList[m_tailList.length - 1]);
			Game.g.m_gameObjectList.push(m_tailList[m_tailList.length - 1]);
			
			
			for (var currSegment:uint = 1; currSegment < m_powerToTailLengthTable[1]; currSegment++)
			{
				m_tailList.push(new tailClass(m_tailList[m_tailList.length - 1], m_parent));
				//parent_.m_backChildGameObjectList.push(m_tailList[m_tailList.length - 1]);	
				Game.g.m_gameObjectList.push(m_tailList[m_tailList.length - 1]);
			}// end for
			
		}// end public function TailHolder()
				
		public override function canFire():Boolean
		{
			return (m_tailList.length && ((Game.g.timer - m_fireStartTime) > m_fireDelay));
		}// end public override function canFire():Boolean			
		
		public override function tryFire():Boolean
		{
			
			if (canFire())
			{
				m_parent.m_fire = false;
				m_fireStartTime = Game.g.timer;
				m_tailList[m_tailList.length - 1].detach();
				m_detachedTailList.push(m_tailList[m_tailList.length - 1]);
				SoundManager.playSound("DropTail");
				m_tailList.splice(m_tailList.length - 1, 1);
				if (!m_tailList.length)
				{
					(m_parent as EntityPlayer).terminateWeapon();
				}// end if
				return true;
			}// end if			
			
			return false;
			
		}// end public override function tryFire():Boolean		
		
		public override function terminate():void
		{
			
			for (var currSegment:uint = 0; currSegment < m_tailList.length; currSegment++)
			{
				m_tailList[currSegment].m_terminated = true;
				Entity.m_spatialHashMap.unregisterObject(m_tailList[currSegment]);
			}// end if		
			
			/*for (currSegment = 0; currSegment < m_detachedTailList.length; currSegment++)
			{
				m_detachedTailList[currSegment].m_terminated = true;
				Entity.m_spatialHashMap.unregisterObject(m_detachedTailList[currSegment]);
			}// end if*/				
			
		}// end public override function terminate():void		

		public override function upgrade():Boolean
		{

			if (m_tailList.length < m_powerToTailLengthTable[MAX_UPGRADE_STAGES])
			{
				
				if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
				{
					
					super.upgrade();
					m_currUpgradeStage++;
				
				}// end if
				
				var tailClass:Class; 
				
				if (m_parent is EntityPlayer1)
				{
					tailClass = EntityPlayer1Tail;
				}
				else
				{
					tailClass = EntityPlayer2Tail;
				}// end else				
				
				if (!m_tailList.length)
				{
					
					m_tailList.push(new tailClass(m_parent, m_parent));
					Game.g.m_gameObjectList.push(m_tailList[m_tailList.length - 1]);
					
					
					for (var currSegment:uint = 1; currSegment < m_powerToTailLengthTable[1]; currSegment++)
					{
						m_tailList.push(new tailClass(m_tailList[m_tailList.length - 1], m_parent));	
						Game.g.m_gameObjectList.push(m_tailList[m_tailList.length - 1]);
					}// end for					
					
				}
				else
				{
					var numAddedSegments:uint = 0
					var initLength:uint = m_tailList.length;
					var toLength:uint = m_powerToTailLengthTable[m_currUpgradeStage];
					for (currSegment = m_tailList.length; currSegment < toLength; currSegment++)
					{
						numAddedSegments++;
						m_tailList.push(new tailClass(m_tailList[m_tailList.length - 1], m_parent));
						//m_parent.m_backChildGameObjectList.push(m_tailList[m_tailList.length - 1]);
						Game.g.m_gameObjectList.push(m_tailList[m_tailList.length - 1]);
					}// end for		
				
				}// end else
				
				return true;
				
			}
			else
			{
				return false;
			}// end else
		}// end public override function upgrade():Boolean					
			
		public override function onTeleport():void
		{

			for (var currSegment:uint = 0; currSegment < m_tailList.length; currSegment++)
			{
				m_tailList[currSegment].startTeleport();
			}// end if						
			
		}// end public override function onTeleport():void	
		
		public override function onTeleportDone():void
		{

			for (var currSegment:uint = 0; currSegment < m_tailList.length; currSegment++)
			{
				m_tailList[currSegment].onTeleportDone();
			}// end if				
			
		}// end public override function onTeleportDone():void		
		
		public override function onTeleportCancel():void
		{

			for (var currSegment:uint = 0; currSegment < m_tailList.length; currSegment++)
			{
				m_tailList[currSegment].onTeleportCancel();
			}// end if					
			
		}// end public override function onTeleportCancel():void		
		
	}// end public class TailHolder
	
}// end package