package com.nitrome.engine
{
	
	
	public class EntityHomey extends EntityEnemy 
	{
			
		private var m_parent:EntityHomeyShooter;	
		
		public function EntityHomey(parent_:EntityHomeyShooter)
		{
			
			super(parent_.m_cTX, parent_.m_cTY);
			m_drawOrder = 1;
			m_respawns = false;
			x = parent_.x;
			y = parent_.y;
			m_speed = 2.5;
			m_hitPoints = 0;
			m_maxHitPoints = m_hitPoints;
			m_collisionRect.x = -5;
			m_collisionRect.y = -5;
			m_collisionRect.width = 10;
			m_collisionRect.height = 10;				
			m_glow.m_color = 0xffff0000;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;		
			m_hasLifetime = true;
			m_lifeStartTime = Game.g.timer;
			m_lifetime = 5000;
			m_parent = parent_;
		}// end public function EntityHomey(tx_:uint, ty_:uint, parent_:EntityBlocky)
					
		public override function onLifetimeEnded():void
		{
			onDeath();
			//m_parent.m_homey = null;
		}// end public override function onLifetimeEnded():void			
		
		public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void
		{
			super.onDeath(attacker_, weapon_);
			//m_parent.m_homey = null;
		}// end public override function onDeath(attacker_:Entity = null, weapon_:Entity = null):void				
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedRed;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntityHomey
	
}// end package