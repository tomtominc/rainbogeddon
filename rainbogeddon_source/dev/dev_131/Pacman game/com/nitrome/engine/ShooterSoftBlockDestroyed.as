package com.nitrome.engine
{
	
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.utils.getTimer;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import fl.motion.Color;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class ShooterSoftBlockDestroyed extends GameObject
	{
				
		public function ShooterSoftBlockDestroyed(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);		
			m_drawBitmap = Game.g.m_effectsBitmap;		
			
		}// end public function ShooterSoftBlockDestroyed(tx_:uint, ty_:uint)
		
	}// end public class ShooterSoftBlockDestroyed
	
}// end package