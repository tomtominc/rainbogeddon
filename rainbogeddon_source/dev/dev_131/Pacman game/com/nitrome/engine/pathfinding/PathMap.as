package com.nitrome.engine.pathfinding 
{
	
	import com.nitrome.geom.PointUint;
	import com.nitrome.engine.Entity;
	
	public class PathMap
	{
		
		public  var m_map:Vector.<uint>;
		public  var m_width:uint;
		public  var m_height:uint;
		private var m_stack:Vector.<PathMapStackArg>;
		private var m_stackArgPool:Vector.<PathMapStackArg>;
		private var m_stackPoolPointer:uint;
		public  var m_tileMap:Vector.<uint>;
		public  var m_distanceCap:uint;
		public  var m_wraps:Boolean;
		
		public static const TILE_SENTINEL:uint = uint.MAX_VALUE-1,
							TILE_BLOCKED:uint  = uint.MAX_VALUE;
		
		public function PathMap(width_:uint, height_:uint)
		{
			
			m_map = new Vector.<uint>(width_*height_);
			m_tileMap = new Vector.<uint>;
			m_stack  = new Vector.<PathMapStackArg>;
			m_stackArgPool = new Vector.<PathMapStackArg>;
			for (var currArg:uint = 0; currArg < (m_map.length * 2); currArg++)
			{
				m_stackArgPool.push(new PathMapStackArg(0,0,0));
			}// end for
			m_stackPoolPointer = 0;
			m_width  = width_;
			m_height = height_;
			m_distanceCap = TILE_SENTINEL - 1;
			m_wraps = true;
								
		}// end public function PathMap(width_:uint, height_:uint)
		
		/* *
		 * Assigns a new preallocated stack argument
		 * */
		public function newPathMapStackArg(x_:int, y_:int, dist_:uint):PathMapStackArg
		{		
			
			m_stackArgPool[m_stackPoolPointer].m_x = x_;
			m_stackArgPool[m_stackPoolPointer].m_y = y_;
			m_stackArgPool[m_stackPoolPointer].m_dist = dist_;
			m_stackPoolPointer++;
			return m_stackArgPool[m_stackPoolPointer - 1];
			
		}// end public function newPathMapStackArg(x_:int, y_:int, dist_:uint):PathMapStackArg
		
		/* *
		 * Returns a vector with all the positions of the tiles
		 * that lie within the path
		 * */
		public function getMarkedPathPosList():Vector.<PointUint>
		{
			
			var posX:uint = 0;
			var posY:uint = 0;
			
			var posList:Vector.<PointUint> = new Vector.<PointUint>;
			
			for (var currTile:uint = 0; currTile < m_map.length; currTile++)
			{
				
				posY = currTile / m_width;
				posX = currTile - (posY * m_width);
				
				if (m_map[currTile] <= m_distanceCap)
				{
					posList.push(new PointUint(posX, posY));
				}// end if
				
			}// end for
			
			return posList;
			
		}// end public function getMarkedPathPosList():Vector.<PointUint>
		
		public function getDistFromTarget(tileX_:int, tileY_:int):uint
		{
			
			var currPosOffset:uint = (m_width*tileY_)+tileX_;
			var cellValue:uint = m_map[currPosOffset];
			
			if ((cellValue == TILE_BLOCKED) || (cellValue == TILE_SENTINEL) || (tileX_ < 0) || (tileX_ > m_width-1) || (tileY_ < 0) || (tileY_ > m_height-1))
			{
				
				return TILE_BLOCKED;
				
			}// end if
			
			return cellValue;

		}// end public function getDistFromTarget(tileX_:int, tileY_:int):uint		
		
		public function getNextGoalDir(tileX_:int, tileY_:int):int
		{
			
			var currPosOffset:uint = (m_width*tileY_)+tileX_;
			var cellValue:uint = m_map[currPosOffset];
			
			if ((cellValue == TILE_BLOCKED) || (cellValue == TILE_SENTINEL) || (tileX_ < 0) || (tileX_ > m_width-1) || (tileY_ < 0) || (tileY_ > m_height-1))
			{
				
				return Entity.DIR_NONE;
				
			}// end if
			
			var currDist:uint = cellValue;
			
			var cellLeft:uint;
			var cellRight:uint;
			var cellTop:uint;
			var cellBottom:uint;
						
			var finalX:int;
			var finalY:int;
			
			
			
			// get left
			
			finalX = tileX_ -1;
			finalY = tileY_;
			
			if (finalX < 0)
			{
				if (m_wraps)
				{
					finalX = m_width - 1;
					cellLeft = m_map[(m_width * finalY) + finalX];
				}
				else
				{
					cellLeft = TILE_SENTINEL;
				}// end else
			}
			else
			{
				cellLeft = m_map[(m_width * finalY) + finalX];
			}// end else
			
			
			
			// get right
			
			finalX = tileX_ +1;
			finalY = tileY_;
			
			if (finalX > m_width-1)
			{
				if (m_wraps)
				{
					finalX = 0;
					cellRight = m_map[(m_width * finalY) + finalX];	
				}
				else
				{
					cellRight = TILE_SENTINEL;	
				}// end else
			}
			else
			{
				cellRight = m_map[(m_width * finalY) + finalX];	
			}// end else
			
			
			
			// get top
			
			finalX = tileX_;
			finalY = tileY_-1;
			
			if (finalY < 0)
			{
				if (m_wraps)
				{
					finalY = m_height - 1;
					cellTop = m_map[(m_width * finalY) + finalX];
				}
				else
				{
					cellTop = TILE_SENTINEL;	
				}// end else
			}
			else
			{
				cellTop = m_map[(m_width * finalY) + finalX];	
			}// end else
			
							
			
			// get bottom
			
			finalX = tileX_;
			finalY = tileY_+1;
			
			if (finalY > m_height-1)
			{
				if (m_wraps)
				{
					finalY = 0;
					cellBottom = m_map[(m_width * finalY) + finalX];	
				}
				else
				{
					cellBottom = TILE_SENTINEL;
				}// end else
			}
			else
			{
				cellBottom = m_map[(m_width * finalY) + finalX];	
			}// end else
			
			
			
			
			var lowestDist:uint = cellLeft;
			var lowestDir:uint = Entity.DIR_LEFT;
			
			var choice:uint = Math.floor(Math.random()*2);
			
			if (cellRight < lowestDist)
			{
				lowestDist = cellRight;
				lowestDir = Entity.DIR_RIGHT;
			}
			else if((cellRight == lowestDist) && choice)
			{
				lowestDist = cellRight;
				lowestDir = Entity.DIR_RIGHT;
			}// end else
			
		
			if (cellTop < lowestDist)
			{
				lowestDist = cellTop;
				lowestDir = Entity.DIR_UP;
			}
			else if((cellTop == lowestDist) && choice)
			{
				lowestDist = cellTop;
				lowestDir = Entity.DIR_UP;
			}// end else	
			
			
			if (cellBottom < lowestDist)
			{
				lowestDist = cellBottom;
				lowestDir = Entity.DIR_DOWN;
			}
			else if((cellBottom == lowestDist) && choice)
			{
				lowestDist = cellBottom;
				lowestDir = Entity.DIR_DOWN;
			}// end else			
			
			
			//if (lowestDist <= currDist)
			//{
				return lowestDir;
			/*}
			else
			{
				return Entity.DIR_NONE;
			}// end else*/
			
		}// end public function getNextGoalDir(tileX_:int, tileY_:int):int
		
		public function getNextEvasionDir(tileX_:int, tileY_:int):int
		{
			
			var currPosOffset:uint = (m_width*tileY_)+tileX_;
			var cellValue:uint = m_map[currPosOffset];
			
			if ((cellValue == TILE_BLOCKED) || (cellValue == TILE_SENTINEL) || (tileX_ < 0) || (tileX_ > m_width-1) || (tileY_ < 0) || (tileY_ > m_height-1))
			{
				
				return Entity.DIR_NONE;
				
			}// end if
			
			var currDist:uint = cellValue;
			
			var cellLeft:uint;
			var cellRight:uint;
			var cellTop:uint;
			var cellBottom:uint;
						
			var finalX:int;
			var finalY:int;
			
			
			
			// get left
			
			finalX = tileX_ -1;
			finalY = tileY_;
			
			if (finalX < 0)
			{
				if (m_wraps)
				{
					finalX = m_width - 1;
					cellLeft = m_map[(m_width * finalY) + finalX];
				}
				else
				{
					cellLeft = 0;
				}// end else
			}
			else
			{
				cellLeft = m_map[(m_width * finalY) + finalX];
			}// end else

			
			
			
			// get right
			
			finalX = tileX_ +1;
			finalY = tileY_;
			
			if (finalX > m_width-1)
			{
				if (m_wraps)
				{
					finalX = 0;
					cellRight = m_map[(m_width * finalY) + finalX];
				}
				else
				{
					cellRight = 0;
				}// end else
			}
			else
			{
				cellRight = m_map[(m_width * finalY) + finalX];		
			}// end else
			
			
			
			// get top
			
			finalX = tileX_;
			finalY = tileY_-1;
			
			if (finalY < 0)
			{
				if (m_wraps)
				{
					finalY = m_height - 1;
					cellTop = m_map[(m_width * finalY) + finalX];
				}
				else
				{
					cellTop = 0;
				}// end else
			}
			else
			{
				cellTop = m_map[(m_width * finalY) + finalX];
			}// end else
			
							
			
			// get bottom
			
			finalX = tileX_;
			finalY = tileY_+1;
			
			if (finalY > m_height-1)
			{
				if (m_wraps)
				{
					finalY = 0;
					cellBottom = m_map[(m_width * finalY) + finalX];	
				}
				else
				{
					cellBottom = 0;	
				}// end else
			}
			else
			{
				cellBottom = m_map[(m_width * finalY) + finalX];	
			}// end else
			
			
			
			
			var highestDist:uint = (cellLeft == TILE_BLOCKED) ? 0 : cellLeft;
			var highestDir:uint = Entity.DIR_LEFT;
			
			var choice:uint = Math.floor(Math.random()*2);
			
			if (cellRight != TILE_BLOCKED)
			{
			
				if (cellRight > highestDist)
				{
					highestDist = cellRight;
					highestDir = Entity.DIR_RIGHT;
				}
				else if((cellRight == highestDist) && choice)
				{
					highestDist = cellRight;
					highestDir = Entity.DIR_RIGHT;
				}// end else
			
			}// end if
			
			
			if (cellTop != TILE_BLOCKED)
			{
		
				if (cellTop > highestDist)
				{
					highestDist = cellTop;
					highestDir = Entity.DIR_UP;
				}
				else if((cellTop == highestDist) && choice)
				{
					highestDist = cellTop;
					highestDir = Entity.DIR_UP;
				}// end else	
				
			}// end if
			
			
			if (cellBottom != TILE_BLOCKED)
			{
			
				if (cellBottom > highestDist)
				{
					highestDist = cellBottom;
					highestDir = Entity.DIR_DOWN;
				}
				else if((cellBottom == highestDist) && choice)
				{
					highestDist = cellBottom;
					highestDir = Entity.DIR_DOWN;
				}// end else			
			
			}// end if
			
			/*if (highestDist >= currDist)
			{*/
				return highestDir;
			/*}
			else
			{
				return Entity.DIR_NONE;
			//}// end else*/
			
		}// end public function getNextEvasionDir(tileX_:int, tileY_:int):int		
		
		public function floodFill(goalX_:int, goalY_:int)
		{
				
			
			if((goalX_ < 0) || (goalY_ < 0) || (goalX_ >= m_width) || (goalY_ >= m_height))
			{
				return;
			}// end if
			
			var tileY:uint = 0;
			var tileX:uint = 0;			
			
			m_map = new Vector.<uint>;
			m_map = m_map.concat(m_tileMap);
	
			
			m_stack.splice(0, m_stack.length);
			
			m_stackPoolPointer = 0;
			m_stack.push(newPathMapStackArg(goalX_, goalY_, 0));
			
			var distance:uint = 0;
			
			var offset:uint = (m_width*goalY_)+goalX_;
			
			if(m_map[offset] == TILE_BLOCKED)
			{
				return;
			}// end if
			
			
			var currMarker:int = 0;
			
			m_map[offset] = m_stack[currMarker].m_dist;
			
			for(currMarker = 0; currMarker < m_stack.length; currMarker++)
			{
				
				if (m_stack[currMarker].m_dist + 1 > m_distanceCap)
				{
					continue;
				}// end if
				
				offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x;
				
				if((m_stack[currMarker].m_x+1 < m_width))
				{
						
					offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x + 1;
					
					if(m_map[offset] != TILE_BLOCKED)
					{
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x+1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
						}// end if
						
					}// end if
						
				}
				else if(m_wraps)
				{
					
					offset = (m_width * m_stack[currMarker].m_y);
					
					if(m_map[offset] != TILE_BLOCKED)
					{
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(0, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
						}// end if
						
					}// end if					
					
				}// end else
					
				if((m_stack[currMarker].m_x-1 >= 0))
				{
						
					offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x - 1;
					
					if(m_map[offset] != TILE_BLOCKED)
					{					
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
						}// end if
					
					}// end if
						
				}
				else if(m_wraps)
				{
					
					offset = (m_width * m_stack[currMarker].m_y) + (m_width-1);
					
					if(m_map[offset] != TILE_BLOCKED)
					{					
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_width-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
						}// end if
					
					}// end if					
					
				}// end else
					
					
				if((m_stack[currMarker].m_y+1 < m_height))
				{
						
					offset = (m_width * (m_stack[currMarker].m_y + 1)) + m_stack[currMarker].m_x;
					
					if(m_map[offset] != TILE_BLOCKED)
					{						
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y+1, m_stack[currMarker].m_dist+1));
						}// end if
						
					}// end if
					
				}
				else if(m_wraps)
				{
					
					offset =  m_stack[currMarker].m_x;
					
					if(m_map[offset] != TILE_BLOCKED)
					{						
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x, 0, m_stack[currMarker].m_dist+1));
						}// end if
						
					}// end if					
					
				}// end else
					
				if((m_stack[currMarker].m_y-1 >= 0))
				{
						
					offset = (m_width * (m_stack[currMarker].m_y - 1)) + m_stack[currMarker].m_x;
					
					if(m_map[offset] != TILE_BLOCKED)
					{						
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y-1, m_stack[currMarker].m_dist+1));
						}// end if
					
					}// end if
						
				}
				else if(m_wraps)
				{
					
					offset = (m_width * (m_height - 1)) + m_stack[currMarker].m_x;
					
					if(m_map[offset] != TILE_BLOCKED)
					{						
					
						distance = m_map[offset];
							
						if((distance > (m_stack[currMarker].m_dist+1)))
						{
							m_map[offset] = m_stack[currMarker].m_dist+1;
							m_stack.push(newPathMapStackArg(m_stack[currMarker].m_x, m_height - 1, m_stack[currMarker].m_dist+1));
						}// end if
					
					}// end if					
				
				}// end else

				
			}// end for
			
			
		}// end public function floodFill(goalX_:uint, goalY_:uint)	
		
		
		/*public function floodFill2Goal(goalAX_:uint, goalAY_:uint, goalBX_:uint, goalBY_:uint)	
		{
				
			
			if((goalAX_ < 0) || (goalAY_ < 0) || (goalAX_ >= m_width) || (goalAY_ >= m_height) || (goalBX_ < 0) || (goalBY_ < 0) || (goalBX_ >= m_width) || (goalBY_ >= m_height))
			{
				return;
			}// end if
				
			
			var tileY:uint = 0;
			var tileX:uint = 0;			
			
			m_map = new Vector.<uint>;
			m_map = m_map.concat(m_tileMap);			
			
			m_stack.splice(0, m_stack.length);
			
			m_stack.push(new PathMapStackArg(goalAX_, goalAY_, 0));
			
			var distance:uint = 0;
			
			var offset:uint = (m_width*goalAY_)+goalAX_;
			
			var currMarker:int = 0;
			
			
			if(m_map[offset] != TILE_BLOCKED)
			{

				
				m_map[offset] = m_stack[currMarker].m_dist;
				
				for(currMarker = 0; currMarker < m_stack.length; currMarker++)
				{
					
					if (m_stack[currMarker].m_dist + 1 > m_distanceCap)
					{
						continue;
					}// end if				
					
					offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x;
					
					if((m_stack[currMarker].m_x+1 < m_width))
					{
							
						offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x + 1;
						
						if(m_map[offset] != TILE_BLOCKED)
						{
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x+1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * m_stack[currMarker].m_y);
						
						if(m_map[offset] != TILE_BLOCKED)
						{
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(0, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if					
						
					}// end else
						
					if((m_stack[currMarker].m_x-1 >= 0))
					{
							
						offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x - 1;
						
						if(m_map[offset] != TILE_BLOCKED)
						{					
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * m_stack[currMarker].m_y) + (m_width-1);
						
						if(m_map[offset] != TILE_BLOCKED)
						{					
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_width-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if					
						
					}// end else
						
						
					if((m_stack[currMarker].m_y+1 < m_height))
					{
							
						offset = (m_width * (m_stack[currMarker].m_y + 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y+1, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if
						
					}
					else if(m_wraps)
					{
						
						offset =  m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, 0, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if					
						
					}// end else
						
					if((m_stack[currMarker].m_y-1 >= 0))
					{
							
						offset = (m_width * (m_stack[currMarker].m_y - 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y-1, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * (m_height - 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_height - 1, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if					
					
					}// end else

					
				}// end for
			
			}// end if
			
			
			if(m_map[(m_width*goalBY_)+goalBX_] != TILE_BLOCKED)
			{			
			

				m_stack.splice(0, m_stack.length);
				
				m_stack.push(new PathMapStackArg(goalBX_, goalBY_, 0));
				
				distance = 0;
				
				offset = (m_width*goalBY_)+goalBX_;
				

				
				currMarker = 0;
				
				m_map[offset] = m_stack[currMarker].m_dist;
				
				for(currMarker = 0; currMarker < m_stack.length; currMarker++)
				{
					
					if (m_stack[currMarker].m_dist + 1 > m_distanceCap)
					{
						continue;
					}// end if					
					
					offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x;
					
					if((m_stack[currMarker].m_x+1 < m_width))
					{
							
						offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x + 1;
						
						if(m_map[offset] != TILE_BLOCKED)
						{
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x+1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * m_stack[currMarker].m_y);
						
						if(m_map[offset] != TILE_BLOCKED)
						{
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(0, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if					
						
					}// end else
						
					if((m_stack[currMarker].m_x-1 >= 0))
					{
							
						offset = (m_width * m_stack[currMarker].m_y) + m_stack[currMarker].m_x - 1;
						
						if(m_map[offset] != TILE_BLOCKED)
						{					
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * m_stack[currMarker].m_y) + (m_width-1);
						
						if(m_map[offset] != TILE_BLOCKED)
						{					
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_width-1, m_stack[currMarker].m_y, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if					
						
					}// end else
						
						
					if((m_stack[currMarker].m_y+1 < m_height))
					{
							
						offset = (m_width * (m_stack[currMarker].m_y + 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y+1, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if
						
					}
					else if(m_wraps)
					{
						
						offset =  m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, 0, m_stack[currMarker].m_dist+1));
							}// end if
							
						}// end if					
						
					}// end else
						
					if((m_stack[currMarker].m_y-1 >= 0))
					{
							
						offset = (m_width * (m_stack[currMarker].m_y - 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_stack[currMarker].m_y-1, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if
							
					}
					else if(m_wraps)
					{
						
						offset = (m_width * (m_height - 1)) + m_stack[currMarker].m_x;
						
						if(m_map[offset] != TILE_BLOCKED)
						{						
						
							distance = m_map[offset];
								
							if((distance > (m_stack[currMarker].m_dist+1)))
							{
								m_map[offset] = m_stack[currMarker].m_dist+1;
								m_stack.push(new PathMapStackArg(m_stack[currMarker].m_x, m_height - 1, m_stack[currMarker].m_dist+1));
							}// end if
						
						}// end if					
					
					}// end else

					
				}// end for			
			
			}// end if
			
			
		}// end public function floodFill2Goal(goalAX_:uint, goalAY_:uint, goalBX_:uint, goalBY_:uint)*/			
		
		
	}// end public class PathMap

}// end package