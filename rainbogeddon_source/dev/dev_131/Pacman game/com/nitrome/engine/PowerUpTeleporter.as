package com.nitrome.engine
{
	
	public class PowerUpTeleporter extends PowerUp
	{
		
		public function PowerUpTeleporter(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);				
			
		}// end public function PowerUpTeleporter(tx_:uint, ty_:uint)
		
		public override function get soundID():String
		{		
			return "Teleport";
		}// end public override function get soundID():String			
		
		public override function get displayName():String
		{		
			return "TELEPORTER";
		}// end public override function get displayName():String			
		
		public override function get holder():Class
		{		
			return TeleporterHolder;
		}// end public override function get holder():Class		
		
	}// end public class PowerUpTeleporter
	
}// end package