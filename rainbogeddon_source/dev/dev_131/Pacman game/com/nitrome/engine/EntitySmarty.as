package com.nitrome.engine
{
	
	
	public class EntitySmarty extends EntityEnemy 
	{
			
		public function EntitySmarty(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);
			m_speed = 3.0;
			m_glow.m_color = 0xffe800ff;
			//m_canSmartFollow = false;
			m_recoverWaitDuration = 4000;
			m_checkDirMaxTimeout = 250;
			m_checkDirTimeout = m_checkDirMaxTimeout;				
			
		}// end public function EntitySmarty(tx_:uint, ty_:uint)			
		
		public override function get respawnEffectType():Class
		{
			return EnemyRespawnPink;
		}// end public override function get respawnEffectType():Class			
		
		public override function get destroyedEffectType():Class
		{
			return EnemyDestroyedPink;
		}// end public override function get destroyedEffectType():Class					
		
	}// end public class EntitySmarty
	
}// end package