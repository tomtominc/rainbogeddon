package com.nitrome.engine
{
	import com.nitrome.util.HiddenInt;
	
	
	public class EntityCollectible extends EntityCentered 
	{
		
		public var m_score:HiddenInt;
		public var m_collected:Boolean;
		
		public function EntityCollectible(tx_:uint, ty_:uint)
		{
			super(tx_, ty_);	
			m_canDie = false;
			m_collected = false;
			m_moves = false;
			m_score = new HiddenInt();
			m_drawBitmap = Game.g.m_secondaryEntityBitmap;
		}// end public function EntityCollectible(tx_:uint, ty_:uint)
		
		public function collect()
		{
			if (!m_collected)
			{
				m_collected = true;
				m_terminated = true;
				super.onDeath();
			}// end if
		}// end public function collect()		
		
		public override function doPhysics():void
		{
						
								
		}// end override function doPhysics():void			
		
	}// end public class EntityCollectible
	
}// end package