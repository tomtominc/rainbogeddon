package com.nitrome.engine
{
	
	public class PlayerHit extends EffectCentered
	{
				
		public function PlayerHit(tx_:uint, ty_:uint)
		{
		
			m_drawFast = true;
			super(tx_, ty_);				
			
		}// end public function PlayerHit(tx_:uint, ty_:uint)
		
	}// end public class PlayerHit
	
}// end package