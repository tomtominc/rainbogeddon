package com.nitrome.engine
{
		
	public class EntityPlayer1SafetyBubble extends EntitySafetyBubble
	{
				
		public function EntityPlayer1SafetyBubble(tx_:uint, ty_:uint)
		{
		
			super(tx_, ty_);		

		}// end public function EntityPlayer1SafetyBubble(tx_:uint, ty_:uint)
		
		public override function get bubbleParticleType():Class
		{
			return Player1BubbleParticle;
		}// end public override function get bubbleParticleType():Class		
		
	}// end public class EntityPlayer1SafetyBubble
	
}// end package