package com.nitrome.engine
{

	
	public class DrillHolder extends WeaponHolder
	{
		
		
		private var m_drill:EntityDrill;
		private var m_drillPlaced:Boolean;
		private var m_drillDamageTable:Object;
		
		public function DrillHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_drill = new EntityDrill(parent_);
			m_parent.m_animModifier = "_drill";
			m_parent.m_forceUpdateAnim = true;
			//m_drill.m_parent = m_parent;
			m_drillDamageTable = new Object();
			m_drillDamageTable[1] = 1;
			m_drillDamageTable[2] = 2;
			m_drillDamageTable[3] = 4;			
			placeDrill();
			m_drillPlaced = false;
		}// end public function DrillHolder()
				
		public function placeDrill():void
		{
					
			if (!m_drillPlaced && Game.g.m_gameObjectList)
			{
				m_drillPlaced = true;
				Game.g.m_gameObjectList.push(m_drill);
			}// end if
			
		}// end public function placeDrill():void
	
		public override function tryFire():Boolean
		{
			
			if (m_parent.m_lastDir != Entity.DIR_NONE)
			{
				m_parent.m_stopSidewaysOnCorridor = true;
				m_parent.m_fire = false;
				m_parent.m_animModifier = "_drill_on";
				m_parent.m_forceUpdateAnim = true;
				m_drill.turnOn();
				return true;
			}// end if			
			
			return false;
			
		}// end public override function tryFire():Boolean			
		
		public override function upgrade():Boolean
		{
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				m_drill.m_damage = m_drillDamageTable[m_currUpgradeStage];
				return true;
			}
			else
			{
				return false;
			}// end else
		}// end public override function upgrade():Boolean				
		
		public override function terminate():void
		{
			
			m_parent.m_stopSidewaysOnCorridor = false;
			m_parent.m_animModifier = "";
			m_parent.m_forceUpdateAnim = true;
			m_drill.terminate();
			
		}// end public override function terminate():void			
		
		public override function onChangeDir():void
		{
			placeDrill();
		}// end public override function onChangeDir():void				
		
		public override function tryStopFire():void
		{
			m_parent.m_stopSidewaysOnCorridor = false;
			m_parent.m_stopFire = false;
			m_parent.m_animModifier = "_drill";
			m_parent.m_forceUpdateAnim = true;
			m_drill.turnOff();
		}// end public override function tryStopFire():void			
		
	}// end public class BulletHolder
	
}// end package