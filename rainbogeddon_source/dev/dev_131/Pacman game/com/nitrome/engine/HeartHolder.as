package com.nitrome.engine
{

	
	public class HeartHolder extends WeaponHolder
	{
		
		private var m_speedTable:Object;
		
		public function HeartHolder(parent_:EntityCharacter)
		{
			super(parent_);
			m_speedTable = new Object();
			m_speedTable[2] = EntityPlayer.SPEED*1.25;
			m_speedTable[3] = EntityPlayer.SPEED*1.5;			
			
		}// end public function HeartHolder()			
			
		public override function upgrade():Boolean
		{
			if (m_currUpgradeStage < MAX_UPGRADE_STAGES)
			{
				super.upgrade();
				m_currUpgradeStage++;
				m_parent.m_speed = m_speedTable[m_currUpgradeStage];
				return true;
			}
			else
			{
				return false;
			}// end else
		}// end public override function upgrade():Boolean					
		
		public override function terminate():void
		{
			
			m_parent.m_speed = EntityPlayer.SPEED;
			
		}// end public override function terminate():void				
		
	}// end public class BulletHolder
	
}// end package