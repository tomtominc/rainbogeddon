package com.nitrome.util.string {
	/**
	 * Checks a string against a list of strings and returns a result with a number at the end if a duplicate
	 * string is found:
	 *
	 * fileName(1)
	 *
	 * In the case of a name already in the list that is in that format and matches the number, the number in parenthesis is incremented:
	 *
	 * fileName(2)
	 *
	 * And so on...
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public function duplicateName(name:String, list:Array):String{
		var i:int;
		var listName:String;
		var parenthesisMatch:Array;
		var count:int = 0;
		var n:int;
		var numbers:Array = [];
		
		// remove any existing parenthesis from the match at the start and capture the number
		if(name.match(/\(\d+\)/)){
			count = name.match(/(?<=\()\d+(?=\))/)[0];
			name = name.replace(/\(\d+\)/, "");
		}
		
		// get numbers for all the copies of this name
		for(i = 0; i < list.length; i++){
			if(list[i] == name || list[i].substr(0, list[i].indexOf("(")) == name){
				// check for a number in parenthesis in the list item
				// if it matches, try to beat that value - otherwise set the count to 1
				parenthesisMatch = list[i].match(/(?<=\()\d+(?=\))/);
				if(parenthesisMatch){
					n = parenthesisMatch[0];
					numbers.push(n);
				} else {
					numbers.push(0);
				}
			}
		}
		
		// sort them
		numbers.sort(function(a:int, b:int):Number{
			if(a < b) return -1;
			else if(a > b) return 1;
			return 0;
		});
		
		// for any number match we set the counter to be the same but one higher
		for(i = 0; i < numbers.length; i++){
			if(count == numbers[i]){
				count = numbers[i] + 1;
			}
		}
		if(count){
			return name + "(" + count + ")";
		}
		return name;
	}

}