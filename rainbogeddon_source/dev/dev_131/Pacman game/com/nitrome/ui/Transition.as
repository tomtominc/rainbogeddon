/**
* Transition
* @author Aaron Steed, nitrome.com
*/

package com.nitrome.ui {
	import com.nitrome.sound.SoundManager;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Transition extends MovieClip{
		
		public var bitmap:Bitmap;
		public var bitmapData:BitmapData;
		public var currentCallback:Object;
		public var nextCallback:Object;
		public var step:Number;
		
		public static const TWEEN_DELAY:int = 15;
	
		public function Transition() {
			// the transition is always central to the screen, so logically the width and height will
			// be double the position
			bitmapData = new BitmapData(x * 2, y * 2, true, 0x00000000);
			bitmap = new Bitmap(bitmapData);
			bitmap.visible = false;
			bitmap.x -= x;
			bitmap.y -= y;
			addChild(bitmap);
			step = 1.0 / TWEEN_DELAY;
		}
		
		/* Initiates a transition or queues another transition if one is already underway */
		public function goto(callback:Object):void{
			if(!currentCallback){
				initTransition(callback);
			} else {
				nextCallback = callback;
			}
		}
		
		/* Prep the transition */
		public function initTransition(callback:Object):void {
			currentCallback = callback;
			var i:int;
			
			bitmapData.fillRect(bitmapData.rect, 0x00000000);
			NitromeGame.root.curvesClip.visible = false;
			bitmapData.draw(parent);
			NitromeGame.root.curvesClip.visible = true;
			bitmap.visible = true;
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			changeOver();
		}
		
		/* Exectute change over */
		private function changeOver():void{
			if(currentCallback is String){
				(parent as MovieClip).gotoAndStop(currentCallback as String);
			} else if(currentCallback is Function){
				(currentCallback as Function)();
			}
		}
		
		/* loop */
		private function update(e:Event):void {
			if(bitmap.alpha > 0){
				bitmap.alpha -= step;
			} else {
				bitmap.alpha = 1.0;
				bitmap.visible = false;
				removeEventListener(Event.ENTER_FRAME, update);
				finish();
			}
		}
		
		/* Finish the transition - queue another if need be */
		private function finish():void{
			currentCallback = null;
			if(nextCallback){
				initTransition(nextCallback);
				nextCallback = null;
			}
		}
	}
	
}
