﻿package com.nitrome.ui.button {
	import flash.events.MouseEvent;
	
	/**
	* Goes to gameReset frame and clears the score
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class TryAgainButton extends BasicButton{
		
		public function TryAgainButton(){
		}
		 
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			if(!NitromeGame.root.popUpHolder.keyPressed) execute();
		}
		public function execute():void{
			NitromeGame.root.popUpHolder.hide();
			Game.m_retrying = true;
			Game.score.value = 0;
			NitromeGame.root.transition.goto("gameReset");
		}
		
	}
}