﻿package com.nitrome.ui.button {
	import com.nitrome.sound.SoundManager;
	import flash.events.MouseEvent;
	
	/**
	* Goto help frame
	*
	* @author Aaron Steed, nitrome.com
	*/
	public class HelpButton extends BasicButton{
		
		override public function onClick(e:MouseEvent):void{
			super.onClick(e);
			//SoundManager.playSound("Help");
			NitromeGame.root.transition.goto("help");
		}
		
		override protected function onMouseOver(e:MouseEvent):void{
			super.onMouseOver(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.menuPosLock(1);
				NitromeGame.root.hatNBowContainer.bow.menuPosLock(1);
			}// end if
		}
		override protected function onMouseOut(e:MouseEvent):void{
			super.onMouseOut(e);
			if (NitromeGame.root.hatNBowContainer)
			{				
				NitromeGame.root.hatNBowContainer.hat.abortMenuPosLock();
				NitromeGame.root.hatNBowContainer.bow.abortMenuPosLock();		
			}// end if
		}			
		
		public override function get clickSound():String
		{
			return "Help";
		}		
		
	}
	
}