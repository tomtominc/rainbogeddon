package com.nitrome.gfx {
	import flash.display.BitmapData;
	/**
	 * Draws a Bresenham Ellipse.
	 *
	 * ported from:
	 * http://homepage.smc.edu/kennedy_john/belipse.pdf
	 *
	 * (if he mentioned it was written in Delphi in the paper it would have saved me some time)
	 *
	 * @author Aaron Steed, nitrome.com
	 */
	public function bresenhamEllipse(cx:int, cy:int, radiusX:int, radiusY:int, bitmapData:BitmapData, col:uint = 0x00000000):void{
		var x:int, y:int;
		var changeX:int, changeY:int;
		var error:int;
		var twoASquare:int, twoBSquare:int;
		var stoppingX:int, stoppingY:int;
		
		twoASquare = 2 * radiusX * radiusX;
		twoBSquare = 2 * radiusY * radiusY;
		x = radiusX;
		y = 0;
		changeX = radiusY * radiusY * (1 - 2 * radiusX);
		changeY = radiusX * radiusX;
		error = 0;
		stoppingX = twoBSquare * radiusX;
		stoppingY = 0;
		
		while(stoppingX >= stoppingY){
			bitmapData.setPixel32(cx + x, cy + y, col);
			bitmapData.setPixel32(cx - x, cy + y, col);
			bitmapData.setPixel32(cx - x, cy - y, col);
			bitmapData.setPixel32(cx + x, cy - y, col);
			y++;
			stoppingY += twoASquare;
			error += changeY;
			changeY += twoASquare;
			if(2 * error + changeX > 0){
				x--;
				stoppingX -= twoBSquare;
				error += changeX;
				changeX += twoBSquare;
			}
		}
		
		x = 0;
		y = radiusY;
		changeX = radiusY * radiusY;
		changeY = radiusX * radiusX * (1 - 2 * radiusY);
		error = 0;
		stoppingX = 0;
		stoppingY = twoASquare * radiusY;
		
		while(stoppingX <= stoppingY){
			bitmapData.setPixel32(cx + x, cy + y, col);
			bitmapData.setPixel32(cx - x, cy + y, col);
			bitmapData.setPixel32(cx - x, cy - y, col);
			bitmapData.setPixel32(cx + x, cy - y, col);
			x++;
			stoppingX += twoBSquare;
			error += changeX;
			changeX += twoBSquare;
			if(2 * error + changeY > 0){
				y--;
				stoppingY -= twoASquare;
				error += changeY;
				changeY += twoASquare;
			}
		}
	}

}