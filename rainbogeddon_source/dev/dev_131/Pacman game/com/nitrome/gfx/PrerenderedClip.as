package com.nitrome.gfx {
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Chris Burt-Brown
	 */
	public class PrerenderedClip extends MovieClip {
		
		public var frames:/*PrerenderedFrame*/Array;
		public static var animationLibrary:/*Array*/Dictionary = new Dictionary;
		
		public var fastRender:Boolean = true;
		
		public var master:LayeredClip = null;
		
		public function PrerenderedClip() {
			var c:Class = Object(this).constructor;
			
			if(animationLibrary[c]) {
				frames = animationLibrary[c];
				return;
			}
			
			frames = animationLibrary[c] = [];
			
			for(var n:Number = totalFrames; n >= 1; n --) {
				gotoAndStop(n);
				frames[n] = new PrerenderedFrame;
				frames[n].setupFrom(this);
				
				// slows down load time, but allows identical frames of animation to be reclaimed
				if(n < totalFrames && frames[n + 1].equals(frames[n])) {
					frames[n + 1] = frames[n];
				}
			}
		}
		
		public function flatten():void {
			/* For dynamic PrerenderedClips, e.g tetris pieces, you might want to fill it with stuff and then convert it to a bitmap. */
			
			var frame:PrerenderedFrame = new PrerenderedFrame;
			frame.setupFrom(this);
			frames = [frame];
		}
		
		public function renderTo(target:BitmapData, cameraPositionX:Number, cameraPositionY:Number):void {
			if(!visible) return;
			
			if(fastRender) {
				var pf:PrerenderedFrame = frames[currentFrame];
				if(pf && pf.data) {
					target.copyPixels(pf.data, pf.dataRect, new Point(cameraPositionX + pf.offsetX, cameraPositionY + pf.offsetY), null, null, true);
				}
			} else {
				var m:Matrix = transform.matrix.clone();
				m.translate(cameraPositionX - x, cameraPositionY - y);
				target.draw(this, m, transform.colorTransform, blendMode);
			}
		}
		
	}

}